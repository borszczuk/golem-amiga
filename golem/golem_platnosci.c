
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
$Id: golem_platnosci.c,v 1.1 2003/01/01 20:40:54 carl-os Exp $.
*/


#include "golem.h"


/// PLAT STOP CHUNKS
#define PLAT_NUM_STOPS (sizeof(Plat_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Plat_Stops[] =
{
        ID_PAY, ID_CAT,
        ID_PAY, ID_VERS,
        ID_PAY, ID_PAY,
        NULL, NULL,
};
//|

/// WczytajP�atno�ci

long WczytajP�atno�ci(void)
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
char   ValidFile = FALSE;



     set(app, MUIA_Application_Sleep, TRUE);

     if(iff = AllocIFF())
       {
       if(iff->iff_Stream = Open(P�atno�ciFileName, MODE_OLDFILE))
           {
           InitIFFasDOS(iff);

           StopChunks(iff, Plat_Stops, PLAT_NUM_STOPS);

           if(!OpenIFF(iff, IFFF_READ))
               {

               while(TRUE)
                  {
                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                     break;

                  if(cn = CurrentChunk(iff))
                     {
                     LONG ID = cn->cn_ID;

                     if(!ValidFile)
                        {
                        if((ID == ID_CAT) && (cn->cn_Type == ID_PAY))
                           {
                           ValidFile = TRUE;
                           continue;
                           }


                        break;
                        }

///                    ID_VERS
                     if(ID == ID_VERS)
                        {
                        struct BaseVersion version;

                        if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                           {

                           }
                        else
                           {
                           _RC = IoErr();
                           break;
                           }

                        continue;
                        }
//|
///                    ID_PAY

                     if(ID == ID_PAY)
                        {
                        struct P�atno��List *p�at = calloc(1, sizeof(struct P�atno��List));

                        if(p�at)
                           {
                           if(ReadChunkBytes(iff, &p�at->pl_p�atno��, cn->cn_Size) != cn->cn_Size)
                              {
                              free(p�at);
                              _RC = IoErr();
                              break;
                              }

                           DodajP�atno��(p�at);
                           }

                        continue;
                        }
//|

                     }
                  }

               CloseIFF(iff);
               }

            Close(iff->iff_Stream);
            }


//       if(_RC == IFFERR_EOF) Error = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

       FreeIFF(iff);
       }

       set(app, MUIA_Application_Sleep, FALSE);


       return(0);
}

//|
/// ZapiszP�atno�ci

char ZapiszP�atno�ci(void)
{
struct IFFHandle *MyIFFHandle;

    set(app, MUIA_Application_Sleep, TRUE);

    if(MyIFFHandle = AllocIFF())
        {
        BPTR  FileHandle;

        if(FileHandle = Open(P�atno�ciFileName, MODE_NEWFILE))
           {
           MyIFFHandle->iff_Stream = FileHandle;
           InitIFFasDOS(MyIFFHandle);

           if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
               {
               struct BaseVersion version;
               struct P�atno��List *p�at;

               PushChunk(MyIFFHandle, ID_PAY, ID_CAT, IFFSIZE_UNKNOWN);

               PushChunk(MyIFFHandle, ID_PAY, ID_FORM, IFFSIZE_UNKNOWN);
                   PushChunk(MyIFFHandle, ID_PAY, ID_VERS, IFFSIZE_UNKNOWN);
                   version.Version = VERSION;
                   version.Revision = REVISION;
                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                   PopChunk(MyIFFHandle);

                   PushChunk(MyIFFHandle, ID_PAY, ID_ANNO, IFFSIZE_UNKNOWN);
                   WriteChunkBytes(MyIFFHandle, ScreenTitle, strlen(ScreenTitle));
                   WriteChunkBytes(MyIFFHandle, " <", 2);
                   WriteChunkBytes(MyIFFHandle, EMAIL, strlen(EMAIL));
                   WriteChunkBytes(MyIFFHandle, ">, ", 3);
                   WriteChunkBytes(MyIFFHandle, WWW, strlen(WWW));
                   PopChunk(MyIFFHandle);
               PopChunk(MyIFFHandle);


               PushChunk(MyIFFHandle, ID_PAY, ID_FORM, IFFSIZE_UNKNOWN);

               if(!IsListEmpty(&p�atno�ci))
                 {
                 for(p�at = (struct P�atno��List *)p�atno�ci.lh_Head; p�at->pl_node.mln_Succ; p�at = (struct P�atno��List *)p�at->pl_node.mln_Succ)
                   {
                   PushChunk(MyIFFHandle, ID_PAY, ID_PAY, IFFSIZE_UNKNOWN);
                   WriteChunkBytes(MyIFFHandle, &p�at->pl_p�atno��, sizeof(struct P�atno��));
                   PopChunk(MyIFFHandle);
                   }
                 }

               PopChunk(MyIFFHandle);

               PopChunk(MyIFFHandle);
               CloseIFF(MyIFFHandle);
               }
           else
               {
               DisplayBeep(0);
               D(bug("*** OpenIFF() nie powiod�o si�\n"));
               }

           Close(FileHandle);
           }
        else
           {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
           D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", P�atno�ciFileName));
           }

        FreeIFF(MyIFFHandle);
        }
     else
        {
        DisplayBeep(0);
        D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
        }

    set(app, MUIA_Application_Sleep, FALSE);


    return(0);
}



//|

/// DodajP�atno��

int DodajP�atno��(struct P�atno��List *p�at)
{
/*
** Dodaje jednostk� do listy
*/

struct P�atno��List *node;
int i = 0;

    if(!IsListEmpty(&p�atno�ci))
       {
       for(node = (struct P�atno��List *)p�atno�ci.lh_Head; node->pl_node.mln_Succ; node = (struct P�atno��List *)node->pl_node.mln_Succ, i++)
           {
           if(StrnCmp(MyLocale, p�at->pl_p�atno��.Nazwa, node->pl_p�atno��.Nazwa, -1, SC_COLLATE2) < 0)
               {
               if((struct Node *)node->pl_node.mln_Pred)
                   Insert(&p�atno�ci, (struct Node *)p�at, (struct Node *)node->pl_node.mln_Pred);
               else
                   AddHead(&p�atno�ci, (struct Node *)p�at);

               return(i);
               }
           }
       }

       AddTail(&p�atno�ci, (struct Node *)p�at);
       return(MUIV_List_Insert_Bottom);
}
//|
/// Znajd�P�atno��Nazwa
struct P�atno��List *Znajd�P�atno��Nazwa(char *Nazwa)
{
/*
** Przeszukuj� list� metod p�atno�ci w poszukiwaniu
** tej o podanej nazwie
*/

struct P�atno��List *node;

    if( strlen(Nazwa) >0 )
       {
       if(!IsListEmpty(&p�atno�ci))
           {
           for(node = (struct P�atno��List *)p�atno�ci.lh_Head; node->pl_node.mln_Succ; node = (struct P�atno��List *)node->pl_node.mln_Succ)
           if(!stricmp(Nazwa, node->pl_p�atno��.Nazwa))
               return(node);
           }
       }

    return(NULL);
}
//|
/// Znajd�P�atno��Numer
struct P�atno��List *Znajd�P�atno��Numer(int Numer)
{
/*
** Przeszukuj� list� rodzajow p�atno�ci w poszukiwaniu
** tej o podanym numerze kolejnym (potrzebne do odszukania
** p�atno�ci wybranej w sajkl-gad�ecie w oknie faktury
*/

struct P�atno��List *node;

    if(!IsListEmpty(&p�atno�ci))
       {
       int count=0;

       for(node = (struct P�atno��List *)p�atno�ci.lh_Head; node->pl_node.mln_Succ; node = (struct P�atno��List *)node->pl_node.mln_Succ)
           {
           if(count == Numer)
               return(node);

           count++;
           }
       }

    return(NULL);
}
//|

/// Usu�P�atno�ci
void Usu�P�atno�ci(char DeleteAll)
{
/*
** Usuwa skasowane p�atno�ci (je�li DeleteAll=FALSE)
** albo ca�� list� (je��i DeleteAll=TRUE)
*/

struct P�atno��List   *p�at, *pl_next;

    if(!IsListEmpty(&p�atno�ci))
       {
       char Delete = FALSE;

       for(p�at = (struct P�atno��List *)p�atno�ci.lh_Head; p�at->pl_node.mln_Succ; )
           {
           Delete = DeleteAll;

           if(DeleteAll == FALSE)
               Delete = p�at->Deleted;

           pl_next = (struct P�atno��List *)p�at->pl_node.mln_Succ;

           if(Delete)
               {
               Remove((struct Node*)p�at);
               free(p�at);
               }

           p�at = pl_next;
           }
       }
}
//|


/// EDYCJA RODZAJU P�ATNO�CI STRINGS

#define MSG_ED_PAY_WIN_TITLE "Rodzaje p�atno�ci"
#define MSG_ED_PAY_NAME      "_Nazwa"
#define MSG_ED_PAY_DELAY     "_Zw�oka"
#define MSG_ED_PAY_CASH      "_Got�wka"
#define MSG_ED_PAY_DAYS      "dni"

#define MSG_ED_PAY_OK        "F10 - _Ok"
#define MSG_ED_PAY_CANCEL    "ESC - Ponie_chaj"

//|

static Object *EdycjaP�atno�ciWindow,             /* EDYCJA RODZAJU P�ATNO�CI */
              *ST_Pay_Nazwa,
              *ST_Pay_Delay,
              *CH_Pay_Cash,
              *BT_Pay_Ok,
              *BT_Pay_Cancel;

/// EdycjaP�atno�ciSetup
char EdycjaP�atno�ciSetup(struct P�atno��List *P�at)
{
struct P�atno�� *p�at = &P�at->pl_p�atno��;

///   EdycjaRodzajuP�atno�ci

                     EdycjaP�atno�ciWindow = WindowObject,
                        MUIA_Window_Title      , MSG_ED_PAY_WIN_TITLE,
                        MUIA_Window_ID         , ID_WIN_ED_PAY,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, ColGroup(2),
                                  GroupFrame,

                                  Child, MakeLabel2(MSG_ED_PAY_NAME),
                                  Child, ST_Pay_Nazwa = MakeString(PAY_NAME_LEN, MSG_ED_PAY_NAME),

                                  Child, MakeLabel2(MSG_ED_PAY_DELAY),
                                  Child, HGroup,
                                         Child, ST_Pay_Delay = MakeNumericString(10, MSG_ED_PAY_DELAY),
                                         Child, MakeLabel2(MSG_ED_PAY_DAYS),
                                         End,
                                  Child, MakeLabel2(MSG_ED_PAY_CASH),
                                  Child, HGroup,
                                         Child, CH_Pay_Cash = MakeCheck(MSG_ED_PAY_CASH),
                                         Child, HVSpace,
                                         End,
                                  End,

                           Child, HGroup,
                                  Child, BT_Pay_Ok     = TextButton(MSG_ED_PAY_OK),
                                  Child, BT_Pay_Cancel = TextButton(MSG_ED_PAY_CANCEL),
                                  End,

                           End,
                      End;
//|

    if( !EdycjaP�atno�ciWindow )
       return(FALSE);


    // notyfikacja

    DoMethod(EdycjaP�atno�ciWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
    DoMethod(EdycjaP�atno�ciWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

    set(CH_Pay_Cash     , MUIA_CycleChain, FALSE);
    DoMethod(CH_Pay_Cash, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_Pay_Delay, 3, MUIM_Set, MUIA_Disabled, MUIV_TriggerValue);

    DoMethod(BT_Pay_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
    DoMethod(BT_Pay_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


    // setting up

    setstring(ST_Pay_Nazwa, p�at->Nazwa);
    setnumstring(ST_Pay_Delay, p�at->Zw�oka);
    setcheckmark(CH_Pay_Cash, p�at->Got�wka);


    _attachwin( EdycjaP�atno�ciWindow );

    set(EdycjaP�atno�ciWindow, MUIA_Window_ActiveObject, ST_Pay_Nazwa);


    return(TRUE);
}
//|
/// EdycjaP�atno�ciFinish
char EdycjaP�atno�ciFinish(struct P�atno��List *P�at, char EditMode)
{
/*
** sprawdza poprawno�� wprowadzonych danych
** dot. p�atno�ci. Zwraca FALSE, je�li co�
** jest nie tak, TRUE, je�li wszystkie
** elementy s� poprawne, oraz REFRESH,
** je�li nazwa produktu zosta�a zmieniona
** (istotne tylko podczas edycji produktu)
*/

struct P�atno�� *p�at = &P�at->pl_p�atno��;
char   result = TRUE;


    if(strlen((char *)xget(ST_Pay_Nazwa, MUIA_String_Contents)) == 0)
       {
       set(EdycjaP�atno�ciWindow, MUIA_Window_ActiveObject, ST_Pay_Nazwa);
       return(FALSE);
       }


    {
    struct P�atno��List *tmp;

    if(EditMode)
      {
      Remove((struct Node *)P�at);
      tmp = Znajd�P�atno��Nazwa((char *)xget(ST_Pay_Nazwa, MUIA_String_Contents));
      DodajP�atno��(P�at);
      }
    else
      {
      tmp = Znajd�P�atno��Nazwa((char *)xget(ST_Pay_Nazwa, MUIA_String_Contents));
      }

    if(tmp)
       {
       set(EdycjaP�atno�ciWindow, MUIA_Window_ActiveObject, ST_Pay_Nazwa);
       return(FALSE);
       }
    }

    if(StrnCmp(MyLocale, p�at->Nazwa, (char *)xget(ST_Pay_Nazwa, MUIA_String_Contents), -1, SC_COLLATE2) != 0)
       result = REFRESH;

    strcpy(p�at->Nazwa, (char *)xget(ST_Pay_Nazwa, MUIA_String_Contents));
    p�at->Zw�oka  = getnumstr(ST_Pay_Delay);
    p�at->Got�wka = getcheckmark(CH_Pay_Cash);

    return(result);

}
//|
/// EdycjaP�atno�ci
char EdycjaP�atno�ci(struct P�atno��List *p�at, char EditMode)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

    set(app, MUIA_Application_Sleep, TRUE);

    if( !EdycjaP�atno�ciSetup(p�at) )
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       _sleep(FALSE);
       return(NULL);
       }


    if(WinOpen(EdycjaP�atno�ciWindow))
        {
        while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);
          switch(ID)
            {
            case ID_OK:
               result = EdycjaP�atno�ciFinish(p�at, EditMode);

               if(result == FALSE)
                 {
                 DisplayBeep(0);
                 }
               else
                 {
                 running = FALSE;
                 }
               break;


            case ID_CANCEL:
               running = FALSE;
               break;

            }
          if(running && signal) Wait(signal);
          }

        set(EdycjaP�atno�ciWindow, MUIA_Window_Open, FALSE);
        }
     else
        {
        DisplayBeep(NULL);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        }

    _detachwin( EdycjaP�atno�ciWindow );
    _sleep(FALSE);

    return(result);

}
//|

