
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
a$Id: golem_custlist_mcc.c,v 1.1 2003/01/01 20:40:54 carl-os Exp $.
*/

/*

aktywacja przez klik

Golem_CustList.mcc: 9D510090
Golem_CustList.mcc: 80426F3F
Golem_CustList.mcc: 804238CA
Golem_CustList.mcc: 804238CA
Golem_CustList.mcc: 9D510090


ruszanie klawiszami

Golem_CustList.mcc:      103
Golem_CustList.mcc: 80426F3F
Golem_CustList.mcc: 804238CA
Golem_CustList.mcc: 804238CA
Golem_CustList.mcc:      104
Golem_CustList.mcc:      104
Golem_CustList.mcc: 804280EC

*/

/// Includes

#include <clib/alib_protos.h>
#include <clib/graphics_protos.h>
#include <clib/utility_protos.h>
#include <clib/muimaster_protos.h>
#include <pragmas/muimaster_pragmas.h>
#include <libraries/mui.h>
#if MUIMASTER_VLATEST <= 14
#include <mui/mui33_mcc.h>
#endif
#include <mui/muiundoc.h>


#include <stdio.h>
#include <string.h>
#include <mui/interlist.h>
#include "golem_strings.h"
#include "golem_structs.h"
#include "golem_custlist_mcc.h"
#include <mui/nlist_mcc.h>

//#define __NAME      "Golem_CustList: "
#define CLASS       MUIC_Golem_CustList
#define __NAME      CLASS ": "
#define SUPERCLASS  MUIC_List

void _XCEXIT(int code) {}

//|
/// Data
struct Data
{
   ULONG UsingNList;

   struct Hook DisplayHook;
};
//|

#include "golem_custlist_revision.h"

#define UserLibID CUSTLIST_VERSTAG " � 1998-2002 Marcin Orlowski <carlos@wfmh.org.pl>"
#define MASTERVERSION 14


#define MYDEBUG 0
#define MCC_USES_LOCALE

#define VERSION CUSTLIST_VERSION
#define REVISION CUSTLIST_REVISION
#include "golem_mccheader_nlist.c"


#define MSG_CUST_LIST_DEL    ""
#define MSG_CUST_LIST_NAME   "\0338\033bNazwa"
#define MSG_CUST_LIST_ULICA  "\0338\033bAdres"
#define MSG_CUST_LIST_KOD    "\0338\033bKod"
#define MSG_CUST_LIST_MIASTO "\0338\033bMiasto"


/// Customer list hooks
/// HOOK: CustomerList_Display
void * __saveds __asm CustomerList_Display(register __a0 struct Hook *hook, register __a2 char **array, register __a1 struct KlientList *node)
{
static char Del[20],
			Name[CUST_NAME_LEN + 10],
			Ulica[CUST_ADRES_LEN + 10],
			Kod[CUST_KOD_LEN + 10],
			Miasto[CUST_MIASTO_LEN + 10];

struct Data *data = hook->h_Data;



/*,
			Ulica[CUST_ADRES_LEN + 10],
			Kod[CUST_KOD_LEN + 10],
			Miasto[CUST_MIASTO_LEN + 10];
*/

   if(node)
	 {
	 struct Klient *klient = &node->kl_klient;

	 if( node->Deleted == FALSE )
	   {
//       sprintf(Del, "%ld", array[-1]+1);
	   sprintf( Del, "%ld", node->numer );
	   }
	 else
	   {
	   Del[0] = '*';
	   Del[1] = 0;
	   }
//     sprintf(Del   , "%s" , ((node->Deleted==FALSE) ? " " : "*"));

	 *array++ = Del;

	 if( klient->Separator )
	   {
	   if( data->UsingNList )
		 {
		 strcpy(Del, "\033t\033c\033E");   // Gruba krecha
		 sprintf(Name, "\033t\033c\033E%s", klient->Nazwa1);   // Gruba krecha

		 *array++ = Name;
		 *array++ = Del;
		 *array++ = Del;
		 *array++ = Del;
		 }
	   else
		 {
		 Del[0] = 0;
		 strcpy(Name, "\033b\0338* * * * * *");   // Gruba krecha
		 strcpy(Kod, "\033b\0338* * *");   // Gruba krecha

		 *array++ = Name;
		 *array++ = Ulica;
		 *array++ = Kod;
		 *array++ = Ulica;
		 }
	   }
	 else
	   {
	   char attr[20] = "";

	   // no NIP no fun
	   if( klient->NIP[0] == 0 )
		 strcpy(attr, "\0338");

	   if( node->Tymczasowy )
		 strcat( attr, "\033b" );

	   sprintf(Name  , "%s%s", attr, klient->Nazwa1);
	   sprintf(Ulica , "%s%s", attr, klient->Ulica);
	   sprintf(Kod   , "%s%s", attr, klient->Kod);
	   sprintf(Miasto, "%s%s", attr, klient->Miasto);

	   *array++ = Name;
	   *array++ = Ulica;
	   *array++ = Kod;
	   *array++ = Miasto;
	   }
	 }
   else
	 {
	 *array++ = MSG_CUST_LIST_DEL;
	 *array++ = MSG_CUST_LIST_NAME;
	 *array++ = MSG_CUST_LIST_ULICA;
	 *array++ = MSG_CUST_LIST_KOD;
	 *array++ = MSG_CUST_LIST_MIASTO;
	 }


   return(0);
}
//|
/// HOOK: CustomerList_CompareStr
LONG __saveds __asm CustomerList_CompareStr(register __a1 struct KlientList *Node1, register __a2 struct KlientList *Node2)
{
	return((LONG)StrnCmp(Locale, Node1->kl_klient.Nazwa1, Node2->kl_klient.Nazwa1, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG _New(struct IClass *cl, Object *obj, Msg msg)
{
//static const struct Hook CustomerList_DisplayHook     = { {NULL, NULL}, (VOID *)CustomerList_Display   ,NULL,NULL };
static const struct Hook CustomerList_CompareHookStr  = { {NULL, NULL}, (VOID *)CustomerList_CompareStr,NULL,NULL };

	D(bug( __NAME "OM_NEW\n"));

	obj = (Object *)DoSuperNew(cl, obj,
					InputListFrame,
					_MUIA_List_AutoVisible   , TRUE,
//                    _MUIA_List_DisplayHook   , &CustomerList_DisplayHook,
					_MUIA_List_CompareHook   , &CustomerList_CompareHookStr,
					_MUIA_List_Format        , "MIW=1 MAW=-1 P=\033r BAR,"
											   "MIW=1 MAW=49 BAR,"
											   "MIW=1 MAW=35 BAR,"
											   "MIW=1 MAW=-1 BAR,"
											   "MIW=1 MAW=-1 ",

					_MUIA_List_Title         , TRUE,

					MUIA_NList_TitleSeparator, TRUE,
					MUIA_NList_AutoCopyToClip, TRUE
					);


		  if(obj)
			 {
			 struct Data *data = INST_DATA(cl,obj);

			 data->UsingNList     = FALSE;

			 if( cl )
			   {
			   D(bug(__NAME "SuperClassID: %s\n", cl->cl_Super->cl_ID));

			   if( strcmp( cl->cl_Super->cl_ID, "NList.mcc" ) == 0)
				   data->UsingNList = TRUE;
			   }


			 data->DisplayHook.h_Data  = (APTR)data;
			 data->DisplayHook.h_Entry = (VOID *)CustomerList_Display;
			 set(obj, _MUIA_List_DisplayHook, &data->DisplayHook );


			 /* trick to set up arguments */

			 msg->MethodID = OM_SET;
			 DoMethodA(obj, (Msg)msg);
			 msg->MethodID = OM_NEW;
			 }

	  return((ULONG)obj);
}

//|
/// OM_SET

ULONG ASM _Set( REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg )
{
struct Data *data = INST_DATA(cl,obj);
struct TagItem *tags,*tag;


//    D(bug( __NAME "OM_SET\n" ));


	for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
	   {
//       D(bug(__NAME "%8lx\n", tag->ti_Tag ));

	   switch(tag->ti_Tag)
		  {
		  case MUIA_List_Active:
			   {
			   LONG active  = 0;
			   LONG set_active;
			   ULONG entries;


			   // let's do the move first...
			   SetSuperAttrs( cl, obj, MUIA_List_Active, tag->ti_Data, TAG_DONE );


			   // now let's check if there's something to be fixed...

			   get( obj, MUIA_List_Active , &active );
			   get( obj, MUIA_List_Entries, &entries );

			   if( active != MUIV_List_Active_Off )
				   {
				   struct KlientList *kl;

				   // is there any separator under active?
				   DoMethod( obj, MUIM_List_GetEntry, active, &kl );
				   if( kl->kl_klient.Separator )
					   {
					   set_active = active;

					   switch( tag->ti_Data )
						   {
						   case MUIV_List_Active_Up:
						   case MUIV_List_Active_Bottom:
						   case MUIV_List_Active_PageUp:
								set_active--;
								if( set_active < 0 )
								   set_active = 1;
								break;


						   case MUIV_List_Active_Down:
						   case MUIV_List_Active_Top:
						   case MUIV_List_Active_PageDown:
								set_active++;
								break;
						   }

					   D(bug( "Active: %ld\n", set_active ));

					   if( ( set_active < 0 ) || ( set_active > entries ) )
						   set_active = MUIV_List_Active_Off;

					   SetSuperAttrs( cl, obj, MUIA_List_Active, set_active, TAG_DONE );
					   }
				   }
			   }
			   return( TRUE );
		  }
	   }

	return( DoSuperMethodA(cl, obj, msg) );
}

//|
/// OM_GET
static ULONG ASM _Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ProductList_Data *data = INST_DATA(cl,obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

	switch(((struct opGet *)msg)->opg_AttrID)
	   {
	   case MUIA_Version:
			*store = CUSTLIST_VERSION;
			return(TRUE);
			break;

	   case MUIA_Revision:
			*store = CUSTLIST_REVISION;
			return(TRUE);
			break;

	   }

	return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher

__saveds __asm ULONG _Dispatcher( REG(a0) struct IClass *cl,
								  REG(a2) Object *obj,
								  REG(a1) Msg msg )
{

//    D(bug(__NAME "%8lx\n", msg->MethodID ));

	switch( msg->MethodID )
	   {
	   case OM_NEW      : return( _New   ( cl, obj, (APTR)msg ));
	   case OM_SET      : return( _Set   ( cl, obj, (APTR)msg ));
	   case OM_GET      : return( _Get   ( cl, obj, (APTR)msg ));
	   }

	return( DoSuperMethodA( cl,obj,msg ) );
}

//|

