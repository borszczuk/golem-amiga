
/*
$Id: golem_custlist_mcc.h,v 1.1 2003/01/01 20:40:54 carl-os Exp $.
*/

/*** Include stuff ***/


#ifndef GOLEM_CUSTLIST_MCC_H
#define GOLEM_CUSTLIST_MCC_H

#ifndef LIBRARIES_MUI_H
#include "libraries/mui.h"
#endif


/*** MUI Defines ***/

#define MUIC_Golem_CustList  "Golem_CustList.mcc"
#define MUIC_Golem_CustListP "Golem_CustList.mcp"
#define Golem_CustListObject MUI_NewObject(MUIC_Golem_CustList


/*** Methods ***/


/*** Method structs ***/


/*** Special method values ***/


/*** Special method flags ***/


/*** Attributes ***/


/*** Special attribute values ***/


/*** Structures, Flags & Values ***/


/*** Configs ***/


#endif /* GOLEM_CUSTLIST_MCC_H */



