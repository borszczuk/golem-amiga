/*
*/


/*** Include stuff ***/


#ifndef GOLEM_CALENDAR_MCC_H
#define GOLEM_CALENDAR_MCC_H

#ifndef LIBRARIES_MUI_H
#include "libraries/mui.h"
#endif


/*** MUI Defines ***/

#define MUIC_Golem_Calendar  "Golem_Calendar.mcc"
#define MUIC_Golem_CalendarP "Golem_Calendar.mcp"
#define Golem_CalendarObject MUI_NewObject(MUIC_Golem_Calendar

#ifndef CARLOS_MUI
#define MUISERIALNR_CARLOS 2447
#define TAGBASE_CARLOS (TAG_USER | ( MUISERIALNR_CARLOS << 16))
#define CARLOS_MUI
#endif

#define CALENDAR_TB (TAGBASE_CARLOS + 0x220)



/*** Methods ***/

//#define MUIM_Tron_Demo           0x8002000b


/*** Method structs ***/

#define MUIM_Calendar_OpenCalendar   CALENDAR_TB +  0   /* PRIVATE */


/*** Special method values ***/


/*** Special method flags ***/


/*** Attributes ***/

//#define MUIA_DateString_Date           CALENDAR_TB +  100


/*** Special attribute values ***/


/*** Structures, Flags & Values ***/

//struct MUIP_BankString_Fill           { ULONG MethodID; long Fill; };


/*** Configs ***/

//#define MUICFG_Tron_Pen1           0x80020005


#endif /* GOLEM_CALENDAR_MCC_H */



