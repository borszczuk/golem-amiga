
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
** $Id: golem_inspektor.c,v 1.1 2003/01/01 20:40:54 carl-os Exp $.
*/


#define NO_MT_MACROS
#include "golem.h"


#include "golem_inspektor_revision.h"


#include "golem_bstring_revision.h"
#include "golem_custlist_revision.h"
#include "golem_unitlist_revision.h"
#include "golem_paylist_revision.h"
#include "golem_bankstring_revision.h"
#include "golem_prtlist_revision.h"

#include "//vapor/tip/tip_revision.h"


char *Title2   = "Golem Inspektor � " INSPEKTOR_VERSIONREVISION ;


Object *InspektorWindow,
       *LV_Inspektor_List,
       *BT_Inspektor_Ok;

/// _DoSuperNew()

ULONG __stdargs _DoSuperNew(struct IClass *cl, Object *obj, ULONG tag1, ...)
{
    return(DoSuperMethod(cl, obj, OM_NEW, &tag1, NULL));
}
//|

#define MSG_INSP_OK   "_Ok"
#define MSG_INSP_BRAK "\033c\033bBRAK"

/// RESULT LIST CLASS

struct MUI_CustomClass *CL_ResultList = NULL;

/// Data
struct ResultList_Data
{
   ULONG Dummy;
};
//|

/// HOOK: ResultList_Display
void * __saveds __asm ResultList_Display(register __a2 char **array, register __a1 struct Result *node)
{
static char Del[16], attr[10], nazwa[50], required[25], available[25] ;

   if(node)
     {
     if( node->fatal )
       strcpy(attr, "\033b\0338");
     else
       attr[0] = 0;

     sprintf( nazwa, "%s%s", attr, node->nazwa);
     *array++ = nazwa;

     sprintf( required, "%s%s", attr, node->req_ver);
     *array++ = required;

     sprintf( available, "%s%s  %s", attr, node->avail_ver,  node->fatal ? "<--" : "Ok" );
     *array++ = available;
     }
   else
     {
     *array++ = "Element";
     *array++ = "Wymagany";
     *array++ = "Dost�pny";
     }


     return(0);
}
//|
/// HOOK: ResultList_Construct
APTR __saveds __asm ResultList_Construct(register __a2 APTR pool, register __a1 struct Result *node)
{
struct  Result *new;

        if( new = malloc(sizeof(struct Result)) )
           {
           memcpy(new, node, sizeof(struct Result));
           }

        return(new);
}
//|
/// HOOK: ResultList_Destruct
VOID __saveds __asm ResultList_Destruct(register __a2 APTR pool, register __a1 struct Result *node)
{
        free(node);
}
//|

/// OM_NEW

ULONG ResultList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook ResultList_DisplayHook     = { {NULL, NULL}, (VOID *)ResultList_Display   ,NULL,NULL };
static const struct Hook ResultList_ConstructHook   = { {NULL, NULL}, (VOID *)ResultList_Construct ,NULL,NULL };
static const struct Hook ResultList_DestructHook    = { {NULL, NULL}, (VOID *)ResultList_Destruct  ,NULL,NULL };

    obj = (Object *)_DoSuperNew(cl,obj,
                    InputListFrame,
                    MUIA_List_AutoVisible  , TRUE,
                    MUIA_List_DisplayHook  , &ResultList_DisplayHook,
                    MUIA_List_ConstructHook, &ResultList_ConstructHook,
                    MUIA_List_DestructHook , &ResultList_DestructHook,

                    MUIA_List_Format       , "MIW=1 P=\033l BAR, MAW=-1 BAR, ",
                    MUIA_List_Title        , TRUE,


                TAG_MORE, msg->ops_AttrList);


      if(obj)
          {
          struct ResultList_Data *data = INST_DATA(cl,obj);

//          DoMethod(obj, MUIM_ResultList_Init, NULL);

//          msg->MethodID = OM_SET;
//          DoMethodA(obj, (Msg)msg);
//          msg->MethodID = OM_NEW;

          }

      return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG ResultList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

    switch (msg->MethodID)
       {
       case OM_NEW                   : return(ResultList_New       (cl, obj, (APTR)msg));
       }

    return(DoSuperMethodA(cl,obj,msg));
}

//|

//|

/// iTextButton
Object *iTextButton(char *num)
{
// buton textowy

        Object *obj = MUI_MakeObject(MUIO_Button,num);

        if(obj) set(obj, MUIA_CycleChain, TRUE);
        return(obj);
}
//|
/// _WinOpen
ULONG _WinOpen( Object *win )
{
ULONG result;

    set( win, MUIA_Window_Open, TRUE );
    get( win, MUIA_Window_Open, &result );
    return( result );
}
//|

/// CheckClass

char CheckClass( char *name, ULONG req_ver, ULONG req_rev )
{
Object *obj;
struct Result result = {0};

    obj = MUI_NewObject( name, TAG_DONE );

    strcpy( result.nazwa, name );

    if( obj )
       {
       ULONG ver;
       ULONG rev;

       get( obj, MUIA_Version, &ver);
       get( obj, MUIA_Revision, &rev);

//       if( (ver < 10) || (ver > 30) )
//           ver = 0;
//       if( (rev < 10) || (rev > 30) )
//           rev = 0;

       sprintf(result.req_ver, "v%ld.%ld", req_ver, req_rev );
       sprintf(result.avail_ver, "v%ld.%ld", ver, rev );

//       printf("%ld, av: %ld\n", ((req_ver*100) + req_rev), ((ver*100) + rev) );

       if( ((req_ver*100) + req_rev) > ((ver*100) + rev) )
           result.fatal = TRUE;


       MUI_DisposeObject( obj );
       }
    else
       {
       strcpy( result.avail_ver, MSG_INSP_BRAK );
       result.fatal = TRUE;
       }


    DoMethod( LV_Inspektor_List, MUIM_List_InsertSingle, &result, MUIV_List_Insert_Bottom );

    return( result.fatal );
}

//|
/// CheckLib

char CheckLib( char *name, UWORD req_ver, UWORD req_rev )
{
APTR base;
struct Result result = {0};

    base = OpenLibrary( name, 0 );

    strcpy( result.nazwa, name );

    if( base )
       {
       UWORD ver;
       UWORD rev;
       struct Library *lib = (struct Library *)base;

       ver = lib->lib_Version;
       rev = lib->lib_Revision;

       sprintf(result.req_ver, "v%ld.%ld", req_ver, req_rev );
       sprintf(result.avail_ver, "v%ld.%ld", ver, rev );

       if( ((req_ver*100) + req_rev) > ((ver*100) + rev) )
           result.fatal = TRUE;

       CloseLibrary( base );
       }
    else
       {
       strcpy( result.avail_ver, MSG_INSP_BRAK );
       result.fatal = TRUE;
       }


    DoMethod( LV_Inspektor_List, MUIM_List_InsertSingle, &result, MUIV_List_Insert_Bottom );

    return( result.fatal );
}

//|

/// Inspector_CreateClass
struct MUI_CustomClass * Inspector_CreateClass( char *list_class )
{
    return( MUI_CreateCustomClass(NULL, list_class, NULL, sizeof(struct ResultList_Data), ResultList_Dispatcher) );
}
//|

/// InspektorSetup
char InspektorSetup(void)
{
char result = FALSE;

    InspektorWindow = WindowObject,
       MUIA_Window_Title      , Title2,
       MUIA_Window_ID         , 0x0000,
//       MUIA_Window_ScreenTitle, ScrTitle,
       WindowContents,
           VGroup,
               Child, LV_Inspektor_List = ListviewObject,
                      MUIA_CycleChain, TRUE,
                      _MUIA_Listview_List, NewObject(CL_ResultList->mcc_Class, NULL, TAG_DONE),
                      End,

               Child, BT_Inspektor_Ok = iTextButton( MSG_INSP_OK ),
           End,
       End;


    if( InspektorWindow )
       {
       _disable( BT_Inspektor_Ok );

       DoMethod( InspektorWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL );
       DoMethod( BT_Inspektor_Ok, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK );

       DoMethod( app, OM_ADDMEMBER, InspektorWindow );


       result = TRUE;
       }

    return( result );
}
//|
/// Inspektor
char Inspektor( Object *ParentWindow )
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;


    _sleep(TRUE);

    if( ! InspektorSetup() )
       {
       DisplayBeep(0);
       MUI_Request(app, ParentWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       _sleep(FALSE);
       return(NULL);
       }


    if( _WinOpen(InspektorWindow) )
        {
        char cnt = 0;

        _sleep(TRUE);

        // inspecting...

        cnt += CheckClass( MUIC_Golem_BString, BSTRING_VERSION, BSTRING_REVISION );
        cnt += CheckClass( MUIC_Golem_CustList, CUSTLIST_VERSION, CUSTLIST_REVISION );
        cnt += CheckClass( MUIC_Golem_UnitList, UNITLIST_VERSION, UNITLIST_REVISION );
        cnt += CheckClass( MUIC_Golem_PayList, PAYLIST_VERSION, PAYLIST_REVISION );
        cnt += CheckClass( MUIC_Golem_BankString, BANKSTRING_VERSION, BANKSTRING_REVISION );
        cnt += CheckClass( MUIC_Golem_PrtList, PRTLIST_VERSION, PRTLIST_REVISION );
        cnt += CheckClass( MUIC_Tipwindow, TIP_VERSION, TIP_REVISION );

        cnt += CheckLib( WFMH_NAME, WFMH_VMIN, 0 );
        cnt += CheckLib( MUITOOLKIT_NAME, MUITOOLKIT_VMIN, 0 );

//        CheckClass( MUIC_Golem_BetterString );

        // done

        _enable( BT_Inspektor_Ok );
        _sleep(FALSE);

        set( InspektorWindow, MUIA_Window_ActiveObject, BT_Inspektor_Ok );

        if( cnt > 0 )
           {
           MUI_Request(app, InspektorWindow, 0, Title2, "*_Ok",
                   "\033c\033u\033bU W A G A !\033n\n\n"
                   "Z testu wynika, i� \033b\0338%ld\033n\0332 element�w\n"
                   "jest zbyt starych (b�d� ich nie ma wcale) aby\n"
                   "Golem m�g� pracowa� poprawnie. R�wnie� starsze ich\n"
                   "mog� znajdowa� si� aktualnie w pami�ci komputera\n"
                   "(wykonaj polecenie AVAIL FLUSH a je�li to nie pomo�e,\n"
                   "uruchom komputer na nowo i ponownie przeprowad� test.\n"
                   "\n"
                   "Wi�kszo�� komponent�w stosowanych przez pakiet\n"
                   "znale�� mo�na tak�e na stronie projektu\n"
                   "W).F.M.H. Common Software Library, pod adresem\n"
                   "\n"
                   "\033bhttp://wfmh.sf.net/\033n"
                   ,
                   cnt
                   );
           }

        while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);
          switch(ID)
            {
            case ID_OK:
            case ID_CANCEL:
                 running = FALSE;
                 break;

            }
          if(running && signal) Wait(signal);
          }

        }
     else
        {
        DisplayBeep(NULL);
        MUI_Request(app, ParentWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        }

    _detachwin( InspektorWindow );
    _sleep(FALSE);

    return(result);

}
//|


