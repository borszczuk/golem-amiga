
/*
$Id: golem_bankstring_mcc.h,v 1.1 2003/01/01 20:40:53 carl-os Exp $.
*/


/*** Include stuff ***/


#ifndef GOLEM_BANKSTRING_MCC_H
#define GOLEM_BANKSTRING_MCC_H

#ifndef LIBRARIES_MUI_H
#include "libraries/mui.h"
#endif


/*** MUI Defines ***/

#define MUIC_Golem_BankString  "Golem_BankString.mcc"
#define MUIC_Golem_BankStringP "Golem_BankString.mcp"
#define Golem_BankStringObject MUI_NewObject(MUIC_Golem_BankString

#ifndef CARLOS_MUI
#define MUISERIALNR_CARLOS 2447
#define TAGBASE_CARLOS (TAG_USER | ( MUISERIALNR_CARLOS << 16))
#define CARLOS_MUI
#endif

#define BANKSTRING_TB (TAGBASE_CARLOS + 0x100)



/*** Methods ***/

//#define MUIM_Tron_Demo           0x8002000b


/*** Method structs ***/

#define MUIM_BankString_DisplayInfo   ( BANKSTRING_TB +  0 )   /* PRIVATE */
#define MUIM_BankString_Validate      ( BANKSTRING_TB +  1 )   /* PRIVATE */
#define MUIM_BankString_Fill          ( BANKSTRING_TB +  2 )   /* PRIVATE */
#define MUIM_BankString_ReadBank      ( BANKSTRING_TB +  3 )   /* PRIVATE */
#define MUIM_BankString_ReadBankInfo  ( BANKSTRING_TB +  4 )   /* PRIVATE */


/*** Special method values ***/


/*** Special method flags ***/


/*** Attributes ***/

#define MUIA_BankString_Account   ( BANKSTRING_TB + 100 )
#define MUIA_BankString_NoInput   ( BANKSTRING_TB + 101 )   /* {I..} v15.0 */


/*** Special attribute values ***/


/*** Structures, Flags & Values ***/

struct MUIP_BankString_Fill           { ULONG MethodID; long Fill; };


/*** Configs ***/

//#define MUICFG_Tron_Pen1           0x80020005


#endif /* GOLEM_BANKSTRING_MCC_H */



