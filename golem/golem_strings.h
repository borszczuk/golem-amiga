
/// APPLICATION

#define MSG_APP_DESC     "System finansowo-ksi�gowy"
#define MSG_WINDOW_TITLE "System finansowo-ksi�gowy"

#define MSG_APP_QUIT     "\033cNa pewno chcesz sko�czy�\nprac� z programem?"
#define MSG_APP_QUIT_GAD "_Tak|*_Nie"

//|
/// APPLICATION � ERROR MESSAGES

#define MSG_ERR_MUI         "Golem wymaga pakietu MUI\nw wersji 3.8 lub nowszej"
#define MSG_ERR_LIB         "Nie mog� otworzy� biblioteki \"%s\", v%ld+"
#define MSG_WAR_NO_POLSKI   "Do swojej poprawnej pracy Golem wymaga zainstalowanego\npakietu WFMH LocalePL w wersji v2! lub nowszego.\n\nJako legalny u�ytkownik systemu Golem masz prawo do 20%\nzni�ki przy zakupie pakietu WFMH LocalePL."
#define MSG_ERR_SQL         "Nie mog� otworzy� biblioteki msql.library"
#define MSG_ERR_SQL_CONNECT "Nie mog� po��czy� si� z serwerem SQL"

#define MSG_ERR_APP         "Nie mog� utworzy� aplikacji MUI"
#define MSG_ERR_MEMORY      "Zbyt ma�o wolnej pami�ci\naby mo�na by�o kontunuowa�!\nZako�cz prac� innych program�w,\ni wtedy spr�buj ponownie."

#define MSG_ERR_DATE        "Nie mo�esz uruchomi� systemu GOLEM,\nponiewa� aktualna data jest \0338wcze�niejsza\0332,\nni� data ostatniego uruchomienia GOLEMa!\n\n\033iSprawd� ustawienia zegara systemowego!"
#define MSG_ERR_DATE_GAD    "_Ustawienia zegara|*_Sko�cz"

#define MSG_ERR_OK          "Ok"

#define MSG_ERR_WINDOW      "Nie mog� otworzy� okna!"

//|

/// DRUKOWANIE + PARSER

#define MSG_ERR_TEMPLATE  "\033cNie mog� otworzy� formatki drukowanego dokumentu!\nDokument nie zostanie wydrukowany!"

#define MSG_ERR_PRT_INIT  "\033cNie mog� zainicjalizowa� drukarki!\nDokument nie zostanie wydrukowany!"

//#define MSG_CMD_GOLEM     "-- System WFMH Golem %s Amiga (C) 1997-2000 W.F.M.H. --- (091) 4825283 -- http://amiga.com.pl/golem/ --"
#define MSG_CMD_GOLEM     "-- System Golem %s Amiga (C) 1997-2000 W.F.M.H. -- http://amiga.com.pl/golem/ --"

//|

/// DEMO

#define MSG_DEMO_TEXT  "\
\033c\
\033u\033bGolem " VERSIONREVISION " BETA-DEMO (" DATE ")\033n\n\
\n\
To jest wersja \033bBETA-DEMO\033n systemu Golem.\n\
\n\
\033bBeta-demo\033n oznacza, i� w przeciwie�stwie do powszechnie\n\
znanych wersji demo, b�d�cych jedynie okrojonymi wersjami program�w,\n\
ta wersja powsta�a przez okrojenie produktu b�d�cego jeszcze w produkcji!\n\
\n\
Jej zadaniem nie jest pokazanie pe�nych mo�liwo�ci i funkcji systemu,\n\
gdy� na tym etapie nie jest to jeszcze mo�liwe, jednak�e pozwala\n\
\033bzgrubnie\033n zapozna� si� z systemem, tu za� z modu�em magazyn-sprzeda�.\n\
\n\
Ta wersja Golema przestanie dzia�a� dnia " EXPIREDATE "\n\
\n\
Informacje na temat warunk�w przedsprzeda�y systemu Golem\n\
znajdziesz w za��czonej do programu dokumentacji!"


//|

/// STARTUP WINDOW

#define MSG_STARTUP_INIT      "Inicjalizuj�..."
#define MSG_STARTUP_DIRS      "Sprawdzam katalogi robocze..."
#define MSG_STARTUP_HELP      "Sprawdzam pliki pomocy..."
#define MSG_STARTUP_USERS     "Wczytuj� list� u�ytkownik�w..."
#define MSG_STARTUP_STOCK     "Wczytuj� dane magazynowe..."
#define MSG_STARTUP_CUSTOMERS "Wczytuj� list� kontrahent�w..."
#define MSG_STARTUP_NUMBERS   "Wczytuj� informacje o dokumentach..."
#define MSG_STARTUP_DATES     "Sprawdzam kolejno�� dat..."
#define MSG_STARTUP_SQL       "��cze si� z serwerem SQL..."

#define MSG_STARTUP_OK        "_Ok"

//|


/// UNIVERSAL STRINGS

#define MSG_OK          "*_Ok"
#define MSG_CANCEL      "Ponie_chaj"
#define MSG_CONT_CANCEL "_Kontynuuj|*Ponie_chaj!"

//|

/// LISTA U�YTKOWNIK�W

#define MSG_USER_LIST_DEL ""
#define MSG_USER_LIST_NAME "\0338\033bImi� i nazwisko"

//|

/// LISTA PRODUKT�W

#define MSG_PROD_LIST_DEL    ""
#define MSG_PROD_LIST_INDEX  "\0338\033bIndeks"
#define MSG_PROD_LIST_NAME   "\0338\033bNazwa"
#define MSG_PROD_LIST_SWW    "\0338\033bSWW"
#define MSG_PROD_LIST_NETTO  "\0338\033bNetto"
#define MSG_PROD_LIST_VAT    "\0338\033bVAT"
#define MSG_PROD_LIST_BRUTTO "\0338\033bBrutto"
#define MSG_PROD_LIST_JEDN   "\0338\033bJ.m."
#define MSG_PROD_LIST_AMOUNT "\0338\033bIlo��"

//|
/// MAGAZYN WINDOW

#define MSG_MAG_WIN_TITLE "Edycja magazynu"

#define MSG_MAG_GROUP "_Grupa"

#define MSG_MAG_SELECT    "_Nazwa"
#define MSG_MAG_SELECTIDX "I_ndeks"

#define MSG_MAG_FIND   "F7 - Znajd�"

#define MSG_MAG_GROUP_DODAJ  "_+"
#define MSG_MAG_GROUP_SKASUJ "_-"
#define MSG_MAG_GROUP_EDYTUJ "Ed_ytuj"

#define MSG_MAG_DODAJ     "F1 - Dod_aj"
#define  SH_MAG_DODAJ     "Dodaj nowy element do aktualnej grupy magazynowej\n\nRodzaj dodawanego elementu (towary b�d� us�ugi) zale�y\nod typu grupy (patrz \"Edycja\" grupy). Je�li chcesz\nmiesza� typy element�w magazynowych w obr�bie danej\ngrupy, u�yj skr�tu \"Shift F1\", kt�ry zmienia typ\ndodawanego elementu (z towar�w na us�ugi i vice versa)"
#define MSG_MAG_EDYTUJ    "F2 - _Edytuj"
#define  SH_MAG_EDYTUJ    "Edycja parametr�w zaznaczonego\nelementu magazynowego"
#define MSG_MAG_USUN      "DEL - _Usu�"
#define  SH_MAG_USUN      "Zaznacza aktywny element magazynowy jako\nprzeznaczony do skasowania. Golem fizycznie\nusunie dane elementy z bazy magazynowej\ndopiero w momencie zako�czenia edycji\ni finalnego zapisu bazy.\n\nJe�li element jest ju� zaznaczony (patrzy symbol\n\"*\" w pierwszej kolumnie listy) do skasowania,\nponowne u�ycie tej opcji anuluje to zaznaczenie"
#define MSG_MAG_PRZENIES  "F6 - _Przenie�"
#define  SH_MAG_PRZENIES  "Pozwala przenie�� zanaczony element\ndo innej grupy magazynowej"
#define MSG_MAG_PRINT     "_Wydrukuj"
#define  SH_MAG_PRINT     "Przej�cie do okna r��nych wydruk�w magazynowych"
#define MSG_MAG_IMPORT    "Impo_rt"
#define  SH_MAG_IMPORT    "Pozwala na import danych magazynowych z innych program�w"
#define MSG_MAG_DOC_PRINT "Drukuj� list� magazynow�..."


#define MSG_MAG_PRINT_Q  "\033cNa pewno chcesz wydrukowa� wszystkie (%ld)\n wy�wietlone elementy grupy\naktywnej (\0338%s\0332)?"
#define MSG_MAG_PRINT_G  "_Tak, wydrukuj|*_Nie"


#define MSG_MAG_FILTER "_Filtr"
#define  SH_MAG_FILTER "Umo�liwia ustawienie filtra\nwy�wietlania element�w"
#define MSG_MAG_FILTER_ALL "_*"
#define  SH_MAG_FILTER_ALL "Wy��cza filtr wy�wietlania (je�li\ntakowy jest aktywny) i wy�wietla\nwszystkie elementy"

#define MSG_MAG_OK "F10 - Zapisz magazyn"
#define MSG_MAG_CANCEL "ESC - Anuluj wszystkie zmiany"

#define MSG_MAG_PROD_DELETED "Produkt \"%s\"\njest aktualnie zaznaczony jako \033bSKASOWANY\033n,\ni nie mo�e by� modyfikowany. Czy chcesz go teraz\nodkasowa� i przyst�pi� do edycji?"
#define MSG_MAG_PROD_DELETED_GAD "*_Tak, odkasuj|_Nie"

#define MSG_MAG_NO_GROUP_ERROR "Nie mog� dopisa� nowego produktu,\ngdy� nie zdefiniowa�e� jeszcze �adnej\njeszcze grupy magazynowej!"

#define MSG_MAG_GROUP_DELETED "Grupa \"%s\"\njest aktualnie zaznaczona jako \033bSKASOWANA\033n,\ni nie mo�e by� modyfikowana! Je�li chcesz dokona�\nw niej jakichkolwiek zmian, musisz j� najpierw odkasowa�.\nCzy chcesz to zrobi� teraz?"

//#define MSG_MAG_NO_GROUPS "\033c\033b\0338BRAK GRUP MAGAZYNOWYCH"
#define MSG_MAG_NO_GROUPS "BRAK GRUP MAGAZYNOWYCH"



#define MSG_MAG_SAVE "\033cNa pewno chcesz zapisa� aktualny\nstan magazynu? Je�li jakie� produkty\nb�d� grupy magazynowe zosta�y zaznaczone jako\nskasowane, zostan� one usuni�te przed zapisaniem\nstanu magazynu.\n\nNa pewno chcesz kontynuowa�?"
#define MSG_MAG_SAVE_GAD "_Tak, zapisz|*_Nie"

#define MSG_MAG_CANCEL_REQ     "\033cNa pewno chcesz zako�czy� edycj�\nmagazynu i uniewa�ni� wszystkie\nwprowadzone do� zmiany?"
#define MSG_MAG_CANCEL_REQ_GAD "_Tak, zako�cz|*_Nie"


#define MSG_ERR_LOAD_STOCK "B��dy podczas wczytywania\ndanych magazynowych:\n\n%5ld b��dnych produkt�w\n%5ld b��dnych grup\n%5ld produkt�w bez grupy\n%5ld duplikat�w grup\n\nWczytano %ld element�w."


#define MSG_ERR_DOC_EXISTS     "\033c\033b%s\033n\n\nDokument o tym numerze ju� istnieje!\nJe�li wystawisz nowy dokument, poprzedni\nrekord w bazie danych zostanie skasowany!\n\nJe�li nie wystawiasz tego dokumentu\ncelowo, przerwij t� operacj�, a nast�pnie\nsprawdz ustawienia numeracji dokument�w,\nb�d� sprawd� wystawione ju� dokumenty\n\nNa pewno chcesz kontynuowa�?"
#define MSG_ERR_DOC_EXISTS_GAD "_Tak|*_Nie"


//|



/// EDYCJA UPRAWNIE� U�YTKOWNIKA STRINGS

#define MSG_ACC_WIN_TITLE "Uprawnienia u�ytkownika"

#define MSG_USER_ED_NAME   "N_azwisko"
#define MSG_USER_ED_PASSWD "_Has�o"
#define MSG_USER_ED_ACESS  "_Uprawnienia"

#define MSG_ACC_MISC_PAGE       "Og�lne"
#define MSG_ACC_MISC_PAGE_TITLE "Uprawnienia og�lne"


#define MSG_ACC_OK     "_Ok"
#define MSG_ACC_CANCEL "Ponie_chaj"

//|


/// FILTR

#define MSG_FILTR_WIN_TITLE "Filtr wy�wietlania"
#define MSG_FILTR_NAME "_Filtr nazwy"

#define MSG_FILTR_OK "_Zastosuj"
#define MSG_FILTR_CANCEL "Ponie_chaj"
//|



/// PROGRESS WINDOW

#define MSG_PROG_WIN_TITLE "Pracuj�..."

#define MSG_PROG_MAG_SAVE  "Zapisuj� dane magazynowe..."
#define MSG_PROG_DOC_PRINT "Drukuj� dokumenty..."
#define MSG_PROG_DOC_SAVE  "Zapisuj� dane dokumentu..."
#define MSG_PROG_COM_CALL  "Wywo�uj� program dot. zdarzenia..."
#define MSG_PROG_POS_PRINT "Drukuj� paragon fiskalny..."

//|

/// LISTA DOKUMENT�W

#define MSG_DOC_LIST_TYP   "\033b\0338Typ"
#define MSG_DOC_LIST_ODB   "\033b\0338Odbiorca"
#define MSG_DOC_LIST_NIP   "\033b\0338NIP"
#define MSG_DOC_LIST_REGON "\033b\0338Regon"
#define MSG_DOC_LIST_NUMER "\033b\0338Numer"
#define MSG_DOC_LIST_DATA  "\033b\0338Data"
#define MSG_DOC_LIST_TOTAL "\033b\0338Brutto"
//|

/// KONIEC MIESIACA

#define MSG_KM_INFO     "\033c\033u\033bKoniec okresu rachunkowego!\033n\nZako�czy� si� w�a�nie miesiac rachunkowy, musisz\nzatem zamkn�� ksi�gi, przed przyst�pieniem do\ndalszej pracy z programem"
#define MSG_KR_INFO     "\033c\033u\033bKoniec okresu rachunkowego!\033n\nZako�czy� si� w�a�nie rok rachunkowy, musisz\nzatem zamkn�� ksi�gi, przed przyst�pieniem do\ndalszej pracy z programem"

//|

/// ODBIORCA SELECTOR

#define MSG_ODB_TITLE     "Wybierz odbiorc�"
#define MSG_ODB_SEL_TITLE "Wybierz odbiorc�"

#define MSG_ODB_ADD_TITLE  "Nazwisko nowego odbiorcy"
#define MSG_ODB_NOFIFO     "Wszystkie pola na nazwiska odbiorc�w\ns� ju� zaj�te. Czy chcesz u�y�\nodbiorcy \"%s\"\njednorazowo do niniejszego dokumentu?"
#define MSG_ODB_NOFIFO_GAD "*_Tak, u�yj|_Nie"
#define MSG_ODB_UPVAT_EXP  "Upowa�nienie VAT tego klienta\nstraci�o wa�no�� dn. %s!"

#define MSG_ODB_OK     "_Ok"
#define MSG_ODB_ADD    "F1 - Dod_aj"
#define MSG_ODB_CANCEL "Ponie_chaj"     

//|


/// STRING REQUESTER

#define MSG_STR_TITLE  "Wpisz tekst"

#define MSG_STR_STRING "_Tekst"
#define MSG_STR_OK     "_Ok"
#define MSG_STR_CANCEL "Ponie_chaj"

//|


/// ZAKUPY

#define MSG_ZAK_TITLE      "Zakupy"

#define MSG_ZAK_MIESIAC    "_Miesi�c"
#define MSG_ZAK_LOAD       "_Wczytaj"
#define MSG_ZAK_SAVE       "Zapi_sz"
#define MSG_ZAK_INDEX      "_Indeksuj"
#define MSG_ZAK_SORT       "_Posortuj"

#define MSG_ZAK_LIST_DEL   ""
#define MSG_ZAK_LIST_LP    "\0338\033bLP"
#define MSG_ZAK_LIST_NUMER "\0338\033b2.Numer"
#define MSG_ZAK_LIST_DATA  "\0338\033b3.Data"
#define MSG_ZAK_LIST_NAZWA "\0338\033b6.Nazwa"
#define MSG_ZAK_LIST_BRUTTO"\0338\033b8.Brutto"
#define MSG_ZAK_LIST_VAT   "\0338\033b18.Vat"

#define MSG_ZAK_ADD        "F1 - Dod_aj"
#define MSG_ZAK_EDIT       "F2 - _Edytuj"
#define MSG_ZAK_DEL        "DEL - _Usu�"
#define MSG_ZAK_PRINT      "F5 - Wydrukuj"
#define MSG_ZAK_INFO       "Info"

#define MSG_ZAK_OK         "F10 - Ok"
#define MSG_ZAK_CANCEL     "ESC - Anuluj wszyskie zmiany"

//|
/// EDYCJA ZAKUPU

#define MSG_ZAK_EDIT_TITLE  "Edycja zakupu"

#define MSG_ZAKE_PAGE1      "F1 - Dokument"
#define MSG_ZAKE_PAGE2      "F2 - Zakupy 1"
#define MSG_ZAKE_PAGE3      "F3 - Zakupy 2"

#define MSG_ZAKE_VAT1       "(VAT "
#define MSG_ZAKE_VAT2       ")"
#define MSG_ZAKE_P1         "1. _Lp"
#define MSG_ZAKE_P2         "2. _Numer"
#define MSG_ZAKE_P3         "3. _Data otrzymania"
#define MSG_ZAKE_P4         "4. Data _wystawienia"
#define MSG_ZAKE_P5         "5. _Kontrahent"

#define MSG_ZAKE_P9         "9. _Zakupy nie do odlicze�"
#define MSG_ZAKE_P10        "10. Netto (22%)"
#define MSG_ZAKE_P12        "12. Netto (7%)"

#define MSG_ZAKE_P14        "14. Netto (22%)"
#define MSG_ZAKE_P16        "16. Netto (7%)"

#define MSG_ZAKE_P8         "8. Warto�� zakupu brutto"
#define MSG_ZAKE_P18        "18. VAT podlegaj�cy doliczeniu"

#define MSG_ZAKE_OPIS       "Opis zdarzenia"

#define MSG_ZAKE_INFO1      "\033c\0338Zakupy, za kt�re nie przys�uguje\nodliczenie, zakupy nieopodatkowane\nzakupy opodatkowane stawk� 0%"
#define MSG_ZAKE_INFO2      "\033c\0338Zakupy opodatkowane s�u��ce\nsprzeda�y opodatkowanej"
#define MSG_ZAKE_INFO3      "\033c\0338Zakupy opodatkowane s�u��ce sprzeda�y\nopodatkowanej i zwolnionej"

#define MSG_ZAKE_NEXT       "_>>>"


/*
#define MSG_ZAKE_PRZYCHOD_TITLE "Przych�d"
#define MSG_ZAKE_P7         "7. _Warto�� sprz. towar�w"
#define MSG_ZAKE_P8         "8. _Pozosta�e przychody"

#define MSG_ZAKE_P10        "10. Zakup towar�w _handlowych"
#define MSG_ZAKE_P11        "11. Koszty _uboczne zakupu"

#define MSG_ZAKE_P12        "12. Koszty _reprezentacji"
#define MSG_ZAKE_P13        "13. Wynagrodzenia w got�wce"
#define MSG_ZAKE_P14        "14. Pozosta�e wydatki"

#define MSG_ZAKE_P16        "_16."
#define MSG_ZAKE_P17        "17. Uwagi"
*/

#define MSG_ZAKE_OK         "_Ok"
#define MSG_ZAKE_CANCEL     "Ponie_chaj"


#define MSG_ZAK_DELETED_GAD  "\033cDane dotycz�ce zakupu\n\"\0338%s\0332\"\ns� aktualnie zaznaczone jako \033bSKASOWANE\033n,\ni nie mog� by� modyfikowane! Je�li chcesz dokona� w nich\njakichkolwiek zmian, musisz je najpierw odkasowa�.\nCzy chcesz to zrobi� teraz?"
#define MSG_ZAK_DELETED      "*_Tak, odkasuj|_Nie"

//|
/// INFORMACJE O ZAKUPACH

#define MSG_ZAK_INFO_TITLE  "Informacje o zakupach"
#define MSG_ZAKI_OK         "_Ok"


#define MSG_ZAKI_COUNT      "Liczba wpis�w"

#define MSG_ZAKI_P8          "8. Warto�� zakup�w brutto"
#define MSG_ZAKI_P18         "18. VAT podlegaj�cy odliczeniu"

#define MSG_ZAKI_P9          "9. Zakupy nie do odpisania"
#define MSG_ZAKI_P10        "10. Warto�� netto"
#define MSG_ZAKI_P11        ", 22% VAT"
#define MSG_ZAKI_P12        "12. Warto�� netto"
#define MSG_ZAKI_P13        ", 7% VAT"

#define MSG_ZAKI_P14        "14. Warto�� netto"
#define MSG_ZAKI_P15        ", 22% VAT"
#define MSG_ZAKI_P16        "16. Warto�� netto"
#define MSG_ZAKI_P17        ", 7% VAT"

#define MSG_ZAKI_INFO1      "\033c\0338Zakupy, za kt�re nie przys�uguje odliczenie"


//|
/// INDEX

#define MSG_IDX_TITLE      "Indeksowanie"
#define MSG_IDX_GTITLE     "Wpisz numer pocz�tkowy"

#define MSG_IDX_OK         "_Ok"
#define MSG_IDX_CANCEL     "Ponie_chaj"

//|




/// ROZLICZENIA

#define MSG_ROZL_ZAMKNIJ_REQ     "Zamknij okres rachunkowy"
#define MSG_ROZL_ZAMKNIJ_REQ_GAD "_Miesi�c|_Rok|*Ponie_chaj"

#define MSG_ROZL_ZAMKNIJ_MIESIAC "Na pewno chcesz zamkn�� miesi�c?"
#define MSG_ROZL_ZAMKNIJ_ROK     "Na pewno chcesz zamkn�� rok?"
#define MSG_ROZL_ZAMKNIJ_GAD     "_Tak|*_Nie"

//|
