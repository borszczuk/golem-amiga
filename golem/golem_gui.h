
#ifndef GOLEM_GUI_H
#define GOLEM_GUI_H

#ifdef GUI_PROTOS_H
extern Object *_Fake,
#else
Object *_Fake,
#endif

//       *ToolBar,                      /* TOOLBAR OBJECT */

       *StartupWindow,                /* STARTUP WINDOW     */
       *GA_Startup_Info,
       *CR_Startup_Scroll,
       *BT_Startup_Ok,


       *TOD_TipWindow,                /* Tip of the day window */


       *MainWindow,                   /* TOOLBAR = MAIN WINDOW */

       *GR_ToolBar_Menus,

       *BT_ToolBar_Sprzeda�,
           *BT_ToolBar_Vat,
           *BT_ToolBar_Vat_Kor,
//           *BT_ToolBar_Upr,
//           *BT_ToolBar_Upr_Kor,
           *BT_ToolBar_Vat_Par,
//           *BT_ToolBar_Upr_Par,
           *BT_ToolBar_Vat_ProForma,
           *BT_ToolBar_Paragon,
           *BT_ToolBar_Nieudokumentowana,
           *BT_ToolBar_Order,
           *BT_ToolBar_Sprzeda�_Back,
       *BT_ToolBar_Zakupy,
           *BT_ToolBar_Zakupy_Koszty,
           *BT_ToolBar_Zakupy_Back,
       *BT_ToolBar_Redagowanie,
           *BT_ToolBar_Red_Bie��ce,
           *BT_ToolBar_Red_Od�o�one,
           *BT_ToolBar_Red_Przelew,
           *BT_ToolBar_Red_Back,
       *BT_ToolBar_BazyDanych,
           *BT_ToolBar_Magazyn,
           *BT_ToolBar_Kontrahenci,
           *BT_ToolBar_UserBase,
           *BT_ToolBar_Settings_Pay,
           *BT_ToolBar_Settings_Unit,
           *BT_ToolBar_Bazy_Back,
       *BT_ToolBar_Rozliczenia,
           *BT_ToolBar_Rozliczenia_Zamknij,
//           *BT_ToolBar_Rozliczenia_ZamknijMiesiac,
//           *BT_ToolBar_Rozliczenia_ZamknijRok,
           *BT_ToolBar_Rozliczenia_Norm,
           *BT_ToolBar_Rozliczenia_Fisk_Dobowy,
           *BT_ToolBar_Rozliczenia_Fisk_Okresowy,
           *BT_ToolBar_Rozliczenia_Fisk_StanKasy,
           *BT_ToolBar_Rozliczenia_Fisk_RaportZmiany,
           *BT_ToolBar_Rozliczenia_Back,
       *GR_ToolBar_LoginLogout,
           *BT_ToolBar_Login,
           *BT_ToolBar_Logout,
       *BT_ToolBar_System,
           *BT_ToolBar_Settings,
           *BT_ToolBar_Numeracja,
           *BT_ToolBar_Drukarki,
           *BT_ToolBar_Tip,
           *BT_ToolBar_Inspektor,
           *BT_ToolBar_MUI,
           *BT_ToolBar_System_Back,
       *BT_ToolBar_About,
       *BT_ToolBar_Quit,



       *FiltrWindow,                  /* FILTR MAGAZYNOWY           */
       *ST_Filtr_Nazwa,

       *BT_Filtr_Ok,
       *BT_Filtr_Cancel,









       *ProgressWindow,                    /* PROGRESS BAR */
       *TX_Prog_Info,
       *GA_Prog_Info,




       *AccessWindow,                      /* EDYCJA UPRAWNIE� U�YTKOWNIKA */
       *LV_Acc_Strony,
       *GR_Acc_Strony,

       *BT_Acc_Ok,
       *BT_Acc_Cancel,





       *ZakupyWindow,              /* ZAKUPY WINDOW */
       *CY_Zak_Miesiac,
       *BT_Zak_Load,
       *BT_Zak_Save,
       *BT_Zak_Index,
       *BT_Zak_Sort,
       *LV_Zak_Zakupy,
       *BT_Zak_Add,
       *BT_Zak_Edit,
       *BT_Zak_Del,
       *BT_Zak_Info,
       *BT_Zak_Print,
       *BT_Zak_Ok,
       *BT_Zak_Cancel,


       *EdycjaZakupuWindow,         /* EDYCJA ZAKUPU WINDOW */
       *GR_ZakE_Pages,
       *ST_ZakE_LP,

       *ST_ZakE_P1,
       *ST_ZakE_P2,
       *ST_ZakE_P3,
       *ST_ZakE_P4,
       *BT_ZakE_Kontrahent,
       *TX_ZakE_P5,
       *TX_ZakE_P6,
       *TX_ZakE_P7,
       *BT_ZakE_Next1,
       *ST_ZakE_P9,
       *ST_ZakE_P10,
       *TX_ZakE_P11,
       *ST_ZakE_P12,
       *TX_ZakE_P13,
       *BT_ZakE_Next2,
       *ST_ZakE_P14,
       *TX_ZakE_P15,
       *ST_ZakE_P16,
       *TX_ZakE_P17,
       *BT_ZakE_Next3,

       *ST_ZakE_Opis,

//       *TX_ZakE_P8,
//       *TX_ZakE_P18,


       *BT_ZakE_Ok,
       *BT_ZakE_Cancel,


       *InformacjeOZakupachWindow,  /* INFORMACJE O ZAKUPACH */
       *TX_ZakI_Count,
       *TX_ZakI_P8,
       *TX_ZakI_P9,
       *TX_ZakI_P10,
       *TX_ZakI_P11,
       *TX_ZakI_P12,
       *TX_ZakI_P13,
       *TX_ZakI_P14,
       *TX_ZakI_P15,
       *TX_ZakI_P16,
       *TX_ZakI_P17,
       *TX_ZakI_P18,

       *BT_ZakI_Ok,


       *IndexRequesterWindow,       /* INDEX REQUESTER      */
       *ST_Idx_String,
       *BT_Idx_Ok,
       *BT_Idx_Cancel,

/*
       *SortRequesterWindow,        /* SORT REQUESTER       */
       *GR_Sort_Order,
       *RA_Sort_Order,
       *CH_Sort_Reverse,
       *BT_Sort_Ok,
       *BT_Sort_Cancel,
*/


       *FAKE;

#endif

