
/// Includes

#include "golem.h"

#define SAVEDS __saveds
#define ASM __asm
#define REG(x) register __ ## x

LONG __near __stack = 14000;

#ifdef DEMO
  char BaseScreenTitle[] = "WFMH Golem-KPiR " VERSIONREVISION " BETA-DEMO (" DATE ") � 1997-1998 Marcin Or�owski";
#else
  char BaseScreenTitle[] = "WFMH Golem-KPiR " VERSIONREVISION " (" DATE ") � 1997-1998 Marcin Or�owski";
#endif

char ScreenTitle[256];


//long ScreenTitleLen = sizeof(ScreenTitle);
char GuideName[256];

/// STRUCT: EasyStruct
struct   EasyStruct MyEasy = {            // Requesters used if no MUI
     sizeof (struct EasyStruct),
     0,
     TITLE,
     "%s",
     "Ok",
};
//|
/// STRUCT: Library *

struct  Library *MUIMasterBase   = NULL;
struct  Library *LocaleBase      = NULL;
struct  Locale  *MyLocale        = NULL;
//struct  Catalog *MyCatalog       = NULL;
//struct  Catalog *CurrencyCatalog = NULL;
//struct  Catalog *MediaCatalog    = NULL;

//struct  Library *TextFieldBase   = NULL;
//Class   *TextFieldClass          = NULL;
//|
/// DoSuperNew()

ULONG __stdargs DoSuperNew(struct IClass *cl, Object *obj, ULONG tag1, ...)
{
    return(DoSuperMethod(cl, obj, OM_NEW, &tag1, NULL));
}
//|

long __near __oslibversion = 38;           // Only 2.1 and up

#include "golem_structs.h"

//|

#ifdef BETA
char beta_watermark_1[] = BETA_WATERMARK;
#endif


enum{PAGE_MAIN=0,
     PAGE_SPRZEDAZ,
     PAGE_ZAKUPY,
     PAGE_BAZYDANYCH,
     PAGE_ROZLICZENIA,
     PAGE_SYSTEM,

    };

struct DateStamp    Today;

struct SystemInfo  sys =
{
    0                      // last_start
};


struct UserList  *logged_user = NULL;

struct List      jedn_miary;
struct List      p�atno�ci;
struct List      kontrahenci;
struct List      u�ytkownicy;
struct List      grupy;
struct GrupaList *grupa_aktywna = NULL;
char   IDGrup[256];

/// Global variables

char *EMPTY[] = {"\0338\033b", "\0"};       // atrybut produktu o ilo�ci 0
char *VatTable[]       = {"zw.", "0%", "7%", "17%", "22%", NULL};
char *VatTablePosnet[] = { "Z" , "D" , "C" , "B"  ,  "A" , NULL};
char VatValTable[]     = {  0,    0,    7,    17,    22};



Object *app = NULL;                          /* APPLICATION OBJECT */


//|

/// CleanUp
void CleanUp(void)
{

   DeleteClasses();

   if(MyLocale)        { CloseLocale(MyLocale); MyLocale = NULL; }
//   if(MyCatalog)       { CloseCatalog(MyCatalog); MyCatalog = NULL; }
//   if(CurrencyCatalog) { CloseCatalog(CurrencyCatalog); CurrencyCatalog = NULL; }
//   if(MediaCatalog)    { CloseCatalog(MediaCatalog); MediaCatalog = NULL; }
   if(LocaleBase)      { CloseLibrary(LocaleBase); LocaleBase = NULL; }
   if(MUIMasterBase)    {CloseLibrary(MUIMasterBase); MUIMasterBase = NULL; }

//   if(TextFieldBase)    {CloseLibrary(TextFieldBase); TextFieldClass = NULL; TextFieldBase = NULL; }
}
//|

/// Price2String
char *Price2String(double val)
{
static char out[16];

    sprintf(out, "%#.2f", val);
    *strchr(out, '.') = settings.def_dot;

    return(out);
}
//|
/// String2Price
double String2Price(char *string)
{
char *dot;
static char str[30];

    stccpy(str, string, 30);
    if(dot = strchr(str, settings.def_dot))
       {
       *dot = '.';
       stccpy(str, str, dot - str + 4);  // because this function includes NULL in the lenght
       }

    return(atof(str));
}
//|

/// Mar�a2String
char *Mar�a2String(double val)
{
static char out[20];

    sprintf(out, "%#.2f", val);
    *strchr(out, '.') = settings.def_dot;

    return(out);
}
//|
/// String2Mar�a
double String2Mar�a(char *string)
{
char *dot;
static char str[30];

    stccpy(str, string, 30);
    if(dot = strchr(str, settings.def_dot))
       {
       *dot = '.';
       stccpy(str, str, dot - str + 4);  // because this function includes NULL in the length
       }

    return(atof(str));
}
//|


/// PAY LIST CLASS

struct MUI_CustomClass *CL_PayList = NULL;

/// Data
struct PayList_Data
{
   ULONG Dummy;
};
//|

/// Pay list hooks
/// HOOK: PayList_Display
void * __saveds __asm PayList_Display(register __a2 char **array, register __a1 struct P�atno��List *node)
{
static char Del[6],
            Delay[15];

   if(node)
     {
     struct P�atno�� *p�at = &node->pl_p�atno��;

     sprintf(Del   , "%s" , ((node->Deleted==FALSE) ? " " : "*"));

     *array++ = Del;
     *array++ = p�at->Nazwa;

     if(p�at->Got�wka)
       strcpy(Delay, "---");
     else
       sprintf(Delay, "%ld", p�at->Zw�oka);
     *array++ = Delay;
     }
   else
     {
     *array++ = "";
     *array++ = MSG_PAY_NAME;
     *array++ = MSG_PAY_DELAY;
     }


     return(0);
}
//|
/// HOOK: PayList_CompareStr
LONG __saveds __asm PayList_CompareStr(register __a1 struct P�atno��List *Node1, register __a2 struct P�atno��List *Node2)
{
    return((LONG)StrnCmp(MyLocale, Node1->pl_p�atno��.Nazwa, Node2->pl_p�atno��.Nazwa, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG PayList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook PayList_DisplayHook     = { {NULL, NULL}, (VOID *)PayList_Display   ,NULL,NULL };
static const struct Hook PayList_CompareHookStr  = { {NULL, NULL}, (VOID *)PayList_CompareStr,NULL,NULL };

    obj = (Object *)DoSuperNew(cl,obj,
                    InputListFrame,
                    _MUIA_List_AutoVisible  , TRUE,
                    _MUIA_List_DisplayHook  , &PayList_DisplayHook,
                    _MUIA_List_CompareHook  , &PayList_CompareHookStr,
                    _MUIA_List_Format       , "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=80 BAR,"
                                              "MIW=1 MAW=-1 P=\033r ",

                    _MUIA_List_Title        , TRUE,

#ifdef USE_NLIST
                     MUIA_NList_TitleSeparator, TRUE,
                     MUIA_NList_AutoCopyToClip, TRUE,
#endif

                TAG_MORE, msg->ops_AttrList);

/*
      if(obj)
          {
          struct PayList_Data *data = INST_DATA(cl,obj);

          }
*/

      return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG PayList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

    switch (msg->MethodID)
       {
       case OM_NEW                   : return(PayList_New       (cl, obj, (APTR)msg));
       }

    return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
/// UNIT LIST CLASS

struct MUI_CustomClass *CL_UnitList = NULL;

/// Data
struct UnitList_Data
{
   ULONG Dummy;
};
//|

/// Unit list hooks
/// HOOK: UnitList_Display
void * __saveds __asm UnitList_Display(register __a2 char **array, register __a1 struct JednostkaList *node)
{
static char Del[6];

/*,
            Name[CUST_NAME_LEN + 10],
            Ulica[CUST_ADRES_LEN + 10],
            Kod[CUST_KOD_LEN + 10],
            Miasto[CUST_MIASTO_LEN + 10];
*/

   if(node)
     {
     struct Jednostka *unit = &node->ul_jedn;

     sprintf(Del   , "%s" , ((node->Deleted==FALSE) ? " " : "*"));

     *array++ = Del;
     *array++ = unit->Nazwa;
     }
   else
     {
     *array++ = "";
     *array++ = MSG_UNIT_NAME;
     }


     return(0);
}
//|
/// HOOK: UnitList_CompareStr
LONG __saveds __asm UnitList_CompareStr(register __a1 struct JednostkaList *Node1, register __a2 struct JednostkaList *Node2)
{
    return((LONG)StrnCmp(MyLocale, Node1->ul_jedn.Nazwa, Node2->ul_jedn.Nazwa, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG UnitList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook UnitList_DisplayHook     = { {NULL, NULL}, (VOID *)UnitList_Display   ,NULL,NULL };
static const struct Hook UnitList_CompareHookStr  = { {NULL, NULL}, (VOID *)UnitList_CompareStr,NULL,NULL };

    obj = (Object *)DoSuperNew(cl,obj,
                    InputListFrame,
                    _MUIA_List_AutoVisible  , TRUE,
                    _MUIA_List_DisplayHook  , &UnitList_DisplayHook,
                    _MUIA_List_CompareHook  , &UnitList_CompareHookStr,
                    _MUIA_List_Format       , "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=98 BAR",

                    _MUIA_List_Title        , TRUE,

#ifdef USE_NLIST
                     MUIA_NList_TitleSeparator, TRUE,
                     MUIA_NList_AutoCopyToClip, TRUE,
#endif

                TAG_MORE, msg->ops_AttrList);

/*
      if(obj)
          {
          struct UnitList_Data *data = INST_DATA(cl,obj);

          }
*/

      return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG UnitList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

    switch (msg->MethodID)
       {
       case OM_NEW                   : return(UnitList_New       (cl, obj, (APTR)msg));
       }

    return(DoSuperMethodA(cl,obj,msg));
}

//|

//|

/// GROUP LIST CLASS

struct MUI_CustomClass *CL_GroupList = NULL;

/// Data
struct GroupList_Data
{
   ULONG Dummy;
};
//|

/// Group list hooks
/// HOOK: GroupList_Display
void * __saveds __asm GroupList_Display(register __a2 char **array, register __a1 struct GrupaList *node)
{
static char Del[6],
            Name[GROUP_NAME_LEN+10],
            Ilo��[10+5];


   if(node)
     {
     char *_Empty = EMPTY[(node->Ilo�� > 0)];

     sprintf(Del  , "%s%s" , _Empty, ((node->Deleted==FALSE) ? " " : "*"));
     sprintf(Name , "%s%s" , _Empty, node->gm_grupa.Nazwa);
     sprintf(Ilo��, "%s%ld", _Empty, node->Ilo��);

     *array++ = Del;
     *array++ = Name;
     *array++ = Ilo��;
     }
   else
     {
     *array++ = MSG_PROD_LIST_DEL;
     *array++ = MSG_PROD_LIST_NAME;
     *array++ = MSG_PROD_LIST_AMOUNT;
     }


     return(0);
}
//|
/// HOOK: GroupList_CompareStr
LONG __saveds __asm GroupList_CompareStr(register __a1 struct GrupaList *Node1, register __a2 struct GrupaList *Node2)
{
    return((LONG)StrnCmp(MyLocale, Node1->gm_grupa.Nazwa, Node2->gm_grupa.Nazwa, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG GroupList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook GroupList_DisplayHook     = { {NULL, NULL}, (VOID *)GroupList_Display   ,NULL,NULL };
static const struct Hook GroupList_CompareHookStr  = { {NULL, NULL}, (VOID *)GroupList_CompareStr,NULL,NULL };

    obj = (Object *)DoSuperNew(cl,obj,
                    InputListFrame,
                    _MUIA_List_AutoVisible  , TRUE,
                    _MUIA_List_DisplayHook  , &GroupList_DisplayHook,
                    _MUIA_List_CompareHook  , &GroupList_CompareHookStr,
                    _MUIA_List_Format       , "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=89 BAR,"
                                              "MIW=1 MAW=10 P=\033r",
                    _MUIA_List_Title        , TRUE,
#ifdef USE_NLIST
                     MUIA_NList_TitleSeparator, TRUE,
                     MUIA_NList_AutoCopyToClip, TRUE,
#endif

                TAG_MORE, msg->ops_AttrList);

/*
      if(obj)
          {
          struct GroupList_Data *data = INST_DATA(cl,obj);

          }
*/

      return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG GroupList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

    switch (msg->MethodID)
       {
       case OM_NEW                   : return(GroupList_New       (cl, obj, (APTR)msg));
       }

    return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
/// FAKTURA PRODUCT LIST CLASS

struct MUI_CustomClass *CL_FakturaProductList = NULL;

/// Data
struct FakturaProductList_Data
{
   ULONG Dummy;
};
//|

/// Product list hooks
/// HOOK: FakturaProductList_Display
void * __saveds __asm FakturaProductList_Display(register __a2 char **array, register __a1 struct FakProductList *node)
{
static char Name[PROD_NAME_LEN+10],
            Ilo��[15+5],
            Netto[15+5],
            Vat[15+5],
            Warto��[15+5];

static double brutto;


   if(node)
     {
     double _ilo�� = node->pl_prod.Ilo��;
     double _vat;

     sprintf(Name   , "%s" , node->pl_prod.Nazwa);
     sprintf(Ilo��  , "%#.2f", _ilo��);

     brutto = node->pl_prod.Zakup + node->pl_prod.Mar�a;
     brutto = brutto - CalcRabat(brutto, node->pl_prod.Rabat);

     sprintf(Netto, "%#.2f", brutto);

     brutto = _ilo�� * brutto;
     _vat = CalcVat(brutto, node->pl_prod.Vat);
     sprintf(Vat, "%#.2f", _vat);

     brutto = brutto + _vat;
     strcpy(Warto��, Price2String(brutto));

     *array++ = Name;
     *array++ = Ilo��;
     *array++ = Netto;
     *array++ = Vat;
     *array++ = Warto��;
     }
   else
     {
     *array++ = MSG_FAK_PROD_LIST_NAME;
     *array++ = MSG_FAK_PROD_LIST_AMOUNT;
     *array++ = MSG_FAK_PROD_LIST_NETTO;
     *array++ = MSG_FAK_PROD_LIST_VAT;
     *array++ = MSG_FAK_PROD_LIST_SUM;
     }


     return(0);
}
//|
/// HOOK: FakturaProductList_CompareStr
LONG __saveds __asm FakturaProductList_CompareStr(register __a1 struct FakProductList *Node1, register __a2 struct FakProductList *Node2)
{
    return((LONG)StrnCmp(MyLocale, Node1->pl_prod.Nazwa, Node2->pl_prod.Nazwa, -1, SC_COLLATE2));
}
//|
/*
/// HOOK: FakturaProductList_Construct
APTR __saveds __asm FakturaProductList_Construct(register __a2 APTR pool, register __a1 struct FakProductList *node)
{
struct  FakProductList *new;

        if(new = calloc(1, sizeof(struct FakProductList)))
           {
           memcpy(new, node, sizeof(struct FakProductList));
           }

        return(new);
}
//|
*/
/// HOOK: FakturaProductList_Destruct
VOID __saveds __asm FakturaProductList_Destruct(register __a2 APTR pool, register __a1 struct FakProductList *node)
{
        free(node);
}
//|
//|

/// OM_NEW

ULONG FakturaProductList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
//static const struct Hook FakturaProductList_ConstructHook   = { {NULL, NULL}, (VOID *)FakturaProductList_Construct ,NULL,NULL };
static const struct Hook FakturaProductList_DestructHook    = { {NULL, NULL}, (VOID *)FakturaProductList_Destruct  ,NULL,NULL };
static const struct Hook FakturaProductList_DisplayHook     = { {NULL, NULL}, (VOID *)FakturaProductList_Display   ,NULL,NULL };
static const struct Hook FakturaProductList_CompareHookStr  = { {NULL, NULL}, (VOID *)FakturaProductList_CompareStr,NULL,NULL };

    obj = (Object *)DoSuperNew(cl,obj,
                    InputListFrame,
                    _MUIA_List_AutoVisible  , TRUE,
//                    _MUIA_List_ConstructHook, &FakturaProductList_ConstructHook,
                    _MUIA_List_DestructHook , &FakturaProductList_DestructHook,
                    _MUIA_List_DisplayHook  , &FakturaProductList_DisplayHook,
                    _MUIA_List_CompareHook  , &FakturaProductList_CompareHookStr,
                    _MUIA_List_Format       , "MIW=1 MAW=60 BAR,"
                                              "MIW=1 MAW=-1 P=\033r BAR,"
                                              "MIW=1 MAW=-1 P=\033r BAR,"
                                              "MIW=1 MAW=-1 P=\033r BAR,"
                                              "MIW=1 MAW=-1 P=\033r",
                    _MUIA_List_Title        , TRUE,
#ifdef USE_NLIST
                     MUIA_NList_TitleSeparator, TRUE,
                     MUIA_NList_AutoCopyToClip, TRUE,
#endif

                TAG_MORE, msg->ops_AttrList);
/*
      if(obj)
          {
          struct FakturaProductList_Data *data = INST_DATA(cl,obj);

          }
*/

      return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG FakturaProductList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

    switch (msg->MethodID)
       {
       case OM_NEW                   : return(FakturaProductList_New       (cl, obj, (APTR)msg));
       }

    return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
/// PRODUCT LIST CLASS

struct MUI_CustomClass *CL_ProductList = NULL;

/// Data
struct ProductList_Data
{
   ULONG Dummy;
};
//|

/// Product list hooks
/// HOOK: ProductList_Display
void * __saveds __asm ProductList_Display(register __a2 char **array, register __a1 struct ProductList *node)
{
static char Del[6],
            Name[PROD_NAME_LEN+10],
            SWW[PROD_SWW_LEN+10],
            Sprzeda�[15+5],
            Vat[10+5],
            Jedn[PROD_JEDN_LEN+5],
            Ilo��[15+5];


   if(node)
     {
     char *_Empty = EMPTY[(node->pl_prod.Ilo�� > 0)];

     sprintf(Del  , "%s%s"   , _Empty, ((node->Deleted==FALSE) ? " " : "*"));
     sprintf(Name , "%s%s"   , _Empty, node->pl_prod.Nazwa);
     sprintf(SWW  , "%s%s"   , _Empty, node->pl_prod.SWW);
     sprintf(Sprzeda�, "%s%s", _Empty, Price2String(node->pl_prod.Mar�a + node->pl_prod.Zakup));
     sprintf(Vat  , "%s%s"   , _Empty, VatTable[node->pl_prod.Vat]);
     sprintf(Jedn , "%s%s"   , _Empty, node->pl_prod.JednMiary);
     sprintf(Ilo��, "%s%s"   , _Empty, Price2String(node->pl_prod.Ilo��));

     *array++ = Del;
     *array++ = Name;
     *array++ = SWW;
     *array++ = Sprzeda�;
     *array++ = Vat;
     *array++ = Jedn;
     *array++ = Ilo��;
     }
   else
     {
     *array++ = MSG_PROD_LIST_DEL;
     *array++ = MSG_PROD_LIST_NAME;
     *array++ = MSG_PROD_LIST_SWW;
     *array++ = MSG_PROD_LIST_NETTO;
     *array++ = MSG_PROD_LIST_VAT;
     *array++ = MSG_PROD_LIST_JEDN;
     *array++ = MSG_PROD_LIST_AMOUNT;
     }


     return(0);
}
//|
/// HOOK: ProductList_CompareStr
LONG __saveds __asm ProductList_CompareStr(register __a1 struct ProductList *Node1, register __a2 struct ProductList *Node2)
{
    return((LONG)StrnCmp(MyLocale, Node1->pl_prod.Nazwa, Node2->pl_prod.Nazwa, -1, SC_COLLATE2));
}
//|

/*
/// HOOK: ProductList_Construct
APTR __saveds __asm ProductList_Construct(register __a2 APTR pool, register __a1 struct ProductList *node)
{
struct  ProductList *new;

        if(new = calloc(1, sizeof(struct ProductList)))
                 memcpy(new, node, sizeof(struct ProductList));

        return(new);
}
//|
/// HOOK: ProductList_Destruct
VOID __saveds __asm ProductList_Destruct(register __a2 APTR pool, register __a1 struct ProductList *node)
{
        free(node);
}
//|
*/

//|

/// OM_NEW

ULONG ProductList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
//static const struct Hook ProductList_ConstructHook   = { {NULL, NULL}, (VOID *)ProductList_Construct ,NULL,NULL };
//static const struct Hook ProductList_DestructHook    = { {NULL, NULL}, (VOID *)ProductList_Destruct  ,NULL,NULL };
static const struct Hook ProductList_DisplayHook     = { {NULL, NULL}, (VOID *)ProductList_Display   ,NULL,NULL };
static const struct Hook ProductList_CompareHookStr  = { {NULL, NULL}, (VOID *)ProductList_CompareStr,NULL,NULL };

    obj = (Object *)DoSuperNew(cl,obj,
                    InputListFrame,
                    _MUIA_List_AutoVisible  , TRUE,
//                    _MUIA_List_ConstructHook, &ProductList_ConstructHook,
//                    _MUIA_List_DestructHook , &ProductList_DestructHook,
                    _MUIA_List_DisplayHook  , &ProductList_DisplayHook,
                    _MUIA_List_CompareHook  , &ProductList_CompareHookStr,
                    _MUIA_List_Format       , "MIW=1 MAW=-1  BAR,"
                                              "MIW=1 MAW=49 BAR,"
                                              "MIW=1 MAW=10 BAR,"
                                              "MIW=1 MAW=-1 P=\033r BAR,"
                                              "MIW=1 MAW=-1 P=\033r BAR,"
                                              "MIW=1 MAW=10 P=\033r BAR,"
                                              "MIW=1 MAW=-1 P=\033r",
                    _MUIA_List_Title        , TRUE,
#ifdef USE_NLIST
                     MUIA_NList_TitleSeparator, TRUE,
                     MUIA_NList_AutoCopyToClip, TRUE,
#endif

                TAG_MORE, msg->ops_AttrList);
/*
      if(obj)
          {
          struct ProductList_Data *data = INST_DATA(cl,obj);

          }
*/

      return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG ProductList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

    switch (msg->MethodID)
       {
       case OM_NEW                   : return(ProductList_New       (cl, obj, (APTR)msg));
       }

    return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
/// USERS LIST CLASS

struct MUI_CustomClass *CL_UsersList = NULL;

/// Data
struct UsersList_Data
{
   ULONG Dummy;
};
//|

/// Users list hooks
/// HOOK: UsersList_Display
void * __saveds __asm UsersList_Display(register __a2 char **array, register __a1 struct UserList *node)
{
static char Del[2];

   if(node)
     {
     sprintf(Del, "%s", ((node->Deleted==FALSE) ? " " : "*"));
     *array++ = Del;
     *array++ = node->user.Nazwa;
     }
   else
     {
     *array++ = MSG_USER_LIST_DEL;
     *array++ = MSG_USER_LIST_NAME;
     }


     return(0);
}
//|
/// HOOK: UsersList_CompareStr
LONG __saveds __asm UsersList_CompareStr(register __a1 struct UserList *Node1, register __a2 struct UserList *Node2)
{
    return((LONG)StrnCmp(MyLocale, Node1->user.Nazwa, Node2->user.Nazwa, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG UsersList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook UsersList_DisplayHook     = { {NULL, NULL}, (VOID *)UsersList_Display   ,NULL,NULL };
static const struct Hook UsersList_CompareHookStr  = { {NULL, NULL}, (VOID *)UsersList_CompareStr,NULL,NULL };

    obj = (Object *)DoSuperNew(cl,obj,
                    InputListFrame,
                    _MUIA_List_AutoVisible  , TRUE,
                    _MUIA_List_DisplayHook  , &UsersList_DisplayHook,
                    _MUIA_List_CompareHook  , &UsersList_CompareHookStr,
                    _MUIA_List_Format       , "MIW=1 BAR, MAW=-1",
                    _MUIA_List_Title        , TRUE,
#ifdef USE_NLIST
                     MUIA_NList_TitleSeparator, TRUE,
                     MUIA_NList_AutoCopyToClip, TRUE,
#endif

                TAG_MORE, msg->ops_AttrList);

/*
      if(obj)
          {
          struct UsersList_Data *data = INST_DATA(cl,obj);

          DoMethod(obj, MUIM_UsersList_Init, NULL);
          }
*/
      return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG UsersList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

    switch (msg->MethodID)
       {
       case OM_NEW                   : return(UsersList_New       (cl, obj, (APTR)msg));

/*
       case MUIM_UsersList_Init  : return(UsersList_Init      (cl, obj, (APTR)msg));

       case MUIM_UsersList_Load   : return(UsersList_Load      (cl, obj, (APTR)msg));
       case MUIM_UsersList_Save   : return(UsersList_Save      (cl, obj, (APTR)msg));
*/
       }

    return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
/// DOCUMENT LIST CLASS

struct MUI_CustomClass *CL_DocumentList = NULL;

/*
** ta klasa u�ywa Construct/Destruct Hooks!!!
**
** Odbiorca, numer, data
**
*/

/// Data
struct DocumentList_Data
{
   ULONG SortOrder;
};
//|

/// Document list hooks

/// HOOK: DocumentList_Display
void * __saveds __asm DocumentList_Display(register __a2 char **array, register __a1 struct DocumentScan *node)
{
static char Data[2+2+4+2+1];
static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };


   if(node)
     {
     *array++ = RodzajeDokument�wLVTable[node->RodzajDokumentu];
     *array++ = node->Nazwa;
     *array++ = node->NIP;
     *array++ = node->Regon;
     *array++ = node->Numer;

     FormatDateHook.h_Data = Data;
     FormatDate(MyLocale, "%d.%m.%Y", &node->data_wystawienia, &FormatDateHook);

     *array++ = Data;

     }
   else
     {
     *array++ = MSG_DOC_LIST_TYP;
     *array++ = MSG_DOC_LIST_ODB;
     *array++ = MSG_DOC_LIST_NIP;
     *array++ = MSG_DOC_LIST_REGON;
     *array++ = MSG_DOC_LIST_NUMER;
     *array++ = MSG_DOC_LIST_DATA;
     }


     return(0);
}
//|
/// HOOK: DocumentList_Construct
APTR __saveds __asm DocumentList_Construct(register __a2 APTR pool, register __a1 struct DocumentScan *node)
{
struct  DocumentScan *new;

        if(new = malloc(sizeof(struct DocumentScan)))
           {
           memcpy(new, node, sizeof(struct DocumentScan));
           }

        return(new);
}
//|
/// HOOK: DocumentList_Destruct
VOID __saveds __asm DocumentList_Destruct(register __a2 APTR pool, register __a1 struct DocumentScan *node)
{
        free(node);
}
//|

/// HOOK: DocumentList_CompareNazwa
LONG __saveds __asm DocumentList_CompareNazwa(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

    return((LONG)StrnCmp(MyLocale, Node1->Nazwa, Node2->Nazwa, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareNIP
LONG __saveds __asm DocumentList_CompareNIP(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

    return((LONG)StrnCmp(MyLocale, Node1->NIP, Node2->NIP, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareRegon
LONG __saveds __asm DocumentList_CompareRegon(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

    return((LONG)StrnCmp(MyLocale, Node1->Regon, Node2->Regon, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareNumer
LONG __saveds __asm DocumentList_CompareNumer(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

    return((LONG)StrnCmp(MyLocale, Node1->Numer, Node2->Numer, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareData
LONG __saveds __asm DocumentList_CompareData(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

    return((LONG)CompareDates(&Node2->data_wystawienia, &Node1->data_wystawienia));

}
//|
/// HOOK: DocumentList_CompareNazwaR
LONG __saveds __asm DocumentList_CompareNazwaR(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

    return((LONG)StrnCmp(MyLocale, Node2->Nazwa, Node1->Nazwa, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareNIPR
LONG __saveds __asm DocumentList_CompareNIPR(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

    return((LONG)StrnCmp(MyLocale, Node2->NIP, Node1->NIP, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareRegonR
LONG __saveds __asm DocumentList_CompareRegonR(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

    return((LONG)StrnCmp(MyLocale, Node2->Regon, Node1->Regon, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareNumerR
LONG __saveds __asm DocumentList_CompareNumerR(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

    return((LONG)StrnCmp(MyLocale, Node2->Numer, Node1->Numer, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareDataR
LONG __saveds __asm DocumentList_CompareDataR(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

    return((LONG)CompareDates(&Node1->data_wystawienia, &Node2->data_wystawienia));

}
//|


//|

static const struct Hook DocumentList_CompareHookNazwa  = { {NULL, NULL}, (VOID *)DocumentList_CompareNazwa  ,NULL,NULL };
static const struct Hook DocumentList_CompareHookNIP    = { {NULL, NULL}, (VOID *)DocumentList_CompareNIP    ,NULL,NULL };
static const struct Hook DocumentList_CompareHookRegon  = { {NULL, NULL}, (VOID *)DocumentList_CompareRegon  ,NULL,NULL };
static const struct Hook DocumentList_CompareHookNumer  = { {NULL, NULL}, (VOID *)DocumentList_CompareNumer  ,NULL,NULL };
static const struct Hook DocumentList_CompareHookData   = { {NULL, NULL}, (VOID *)DocumentList_CompareData   ,NULL,NULL };

static const struct Hook DocumentList_CompareHookNazwaR = { {NULL, NULL}, (VOID *)DocumentList_CompareNazwaR ,NULL,NULL };
static const struct Hook DocumentList_CompareHookNIPR   = { {NULL, NULL}, (VOID *)DocumentList_CompareNIPR  ,NULL,NULL };
static const struct Hook DocumentList_CompareHookRegonR = { {NULL, NULL}, (VOID *)DocumentList_CompareRegonR ,NULL,NULL };
static const struct Hook DocumentList_CompareHookNumerR = { {NULL, NULL}, (VOID *)DocumentList_CompareNumerR ,NULL,NULL };
static const struct Hook DocumentList_CompareHookDataR  = { {NULL, NULL}, (VOID *)DocumentList_CompareDataR  ,NULL,NULL };

/// OM_NEW

static const struct Hook DocumentList_DisplayHook     = { {NULL, NULL}, (VOID *)DocumentList_Display    ,NULL,NULL };
static const struct Hook DocumentList_ConstructHook   = { {NULL, NULL}, (VOID *)DocumentList_Construct  ,NULL,NULL };
static const struct Hook DocumentList_DestructHook    = { {NULL, NULL}, (VOID *)DocumentList_Destruct   ,NULL,NULL };

ULONG DocumentList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{

    obj = (Object *)DoSuperNew(cl,obj,
                    InputListFrame,
                    _MUIA_List_AutoVisible  , TRUE,
                    _MUIA_List_DisplayHook  , &DocumentList_DisplayHook,
//                    _MUIA_List_CompareHook  , &DocumentList_CompareHookNumer,
                    _MUIA_List_ConstructHook, &DocumentList_ConstructHook,
                    _MUIA_List_DestructHook , &DocumentList_DestructHook,

                    _MUIA_List_Format       , "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=40 BAR,"
                                              "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1     ",
                    _MUIA_List_Title        , TRUE,

#ifdef USE_NLIST
                     MUIA_NList_TitleSeparator, TRUE,
                     MUIA_NList_AutoCopyToClip, TRUE,
#endif

                TAG_MORE, msg->ops_AttrList);


      if(obj)
          {
          /*** trick to set arguments ***/


          set(obj, MUIA_DocumentList_Order, 4);

          msg->MethodID = OM_SET;
          DoMethodA(obj, (Msg)msg);
          msg->MethodID = OM_NEW;
          }


      return((ULONG)obj);
}

//|
/// OM_SET

ULONG ASM DocumentList_Set(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct DocumentList_Data *data = INST_DATA(cl, obj);
struct TagItem *tags,*tag;

    for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
       {
       switch(tag->ti_Tag)
          {
          case MUIA_DocumentList_Order:
               {
               data->SortOrder = tag->ti_Data;

               if((tag->ti_Data & 0x80000000) == 0)
                   {
                   switch(tag->ti_Data)
                      {
                      case 0: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNazwa); break;
                      case 1: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNIP); break;
                      case 2: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookRegon); break;
                      case 3: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNumer); break;
                      case 4: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookData); break;
                      }
                   }
                else
                   {
                   switch(tag->ti_Data & 0x7fffffff)
                      {
                      case 0: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNazwaR); break;
                      case 1: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNIPR); break;
                      case 2: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookRegonR); break;
                      case 3: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNumerR); break;
                      case 4: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookDataR); break;
                      }
                   }

               }
               break;
          }
        }

    return(DoSuperMethodA(cl, obj, msg));
}

//|
/// OM_GET
static ULONG ASM DocumentList_Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct DocumentList_Data *data = INST_DATA(cl, obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

        switch(((struct opGet *)msg)->opg_AttrID)
                {
                case MUIA_DocumentList_Order:
                     *store = data->SortOrder;
                     return(TRUE);
                     break;
                }

        return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher

__saveds __asm ULONG DocumentList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

    switch (msg->MethodID)
       {
       case OM_NEW                   : return(DocumentList_New       (cl, obj, (APTR)msg));
       case OM_SET                   : return(DocumentList_Set       (cl, obj, (APTR)msg));
       case OM_GET                   : return(DocumentList_Get       (cl, obj, (APTR)msg));
       }

    return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
/// SMART CYCLE CLASS

struct MUI_CustomClass *CL_SmartCycle = NULL;

/*
**
*/

/// Data
struct SmartCycle_Data
{
   APTR   Entries[30];
   Object *Cycle;
   long   Key;
};
//|

/// OM_NEW

ULONG SmartCycle_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
Object *Cycle;

    obj = (Object *)DoSuperNew(cl,obj,

                MUIA_Group_Horiz, TRUE,
                MUIA_CycleChain, FALSE,

                Child, Cycle = HVSpace,

                TAG_MORE, msg->ops_AttrList);

      if(obj)
        {
        struct SmartCycle_Data *data = INST_DATA(cl, obj);

        data->Cycle = Cycle;
        data->Entries[0] = 0L;
        data->Key = 0;


        /*** trick to set arguments ***/

        msg->MethodID = OM_SET;
        DoMethodA(obj, (Msg)msg);
        msg->MethodID = OM_NEW;
        }


      return((ULONG)obj);
}

//|
/// OM_SET

ULONG ASM SmartCycle_Set(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct SmartCycle_Data *data = INST_DATA(cl, obj);
struct TagItem *tags,*tag;

    for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
       {
       switch(tag->ti_Tag)
          {
          case MUIA_ControlChar:
               set(data->Cycle, MUIA_ControlChar, tag->ti_Data);
               data->Key = tag->ti_Data;
               break;

          case MUIA_Cycle_Entries:
               {
               char ** str = (char **)tag->ti_Data;
               int  i=0;

               for(;; i++)
                  {
                  data->Entries[i] = str[i];
                  if(data->Entries[i] == '\0')
                      break;
                  }

               DoMethod(obj, OM_REMMEMBER, data->Cycle);
               DisposeObject(data->Cycle);
               data->Cycle = CycleObject, MUIA_Cycle_Entries, data->Entries,
                                          MUIA_CycleChain, TRUE,
                                          End;
               DoMethod(obj, OM_ADDMEMBER, data->Cycle);
               DoMethod(data->Cycle, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_TYP_P�ATNO�CI);

               if(data->Key != 0)
                   set(data->Cycle, MUIA_ControlChar, data->Key);

               break;
               }
          }
        }

    return(DoSuperMethodA(cl, obj, msg));
}

//|
/// OM_GET
static ULONG ASM SmartCycle_Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct SmartCycle_Data *data = INST_DATA(cl, obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

        switch(((struct opGet *)msg)->opg_AttrID)
                {
                case MUIA_Cycle_Active:
                     *store = (ULONG)xget(data->Cycle, MUIA_Cycle_Active);
                     return(TRUE);
                     break;
                }

        return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher

__saveds __asm ULONG SmartCycle_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

    switch (msg->MethodID)
       {
       case OM_NEW        : return(SmartCycle_New       (cl, obj, (APTR)msg));
       case OM_SET        : return(SmartCycle_Set       (cl, obj, (APTR)msg));
       case OM_GET        : return(SmartCycle_Get       (cl, obj, (APTR)msg));

/*
       case MUIM_Notify:
                {
                struct SmartCycle_Data *data = INST_DATA(cl,obj);

                        return(DoMethodA(data->Cycle, msg));
                }
*/

       }

    return(DoSuperMethodA(cl,obj,msg));
}

//|

//|

/*
/// ZAKUPY LIST CLASS

struct MUI_CustomClass *CL_ZakupyList = NULL;

/*
struct Zakupy
{
    long   lp;                                         // 1
    struct DateStamp DataZdarzenia;                    // 2
    char   NumerDowoduKsi�gowego[STR_ZAK_DOW�D_LEN];   // 3
    struct Klient Kontrahent;                          // 4
    char   OpisZdarzenia[STR_ZAK_OPIS_LEN];            // 5
// Przych�d
    double Warto��SprzedanychTowar�w;                  // 6
    double Pozosta�ePrzychody;                         // 7

    double ZakupTowar�wHandlowych;                     // 10
    double KosztyUboczneZakupu;                        // 11

// Wydatki
    double KosztyReprezentacjiIReklamy;                // 12
    double Wynagrodzenia;                              // 13
    double Pozosta�eWydatki;                           // 14

    double Pole16;                                     // 16
    char   Uwagi[STR_ZAK_UWAGI_LEN];                   // 17
*/

/// Data
struct ZakupyList_Data
{
   ULONG Dummy;
};
//|

/// Zakupy list hooks
/// HOOK: ZakupyList_Display
void * __saveds __asm ZakupyList_Display(register __a2 char **array, register __a1 struct ZakupList *node)
{
static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };
static char Del[6],
            LP[5],
            Data[2+1+2+1],  // dd.mm\0
            In[15],
            Out[15];

/*,
            Name[CUST_NAME_LEN + 10],
            Ulica[CUST_ADRES_LEN + 10],
            Kod[CUST_KOD_LEN + 10],
            Miasto[CUST_MIASTO_LEN + 10];
*/


   if(node)
     {
     struct Zakup *zak = &node->za_zakup;

     sprintf(Del   , "%s" , ((node->Deleted==FALSE) ? " " : "*"));
     *array++ = Del;

     sprintf(LP    , "%ld", zak->LP);
     *array++ = LP;

     FormatDateHook.h_Data = Data;
     FormatDate(MyLocale, "%d.%m", &zak->DataZdarzenia, &FormatDateHook);
     *array++ = Data;

     *array++ = zak->Kontrahent.Nazwa1;
     *array++ = zak->OpisZdarzenia;

     {
     double suma = zak->Warto��SprzedanychTowar�w + zak->Pozosta�ePrzychody;

     if(suma == 0)
         In[0] = 0;
     else
         strcpy(In, Price2String(suma));
     }
     *array++ = In;

     {
     double suma = zak->KosztyReprezentacjiIReklamy + zak->Wynagrodzenia + zak->Pozosta�eWydatki;

     if(suma == 0)
       Out[0] = 0;
     else
       strcpy(Out, Price2String(suma));
     }
     *array++ = Out;

     }
   else
     {
     *array++ = MSG_ZAK_LIST_DEL;
     *array++ = MSG_ZAK_LIST_LP;
     *array++ = MSG_ZAK_LIST_DATA;
     *array++ = MSG_ZAK_LIST_NAZWA;
     *array++ = MSG_ZAK_LIST_OPIS;
     *array++ = MSG_ZAK_LIST_IN;
     *array++ = MSG_ZAK_LIST_OUT;
     }


     return(0);
}
//|
/// HOOK: ZakupyList_CompareStr
LONG __saveds __asm ZakupyList_CompareStr(register __a1 struct KlientList *Node1, register __a2 struct KlientList *Node2)
{
    return((LONG)StrnCmp(MyLocale, Node1->kl_klient.Nazwa1, Node2->kl_klient.Nazwa1, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG ZakupyList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook ZakupyList_DisplayHook     = { {NULL, NULL}, (VOID *)ZakupyList_Display   ,NULL,NULL };
static const struct Hook ZakupyList_CompareHookStr  = { {NULL, NULL}, (VOID *)ZakupyList_CompareStr,NULL,NULL };

    obj = (Object *)DoSuperNew(cl,obj,
                    InputListFrame,
                    _MUIA_List_AutoVisible  , TRUE,
                    _MUIA_List_DisplayHook  , &ZakupyList_DisplayHook,
                    _MUIA_List_CompareHook  , &ZakupyList_CompareHookStr,
                    _MUIA_List_Format       , "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1 ",

                    _MUIA_List_Title        , TRUE,

#ifdef USE_NLIST
                     MUIA_NList_TitleSeparator, TRUE,
                     MUIA_NList_AutoCopyToClip, TRUE,
#endif

                TAG_MORE, msg->ops_AttrList);

/*
      if(obj)
          {
          struct ZakupyList_Data *data = INST_DATA(cl,obj);

          }
*/

      return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG ZakupyList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

    switch (msg->MethodID)
       {
       case OM_NEW                   : return(ZakupyList_New       (cl, obj, (APTR)msg));
       }

    return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
*/
/// ZAKUPY LIST CLASS

struct MUI_CustomClass *CL_ZakupyList = NULL;

/// Data
struct ZakupyList_Data
{
   ULONG SortOrder;
};
//|

/// Zakupy list hooks
/// HOOK: ZakupyList_Display
void * __saveds __asm ZakupyList_Display(register __a2 char **array, register __a1 struct ZakupList *node)
{
static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };
static char Del[6],
            LP[10],
            Data[2+1+2+1],  // dd.mm\0
            brutto[15],
            vat[15];

/*,
            Name[CUST_NAME_LEN + 10],
            Ulica[CUST_ADRES_LEN + 10],
            Kod[CUST_KOD_LEN + 10],
            Miasto[CUST_MIASTO_LEN + 10];
*/


   if(node)
     {
     struct Zakup *zak = &node->za_zakup;

     sprintf(Del   , "%s" , ((node->Deleted==FALSE) ? " " : "*"));
     *array++ = Del;

     sprintf(LP    , "%ld", zak->P1);      // lp
     *array++ = LP;

     *array++ = zak->P2;                   // nr faktury

     FormatDateHook.h_Data = Data;
     FormatDate(MyLocale, "%d.%m", &zak->P3, &FormatDateHook);
     *array++ = Data;                      // data otrzymania

     *array++ = zak->Sprzedawca.Nazwa1;

     {
     double _brutto, _vat;

     _vat  = CalcVat(zak->P10 + zak->P14, VAT_22);      // vat 22%
     _vat += CalcVat(zak->P12 + zak->P16, VAT_7);       // vat 7%
     _brutto = zak->P9 + zak->P10 + zak->P12 + zak->P14 + zak->P16 + _vat;

     strcpy(brutto, Price2String(_brutto));
     *array++ = brutto;
     strcpy(vat, Price2String(_vat));
     *array++ = vat;
     }


     }
   else
     {
     *array++ = MSG_ZAK_LIST_DEL;
     *array++ = MSG_ZAK_LIST_LP;
     *array++ = MSG_ZAK_LIST_NUMER;
     *array++ = MSG_ZAK_LIST_DATA;
     *array++ = MSG_ZAK_LIST_NAZWA;
     *array++ = MSG_ZAK_LIST_BRUTTO;
     *array++ = MSG_ZAK_LIST_VAT;
     }


     return(0);
}
//|

/// HOOK: ZakupyList_CompareLP
LONG __saveds __asm ZakupyList_CompareLP(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
    if(Node2->za_zakup.P1 == Node1->za_zakup.P1)   return((LONG)0);
    if(Node2->za_zakup.P1 <  Node1->za_zakup.P1)   return((LONG)1);
    if(Node2->za_zakup.P1 >  Node1->za_zakup.P1)   return((LONG)-1);
}
//|
/// HOOK: ZakupyList_CompareNumer
LONG __saveds __asm ZakupyList_CompareNumer(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
    return((LONG)stricmp(Node1->za_zakup.P2, Node2->za_zakup.P2));

}
//|
/// HOOK: ZakupyList_CompareData
LONG __saveds __asm ZakupyList_CompareData(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
    return((LONG)CompareDates(&Node2->za_zakup.P3, &Node1->za_zakup.P3));

}
//|

/// HOOK: ZakupyList_CompareLPR
LONG __saveds __asm ZakupyList_CompareLPR(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
    if(Node2->za_zakup.P1 == Node1->za_zakup.P1)   return((LONG)0);
    if(Node2->za_zakup.P1 >  Node1->za_zakup.P1)   return((LONG)1);
    if(Node2->za_zakup.P1 <  Node1->za_zakup.P1)   return((LONG)-1);
}
//|
/// HOOK: ZakupyList_CompareNumerR
LONG __saveds __asm ZakupyList_CompareNumerR(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
    return((LONG)stricmp(Node2->za_zakup.P2, Node1->za_zakup.P2));

}
//|
/// HOOK: ZakupyList_CompareDataR
LONG __saveds __asm ZakupyList_CompareDataR(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
    return((LONG)CompareDates(&Node1->za_zakup.P3, &Node2->za_zakup.P3));

}
//|
//|

static const struct Hook ZakupyList_DisplayHook      = { {NULL, NULL}, (VOID *)ZakupyList_Display      , NULL,NULL };
static const struct Hook ZakupyList_CompareHookLP    = { {NULL, NULL}, (VOID *)ZakupyList_CompareLP    , NULL,NULL };
static const struct Hook ZakupyList_CompareHookNumer = { {NULL, NULL}, (VOID *)ZakupyList_CompareNumer , NULL,NULL };
static const struct Hook ZakupyList_CompareHookData  = { {NULL, NULL}, (VOID *)ZakupyList_CompareData  , NULL,NULL };
static const struct Hook ZakupyList_CompareHookLPR   = { {NULL, NULL}, (VOID *)ZakupyList_CompareLPR   , NULL,NULL };
static const struct Hook ZakupyList_CompareHookNumerR= { {NULL, NULL}, (VOID *)ZakupyList_CompareNumerR, NULL,NULL };
static const struct Hook ZakupyList_CompareHookDataR = { {NULL, NULL}, (VOID *)ZakupyList_CompareDataR , NULL,NULL };

/// OM_NEW

ULONG ZakupyList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{

    obj = (Object *)DoSuperNew(cl,obj,
                    InputListFrame,
                    _MUIA_List_AutoVisible  , TRUE,
                    _MUIA_List_DisplayHook  , &ZakupyList_DisplayHook,
                    _MUIA_List_CompareHook  , &ZakupyList_CompareHookLP,
                    _MUIA_List_Format       , "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1 BAR P=\033r,"
                                              "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1 BAR P=\033c,"
                                              "MIW=1 MAW=-1 BAR,"
                                              "MIW=1 MAW=-1 BAR P=\033r,"
                                              "MIW=1 MAW=-1 P=\033r",

                    _MUIA_List_Title        , TRUE,

#ifdef USE_NLIST
                     MUIA_NList_TitleSeparator, TRUE,
                     MUIA_NList_AutoCopyToClip, TRUE,
#endif

                TAG_MORE, msg->ops_AttrList);


      if(obj)
          {
//          struct ZakupyList_Data *data = INST_DATA(cl,obj);


          /*** trick to set arguments ***/

          msg->MethodID = OM_SET;
          DoMethodA(obj, (Msg)msg);
          msg->MethodID = OM_NEW;

          }


      return((ULONG)obj);
}

//|
/// OM_SET

ULONG ASM ZakupyList_Set(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ZakupyList_Data *data = INST_DATA(cl, obj);
struct TagItem *tags,*tag;

    for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
       {
       switch(tag->ti_Tag)
          {
          case MUIA_ZakupyList_Order:
               {
               data->SortOrder = tag->ti_Data;

               if((tag->ti_Data & 0x80000000) == 0)
                   {
                   switch(tag->ti_Data)
                      {
                      case 0: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookLP); break;
                      case 1: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookNumer); break;
                      case 2: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookData); break;
                      }
                   }
                else
                   {
                   switch(tag->ti_Data & 0x7fffffff)
                      {
                      case 0: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookLPR); break;
                      case 1: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookNumerR); break;
                      case 2: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookDataR); break;
                      }
                   }

               }
               break;
          }
        }

    return(DoSuperMethodA(cl, obj, msg));
}

//|
/// OM_GET
static ULONG ASM ZakupyList_Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ZakupyList_Data *data = INST_DATA(cl, obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

        switch(((struct opGet *)msg)->opg_AttrID)
                {
                case MUIA_ZakupyList_Order:
                     *store = data->SortOrder;
                     return(TRUE);
                     break;
                }

        return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher

__saveds __asm ULONG ZakupyList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

    switch (msg->MethodID)
       {
       case OM_NEW                   : return(ZakupyList_New       (cl, obj, (APTR)msg));
       case OM_SET                   : return(ZakupyList_Set       (cl, obj, (APTR)msg));
       case OM_GET                   : return(ZakupyList_Get       (cl, obj, (APTR)msg));
       }

    return(DoSuperMethodA(cl,obj,msg));
}

//|

//|


/// WinOpen(Object *win)
long WinOpen(Object *win)
{
    set(win, MUIA_Window_Open, TRUE);
    return(xget(win, MUIA_Window_Open));
}
//|

/// COUNT STOP CHUNKS
#define COUNT_NUM_STOPS (sizeof(Count_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Count_Stops[] =
{
        ID_COUN, ID_CAT,
        ID_COUN, ID_VERS,
        ID_COUN, ID_COUN,
        NULL, NULL,
};
//|

/// WczytajCount

long WczytajCount(char *FileName)
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
int    Errors = 0;
char   ValidFile = FALSE;
int    i;
long   Count = 0;


#ifndef DEMO

     set(app, MUIA_Application_Sleep, TRUE);

     if(iff = AllocIFF())
       {
       if(iff->iff_Stream = Open(FileName, MODE_OLDFILE))
           {
           InitIFFasDOS(iff);

           StopChunks(iff, Count_Stops, COUNT_NUM_STOPS);
//           StopOnExit(iff, ID_COUN, ID_FORM);

           if(!OpenIFF(iff, IFFF_READ))
               {

               while(TRUE)
                  {
                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                     break;

                  if(cn = CurrentChunk(iff))
                     {
                     LONG ID = cn->cn_ID;

                     if(!ValidFile)
                        {
                        if((ID == ID_CAT) && (cn->cn_Type == ID_COUN))
                           {
//                           Usu�Magazyn();
                           ValidFile = TRUE;
                           continue;
                           }


                        break;
                        }

///                    ID_VERS
                     if(ID == ID_VERS)
                        {
                        struct BaseVersion version;

                        if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                           {

                           }
                        else
                           {
                           _RC = IoErr();
                           break;
                           }

                        continue;
                        }
//|
///                    ID_COUN

                     if(ID == ID_COUN)
                        {
                        if(ReadChunkBytes(iff, &Count, sizeof(long)) != cn->cn_Size)
                           _RC = IoErr();
                        break;
                        }
//|

                     }
                  }

               CloseIFF(iff);
               }

            Close(iff->iff_Stream);
            }


//       if(_RC == IFFERR_EOF) _RC = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

       FreeIFF(iff);
       }

       set(app, MUIA_Application_Sleep, FALSE);

#endif

       return(Count);
}

//|
/// ZapiszCount

char ZapiszCount(char *FileName, long Count)
{
#ifndef DEMO

struct IFFHandle *MyIFFHandle;
int i;

    set(app, MUIA_Application_Sleep, TRUE);

    if(MyIFFHandle = AllocIFF())
        {
        BPTR  FileHandle;

        if(FileHandle = Open(FileName, MODE_NEWFILE))
           {
           MyIFFHandle->iff_Stream = FileHandle;
           InitIFFasDOS(MyIFFHandle);

           if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
               {
               struct BaseVersion version;

               PushChunk(MyIFFHandle, ID_COUN, ID_CAT, IFFSIZE_UNKNOWN);

               PushChunk(MyIFFHandle, ID_COUN, ID_FORM, IFFSIZE_UNKNOWN);
                   PushChunk(MyIFFHandle, ID_COUN, ID_VERS, IFFSIZE_UNKNOWN);
                   version.Version = VERSION;
                   version.Revision = REVISION;
                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                   PopChunk(MyIFFHandle);

/*
                   PushChunk(MyIFFHandle, ID_COUN, ID_ANNO, IFFSIZE_UNKNOWN);
                   WriteChunkBytes(MyIFFHandle, ScreenTitle, sizeof(ScreenTitle)-1);
                   WriteChunkBytes(MyIFFHandle, " <", 2);
                   WriteChunkBytes(MyIFFHandle, EMAIL, strlen(EMAIL));
                   WriteChunkBytes(MyIFFHandle, ">, ", 3);
                   WriteChunkBytes(MyIFFHandle, WWW, strlen(WWW));
                   PopChunk(MyIFFHandle);
*/
               PopChunk(MyIFFHandle);

               PushChunk(MyIFFHandle, ID_COUN, ID_FORM, IFFSIZE_UNKNOWN);

               PushChunk(MyIFFHandle, ID_COUN, ID_COUN, IFFSIZE_UNKNOWN);
               WriteChunkBytes(MyIFFHandle, &Count, sizeof(Count));
               PopChunk(MyIFFHandle);

               PopChunk(MyIFFHandle);

               PopChunk(MyIFFHandle);
               CloseIFF(MyIFFHandle);
               }
           else
               {
               DisplayBeep(0);
               D(bug("*** OpenIFF() nie powiod�o si�\n"));
               }

           Close(FileHandle);
           }
        else
           {
//           MUI_Request(app, (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
           D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", FileName));
           }

        FreeIFF(MyIFFHandle);
        }
     else
        {
        DisplayBeep(0);
        D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
        }

    set(app, MUIA_Application_Sleep, FALSE);

#endif

    return(0);
}



//|


/// NUMB STOP CHUNKS
#define NUMB_NUM_STOPS (sizeof(Numb_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Numb_Stops[] =
{
        ID_NUMB, ID_CAT,
        ID_NUMB, ID_VERS,

        ID_NUMB, ID_FVOR,
        ID_NUMB, ID_FVKO,
        ID_NUMB, ID_FVPO,
        ID_NUMB, ID_RUOR,
        ID_NUMB, ID_RUKO,
        ID_NUMB, ID_RUPO,
        ID_NUMB, ID_PARA,

        NULL, NULL,
};
//|

/// WczytajNumeracj�Dokument�w

long WczytajNumeracj�Dokument�w(void)
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
long   Errors = 0;
char   ValidFile = FALSE;

#ifndef DEMO

     set(app, MUIA_Application_Sleep, TRUE);

     if(iff = AllocIFF())
       {
       if(iff->iff_Stream = Open(NumeracjaFileName, MODE_OLDFILE))
           {
           InitIFFasDOS(iff);

           StopChunks(iff, Numb_Stops, NUMB_NUM_STOPS);

           if(!OpenIFF(iff, IFFF_READ))
               {

               while(TRUE)
                  {
                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                     break;

                  if(cn = CurrentChunk(iff))
                     {
                     LONG ID = cn->cn_ID;

                     if(!ValidFile)
                        {
                        if((ID == ID_CAT) && (cn->cn_Type == ID_NUMB))
                           {
                           ValidFile = TRUE;
                           continue;
                           }


                        break;
                        }

///                    ID_VERS
                     if(ID == ID_VERS)
                        {
                        struct BaseVersion version;

                        if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                           {

                           }
                        else
                           {
                           _RC = IoErr();
                           break;
                           }

                        continue;
                        }
//|

                     _read(ID_FVOR, &docs.numer_fvat)
                     _read(ID_FVKO, &docs.numer_fvat_kor)
                     _read(ID_FVPO, &docs.numer_fvat_par)
                     _read(ID_RUOR, &docs.numer_rach)
                     _read(ID_RUKO, &docs.numer_rach_kor)
                     _read(ID_RUPO, &docs.numer_rach_par)
                     _read(ID_PARA, &docs.numer_paragon)

                     }
                  }

               CloseIFF(iff);
               }

           Close(iff->iff_Stream);
           }


//       if(_RC == IFFERR_EOF) _RC = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

       FreeIFF(iff);
       }

       set(app, MUIA_Application_Sleep, FALSE);

#endif

       return(0);
}

//|
/// ZapiszNumeracj�Dokument�w

char ZapiszNumeracj�Dokument�w(void)
{
#ifndef DEMO

#define _ID_TYPE ID_NUMB

struct IFFHandle *MyIFFHandle;

    set(app, MUIA_Application_Sleep, TRUE);

    if(MyIFFHandle = AllocIFF())
        {
        BPTR  FileHandle;

        if(FileHandle = Open(NumeracjaFileName, MODE_NEWFILE))
           {
           MyIFFHandle->iff_Stream = FileHandle;
           InitIFFasDOS(MyIFFHandle);

           if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
               {
               struct BaseVersion version;

               PushChunk(MyIFFHandle, ID_NUMB, ID_CAT, IFFSIZE_UNKNOWN);

               PushChunk(MyIFFHandle, ID_NUMB, ID_FORM, IFFSIZE_UNKNOWN);
                   PushChunk(MyIFFHandle, ID_NUMB, ID_VERS, IFFSIZE_UNKNOWN);
                   version.Version = VERSION;
                   version.Revision = REVISION;
                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                   PopChunk(MyIFFHandle);

               PopChunk(MyIFFHandle);

               PushChunk(MyIFFHandle, ID_NUMB, ID_FORM, IFFSIZE_UNKNOWN);

                 _write(ID_FVOR, docs.numer_fvat)
                 _write(ID_FVKO, docs.numer_fvat_kor)
                 _write(ID_FVPO, docs.numer_fvat_par)
                 _write(ID_RUOR, docs.numer_rach)
                 _write(ID_RUKO, docs.numer_rach_kor)
                 _write(ID_RUPO, docs.numer_rach_par)
                 _write(ID_PARA, docs.numer_paragon)

               PopChunk(MyIFFHandle);

               PopChunk(MyIFFHandle);
               CloseIFF(MyIFFHandle);
               }
           else
               {
               DisplayBeep(0);
               D(bug("*** OpenIFF() nie powiod�o si�\n"));
               }

           Close(FileHandle);
           }
        else
           {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
           D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", NumeracjaFileName));
           }

        FreeIFF(MyIFFHandle);
        }
     else
        {
        DisplayBeep(0);
        D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
        }

    set(app, MUIA_Application_Sleep, FALSE);

#endif

    return(0);

#undef _ID_TYPE
}



//|


/// SYS STOP CHUNKS
#define SYS_NUM_STOPS (sizeof(Sys_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Sys_Stops[] =
{
        ID_SYS, ID_CAT,
        ID_SYS, ID_VERS,

        ID_SYS, ID_STRT,
        ID_SYS, ID_OPER,

        NULL, NULL,
};
//|

/// WczytajSystemInfo

char WczytajSystemInfo(void)
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
long   Errors = 0;
char   ValidFile = FALSE;

#define _ID_TYPE ID_SYS

     set(app, MUIA_Application_Sleep, TRUE);

     if(iff = AllocIFF())
       {
       if(iff->iff_Stream = Open(SystemFileName, MODE_OLDFILE))
           {
           InitIFFasDOS(iff);

           StopChunks(iff, Sys_Stops, SYS_NUM_STOPS);

           if(!OpenIFF(iff, IFFF_READ))
               {

               while(TRUE)
                  {
                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                     break;

                  if(cn = CurrentChunk(iff))
                     {
                     LONG ID = cn->cn_ID;

                     if(!ValidFile)
                        {
                        if((ID == ID_CAT) && (cn->cn_Type == _ID_TYPE))
                           {
                           ValidFile = TRUE;
                           continue;
                           }

                        break;
                        }

///                    ID_VERS
                     if(ID == ID_VERS)
                        {
                        struct BaseVersion version;

                        if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                           {

                           }
                        else
                           {
                           _RC = IoErr();
                           break;
                           }

                        continue;
                        }
//|

                     _read(ID_STRT, &sys.last_start)
                     _read(ID_OPER, &sys.last_operation)
                     }
                  }

               CloseIFF(iff);
               }

            Close(iff->iff_Stream);
            }


//       if(_RC == IFFERR_EOF) _RC = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

       FreeIFF(iff);
       }

       set(app, MUIA_Application_Sleep, FALSE);

       return(0);
}

//|
/// ZapiszSystemInfo

char ZapiszSystemInfo(void)
{

#define _ID_TYPE ID_SYS

struct IFFHandle *MyIFFHandle;

    set(app, MUIA_Application_Sleep, TRUE);

    if(MyIFFHandle = AllocIFF())
        {
        BPTR  FileHandle;

        if(FileHandle = Open(SystemFileName, MODE_NEWFILE))
           {
           MyIFFHandle->iff_Stream = FileHandle;
           InitIFFasDOS(MyIFFHandle);

           if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
               {
               struct BaseVersion version;

               PushChunk(MyIFFHandle, _ID_TYPE, ID_CAT, IFFSIZE_UNKNOWN);

               PushChunk(MyIFFHandle, _ID_TYPE, ID_FORM, IFFSIZE_UNKNOWN);
                   PushChunk(MyIFFHandle, _ID_TYPE, ID_VERS, IFFSIZE_UNKNOWN);
                   version.Version = VERSION;
                   version.Revision = REVISION;
                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                   PopChunk(MyIFFHandle);

               PopChunk(MyIFFHandle);

               PushChunk(MyIFFHandle, _ID_TYPE, ID_FORM, IFFSIZE_UNKNOWN);

               _write(ID_STRT, sys.last_start)
               _write(ID_OPER, sys.last_operation)

               PopChunk(MyIFFHandle);

               PopChunk(MyIFFHandle);
               CloseIFF(MyIFFHandle);
               }
           else
               {
               DisplayBeep(0);
               D(bug("*** OpenIFF() nie powiod�o si�\n"));
               }

           Close(FileHandle);
           }
        else
           {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
           D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", SystemFileName));
           }

        FreeIFF(MyIFFHandle);
        }
     else
        {
        DisplayBeep(0);
        D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
        }

    set(app, MUIA_Application_Sleep, FALSE);


    return(0);

#undef _ID_TYPE
}



//|


/// Create classes...

char CreateClasses(void)
{
char Result = TRUE;

   Result = Result && (CL_UsersList          = MUI_CreateCustomClass(NULL, _MUIC_List, NULL, sizeof(struct UsersList_Data          ), UsersList_Dispatcher          ));
   Result = Result && (CL_ProductList        = MUI_CreateCustomClass(NULL, _MUIC_List, NULL, sizeof(struct ProductList_Data        ), ProductList_Dispatcher        ));
   Result = Result && (CL_FakturaProductList = MUI_CreateCustomClass(NULL, _MUIC_List, NULL, sizeof(struct FakturaProductList_Data ), FakturaProductList_Dispatcher ));
   Result = Result && (CL_GroupList          = MUI_CreateCustomClass(NULL, _MUIC_List, NULL, sizeof(struct GroupList_Data          ), GroupList_Dispatcher          ));
//   Result = Result && (CL_CustomerList       = MUI_CreateCustomClass(NULL, _MUIC_List, NULL, sizeof(struct CustomerList_Data       ), CustomerList_Dispatcher       ));
   Result = Result && (CL_UnitList           = MUI_CreateCustomClass(NULL, _MUIC_List, NULL, sizeof(struct UnitList_Data           ), UnitList_Dispatcher           ));
   Result = Result && (CL_PayList            = MUI_CreateCustomClass(NULL, _MUIC_List, NULL, sizeof(struct PayList_Data            ), PayList_Dispatcher            ));
   Result = Result && (CL_DocumentList       = MUI_CreateCustomClass(NULL, _MUIC_List, NULL, sizeof(struct DocumentList_Data       ), DocumentList_Dispatcher       ));
   Result = Result && (CL_SmartCycle         = MUI_CreateCustomClass(NULL, MUIC_Group, NULL, sizeof(struct SmartCycle_Data         ), SmartCycle_Dispatcher         ));
//   Result = Result && (CL_BString            = MUI_CreateCustomClass(NULL, MUIC_BetterString , NULL, sizeof(struct BString_Data    ), BString_Dispatcher            ));
   Result = Result && (CL_ZakupyList         = MUI_CreateCustomClass(NULL, _MUIC_List, NULL, sizeof(struct ZakupyList_Data         ), ZakupyList_Dispatcher         ));

   return(Result);
}
//|
/// Delete classes...
void DeleteClasses(void)
{

   if(CL_UsersList         ) { MUI_DeleteCustomClass(CL_UsersList); CL_UsersList = NULL; }
   if(CL_ProductList       ) { MUI_DeleteCustomClass(CL_ProductList); CL_ProductList = NULL; }
   if(CL_FakturaProductList) { MUI_DeleteCustomClass(CL_FakturaProductList); CL_FakturaProductList = NULL; }
   if(CL_GroupList         ) { MUI_DeleteCustomClass(CL_GroupList); CL_GroupList = NULL; }
//   if(CL_CustomerList      ) { MUI_DeleteCustomClass(CL_CustomerList); CL_CustomerList = NULL; }
   if(CL_UnitList          ) { MUI_DeleteCustomClass(CL_UnitList); CL_UnitList = NULL; }
   if(CL_PayList           ) { MUI_DeleteCustomClass(CL_PayList); CL_PayList = NULL; }
   if(CL_DocumentList      ) { MUI_DeleteCustomClass(CL_DocumentList); CL_DocumentList = NULL; }
   if(CL_SmartCycle        ) { MUI_DeleteCustomClass(CL_SmartCycle); CL_SmartCycle = NULL; }
//   if(CL_BString           ) { MUI_DeleteCustomClass(CL_BString); CL_BString = NULL; }
   if(CL_ZakupyList        ) { MUI_DeleteCustomClass(CL_ZakupyList); CL_ZakupyList = NULL; }

}
//|

/// SmartCreateDir

char SmartCreateDir(char *DirName)
{
BPTR Dir;

    if(!(Dir = Lock(DirName, ACCESS_READ)))
       {
       if(!(Dir = CreateDir(DirName)))
           {
           DisplayBeep(0);
           return(FALSE);
           }
       }
    if(Dir)   UnLock(Dir);

    return(TRUE);
}

//|

/// GetSmartGuide
void GetSmartGuide(char *Output)
{
/*
** Looks for AmigaGuide on-line documentation
** The most correct location should be the last one,
** while the 1st should be default one (it will be
** returned even if file doesn't exists there).
*/

char Language[40];
char Tmp[128];
char *Locations[] = {
                      "PROGDIR:Docs/Golem.guide",

                      "PROGDIR:Golem.guide",
                      "PROGDIR:Docs/Golem.guide",
                      "PROGDIR:Golem_english.guide",
                      "PROGDIR:Docs/Golem_english.guide",
                      "PROGDIR:%s.guide",
                      "PROGDIR:Golem_%s.guide",
                      "PROGDIR:Docs/Golem_%s.guide",
                      "HELP:%s/Golem.guide"
                     };
int  Index        = (sizeof(Locations)/sizeof(char *)) - 1;


   sprintf(Tmp, Locations[0], "english");

   if(GetVar("language", Language, sizeof(Language), NULL) > 0)
     {
     while(Index >= 0)
       {
       BPTR GuideLock;

       sprintf(Tmp, Locations[Index], Language);

       if(GuideLock = Lock(Tmp, ACCESS_READ))
          {
          UnLock(GuideLock);
          break;
          }
       else
          Index--;
       }
     }

     sprintf(Output, Tmp);

}
//|

/// SetMainInputEvents
void SetMainInputEvents(char page)
{

    D(bug("SetMainInputEvents() Page: %ld\n", page));

    switch(page)
       {
       case PAGE_MAIN:
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_SPRZEDAZ);
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_ZAKUPY);
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_REDAGOWANIE);
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f4", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_BAZYDANYCH);
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f5", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_ROZLICZENIA);
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_SYSTEM);
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f8", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_LOGINOUT);
          break;

       case PAGE_SPRZEDAZ:
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_VAT);
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_UPROSZCZONY);
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f5", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_VAT_PAR);
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_UPROSZCZONY_PAR);
          if(settings.posnet_active)
              DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f7", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_PARAGON);
          break;

       case PAGE_BAZYDANYCH:
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_MAGAZYN);
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_KONTRAHENCI);
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_USERS);
          break;

       case PAGE_ROZLICZENIA:
//          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_MAGAZYN);

          if(settings.posnet_active)
            {
            DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_DOBOWY);
            DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_OKRESOWY);
            DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f4", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_STAN_KASY);
            DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f5", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_ZMIANY);
            }
          break;

       case PAGE_SYSTEM:
          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_SETTINGS);
          break;

       }
}
//|
/// ClearMainInputEvents
void ClearMainInputEvents(char page)
{

    D(bug("ClearMainInputEvents() Page: %ld\n", page));

    switch(page)
       {
       case PAGE_MAIN:
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          break;

       case PAGE_SPRZEDAZ:
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          if(settings.posnet_active)
              DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          break;

       case PAGE_BAZYDANYCH:
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          break;

       case PAGE_ROZLICZENIA:
//          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          if(settings.posnet_active)
            {
            DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
            DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
            DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
            DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
            }
          break;

       case PAGE_SYSTEM:
          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
          break;

       }
}
//|
/// SwitchToPage
void SwitchToPage(int page)
{

    ClearMainInputEvents(xget(GR_ToolBar_Menus, MUIA_Group_ActivePage));

    DoMethod(BT_ToolBar_Login, MUIM_MultiSet, MUIA_Disabled, !settings.posnet_active,
                            BT_ToolBar_Paragon,
                            BT_ToolBar_Rozliczenia_Fisk_Dobowy,
                            BT_ToolBar_Rozliczenia_Fisk_Okresowy,
                            BT_ToolBar_Rozliczenia_Fisk_StanKasy,
                            BT_ToolBar_Rozliczenia_Fisk_RaportZmiany,
                            NULL);


    SetMainInputEvents(page);
    set(GR_ToolBar_Menus, MUIA_Group_ActivePage, page);

    switch(page)
       {
       case PAGE_SPRZEDAZ:
           set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Sprzeda�_Back);
           break;
       case PAGE_ZAKUPY:
           set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Zakupy_Back);
           break;
       case PAGE_BAZYDANYCH:
           set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Bazy_Back);
           break;
       case PAGE_SYSTEM:
           set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_System_Back);
           break;                                                     
       case PAGE_ROZLICZENIA:
           set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Rozliczenia_Back);
           break;
       }
}
//|

/// Main
int main(void)
{
//APTR StartupWindow, MainWindow, LoginWindow;

/// Opening libraries, catalog etc...



   if((MUIMasterBase = OpenLibrary(MUIMASTER_NAME, MUIMASTER_VMIN)) == NULL)
       {
       EasyRequest(NULL, &MyEasy, NULL, MSG_ERR_MUI);
       CleanUp();
       return(EXIT_FAILURE);
       }


   if(((LocaleBase = OpenLibrary("locale.library", 38)) == NULL))
       {
       MUI_Request(app, NULL, 0, TITLE, MSG_OK, MSG_ERR_LOCALE);
       CleanUp();
       return(EXIT_FAILURE);
       }
   MyLocale  = OpenLocale(NULL);


   if(stricmp(MyLocale->loc_LanguageName, "polski.language") != 0)
     {
     DisplayBeep(0);
     MUI_Request(app, NULL, 0, TITLE, MSG_OK, MSG_WAR_NO_POLSKI);
     CleanUp();
     return(EXIT_FAILURE);
     }



//|

   if(CreateClasses())
      {
      if(app = CreateApp())
        {

#ifdef DEMO
        {
        struct DateStamp ds = {0};

        ds.ds_Days = _EXPIREDATE
        MUI_Request(app, NULL, 0, TITLE, MSG_OK, MSG_DEMO_TEXT);

        DateStamp(&Today);

        if(CompareDates(&Today, &ds) < 0)
           {
           MUI_Request(app, NULL, 0, TITLE, MSG_OK, "\033cOkres testowy programu Golem min��!\nPobierz now� wersj� z serwera FTP\n\0338http://amiga.com.pl/golem/\0332");
           goto Wrong_Date_Quit;
           }
        }
#endif


        Logout();
        ToolBarLogin(FALSE);


// Inicjalizacja
        set(BT_Startup_Ok, MUIA_Disabled, TRUE);
        set(StartupWindow, MUIA_Window_Open, TRUE);

        set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_INIT);
//        Delay(10);

        DateStamp(&Today);


/// LOADING DOCUMENTS INFORMATION & SETTINGS

//        Delay(25);

        DateStamp(&docs.data_fvat);
        DateStamp(&docs.data_fvat_kor);
        DateStamp(&docs.data_fvat_par);
        DateStamp(&docs.data_rach);
        DateStamp(&docs.data_rach_kor);
        DateStamp(&docs.data_rach_par);
        DateStamp(&docs.data_paragon);
        WczytajUstawienia();
        posnet_DisplayAbout();

        set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_NUMBERS);
        WczytajNumeracj�Dokument�w();

        WczytajKodyDrukarki();

#ifdef DEMO
        if(settings.posnet_active)
           MUI_Request(app, StartupWindow, 0, TITLE, MSG_OK, "\033c\033bU W A G A !\033n\nNiekt�re z funkcji obs�uguj�cych drukarki fiskalne NIE s�\nzablokowane w wersji demonstracyjnej!\n\nNIE POD��CZAJ zatem zafiskalizowanej drukarki albo nie w��czaj obs�ugi\nPosnetu w preferencjach programu.");
#endif


//|
/// CHECKING PROGRAM DIRS

//        Delay(25);
        set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_DIRS);

        {
        static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };
        BPTR  Dir;
        char   DirName[64];
        char   Miesiac[3];
        char   Rok[5];
        char   result = TRUE;
        int    i;

        result = result && SmartCreateDir(GolemConfigDir);
        result = result && SmartCreateDir(DatabasesDir);


        // katalogi dla dokument�w

        FormatDateHook.h_Data = Miesiac;
        FormatDate(MyLocale, "%m", &Today, &FormatDateHook);
        FormatDateHook.h_Data = Rok;
        FormatDate(MyLocale, "%Y", &Today, &FormatDateHook);

        sprintf(DirName, "%s/%s", DatabasesDir, Rok);
        result = result && SmartCreateDir(DirName);
        sprintf(DirName, "%s/%s/%s", DatabasesDir, Rok, Miesiac);
        result = result && SmartCreateDir(DirName);

        for(i=0; i < DOCUMENT_COUNT; i++)
           {
           sprintf(DirName, "%s/%s/%s/%02ld", DatabasesDir, Rok, Miesiac, i);
           result = result && SmartCreateDir(DirName);
           }

        for(i=ZAKUPY; i < DOCUMENT_COUNT_2; i++)
           {
           sprintf(DirName, "%s/%s/%s/%02lx", DatabasesDir, Rok, Miesiac, i);
           result = result && SmartCreateDir(DirName);
           }

         if(!result)
           {
           MUI_Request(app, StartupWindow, 0, TITLE, "*_Sprawdz�", "Nie mog� utworzy� niezb�dnych katalog�w!\nPrawdopodobnie dysk jest zabezpieczony przed zapisem");
           goto Create_Dirs_Problems_Quit;
           }


        }
//|
/// LOADING SYSTEM INFORMATION

          set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_DATES);

          WczytajSystemInfo();

//          if(WczytajSystemInfo())
            {
//            D(bug("Today '%s'\n", Date2Str(&Today)));
//            D(bug("SystemInfo: LastStart '%s'\n", Date2Str(&sys.last_start)));

            // sprawdzamy czy czas tu biegnie normalnie... ;-)
            if(CompareDates(&sys.last_start, &Today) < 0)
              {
              DateStamp(&sys.last_start);
              ZapiszSystemInfo();

              if(MUI_Request(app, NULL, 0, TITLE, MSG_ERR_DATE_GAD, MSG_ERR_DATE))
                 {
                 system("RUN <>nil: SYS:Prefs/Time <>nil:");
                 }

              goto Wrong_Date_Quit;
              }

//            Sprawd�CzyKoniecMiesi�caRoku();

            DateStamp(&sys.last_start);
            ZapiszSystemInfo();
            }
/*
          else
            {
            DateStamp(&sys.last_start);
            ZapiszSystemInfo();
            }
*/

//|
/// CHECKING ONLINE HELP

//        Delay(25);
        set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_HELP);

        GetSmartGuide(GuideName);
        set(app, MUIA_Application_HelpFile, GuideName);

//|
/// LOADING USERS DATABASE

//        Delay(25);

        set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_USERS);
        NewList(&u�ytkownicy);
#ifndef DEMO
        WczytajU�ytkownik�w();
#endif
        if(IsListEmpty(&u�ytkownicy))
            DodajAdministratora();

//|
/// LOADING STOCK DATABASE & UNITS & PAYMENTS

//        Delay(25);

        NewList(&jedn_miary);
        WczytajJednostki();

        NewList(&p�atno�ci);
        WczytajP�atno�ci();


        InitMagazyn();
#ifndef DEMO
        WczytajMagazyn();
#endif

//|
/// LOADING CUSTOMERS DATABASE

//        Delay(25);

        NewList(&kontrahenci);
#ifndef DEMO
        WczytajKlient�w();
#endif

//|




#ifdef DEMO
        set(GA_Startup_Info, MUIA_Gauge_InfoText, "\0338Prosz� poczeka� kilka sekund");
        Delay(51 * 4);
#endif
        set(GA_Startup_Info, MUIA_Gauge_InfoText, "");
        set(BT_Startup_Ok  , MUIA_Disabled, FALSE);
        set(StartupWindow  , MUIA_Window_ActiveObject, BT_Startup_Ok);




//        set(BT_ToolBar_Zakupy          , MUIA_Disabled, TRUE);
        set(BT_ToolBar_Rozliczenia_Norm, MUIA_Disabled, TRUE);




        {
        char  running = TRUE;
        ULONG signal  = 0;

        while(running)
          {
          switch (DoMethod(app, MUIM_Application_Input, &signal))
            {
            case MUIV_Application_ReturnID_Quit:
                 switch(xget(GR_ToolBar_Menus, MUIA_Group_ActivePage))
                   {
                   case 0:
                        set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Quit);
                        if(logged_user)
                           {
                           if(IsEnv("Golem/LogoutNotQuit"))
                              {
                              goto ToolbarLogout;
                              }
                           else
                              {
                              if(MUI_Request(app, MainWindow, 0, TITLE, MSG_APP_QUIT_GAD, MSG_APP_QUIT))
                                 running = FALSE;
                              }
                           }
                        else
                           running = FALSE;
                        break;

                   default:
                        SwitchToPage(0);
                        break;

                   }
                   break;

            case ID_STARTUP_CLOSED:
                 set(MainWindow   , MUIA_Window_Open, TRUE);
                 set(StartupWindow, MUIA_Window_Open, FALSE);
                 set(app, MUIA_Application_Sleep, FALSE);

                 posnet_DisplayIdle();
                 break;


            case ID_TOOLBAR_MAINMENU:
                 SwitchToPage(0);
                 break;


///        **********   Sprzeda�    ***********

            case ID_TOOLBAR_SPRZEDAZ:
                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                     SwitchToPage(PAGE_SPRZEDAZ);
                 break;

            case ID_TOOLBAR_VAT:
                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                   {
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Vat);
                   Sprzeda�(FAKTURA_VAT);
                   }
                 break;

            case ID_TOOLBAR_UPROSZCZONY:
                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                   {
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Upr);
                   Sprzeda�(RACHUNEK);
                   }
                 break;


            case ID_TOOLBAR_VAT_PAR:
                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                   {
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Vat_Par);
                   Sprzeda�(FVAT_PAR);
                   }
                 break;

            case ID_TOOLBAR_UPROSZCZONY_PAR:
                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                   {
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Upr_Par);
                   Sprzeda�(RACH_PAR);
                   }
                 break;

            case ID_TOOLBAR_PARAGON:
                 if(settings.posnet_active)
                   {
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Paragon);
                   Sprzeda�(PARAGON);
                   }
                 break;

//|
///        **********    Zakupy     ***********


            case ID_TOOLBAR_ZAKUPY:
//                 if(!Sprawd�CzyKoniecMiesi�caRoku())
//                   {
                   SwitchToPage(PAGE_ZAKUPY);
//                   }
                 break;

           case ID_TOOLBAR_ZAKUPY_KOSZTY:
                   Zakupy();
                break;

//|
///        **********  Rozliczenia  ***********

            case ID_TOOLBAR_ROZLICZENIA:
                 SwitchToPage(PAGE_ROZLICZENIA);
                 break;

            case ID_TOOLBAR_POSNET_RAPORT_DOBOWY:
                 RaportFiskalny(POSNET_RAPORT_DOBOWY);
                 posnet_DisplayIdle();
                 break;

            case ID_TOOLBAR_POSNET_RAPORT_OKRESOWY:
                 RaportFiskalny(POSNET_RAPORT_OKRESOWY);
                 posnet_DisplayIdle();
                 break;

            case ID_TOOLBAR_POSNET_STAN_KASY:
                 posnet_StanKasy();
                 break;

            case ID_TOOLBAR_POSNET_RAPORT_ZMIANY:
                 posnet_RaportZmiany();
                 break;



//|
///        **********  Bazy danych  ***********

            case ID_TOOLBAR_BAZYDANYCH:
                 SwitchToPage(PAGE_BAZYDANYCH);
                 break;

            case ID_TOOLBAR_MAGAZYN:
                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Magazyn);
                 EdycjaMagazynu();
                 break;

            case ID_TOOLBAR_KONTRAHENCI:
                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Kontrahenci);
                 EdycjaKontrahent�w();
                 break;

            case ID_TOOLBAR_USERS:
                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_UserBase);
                 U�ytkownicy();
                 break;

//|
///        **********  Redagowanie  ***********

            case ID_TOOLBAR_REDAGOWANIE:
                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                   {
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Redagowanie);
                   Redagowanie();
                   }
                 break;

//|
///        **********    System     ***********

            case ID_TOOLBAR_SYSTEM:
                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                   {
                   SwitchToPage(PAGE_SYSTEM);
                   }
                 break;

            case ID_TOOLBAR_SETTINGS:
                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Settings);
                 EdycjaUstawie�();
                 break;

//|


            case ID_TOOLBAR_ABOUT:
                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_About);
                 set(app, MUIA_Application_Sleep, TRUE);
                 set(GA_Startup_Info  , MUIA_Gauge_InfoText, WWW);
                 set(BT_Startup_Ok    , MUIA_Disabled, FALSE);
                 set(CR_Startup_Scroll, MUIA_Virtgroup_Top, 0);
                 set(StartupWindow    , MUIA_Window_ActiveObject, BT_Startup_Ok);
                 set(StartupWindow    , MUIA_Window_Open, TRUE);

                 posnet_DisplayAbout();
                 break;




            case ID_TOOLBAR_LOGINOUT:
                 {
                 if(logged_user == NULL)
                   {
                   if(logged_user = Login())
                     {
                     ToolBarLogin(TRUE);
                     SetMainInputEvents(0);
                     }
                   }
                 else
                   {
                   goto ToolbarLogout;
                   }
                 }
                 break;


            case ID_TOOLBAR_LOGIN:
                 {
                 if(logged_user = Login())
                   {
                   ToolBarLogin(TRUE);
                   SetMainInputEvents(0);
                   }
                 }
                 break;
            case ID_TOOLBAR_LOGOUT:
ToolbarLogout:
                 Logout();
                 ClearMainInputEvents(0);
                 ToolBarLogin(FALSE);
                 break;



            }
          if(running && signal) Wait(signal);
          }
        }

        Usu�Magazyn();
        Usu�Klient�w(TRUE);
        Usu�Jednostki(TRUE);
        Usu�U�ytkownik�w(TRUE);

No_LocalePL_Quit:
Create_Dirs_Problems_Quit:
Wrong_Date_Quit:
Nie_Zamykaj_Miesiaca:
        if(app) MUI_DisposeObject(app);
        }
      else
        {
        EasyRequest(NULL, &MyEasy, NULL, MSG_ERR_APP);
        CleanUp();
        return(EXIT_FAILURE);
        }

      DeleteClasses();
      }

   CleanUp();
   return(EXIT_SUCCESS);
}
//|

