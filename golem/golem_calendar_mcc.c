
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 


/*** includes ***/

#include <clib/alib_protos.h>
#include <clib/graphics_protos.h>
#include <clib/utility_protos.h>
#include <clib/muimaster_protos.h>
#include <pragmas/muimaster_pragmas.h>
#include <libraries/mui.h>
#if MUIMASTER_VLATEST <= 14
#include <mui/mui33_mcc.h>
#endif

#define MYDEBUG 0

//#include <mui/betterstring_mcc.h>
#include <mui/muiundoc.h>

#include "string.h"
#include "stdio.h"
#include "golem_structs.h"
#include "golem_macros.h"
#include "golem_protos.h"
#include "golem_calendar_mcc.h"
#include <mui/monthnavigator_mcc.h>
#include <mui/mui.h>


#define __NAME      "Golem_Calendar"
#define CLASS       MUIC_Golem_Calendar
#define SUPERCLASS  MUIC_Group

void _XCEXIT(int code) {}


struct Data
{
	Object *ST_Data;

};

#include "golem_calendar_revision.h"

#define UserLibID VERSTAG " � 1998-2002 Marcin Orlowski <carlos@wfmh.org.pl>"
#define MASTERVERSION 14

#define MCC_USES_IFFPARSE
#include "mccheader.c"

#define TITLE "WFMH Golem"


/// xget
LONG xget(Object *obj, ULONG attribute)
{
LONG x;

	get(obj, attribute, &x);
	return(x);
}
//|

/// OM_NEW

ULONG ASM _New(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) struct opSet *msg)
{
struct Data *data;

static char *months[] = { "Stycze�"    , "Luty"    , "Marzec",
						  "Kwiecie�"   , "Maj"     , "Czerwiec",
						  "Lipiec"     , "Sierpie�", "Wrzesie�",
						  "Pa�dziernik", "Listopad", "Grudzie�",
						  NULL
						};

//Object *app = (Object *)xget(obj, MUIA_ApplicationObject);
Object *CY_Month, *BT_Prev, *CY_Year, *BT_Next;


	D(bug("Setting up Calendar group...\n"));

	obj = (Object *)DoSuperNew(cl, obj,

					MUIA_Group_Horiz, FALSE,

					Child, HGroup,
						   GroupFrame,

						   Child, CY_Month = CycleObject,
								   MUIA_Cycle_Entries, months,
//                                   MUIA_Font, MUIV_Font_Button,
								   MUIA_CycleChain, TRUE,
								   End,


						   Child, BT_Prev = ImageObject,
									ButtonFrame,
								   MUIA_InputMode     , MUIV_InputMode_RelVerify,
								   MUIA_Image_Spec    , MUII_ArrowLeft,
								   MUIA_Image_FreeVert, TRUE,
								   MUIA_ShowSelState  , FALSE,
								   MUIA_CycleChain    , TRUE,
								   End,

						   Child, CY_Year = NumericbuttonObject,
								   MUIA_Numeric_Format, "%lu",
								   MUIA_Numeric_Min   , 1995,
								   MUIA_Numeric_Max   , 2005,
//                                   MUIA_Font          , MUIV_Font_Button,
								   MUIA_CycleChain    , TRUE,
								   End,

						   Child, BT_Next = ImageObject,
								   ButtonBack, ButtonFrame,
								   MUIA_InputMode     , MUIV_InputMode_RelVerify,
								   MUIA_Image_Spec    , MUII_ArrowRight,
								   MUIA_Image_FreeVert, TRUE,
								   MUIA_ShowSelState  , FALSE,
								   MUIA_CycleChain    , TRUE,
								   End,
						   End,

					Child, MonthNavigatorObject,
						   GroupFrame,
						   MUIA_MonthNavigator_InputMode, MUIV_MonthNavigator_InputMode_Immediate,
						   MUIM_MonthNavigator_Mark     , MUIV_MonthNavigator_MarkHook_HiToday,
						   End,


				   TAG_DONE);


		if( obj )
		   {
//           D(bug(" Calendar.mcc properly initialized...\n"));

		   /*** init data ***/
		   data = INST_DATA(cl, obj);


		   /*** trick to set arguments ***/
		   msg->MethodID = OM_SET;
		   DoMethodA(obj, (Msg)msg);
		   msg->MethodID = OM_NEW;
		   }
		else
		   {
		   D(bug(" Can't init Calendar.mcc\n"));
		   }


		return((ULONG)obj);
}
//|
/*
/// OM_SET

ULONG ASM _Set(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct Data *data = INST_DATA(cl,obj);
struct TagItem *tags,*tag;


	for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
	   {
	   switch(tag->ti_Tag)
		  {

//        DoMethod(data->ST_Numer, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, obj, 3, MUIM_Set, MUIA_BankString_Account, MUIV_TriggerValue);
/*
		  case MUIA_BankString_Account:
			   {
			   strncpy( data->Numer, (char *)tag->ti_Data, CUST_BANK_LEN );

			   DoMethod(obj, MUIM_BankString_Validate);

			   return(TRUE);
			   }
		   break;
*/

		   }
		}

	return(DoSuperMethodA(cl, obj, msg));
}

//|
*/
// SET

/// Dispatcher
ULONG ASM SAVEDS _Dispatcher(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{

	switch (msg->MethodID)
	   {
	   case OM_NEW: return(_New (cl, obj, (APTR)msg));
//       case OM_SET: return(_Set (cl, obj, (APTR)msg));
//       case OM_GET: return(_Get (cl, obj, (APTR)msg));

//       case MUIM_Calendar_OpenCalendar : return(_OpenCalendar (cl, obj, (APTR)msg));

	   }

	return((ULONG)DoSuperMethodA(cl,obj,msg));

}
//|

