#define BSTRING_VERSION 15
#define BSTRING_REVISION 1
#define BSTRING_VERSIONREVISION "15.1"
#define BSTRING_DATE "10.06.2002"
#define BSTRING_VERS "Golem_BString.mcc 15.1"
#define BSTRING_VSTRING "Golem_BString.mcc 15.1 (10.06.2002)\r\n"
#define BSTRING_VERSTAG "$VER: Golem_BString.mcc 15.1 (10.06.2002)"
