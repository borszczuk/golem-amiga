
/*
$Id: golem_prtlist_mcc.h,v 1.1 2003/01/01 20:40:52 carl-os Exp $.
*/

/*** Include stuff ***/


#ifndef GOLEM_PRTLIST_MCC_H
#define GOLEM_PRTLIST_MCC_H

#ifndef LIBRARIES_MUI_H
#include "libraries/mui.h"
#endif


/*** MUI Defines ***/

#define MUIC_Golem_PrtList  "Golem_PrtList.mcc"
#define MUIC_Golem_PrtListP "Golem_PrtList.mcp"
#define Golem_PrtListObject MUI_NewObject(MUIC_Golem_PrtList


/*** Methods ***/


/*** Method structs ***/


/*** Special method values ***/


/*** Special method flags ***/


/*** Attributes ***/


/*** Special attribute values ***/


/*** Structures, Flags & Values ***/


/*** Configs ***/


#endif /* GOLEM_PRTLIST_MCC_H */



