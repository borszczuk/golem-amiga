
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

#include "Golem.h"

//#ifdef BETA
//char beta_watermark_5[] = BETA_WATERMARK;
//#endif

struct List      zakupy;


/// ApplyZakFilter

void ApplyZakFilter(void)
{
struct ZakupList *zak;

    set(LV_Zak_Zakupy, MUIA_List_Quiet, TRUE);

    if(!IsListEmpty(&zakupy))
       {
       for(zak = (struct ZakupList *)zakupy.lh_Head; zak->za_node.mln_Succ; zak = (struct ZakupList *)zak->za_node.mln_Succ)
           {
           DoMethod(LV_Zak_Zakupy, MUIM_List_InsertSingle, zak, MUIV_List_Insert_Sorted);
           }
       }

    set(LV_Zak_Zakupy, MUIA_List_Quiet, FALSE);
}

//|
/// ZnajdzZakupLVAddr

/*
** szuka zakupu o podanym adresie na liscie LV
** zwraca -1 jesli nie znaleziono, lub numer
** linii na liscie ktora trzyma ten zakup
*/

int ZnajdzZakupLVAddr( struct ZakupList *zak )
{
struct ZakupList *tmp;
int index = -1;
int i;


    for(i=0; ; i++)
       {

       DoMethod(LV_Zak_Zakupy, MUIM_List_GetEntry, i, &tmp);

       if(!tmp || tmp == zak)
         break;
       }

    if(!tmp)
       return(-1);
    else
       return(i);
}
//|
/// DodajZakup

int DodajZakup(struct ZakupList *zak)
{
/*
** Dodaje zakup to listy zakup�w
*/

/*
struct ZakupList *node;
int i = 0;

    if(!IsListEmpty(&zakupy))
       {
       for(node = (struct ZakupList *)zakupy.lh_Head; node->za_node.mln_Succ; node = (struct ZakupList *)node->za_node.mln_Succ)
           {
           if(CompareDates(&zak->za_zakup.P3, &node->za_zakup.P3) < 0)
               {
               if((struct Node *)node->za_node.mln_Pred)
                   Insert(&zakupy, (struct Node *)zak, (struct Node *)node->za_node.mln_Pred);
               else
                   AddHead(&zakupy, (struct Node *)zak);

               return(i);
               }
           i++;
           }
       }
*/

       AddTail(&zakupy, (struct Node *)zak);
       return(MUIV_List_Insert_Bottom);
}
//|
/// Usu�Zakupy
void Usu�Zakupy(char Skasowa�Wszystko)
{
/*
** Usuwa wszystkie grupy magazynowe
** oraz znajdujace si� w nich produkty
**
** Jesli SkasowacWszystko == FALSE, usuwa tylko elementy zaznaczone jako skasowane
*/

struct ZakupList *zak, *zak_next;

    D(bug("UsunZakupy()\n"));

    set(LV_Zak_Zakupy, MUIA_List_Quiet, TRUE);

    if(!IsListEmpty(&zakupy))
       {
       for(zak = (struct ZakupList *)zakupy.lh_Head; zak->za_node.mln_Succ; )
           {
           char del = zak->Deleted;

           if(Skasowa�Wszystko)
               del = TRUE;

           zak_next = (struct ZakupList *)zak->za_node.mln_Succ;

           if(del)
               {
               int i = ZnajdzZakupLVAddr(zak);

               D(bug(" Removing 0x%lx (#%ld)\n", zak, i));

               if(i != -1)
                 DoMethod(LV_Zak_Zakupy, MUIM_List_Remove, i);
               else
                 {
                 DisplayBeep(0);
                 D(bug("** Can't find entry 0x%lx (#%ld)\n", zak, i));
                 }

               Remove((struct Node*)zak);
               free(zak);
               }
           zak = zak_next;

           }
       }

/*
    if(Skasowa�Wszystko)
           {
           NewList(&zakupy);
//           DoMethod(LV_Zak_Zakupy, _MUIM_List_Clear, NULL);
           }
*/

    set(LV_Zak_Zakupy, MUIA_List_Quiet, FALSE);


}
//|

/// ZAK STOP CHUNKS
#define ZAK_NUM_STOPS (sizeof(Zak_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Zak_Stops[] =
{
        ID_ZAK, ID_CAT,
        ID_ZAK, ID_VERS,
        ID_ZAK, ID_ZAK,
        NULL, NULL,
};
//|
/// WczytajZakupy

int WczytajZakupy(char *FileName)
{

struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
char   ValidFile = FALSE;
int    Current = 0;                   // liczba wczytanych produkt�w
int    CurrentGauge = 1;

int    Errors = 0;
int    Error_Products = 0;            // produkty do kt�rych nie znaleziono grup
int    Error_Skipped_Groups = 0;      // grupy kt�re nie zosta�y wczytane
int    Error_Skipped_Products = 0;    // produkty kt�re nie zosta�y wczytane
int    Error_Duplicated_Groups = 0;   // powtarzaj�ce si� definicje grup

#ifndef DEMO

     set(app, MUIA_Application_Sleep, TRUE);

//     Usu�Zakupy(TRUE);

     if(iff = AllocIFF())
       {
       if(iff->iff_Stream = Open(FileName, MODE_OLDFILE))
           {
           InitIFFasDOS(iff);

           StopChunks(iff, Zak_Stops, ZAK_NUM_STOPS);
//           StopOnExit(iff, ID_MAG, ID_FORM);

           if(!OpenIFF(iff, IFFF_READ))
               {
               while(TRUE)
                  {
                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                     break;

                  if(cn = CurrentChunk(iff))
                     {
                     LONG ID = cn->cn_ID;

                     if(!ValidFile)
                        {
                        if((ID == ID_CAT) && (cn->cn_Type == ID_ZAK))
                           {
                           ValidFile = TRUE;
                           continue;
                           }

                        break;
                        }

///                       ID_VERS
                     if(ID == ID_VERS)
                        {
                        struct BaseVersion version;

                        if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                           {

                           }
                        else
                           {
                           _RC = IoErr();
                           Errors++;
                           break;
                           }

                        continue;
                        }
//|
///                       ID_ZAK

                     if(ID == ID_ZAK)
                        {
                        struct ZakupList *zak  = calloc(1, sizeof(struct ZakupList));

                        if(zak)
                           {
                           if(ReadChunkBytes(iff, &zak->za_zakup, sizeof(struct Zakup)) != cn->cn_Size)
                              {
                              Error_Skipped_Products++;
                              free(zak);
//                              _RC = IoErr();
                              Errors++;
                              continue;
                              }

                           DodajZakup(zak);
                           }

                        continue;
                        }
//|

                     }
                  }

               CloseIFF(iff);
               }

            Close(iff->iff_Stream);
            }

//       if(_RC == IFFERR_EOF) _RC = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

       FreeIFF(iff);
       }


       set(app, MUIA_Application_Sleep, FALSE);

#endif

/*
       if(Error_Products + Error_Skipped_Products + Error_Duplicated_Groups)
           {
           DisplayBeep(0);
           MUI_Request(app, NULL, 0, TITLE, MSG_OK, MSG_ERR_LOAD_STOCK, Error_Skipped_Products, Error_Skipped_Groups, Error_Products, Error_Duplicated_Groups, Current);
           }
*/
       return(Error_Products + Error_Skipped_Products + Error_Duplicated_Groups + Error_Skipped_Groups);

}

//|
/// ZapiszZakupy

int ZapiszZakupy(char *FileName)
{
#ifndef DEMO

struct IFFHandle *MyIFFHandle;
long   Count = 0;

    set(app, MUIA_Application_Sleep, TRUE);

    if(MyIFFHandle = AllocIFF())
        {
        BPTR  FileHandle;

        if(FileHandle = Open(FileName, MODE_NEWFILE))
           {
           MyIFFHandle->iff_Stream = FileHandle;
           InitIFFasDOS(MyIFFHandle);

           if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
               {
               struct BaseVersion version;
               struct ZakupList *node;


               PushChunk(MyIFFHandle, ID_ZAK, ID_CAT, IFFSIZE_UNKNOWN);

               PushChunk(MyIFFHandle, ID_ZAK, ID_FORM, IFFSIZE_UNKNOWN);
                   PushChunk(MyIFFHandle, ID_ZAK, ID_VERS, IFFSIZE_UNKNOWN);
                   version.Version = VERSION;
                   version.Revision = REVISION;
                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                   PopChunk(MyIFFHandle);

               PopChunk(MyIFFHandle);

               for(node = (struct ZakupList *)zakupy.lh_Head; node->za_node.mln_Succ; node = (struct ZakupList *)node->za_node.mln_Succ)
                     {
                     PushChunk(MyIFFHandle, ID_ZAK, ID_FORM, IFFSIZE_UNKNOWN);

                      PushChunk(MyIFFHandle, ID_ZAK, ID_ZAK, IFFSIZE_UNKNOWN);
                      WriteChunkBytes(MyIFFHandle, &node->za_zakup, sizeof(struct Zakup));
                      PopChunk(MyIFFHandle);

                     PopChunk(MyIFFHandle);

                     Count++;
                     }

               PopChunk(MyIFFHandle);
               CloseIFF(MyIFFHandle);

//               ZapiszCount(ZakupyCountFileName, Count);
               }
           else
               {
               DisplayBeep(0);
               D(bug("*** OpenIFF() nie powiod�o si�\n"));
               }

           Close(FileHandle);
           }
        else
           {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
           D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", FileName));
           }

        FreeIFF(MyIFFHandle);
        }
     else
        {
        DisplayBeep(0);
        D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
        }

    set(app, MUIA_Application_Sleep, FALSE);

#endif

    return(0);
}



//|


/// IndexRequesterSetup
void IndexRequesterSetup(long def_value)
{

    if(!def_value)
       setstring(ST_Idx_String, "0");
    else
       set(ST_Idx_String, MUIA_String_Integer, def_value);


    set(IndexRequesterWindow, MUIA_Window_ActiveObject, ST_Idx_String);
}
//|
/// IndexRequesterFinish
long IndexRequesterFinish(void)
{
/*
** zwraca 0 jesli cancel (nie moze byc zero jaki numer indeksu
** zwraca INT jesli ok.
*/

long result = TRUE;

    get(ST_Idx_String, MUIA_String_Integer, &result);

    return(result);
}
//|
/// IndexRequester

long IndexRequester(long def_value)
{
/*
*/

char  running = TRUE;
ULONG signal  = 0;
long  result  = FALSE;


    set(app, MUIA_Application_Sleep, TRUE);

    IndexRequesterSetup(def_value);

    if(WinOpen(IndexRequesterWindow))
        {
        while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);

          switch(ID)
            {
            case ID_OK:
               result = IndexRequesterFinish();

               // kicha, nie moze byc index=0 -> CANCEL
               if(!result)
                   DisplayBeep(0);
               else
                   running = FALSE;

               break;


            case ID_CANCEL:
               result = 0;
               running = FALSE;
               break;
            }
          if(running && signal) Wait(signal);
          }

        set(IndexRequesterWindow, MUIA_Window_Open, FALSE);
        }
     else
        {
        DisplayBeep(0);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        }

    set(app, MUIA_Application_Sleep, FALSE);

    return(result);
}

//|

/// InformacjeOZakupach

char InformacjeOZakupach(void)
{
/*
*/

char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

int    Count = 0;
double P8, P9, P10, P11, P12, P13, P14, P15, P16, P17, P18;

    set(app, MUIA_Application_Sleep, TRUE);


    // Policzmy co trzeba...

    P8 = P9 = P10 = P11 = P12 = P13 = P14 = P15 = P16 = P17 = P18 = 0;


    if(!IsListEmpty(&zakupy))
       {
       struct ZakupList *zakup;

       for(zakup = (struct ZakupList *)zakupy.lh_Head; zakup->za_node.mln_Succ; zakup=(struct ZakupList *)zakup->za_node.mln_Succ)
           {
           struct Zakup *zak = &zakup->za_zakup;
           double vat22, vat7;

           Count++;

           vat7  = CalcVat(zak->P12 + zak->P16, VAT_7);
           vat22 = CalcVat(zak->P10 + zak->P14, VAT_22);

           P8 += zak->P9 + zak->P10 + zak->P12 + zak->P14 + zak->P16 + vat7 + vat22;
           P18 += vat7 + vat22;

           P9 += zak->P9;

           P10 += zak->P10;
           P11 += CalcVat(zak->P10, VAT_22);
           P12 += zak->P12;
           P13 += CalcVat(zak->P12, VAT_7);

           P14 += zak->P14;
           P15 += CalcVat(zak->P14, VAT_22);
           P16 += zak->P16;
           P17 += CalcVat(zak->P16, VAT_7);
           }
       }           

    // ...i �adnie wy�wietlmy...

    settext(TX_ZakI_Count, wfmh_itoa(Count));

    settext(TX_ZakI_P8 , Price2String(P8));
    settext(TX_ZakI_P9 , Price2String(P9));
    settext(TX_ZakI_P10, Price2String(P10));
    settext(TX_ZakI_P11, Price2String(P11));
    settext(TX_ZakI_P12, Price2String(P12));
    settext(TX_ZakI_P13, Price2String(P13));
    settext(TX_ZakI_P14, Price2String(P14));
    settext(TX_ZakI_P15, Price2String(P15));
    settext(TX_ZakI_P16, Price2String(P16));
    settext(TX_ZakI_P17, Price2String(P17));
    settext(TX_ZakI_P18, Price2String(P18));


    // ... i niech si� ludziska ciesz�...

    set(InformacjeOZakupachWindow, MUIA_Window_ActiveObject, BT_ZakI_Ok);

    if(WinOpen(InformacjeOZakupachWindow))
        {
        while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);

          switch(ID)
            {
            case ID_OK:
            case ID_CANCEL:
               running = FALSE;
               break;
            }
          if(running && signal) Wait(signal);
          }

        set(InformacjeOZakupachWindow, MUIA_Window_Open, FALSE);
        }
     else
        {
        DisplayBeep(0);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        }

    set(app, MUIA_Application_Sleep, FALSE);

    return(result);
}

//|

/// EdycjaZakupuSetup
void EdycjaZakupuSetup(struct ZakupList *zakup)
{
struct Zakup *zak = &zakup->za_zakup;
char adres[CUST_ADRES_LEN + CUST_KOD_LEN + CUST_MIASTO_LEN + 1];

    if(zak->P1 != 0)
       set(ST_ZakE_P1, MUIA_String_Integer, zak->P1);
    else
       setstring(ST_ZakE_P1, "");

    setstring(ST_ZakE_P2, zak->P2);

    if(zak->P3.ds_Days == 0)
       setstring(ST_ZakE_P3, wfmh_Date2Str(&Today));
    else
       setstring(ST_ZakE_P3, wfmh_Date2Str(&zak->P3));

    if(zak->P4.ds_Days == 0)
       setstring(ST_ZakE_P4, wfmh_Date2Str(&Today));
    else
       setstring(ST_ZakE_P4, wfmh_Date2Str(&zak->P4));



    if(zak->Sprzedawca.NIP[0] != 0)
       settext(TX_ZakE_P5, NIP2Str(zak->Sprzedawca.NIP, zak->Sprzedawca.Sp��kaCywilna));
    else
       if(zak->Sprzedawca.Regon[0] != 0)
         settext(TX_ZakE_P5, zak->Sprzedawca.Regon);
       else
         settext(TX_ZakE_P5, "");

    settext(TX_ZakE_P6, zak->Sprzedawca.Nazwa1);
    sprintf(adres, "%s, %s %s", zak->Sprzedawca.Ulica, zak->Sprzedawca.Kod, zak->Sprzedawca.Miasto);
    settext(TX_ZakE_P7, adres);

    setstring(ST_ZakE_P9 , Price2String(zak->P9));
    setstring(ST_ZakE_P10, Price2String(zak->P10));
    setstring(ST_ZakE_P12, Price2String(zak->P12));
    setstring(ST_ZakE_P14, Price2String(zak->P14));
    setstring(ST_ZakE_P16, Price2String(zak->P16));

    settext(TX_ZakE_P11, Price2String(CalcVat(String2Price(getstr(ST_ZakE_P10)), VAT_22)));
    settext(TX_ZakE_P13, Price2String(CalcVat(String2Price(getstr(ST_ZakE_P12)), VAT_7)));
    settext(TX_ZakE_P15, Price2String(CalcVat(String2Price(getstr(ST_ZakE_P14)), VAT_22)));
    settext(TX_ZakE_P17, Price2String(CalcVat(String2Price(getstr(ST_ZakE_P16)), VAT_7)));

    setstring(ST_ZakE_Opis, (zak->Opis));

    set(GR_ZakE_Pages, MUIA_Group_ActivePage, 0);
    set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_P1);
}
//|
/// EdycjaZakupuFinish
char EdycjaZakupuFinish(struct ZakupList *zakup, char EditMode)
{
/*
** sprawdza poprawno�� wprowadzonych danych dot. produktu. Zwraca FALSE,
** je�li co� jest nie tak, TRUE, je�li wszystkie elementy s� poprawne,
** oraz REFRESH, je�li nazwa produktu zosta�a zmieniona
** (istotne tylko podczas edycji produktu)
**
** Je�li EditMode == TRUE, produkt jest edytowany, zatem mo�na go usun�� z
** listy przed poszukiwaniem duplikatu
*/

struct Zakup *zak = &zakup->za_zakup;
char   result = TRUE;
struct DateStamp *ds;

/*
    if(!(strlen((char *)xget(ST_ZakE_P1, MUIA_String_Contents))) )
       {
       set(GR_ZakE_Pages, MUIA_Group_ActivePage, 0);
       set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_P1);
       return(FALSE);
       }
*/
    if(!(strlen((char *)xget(ST_ZakE_P2, MUIA_String_Contents))) )
       {
       set(GR_ZakE_Pages, MUIA_Group_ActivePage, 0);
       set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_P2);
       return(FALSE);
       }

    if(!(ds = wfmh_Str2Date((char *)xget(ST_ZakE_P3, MUIA_String_Contents))))
       {
       set(GR_ZakE_Pages, MUIA_Group_ActivePage, 0);
       set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_P3);
       return(FALSE);
       }

    if(!(ds = wfmh_Str2Date((char *)xget(ST_ZakE_P4, MUIA_String_Contents))))
       {
       set(GR_ZakE_Pages, MUIA_Group_ActivePage, 0);
       set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_P4);
       return(FALSE);
       }


    get(ST_ZakE_P1, MUIA_String_Integer, &zak->P1);
    copystr(zak->P2, ST_ZakE_P2);
    memcpy(&zak->P3, wfmh_Str2Date((char *)xget(ST_ZakE_P3, MUIA_String_Contents)), sizeof(struct DateStamp));
    memcpy(&zak->P4, wfmh_Str2Date((char *)xget(ST_ZakE_P4, MUIA_String_Contents)), sizeof(struct DateStamp));

    zak->P9  = String2Price(getstr(ST_ZakE_P9));
    zak->P10 = String2Price(getstr(ST_ZakE_P10));
    zak->P12 = String2Price(getstr(ST_ZakE_P12));
    zak->P14 = String2Price(getstr(ST_ZakE_P14));
    zak->P16 = String2Price(getstr(ST_ZakE_P16));

    copystr(zak->Opis, ST_ZakE_Opis);

//    if(StrnCmp(MyLocale, Product->Nazwa, (char *)xget(ST_Mag_Nazwa, MUIA_String_Contents), -1, SC_COLLATE2) != 0)
       result = REFRESH;

    return(result);
}
//|
/// EdycjaZakupu

char EdycjaZakupu(struct ZakupList *zakup, char EditMode)
{
/*
** G�owna p�tla edycji produktu
** Zwraca to co EdycjaZakupuFinish()
** b�d� FALSE je�li przerwano edycj�
**
** Je�li EditMode == TRUE, to edytujemy
** istniej�cy ju� produkt
**
** Robimy kopi�, bo �atwiej miesza� w danych SPRZEDAWCA
** jak cancel, to copy back starocie i ju�.
*/

char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;
struct Zakup  zak_tmp = {0};

    set(app, MUIA_Application_Sleep, TRUE);

    memcpy(&zak_tmp, &zakup->za_zakup, sizeof(struct Zakup));
    EdycjaZakupuSetup(zakup);

    if(WinOpen(EdycjaZakupuWindow))
        {
        while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);

          switch(ID)
            {
            case ID_PAGE_0:
               set(GR_ZakE_Pages, MUIA_Group_ActivePage, ID - ID_PAGE_0);
               set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_P1);
               break;

            case ID_PAGE_1:
               set(GR_ZakE_Pages, MUIA_Group_ActivePage, ID - ID_PAGE_0);
               set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_P9);
               break;

            case ID_PAGE_2:
               set(GR_ZakE_Pages, MUIA_Group_ActivePage, ID - ID_PAGE_0);
               set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_P14);
               break;

            case ID_ZAKE_CALC_P11:
               settext(TX_ZakE_P11, Price2String(CalcVat(String2Price(getstr(ST_ZakE_P10)), VAT_22)));
               break;
            case ID_ZAKE_CALC_P13:
               settext(TX_ZakE_P13, Price2String(CalcVat(String2Price(getstr(ST_ZakE_P12)), VAT_7)));
               break;
            case ID_ZAKE_CALC_P15:
               settext(TX_ZakE_P15, Price2String(CalcVat(String2Price(getstr(ST_ZakE_P14)), VAT_22)));
               break;
            case ID_ZAKE_CALC_P17:
               settext(TX_ZakE_P17, Price2String(CalcVat(String2Price(getstr(ST_ZakE_P16)), VAT_7)));
               break;



            case ID_KLIENT_WYBIERZ:
               {
               struct Filtr sel_cust_filter =
                   {
                   TRUE,
                   "#?",              // Nazwa

                   1,                 // Sprzedawca
                   0,                 // Nabywca
                   1,                 // Kosztowiec
                   0,                 // A
                   0,                 // B
                   0                  // C
                   };

               struct KlientList *klientList = KontrahentSelector(&sel_cust_filter, NULL, NULL);

               if(klientList)
                   {
                   struct Klient *klient = &klientList->kl_klient;

                   char adres[CUST_ADRES_LEN + CUST_KOD_LEN + CUST_MIASTO_LEN + 1];

                   memcpy(&zakup->za_zakup.Sprzedawca, klient, sizeof(struct Klient));

                   if(klient->NIP[0] != 0)
                       settext(TX_ZakE_P5, NIP2Str(klient->NIP, klient->Sp��kaCywilna));
                   else
                       if(klient->Regon[0] != 0)
                           settext(TX_ZakE_P5, klient->Regon);
                       else
                           settext(TX_ZakE_P5, "");

                   settext(TX_ZakE_P6, klient->Nazwa1);
                   sprintf(adres, "%s, %s %s", klient->Ulica, klient->Kod, klient->Miasto);
                   settext(TX_ZakE_P7, adres);

                   set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, BT_ZakE_Next1);
                   }
               }
               break;



            case ID_OK:
               result = EdycjaZakupuFinish(zakup, EditMode);

               if(result == FALSE)
                  {
                  DisplayBeep(0);
                  }
               else
                  {
                  running = FALSE;
                  }
               break;


            case ID_CANCEL:
               if(EditMode)
                   memcpy(&zakup->za_zakup, &zak_tmp, sizeof(struct Zakup));

               running = FALSE;
               break;
            }
          if(running && signal) Wait(signal);
          }

        set(EdycjaZakupuWindow, MUIA_Window_Open, FALSE);
        }
     else
        {
        DisplayBeep(0);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        }

    set(app, MUIA_Application_Sleep, FALSE);

    return(result);
}

//|

/// Uporz�dkujZakupy

/*
** Uk�adamy exec'ow� list� zakup�w
** w takiej kolejno�ci w jakiej jest wy�wietlona
** na ekranie li�cie lv
*/

void Uporz�dkujZakupy(Object *lv)
{
struct ZakupList *zak;
int    i;

    NewList(&zakupy);

    for(i = 0; ; i++)
       {
       DoMethod(lv, MUIM_List_GetEntry, i, &zak);

       if(!zak)
           break;

       AddTail(&zakupy, (struct Node *)zak);
       }

}

//|

/// Zakupy

//#define FAK_ZMIANY(x)  {ZmianyNaFakturze += x; set(BT_Fak_Ok, MUIA_Disabled, (ZmianyNaFakturze == 0)); set(BT_Fak_Usu�, MUIA_Disabled, (ZmianyNaFakturze == 0)); }


#define ZAK_ZMIANY(x)  {ZmianyWZakupach += x; set(BT_Zak_Ok, MUIA_Disabled, (ZmianyWZakupach == 0));}

char Zakupy(void)
{
//static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };

struct DocumentPrint dp={0};
struct StringRequest paragon_req = {0};       // je�li dokument jest do paragonu, tutaj jest
                                              // numer paragonu wpisany przez u�ytkownika

char   running                       = TRUE;
ULONG  signal                        = 0;
char   result                        = FALSE;
char   temp_prod_name[PROD_NAME_LEN] = "";
char   data_str[]                    = "00.00.0000";
struct DateStamp datestamp;
long   ZmianyWZakupach = 0;


    NewList(&zakupy);


    // nie jest �le... go�! ;-)

    set(app, MUIA_Application_Sleep, TRUE);

/*
    // aktualny miesiac i rok
    {
    char _data[2+1+4+1];  // mm yyyy\0
    long _mc;
    long _rok;

    DateStamp(&datestamp);
    FormatDateHook.h_Data = _data;
    FormatDate(MyLocale, "%m %Y", &datestamp, &FormatDateHook);
    sscanf(_data, "%ld %ld", &_mc, &_rok);

    dp.Miesiac = _mc;
    dp.Rok = _rok;
    }
*/


// Setting up

    set(ZakupyWindow, MUIA_Window_DefaultObject, LV_Zak_Zakupy);
    set(ZakupyWindow, MUIA_Window_ActiveObject , LV_Zak_Zakupy);


//  Nag��wek i numer dokumentu                  

    if(WinOpen(ZakupyWindow))
       {
       while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);
          switch(ID)
            {
            case ID_INFO:
               InformacjeOZakupach();
               break;

            case ID_ADD:
               {
               struct ZakupList *zak;

               zak = calloc(1, sizeof(struct ZakupList));

               if(zak)
                  {
                  zak->Deleted = FALSE;

                  if(EdycjaZakupu(zak, FALSE))
                     {
                     long pos = DodajZakup(zak);

                     DoMethod(LV_Zak_Zakupy, _MUIM_List_InsertSingle, zak, pos);
                     set(LV_Zak_Zakupy, _MUIA_List_Active, pos);
                     ZAK_ZMIANY(1)
                     }
                  else
                     {
                     free(zak);
                     }
                  }
               else
                  {
                  DisplayBeep(0);
                  MUI_Request(app, ZakupyWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                  }
               }
               break;


            case ID_EDIT:
               {
               if(xget(LV_Zak_Zakupy, _MUIA_List_Active) != _MUIV_List_Active_Off)
                   {
                   struct ZakupList *zak;

                   DoMethod(LV_Zak_Zakupy, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &zak);

                   if(zak->Deleted)
                     {
                     if(MUI_Request(app, ZakupyWindow, 0, TITLE, MSG_ZAK_DELETED_GAD , MSG_ZAK_DELETED, zak->za_zakup.P2))
                       {
                       zak->Deleted = FALSE;
                       DoMethod(LV_Zak_Zakupy, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                       ZAK_ZMIANY(-1)
                       }
                     else
                       break;
                     }

                   switch(EdycjaZakupu(zak, TRUE))
                     {
                     case TRUE:
                         DoMethod(LV_Zak_Zakupy, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                         ZAK_ZMIANY(1)
                         break;

                     case REFRESH:
                         {
                         long pos;
                         set(LV_Zak_Zakupy, _MUIA_List_Quiet, TRUE);

                         DoMethod(LV_Zak_Zakupy, _MUIM_List_Remove, _MUIV_List_Remove_Active);

                         Remove((struct Node*)zak);
                         pos = DodajZakup(zak);

                         DoMethod(LV_Zak_Zakupy, _MUIM_List_InsertSingle, zak, pos);
                         set(LV_Zak_Zakupy, _MUIA_List_Active, pos);

                         set(LV_Zak_Zakupy, _MUIA_List_Quiet, FALSE);
                         ZAK_ZMIANY(1)
                         }
                         break;
                     }
                   }
               }
               break;


            case ID_DELETE:
               {
               if(xget(LV_Zak_Zakupy, _MUIA_List_Active) != _MUIV_List_Active_Off)
                   {
                   struct ZakupList *zak;

                   DoMethod(LV_Zak_Zakupy, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &zak);

                   zak->Deleted = ((zak->Deleted + 1) & 0x1);

                   if(zak->Deleted)
                       ZAK_ZMIANY(1)
                   else
                       ZAK_ZMIANY(-1)

                   DoMethod(LV_Zak_Zakupy, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);

                   if(ID == ID_MAG_SKASUJ_AND_SKIP)
                       set(LV_Zak_Zakupy, _MUIA_List_Active, _MUIV_List_Active_Down);
                   }
               }
               break;



            /*                         */
            /*    Disk I/O             */
            /*                         */

            case ID_LOAD:
               {
//               static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };
               char   FileName[80];
               char   Miesiac[3];
               char   Rok[5];
               char   result = TRUE;
               int    miesiac;

               // kasujemy aktualna list�

               Usu�Zakupy(TRUE);


               // katalogi dla dokument�w

               miesiac = getcycle(CY_Zak_Miesiac);
               if(miesiac == 0)
                   {
                   wfmh_Date2StrFmtBuffer( Miesiac, "%m", &Today );
//                   FormatDateHook.h_Data = Miesiac;
//                   FormatDate(MyLocale, "%m", &Today, &FormatDateHook);
                   }
               else
                   {
                   sprintf(Miesiac, "%02ld", miesiac);
                   }

               wfmh_Date2StrFmtBuffer( Rok, "%Y", &Today );
//               FormatDateHook.h_Data = Rok;
//               FormatDate(MyLocale, "%Y", &Today, &FormatDateHook);

               sprintf(FileName, "%s/%s/%s/%02lx/%s", DatabasesDir, Rok, Miesiac, ZAKUPY, ZakupyFileName);

               WczytajZakupy(FileName);

               ApplyZakFilter();
               }
               break;


            case ID_SAVE:
               {
//               static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };
               char   FileName[80];
               char   Miesiac[3];
               char   Rok[5];
               char   result = TRUE;
               int    miesiac;

               // kasujemy DELETED

               Usu�Zakupy(FALSE);

               // katalogi dla dokument�w

               miesiac = getcycle(CY_Zak_Miesiac);
               if(miesiac == 0)
                   {
                   wfmh_Date2StrFmtBuffer( Miesiac, "%m", &Today );
//                   FormatDateHook.h_Data = Miesiac;
//                   FormatDate(MyLocale, "%m", &Today, &FormatDateHook);
                   }
               else
                   {
                   sprintf(Miesiac, "%02ld", miesiac);
                   }

               wfmh_Date2StrFmtBuffer( Rok, "%Y", &Today );
//               FormatDateHook.h_Data = Rok;
//               FormatDate(MyLocale, "%Y", &Today, &FormatDateHook);

               sprintf(FileName, "%s/%s/%s/%02lx/%s", DatabasesDir, Rok, Miesiac, ZAKUPY, ZakupyFileName);

               ZapiszZakupy(FileName);
               }
               break;


            /*                         */
            /*    Misc                 */
            /*                         */

            case ID_INDEX:
               {
               LONG  entries = xget(LV_Zak_Zakupy, MUIA_List_Entries);
               long  index = 1;

               if(entries)
                   {

                   _sleep(TRUE);

                   if(index = IndexRequester(index))
                      {
                      struct ZakupList *node;
                      int    i;

                      set(LV_Zak_Zakupy, MUIA_List_Quiet, TRUE);

                      for(i = 0; ; i++)
                         {
                         DoMethod(LV_Zak_Zakupy, MUIM_List_GetEntry, i, &node);

                         if(!node)
                           break;

                         node->za_zakup.P1 = index++;
                         }

                      // prze��czamy na sortowanie po numerach
                      set(LV_Zak_Zakupy, MUIA_ZakupyList_Order, 0);
                      DoMethod(LV_Zak_Zakupy, MUIM_List_Sort);
                      Uporz�dkujZakupy(LV_Zak_Zakupy);

                      set(LV_Zak_Zakupy, MUIA_List_Quiet, FALSE);
                      }

                   _sleep(FALSE);

                   }
               else
                   {
                   DisplayBeep(0);
                   }              
               }
               break;




            case ID_SORT:
               {
               LONG  entries = xget(LV_Zak_Zakupy, MUIA_List_Entries);
               int   order = 1;

               if(entries)
                   {

                   _sleep(TRUE);

                   order = SortRequester(SORT_ZAKUPY, xget(LV_Zak_Zakupy, MUIA_ZakupyList_Order));
                   if(order != -1L)
                      {
                      set(LV_Zak_Zakupy, MUIA_ZakupyList_Order, order);
                      DoMethod(LV_Zak_Zakupy, MUIM_List_Sort);
                      Uporz�dkujZakupy(LV_Zak_Zakupy);
                      }

                   _sleep(FALSE);

                   }
               else
                   {
                   DisplayBeep(0);
                   }
               }
               break;


            /*                         */
            /*    Ko�czymy edycj�...   */
            /*                         */

            case ID_OK:
               {
               result  = TRUE;
               running = FALSE;
               }
               break;


            case ID_CANCEL:
/*
               if(ZmianyNaFakturze)
                   {
                   if(!MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_CANCEL_REQ_GAD, MSG_FAK_CANCEL_REQ))
                     break;

                   WczytajMagazyn();
                   }
*/
               running = FALSE;
               break;
            }
          if(running && signal) Wait(signal);
          }
       }
    else
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       }

    set(ZakupyWindow, MUIA_Window_Open, FALSE);

    Usu�Zakupy(TRUE);

    set(app, MUIA_Application_Sleep, FALSE);

    return(result);

}
//|



/*
/// InformacjeOZakupach

char InformacjeOZakupach(void)
{
/*
*/

char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

int    Count = 0;
double P7, P8, P10, P11, P12, P13, P14, P15, P16;

    set(app, MUIA_Application_Sleep, TRUE);


    // Policzmy co trzeba...

    P7 = P8 = P10 = P11 = P12 = P13 = P14 = P16 = 0;


    if(!IsListEmpty(&zakupy))
       {
       struct ZakupList *zakup;

       for(zakup = (struct ZakupList *)zakupy.lh_Head; zakup->za_node.mln_Succ; zakup=zakup->za_node.mln_Succ)
           {
           struct Zakup *zak = &zakup->za_zakup;

           Count++;

           P7 += zak->Warto��SprzedanychTowar�w;
           P8 += zak->Pozosta�ePrzychody;

           P10 += zak->ZakupTowar�wHandlowych;
           P11 += zak->KosztyUboczneZakupu;

           P12 += zak->KosztyReprezentacjiIReklamy;
           P13 += zak->Wynagrodzenia;
           P14 += zak->Pozosta�eWydatki;

           P16 += zak->P16;
           }
       }

    // ...i �adnie wy�wietlmy...

    set(TX_ZakI_Count, MUIA_String_Integer, Count);

    settext(TX_ZakI_P7, Price2String(P7));
    settext(TX_ZakI_P8, Price2String(P8));
    settext(TX_ZakI_P9, Price2String(P7 + P8));

    settext(TX_ZakI_P10, Price2String(P10));
    settext(TX_ZakI_P11, Price2String(P11));

    settext(TX_ZakI_P12, Price2String(P12));
    settext(TX_ZakI_P13, Price2String(P13));
    settext(TX_ZakI_P14, Price2String(P14));
    settext(TX_ZakI_P15, Price2String(P11 + P12 + P13 + P14));

    settext(TX_ZakI_P16, Price2String(P16));


    // ... i niech si� ludziska ciesz�...

    set(InformacjeOZakupachWindow, MUIA_Window_ActiveObject, BT_ZakI_Ok);

    if(WinOpen(InformacjeOZakupachWindow))
        {
        while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);

          switch(ID)
            {
            case ID_OK:
            case ID_CANCEL:
               running = FALSE;
               break;
            }
          if(running && signal) Wait(signal);
          }

        set(InformacjeOZakupachWindow, MUIA_Window_Open, FALSE);
        }
     else
        {
        DisplayBeep(0);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        }

    set(app, MUIA_Application_Sleep, FALSE);

    return(result);
}

//|

/// EdycjaZakupuSetup
void EdycjaZakupuSetup(struct ZakupList *zakup)
{
struct Zakup *zak = &zakup->za_zakup;

    if(zak->LP != 0)
       set(ST_ZakE_LP, MUIA_String_Integer, zak->LP);
    else
       setstring(ST_ZakE_LP, "");

    if(zak->DataZdarzenia.ds_Days == 0)
       setstring(ST_ZakE_Data, Date2Str(&Today));
    else
       setstring(ST_ZakE_Data, Date2Str(&zak->DataZdarzenia));

    setstring(ST_ZakE_Numer, zak->NumerDowoduKsi�gowego);
    setstring(ST_ZakE_Opis, zak->OpisZdarzenia);

    setstring(ST_ZakE_P7 , Price2String(zak->Warto��SprzedanychTowar�w));
    setstring(ST_ZakE_P8 , Price2String(zak->Pozosta�ePrzychody));
    setstring(ST_ZakE_P10, Price2String(zak->ZakupTowar�wHandlowych));
    setstring(ST_ZakE_P11, Price2String(zak->KosztyUboczneZakupu));

    setstring(ST_ZakE_P12, Price2String(zak->KosztyReprezentacjiIReklamy));
    setstring(ST_ZakE_P13, Price2String(zak->Wynagrodzenia));
    setstring(ST_ZakE_P14, Price2String(zak->Pozosta�eWydatki));

    setstring(ST_ZakE_P16, Price2String(zak->P16));
    setstring(ST_ZakE_P17, zak->Uwagi);

    set(GR_ZakE_Pages, MUIA_Group_ActivePage, 0);
    set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_LP);
}
//|
/// EdycjaZakupuFinish
char EdycjaZakupuFinish(struct ZakupList *zakup, char EditMode)
{
/*
** sprawdza poprawno�� wprowadzonych danych dot. produktu. Zwraca FALSE,
** je�li co� jest nie tak, TRUE, je�li wszystkie elementy s� poprawne,
** oraz REFRESH, je�li nazwa produktu zosta�a zmieniona
** (istotne tylko podczas edycji produktu)
**
** Je�li EditMode == TRUE, produkt jest edytowany, zatem mo�na go usun�� z
** listy przed poszukiwaniem duplikatu
*/

struct Zakup *zak = &zakup->za_zakup;
char   result = TRUE;
struct DateStamp *ds;

    if(!(strlen((char *)xget(ST_ZakE_LP, MUIA_String_Contents))) )
       {
       set(GR_ZakE_Pages, MUIA_Group_ActivePage, 0);
       set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_LP);
       return(FALSE);
       }
    if(!(strlen((char *)xget(ST_ZakE_Numer, MUIA_String_Contents))) )
       {
       set(GR_ZakE_Pages, MUIA_Group_ActivePage, 0);
       set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_Numer);
       return(FALSE);
       }

    if(!(strlen(getstr(ST_ZakE_Opis))))
       {
       set(GR_ZakE_Pages, MUIA_Group_ActivePage, 1);
       set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_Opis);
       return(FALSE);
       }


    if(!(ds = Str2Date((char *)xget(ST_ZakE_Data, MUIA_String_Contents))))
       {
       set(GR_ZakE_Pages, MUIA_Group_ActivePage, 0);
       set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_Data);
       return(FALSE);
       }


    get(ST_ZakE_LP, MUIA_String_Integer, &zak->LP);
    memcpy(&zak->DataZdarzenia, Str2Date((char *)xget(ST_ZakE_Data, MUIA_String_Contents)), sizeof(struct DateStamp));
    copystr(zak->NumerDowoduKsi�gowego, ST_ZakE_Numer);
    copystr(zak->OpisZdarzenia        , ST_ZakE_Opis);

    zak->Warto��SprzedanychTowar�w   = String2Price(getstr(ST_ZakE_P7));
    zak->Pozosta�ePrzychody          = String2Price(getstr(ST_ZakE_P8));

    zak->ZakupTowar�wHandlowych      = String2Price(getstr(ST_ZakE_P10));
    zak->KosztyUboczneZakupu         = String2Price(getstr(ST_ZakE_P11));

    zak->KosztyReprezentacjiIReklamy = String2Price(getstr(ST_ZakE_P12));
    zak->Wynagrodzenia               = String2Price(getstr(ST_ZakE_P13));
    zak->Pozosta�eWydatki            = String2Price(getstr(ST_ZakE_P14));

    zak->P16                         = String2Price(getstr(ST_ZakE_P16));
    copystr(zak->Uwagi, ST_ZakE_P17);



//    if(StrnCmp(MyLocale, Product->Nazwa, (char *)xget(ST_Mag_Nazwa, MUIA_String_Contents), -1, SC_COLLATE2) != 0)
       result = REFRESH;

    return(result);
}
//|
/// EdycjaZakupu

char EdycjaZakupu(struct ZakupList *zakup, char EditMode)
{
/*
** G�owna p�tla edycji produktu
** Zwraca to co EdycjaZakupuFinish()
** b�d� FALSE je�li przerwano edycj�
**
** Je�li EditMode == TRUE, to edytujemy
** istniej�cy ju� produkt
*/

char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

//struct Zakup zak_tmp = {0};

    set(app, MUIA_Application_Sleep, TRUE);

//    memcpy(&zak_tmp, &zakup->za_zakup, sizeof(struct Zakup));

    EdycjaZakupuSetup(zakup);

    if(WinOpen(EdycjaZakupuWindow))
        {
        while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);

          switch(ID)
            {
            case ID_PAGE_0:
               set(GR_ZakE_Pages, MUIA_Group_ActivePage, ID - ID_PAGE_0);
               set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_LP);
               break;

            case ID_PAGE_1:
               set(GR_ZakE_Pages, MUIA_Group_ActivePage, ID - ID_PAGE_0);
               set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_Opis);
               break;

            case ID_PAGE_2:
               set(GR_ZakE_Pages, MUIA_Group_ActivePage, ID - ID_PAGE_0);
               set(EdycjaZakupuWindow, MUIA_Window_ActiveObject, ST_ZakE_P12);
               break;


            case ID_OK:
               result = EdycjaZakupuFinish(zakup, EditMode);

               if(result == FALSE)
                  {
                  DisplayBeep(0);
                  }
               else
                  {
                  running = FALSE;
                  }
               break;


            case ID_CANCEL:
               running = FALSE;
               break;
            }
          if(running && signal) Wait(signal);
          }

        set(EdycjaZakupuWindow, MUIA_Window_Open, FALSE);
        }
     else
        {
        DisplayBeep(0);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        }

    set(app, MUIA_Application_Sleep, FALSE);

    return(result);
}

//|

/// Zakupy

//#define FAK_ZMIANY(x)  {ZmianyNaFakturze += x; set(BT_Fak_Ok, MUIA_Disabled, (ZmianyNaFakturze == 0)); set(BT_Fak_Usu�, MUIA_Disabled, (ZmianyNaFakturze == 0)); }


#define ZAK_ZMIANY(x)  {ZmianyWZakupach += x; set(BT_Zak_Ok, MUIA_Disabled, (ZmianyWZakupach == 0));}

char Zakupy(void)
{
//static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };

struct DocumentPrint dp={0};
struct StringRequest paragon_req = {0};       // je�li dokument jest do paragonu, tutaj jest
                                              // numer paragonu wpisany przez u�ytkownika

char   running                       = TRUE;
ULONG  signal                        = 0;
char   result                        = FALSE;
char   temp_prod_name[PROD_NAME_LEN] = "";
char   data_str[]                    = "00.00.0000";
struct DateStamp datestamp;
long   ZmianyWZakupach = 0;


    NewList(&zakupy);


    // nie jest �le... go�! ;-)

    set(app, MUIA_Application_Sleep, TRUE);

/*
    // aktualny miesiac i rok
    {
    char _data[2+1+4+1];  // mm yyyy\0
    long _mc;
    long _rok;

    DateStamp(&datestamp);
    FormatDateHook.h_Data = _data;
    FormatDate(MyLocale, "%m %Y", &datestamp, &FormatDateHook);
    sscanf(_data, "%ld %ld", &_mc, &_rok);

    dp.Miesiac = _mc;
    dp.Rok = _rok;
    }
*/


    D(bug("** 2\n"));


// Setting up

//    set(FakturaWindow, MUIA_Window_ActiveObject , LV_Fak_Produkty);



//  Nag��wek i numer dokumentu


    if(WinOpen(ZakupyWindow))
       {
       while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);
          switch(ID)
            {
            case ID_INFO:
               InformacjeOZakupach();
               break;

            case ID_ADD:
               {
               struct ZakupList *zak;

               zak = calloc(1, sizeof(struct ZakupList));

               if(zak)
                  {
                  zak->Deleted = FALSE;

                  if(EdycjaZakupu(zak, FALSE))
                     {
                     long pos = DodajZakup(zak);

                     DoMethod(LV_Zak_Zakupy, _MUIM_List_InsertSingle, zak, pos);
                     set(LV_Zak_Zakupy, _MUIA_List_Active, pos);
                     ZAK_ZMIANY(1)
                     }
                  else
                     {
                     free(zak);
                     }
                  }
               else
                  {
                  DisplayBeep(0);
                  MUI_Request(app, ZakupyWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                  }
               }
               break;


            case ID_EDIT:
               {
               if(xget(LV_Zak_Zakupy, _MUIA_List_Active) != _MUIV_List_Active_Off)
                   {
                   struct ZakupList *zak;

                   DoMethod(LV_Zak_Zakupy, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &zak);

                   if(zak->Deleted)
                     {
                     if(MUI_Request(app, ZakupyWindow, 0, TITLE, MSG_ZAK_DELETED_GAD , MSG_ZAK_DELETED, zak->za_zakup.OpisZdarzenia))
                       {
                       zak->Deleted = FALSE;
                       DoMethod(LV_Zak_Zakupy, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                       ZAK_ZMIANY(-1)
                       }
                     else
                       break;
                     }

                   switch(EdycjaZakupu(zak, TRUE))
                     {
                     case TRUE:
                         DoMethod(LV_Zak_Zakupy, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                         ZAK_ZMIANY(1)
                         break;

                     case REFRESH:
                         {
                         long pos;
                         set(LV_Zak_Zakupy, _MUIA_List_Quiet, TRUE);

                         DoMethod(LV_Zak_Zakupy, _MUIM_List_Remove, _MUIV_List_Remove_Active);

                         Remove((struct Node*)zak);
                         pos = DodajZakup(zak);

                         DoMethod(LV_Zak_Zakupy, _MUIM_List_InsertSingle, zak, pos);
                         set(LV_Zak_Zakupy, _MUIA_List_Active, pos);

                         set(LV_Zak_Zakupy, _MUIA_List_Quiet, FALSE);
                         ZAK_ZMIANY(1)
                         }
                         break;
                     }
                   }
               }
               break;


            case ID_DELETE:
               {
               if(xget(LV_Zak_Zakupy, _MUIA_List_Active) != _MUIV_List_Active_Off)
                   {
                   struct ZakupList *zak;

                   DoMethod(LV_Zak_Zakupy, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &zak);

                   zak->Deleted = ((zak->Deleted + 1) & 0x1);

                   if(zak->Deleted)
                       ZAK_ZMIANY(1)
                   else
                       ZAK_ZMIANY(-1)

                   DoMethod(LV_Zak_Zakupy, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);

                   if(ID == ID_MAG_SKASUJ_AND_SKIP)
                       set(LV_Zak_Zakupy, _MUIA_List_Active, _MUIV_List_Active_Down);
                   }
               }
               break;




            case ID_LOAD:
               {
//               static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };
               char   FileName[80];
               char   Miesiac[3];
               char   Rok[5];
               char   result = TRUE;
               int    miesiac;

               // katalogi dla dokument�w

               miesiac = getcycle(CY_Zak_Miesiac);
               if(miesiac == 0)
                   {
                   wfmh_Date2StrFmtBuffer( Miesiac, "%m", &Today );

//                   FormatDateHook.h_Data = Miesiac;
//                   FormatDate(MyLocale, "%m", &Today, &FormatDateHook);
                   }
               else
                   {
                   sprintf(Miesiac, "%ld", miesiac);
                   }

               wfmh_Date2StrFmtBuffer( Rok, "%Y", &Today );
//               FormatDateHook.h_Data = Rok;
//               FormatDate(MyLocale, "%Y", &Today, &FormatDateHook);

               sprintf(FileName, "%s/%s/%s/%02lx/%s", DatabasesDir, Rok, Miesiac, ZAKUPY, ZakupyFileName);

               WczytajZakupy(FileName);
               }
               break;


            case ID_SAVE:
               {

               }
               break;



            /*                         */
            /*    Ko�czymy edycj�...   */
            /*                         */

            case ID_OK:
               {
               result  = TRUE;
               running = FALSE;
               }
               break;


            case ID_CANCEL:
/*
               if(ZmianyNaFakturze)
                   {
                   if(!MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_CANCEL_REQ_GAD, MSG_FAK_CANCEL_REQ))
                     break;

                   WczytajMagazyn();
                   }
*/
               running = FALSE;
               break;
            }

          }

       if(running && signal) Wait(signal);
       }
    else
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       }

    set(ZakupyWindow, MUIA_Window_Open, FALSE);

    Usu�Zakupy(TRUE);

    set(app, MUIA_Application_Sleep, FALSE);

    return(result);

}
//|
*/
