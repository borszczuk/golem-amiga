
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

#include "golem.h"

/// USER STOP CHUNKS
#define USER_NUM_STOPS (sizeof(User_Stops) / (2 * sizeof(ULONG)))

STATIC LONG User_Stops[] =
{
        ID_USER, ID_CAT,
        ID_USER, ID_VERS,
        ID_USER, ID_USER,
        NULL, NULL,
};
//|

/// DodajU�ytkownika

int DodajU�ytkownika(struct UserList *user)
{
/*
** Dodaje u�ytkownika do listy
*/

struct UserList *node;
int i = 0;

    if(!IsListEmpty(&u�ytkownicy))
       {
       for(node = (struct UserList *)u�ytkownicy.lh_Head; node->node.mln_Succ; node = (struct UserList *)node->node.mln_Succ, i++)
           {
           if(StrnCmp(MyLocale, user->user.Nazwa, node->user.Nazwa, -1, SC_COLLATE2) < 0)
               {
               if((struct Node *)node->node.mln_Pred)
                   Insert(&u�ytkownicy, (struct Node *)user, (struct Node *)node->node.mln_Pred);
               else
                   AddHead(&u�ytkownicy, (struct Node *)user);

               return(i);
               }
           }
       }

       AddTail(&u�ytkownicy, (struct Node *)user);
       return(MUIV_List_Insert_Bottom);
}
//|
/// Znajd�U�ytkownikaNazwa
struct UserList *Znajd�U�ytkownikaNazwa(char *Nazwa)
{
/*
** Przeszukuj� list� u�ytkownik�w w poszukiwaniu
** tego o podanej nazwie
*/

struct UserList *node;

    if(!IsListEmpty(&u�ytkownicy))
       {
       for(node = (struct UserList *)u�ytkownicy.lh_Head; node->node.mln_Succ; node = (struct UserList *)node->node.mln_Succ)
       if(!stricmp(Nazwa, node->user.Nazwa))
           return(node);
       }

    return(NULL);
}
//|
/// Znajd�U�ytkownikaCRC
struct UserList *Znajd�U�ytkownikaCRC(long CRC)
{
/*
** Przeszukuj� list� u�ytkownik�w w poszukiwaniu
** tego o podanym CRC
*/

struct UserList *node;

    if(!IsListEmpty(&u�ytkownicy))
       {
       for(node = (struct UserList *)u�ytkownicy.lh_Head; node->node.mln_Succ; node = (struct UserList *)node->node.mln_Succ)
           {
           if(CRC == node->user.Has�o_CRC)
              return(node);
           }
       }

    return(NULL);
}
//|
/// Usu�U�ytkownik�w
void Usu�U�ytkownik�w(char DeleteAll)
{
/*
** Usuwa skasowanych u�ytkownik�w (je�li DeleteAll=FALSE)
** albo ca�� list� (je��i DeleteAll=TRUE)
*/

struct UserList   *user, *next;

    if(!IsListEmpty(&u�ytkownicy))
       {
       char Delete = FALSE;

       for(user = (struct UserList *)u�ytkownicy.lh_Head; user->node.mln_Succ; )
           {
           Delete = DeleteAll;

           if(DeleteAll == FALSE)
               Delete = user->Deleted;

           next = (struct UserList *)user->node.mln_Succ;

           if(Delete)
               {
               Remove((struct Node*)user);
               free(user);
               }

           user = next;
           }
       }
}
//|

/// PoliczU�ytkownik�w
long PoliczU�ytkownik�w(char UserLevel)
{
/*
** Liczy u�ytkownik�w o zadanym stopniu dost�pu
** Ignoruje u�ytkownik�w zaznaczonych jako skasowani
*/

struct UserList *node;
long   count = 0;

    if(!IsListEmpty(&u�ytkownicy))
       {
       for(node = (struct UserList *)u�ytkownicy.lh_Head; node->node.mln_Succ; node = (struct UserList *)node->node.mln_Succ)
           {
           if((node->user.UserLevel == UserLevel) && (node->Deleted == FALSE))
              {
//              printf("%s\n", node->user.Nazwa);

              count++;
              }
           }
       }

//printf("%ld\n", count);

    return(count);
}
//|
///DodajAdministratora

void DodajAdministratora(void)
{
struct UserList *User = calloc(1, sizeof(struct UserList));

    if(User)
       {
          strcpy(User->user.Nazwa, "Administrator");
          User->user.UserLevel = MUIV_UserLevel_Admin;
          User->user.Has�o_CRC = 0x5fa0aad7;          // wfmh
       DodajU�ytkownika(User);
       }                
    else
       {
       DisplayBeep(0);
       D(bug("** No memory for Admin!\n"));
       }
}

//|
/// ToolBarLogin
void ToolBarLogin(char mode)
{
    set(GR_ToolBar_LoginLogout, MUIA_Group_ActivePage, mode);

    DoMethod(BT_ToolBar_Login, MUIM_MultiSet, MUIA_Disabled, !mode,
                            BT_ToolBar_Sprzeda�,
                            BT_ToolBar_Zakupy,
                            BT_ToolBar_Redagowanie,
                            BT_ToolBar_BazyDanych,
                            BT_ToolBar_Rozliczenia,
                            BT_ToolBar_System,
                            NULL);

    if(mode)
      set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Logout);
    else
      set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Login);


}
//|

/// WczytajU�ytkownik�w

char WczytajU�ytkownik�w(void)
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
long   Errors = 0;
char   ValidFile = FALSE;

#ifndef DEMO

     set(app, MUIA_Application_Sleep, TRUE);

     if(iff = AllocIFF())
       {
       if(iff->iff_Stream = Open(U�ytkownicyFileName, MODE_OLDFILE))
           {
           InitIFFasDOS(iff);

           StopChunks(iff, User_Stops, USER_NUM_STOPS);

           if(!OpenIFF(iff, IFFF_READ))
               {

               while(TRUE)
                  {
                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                     break;

                  if(cn = CurrentChunk(iff))
                     {
                     LONG ID = cn->cn_ID;

                     if(!ValidFile)
                        {
                        if((ID == ID_CAT) && (cn->cn_Type == ID_USER))
                           {
                           ValidFile = TRUE;
                           continue;
                           }


                        break;
                        }

///                    ID_VERS
                     if(ID == ID_VERS)
                        {
                        struct BaseVersion version;

                        if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                           {

                           }
                        else
                           {
                           _RC = IoErr();
                           break;
                           }

                        continue;
                        }
//|
///                    ID_USER

                     if(ID == ID_USER)
                        {
                        struct UserList *User = calloc(1, sizeof(struct UserList));

                        if(User)
                           {
                           if(ReadChunkBytes(iff, &User->user, sizeof(struct User)) != cn->cn_Size)
                              {
                              free(User);
                              _RC = IoErr();
                              break;
                              }

                           DodajU�ytkownika(User);
                           }

                        continue;
                        }
//|

                     }
                  }

               CloseIFF(iff);
               }

            Close(iff->iff_Stream);
            }


//       if(_RC == IFFERR_EOF) _RC = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

       FreeIFF(iff);
       }

       set(app, MUIA_Application_Sleep, FALSE);

#endif

  return(0);
}

//|
/// ZapiszU�ytkownik�w

char ZapiszU�ytkownik�w(void)
{
#ifndef DEMO

struct IFFHandle *MyIFFHandle;

    set(app, MUIA_Application_Sleep, TRUE);

    if(MyIFFHandle = AllocIFF())
        {
        BPTR  FileHandle;

        if(FileHandle = Open(U�ytkownicyFileName, MODE_NEWFILE))
           {
           MyIFFHandle->iff_Stream = FileHandle;
           InitIFFasDOS(MyIFFHandle);

           if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
               {
               struct BaseVersion version;
               struct UserList    *user;

               PushChunk(MyIFFHandle, ID_USER, ID_CAT, IFFSIZE_UNKNOWN);

               PushChunk(MyIFFHandle, ID_USER, ID_FORM, IFFSIZE_UNKNOWN);
                   PushChunk(MyIFFHandle, ID_USER, ID_VERS, IFFSIZE_UNKNOWN);
                   version.Version = VERSION;
                   version.Revision = REVISION;
                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                   PopChunk(MyIFFHandle);

                   PushChunk(MyIFFHandle, ID_USER, ID_ANNO, IFFSIZE_UNKNOWN);
                   WriteChunkBytes(MyIFFHandle, ScreenTitle, strlen(ScreenTitle));
                   WriteChunkBytes(MyIFFHandle, " <", 2);
                   WriteChunkBytes(MyIFFHandle, EMAIL, strlen(EMAIL));
                   WriteChunkBytes(MyIFFHandle, ">, ", 3);
                   WriteChunkBytes(MyIFFHandle, WWW, strlen(WWW));
                   PopChunk(MyIFFHandle);
               PopChunk(MyIFFHandle);


               PushChunk(MyIFFHandle, ID_USER, ID_FORM, IFFSIZE_UNKNOWN);

               if(!IsListEmpty(&u�ytkownicy))
                 {
                 for(user = (struct UserList *)u�ytkownicy.lh_Head; user->node.mln_Succ; user = (struct UserList *)user->node.mln_Succ)
                   {
                   PushChunk(MyIFFHandle, ID_USER, ID_USER, IFFSIZE_UNKNOWN);
                   WriteChunkBytes(MyIFFHandle, &user->user, sizeof(struct User));
                   PopChunk(MyIFFHandle);
                   }
                 }

               PopChunk(MyIFFHandle);

               PopChunk(MyIFFHandle);
               CloseIFF(MyIFFHandle);
               }
           else
               {
               DisplayBeep(0);
               D(bug("*** OpenIFF() nie powiod�o si�\n"));
               }

           Close(FileHandle);
           }
        else
           {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
           D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", U�ytkownicyFileName));
           }

        FreeIFF(MyIFFHandle);
        }
     else
        {
        DisplayBeep(0);
        D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
        }

    set(app, MUIA_Application_Sleep, FALSE);

#endif

    return(0);
}



//|


/// EDYCJA U�YTKOWNIKA STRINGS

#define MSG_USER_ED_WIN_TITLE "Edycja u�ytkownika"

#define MSG_USER_ED_NAME   "N_azwisko"
#define MSG_USER_ED_PASSWD "_Has�o"
#define MSG_USER_ED_ACCESS "_Uprawnienia"

#define MSG_USER_ED_OK     "_Ok"
#define MSG_USER_ED_CANCEL "Ponie_chaj"

#define MSG_USER_ED_PASSWD_CHANGE     "Na pewno chcesz zmieni� has�o?"
#define MSG_USER_ED_PASSWD_CHANGE_GAD "_Tak|*_Nie"

#define MSG_USER_ED_SHORT_NAME   "Wprowad� pe�ne imi� i nazwisko!"
#define MSG_USER_ED_SHORT_PASSWD "\033cPoda�e� zbyt kr�tkie has�o!\nU�yj co najmniej 4 znak�w."
#define MSG_USER_ED_WRONG_PASSWD "\033cWprowadzone has�o nie jest prawid�owe!\nWprowad� inny ci�g znak�w."

//|
/// EdycjaU�ytkownikaSetup

static Object *EdycjaU�ytkownikaWindow,           /* EDYCJA U�YTKOWNIKA */
       *ST_UserEdit_Nazwa,
       *ST_UserEdit_Has�o,
//       *BT_UserEdit_Access,
       *BT_UserEdit_Ok,
       *BT_UserEdit_Cancel;


char EdycjaU�ytkownikaSetup(struct UserList *User)
{
struct User *user = &User->user;

///   EdycjaU�ytkownikaWindow

//                 MUIA_Application_Window,
                     EdycjaU�ytkownikaWindow = WindowObject,
                        MUIA_Window_Title      , MSG_USER_ED_WIN_TITLE,
                        MUIA_Window_ID         , ID_WIN_USER_EDIT,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, HGroup,
                                  GroupFrame,
                                  MUIA_Group_Columns, 2,

                                  Child, MakeLabel2(MSG_USER_ED_NAME),
                                  Child, ST_UserEdit_Nazwa = MakeString(USER_NAME_LEN, MSG_USER_ED_NAME),
                                  Child, MakeLabel2(MSG_USER_ED_PASSWD),
                                  Child, ST_UserEdit_Has�o = MakeStringSecret(USER_PASSWD_LEN, MSG_USER_ED_PASSWD),

//                                  Child, HVSpace,
//                                  Child, BT_UserEdit_Access = TextButton(MSG_USER_ED_ACCESS),

                                  End,

                           Child, HGroup,
                                  Child, BT_UserEdit_Ok     = TextButton(MSG_USER_ED_OK),
                                  Child, BT_UserEdit_Cancel = TextButton(MSG_USER_ED_CANCEL),
                                  End,

                           End,
                      End;
//|

    if(!EdycjaU�ytkownikaWindow)
       return(FALSE);

    setstring(ST_UserEdit_Nazwa, user->Nazwa);
    setstring(ST_UserEdit_Has�o, "");


    DoMethod(ST_UserEdit_Nazwa, MUIM_MultiSet, MUIA_Disabled, (logged_user->user.UserLevel != MUIV_UserLevel_Admin),
                                         ST_UserEdit_Nazwa,
                                         NULL);

    if(logged_user->user.UserLevel == MUIV_UserLevel_Admin)
      set(EdycjaU�ytkownikaWindow, MUIA_Window_ActiveObject, ST_UserEdit_Nazwa);
    else
      set(EdycjaU�ytkownikaWindow, MUIA_Window_ActiveObject, ST_UserEdit_Has�o);


    DoMethod(EdycjaU�ytkownikaWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
//    DoMethod(BT_UserEdit_Access , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ACCESS);
    DoMethod(BT_UserEdit_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
    DoMethod(BT_UserEdit_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


    _attachwin(EdycjaU�ytkownikaWindow);

    return(TRUE);

}
//|
/// EdycjaU�ytkownikaFinish
char EdycjaU�ytkownikaFinish(struct UserList *User, char EditMode)
{
/*
** sprawdza poprawno�� wprowadzonych danych
** dot. u�ytkownika. Zwraca FALSE, je�li co�
** jest nie tak, TRUE, je�li wszystkie
** elementy s� poprawne, oraz REFRESH,
** je�li nazwisko u�ytkownika zosta�o zmienione
** (istotne tylko podczas edycji produktu)
*/

struct User *user = &User->user;
char   result = TRUE;
long   crc;
char   has�o_len;


    has�o_len = strlen((char *)xget(ST_UserEdit_Has�o, MUIA_String_Contents));
    if(has�o_len > 0)
       {
       crc = wfmh_CRC32((char *)xget(ST_UserEdit_Has�o, MUIA_String_Contents), has�o_len);
       }

    if(EditMode == TRUE) // edycja istniej�cego u�ytkownika
      {
      if(has�o_len > 3)
        {
        struct UserList *tmp;

        if(!(MUI_Request(app, EdycjaU�ytkownikaWindow, 0, TITLE, MSG_USER_ED_PASSWD_CHANGE_GAD , MSG_USER_ED_PASSWD_CHANGE)))
           {
           set(EdycjaU�ytkownikaWindow, MUIA_Window_ActiveObject, ST_UserEdit_Has�o);
           return(FALSE);
           }                                 

        Remove((struct Node *)User);
        tmp = Znajd�U�ytkownikaCRC(crc);
        DodajU�ytkownika(User);

        if(tmp)
           {
           MUI_Request(app, EdycjaU�ytkownikaWindow, 0, TITLE, MSG_OK, MSG_USER_ED_WRONG_PASSWD);
           set(ST_UserEdit_Has�o, MUIA_String_Contents, "");
           set(EdycjaU�ytkownikaWindow, MUIA_Window_ActiveObject, ST_UserEdit_Has�o);
           return(FALSE);
           }
        }
      else
        {
        if(has�o_len != 0)
           {
           MUI_Request(app, EdycjaU�ytkownikaWindow, 0, TITLE, MSG_OK, MSG_USER_ED_SHORT_PASSWD);
           set(EdycjaU�ytkownikaWindow, MUIA_Window_ActiveObject, ST_UserEdit_Has�o);
           return(FALSE);
           }
        }
      }
    else
      {
      if(has�o_len > 3)
        {
        if(Znajd�U�ytkownikaCRC(crc))
           {
           MUI_Request(app, EdycjaU�ytkownikaWindow, 0, TITLE, MSG_OK, MSG_USER_ED_WRONG_PASSWD);
           set(ST_UserEdit_Has�o, MUIA_String_Contents, "");
           set(EdycjaU�ytkownikaWindow, MUIA_Window_ActiveObject, ST_UserEdit_Has�o);
           return(FALSE);
           }
        }
      else
        {
        MUI_Request(app, EdycjaU�ytkownikaWindow, 0, TITLE, MSG_OK, MSG_USER_ED_SHORT_PASSWD);
        set(EdycjaU�ytkownikaWindow, MUIA_Window_ActiveObject, ST_UserEdit_Has�o);
        return(FALSE);
        }
      }


    if(strlen((char *)xget(ST_UserEdit_Nazwa, MUIA_String_Contents)) < 6)
       {
       MUI_Request(app, EdycjaU�ytkownikaWindow, 0, TITLE, MSG_OK, MSG_USER_ED_SHORT_NAME);
       set(EdycjaU�ytkownikaWindow, MUIA_Window_ActiveObject, ST_UserEdit_Nazwa);
       return(FALSE);
       }



    // szukamy takiego samego nazwiska
    {
    struct UserList *tmp;

    if(EditMode)
      {
      Remove((struct Node *)User);
      tmp = Znajd�U�ytkownikaNazwa((char *)xget(ST_UserEdit_Nazwa, MUIA_String_Contents));
      DodajU�ytkownika(User);
      }
    else
      {
      tmp = Znajd�U�ytkownikaNazwa((char *)xget(ST_UserEdit_Nazwa, MUIA_String_Contents));
      }

    if(tmp)
       {
       set(EdycjaU�ytkownikaWindow, MUIA_Window_ActiveObject, ST_UserEdit_Nazwa);
       return(FALSE);
       }
    }

    if(StrnCmp(MyLocale, user->Nazwa, (char *)xget(ST_UserEdit_Nazwa, MUIA_String_Contents), -1, SC_COLLATE2) != 0)
       result = REFRESH;

    strcpy(user->Nazwa, (char *)xget(ST_UserEdit_Nazwa, MUIA_String_Contents));

    if(EditMode)
       {
       if(has�o_len > 0)
         user->Has�o_CRC = crc;
       }
    else
       {
       user->Has�o_CRC = crc;
       }

    return(result);

}
//|
/// EdycjaU�ytkownika
char EdycjaU�ytkownika(struct UserList *user, char EditMode)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

    set(app, MUIA_Application_Sleep, TRUE);

    if(!EdycjaU�ytkownikaSetup(user))
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       _sleep(FALSE);
       return(FALSE);      // cancel
       }


    if(WinOpen(EdycjaU�ytkownikaWindow))
        {
        while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);
          switch(ID)
            {
            case ID_OK:
               result = EdycjaU�ytkownikaFinish(user, EditMode);

               if(result == FALSE)
                 {
                 DisplayBeep(0);
                 }
               else
                 {
                 running = FALSE;
                 }
               break;


            case ID_CANCEL:
               running = FALSE;
               break;

            }
          if(running && signal) Wait(signal);
          }

        set(EdycjaU�ytkownikaWindow, MUIA_Window_Open, FALSE);
        }
     else
        {
        DisplayBeep(NULL);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        }


    _detachwin(EdycjaU�ytkownikaWindow);

    set(app, MUIA_Application_Sleep, FALSE);

    return(result);

}
//|


/// OKNO U�YTKOWNIK�W STRINGS

#define MSG_USER_WIN_TITLE "U�ytkownicy"

#define MSG_USER_ADD    "F1 - Dod_aj"
#define MSG_USER_EDIT   "F2 - _Edytuj"
#define MSG_USER_DELETE "DEL - S_kasuj"

#define MSG_USER_OK     "F10 - Zapi_sz"
#define MSG_USER_CANCEL "ESC - Ponie_chaj"

#define MSG_USER_ED_DELETED     "\033cDane u�ytkownika \"\0338%s\0332\"\ns� aktualnie zaznaczone jako \033bSKASOWANE\033n,\ni nie mog� by� modyfikowane! Je�li chcesz dokona� w nich\njakichkolwiek zmian, musisz je najpierw odkasowa�.\nCzy chcesz to zrobi� teraz?"
#define MSG_USER_ED_DELETED_GAD "*_Tak, odkasuj|_Nie"

#define MSG_USER_SAVE     "Na pewno chcesz zapisa� zmiany?"
#define MSG_USER_SAVE_GAD "_Tak, zapisz|*_Nie"

#define MSG_USER_NO_ADMIN "\033cSystem nie mo�e zosta� bez administratora!\n\nOdkasuj co najmniej jedn� osob�\nposiadaj�ca takie uprawnienia,\nalbo dopisz nowego administratora!"

#define MSG_USER_CANCEL_REQ     "\033cNa pewno chcesz zako�czy� edycj�\nu�ytkownik�w i uniewa�ni� wszystkie\nwprowadzone zmiany?"
#define MSG_USER_CANCEL_REQ_GAD "_Tak, zako�cz|*_Nie"

#define MSG_USER_CANT_DELETE "\033cNie mo�esz skasowa� sam siebie!"

//|
/// U�ytkownicySetup

static Object
       *UserWindow,                        /* LOGIN WINDOW         */
       *LV_User_Employers,
       *BT_User_Add,
       *BT_User_Edit,
       *BT_User_Delete,
       *BT_User_Ok,
       *BT_User_Cancel;


char U�ytkownicySetup(void)
{

///   User Window

        UserWindow = WindowObject,
                        MUIA_Window_Title      , MSG_USER_WIN_TITLE,
                        MUIA_Window_ID         , ID_WIN_USER,
                        MUIA_Window_ScreenTitle, ScreenTitle,
//                        MUIA_Window_CloseGadget, FALSE,
                        WindowContents, VGroup,

                                 Child, VGroup,
                                        GroupFrame,

                                        Child, LV_User_Employers = ListviewObject,
                                               MUIA_CycleChain, TRUE,
                                               _MUIA_Listview_List, NewObject(CL_UsersList->mcc_Class, NULL, TAG_DONE),
                                               End,

                                        Child, HGroup,
                                               Child, BT_User_Add    = TextButton(MSG_USER_ADD),
                                               Child, BT_User_Edit   = TextButton(MSG_USER_EDIT),
                                               Child, BT_User_Delete = TextButton(MSG_USER_DELETE),
                                               End,

                                        End,

                                 Child, HGroup,
                                        MUIA_Group_SameSize, TRUE,

                                        Child, BT_User_Ok     = TextButton(MSG_USER_OK),
                                        Child, BT_User_Cancel = TextButton(MSG_USER_CANCEL),
                                        End,
                                 End,

                       End;

//|


    if( !UserWindow )
       return(FALSE);


    // settings

    {
    struct UserList *user;

    if(logged_user->user.UserLevel == MUIV_UserLevel_Admin)
      {
      if(!IsListEmpty(&u�ytkownicy))
         for(user = (struct UserList *)u�ytkownicy.lh_Head; user->node.mln_Succ; user = (struct UserList *)user->node.mln_Succ)
            DoMethod(LV_User_Employers, _MUIM_List_InsertSingle, user, _MUIV_List_Insert_Sorted);

      }
    else
      {
      DoMethod(LV_User_Employers, _MUIM_List_InsertSingle, logged_user, _MUIV_List_Insert_Sorted);
      }
    }


    DoMethod(BT_User_Add, MUIM_MultiSet, MUIA_Disabled, (logged_user->user.UserLevel != MUIV_UserLevel_Admin),
                                         BT_User_Add , BT_User_Delete,
                                         NULL);


    set(BT_User_Ok        , MUIA_Disabled, TRUE);
    set(LV_User_Employers, _MUIA_List_Active, 0);

    set(UserWindow, MUIA_Window_ActiveObject , LV_User_Employers);
    set(UserWindow, MUIA_Window_DefaultObject, LV_User_Employers);



    // notyfikacje

    DoMethod(UserWindow      ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

    DoMethod(LV_User_Employers, MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_USER_EDIT);

    DoMethod(BT_User_Add     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_USER_ADD);
    DoMethod(BT_User_Edit    ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_USER_EDIT);
    DoMethod(BT_User_Delete  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_USER_DEL);

    DoMethod(UserWindow      , MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_USER_ADD);
    DoMethod(UserWindow      , MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_USER_EDIT);
    DoMethod(UserWindow      , MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);
    DoMethod(UserWindow      , MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_USER_DEL);

    DoMethod(BT_User_Ok      ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
    DoMethod(BT_User_Cancel  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);



    _attachwin(UserWindow);


    return( TRUE );
}
//|
/// U�ytkownicy

#define USER_ZMIANY(x)  {ZmianyWU�ytkownikach += x; set(BT_User_Ok, MUIA_Disabled, (ZmianyWU�ytkownikach == 0));}

char U�ytkownicy(void)
{
char  running = TRUE;
ULONG signal  = 0;
short ZmianyWU�ytkownikach = 0;

    set(app, MUIA_Application_Sleep, TRUE);


    if( !U�ytkownicySetup() )
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       _sleep(FALSE);
       return(NULL);
       }


    if(WinOpen(UserWindow))
        {
        while(running)
          {
          switch (DoMethod(app, MUIM_Application_Input, &signal))
            {

            case ID_USER_ADD:
               {
               if(logged_user->user.UserLevel != MUIV_UserLevel_Admin)
                   break;

               {
               struct UserList *user = calloc(1, sizeof(struct UserList));

               if(user)
                 {
                 user->Deleted = FALSE;

                 if(EdycjaU�ytkownika(user, FALSE))
                   {
                   long pos = DodajU�ytkownika(user);

                   DoMethod(LV_User_Employers, _MUIM_List_InsertSingle, user, pos);
                   set(LV_User_Employers, _MUIA_List_Active, pos);
                   USER_ZMIANY(1)
                   }
                 else
                   {
                   free(user);
                   }
                 }
               else
                 {
                 DisplayBeep(0);
                 MUI_Request(app, UserWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                 }
               }
               }
               break;






            case ID_USER_EDIT:
               {
               if(xget(LV_User_Employers, _MUIA_List_Active) != _MUIV_List_Active_Off)
                   {
                   struct UserList *user;

                   DoMethod(LV_User_Employers, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &user);

                   if(user->Deleted)
                     {
                     if(MUI_Request(app, UserWindow, 0, TITLE, MSG_USER_ED_DELETED_GAD , MSG_USER_ED_DELETED, user->user.Nazwa))
                       {
                       user->Deleted = FALSE;
                       DoMethod(LV_User_Employers, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                       USER_ZMIANY(-1)
                       }
                     else
                       break;
                     }

                   switch(EdycjaU�ytkownika(user, TRUE))
                     {
                     case TRUE:
                         DoMethod(LV_User_Employers, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                         USER_ZMIANY(1)
                         break;

                     case REFRESH:
                         {
                         long pos;
                         set(LV_User_Employers, _MUIA_List_Quiet, TRUE);

                         DoMethod(LV_User_Employers, _MUIM_List_Remove, _MUIV_List_Remove_Active);

                         Remove((struct Node*)user);
                         pos = DodajU�ytkownika(user);

                         DoMethod(LV_User_Employers, _MUIM_List_InsertSingle, user, pos);
                         set(LV_User_Employers, _MUIA_List_Active, pos);

                         set(LV_User_Employers, _MUIA_List_Quiet, FALSE);
                         USER_ZMIANY(1)
                         }
                         break;
                     }
                   }
               }
               break;




            case ID_USER_DEL:
               {
               if(logged_user->user.UserLevel != MUIV_UserLevel_Admin)
                   break;

               if(xget(LV_User_Employers, _MUIA_List_Active) != _MUIV_List_Active_Off)
                   {
                   struct UserList *user;

                   DoMethod(LV_User_Employers, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &user);

                   if(logged_user != user)
                     {
                     user->Deleted = ((user->Deleted + 1) & 0x1);

                     if(user->Deleted)
                       USER_ZMIANY(1)
                     else
                       USER_ZMIANY(-1)

                     DoMethod(LV_User_Employers, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);

/*                     if(ID == ID_USER_DEL_AND_SKIP)
                          set(LV_User_Employers, _MUIA_List_Active, _MUIV_List_Active_Down);

*/                   }
                   else
                     {
                     DisplayBeep(0);
                     MUI_Request(app, UserWindow, 0, TITLE, MSG_OK, MSG_USER_CANT_DELETE);
                     }              
                   }
               }
               break;









            case ID_OK:
               {
               if(ZmianyWU�ytkownikach)
                 {
                 if(PoliczU�ytkownik�w(MUIV_UserLevel_Admin) == 0)
                   {
                   MUI_Request(app, UserWindow, 0, TITLE, MSG_OK, MSG_USER_NO_ADMIN);
                   }
                 else
                   {
                   if(MUI_Request(app, UserWindow, 0, TITLE, MSG_USER_SAVE_GAD, MSG_USER_SAVE))
                     {
                     Usu�U�ytkownik�w(FALSE);
                     ZapiszU�ytkownik�w();
                     running = FALSE;
                     break;
                     }
                   }
                 }
               }
               break;


            case ID_CANCEL:
               if(ZmianyWU�ytkownikach)
                 {
                 if(MUI_Request(app, UserWindow, 0, TITLE, MSG_USER_CANCEL_REQ_GAD, MSG_USER_CANCEL_REQ))
                   {
                   Usu�U�ytkownik�w(TRUE);
                   WczytajU�ytkownik�w();
                   }
                 else
                   break;
                 }

               running = FALSE;
               break;

            }
          if(running && signal) Wait(signal);
          }

        }
     else
        {
        DisplayBeep(NULL);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        }


    _detachwin( UserWindow );
    set(app, MUIA_Application_Sleep, FALSE);

    return(TRUE);

}
//|


/// LOGIN WINDOW STRINGS

#define MSG_LOGIN_WIN_TITLE "Wprowad� has�o"

#define MSG_LOGIN_OK        "_Ok"
#define MSG_LOGIN_PASSWD    "_Has�o"
#define MSG_LOGIN_CANCEL    "Ponie_chaj"

//|
/// LoginSetup

static Object
       *LoginWindow,                       /* LOGIN WINDOW */
       *ST_Login_Has�o,
       *BT_Login_Ok,
       *BT_Login_Cancel;


char LoginSetup(void)
{

///   Login Window
                     LoginWindow = WindowObject,
                        MUIA_Window_Title      , MSG_LOGIN_WIN_TITLE,
                        MUIA_Window_ID         , ID_WIN_LOGIN,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, HGroup,
                                  GroupFrame,
                                  MUIA_Group_Columns, 2,

                                  Child, MakeLabel2(MSG_USER_ED_PASSWD),
                                  Child, ST_Login_Has�o = MakeStringSecret(USER_PASSWD_LEN, MSG_LOGIN_PASSWD),

                                  End,

                           Child, HGroup,
                                  MUIA_Group_SameSize, TRUE,
                                  Child, BT_Login_Ok     = TextButton(MSG_LOGIN_OK),
                                  Child, BT_Login_Cancel = TextButton(MSG_LOGIN_CANCEL),
                                  End,

                           End,
                      End;
///|

    if( !LoginWindow )
       return(FALSE);



    // notifications

    DoMethod(LoginWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
    DoMethod(BT_Login_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
    DoMethod(BT_Login_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


    _attachwin(LoginWindow);

    set(LoginWindow, MUIA_Window_ActiveObject, ST_Login_Has�o);


    return(TRUE);

}

//|
/// Login
struct UserList *Login(void)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;
struct UserList *user = NULL;

    set(app, MUIA_Application_Sleep, TRUE);


    if( !LoginSetup() )
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       _sleep(FALSE);
       return(NULL);
       }


    if(WinOpen(LoginWindow))
        {
        while(running)
          {
          switch (DoMethod(app, MUIM_Application_Input, &signal))
            {
            case ID_OK:
               {
               char *has�o = (char *)xget(ST_Login_Has�o, MUIA_String_Contents);
               long crc;

               if(*has�o != '\0')
                   {
                   struct UserList *tmp;

                   crc = wfmh_StringCRC32( has�o );
                   tmp = Znajd�U�ytkownikaCRC(crc);

                   if(tmp)
                     {
                     user = tmp;

                     sprintf(ScreenTitle, "WFMH Golem %s (%s) � %s", VERSIONREVISION, DATE, tmp->user.Nazwa);

                     set(MainWindow, MUIA_Window_ScreenTitle, ScreenTitle);
                     MUI_Request(app, LoginWindow, 0, TITLE, MSG_OK, "%s", tmp->user.Nazwa);
//                     set(MainWindow, MUIA_Window_Activate, TRUE);


// LOADING SYSTEM INFORMATION

            WczytajSystemInfo();

//            Sprawd�CzyKoniecMiesi�caRoku();

            DateStamp(&sys.last_start);
            ZapiszSystemInfo();


                     result  = TRUE;
                     running = FALSE;
                     }
                   else
                     {
                     DisplayBeep(0);
                     setstring(ST_Login_Has�o, "");
                     set(LoginWindow, MUIA_Window_ActiveObject, ST_Login_Has�o);
                     }
                   }
                 else
                   {
                   set(LoginWindow, MUIA_Window_ActiveObject, ST_Login_Has�o);
                   }

               }
               break;

            case ID_CANCEL:
               running = FALSE;
               break;

            }
          if(running && signal) Wait(signal);
          }

        }
     else
        {
        DisplayBeep(NULL);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        }


    _detachwin( LoginWindow );

    set(app, MUIA_Application_Sleep, FALSE);

    return(user);

}
//|


/// Logout

void Logout(void)
{

    strcpy(ScreenTitle, BaseScreenTitle);
    set(MainWindow, MUIA_Window_ScreenTitle, ScreenTitle);
    set(MainWindow, MUIA_Window_Activate, FALSE);
    set(MainWindow, MUIA_Window_Activate, TRUE);

    logged_user = NULL;

}
//|

