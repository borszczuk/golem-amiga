/*
*/


/*** Include stuff ***/


#ifndef GOLEM_PAYLIST_MCC_H
#define GOLEM_PAYLIST_MCC_H

#ifndef LIBRARIES_MUI_H
#include "libraries/mui.h"
#endif


/*** MUI Defines ***/

#define MUIC_Golem_PayList  "Golem_PayList.mcc"
#define MUIC_Golem_PayListP "Golem_PayList.mcp"
#define Golem_PayListObject MUI_NewObject(MUIC_Golem_PayList


/*** Methods ***/


/*** Method structs ***/


/*** Special method values ***/


/*** Special method flags ***/


/*** Attributes ***/


/*** Special attribute values ***/


/*** Structures, Flags & Values ***/


/*** Configs ***/


#endif /* GOLEM_PAYLIST_MCC_H */




