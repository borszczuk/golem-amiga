
//#define DEMO

//#define CUSTOMER_BETA
//#define PATRYK_BETA

//#define TOOLBAR
//#define USE_NLIST
//#define USE_BETTERSTRING

#define  REPLACE_TEXTOBJECT

#define MYDEBUG 0

/*######################################################################*/

//char beta_aleksanda[]="A#@$dxD#@!2";

//#define BETA_EVEREST
//#define BETA_ABACUS
//#define BETA_ALEKSANDRA

#ifdef  BETA_EVEREST
#define BETA
//2343
#define BETA_LICENCE 2342
#define BETA_WATERMARK "@!$#DSCA$1CAS"
#define BETA_USER_1 "Everest"
#define BETA_USER_2 "Witosa 61"
#define BETA_USER_3 "68-200 �ary"
#endif

#ifdef  BETA_ABACUS
#define BETA
#define BETA_LICENCE 2343
#define BETA_WATERMARK ")*$^@!DD&#@13"
#define BETA_USER_1 "Abacus"
#define BETA_USER_2 "W�. Jagie��y 5 / 37"
#define BETA_USER_3 "60-694 Pozna�"

/*
Abacus
ftp.amiga.com.pl
ID  : 2344
PASS: golem2344beta
*/

#endif

#ifdef  BETA_ALEKSANDRA
#define BETA
#define BETA_LICENCE 2344
#define BETA_WATERMARK "@#$@@!#@#%323"
#define BETA_USER_1 "Aleksandra"
#define BETA_USER_2 "Okr��na 17"
#define BETA_USER_3 "58-500 Jelenia G�ra"
#endif

/*######################################################################*/

#include "golem_includes.h"
#include "golem_structs.h"
#include "kpir_revision.h"
#include "kpir_strings.h"
#include "golem_protos.h"


#ifdef  StringObject
#undef  StringObject
#define StringObject  NewObject(CL_BString->mcc_Class, NULL
#endif


#ifdef DEMO
#define TITLE "WFMH Golem " VERSIONREVISION " BETA-DEMO"
#else
#define TITLE "WFMH Golem " VERSIONREVISION
#endif

#define EMAIL "info@amiga.com.pl"
#define WWW   "http://amiga.com.pl/golem/"

#define settext(a,b)    set(a, MUIA_Text_Contents, b)
#define getstr(a)       (char *)xget((a), MUIA_String_Contents)
#define copystr(a,b)    strcpy((a), (char *)xget((b), MUIA_String_Contents))
#define getnumstr(a)    xget((a), MUIA_String_Integer)
#define getcycle(a)     xget((a), MUIA_Cycle_Active)
#define getcheckmark(a) xget((a), MUIA_Selected)

//#define _read(a,b)   {if(ID == (a)) {if(ReadChunkBytes(iff, (b), cn->cn_Size) != cn->cn_Size) {Error = IoErr(); break; } continue; }}
//#define _read(a,b)   {if(ID == (a)) {if(ReadChunkBytes(iff, (b), sizeof(cn->cn_Size)) != cn->cn_Size) {Error = IoErr(); break; } continue; }}

#define _read(a,b)        {if(ID == (a)) {if(ReadChunkBytes(iff, (b), sizeof(b)) != cn->cn_Size) {Errors++; continue;}}}
#define _read_debug(a,b)  {if(ID == (a)) {D(bug(" Chunk: %04lx  want to read: %ld\n", a, sizeof(b))); if(ReadChunkBytes(iff, (b), sizeof(b)) != cn->cn_Size) {Errors++; continue; }}}

#define _write(a,b)  {PushChunk(MyIFFHandle, _ID_TYPE, (a), IFFSIZE_UNKNOWN); WriteChunkBytes(MyIFFHandle, &(b), sizeof(b)); PopChunk(MyIFFHandle); }
#define _write_size(a,b,c)  {PushChunk(MyIFFHandle, _ID_TYPE, (a), IFFSIZE_UNKNOWN); WriteChunkBytes(MyIFFHandle, &(b), sizeof(c)); PopChunk(MyIFFHandle); }

#define _read_size(a,b,c)    {if(ID == (a)) {if(ReadChunkBytes(iff, (b), c) != cn->cn_Size) {Errors++; continue;}}}

#define _sleep(a)  set(app, MUIA_Application_Sleep, (a))

/// ReturnIDs

enum{ID_OK = 1,
     ID_CANCEL,
     ID_USE,

     ID_STARTUP_CLOSED,

     ID_TOOLBAR_SPRZEDAZ,
        ID_TOOLBAR_VAT,
        ID_TOOLBAR_UPROSZCZONY,
        ID_TOOLBAR_VAT_PAR,
        ID_TOOLBAR_UPROSZCZONY_PAR,
        ID_TOOLBAR_PARAGON,

     ID_TOOLBAR_ZAKUPY,
        ID_TOOLBAR_ZAKUPY_KOSZTY,

     ID_TOOLBAR_BAZYDANYCH,
        ID_TOOLBAR_MAGAZYN,
        ID_TOOLBAR_KONTRAHENCI,
        ID_TOOLBAR_USERS,

     ID_TOOLBAR_ROZLICZENIA,
        ID_TOOLBAR_POSNET_RAPORT_DOBOWY,
        ID_TOOLBAR_POSNET_RAPORT_OKRESOWY,
        ID_TOOLBAR_POSNET_STAN_KASY,
        ID_TOOLBAR_POSNET_RAPORT_ZMIANY,

     ID_TOOLBAR_SYSTEM,
        ID_TOOLBAR_SETTINGS,

     ID_TOOLBAR_REDAGOWANIE,
     ID_TOOLBAR_LOGIN,
     ID_TOOLBAR_LOGOUT,
     ID_TOOLBAR_LOGINOUT,
     ID_TOOLBAR_ABOUT,

     ID_TOOLBAR_MAINMENU,

     ID_FIRST_GROUP,
     ID_PREV_GROUP,
     ID_NEXT_GROUP,
     ID_LAST_GROUP,

     ID_MOVE,

     ID_MAG_WYBIERZ,
     ID_MAG_ZNAJD�,
     ID_MAG_DODAJ,
     ID_MAG_EDYTUJ,
     ID_MAG_SKASUJ,
     ID_MAG_SKASUJ_AND_SKIP,
     ID_MAG_PRINT,
     ID_MAG_FILTER,
     ID_MAG_FILTER_ALL,

     ID_MAG_USU�_GRUP�,
     ID_MAG_DODAJ_GRUP�,
     ID_MAG_EDYCJA_GRUPY,
     ID_MAG_ZMIE�_GRUP�,

     ID_ACTIVATE,

     ID_CUST_DODAJ,
     ID_CUST_USU�,
     ID_CUST_EDYTUJ,
     ID_CUST_USU�_AND_SKIP,
     ID_CUST_KOPERTA,

     ID_CUST_FILTER,
     ID_CUST_FILTER_ALL,

     ID_CUST_UPVAT,
     ID_CUST_SC,

     ID_KLIENT_WYBIERZ,
     ID_FAK_KLIENT_ODBIORCA,
     ID_FAK_PROD_ACK,
     ID_FAK_PROD_ACK_LV,
     ID_FAK_CALC_WARTO��,
     ID_FAK_USU�,
     ID_FAK_TYP_P�ATNO�CI,
     ID_FAK_FREE_HAND,
     ID_FAK_ODBIORCA,

     ID_ACCEPT,
     ID_DELETE,
     ID_FIND,
     ID_ADD,

     ID_SET_UNIT_ADD,
     ID_SET_UNIT_EDIT,
     ID_SET_UNIT_DEL,

     ID_SET_PAY_ADD,
     ID_SET_PAY_EDIT,
     ID_SET_PAY_DEL,

     ID_USER_ADD,
     ID_USER_EDIT,
     ID_USER_DEL,

     ID_RED_DOC_TYPE,
     ID_RED_VIEW,
     ID_RED_RED,
     ID_RED_CANCEL,
     ID_RED_PRINT,
     ID_RED_KOREKTA,

     ID_MAG_MARZA_ACK,
     ID_MAG_BRUTTO_ACK,
     ID_MAG_ZAKUP_ACK,
     ID_MAG_VAT_ACK,
     ID_MAG_MARZA_PROCENT_ACK,

     ID_BACK,

     ID_FREE_MARZA_ACK,
     ID_FREE_BRUTTO_ACK,
     ID_FREE_ZAKUP_ACK,
     ID_FREE_VAT_ACK,
     ID_FREE_MARZA_PROCENT_ACK,

     ID_PAGE_0,
     ID_PAGE_1,
     ID_PAGE_2,

     ID_ACCESS,

     ID_STR_ACK,

     ID_LOAD,
     ID_SAVE,
     ID_EDIT,

     ID_INFO,

     ID_ZAKE_CALC_P11,
     ID_ZAKE_CALC_P13,
     ID_ZAKE_CALC_P15,
     ID_ZAKE_CALC_P17,

     ID_INDEX,
     ID_SORT,

    };

//|


enum{ID_WIN_STARTUP, ID_WIN_MAIN,
     ID_WIN_MAGAZYN, ID_WIN_EDYCJA_PROD, ID_WIN_FILTR,
     ID_WIN_GRUPA, ID_WIN_KONTRAHENCI, ID_WIN_KONTRAHENCI_EDIT, ID_CUST_WIN_FILTR,
     ID_WIN_FAKTURA, ID_WIN_GROUPSELECTOR,
     ID_WIN_DRUKOWANIE, ID_WIN_USTAWIENIA,
     ID_WIN_ED_UNIT, ID_WIN_ED_PAY,
     ID_WIN_PROG, ID_WIN_USER, ID_WIN_USER_EDIT,
     ID_WIN_LOGIN, ID_WIN_RED,
     ID_WIN_FREEHAND,
     ID_WIN_ODBIORCASELECTOR,
     ID_WIN_STRINGREQUEST,
     ID_WIN_ODBIORCA_DOKUMENTU,
     ID_WIN_KONTRAHENTSELECTOR,
     ID_WIN_ACCESS,
     ID_WIN_POSNET_RAPORT,
     ID_WIN_RESZTA,
     ID_WIN_ZAKUPY, ID_WIN_ZAKUP_EDIT, ID_WIN_ZAKUP_INFO,
     ID_WIN_INDEXREQUEST, ID_WIN_SORTREQUEST,

    };

/* Rodzaje dokument�w i rodzaje numeracji */

enum{ FAKTURA_VAT,
      RACHUNEK,
      FVAT_KOR,
      RACH_KOR,
      FVAT_PAR,
      RACH_PAR,
      PARAGON,

      DOCUMENT_COUNT,
    };


/* Rodzaje dokument�w dodatkowych i rodzaje numeracji */

enum{ ZAKUPY = 0x80,

      DOCUMENT_COUNT_2,
    };


enum{ NR_RRRR,
      NR_RR,
      NR_MM_RRRR,
      NR_MM_RR,
      RRRR_NR,
      RR_NR,
      RRRR_MM_NR,
      RR_MM_NR,
    };


/* settings.printer_charset */

enum{ PRINT_AMIGAPL,
      PRINT_NOPL,
      PRINT_CHR8,
      PRINT_ISO,
      PRINT_LATIN2,
      PRINT_MAZOVIA,
      PRINT_SUXX95,
    };

enum{ POSNET_RAPORT_DOBOWY,
      POSNET_RAPORT_OKRESOWY,
    };

enum{ VAT_ZW = 0,
      VAT_0,
      VAT_7,
      VAT_17,
      VAT_22,
    };

//extern   struct  Locale    *MyLocale;

#define RB CHECKIT
#define TG CHECKIT|MENUTOGGLE

/*************************************************************************/

#define GolemTemplateDir      "PROGDIR:wfmh_wzorce"

#define GolemConfigDir        "PROGDIR:wfmh_ustawienia"
#define UstawieniaFileName    GolemConfigDir "/ustawienia.golem"
#define KodyDrukarkiFileName  GolemConfigDir "/drukarka.golem"

#define DatabasesDir          "PROGDIR:wfmh_dane"
#define MagazynFileName       DatabasesDir "/magazyn.golem"
#define MagazynCountFileName  DatabasesDir "/magazyn_info.golem"
#define MagazynFileNameBak    MagazynFileName ".bak"
#define KlienciFileName       DatabasesDir "/klienci.golem"
#define KlienciCountFileName  DatabasesDir "/klienci_info.golem"
#define NumeracjaFileName     DatabasesDir "/numeracja.golem"
#define JednostkiFileName     DatabasesDir "/jednostki.golem"
#define P�atno�ciFileName     DatabasesDir "/platnosci.golem"
#define SystemFileName        DatabasesDir "/system.golem"
#define U�ytkownicyFileName   DatabasesDir "/pracownicy.golem"

#define StatusFileName        "status.golem"
#define ZakupyFileName        "zakupy.golem"


#define FV_NAME_HEAD "FVAT"
#define RU_NAME_HEAD "RUPR"

#ifndef IFF_HEAD
#define IFF_HEAD

#define ID_SYS     MAKE_ID('S','Y','S',' ')     // System information
#define ID_STRT    MAKE_ID('S','T','R','T')     //   last_start
#define ID_OPER    'OPER'                       //   last_operation

#define ID_MAG     MAKE_ID('M','A','G',' ')     // Magazyn
#define ID_PROD    MAKE_ID('P','R','O','D')     // Product information
#define ID_COUN    MAKE_ID('C','O','U','N')     // Products counter
#define ID_GRUP    MAKE_ID('G','R','U','P')     // Grupa magazynowa
#define ID_CUST    MAKE_ID('C','U','S','T')     // Kontrahenci
#define ID_RECE    MAKE_ID('R','E','C','E')     //  Osoba odbieraj�ca (faktury, rachunku)

#define ID_ANNO    MAKE_ID('A','N','N','O')
#define ID_VERS    MAKE_ID('V','E','R','S')

#define ID_NAME    MAKE_ID('N','A','M','E')     // Name


#define ID_NUMB    MAKE_ID('N','U','M','B')     // Numeracja dokument�w
#define ID_FVAT    MAKE_ID('F','V','A','T')     //   Faktura Vat
#define ID_UPRO    MAKE_ID('U','P','R','O')     //   Rachunek uproszczony


#define ID_SETT    MAKE_ID('S','E','T','T')     // Settings
#define ID_USER    'USER'                       // Dane u�ytkownika

#define ID_PRTD    MAKE_ID('P','R','T','D')     //   Printer device
#define ID_PRTU    MAKE_ID('P','R','T','U')     //   Printer unit
#define ID_WIDW    'WIDW'                       //   Page width (wide)
#define ID_WIDN    'WIDN'                       //   Page width (normal)
#define ID_WIDC    'WIDC'                       //   Page width (condensed)
#define ID_CONV    MAKE_ID('C','O','N','V')     //   Conversion

#define ID_POSA    'POSA'                       //   posnet_active
#define ID_SERD    'SERD'                       //   Serial device
#define ID_SERU    'SERU'                       //   Serial unit
#define ID_PTUA    'PTUA'                       //   ptu_a
#define ID_PTUB    'PTUB'                       //   ptu_b
#define ID_PTUC    'PTUC'                       //   ptu_c
#define ID_PTUD    'PTUD'                       //   ptu_d
#define ID_PTUZ    'PTUZ'                       //   ptu_z

#define ID_DRAW    'DRAW'                       //   posnet_open_drawer
#define ID_IDLE    'IDLE'                       //   posnet_idle
#define ID_IDL1    'IDL1'                       //   posnet_idle_text1
#define ID_IDL2    'IDL2'                       //   posnet_idle_text2
#define ID_NADR    'NADR'                       //   posnet_nadruki
#define ID_NAD1    'NAD1'                       //   posnet_nadruk_1
#define ID_NAD2    'NAD2'                       //   posnet_nadruk_2
#define ID_NAD3    'NAD3'                       //   posnet_nadruk_3

#define ID_FVOR    MAKE_ID('F','V','O','R')     //   Faktura VAT - numeracja
#define ID_FVTP    MAKE_ID('F','V','T','P')     //   Faktura VAT - rodzaj numeracji
#define ID_FMIN    'FMIN'                       //   Faktura VAT - min. dopuszczalna kwota
#define ID_FSRU    'FSRU'                       //   Faktura VAT - sugeruj wystawienie
                                                //                 rachunku je�li kwota za ma�a na FVat
#define ID_FVKO    'FVKO'                       //   Faktura VAT - korekta - numer
#define ID_FVKP    'FVKP'                       //   Faktura VAT - korekta - rodzaj numeracji
#define ID_FVPO    'FVPO'                       //   Faktura VAT - do paragonu
#define ID_FVPP    'FVPP'                       //   Faktura VAT - do paragonu - rodzaj numeracji
#define ID_PARA    'PARA'                       //   Paragon - numer

#define ID_RUOR    MAKE_ID('R','U','O','R')     //   Rachunek upr. - numeracja
#define ID_RUTP    MAKE_ID('R','U','T','P')     //   Rachunek upr. - rodzaj numeracji
#define ID_RUKO    'RUKO'
#define ID_RUKP    'RUKP'
#define ID_RUPO    'RUPO'
#define ID_RUPP    'RUPP'

#define ID_TOWN    MAKE_ID('T','O','W','N')     //   def_miasto
#define ID_UVAT    'UVAT'                       //   def_upvat
#define ID_EXPI    'EXPI'                       //   def_expire_offset
#define ID_UNLI    'UNLI'                       //   def_unlimited
#define ID_ADD     'ADD '                       //   def_addenabled
#define ID_FIFO    'FIFO'                       //   def_FIFO
#define ID_GR1     'GR1 '                       //   def_sprzedawca
#define ID_GR2     'GR2 '                       //   def_nabywca
#define ID_GR3     'GR3 '                       //   def_kosztowiec
#define ID_GR4     'GR4 '                       //   def_a
#define ID_GR5     'GR5 '                       //   def_b
#define ID_GR6     'GR6 '                       //   def_c
#define ID_CLON    'CLON'                       //   clone_filter

#define ID_VAT     MAKE_ID('V','A','T',' ')     //   def_vat
#define ID_UNIT    MAKE_ID('U','N','I','T')     //   def_miara

#define ID_DOT     'DOT '                       //   def_dot

#define ID_PRIN    MAKE_ID('P','R','I','N')     //   doc_print
#define ID_ORIG    'ORIG'                       //   original_text
#define ID_COPY    'COPY'                       //   copy_text
#define ID_DUPE    'DUPE'                       //   dupe_print
#define ID_DPTX    'DPTX'                       //   dupe_text
#define ID_SWW     'SWW '                       //   doc_sww
#define ID_CALL    'CALL'                       //   com_call
#define ID_SALE    'SALE'                       //   com_sale
#define ID_FEED    'FEED'                       //   formfeed
#define ID_UPVT    'UPVT'                       //   upvat_text
#define ID_PCS     'PCS '                       //   def_copies

#define ID_TXT1    'TXT1'                       //   text_fv1
#define ID_TXT2    'TXT2'                       //   text_fv2
#define ID_TXT3    'TXT3'                       //   text_fv3
#define ID_TXT4    'TXT4'                       //   text_fv4
#define ID_TXT5    'TXT5'                       //   text_fv5
#define ID_TXT6    'TXT6'                       //   text_fv6
#define ID_TXT7    'TXT7'                       //   text_fv7
#define ID_TXT8    'TXT8'                       //   text_fv8
#define ID_TXT9    'TXT9'                       //   text_fv9
#define ID_TXT0    'TXT0'                       //   text_fv0

#define ID_PAY     'PAY '                       // Rodzaje p�atno�ci



#define ID_DOC     'DOC '                       // Dokumenty (faktury, rachunki...)
#define ID_DOCT    'DOCT'                       //   typ dokumentu
#define ID_DOCN    'DOCN'                       //   numer dokumentu
//#define ID_CUST                               //   odbiorca dokumentu
#define ID_CREA    'CREA'                       //   data wystawienia
//#define ID_SALE    'SALE'                     //   data sprzeda�y
#define ID_PAYD    'PAYD'                       //   termin platno�ci
#define ID_WYST    'WYST'                       //   osoba wystawiaj�ca
#define ID_REST    'REST'                       //   reszta (w prefsach i na dokumencie)


#define ID_PRTC    'PRTC'                       // printer codes
#define ID_NORM    'NORM'                       //   normal
#define ID_BDON    'BDON'                       //   bold on
#define ID_BDOF    'BDOF'                       //   bold off
#define ID_ITON    'ITON'                       //   italics on
#define ID_ITOF    'ITOF'                       //   italics off
#define ID_UNON    'UNON'                       //   underline on
#define ID_UNOF    'UNOF'                       //   underline off
#define ID_CDON    'CDON'                       //   condensed on
#define ID_CDOF    'CDOF'                       //   condensed off


#define ID_ZAK     'ZAK '                       // zakupy

#endif


/*************************************************************************/

ULONG __stdargs DoSuperNew(struct IClass *cl, Object *obj, ULONG tag1, ...);


#define MUISERIALNR_CARLOS 2447
#define TAGBASE_CARLOS (TAG_USER | ( MUISERIALNR_CARLOS << 16))
#define TB TAGBASE_CARLOS


enum  {
      MUIA_Application_UniqueID = (int)(TB | 0x0000), /* GET - returns unique ID of   */
                                                      /*       the new project        */
                                                      /* SET - releases ID of given   */
                                                      /*       project                */
/*
      MUIM_EmployersList_Load,
      MUIM_EmployersList_Init,
*/

      MUIA_Type,

      MUIA_SmartCycle_Entries,                        /* SmartCycle entries {.S.}     */
      MUIA_SmartCycle_Active,

      MUIA_ZakupyList_Order,                          /* sortowanie listy zakup�w     */
      };




#define REFRESH  2                /* u�ywane przy edycji element�w (magazyn/klienci) */
#define ADD_ONLY 2                /* u�ywane przy dodawaniu grupy, bez prze��czana grupy aktywnej */

