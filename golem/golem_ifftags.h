
/* $Id: golem_ifftags.h,v 1.1 2003/01/01 20:40:53 carl-os Exp $ */


#define FV_NAME_HEAD "FVAT"
#define RU_NAME_HEAD "RUPR"

#ifndef IFF_HEAD
#define IFF_HEAD

#define ID_SYS     MAKE_ID('S','Y','S',' ')     // System information
#define ID_STRT    MAKE_ID('S','T','R','T')     //   last_start
#define ID_OPER    'OPER'                       //   last_operation

#define ID_MAG     MAKE_ID('M','A','G',' ')     // Magazyn
#define ID_PROD    MAKE_ID('P','R','O','D')     // Product information
#define ID_COUN    MAKE_ID('C','O','U','N')     // Products counter
#define ID_GRUP    MAKE_ID('G','R','U','P')     // Grupa magazynowa
#define ID_CUST    MAKE_ID('C','U','S','T')     // Kontrahenci
#define ID_RECE    MAKE_ID('R','E','C','E')     //  Osoba odbieraj�ca (faktury, rachunku)
#define ID_MEMO    'MEMO'                       //  Komentarz
#define ID_NOTE    'NOTE'                       //  Uwagi

#define ID_ANNO    MAKE_ID('A','N','N','O')
#define ID_VERS    MAKE_ID('V','E','R','S')

#define ID_ID      'ID  '
#define ID_NAME    MAKE_ID('N','A','M','E')     // Name
#define ID_TOTL	   'TOTL'						// Total (np. ogolna wartosc dokumentu)
#define ID_TNET    'TNET'						// Total NETTO
#define ID_TVAT    'TVAT'						// Total VAT


#define ID_NUMB    MAKE_ID('N','U','M','B')     // Numeracja dokument�w
#define ID_FVAT    MAKE_ID('F','V','A','T')     //   Faktura Vat
#define ID_UPRO    MAKE_ID('U','P','R','O')     //   Rachunek uproszczony


#define ID_SETT    MAKE_ID('S','E','T','T')     // Settings
#define ID_USER    'USER'                       // Dane u�ytkownika

#define ID_POSA    'POSA'                       //   posnet_active
#define ID_SERD    'SERD'                       //   Serial device
#define ID_SERU    'SERU'                       //   Serial unit
#define ID_PTUA    'PTUA'                       //   ptu_a
#define ID_PTUB    'PTUB'                       //   ptu_b
#define ID_PTUC    'PTUC'                       //   ptu_c
#define ID_PTUD    'PTUD'                       //   ptu_d
#define ID_PTUE    'PTUE'                       //   ptu_e
#define ID_PTUZ    'PTUZ'                       //   ptu_z

#define ID_DRAW    'DRAW'                       //   posnet_open_drawer
#define ID_IDLE    'IDLE'                       //   posnet_idle
#define ID_IDL1    'IDL1'                       //   posnet_idle_text1
#define ID_IDL2    'IDL2'                       //   posnet_idle_text2
#define ID_NADR    'NADR'                       //   posnet_nadruki
#define ID_NAD1    'NAD1'                       //   posnet_nadruk_1
#define ID_NAD2    'NAD2'                       //   posnet_nadruk_2
#define ID_NAD3    'NAD3'                       //   posnet_nadruk_3

#define ID_FVOR    MAKE_ID('F','V','O','R')     //   Faktura VAT - numeracja
#define ID_FVTP    MAKE_ID('F','V','T','P')     //   Faktura VAT - rodzaj numeracji
#define ID_FMIN    'FMIN'                       //   Faktura VAT - min. dopuszczalna kwota
#define ID_FSRU    'FSRU'                       //   Faktura VAT - sugeruj wystawienie
												//                 rachunku je�li kwota za ma�a na FVat
#define ID_FVKO    'FVKO'                       //   Faktura VAT - korekta - numer
#define ID_FVKP    'FVKP'                       //   Faktura VAT - korekta - rodzaj numeracji
#define ID_FVPO    'FVPO'                       //   Faktura VAT - do paragonu
#define ID_FVPP    'FVPP'                       //   Faktura VAT - do paragonu - rodzaj numeracji
#define ID_PARA    'PARA'                       //   Paragon - numer
#define ID_ORDE    'ORDE'                       //   Order   - numer
#define ID_FVPR    'FVPR'                       //   FV ProForma - numer (wewnetrzny)
#define ID_ODRE    'ODRE'                       //   sprzedaz odreczna - numer

#define ID_RUOR    MAKE_ID('R','U','O','R')     //   Rachunek upr. - numeracja
#define ID_RUTP    MAKE_ID('R','U','T','P')     //   Rachunek upr. - rodzaj numeracji
#define ID_RUKO    'RUKO'
#define ID_RUKP    'RUKP'
#define ID_RUPO    'RUPO'
#define ID_RUPP    'RUPP'

#define ID_OROR    'OROR'                       //   Zam�wienie - numer
#define ID_ORTP    'ORTP'                       //   Zam�wienie - rodzaj numeracji

#define ID_TOWN    MAKE_ID('T','O','W','N')     //   def_miasto
#define ID_UVAT    'UVAT'                       //   def_upvat
#define ID_EXPI    'EXPI'                       //   def_expire_offset
#define ID_UNLI    'UNLI'                       //   def_unlimited
#define ID_ADD     'ADD '                       //   def_addenabled
#define ID_FIFO    'FIFO'                       //   def_FIFO
#define ID_GR1     'GR1 '                       //   def_sprzedawca
#define ID_GR2     'GR2 '                       //   def_nabywca
#define ID_GR3     'GR3 '                       //   def_kosztowiec
#define ID_GR4     'GR4 '                       //   def_a
#define ID_GR5     'GR5 '                       //   def_b
#define ID_GR6     'GR6 '                       //   def_c
#define ID_CLON    'CLON'                       //   clone_filter

#define ID_MARZ    'MARZ'                       //   sta�a_mar�a_procentowa

#define ID_VAT     MAKE_ID('V','A','T',' ')     //   def_vat
#define ID_UNIT    MAKE_ID('U','N','I','T')     //   def_miara

#define ID_DOT     'DOT '                       //   def_dot

#define ID_PRIN    MAKE_ID('P','R','I','N')     //   doc_print
#define ID_ORIG    'ORIG'                       //   original_text
#define ID_COPY    'COPY'                       //   copy_text
#define ID_DUPE    'DUPE'                       //   dupe_print
#define ID_DPTX    'DPTX'                       //   dupe_text
#define ID_SWW     'SWW '                       //   doc_sww
#define ID_CALL    'CALL'                       //   com_call
#define ID_SALE    'SALE'                       //   com_sale
#define ID_FEED    'FEED'                       //   formfeed
#define ID_UPVT    'UPVT'                       //   upvat_text
#define ID_PCS     'PCS '                       //   def_copies

#define ID_TXT1    'TXT1'                       //   text_fv1
#define ID_TXT2    'TXT2'                       //   text_fv2
#define ID_TXT3    'TXT3'                       //   text_fv3
#define ID_TXT4    'TXT4'                       //   text_fv4
#define ID_TXT5    'TXT5'                       //   text_fv5
#define ID_TXT6    'TXT6'                       //   text_fv6
#define ID_TXT7    'TXT7'                       //   text_fv7
#define ID_TXT8    'TXT8'                       //   text_fv8
#define ID_TXT9    'TXT9'                       //   text_fv9
#define ID_TXT0    'TXT0'                       //   text_fv0

#define ID_PAY     'PAY '                       // Rodzaje p�atno�ci



#define ID_DOC     'DOC '                       // Dokumenty (faktury, rachunki...)
#define ID_SELL    'SELL'                       //   dane sprzedawcy (z ustawie�)
#define ID_DOCT    'DOCT'                       //   typ dokumentu
#define ID_DOCN    'DOCN'                       //   numer dokumentu
//#define ID_CUST                               //   odbiorca dokumentu
#define ID_CREA    'CREA'                       //   data wystawienia
//#define ID_SALE    'SALE'                     //   data sprzeda�y
#define ID_PAYD    'PAYD'                       //   termin platno�ci
#define ID_WYST    'WYST'                       //   osoba wystawiaj�ca
#define ID_REST    'REST'                       //   reszta (w prefsach i na dokumencie)
#define ID_PARD    'PARD'                       //   domy�lny stan gad�etu PARAGON (FVat)
#define ID_PARR    'PARR'                       //   domy�lny stan gad�etu PARAGON (R. Upr)
#define ID_PARP    'PARP'                       //   domy�lny stan gad�etu PARAGON (Paragon)
#define ID_PARI    'PARI'                       //   domy�lny stan gad�etu dla pozosta�ych

												//     (u�ywany gdy drukarka fiskalna aktywna)


// ustawienia drukarki

#define ID_PRT     'PRT '

#define ID_TYPE    'TYPE'

#define ID_PATH    'PATH'                       //   Default path for FILE printer
#define ID_PRTD    MAKE_ID('P','R','T','D')     //   Printer device
#define ID_PRTU    MAKE_ID('P','R','T','U')     //   Printer unit
#define ID_WIDW    'WIDW'                       //   Page width (wide)
#define ID_WIDN    'WIDN'                       //   Page width (normal)
#define ID_WIDC    'WIDC'                       //   Page width (condensed)
#define ID_CONV    MAKE_ID('C','O','N','V')     //   Conversion
#define ID_PGON    'PGON'                       //   Page Mode On (stronicowanie wydruk�w)
#define ID_PGHE    'PGHE'                       //   Page Height
#define ID_PGFO    'PGFO'                       //   Page Footer (na dokumentach 1stronicowych)

#define ID_PRTC    'PRTC'                       // printer codes
#define ID_NORM    'NORM'                       //   normal
#define ID_BDON    'BDON'                       //   bold on
#define ID_BDOF    'BDOF'                       //   bold off
#define ID_ITON    'ITON'                       //   italics on
#define ID_ITOF    'ITOF'                       //   italics off
#define ID_UNON    'UNON'                       //   underline on
#define ID_UNOF    'UNOF'                       //   underline off
#define ID_CDON    'CDON'                       //   condensed on
#define ID_CDOF    'CDOF'                       //   condensed off


#define ID_BANK    MAKE_ID('B','A','N','K')     // Bank
#define ID_CENT    MAKE_ID('C','E','N','T')     // Centrala

#define ID_MAP0    'MAP0'                       // Mapowanie drukarek
#define ID_MAP1    'MAP1'                       // Mapowanie drukarek
#define ID_MAP2    'MAP2'                       // Mapowanie drukarek
#define ID_MAP3    'MAP3'                       // Mapowanie drukarek
#define ID_MAP4    'MAP4'                       // Mapowanie drukarek
#define ID_MAP5    'MAP5'                       // Mapowanie drukarek
#define ID_MAP6    'MAP6'                       // Mapowanie drukarek
#define ID_MAP7    'MAP7'                       // Mapowanie drukarek
#define ID_MAP8    'MAP8'                       // Mapowanie drukarek
#define ID_MAP9    'MAP9'                       // Mapowanie drukarek

#define ID_ZAK     'ZAK '                       // zakupy




#define ID_SQLS    'SQLS'                      // SQL Settings
#define ID_CONN    'CONN'                      // connection_type
#define ID_HOST    'HOST'                      // TCP/IP host
#define ID_PORT    'PORT'                      // TCP/IP port

#endif

