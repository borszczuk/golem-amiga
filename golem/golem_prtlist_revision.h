#define PRTLIST_VERSION 15
#define PRTLIST_REVISION 1
#define PRTLIST_VERSIONREVISION "15.1"
#define PRTLIST_DATE "10.06.2002"
#define PRTLIST_VERS "Golem_PrtList.mcc 15.1"
#define PRTLIST_VSTRING "Golem_PrtList.mcc 15.1 (10.06.2002)\r\n"
#define PRTLIST_VERSTAG "$VER: Golem_PrtList.mcc 15.1 (10.06.2002)"
