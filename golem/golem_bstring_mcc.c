
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 


/*** includes ***/

#include <clib/alib_protos.h>
#include <clib/graphics_protos.h>
#include <clib/utility_protos.h>
#include <clib/muimaster_protos.h>
#include <pragmas/muimaster_pragmas.h>
#include <libraries/mui.h>

#if MUIMASTER_VLATEST <= 14
#include <mui/mui33_mcc.h>
#endif


#define MYDEBUG 0
//#define USE_TI

#include "golem_bstring_mcc.h"


#ifdef  USE_TI
 #include "//vapor/textinput/textinput_mcc.h"
 #define SUPERCLASS  MUIC_Textinput
#else
 #include <mui/betterstring_mcc.h>
 #define SUPERCLASS  MUIC_BetterString
#endif

#include <mui/muiundoc.h>

#define __NAME      "Golem_BString"
#define CLASS       MUIC_Golem_BString


struct Data
{

	LONG  Dummy;

};

#include "golem_bstring_revision.h"

#define UserLibID BSTRING_VERSTAG " � 1998-2002 Marcin Orlowski <carlos@wfmh.org.pl>"
#define MASTERVERSION 14

#define VERSION BSTRING_VERSION
#define REVISION BSTRING_REVISION
#include "mccheader.c"

/// OM_NEW

ULONG ASM BString_New(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct Data *data;


		if (!(obj = (Object *)DoSuperMethodA(cl, obj, msg)))
				return(0);

		/*** init data ***/
		data = INST_DATA(cl, obj);



		/*** trick to set arguments ***/
		msg->MethodID = OM_SET;
		DoMethodA(obj, (Msg)msg);
		msg->MethodID = OM_NEW;

		return((ULONG)obj);
}
//|
/// OM_GET
static ULONG ASM BString_Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ProductList_Data *data = INST_DATA(cl,obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

	switch(((struct opGet *)msg)->opg_AttrID)
	   {
	   case MUIA_Version:
			*store = BSTRING_VERSION;
			return(TRUE);
			break;

	   case MUIA_Revision:
			*store = BSTRING_REVISION;
			return(TRUE);
			break;

	   }

	return(DoSuperMethodA(cl, obj, msg));
}
//|

/// GoActive

ULONG BString_GoActive(struct IClass *cl, Object *obj, Msg msg)
{

#ifdef USE_TI
	DoMethod( obj, MUIM_Textinput_DoMarkAll );
#else
	nnset( obj, MUIA_String_BufferPos, 0 );
	nnset( obj, MUIA_BetterString_SelectSize, 512 );
#endif

	return(0);

}

//|
/// GoInactive

ULONG BString_GoInactive(struct IClass *cl, Object *obj, Msg msg)
{

#ifdef USE_TI
	nnset( obj, MUIA_Textinput_MarkStart, MUIV_Textinput_NoMark );
	nnset( obj, MUIA_Textinput_MarkEnd  , MUIV_Textinput_NoMark );
#else
	nnset( obj, MUIA_BetterString_SelectSize, 0 );
#endif

	return(0);

}

//|

/// Dispatcher
ULONG ASM SAVEDS _Dispatcher(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{

	switch (msg->MethodID)
	   {
	   case OM_NEW: return(BString_New (cl, obj, (APTR)msg));
	   case OM_GET: return(BString_Get (cl, obj, (APTR)msg));

	   case MUIM_GoActive:
		   {
		   BString_GoActive(cl, obj, (APTR)msg);
		   break;
		   }

	   case MUIM_GoInactive:
		   {
		   BString_GoInactive(cl, obj, (APTR)msg);
		   break;
		   }

	   }

	return((ULONG)DoSuperMethodA(cl,obj,msg));

}
//|

