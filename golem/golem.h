
/*
** $Id: golem.h,v 1.1 2003/01/01 20:40:54 carl-os Exp $
*/


//#define DEMO

//#define BETAONLY

//#define TOOLBAR
//#define USE_NLIST
#define USE_BETTERSTRING

#define MYDEBUG 0

/*######################################################################*/

#include "golem_includes.h"
#include "golem_structs.h"
#include "golem_revision.h"
#include "golem_strings.h"
#include "golem_protos.h"


#ifdef  StringObject
#undef  StringObject
//#define StringObject  NewObject(CL_BString->mcc_Class, NULL
#define StringObject  Golem_BStringObject
#endif


#define TITLE "WFMH Golem OpenSource " VERSIONREVISION

#define EMAIL "carlos@wfmh.org.pl"
#define WWW   "http://golem-amiga.sf.net/"

#include "golem_macros.h"
#include "golem_windowid.h"



/// ReturnIDs

enum{ID_OK = 1,
	 ID_CANCEL,
	 ID_USE,

	 ID_ACCESS,
	 ID_LOAD,
	 ID_SAVE,
	 ID_EDIT,

	 ID_INFO,
	 ID_INDEX,
	 ID_SORT,
	 ID_MOVE,
	 ID_ACTIVATE,
	 ID_ACCEPT,
	 ID_DELETE,
	 ID_FIND,
	 ID_ADD,
	 ID_BACK,
	 ID_NEXT,
	 ID_PREVIOUS,
	 ID_CLEANUP,
	 ID_DELETE_AND_SKIP,
	 ID_SELECT,
	 ID_SELECT_0,
	 ID_SELECT_1,
	 ID_SELECT_2,
	 ID_SELECT_3,
	 ID_SELECT_4,
	 ID_SELECT_5,
	 ID_SELECT_6,
	 ID_SELECT_7,
	 ID_SELECT_8,
	 ID_SELECT_9,
	 ID_IMPORT,
	 ID_EXPORT,



	 ID_PAGE_0 = 50,
	 ID_PAGE_1,
	 ID_PAGE_2,



	 ID_STARTUP_CLOSED   = 95,
	 ID_STARTUP_URL,

	 ID_TOOLBAR_SPRZEDAZ = 100,
		ID_TOOLBAR_VAT,
		ID_TOOLBAR_UPROSZCZONY,
		ID_TOOLBAR_VAT_PAR,
		ID_TOOLBAR_UPROSZCZONY_PAR,
		ID_TOOLBAR_PARAGON,
		ID_TOOLBAR_ORDER,
		ID_TOOLBAR_VAT_PROFORMA,
		ID_TOOLBAR_NIEUDOKUMENTOWANA,
		ID_TOOLBAR_KOREKTA,

	 ID_TOOLBAR_ZAKUPY     = 110,
		ID_TOOLBAR_ZAKUPY_KOSZTY,

	 ID_TOOLBAR_BAZYDANYCH = 120,
		ID_TOOLBAR_MAGAZYN,
		ID_TOOLBAR_KONTRAHENCI,
		ID_TOOLBAR_USERS,
		ID_TOOLBAR_SETTINGS_PAY,
		ID_TOOLBAR_SETTINGS_UNIT,

	 ID_TOOLBAR_ROZLICZENIA = 130 ,
		ID_TOOLBAR_ZAMKNIJ,
		ID_TOOLBAR_ZAMKNIJ_MIESIAC,
		ID_TOOLBAR_ZAMKNIJ_ROK,
		ID_TOOLBAR_POSNET_RAPORT_DOBOWY,
		ID_TOOLBAR_POSNET_RAPORT_OKRESOWY,
		ID_TOOLBAR_POSNET_STAN_KASY,
		ID_TOOLBAR_POSNET_RAPORT_ZMIANY,

	 ID_TOOLBAR_SYSTEM = 140,
		ID_TOOLBAR_SETTINGS,
		ID_TOOLBAR_NUMERACJA,
		ID_TOOLBAR_DRUKARKI,
		ID_TOOLBAR_TIP,
		ID_TOOLBAR_INSPEKTOR,

	 ID_TOOLBAR_REDAGOWANIE = 150,
		ID_TOOLBAR_RED_BIE��CE,
		ID_TOOLBAR_RED_OD�O�ONE,
		ID_TOOLBAR_RED_PRZELEW,
	 ID_TOOLBAR_LOGIN = 160,
	 ID_TOOLBAR_LOGOUT,
	 ID_TOOLBAR_LOGINOUT,
	 ID_TOOLBAR_ABOUT,

	 ID_TOOLBAR_MAINMENU  = 199,

	 ID_FIRST_GROUP = 250,
	 ID_PREV_GROUP,
	 ID_NEXT_GROUP,
	 ID_LAST_GROUP,

	 ID_MAG_WYBIERZ = 300,
	 ID_MAG_ZNAJD�,
	 ID_MAG_DODAJ,
	 ID_MAG_DODAJ_ALT,
	 ID_MAG_EDYTUJ,
	 ID_MAG_SKASUJ,
	 ID_MAG_SKASUJ_AND_SKIP,
	 ID_MAG_PRINT,
	 ID_MAG_FILTER,
	 ID_MAG_FILTER_ALL,

	 ID_MAG_USU�_GRUP�,
	 ID_MAG_DODAJ_GRUP�,
	 ID_MAG_EDYCJA_GRUPY,
	 ID_MAG_ZMIE�_GRUP�,
	 ID_MAG_WYBIERZ_INDEX,

	 ID_CUST_DODAJ  = 350,
	 ID_CUST_USU�,
	 ID_CUST_EDYTUJ,
	 ID_CUST_USU�_AND_SKIP,
	 ID_CUST_KOPERTA,

	 ID_CUST_FILTER,
	 ID_CUST_FILTER_ALL,

	 ID_CUST_UPVAT,
	 ID_CUST_SC,

	 ID_CUST_SEARCH,
	 ID_CUST_SEARCH_ACK,


	 ID_KLIENT_WYBIERZ  = 400,
	 ID_FAK_KLIENT_ODBIORCA,
	 ID_FAK_PROD_ACK,
	 ID_FAK_PROD_ACK_LV,
	 ID_FAK_CALC_WARTO��,
	 ID_FAK_USU�,
//     ID_FAK_TYP_P�ATNO�CI,
	 ID_FAK_FREE_HAND,
	 ID_FAK_ODBIORCA,
	 ID_FAK_PROD_ACK_INDEX,
	 ID_FAK_OD���,
	 ID_FAK_EDIT,
	 ID_FAK_CALC_WARTO��_2,


	 ID_SET_UNIT_ADD   = 450,
	 ID_SET_UNIT_EDIT,
	 ID_SET_UNIT_DEL,
	 ID_SET_UNIT_UPDATE,

	 ID_ED_UNIT_UPDATE = 460,

	 ID_SET_PAY_ADD = 470,
	 ID_SET_PAY_EDIT,
	 ID_SET_PAY_DEL,

	 ID_USER_ADD = 500,
	 ID_USER_EDIT,
	 ID_USER_DEL,

	 ID_RED_DOC_TYPE = 550,
	 ID_RED_VIEW,
	 ID_RED_RED,
	 ID_RED_CANCEL,
	 ID_RED_PRINT,
	 ID_RED_KOREKTA,
	 ID_RED_PRINT_DOC,
	 ID_RED_EDIT_DOC,

	 ID_RED_NAZWAWYBIERZ,
	 ID_RED_NIPWYBIERZ,
	 ID_RED_REGONWYBIERZ,

	 ID_MAG_MARZA_ACK = 570,
	 ID_MAG_BRUTTO_ACK,
	 ID_MAG_ZAKUP_ACK,
	 ID_MAG_VAT_ACK,
	 ID_MAG_MARZA_PROCENT_ACK,


	 ID_FREE_MARZA_ACK  = 600,
	 ID_FREE_BRUTTO_ACK,
	 ID_FREE_ZAKUP_ACK,
	 ID_FREE_VAT_ACK,
	 ID_FREE_MARZA_PROCENT_ACK,


	 ID_STR_ACK,


	 ID_ZAKE_CALC_P11 = 650,
	 ID_ZAKE_CALC_P13,
	 ID_ZAKE_CALC_P15,
	 ID_ZAKE_CALC_P17,

	};

//|




/* Rodzaje dokument�w i rodzaje numeracji */

enum{ FAKTURA_VAT,
	  RACHUNEK,
	  FVAT_KOR,
	  RACH_KOR,
	  FVAT_PAR,
	  RACH_PAR,
	  PARAGON,
	  ORDER,                  // zamowienie od klienta
	  FVAT_PROFORMA,
	  ODRECZNA,               // sprzeda� odr�czna

	  DOCUMENT_COUNT,



	  ZAKUPY = 0x80,          // dokumenty dodatkowe

	  DOCUMENT_COUNT_2,



	  OD_FAKTURA_VAT = 0xa0,  // dokumenty od�o�one podczas wystawiania

	  OD_RACHUNEK,
	  OD_FVAT_KOR,
	  OD_RACH_KOR,
	  OD_FVAT_PAR,
	  OD_RACH_PAR,
	  OD_FVAT_PROFORMA,

	  DOCUMENT_COUNT_3,



	  PRT_CENNIK_DETAL = 0xf0,     // dokumenty ktore nie sa zapisywane
	  PRT_CENNIK_HURT,
	  PRT_CENNIK_STAN,
	  PRT_SPIS_Z_NATURY,
	  PRT_SPIS_LIMIT,
	   
	  PRT_PRZELEW,


	  DOCUMENT_COUNT_4,

	};


enum{ NR_RRRR,
	  NR_RR,
	  NR_MM_RRRR,
	  NR_MM_RR,
	  RRRR_NR,
	  RR_NR,
	  RRRR_MM_NR,
	  RR_MM_NR,
	};


/* settings.printer_charset */

enum{ PRINT_AMIGAPL,
	  PRINT_NOPL,
	  PRINT_CHR8,
	  PRINT_ISO,
	  PRINT_LATIN2,
	  PRINT_MAZOVIA,
	  PRINT_SUXX95,
	};

enum{ POSNET_RAPORT_DOBOWY,
	  POSNET_RAPORT_OKRESOWY,
	};

enum{ VAT_ZW = 0,
	  VAT_0,
	  VAT_7,
	  VAT_17,
	  VAT_22,
	  VAT_3,
	};

enum{ SORT_ZAKUPY = 0,         /* rodzaj SORT requestera */
	  SORT_DOCSCAN,
	  SORT_DOCSCAN_OD�O�ONE,
	};

enum{ DOKUMENTY_BIE��CE = 0,   /* rodzaj dokument�w do przeszukiwania w oknie redagowania */
	  DOKUMENTY_OD�O�ONE,
	};

// usun klientow - flagi
enum{ UK_REMOVE_DELETED = 0,
	  UK_REMOVE_ALL,
	  UK_KEEP_TEMP,
	};


#define F_CUSTED_NIP_REQUIRED  1   /* wymagane podanie NIPu przy edycji klienta */


//extern   struct  Locale    *MyLocale;

#define RB CHECKIT
#define TG CHECKIT|MENUTOGGLE

/*************************************************************************/

#include "golem_dirs.h"

/*************************************************************************/

#include "golem_ifftags.h"

/*************************************************************************/

ULONG __stdargs DoSuperNew(struct IClass *cl, Object *obj, ULONG tag1, ...);


#ifndef CARLOS_MUI
#define MUISERIALNR_CARLOS 2447
#define TAGBASE_CARLOS (TAG_USER | ( MUISERIALNR_CARLOS << 16))
#define CARLOS_MUI
#endif

#define TB TAGBASE_CARLOS

enum  {
	  MUIA_Application_UniqueID = (int)(TB | 0x0000), /* GET - returns unique ID of   */
													  /*       the new project        */
													  /* SET - releases ID of given   */
													  /*       project                */
/*
	  MUIM_EmployersList_Load,
	  MUIM_EmployersList_Init,
*/

	  MUIA_Type,

	  MUIA_SmartCycle_Entries,                        /* SmartCycle entries {.S.}     */
	  MUIA_SmartCycle_Active,

	  MUIA_ZakupyList_Order,                          /* sortowanie listy zakup�w     */
	  MUIA_DocumentList_Order,


	  MUIA_ProdList_IndexCount,                       /* liczba produktow na liscie ktore maja wypelnione pole index */
	  MUIA_ProdList_SWWCount,
	  };


//struct MUIP_List_InsertSingle     { ULONG MethodID; APTR entry; LONG pos; };




#define REFRESH  2                /* u�ywane przy edycji element�w (magazyn/klienci) */
#define ADD_ONLY 2                /* u�ywane przy dodawaniu grupy, bez prze��czana grupy aktywnej */



#define PAGE_FOOTER_HEIGHT 2      /* wysoko�� stopki przy podziale dokumentu na strony (w liniach) */
