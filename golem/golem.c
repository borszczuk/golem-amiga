
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
** $Id: golem.c,v 1.1 2003/01/01 20:40:53 carl-os Exp $.
*/

/// Includes

#include "golem.h"

#define SAVEDS __saveds
#define ASM __asm
#define REG(x) register __ ## x

LONG __near __stack = 14000;

char BaseScreenTitle[] = "WFMH Golem " VERSIONREVISION " (" DATE ") � 1997-2002 Marcin Or�owski";

char ScreenTitle[256];


//long ScreenTitleLen = sizeof(ScreenTitle);
char GuideName[256] = "";

/// STRUCT: EasyStruct
struct   EasyStruct MyEasy = {            // Requesters used if no MUI available
         sizeof (struct EasyStruct),
         0,
         TITLE,
         "%s",
         "Ok",
};
//|
/// STRUCT: Library *

struct  Library *WfmhBase        = NULL;
struct  Library *MuiToolkitBase  = NULL;
struct  Library *MUIMasterBase   = NULL;
struct  Library *LocaleBase      = NULL;
struct  Locale  *MyLocale        = NULL;

//|
/// DoSuperNew()

ULONG __stdargs DoSuperNew(struct IClass *cl, Object *obj, ULONG tag1, ...)
{
        return(DoSuperMethod(cl, obj, OM_NEW, &tag1, NULL));
}
//|

long __near __oslibversion = 38;           // Only 2.1 and up

#include "Golem_structs.h"

//|

/// Millenium Message
#ifdef DEMO
    char fuck_off[] = "Zgubi�e� co� frajerze? Bo raczej nie znajdzesz tego czego szukasz... Serdeczne fak ju dupku.";
#endif
//|


enum{PAGE_MAIN=0,
         PAGE_SPRZEDAZ,
         PAGE_ZAKUPY,
         PAGE_REDAGOWANIE,
         PAGE_BAZYDANYCH,
         PAGE_ROZLICZENIA,
         PAGE_SYSTEM,

        };

struct DateStamp    Today;

struct SystemInfo  sys =
{
        0                      // last_start
};

struct UserList  *logged_user = NULL;

struct List      jedn_miary;
struct List      p�atno�ci;
struct List      kontrahenci;
struct List      u�ytkownicy;
struct List      grupy;
struct List      drukarki;
struct GrupaList *grupa_aktywna = NULL;
char   IDGrup[256];


/// Global variables

char *EMPTY[]          = {"\0338\033b", "\0"}; // atrybut produktu o ilo�ci 0
char *VatTable[]       = {"zw.", "0%", "7%", "17%", "22%", "3%", NULL};
char *VatTablePosnet[] = { "Z" , "D" , "C" , "B"  ,  "A" , "E" , NULL};
char VatValTable[]     = {  0,    0,    7,    17,    22  , 3         };


char *list_class     = MUIC_List;
char *listview_class = MUIC_Listview;
int  using_nlist = 0;


Object *app = NULL;                          /* APPLICATION OBJECT */


//|

/// CleanUp
void CleanUp(void)
{

   DeleteClasses();

   if(MyLocale)          { CloseLocale(MyLocale); MyLocale = NULL; }
   if(LocaleBase)        { CloseLibrary(LocaleBase); LocaleBase = NULL; }
   if( WfmhBase )        { CloseLibrary( WfmhBase ); WfmhBase = NULL; }
   if( MuiToolkitBase )  { CloseLibrary( MuiToolkitBase ); MuiToolkitBase = NULL; }
   if(MUIMasterBase)     { CloseLibrary(MUIMasterBase); MUIMasterBase = NULL; }

}
//|

/// Price2String
char __inline *Price2String(double val)
{
static char out[16];

        sprintf(out, "%#.2f", val);
        *strchr(out, '.') = settings.def_dot;

        return(out);
}
//|
/// String2Price
double __inline String2Price(char *string)
{
char *dot;
static char str[30];

        stccpy(str, string, 30);
        if(dot = strchr(str, settings.def_dot))
           {
           *dot = '.';
           stccpy(str, str, dot - str + 4);  // because this function includes NULL in the lenght
           }

        return(atof(str));
}
//|

/// Mar�a2String
char __inline *Mar�a2String(double val)
{
static char out[20];

        sprintf(out, "%#.2f", val);
        *strchr(out, '.') = settings.def_dot;

        return(out);
}
//|
/// String2Mar�a
double __inline String2Mar�a(char *string)
{
char *dot;
static char str[30];

        stccpy(str, string, 30);
        if(dot = strchr(str, settings.def_dot))
           {
           *dot = '.';
           stccpy(str, str, dot - str + 4);  // because this function includes NULL in the length
           }

        return(atof(str));
}
//|


/// GROUP LIST CLASS

struct MUI_CustomClass *CL_GroupList = NULL;

/// Data
struct GroupList_Data
{
   ULONG Dummy;
};
//|

/// Group list hooks
/// HOOK: GroupList_Display
void * __saveds __asm GroupList_Display(register __a2 char **array, register __a1 struct GrupaList *node)
{
static char Del[6+4],
                        Name[GROUP_NAME_LEN+10],
                        Ilo��[10+5];


   if(node)
         {
         char _Empty[10];

         if( node->gm_grupa.Typ == TYP_USLUGA )
           strcpy(_Empty, "\033b");
         else
           _Empty[0] = 0;


         strcat( _Empty, EMPTY[(node->Ilo�� > 0)] );

         sprintf(Del  , "%s%s" , _Empty, ((node->Deleted==FALSE) ? " " : "*"));
         sprintf(Name , "%s%s" , _Empty, node->gm_grupa.Nazwa);
         sprintf(Ilo��, "%s%ld", _Empty, node->Ilo��);

         *array++ = Del;
         *array++ = Name;
         *array++ = Ilo��;
         }
   else
         {
         *array++ = MSG_PROD_LIST_DEL;
         *array++ = MSG_PROD_LIST_NAME;
         *array++ = MSG_PROD_LIST_AMOUNT;
         }


         return(0);
}
//|
/// HOOK: GroupList_CompareStr
LONG __saveds __asm GroupList_CompareStr(register __a1 struct GrupaList *Node1, register __a2 struct GrupaList *Node2)
{
        return((LONG)StrnCmp(MyLocale, Node1->gm_grupa.Nazwa, Node2->gm_grupa.Nazwa, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG GroupList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook GroupList_DisplayHook     = { {NULL, NULL}, (VOID *)GroupList_Display   ,NULL,NULL };
static const struct Hook GroupList_CompareHookStr  = { {NULL, NULL}, (VOID *)GroupList_CompareStr,NULL,NULL };

        obj = (Object *)DoSuperNew(cl,obj,
                                        InputListFrame,
                                        _MUIA_List_AutoVisible  , TRUE,
                                        _MUIA_List_DisplayHook  , &GroupList_DisplayHook,
                                        _MUIA_List_CompareHook  , &GroupList_CompareHookStr,
                                        _MUIA_List_Format       , "MIW=1 MAW=-1 COL=0 BAR,"
                                                                                          "MIW=1 MAW=89 COL=1 BAR,"
                                                                                          "MIW=1 MAW=10 COL=2 P=\033r",
                                        _MUIA_List_Title        , TRUE,

                                         MUIA_NList_TitleSeparator, TRUE,
                                         MUIA_NList_AutoCopyToClip, TRUE,

                                TAG_MORE, msg->ops_AttrList);

          if(obj)
                  {
                  struct GroupList_Data *data = INST_DATA(cl,obj);

                  msg->MethodID = OM_SET;
                  DoMethodA(obj, (Msg)msg);
                  msg->MethodID = OM_NEW;

                  }


          return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG GroupList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

        switch (msg->MethodID)
           {
           case OM_NEW                   : return(GroupList_New       (cl, obj, (APTR)msg));
           }

        return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
/// FAKTURA PRODUCT LIST CLASS

#define MSG_FAK_PROD_LIST_NAME    "\0338\033bNazwa"
#define MSG_FAK_PROD_LIST_SWW     "\0338\033bSWW"
#define MSG_FAK_PROD_LIST_WARTOSC "\0338\033bWarto��"
#define MSG_FAK_PROD_LIST_NETTO   "\0338\033bNetto"
#define MSG_FAK_PROD_LIST_VAT     "\0338\033bVAT"
#define MSG_FAK_PROD_LIST_JEDN    "\0338\033bJ.m."
#define MSG_FAK_PROD_LIST_NAME    "\0338\033bNazwa"
#define MSG_FAK_PROD_LIST_AMOUNT  "\0338\033bIlo��"
#define MSG_FAK_PROD_LIST_SUM     "\0338\033bBrutto"


struct MUI_CustomClass *CL_FakturaProductList = NULL;


/// Data
struct FakturaProductList_Data
{
   ULONG Dummy;
};
//|

/// Product list hooks
/// HOOK: FakturaProductList_Display
void * __saveds __asm FakturaProductList_Display(register __a2 char **array, register __a1 struct ProductList *node)
{
static char Attr[2 + 2 + 1],
                        Name[PROD_NAME_LEN+10],
                        Ilo��[15+5],
                        CenaNetto[15+5],
                        WartNetto[15+5],
                        Vat[15+5],
                        WartBrutto[15+5];

static double brutto;


   if(node)
         {
         double _ilo�� = node->pl_prod.Ilo��;
         double _vat;


         if( node->pl_prod.Typ == TYP_USLUGA )
           {
           strcpy( Attr, "\033b" );
           }
         else
           Attr[0] = 0;


         // Nazwa

         sprintf(Name   , "%s%s" , Attr, node->pl_prod.Nazwa);


         // Ilo��

         sprintf(Ilo��  , "%s%#.2f", Attr, _ilo��);


         // cena netto produktu

         brutto = node->pl_prod.Zakup + node->pl_prod.Mar�a;
         brutto = brutto - CalcRabat(brutto, node->pl_prod.Rabat);

         sprintf(CenaNetto, "%s%#.2f", Attr, brutto);


         // warto�� netto produktu (najpierw VAT liczymy)

         brutto = _ilo�� * brutto;

         sprintf(WartNetto, "%s%#.2f", Attr, brutto);

         _vat = CalcVat(brutto, node->pl_prod.Vat);
         sprintf(Vat, "%s%#.2f", Attr, _vat);

         brutto = brutto + _vat;
         sprintf(WartBrutto, "%s%s", Attr, Price2String(brutto));

         *array++ = Name;
         *array++ = Ilo��;
         *array++ = CenaNetto;
         *array++ = WartNetto;
         *array++ = Vat;
         *array++ = WartBrutto;
         }
   else
         {
         *array++ = MSG_FAK_PROD_LIST_NAME;
         *array++ = MSG_FAK_PROD_LIST_AMOUNT;
         *array++ = MSG_FAK_PROD_LIST_NETTO;
         *array++ = MSG_FAK_PROD_LIST_WARTOSC;
         *array++ = MSG_FAK_PROD_LIST_VAT;
         *array++ = MSG_FAK_PROD_LIST_SUM;
         }


         return(0);
}
//|
/// HOOK: FakturaProductList_CompareStr
LONG __saveds __asm FakturaProductList_CompareStr(register __a1 struct ProductList *Node1, register __a2 struct ProductList *Node2)
{
        return((LONG)StrnCmp(MyLocale, Node1->pl_prod.Nazwa, Node2->pl_prod.Nazwa, -1, SC_COLLATE2));
}
//|

/// HOOK: FakturaProductList_Construct
APTR __saveds __asm FakturaProductList_Construct(register __a2 APTR pool, register __a1 struct ProductList *node)
{
struct  ProductList *new;

                if( new = malloc(sizeof(struct ProductList)) )
                   {
                   memcpy(new, node, sizeof(struct ProductList));
                   }

                return(new);
}
//|
/// HOOK: FakturaProductList_Destruct
VOID __saveds __asm FakturaProductList_Destruct(register __a2 APTR pool, register __a1 struct ProductList *node)
{
                free(node);
}
//|
//|

/// OM_NEW

ULONG FakturaProductList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook FakturaProductList_ConstructHook   = { {NULL, NULL}, (VOID *)FakturaProductList_Construct ,NULL,NULL };
static const struct Hook FakturaProductList_DestructHook    = { {NULL, NULL}, (VOID *)FakturaProductList_Destruct  ,NULL,NULL };
static const struct Hook FakturaProductList_DisplayHook     = { {NULL, NULL}, (VOID *)FakturaProductList_Display   ,NULL,NULL };
static const struct Hook FakturaProductList_CompareHookStr  = { {NULL, NULL}, (VOID *)FakturaProductList_CompareStr,NULL,NULL };

        obj = (Object *)DoSuperNew(cl,obj,
                                        InputListFrame,
                                        _MUIA_List_AutoVisible  , TRUE,
                                        _MUIA_List_ConstructHook, &FakturaProductList_ConstructHook,
                                        _MUIA_List_DestructHook , &FakturaProductList_DestructHook,
                                        _MUIA_List_DisplayHook  , &FakturaProductList_DisplayHook,
                                        _MUIA_List_CompareHook  , &FakturaProductList_CompareHookStr,
                                        _MUIA_List_Format       , "MIW=1 MAW=60 BAR,"
                                                                                          "MIW=1 MAW=-1 P=\033r BAR,"
                                                                                          "MIW=1 MAW=-1 P=\033r BAR,"
                                                                                          "MIW=1 MAW=-1 P=\033r BAR,"
                                                                                          "MIW=1 MAW=-1 P=\033r BAR,"
                                                                                          "MIW=1 MAW=-1 P=\033r",
                                        _MUIA_List_Title        , TRUE,

                                         MUIA_NList_TitleSeparator, TRUE,
                                         MUIA_NList_AutoCopyToClip, TRUE,

                                TAG_MORE, msg->ops_AttrList);

          if(obj)
                  {
                  struct FakturaProductList_Data *data = INST_DATA(cl,obj);

                  msg->MethodID = OM_SET;
                  DoMethodA(obj, (Msg)msg);
                  msg->MethodID = OM_NEW;

                  }


          return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG FakturaProductList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

        switch (msg->MethodID)
           {
           case OM_NEW                   : return(FakturaProductList_New       (cl, obj, (APTR)msg));
           }

        return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
/// PRODUCT LIST CLASS

struct MUI_CustomClass *CL_ProductList = NULL;

/// Data
struct ProductList_Data
{
   ULONG index;
   ULONG sww;
};
//|

/// Product list hooks
/// HOOK: ProductList_Display
void * __saveds __asm ProductList_Display(register __a2 char **array, register __a1 struct ProductList *node)
{
static char Del[20],
                        Index[ PROD_INDEX_LEN + 10 ],
                        Name[ PROD_NAME_LEN + 10 ],
                        SWW[ PROD_SWW_LEN + 10 ],
                        Netto[ 15 + 5 ],
                        Vat[ 10 + 5 ],
                        Brutto[15 + 5],
                        Jedn[ PROD_JEDN_LEN + 5 ],
                        Ilo��[ 15 + 5 ],
                        Ilo��_u�amki[10 + 5];


   if(node)
         {
         struct Product *Product = &node->pl_prod;
         char _Empty[15];
         char _LimitExceeded[15] = "";


         // us�ugi nie maj� stanu magazynowego, wi�c jak jest 0,
         // to nas to i tak wali


         if(Product->Typ != TYP_USLUGA)
           {
           strcpy( _Empty, EMPTY[(node->pl_prod.Ilo�� > 0)] );
           
                //i REDzimy jesli limit ponizej podanego
                 if( node->pl_prod.MinEn )
                        if( node->pl_prod.Ilo�� < node->pl_prod.StanMin )
                                {
                                strcat( _LimitExceeded, "\033b<\033n " );
                                }
           }
         else
           {
           // boldzimy us�ugi
           sprintf( _Empty, "\033b%s", EMPTY[1] );
           }
        


         if( node->Deleted == FALSE )
                {
                sprintf(Del,"%s%s%ld", _LimitExceeded, _Empty, array[-1]+1);
                }
         else
                sprintf(Del     , "%s*", _Empty);


         sprintf(Index   , "%s%s", _Empty, Product->Index);
         sprintf(Name    , "%s%s", _Empty, Product->Nazwa);
         sprintf(SWW     , "%s%s", _Empty, Product->SWW);
         sprintf(Netto   , "%s%s", _Empty, Price2String(Product->Mar�a + Product->Zakup));
         sprintf(Vat     , "%s%s", _Empty, VatTable[Product->Vat]);
         sprintf(Brutto  , "%s%s", _Empty, Price2String( CalcBrutto(Product->Zakup, Product->Mar�a, Product->Vat) ) );
         sprintf(Jedn    , "%s%s", _Empty, Product->Jedn.Nazwa);

         if( Product->Typ != TYP_USLUGA )
                {
                long ca�k;
                char *u�amek;

                ca�k   = (long)ceil(Product->Ilo��);
                u�amek = strchr( Price2String( Product->Ilo�� ), '.' );

                sprintf(Ilo��       , "%s%ld", _Empty, ca�k);

                if( Product->Jedn.Ca�kowita == FALSE )
                   {
                   sprintf(Ilo��_u�amki, "%s%s", _Empty, u�amek);
                   }
                else
                   {
                   Ilo��_u�amki[0] = 0;
                   }
                }
         else
                {
                strcpy(Ilo��   , "\033b---");
                Ilo��_u�amki[0] = 0;
                }

         *array++ = Del;
         *array++ = Index;
         *array++ = Name;
         *array++ = SWW;
         *array++ = Netto;
         *array++ = Vat;
         *array++ = Brutto;
         *array++ = Jedn;
         *array++ = Ilo��;
         *array++ = Ilo��_u�amki;
         }
   else
         {
         *array++ = MSG_PROD_LIST_DEL;
         *array++ = MSG_PROD_LIST_INDEX;
         *array++ = MSG_PROD_LIST_NAME;
         *array++ = MSG_PROD_LIST_SWW;
         *array++ = MSG_PROD_LIST_NETTO;
         *array++ = MSG_PROD_LIST_VAT;
         *array++ = MSG_PROD_LIST_BRUTTO;
         *array++ = MSG_PROD_LIST_JEDN;
         *array++ = MSG_PROD_LIST_AMOUNT;
         *array++ = "";
         }


         return(0);
}
//|
/// HOOK: ProductList_CompareStr
LONG __saveds __asm ProductList_CompareStr(register __a1 struct ProductList *Node1, register __a2 struct ProductList *Node2)
{
        return((LONG)StrnCmp(MyLocale, Node1->pl_prod.Nazwa, Node2->pl_prod.Nazwa, -1, SC_COLLATE2));
}
//|

/*
/// HOOK: ProductList_Construct
APTR __saveds __asm ProductList_Construct(register __a2 APTR pool, register __a1 struct ProductList *node)
{
struct  ProductList *new;

                if(new = calloc(1, sizeof(struct ProductList)))
                                 memcpy(new, node, sizeof(struct ProductList));

                return(new);
}
//|
/// HOOK: ProductList_Destruct
VOID __saveds __asm ProductList_Destruct(register __a2 APTR pool, register __a1 struct ProductList *node)
{
                free(node);
}
//|
*/

//|

/// OM_NEW

ULONG ProductList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
//static const struct Hook ProductList_ConstructHook   = { {NULL, NULL}, (VOID *)ProductList_Construct ,NULL,NULL };
//static const struct Hook ProductList_DestructHook    = { {NULL, NULL}, (VOID *)ProductList_Destruct  ,NULL,NULL };
static const struct Hook ProductList_DisplayHook     = { {NULL, NULL}, (VOID *)ProductList_Display   ,NULL,NULL };
static const struct Hook ProductList_CompareHookStr  = { {NULL, NULL}, (VOID *)ProductList_CompareStr,NULL,NULL };

        obj = (Object *)DoSuperNew(cl,obj,
                                        InputListFrame,
                                        _MUIA_List_AutoVisible  , TRUE,
//                    _MUIA_List_ConstructHook, &ProductList_ConstructHook,
//                    _MUIA_List_DestructHook , &ProductList_DestructHook,
                                        _MUIA_List_DisplayHook  , &ProductList_DisplayHook,
                                        _MUIA_List_CompareHook  , &ProductList_CompareHookStr,
                                        _MUIA_List_Format       ,
                                                                                          "MIW=1 MAW=-1 COL=0 P=\033r BAR,"  /* 0 del/LP */
                                                                                          "MIW=1 MAW=10 COL=1 BAR,"          /* 1 indeks */
                                                                                          "MIW=1 MAW=49 COL=2 BAR,"          /* 2 nazwa  */
                                                                                          "MIW=1 MAW=10 COL=3 BAR,"          /* 3 sww    */
                                                                                          "MIW=1 MAW=-1 COL=4 P=\033r BAR,"  /* 4 netto  */
                                                                                          "MIW=1 MAW=-1 COL=5 P=\033r BAR,"  /* 5 vat    */
                                                                                          "MIW=1 MAW=-1 COL=6 P=\033r BAR,"  /* 6 brutto */
                                                                                          "MIW=1 MAW=10 COL=7 P=\033r BAR,"  /* 7 jedn   */
                                                                                          "MIW=1 MAW=-1 COL=8 P=\033r,"      /* 8 amount (cz. ca�kowita) */
                                                                                          "MIW=1 MAW=-1 COL=9 P=\033l",      /* 9 amount (cz. u�amkowa)  */


                                        _MUIA_List_Title        , TRUE,
#ifdef USE_NLIST
                                         MUIA_NList_TitleSeparator, TRUE,
                                         MUIA_NList_AutoCopyToClip, TRUE,
#endif

                                TAG_MORE, msg->ops_AttrList);
          if(obj)
                  {
                  struct ProductList_Data *data = INST_DATA(cl,obj);

                  data->index = 0;
                  data->sww   = 0;


                  /*** trick to set arguments ***/

                  msg->MethodID = OM_SET;
                  DoMethodA(obj, (Msg)msg);
                  msg->MethodID = OM_NEW;

                  }


          return((ULONG)obj);
}

//|
/// OM_SET

ULONG ASM ProductList_Set(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ProductList_Data *data = INST_DATA(cl,obj);
struct TagItem *tags,*tag;

        for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
           {
           switch(tag->ti_Tag)
                  {
                  case MUIA_ProdList_IndexCount:
                           {
                           data->index = tag->ti_Data;
                           }
                           break;

                  case MUIA_ProdList_SWWCount:
                           {
                           data->sww = tag->ti_Data;
                           }
                           break;
                  }
                }

        return(DoSuperMethodA(cl, obj, msg));
}

//|
/// OM_GET
static ULONG ASM ProductList_Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ProductList_Data *data = INST_DATA(cl,obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

        switch(((struct opGet *)msg)->opg_AttrID)
           {
           case MUIA_ProdList_IndexCount:
                        *store = data->index;
                        return(TRUE);
                        break;

           case MUIA_ProdList_SWWCount:
                        *store = data->sww;
                        return(TRUE);
                        break;

           }

        return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher

__saveds __asm ULONG ProductList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ProductList_Data *data = INST_DATA(cl, obj);

        switch (msg->MethodID)
           {
           case OM_NEW                   : return(ProductList_New (cl, obj, (APTR)msg));
           case OM_SET                   : return(ProductList_Set (cl, obj, (APTR)msg));
           case OM_GET                   : return(ProductList_Get (cl, obj, (APTR)msg));


           case _MUIM_List_InsertSingle:
                   {
                   struct MUIP_List_InsertSingle *ptr;
                   struct ProductList *prod;

                   ptr = (struct MUIP_List_InsertSingle *)msg;
                   prod = ptr->entry;

                   if(prod->pl_prod.Index[0] != 0)
                           data->index++;

                   if(prod->pl_prod.SWW[0] != 0)
                           data->sww++;

                   }
                   break;

           case _MUIM_List_Clear:
                   {
                   data->index = 0;
                   data->sww   = 0;
                   }
                   break;

           }

        return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
/// USERS LIST CLASS

struct MUI_CustomClass *CL_UsersList = NULL;

/// Data
struct UsersList_Data
{
   ULONG Dummy;
};
//|

/// Users list hooks
/// HOOK: UsersList_Display
void * __saveds __asm UsersList_Display(register __a2 char **array, register __a1 struct UserList *node)
{
static char Del[16];

   if(node)
         {
         if( node->Deleted == FALSE )
           sprintf(Del, "%ld", array[-1] + 1);
         else
           strcpy(Del, "*");
//         sprintf(Del, "%s", ((node->Deleted==FALSE) ? " " : "*"));

         *array++ = Del;
         *array++ = node->user.Nazwa;
         }
   else
         {
         *array++ = MSG_USER_LIST_DEL;
         *array++ = MSG_USER_LIST_NAME;
         }


         return(0);
}
//|
/// HOOK: UsersList_CompareStr
LONG __saveds __asm UsersList_CompareStr(register __a1 struct UserList *Node1, register __a2 struct UserList *Node2)
{
        return((LONG)StrnCmp(MyLocale, Node1->user.Nazwa, Node2->user.Nazwa, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG UsersList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook UsersList_DisplayHook     = { {NULL, NULL}, (VOID *)UsersList_Display   ,NULL,NULL };
static const struct Hook UsersList_CompareHookStr  = { {NULL, NULL}, (VOID *)UsersList_CompareStr,NULL,NULL };

        obj = (Object *)DoSuperNew(cl,obj,
                                        InputListFrame,
                                        _MUIA_List_AutoVisible  , TRUE,
                                        _MUIA_List_DisplayHook  , &UsersList_DisplayHook,
                                        _MUIA_List_CompareHook  , &UsersList_CompareHookStr,
                                        _MUIA_List_Format       , "MIW=1 P=\033r BAR, MAW=-1",
                                        _MUIA_List_Title        , TRUE,

                                        MUIA_NList_TitleSeparator, TRUE,
                                        MUIA_NList_AutoCopyToClip, TRUE,


                                TAG_MORE, msg->ops_AttrList);


          if(obj)
                  {
                  struct UsersList_Data *data = INST_DATA(cl,obj);

//          DoMethod(obj, MUIM_UsersList_Init, NULL);

                  msg->MethodID = OM_SET;
                  DoMethodA(obj, (Msg)msg);
                  msg->MethodID = OM_NEW;

                  }

          return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG UsersList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

        switch (msg->MethodID)
           {
           case OM_NEW                   : return(UsersList_New       (cl, obj, (APTR)msg));

/*
           case MUIM_UsersList_Init  : return(UsersList_Init      (cl, obj, (APTR)msg));

           case MUIM_UsersList_Load   : return(UsersList_Load      (cl, obj, (APTR)msg));
           case MUIM_UsersList_Save   : return(UsersList_Save      (cl, obj, (APTR)msg));
*/
           }

        return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
/// DOCUMENT LIST CLASS

struct MUI_CustomClass *CL_DocumentList = NULL;

/*
** ta klasa u�ywa Construct/Destruct Hooks!!!
**
** Odbiorca, numer, data
**
*/

/// Data
struct DocumentList_Data
{
   ULONG SortOrder;
};
//|

/// Document list hooks

/// HOOK: DocumentList_Display
void * __saveds __asm DocumentList_Display( register __a2 char **array,
                                            register __a1 struct DocumentScan *node
                                          )
{
static char Data[2+2+4+2+1],
            Empty[] = "";

//static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };


   if(node)
         {
         *array++ = RodzajeDokument�wLVTable[node->RodzajDokumentu];
         *array++ = node->Nazwa;
         *array++ = node->NIP;
//     *array++ = node->Regon;
         *array++ = node->NumerDokumentu;
         
         strcpy( Data, wfmh_Date2Str( &node->DataWystawienia ) );
         *array++ = Data;

        if( node->TotalFound )
                *array++ = Price2String( node->TotalNetto + node->TotalVat );
        else
                *array++ = Empty;
         }
   else
         {

         *array++ = MSG_DOC_LIST_TYP;
         *array++ = MSG_DOC_LIST_ODB;
         *array++ = MSG_DOC_LIST_NIP;
//     *array++ = MSG_DOC_LIST_REGON;
         *array++ = MSG_DOC_LIST_NUMER;
         *array++ = MSG_DOC_LIST_DATA;
         *array++ = MSG_DOC_LIST_TOTAL;
         }


         return(0);
}
//|
/// HOOK: DocumentList_Construct
APTR __saveds __asm DocumentList_Construct(register __a2 APTR pool, register __a1 struct DocumentScan *node)
{
struct  DocumentScan *new;

                if(new = malloc(sizeof(struct DocumentScan)))
                   {
                   memcpy(new, node, sizeof(struct DocumentScan));
                   }

                return(new);
}
//|
/// HOOK: DocumentList_Destruct
VOID __saveds __asm DocumentList_Destruct(register __a2 APTR pool, register __a1 struct DocumentScan *node)
{
                free(node);
}
//|

/// HOOK: DocumentList_CompareNazwa
LONG __saveds __asm DocumentList_CompareNazwa(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

        return((LONG)StrnCmp(MyLocale, Node1->Nazwa, Node2->Nazwa, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareNIP
LONG __saveds __asm DocumentList_CompareNIP(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

        return((LONG)StrnCmp(MyLocale, Node1->NIP, Node2->NIP, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareRegon
LONG __saveds __asm DocumentList_CompareRegon(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

        return((LONG)StrnCmp(MyLocale, Node1->Regon, Node2->Regon, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareNumer
LONG __saveds __asm DocumentList_CompareNumer(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

        return((LONG)StrnCmp(MyLocale, Node1->NumerDokumentu, Node2->NumerDokumentu, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareData
LONG __saveds __asm DocumentList_CompareData(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

        return((LONG)CompareDates(&Node2->DataWystawienia, &Node1->DataWystawienia));

}
//|
/// HOOK: DocumentList_CompareNazwaR
LONG __saveds __asm DocumentList_CompareNazwaR(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

        return((LONG)StrnCmp(MyLocale, Node2->Nazwa, Node1->Nazwa, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareNIPR
LONG __saveds __asm DocumentList_CompareNIPR(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

        return((LONG)StrnCmp(MyLocale, Node2->NIP, Node1->NIP, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareRegonR
LONG __saveds __asm DocumentList_CompareRegonR(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

        return((LONG)StrnCmp(MyLocale, Node2->Regon, Node1->Regon, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareNumerR
LONG __saveds __asm DocumentList_CompareNumerR(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

        return((LONG)StrnCmp(MyLocale, Node2->NumerDokumentu, Node1->NumerDokumentu, -1, SC_COLLATE2));

}
//|
/// HOOK: DocumentList_CompareDataR
LONG __saveds __asm DocumentList_CompareDataR(register __a1 struct DocumentScan *Node1, register __a2 struct DocumentScan *Node2)
{

        return((LONG)CompareDates(&Node1->DataWystawienia, &Node2->DataWystawienia));

}
//|

//|

static const struct Hook DocumentList_CompareHookNazwa  = { {NULL, NULL}, (VOID *)DocumentList_CompareNazwa  ,NULL,NULL };
static const struct Hook DocumentList_CompareHookNIP    = { {NULL, NULL}, (VOID *)DocumentList_CompareNIP    ,NULL,NULL };
static const struct Hook DocumentList_CompareHookRegon  = { {NULL, NULL}, (VOID *)DocumentList_CompareRegon  ,NULL,NULL };
static const struct Hook DocumentList_CompareHookNumer  = { {NULL, NULL}, (VOID *)DocumentList_CompareNumer  ,NULL,NULL };
static const struct Hook DocumentList_CompareHookData   = { {NULL, NULL}, (VOID *)DocumentList_CompareData   ,NULL,NULL };

static const struct Hook DocumentList_CompareHookNazwaR = { {NULL, NULL}, (VOID *)DocumentList_CompareNazwaR ,NULL,NULL };
static const struct Hook DocumentList_CompareHookNIPR   = { {NULL, NULL}, (void *)DocumentList_CompareNIPR   ,NULL,NULL };
static const struct Hook DocumentList_CompareHookRegonR = { {NULL, NULL}, (VOID *)DocumentList_CompareRegonR ,NULL,NULL };
static const struct Hook DocumentList_CompareHookNumerR = { {NULL, NULL}, (VOID *)DocumentList_CompareNumerR ,NULL,NULL };
static const struct Hook DocumentList_CompareHookDataR  = { {NULL, NULL}, (VOID *)DocumentList_CompareDataR  ,NULL,NULL };

/// OM_NEW

static const struct Hook DocumentList_DisplayHook     = { {NULL, NULL}, (VOID *)DocumentList_Display    ,NULL,NULL };
static const struct Hook DocumentList_ConstructHook   = { {NULL, NULL}, (VOID *)DocumentList_Construct  ,NULL,NULL };
static const struct Hook DocumentList_DestructHook    = { {NULL, NULL}, (VOID *)DocumentList_Destruct   ,NULL,NULL };

ULONG DocumentList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{

        obj = (Object *)DoSuperNew(cl,obj,
                                        InputListFrame,
                                        _MUIA_List_AutoVisible  , TRUE,
                                        _MUIA_List_DisplayHook  , &DocumentList_DisplayHook,
//                    _MUIA_List_CompareHook  , &DocumentList_CompareHookNumer,
                                        _MUIA_List_ConstructHook, &DocumentList_ConstructHook,
                                        _MUIA_List_DestructHook , &DocumentList_DestructHook,

                                        _MUIA_List_Format       , "MIW=1 MAW=-1 COL=0 BAR,"   /* Typ */
                                                                  "MIW=1 MAW=40 COL=1 BAR,"   /* Odbiorca */
                                                                  "MIW=1 MAW=-1 COL=2 BAR,"   /* Nip */
//                                                                "MIW=1 MAW=-1 COL= BAR,"    /* Regon */
                                                                  "MIW=1 MAW=-1 COL=3 BAR,"   /* Numer */
                                                                  "MIW=1 MAW=-1 COL=4 BAR,"   /* Data */
                                                                  "MIW=1 MAW=-1 COL=5 P=\033r", /* Total */
                                        _MUIA_List_Title        , TRUE,


                                         MUIA_NList_TitleSeparator, TRUE,
                                         MUIA_NList_AutoCopyToClip, TRUE,


                                TAG_MORE, msg->ops_AttrList);


          if(obj)
                  {
                  /*** trick to set arguments ***/


                  set(obj, MUIA_DocumentList_Order, 4);

                  msg->MethodID = OM_SET;
                  DoMethodA(obj, (Msg)msg);
                  msg->MethodID = OM_NEW;
                  }


          return((ULONG)obj);
}

//|
/// OM_SET

ULONG ASM DocumentList_Set(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct DocumentList_Data *data = INST_DATA(cl, obj);
struct TagItem *tags,*tag;

        for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
           {
           switch(tag->ti_Tag)
                  {
                  case MUIA_DocumentList_Order:
                           {
                           data->SortOrder = tag->ti_Data;

                           if((tag->ti_Data & 0x80000000) == 0)
                                   {
                                   switch(tag->ti_Data)
                                          {
                                          case 0: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNazwa); break;
                                          case 1: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNIP); break;
                                          case 2: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookRegon); break;
                                          case 3: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookData); break;
                                          case 4: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNumer); break;
                                          }
                                   }
                                else
                                   {
                                   switch(tag->ti_Data & 0x7fffffff)
                                          {
                                          case 0: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNazwaR); break;
                                          case 1: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNIPR); break;
                                          case 2: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookRegonR); break;
                                          case 4: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookDataR); break;
                                          case 3: set(obj, _MUIA_List_CompareHook, &DocumentList_CompareHookNumerR); break;
                                          }
                                   }

                           }
                           break;
                  }
                }

        return(DoSuperMethodA(cl, obj, msg));
}

//|
/// OM_GET
static ULONG ASM DocumentList_Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct DocumentList_Data *data = INST_DATA(cl, obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

                switch(((struct opGet *)msg)->opg_AttrID)
                                {
                                case MUIA_DocumentList_Order:
                                         *store = data->SortOrder;
                                         return(TRUE);
                                         break;
                                }

                return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher

__saveds __asm ULONG DocumentList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

        switch (msg->MethodID)
           {
           case OM_NEW                   : return(DocumentList_New       (cl, obj, (APTR)msg));
           case OM_SET                   : return(DocumentList_Set       (cl, obj, (APTR)msg));
           case OM_GET                   : return(DocumentList_Get       (cl, obj, (APTR)msg));
           }

        return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
/// SMART CYCLE CLASS

struct MUI_CustomClass *CL_SmartCycle = NULL;

/*
** Smart CycleGadget - supports SET!
*/

/// Data
struct SmartCycle_Data
{
   APTR   Entries[30];
   Object *Cycle;
   long   Key;
};
//|

/// OM_NEW

ULONG SmartCycle_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
Object *Cycle;

        obj = (Object *)DoSuperNew(cl,obj,

                                MUIA_Group_Horiz, TRUE,
                                MUIA_CycleChain, FALSE,

                                Child, Cycle = HVSpace,

                                TAG_MORE, msg->ops_AttrList);

          if(obj)
                {
                struct SmartCycle_Data *data = INST_DATA(cl, obj);

                data->Cycle = Cycle;
                data->Entries[0] = 0L;
                data->Key = 0;


                /*** trick to set arguments ***/

                msg->MethodID = OM_SET;
                DoMethodA(obj, (Msg)msg);
                msg->MethodID = OM_NEW;
                }


          return((ULONG)obj);
}

//|
/// OM_SET

ULONG ASM SmartCycle_Set(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct SmartCycle_Data *data = INST_DATA(cl, obj);
struct TagItem *tags,*tag;

        for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
           {
           switch(tag->ti_Tag)
                  {
                  case MUIA_ControlChar:
                           set(data->Cycle, MUIA_ControlChar, tag->ti_Data);
                           data->Key = tag->ti_Data;
                           break;

                  case MUIA_Cycle_Entries:
                           {
                           char ** str = (char **)tag->ti_Data;
                           int  i=0;

                           for(;; i++)
                                  {
                                  data->Entries[i] = str[i];
                                  if(data->Entries[i] == '\0')
                                          break;
                                  }

                           DoMethod(obj, OM_REMMEMBER, data->Cycle);
                           DisposeObject(data->Cycle);
                           data->Cycle = CycleObject, MUIA_Cycle_Entries, data->Entries,
                                                                                  MUIA_CycleChain, TRUE,
                                                                                  End;
                           DoMethod(obj, OM_ADDMEMBER, data->Cycle);
//               DoMethod(data->Cycle, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_TYP_P�ATNO�CI);

                           if(data->Key != 0)
                                   set(data->Cycle, MUIA_ControlChar, data->Key);

                           break;
                           }
                  }
                }

        return(DoSuperMethodA(cl, obj, msg));
}

//|
/// OM_GET
static ULONG ASM SmartCycle_Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct SmartCycle_Data *data = INST_DATA(cl, obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

                switch(((struct opGet *)msg)->opg_AttrID)
                                {
                                case MUIA_Cycle_Active:
                                         *store = (ULONG)xget(data->Cycle, MUIA_Cycle_Active);
                                         return(TRUE);
                                         break;
                                }

                return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher

__saveds __asm ULONG SmartCycle_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

        switch (msg->MethodID)
           {
           case OM_NEW        : return(SmartCycle_New       (cl, obj, (APTR)msg));
           case OM_SET        : return(SmartCycle_Set       (cl, obj, (APTR)msg));
           case OM_GET        : return(SmartCycle_Get       (cl, obj, (APTR)msg));

/*
           case MUIM_Notify:
                                {
                                struct SmartCycle_Data *data = INST_DATA(cl,obj);

                                                return(DoMethodA(data->Cycle, msg));
                                }
*/

           }

        return(DoSuperMethodA(cl,obj,msg));
}

//|

//|

/*
/// ZAKUPY LIST CLASS

struct MUI_CustomClass *CL_ZakupyList = NULL;

/*
struct Zakupy
{
        long   lp;                                         // 1
        struct DateStamp DataZdarzenia;                    // 2
        char   NumerDowoduKsi�gowego[STR_ZAK_DOW�D_LEN];   // 3
        struct Klient Kontrahent;                          // 4
        char   OpisZdarzenia[STR_ZAK_OPIS_LEN];            // 5
// Przych�d
        double Warto��SprzedanychTowar�w;                  // 6
        double Pozosta�ePrzychody;                         // 7

        double ZakupTowar�wHandlowych;                     // 10
        double KosztyUboczneZakupu;                        // 11

// Wydatki
        double KosztyReprezentacjiIReklamy;                // 12
        double Wynagrodzenia;                              // 13
        double Pozosta�eWydatki;                           // 14

        double Pole16;                                     // 16
        char   Uwagi[STR_ZAK_UWAGI_LEN];                   // 17
*/

/// Data
struct ZakupyList_Data
{
   ULONG Dummy;
};
//|

/// Zakupy list hooks
/// HOOK: ZakupyList_Display
void * __saveds __asm ZakupyList_Display(register __a2 char **array, register __a1 struct ZakupList *node)
{
//static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };
static char Del[6],
                        LP[5],
                        Data[2+1+2+1],  // dd.mm\0
                        In[15],
                        Out[15];

/*,
                        Name[CUST_NAME_LEN + 10],
                        Ulica[CUST_ADRES_LEN + 10],
                        Kod[CUST_KOD_LEN + 10],
                        Miasto[CUST_MIASTO_LEN + 10];
*/


   if(node)
         {
         struct Zakup *zak = &node->za_zakup;

         sprintf(Del   , "%s" , ((node->Deleted==FALSE) ? " " : "*"));
         *array++ = Del;

         sprintf(LP    , "%ld", zak->LP);
         *array++ = LP;

//     FormatDateHook.h_Data = Data;
//     FormatDate(MyLocale, "%d.%m", &zak->DataZdarzenia, &FormatDateHook);
         wfmh_Date2StrFmtBuffer( Data, "%d.%m", &zak->DataZdarzenia );
         *array++ = Data;

         *array++ = zak->Kontrahent.Nazwa1;
         *array++ = zak->OpisZdarzenia;

         {
         double suma = zak->Warto��SprzedanychTowar�w + zak->Pozosta�ePrzychody;

         if(suma == 0)
                 In[0] = 0;
         else
                 strcpy(In, Price2String(suma));
         }
         *array++ = In;

         {
         double suma = zak->KosztyReprezentacjiIReklamy + zak->Wynagrodzenia + zak->Pozosta�eWydatki;

         if(suma == 0)
           Out[0] = 0;
         else
           strcpy(Out, Price2String(suma));
         }
         *array++ = Out;

         }
   else
         {
         *array++ = MSG_ZAK_LIST_DEL;
         *array++ = MSG_ZAK_LIST_LP;
         *array++ = MSG_ZAK_LIST_DATA;
         *array++ = MSG_ZAK_LIST_NAZWA;
         *array++ = MSG_ZAK_LIST_OPIS;
         *array++ = MSG_ZAK_LIST_IN;
         *array++ = MSG_ZAK_LIST_OUT;
         }


         return(0);
}
//|
/// HOOK: ZakupyList_CompareStr
LONG __saveds __asm ZakupyList_CompareStr(register __a1 struct KlientList *Node1, register __a2 struct KlientList *Node2)
{
        return((LONG)StrnCmp(MyLocale, Node1->kl_klient.Nazwa1, Node2->kl_klient.Nazwa1, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG ZakupyList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook ZakupyList_DisplayHook     = { {NULL, NULL}, (VOID *)ZakupyList_Display   ,NULL,NULL };
static const struct Hook ZakupyList_CompareHookStr  = { {NULL, NULL}, (VOID *)ZakupyList_CompareStr,NULL,NULL };

        obj = (Object *)DoSuperNew(cl,obj,
                                        InputListFrame,
                                        _MUIA_List_AutoVisible  , TRUE,
                                        _MUIA_List_DisplayHook  , &ZakupyList_DisplayHook,
                                        _MUIA_List_CompareHook  , &ZakupyList_CompareHookStr,
                                        _MUIA_List_Format       , "MIW=1 MAW=-1 BAR,"
                                                                                          "MIW=1 MAW=-1 BAR,"
                                                                                          "MIW=1 MAW=-1 BAR,"
                                                                                          "MIW=1 MAW=-1 BAR,"
                                                                                          "MIW=1 MAW=-1 BAR,"
                                                                                          "MIW=1 MAW=-1 BAR,"
                                                                                          "MIW=1 MAW=-1 ",

                                        _MUIA_List_Title        , TRUE,

#ifdef USE_NLIST
                                         MUIA_NList_TitleSeparator, TRUE,
                                         MUIA_NList_AutoCopyToClip, TRUE,
#endif

                                TAG_MORE, msg->ops_AttrList);

/*
          if(obj)
                  {
                  struct ZakupyList_Data *data = INST_DATA(cl,obj);

                  }
*/

          return((ULONG)obj);
}

//|

/// Dispatcher

__saveds __asm ULONG ZakupyList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

        switch (msg->MethodID)
           {
           case OM_NEW                   : return(ZakupyList_New       (cl, obj, (APTR)msg));
           }

        return(DoSuperMethodA(cl,obj,msg));
}

//|

//|
*/
/// ZAKUPY LIST CLASS

struct MUI_CustomClass *CL_ZakupyList = NULL;

/// Data
struct ZakupyList_Data
{
   ULONG SortOrder;
};
//|

/// Zakupy list hooks
/// HOOK: ZakupyList_Display
void * __saveds __asm ZakupyList_Display(register __a2 char **array, register __a1 struct ZakupList *node)
{
//static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };
static char Del[6],
                        LP[10],
                        Data[2+1+2+1],  // dd.mm\0
                        brutto[15],
                        vat[15];

/*,
                        Name[CUST_NAME_LEN + 10],
                        Ulica[CUST_ADRES_LEN + 10],
                        Kod[CUST_KOD_LEN + 10],
                        Miasto[CUST_MIASTO_LEN + 10];
*/


   if(node)
         {
         struct Zakup *zak = &node->za_zakup;

         sprintf(Del   , "%s" , ((node->Deleted==FALSE) ? " " : "*"));
         *array++ = Del;

         sprintf(LP    , "%ld", zak->P1);      // lp
         *array++ = LP;

         *array++ = zak->P2;                   // nr faktury

//     FormatDateHook.h_Data = Data;
//     FormatDate(MyLocale, "%d.%m", &zak->P3, &FormatDateHook);
         wfmh_Date2StrFmtBuffer( Data, "%d.%m", &zak->P3 );
         *array++ = Data;                      // data otrzymania

         *array++ = zak->Sprzedawca.Nazwa1;

         {
         double _brutto, _vat;

         _vat  = CalcVat(zak->P10 + zak->P14, VAT_22);      // vat 22%
         _vat += CalcVat(zak->P12 + zak->P16, VAT_7);       // vat 7%
         _brutto = zak->P9 + zak->P10 + zak->P12 + zak->P14 + zak->P16 + _vat;

         strcpy(brutto, Price2String(_brutto));
         *array++ = brutto;
         strcpy(vat, Price2String(_vat));
         *array++ = vat;
         }


         }
   else
         {
         *array++ = MSG_ZAK_LIST_DEL;
         *array++ = MSG_ZAK_LIST_LP;
         *array++ = MSG_ZAK_LIST_NUMER;
         *array++ = MSG_ZAK_LIST_DATA;
         *array++ = MSG_ZAK_LIST_NAZWA;
         *array++ = MSG_ZAK_LIST_BRUTTO;
         *array++ = MSG_ZAK_LIST_VAT;
         }


         return(0);
}
//|

/// HOOK: ZakupyList_CompareLP
LONG __saveds __asm ZakupyList_CompareLP(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
        if(Node2->za_zakup.P1 == Node1->za_zakup.P1)   return((LONG)0);
        if(Node2->za_zakup.P1 <  Node1->za_zakup.P1)   return((LONG)1);
        if(Node2->za_zakup.P1 >  Node1->za_zakup.P1)   return((LONG)-1);
}
//|
/// HOOK: ZakupyList_CompareNumer
LONG __saveds __asm ZakupyList_CompareNumer(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
        return((LONG)stricmp(Node1->za_zakup.P2, Node2->za_zakup.P2));

}
//|
/// HOOK: ZakupyList_CompareData
LONG __saveds __asm ZakupyList_CompareData(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
        return((LONG)CompareDates(&Node2->za_zakup.P3, &Node1->za_zakup.P3));

}
//|

/// HOOK: ZakupyList_CompareLPR
LONG __saveds __asm ZakupyList_CompareLPR(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
        if(Node2->za_zakup.P1 == Node1->za_zakup.P1)   return((LONG)0);
        if(Node2->za_zakup.P1 >  Node1->za_zakup.P1)   return((LONG)1);
        if(Node2->za_zakup.P1 <  Node1->za_zakup.P1)   return((LONG)-1);
}
//|
/// HOOK: ZakupyList_CompareNumerR
LONG __saveds __asm ZakupyList_CompareNumerR(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
        return((LONG)stricmp(Node2->za_zakup.P2, Node1->za_zakup.P2));

}
//|
/// HOOK: ZakupyList_CompareDataR
LONG __saveds __asm ZakupyList_CompareDataR(register __a1 struct ZakupList *Node1, register __a2 struct ZakupList *Node2)
{
        return((LONG)CompareDates(&Node1->za_zakup.P3, &Node2->za_zakup.P3));

}
//|
//|

static const struct Hook ZakupyList_DisplayHook      = { {NULL, NULL}, (VOID *)ZakupyList_Display      , NULL,NULL };
static const struct Hook ZakupyList_CompareHookLP    = { {NULL, NULL}, (VOID *)ZakupyList_CompareLP    , NULL,NULL };
static const struct Hook ZakupyList_CompareHookNumer = { {NULL, NULL}, (VOID *)ZakupyList_CompareNumer , NULL,NULL };
static const struct Hook ZakupyList_CompareHookData  = { {NULL, NULL}, (VOID *)ZakupyList_CompareData  , NULL,NULL };
static const struct Hook ZakupyList_CompareHookLPR   = { {NULL, NULL}, (VOID *)ZakupyList_CompareLPR   , NULL,NULL };
static const struct Hook ZakupyList_CompareHookNumerR= { {NULL, NULL}, (VOID *)ZakupyList_CompareNumerR, NULL,NULL };
static const struct Hook ZakupyList_CompareHookDataR = { {NULL, NULL}, (VOID *)ZakupyList_CompareDataR , NULL,NULL };

/// OM_NEW

ULONG ZakupyList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{

        obj = (Object *)DoSuperNew(cl,obj,
                                        InputListFrame,
                                        _MUIA_List_AutoVisible  , TRUE,
                                        _MUIA_List_DisplayHook  , &ZakupyList_DisplayHook,
                                        _MUIA_List_CompareHook  , &ZakupyList_CompareHookLP,
                                        _MUIA_List_Format       , "MIW=1 MAW=-1 BAR,"
                                                                                          "MIW=1 MAW=-1 BAR P=\033r,"
                                                                                          "MIW=1 MAW=-1 BAR,"
                                                                                          "MIW=1 MAW=-1 BAR P=\033c,"
                                                                                          "MIW=1 MAW=-1 BAR,"
                                                                                          "MIW=1 MAW=-1 BAR P=\033r,"
                                                                                          "MIW=1 MAW=-1 P=\033r",

                                        _MUIA_List_Title        , TRUE,

                                         MUIA_NList_TitleSeparator, TRUE,
                                         MUIA_NList_AutoCopyToClip, TRUE,

                                TAG_MORE, msg->ops_AttrList);


          if(obj)
                  {
//          struct ZakupyList_Data *data = INST_DATA(cl,obj);


                  /*** trick to set arguments ***/

                  msg->MethodID = OM_SET;
                  DoMethodA(obj, (Msg)msg);
                  msg->MethodID = OM_NEW;

                  }


          return((ULONG)obj);
}

//|
/// OM_SET

ULONG ASM ZakupyList_Set(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ZakupyList_Data *data = INST_DATA(cl, obj);
struct TagItem *tags,*tag;

        for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
           {
           switch(tag->ti_Tag)
                  {
                  case MUIA_ZakupyList_Order:
                           {
                           data->SortOrder = tag->ti_Data;

                           if((tag->ti_Data & 0x80000000) == 0)
                                   {
                                   switch(tag->ti_Data)
                                          {
                                          case 0: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookLP); break;
                                          case 1: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookNumer); break;
                                          case 2: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookData); break;
                                          }
                                   }
                                else
                                   {
                                   switch(tag->ti_Data & 0x7fffffff)
                                          {
                                          case 0: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookLPR); break;
                                          case 1: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookNumerR); break;
                                          case 2: set(obj, _MUIA_List_CompareHook, &ZakupyList_CompareHookDataR); break;
                                          }
                                   }

                           }
                           break;
                  }
                }

        return(DoSuperMethodA(cl, obj, msg));
}

//|
/// OM_GET
static ULONG ASM ZakupyList_Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ZakupyList_Data *data = INST_DATA(cl, obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

                switch(((struct opGet *)msg)->opg_AttrID)
                                {
                                case MUIA_ZakupyList_Order:
                                         *store = data->SortOrder;
                                         return(TRUE);
                                         break;
                                }

                return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher

__saveds __asm ULONG ZakupyList_Dispatcher(REG(a0) struct IClass *cl,REG(a2) Object *obj,REG(a1) Msg msg)
{

        switch (msg->MethodID)
           {
           case OM_NEW                   : return(ZakupyList_New       (cl, obj, (APTR)msg));
           case OM_SET                   : return(ZakupyList_Set       (cl, obj, (APTR)msg));
           case OM_GET                   : return(ZakupyList_Get       (cl, obj, (APTR)msg));
           }

        return(DoSuperMethodA(cl,obj,msg));
}

//|

//|



/// COUNT STOP CHUNKS
#define COUNT_NUM_STOPS (sizeof(Count_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Count_Stops[] =
{
                ID_COUN, ID_CAT,
                ID_COUN, ID_VERS,
                ID_COUN, ID_COUN,
                NULL, NULL,
};
//|

/// WczytajCount

long WczytajCount(char *FileName)
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
int    Errors = 0;
char   ValidFile = FALSE;
int    i;
long   Count = 0;

         set(app, MUIA_Application_Sleep, TRUE);

         if(iff = AllocIFF())
           {
           if(iff->iff_Stream = Open(FileName, MODE_OLDFILE))
                   {
                   InitIFFasDOS(iff);

                   StopChunks(iff, Count_Stops, COUNT_NUM_STOPS);
//           StopOnExit(iff, ID_COUN, ID_FORM);

                   if(!OpenIFF(iff, IFFF_READ))
                           {

                           while(TRUE)
                                  {
                                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                                         break;

                                  if(cn = CurrentChunk(iff))
                                         {
                                         LONG ID = cn->cn_ID;

                                         if(!ValidFile)
                                                {
                                                if((ID == ID_CAT) && (cn->cn_Type == ID_COUN))
                                                   {
//                           Usu�Magazyn();
                                                   ValidFile = TRUE;
                                                   continue;
                                                   }


                                                break;
                                                }

///                    ID_VERS
                                         if(ID == ID_VERS)
                                                {
                                                struct BaseVersion version;

                                                if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                                                   {

                                                   }
                                                else
                                                   {
                                                   _RC = IoErr();
                                                   break;
                                                   }

                                                continue;
                                                }
//|
///                    ID_COUN

                                         if(ID == ID_COUN)
                                                {
                                                if(ReadChunkBytes(iff, &Count, sizeof(long)) != cn->cn_Size)
                                                   _RC = IoErr();
                                                break;
                                                }
//|

                                         }
                                  }

                           CloseIFF(iff);
                           }

                        Close(iff->iff_Stream);
                        }


//       if(_RC == IFFERR_EOF) _RC = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

           FreeIFF(iff);
           }

           set(app, MUIA_Application_Sleep, FALSE);

           return(Count);
}

//|
/// ZapiszCount

char ZapiszCount(char *FileName, long Count)
{
struct IFFHandle *MyIFFHandle;
int i;

        set(app, MUIA_Application_Sleep, TRUE);

        if(MyIFFHandle = AllocIFF())
                {
                BPTR  FileHandle;

                if(FileHandle = Open(FileName, MODE_NEWFILE))
                   {
                   MyIFFHandle->iff_Stream = FileHandle;
                   InitIFFasDOS(MyIFFHandle);

                   if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
                           {
                           struct BaseVersion version;

                           PushChunk(MyIFFHandle, ID_COUN, ID_CAT, IFFSIZE_UNKNOWN);

                           PushChunk(MyIFFHandle, ID_COUN, ID_FORM, IFFSIZE_UNKNOWN);
                                   PushChunk(MyIFFHandle, ID_COUN, ID_VERS, IFFSIZE_UNKNOWN);
                                   version.Version = VERSION;
                                   version.Revision = REVISION;
                                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                                   PopChunk(MyIFFHandle);

/*
                                   PushChunk(MyIFFHandle, ID_COUN, ID_ANNO, IFFSIZE_UNKNOWN);
                                   WriteChunkBytes(MyIFFHandle, ScreenTitle, sizeof(ScreenTitle)-1);
                                   WriteChunkBytes(MyIFFHandle, " <", 2);
                                   WriteChunkBytes(MyIFFHandle, EMAIL, strlen(EMAIL));
                                   WriteChunkBytes(MyIFFHandle, ">, ", 3);
                                   WriteChunkBytes(MyIFFHandle, WWW, strlen(WWW));
                                   PopChunk(MyIFFHandle);
*/
                           PopChunk(MyIFFHandle);

                           PushChunk(MyIFFHandle, ID_COUN, ID_FORM, IFFSIZE_UNKNOWN);

                           PushChunk(MyIFFHandle, ID_COUN, ID_COUN, IFFSIZE_UNKNOWN);
                           WriteChunkBytes(MyIFFHandle, &Count, sizeof(Count));
                           PopChunk(MyIFFHandle);

                           PopChunk(MyIFFHandle);

                           PopChunk(MyIFFHandle);
                           CloseIFF(MyIFFHandle);
                           }
                   else
                           {
                           DisplayBeep(0);
                           D(bug("*** OpenIFF() nie powiod�o si�\n"));
                           }

                   Close(FileHandle);
                   }
                else
                   {
//           MUI_Request(app, (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
                   D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", FileName));
                   }

                FreeIFF(MyIFFHandle);
                }
         else
                {
                DisplayBeep(0);
                D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
                }

        set(app, MUIA_Application_Sleep, FALSE);

        return(0);
}



//|


/// NUMB STOP CHUNKS
#define NUMB_NUM_STOPS (sizeof(Numb_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Numb_Stops[] =
{
                ID_NUMB, ID_CAT,
                ID_NUMB, ID_VERS,

                ID_NUMB, ID_FVOR,
                ID_NUMB, ID_FVKO,
                ID_NUMB, ID_FVPO,
//        ID_NUMB, ID_RUOR,
//        ID_NUMB, ID_RUKO,
//        ID_NUMB, ID_RUPO,
                ID_NUMB, ID_PARA,
                ID_NUMB, ID_ORDE,
                ID_NUMB, ID_FVPR,
                ID_NUMB, ID_ODRE,
                ID_NUMB, ID_FVPR,

                NULL, NULL,
};
//|

/// WczytajNumeracj�Dokument�w

long WczytajNumeracj�Dokument�w(void)
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
long   Errors = 0;
char   ValidFile = FALSE;

         set(app, MUIA_Application_Sleep, TRUE);

         if(iff = AllocIFF())
           {
           if(iff->iff_Stream = Open(NumeracjaFileName, MODE_OLDFILE))
                   {
                   InitIFFasDOS(iff);

                   StopChunks(iff, Numb_Stops, NUMB_NUM_STOPS);

                   if(!OpenIFF(iff, IFFF_READ))
                           {

                           while(TRUE)
                                  {
                                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                                         break;

                                  if(cn = CurrentChunk(iff))
                                         {
                                         LONG ID = cn->cn_ID;

                                         if(!ValidFile)
                                                {
                                                if((ID == ID_CAT) && (cn->cn_Type == ID_NUMB))
                                                   {
                                                   ValidFile = TRUE;
                                                   continue;
                                                   }

                                                break;
                                                }

///                    ID_VERS
                                         if(ID == ID_VERS)
                                                {
                                                struct BaseVersion version;

                                                if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                                                   {

                                                   }
                                                else
                                                   {
                                                   _RC = IoErr();
                                                   break;
                                                   }

                                                continue;
                                                }
//|

                                         _read(ID_FVOR, &docs.numer_fvat)
                                         _read(ID_FVKO, &docs.numer_fvat_kor)
                                         _read(ID_FVPO, &docs.numer_fvat_par)
//                     _read(ID_RUOR, &docs.numer_rach)
//                     _read(ID_RUKO, &docs.numer_rach_kor)
//                     _read(ID_RUPO, &docs.numer_rach_par)
                                         _read(ID_PARA, &docs.numer_paragon)
                                         _read(ID_ORDE, &docs.numer_order)
                                         _read(ID_FVPR, &docs.numer_proforma)
                                         _read(ID_ODRE, &docs.numer_odreczna)
                                         _read(ID_FVPR, &docs.numer_proforma)
                                         }
                                  }

                           CloseIFF(iff);
                           }

                   Close(iff->iff_Stream);
                   }


//       if(_RC == IFFERR_EOF) _RC = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

           FreeIFF(iff);
           }

           set(app, MUIA_Application_Sleep, FALSE);

           return(0);
}

//|
/// ZapiszNumeracj�Dokument�w

char ZapiszNumeracj�Dokument�w(void)
{
#define _ID_TYPE ID_NUMB

struct IFFHandle *MyIFFHandle;

        set(app, MUIA_Application_Sleep, TRUE);

        if(MyIFFHandle = AllocIFF())
                {
                BPTR  FileHandle;

                if(FileHandle = Open(NumeracjaFileName, MODE_NEWFILE))
                   {
                   MyIFFHandle->iff_Stream = FileHandle;
                   InitIFFasDOS(MyIFFHandle);

                   if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
                           {
                           struct BaseVersion version;

                           PushChunk(MyIFFHandle, ID_NUMB, ID_CAT, IFFSIZE_UNKNOWN);

                           PushChunk(MyIFFHandle, ID_NUMB, ID_FORM, IFFSIZE_UNKNOWN);
                                   PushChunk(MyIFFHandle, ID_NUMB, ID_VERS, IFFSIZE_UNKNOWN);
                                   version.Version = VERSION;
                                   version.Revision = REVISION;
                                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                                   PopChunk(MyIFFHandle);

                           PopChunk(MyIFFHandle);

                           PushChunk(MyIFFHandle, ID_NUMB, ID_FORM, IFFSIZE_UNKNOWN);

                                 _write(ID_FVOR, docs.numer_fvat)
                                 _write(ID_FVKO, docs.numer_fvat_kor)
                                 _write(ID_FVPO, docs.numer_fvat_par)
//                 _write(ID_RUOR, docs.numer_rach)
//                 _write(ID_RUKO, docs.numer_rach_kor)
//                 _write(ID_RUPO, docs.numer_rach_par)
                                 _write(ID_PARA, docs.numer_paragon)
                                 _write(ID_ORDE, docs.numer_order)
                                 _write(ID_ODRE, docs.numer_odreczna)
                                 _write(ID_FVPR, docs.numer_proforma)

                           PopChunk(MyIFFHandle);

                           PopChunk(MyIFFHandle);
                           CloseIFF(MyIFFHandle);
                           }
                   else
                           {
                           DisplayBeep(0);
                           D(bug("*** OpenIFF() nie powiod�o si�\n"));
                           }

                   Close(FileHandle);
                   }
                else
                   {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
                   D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", NumeracjaFileName));
                   }

                FreeIFF(MyIFFHandle);
                }
         else
                {
                DisplayBeep(0);
                D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
                }



        // data ostatniej operacji -> numeracji nie zmienia sie bez powodu
        ZapiszLastOperationToday();


        set(app, MUIA_Application_Sleep, FALSE);

        return(0);

#undef _ID_TYPE
}



//|


/// SYS STOP CHUNKS
#define SYS_NUM_STOPS (sizeof(Sys_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Sys_Stops[] =
{
                ID_SYS, ID_CAT,
                ID_SYS, ID_VERS,

                ID_SYS, ID_STRT,
                ID_SYS, ID_OPER,

                NULL, NULL,
};
//|

/// WczytajSystemInfo

char WczytajSystemInfo(void)
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
long   Errors = 0;
char   ValidFile = FALSE;

#define _ID_TYPE ID_SYS

         set(app, MUIA_Application_Sleep, TRUE);

         if(iff = AllocIFF())
           {
           if(iff->iff_Stream = Open(SystemFileName, MODE_OLDFILE))
                   {
                   InitIFFasDOS(iff);

                   StopChunks(iff, Sys_Stops, SYS_NUM_STOPS);

                   if(!OpenIFF(iff, IFFF_READ))
                           {

                           while(TRUE)
                                  {
                                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                                         break;

                                  if(cn = CurrentChunk(iff))
                                         {
                                         LONG ID = cn->cn_ID;

                                         if(!ValidFile)
                                                {
                                                if((ID == ID_CAT) && (cn->cn_Type == _ID_TYPE))
                                                   {
                                                   ValidFile = TRUE;
                                                   continue;
                                                   }

                                                break;
                                                }

///                    ID_VERS
                                         if(ID == ID_VERS)
                                                {
                                                struct BaseVersion version;

                                                if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                                                   {

                                                   }
                                                else
                                                   {
                                                   _RC = IoErr();
                                                   break;
                                                   }

                                                continue;
                                                }
//|

                                         _read(ID_STRT, &sys.last_start)
                                         _read(ID_OPER, &sys.last_operation)
                                         }
                                  }

                           CloseIFF(iff);
                           }

                        Close(iff->iff_Stream);
                        }


//       if(_RC == IFFERR_EOF) _RC = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

           FreeIFF(iff);
           }

           set(app, MUIA_Application_Sleep, FALSE);

           return(0);
}

//|
/// ZapiszSystemInfo

char ZapiszSystemInfo(void)
{

#define _ID_TYPE ID_SYS

struct IFFHandle *MyIFFHandle;

        set(app, MUIA_Application_Sleep, TRUE);

        if(MyIFFHandle = AllocIFF())
                {
                BPTR  FileHandle;

                if(FileHandle = Open(SystemFileName, MODE_NEWFILE))
                   {
                   MyIFFHandle->iff_Stream = FileHandle;
                   InitIFFasDOS(MyIFFHandle);

                   if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
                           {
                           struct BaseVersion version;

                           PushChunk(MyIFFHandle, _ID_TYPE, ID_CAT, IFFSIZE_UNKNOWN);

                           PushChunk(MyIFFHandle, _ID_TYPE, ID_FORM, IFFSIZE_UNKNOWN);
                                   PushChunk(MyIFFHandle, _ID_TYPE, ID_VERS, IFFSIZE_UNKNOWN);
                                   version.Version = VERSION;
                                   version.Revision = REVISION;
                                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                                   PopChunk(MyIFFHandle);

                           PopChunk(MyIFFHandle);

                           PushChunk(MyIFFHandle, _ID_TYPE, ID_FORM, IFFSIZE_UNKNOWN);

                           _write(ID_STRT, sys.last_start)
                           _write(ID_OPER, sys.last_operation)

                           PopChunk(MyIFFHandle);

                           PopChunk(MyIFFHandle);
                           CloseIFF(MyIFFHandle);
                           }
                   else
                           {
                           DisplayBeep(0);
                           D(bug("*** OpenIFF() nie powiod�o si�\n"));
                           }

                   Close(FileHandle);
                   }
                else
                   {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
                   D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", SystemFileName));
                   }

                FreeIFF(MyIFFHandle);
                }
         else
                {
                DisplayBeep(0);
                D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
                }

        set(app, MUIA_Application_Sleep, FALSE);


        return(0);

#undef _ID_TYPE
}



//|
/// ZapiszLastOperationToday

/*
** ustawia sys.last_operation = GetCurrentTime()
** i zapisuje system info
*/

char ZapiszLastOperationToday(void)
{
        memcpy(&sys.last_operation, wfmh_GetCurrentTime(), sizeof(struct DateStamp ) );
        return( ZapiszSystemInfo() );
}
//|

/// Create classes...

char CreateClasses(void)
{
char Result = TRUE;

   Result = Result && (CL_UsersList          = MUI_CreateCustomClass(NULL, list_class, NULL, sizeof(struct UsersList_Data          ), UsersList_Dispatcher          ));
   Result = Result && (CL_ProductList        = MUI_CreateCustomClass(NULL, list_class, NULL, sizeof(struct ProductList_Data        ), ProductList_Dispatcher        ));
   Result = Result && (CL_FakturaProductList = MUI_CreateCustomClass(NULL, list_class, NULL, sizeof(struct FakturaProductList_Data ), FakturaProductList_Dispatcher ));
   Result = Result && (CL_GroupList          = MUI_CreateCustomClass(NULL, list_class, NULL, sizeof(struct GroupList_Data          ), GroupList_Dispatcher          ));
//   Result = Result && (CL_CustomerList       = MUI_CreateCustomClass(NULL, list_class, NULL, sizeof(struct CustomerList_Data       ), CustomerList_Dispatcher       ));
//   Result = Result && (CL_UnitList           = MUI_CreateCustomClass(NULL, list_class, NULL, sizeof(struct UnitList_Data           ), UnitList_Dispatcher           ));
//   Result = Result && (CL_PayList            = MUI_CreateCustomClass(NULL, list_class, NULL, sizeof(struct PayList_Data            ), PayList_Dispatcher            ));
   Result = Result && (CL_DocumentList       = MUI_CreateCustomClass(NULL, list_class, NULL, sizeof(struct DocumentList_Data       ), DocumentList_Dispatcher       ));
   Result = Result && (CL_SmartCycle         = MUI_CreateCustomClass(NULL, MUIC_Group, NULL, sizeof(struct SmartCycle_Data         ), SmartCycle_Dispatcher         ));
//   Result = Result && (CL_BString            = MUI_CreateCustomClass(NULL, MUIC_BetterString , NULL, sizeof(struct BString_Data    ), BString_Dispatcher            ));
   Result = Result && (CL_ZakupyList         = MUI_CreateCustomClass(NULL, list_class, NULL, sizeof(struct ZakupyList_Data         ), ZakupyList_Dispatcher         ));

   Result = Result && (CL_ResultList         = Inspector_CreateClass( list_class ));

   return(Result);
}
//|
/// Delete classes...
void DeleteClasses(void)
{

   if(CL_ResultList        ) { MUI_DeleteCustomClass(CL_ResultList); CL_ResultList = NULL; }
   if(CL_UsersList         ) { MUI_DeleteCustomClass(CL_UsersList); CL_UsersList = NULL; }
   if(CL_ProductList       ) { MUI_DeleteCustomClass(CL_ProductList); CL_ProductList = NULL; }
   if(CL_FakturaProductList) { MUI_DeleteCustomClass(CL_FakturaProductList); CL_FakturaProductList = NULL; }
   if(CL_GroupList         ) { MUI_DeleteCustomClass(CL_GroupList); CL_GroupList = NULL; }
//   if(CL_CustomerList      ) { MUI_DeleteCustomClass(CL_CustomerList); CL_CustomerList = NULL; }
//   if(CL_UnitList          ) { MUI_DeleteCustomClass(CL_UnitList); CL_UnitList = NULL; }
//   if(CL_PayList           ) { MUI_DeleteCustomClass(CL_PayList); CL_PayList = NULL; }
   if(CL_DocumentList      ) { MUI_DeleteCustomClass(CL_DocumentList); CL_DocumentList = NULL; }
   if(CL_SmartCycle        ) { MUI_DeleteCustomClass(CL_SmartCycle); CL_SmartCycle = NULL; }
//   if(CL_BString           ) { MUI_DeleteCustomClass(CL_BString); CL_BString = NULL; }
   if(CL_ZakupyList        ) { MUI_DeleteCustomClass(CL_ZakupyList); CL_ZakupyList = NULL; }

}
//|

/// SetMainInputEvents
void SetMainInputEvents(char page)
{

//    D(bug("SetMainInputEvents() Page: %ld\n", page));

        DoMethod(MainWindow , MUIM_Notify, MUIA_Window_InputEvent, "up"  , MUIV_Notify_Self, 3, MUIM_Set, MUIA_Window_ActiveObject, MUIV_Window_ActiveObject_Prev);
        DoMethod(MainWindow , MUIM_Notify, MUIA_Window_InputEvent, "down", MUIV_Notify_Self, 3, MUIM_Set, MUIA_Window_ActiveObject, MUIV_Window_ActiveObject_Next);


        switch(page)
           {
           case 66:
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f8", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_LOGIN);
                  break;

           case PAGE_MAIN:
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_SPRZEDAZ);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_ZAKUPY);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_REDAGOWANIE);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f4", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_BAZYDANYCH);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f5", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_ROZLICZENIA);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_SYSTEM);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f8", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_LOGINOUT);
                  break;

           case PAGE_ZAKUPY:
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_ZAKUPY_KOSZTY);
                  break;


           case PAGE_SPRZEDAZ:
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_VAT);
//          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_KOREKTA);
//          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_UPROSZCZONY);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f5", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_VAT_PAR);
//          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_UPROSZCZONY_PAR);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f7", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_VAT_PROFORMA);
                  if(settings.posnet_active)
                          DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f8", MUIV_Notify_Application, 2, MUIM_Application_ReturnID , ID_TOOLBAR_PARAGON);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f9", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_NIEUDOKUMENTOWANA);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_ORDER);
                  break;

           case PAGE_REDAGOWANIE:
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_RED_BIE��CE);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_RED_OD�O�ONE);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_RED_PRZELEW);
                  break;

           case PAGE_BAZYDANYCH:
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_MAGAZYN);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_KONTRAHENCI);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_USERS);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f4", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_SETTINGS_PAY);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f5", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_SETTINGS_UNIT);
                  break;

           case PAGE_ROZLICZENIA:
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_ZAMKNIJ);
                  if(settings.posnet_active)
                        {
//            DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f5", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_DOBOWY);
                        DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_DOBOWY);
                        DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f7", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_OKRESOWY);
                        DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f8", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_STAN_KASY);
                        DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f9", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_ZMIANY);
                        }
                  break;

           case PAGE_SYSTEM:
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_SETTINGS);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_NUMERACJA);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_DRUKARKI);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_TIP);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f7", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_TOOLBAR_INSPEKTOR);
                  DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 1, MUIM_Application_OpenConfigWindow);
                  break;

           }
}
//|
/// ClearMainInputEvents
void ClearMainInputEvents(char page)
{

//    D(bug("ClearMainInputEvents() Page: %ld\n", page));


        switch(page)
           {
           case 66:
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  break;

           case PAGE_MAIN:
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  break;

           case PAGE_ZAKUPY:
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  break;

           case PAGE_SPRZEDAZ:
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
//          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
//          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
//          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  if(settings.posnet_active)
                          DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  break;

           case PAGE_REDAGOWANIE:
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  break;

           case PAGE_BAZYDANYCH:
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  break;

           case PAGE_ROZLICZENIA:
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  if(settings.posnet_active)
                        {
//            DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                        DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                        DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                        DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                        DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                        }
                  break;

           case PAGE_SYSTEM:
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
                  break;

           }


        DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
        DoMethod(MainWindow, MUIM_KillNotify, MUIA_Window_InputEvent);

}
//|
/// SwitchToPage
void SwitchToPage(int page)
{

        ClearMainInputEvents( xget(GR_ToolBar_Menus, MUIA_Group_ActivePage) );

        DoMethod(BT_ToolBar_Login, MUIM_MultiSet, MUIA_Disabled, !settings.posnet_active,
                                                        BT_ToolBar_Paragon,
                                                        BT_ToolBar_Rozliczenia_Fisk_Dobowy,
                                                        BT_ToolBar_Rozliczenia_Fisk_Okresowy,
                                                        BT_ToolBar_Rozliczenia_Fisk_StanKasy,
                                                        BT_ToolBar_Rozliczenia_Fisk_RaportZmiany,
                                                        NULL);

        // odpowiedni tekst, w zaleznosci jakie paragony bedziem wystawiac
//    settext(BT_ToolBar_Paragon, settings.posnet_active ? MSG_TOOLBAR_PARAGON_POSNET : MSG_TOOLBAR_PARAGON );


        SetMainInputEvents(page);
        set(GR_ToolBar_Menus, MUIA_Group_ActivePage, page);

        switch(page)
           {
           case PAGE_SPRZEDAZ:
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Sprzeda�_Back);
                   break;
           case PAGE_ZAKUPY:
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Zakupy_Back);
                   break;
           case PAGE_REDAGOWANIE:
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Red_Bie��ce);
                   break;
           case PAGE_BAZYDANYCH:
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Bazy_Back);
                   break;
           case PAGE_SYSTEM:
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_System_Back);
                   break;                                                     
           case PAGE_ROZLICZENIA:
                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Rozliczenia_Back);
                   break;
           }
}
//|

/// CheckNList

/*
** sprawdza czy s� dost�pne klasy NList(view) v19+
** jesli tak, to ustawia zmienne list_class i listview_class
** by wskazywalny na klasy NList
**
** zwraca BOOL
*/

char CheckNList(void)
{
Object *o1;
Object *o2;
char   result = FALSE;


        o1 = NListObject, End;
        o2 = NListviewObject, End;

        if( o1 && o2 )
           {
           if( xget( o1, MUIA_Version) >= 19 )
                   {
                   list_class     = MUIC_NList;
                   listview_class = MUIC_NListview;
                   using_nlist    = 2;   // skip value

                   result = TRUE;
                   }
           }

        if( o1 )
           MUI_DisposeObject( o1 );

        if( o2 )
           MUI_DisposeObject( o2 );

        return(result);

}


//|

/// Main
int main(void)
{
//APTR StartupWindow, MainWindow, LoginWindow;


        D(bug("Go!\n"));


/// Opening libraries etc...


   if((MUIMasterBase = OpenLibrary(MUIMASTER_NAME, MUIMASTER_VMIN)) == NULL)
           {
           EasyRequest(NULL, &MyEasy, NULL, MSG_ERR_MUI);
           CleanUp();
           return(EXIT_FAILURE);
           }


   if( ( WfmhBase = OpenLibrary(WFMH_NAME, WFMH_VMIN) ) == NULL )
           {
           MUI_Request( app, NULL, 0, TITLE, MSG_ERR_OK, MSG_ERR_LIB, WFMH_NAME, WFMH_VMIN );
           CleanUp();
           return(EXIT_FAILURE);
           }

   if( (MuiToolkitBase = OpenLibrary( MUITOOLKIT_NAME, MUITOOLKIT_VMIN ) ) == NULL )
           {
           MUI_Request( app, NULL, 0, TITLE, MSG_ERR_OK, MSG_ERR_LIB, MUITOOLKIT_NAME, MUITOOLKIT_VMIN );
           CleanUp();
           return(EXIT_FAILURE);
           }


   if(((LocaleBase = OpenLibrary("locale.library", 38)) == NULL))
           {
           MUI_Request(app, NULL, 0, TITLE, MSG_ERR_OK, MSG_ERR_LIB, "locale.library", 38);
           CleanUp();
           return(EXIT_FAILURE);
           }
   MyLocale  = OpenLocale(NULL);


   if(stricmp(MyLocale->loc_LanguageName, "polski.language") != 0)
         {
         DisplayBeep(0);
         MUI_Request(app, NULL, 0, TITLE, MSG_ERR_OK, MSG_WAR_NO_POLSKI);
         CleanUp();
         return(EXIT_FAILURE);
         }



//|


   // sprawdzamy czy kto� zamawia� NLista....

   if( wfmh_isenv("Golem/UseNList") )
           {
           CheckNList();
           }



   if(CreateClasses())
          {
          if(app = CreateApp())
                {
				MUI_Request(app, NULL, 0, TITLE, MSG_OK, "\033c\033b\033uGolem-Amiga OpenSource Project\033n\n\n"
										"Od wersji 1.0 zdecydowa�em si� na udost�pnienie\n"
										"pakietu Golem-Amiga na zasadach Open Source.\n"
										"Zainteresowanych programem i jego rozwojem zapraszam na\n"
										"now� stron� domow� projektu Golem-Amiga Open Source\n"
										"\n"
										"\033b" WWW "\033n"
										);


                Logout();
                ToolBarLogin(FALSE);


// Inicjalizacja
                set(BT_Startup_Ok, MUIA_Disabled, TRUE);
                set(StartupWindow, MUIA_Window_Open, TRUE);

                set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_INIT);
//        Delay(10);

                DateStamp(&Today);



/// LOADING DOCUMENTS INFORMATION & SYSTEM SETTINGS

//        Delay(25);

                DateStamp(&docs.data_fvat);
                DateStamp(&docs.data_fvat_kor);
                DateStamp(&docs.data_fvat_par);
                DateStamp(&docs.data_rach);
                DateStamp(&docs.data_rach_kor);
                DateStamp(&docs.data_rach_par);
                DateStamp(&docs.data_paragon);
                DateStamp(&docs.data_order);
                WczytajUstawienia();
                posnet_DisplayAbout();

                set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_NUMBERS);
                WczytajNumeracj�Dokument�w();



//|
/// CHECKING PROGRAM DIRS

//        Delay(25);
                set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_DIRS);

                {
                BPTR  Dir;
                char   DirName[64];
                char   Miesiac[3];
                char   Rok[5];
                char   result = TRUE;
                int    i;

                result = result && wfmh_CreateDir(GolemConfigDir);
                result = result && wfmh_CreateDir(DatabasesDir);


                // katalogi dla dokument�w

                wfmh_Date2StrFmtBuffer(Miesiac, "%m", &Today );
                wfmh_Date2StrFmtBuffer(Rok    , "%Y", &Today );

                sprintf(DirName, "%s/%s", DatabasesDir, Rok);
                result = result && wfmh_CreateDir(DirName);
                sprintf(DirName, "%s/%s/%s", DatabasesDir, Rok, Miesiac);
                result = result && wfmh_CreateDir(DirName);

                for(i=0; i < DOCUMENT_COUNT; i++)
                   {
                   sprintf(DirName, "%s/%s/%s/%02ld", DatabasesDir, Rok, Miesiac, i);
                   result = result && wfmh_CreateDir(DirName);
                   }


                for(i=ZAKUPY; i < DOCUMENT_COUNT_2; i++)
                   {
                   sprintf(DirName, "%s/%s/%s/%02lx", DatabasesDir, Rok, Miesiac, i);
                   result = result && wfmh_CreateDir(DirName);
                   }


                for(i = OD_FAKTURA_VAT; i < DOCUMENT_COUNT_3; i++)
                   {
                   sprintf(DirName, "%s/%s/%s/%02lx", DatabasesDir, Rok, Miesiac, i);
                   result = result && wfmh_CreateDir(DirName);
                   }

                 if(!result)
                   {
                   MUI_Request(app, StartupWindow, 0, TITLE, "*_Sprawdz�", "Nie mog� utworzy� niezb�dnych katalog�w!\nPrawdopodobnie dysk jest zabezpieczony przed zapisem");
                   goto Create_Dirs_Problems_Quit;
                   }


                }
//|
/// LOADING SYSTEM INFORMATION

                  set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_DATES);

                  WczytajSystemInfo();

//          if(WczytajSystemInfo())
                        {
//            D(bug("Today '%s'\n", Date2Str(&Today)));
//            D(bug("SystemInfo: LastStart '%s'\n", Date2Str(&sys.last_start)));


                         if( !(wfmh_isenv("Golem/DontWatchTheWatch") ))
                           {

                           // sprawdzamy czy czas tu biegnie normalnie... ;-)
                           if(CompareDates(&sys.last_start, &Today) < 0)
                                  {
                                  DateStamp(&sys.last_start);
                                  ZapiszSystemInfo();

                                  if(MUI_Request(app, NULL, 0, TITLE, MSG_ERR_DATE_GAD, MSG_ERR_DATE))
                                         {
                                         system("RUN <>nil: SYS:Prefs/Time <>nil:");
                                         }

                                  goto Wrong_Date_Quit;
                                  }
                           }

//            Sprawd�CzyKoniecMiesi�caRoku();

                        DateStamp(&sys.last_start);
                        ZapiszSystemInfo();
                        }
/*
                  else
                        {
                        DateStamp(&sys.last_start);
                        ZapiszSystemInfo();
                        }
*/

//|
/// CHECKING ONLINE HELP

//        Delay(25);
                set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_HELP);

                {
                char *guide = wfmh_GetGuideName( "Golem" );

                if( guide )
                   {
                   strcpy( GuideName, guide );
                   set(app, MUIA_Application_HelpFile, GuideName);
                   }
                }

//|
/// LOADING USERS DATABASE

//        Delay(25);

                set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_USERS);
                NewList(&u�ytkownicy);
                WczytajU�ytkownik�w();
                if(IsListEmpty(&u�ytkownicy))
                    {
	                MUI_Request(app, NULL, 0, TITLE, MSG_OK, 
													"\033c"
													"Poniewa� nie znaleziono �adnych definicji u�ytkownik�w\n"
													"systemu, zosta�o utworzone domy�lne konto administratora\n"
													"z domy�lnym has�em \n\n\033bwfmh\033n\n"
													"\n"
													"Zalecanym jest utworzenie w�asnych profili u�ytkownik�w\n"
													"lub co najmniej zmiana domy�lnego has�a administratora\n"
													"(w panelu System/Bazy Danych/Dane u�ytkownik�w)"													);

                    DodajAdministratora();
                    }

//|
/// LOADING STOCK DATABASE & UNITS & PAYMENTS & PRINTERS

//        Delay(25);

                NewList(&jedn_miary);
                WczytajJednostki();

                NewList(&p�atno�ci);
                WczytajP�atno�ci();

                NewList(&drukarki);
                WczytajDrukarki();


                // magazyn wczytujemy na koncu, aby np. e.w.
                // importowane jednostki platnosci nie zastapily
                // wpudowanych

                InitMagazyn();
                WczytajMagazyn();

//|
/// LOADING CUSTOMERS DATABASE

//        Delay(25);

                NewList(&kontrahenci);
                WczytajKlient�w();

//|



                set(GA_Startup_Info  , MUIA_Gauge_InfoText, WWW);
                _enable(BT_Startup_Ok);
                set(StartupWindow  , MUIA_Window_ActiveObject, BT_Startup_Ok);



//        _disable(BT_ToolBar_Upr_Kor);
                _disable(BT_ToolBar_Rozliczenia_Norm);



//        DoMethod(MainWindow, MUIM_Notify, MUIA_Window_InputEvent, "control ralt f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, 6666);


                // iventy domyslne dla okna glownego bez zalogowanego usera
                SetMainInputEvents(66);

                {
                char  running = TRUE;
                ULONG signal  = 0;

                while(running)
                  {
                  switch (DoMethod(app, MUIM_Application_Input, &signal))
                        {
                        case MUIV_Application_ReturnID_Quit:
                                 switch(xget(GR_ToolBar_Menus, MUIA_Group_ActivePage))
                                   {
                                   case 0:
                                                set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Quit);
                                                if(logged_user)
                                                   {
                                                   if(wfmh_isenv("Golem/LogoutNotQuit"))
                                                          {
                                                          goto ToolbarLogout;
                                                          }
                                                   else
                                                          {
                                                          if(MUI_Request(app, MainWindow, 0, TITLE, MSG_APP_QUIT_GAD, MSG_APP_QUIT))
                                                                 running = FALSE;
                                                          }
                                                   }
                                                else
                                                   running = FALSE;
                                                break;

                                   default:
                                                SwitchToPage(0);
                                                break;

                                   }
                                   break;


                        case ID_STARTUP_CLOSED:
                                 set(MainWindow   , MUIA_Window_Open, TRUE);
                                 set(StartupWindow, MUIA_Window_Open, FALSE);
                                 _sleep(FALSE);

                                 posnet_DisplayIdle();

//                                 if( xget( MainWindow, MUIA_Window_Open ) != FALSE )
                                         DoMethod( TOD_TipWindow, MUIM_Tip_Show, MUIV_Tip_Show_Startup );
                                 break;


                        case ID_TOOLBAR_MAINMENU:
                                 SwitchToPage(0);
                                 break;


///        **********   Sprzeda�    ***********

                        case ID_TOOLBAR_SPRZEDAZ:
                                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                         SwitchToPage(PAGE_SPRZEDAZ);
                                 break;

                        case ID_TOOLBAR_VAT:
                                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                   {
                                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Vat);
                                   Sprzeda�(FAKTURA_VAT, NULL);
                                   }
                                 break;

                        case ID_TOOLBAR_KOREKTA:
                                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                   {
                                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Vat_Kor);
                                   Korekta();
                                   }
                                 break;

/*
                        case ID_TOOLBAR_UPROSZCZONY:
                                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                   {
                                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Upr);
                                   Sprzeda�(RACHUNEK, NULL);
                                   }
                                 break;
*/

                        case ID_TOOLBAR_VAT_PAR:
                                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                   {
                                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Vat_Par);
                                   Sprzeda�(FVAT_PAR, NULL);
                                   }
                                 break;
/*
                        case ID_TOOLBAR_UPROSZCZONY_PAR:
                                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                   {
                                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Upr_Par);
                                   Sprzeda�(RACH_PAR, NULL);
                                   }
                                 break;
*/
                        case ID_TOOLBAR_PARAGON:
                                 if(settings.posnet_active)
                                   {
                                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Paragon);
                                   Sprzeda�(PARAGON, NULL);
                                   }
                                 break;

                        case ID_TOOLBAR_NIEUDOKUMENTOWANA:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Nieudokumentowana);
                                 Sprzeda�(ODRECZNA, NULL);
                                 break;

                        case ID_TOOLBAR_ORDER:
//                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                   {
                                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Order);
                                   Sprzeda�(ORDER, NULL);
                                   }
                                 break;

                        case ID_TOOLBAR_VAT_PROFORMA:
//                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                   {
                                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Vat);
                                   Sprzeda�(FVAT_PROFORMA, NULL);
                                   }
                                 break;


//|
///        **********    Zakupy     ***********


                        case ID_TOOLBAR_ZAKUPY:
//                 if(!Sprawd�CzyKoniecMiesi�caRoku())
//                   {
                                   SwitchToPage(PAGE_ZAKUPY);
//                   }
                                 break;

                   case ID_TOOLBAR_ZAKUPY_KOSZTY:
                                   Zakupy();
                                break;

//|
///        **********  Redagowanie  ***********

                        case ID_TOOLBAR_REDAGOWANIE:
//                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                   {
                                   SwitchToPage(PAGE_REDAGOWANIE);
                                   }
                                 break;

                        case ID_TOOLBAR_RED_BIE��CE:
//                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                   {
                                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Red_Bie��ce);
                                   Redagowanie(DOKUMENTY_BIE��CE);
                                   }
                                 break;

                        case ID_TOOLBAR_RED_OD�O�ONE:
//                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                   {
                                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Red_Od�o�one);
                                   Redagowanie( DOKUMENTY_OD�O�ONE );
                                   }
                                 break;

                        case ID_TOOLBAR_RED_PRZELEW:
                                   {
                                   set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Red_Przelew);
                                   PrzelewLuzem();
                                   }
                                 break;

//|
///        **********  Bazy danych  ***********

                        case ID_TOOLBAR_BAZYDANYCH:
                                 SwitchToPage(PAGE_BAZYDANYCH);
                                 break;

                        case ID_TOOLBAR_MAGAZYN:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Magazyn);
                                 EdycjaMagazynu();
                                 break;

                        case ID_TOOLBAR_KONTRAHENCI:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Kontrahenci);
                                 EdycjaKontrahent�w();
                                 break;

                        case ID_TOOLBAR_USERS:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_UserBase);
                                 U�ytkownicy();
                                 break;

                        case ID_TOOLBAR_SETTINGS_PAY:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Settings_Pay);
                                 UstawieniaP�atno�ci();
                                 break;

                        case ID_TOOLBAR_SETTINGS_UNIT:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Settings_Unit);
                                 UstawieniaJedn();
                                 break;


//|
///        **********  Rozliczenia  ***********

                        case ID_TOOLBAR_ZAMKNIJ:
                           switch( MUI_Request( app, MainWindow, 0, TITLE, MSG_ROZL_ZAMKNIJ_REQ_GAD, MSG_ROZL_ZAMKNIJ_REQ) )
                                   {
                                   case 2:
                                           ZamknijRok();
                                           break;
                                   case 1:
                                           ZamknijMiesiac();
                                           break;
                                   case 0:
                                           break;
                                   }
                           break;

/*
                        case ID_TOOLBAR_ZAMKNIJ_MIESIAC:
                                 if( MUI_Request(app, MainWindow, 0, TITLE, MSG_ROZL_ZAMKNIJ_GAD, MSG_ROZL_ZAMKNIJ_MIESIAC) )
                                   ZamknijMiesiac();
                                 break;

                        case ID_TOOLBAR_ZAMKNIJ_ROK:
                                 if( MUI_Request(app, MainWindow, 0, TITLE, MSG_ROZL_ZAMKNIJ_GAD, MSG_ROZL_ZAMKNIJ_ROK) )
                                   ZamknijRok();
                                 break;
*/

                        case ID_TOOLBAR_ROZLICZENIA:
                                 SwitchToPage(PAGE_ROZLICZENIA);
                                 break;

                        case ID_TOOLBAR_POSNET_RAPORT_DOBOWY:
                                 RaportFiskalny(POSNET_RAPORT_DOBOWY);
                                 posnet_DisplayIdle();
                                 break;

                        case ID_TOOLBAR_POSNET_RAPORT_OKRESOWY:
                                 RaportFiskalny(POSNET_RAPORT_OKRESOWY);
                                 posnet_DisplayIdle();
                                 break;

                        case ID_TOOLBAR_POSNET_STAN_KASY:
                                 posnet_StanKasy();
                                 break;

                        case ID_TOOLBAR_POSNET_RAPORT_ZMIANY:
                                 posnet_RaportZmiany();
                                 break;



//|
///        **********    System     ***********

                        case ID_TOOLBAR_SYSTEM:
//                 if(!Sprawd�CzyKoniecMiesi�caRoku())
                                   {
                                   SwitchToPage(PAGE_SYSTEM);
                                   }
                                 break;

                        case ID_TOOLBAR_SETTINGS:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Settings);
                                 EdycjaUstawie�();
                                 break;

                        case ID_TOOLBAR_NUMERACJA:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Numeracja);
                                 EdycjaNumeracji();
                                 break;

                        case ID_TOOLBAR_DRUKARKI:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Drukarki);
                                 EdycjaDrukarek();
                                 break;

//|


                        case ID_TOOLBAR_ABOUT:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_About);
                                 _sleep(TRUE);
                                 set(GA_Startup_Info  , MUIA_Gauge_InfoText, WWW);
                                 set(BT_Startup_Ok    , MUIA_Disabled, FALSE);
                                 set(CR_Startup_Scroll, MUIA_Virtgroup_Top, 0);
                                 set(StartupWindow    , MUIA_Window_ActiveObject, BT_Startup_Ok);
                                 set(StartupWindow    , MUIA_Window_Open, TRUE);

                                 posnet_DisplayAbout();
                                 break;


                        case ID_TOOLBAR_TIP:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Tip);
                                 DoMethod( TOD_TipWindow, MUIM_Tip_Show, MUIV_Tip_Show_Next );
                                 break;

                        case ID_TOOLBAR_INSPEKTOR:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Inspektor);
                                 Inspektor( MainWindow );
                                 break;

                        case ID_STARTUP_URL:
                                 if( !wfmh_OpenURL( WWW ))
                                   DisplayBeep(0);
                                 break;


                        case ID_TOOLBAR_LOGINOUT:
                                 {
                                 if(logged_user == NULL)
                                   goto ToolbarLogin;
                                 else
                                   goto ToolbarLogout;
                                 }
                                 break;


                        case ID_TOOLBAR_LOGIN:
                                 {
ToolbarLogin:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Login);
                                 if(logged_user = Login())
                                   {
                                   ToolBarLogin(TRUE);
                                   ClearMainInputEvents(66);
                                   SetMainInputEvents(0);
                                   }
                                 }
                                 break;
                        case ID_TOOLBAR_LOGOUT:
ToolbarLogout:
                                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Logout);
                                 Logout();
                                 ClearMainInputEvents(0);
                                 ToolBarLogin(FALSE);
                                 SetMainInputEvents(66);
                                 break;



                        }
                  if(running && signal) Wait(signal);
                  }
                }

                Usu�Magazyn();
                Usu�Klient�w( UK_REMOVE_ALL );
                Usu�Jednostki( TRUE );
                Usu�U�ytkownik�w( TRUE );
                Usu�Drukarki( TRUE );

No_LocalePL_Quit:
Create_Dirs_Problems_Quit:
Wrong_Date_Quit:
Nie_Zamykaj_Miesiaca:
No_SQL_Quit:
                if(app) MUI_DisposeObject(app);
                }
          else
                {
                EasyRequest(NULL, &MyEasy, NULL, MSG_ERR_APP);
                CleanUp();
                D(bug("Quit: EXIT_FAILURE\n"));
                return(EXIT_FAILURE);
                }

          DeleteClasses();
          }
   else
          {
          DisplayBeep(0);
          D(bug("Can't create classes!\n"));
          }

No_Keyfile_Quit:
   CleanUp();
   D(bug("Quit: EXIT_SUCCESS\n"));
   return(EXIT_SUCCESS);
}
//|

