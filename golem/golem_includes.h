
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <error.h>
#include <ctype.h>
#include <time.h>

#include <exec/exec.h>
#include <exec/types.h>
#include <exec/memory.h>
#include <exec/execbase.h>
#include <dos/dos.h>
#include <dos/dostags.h>
#include <devices/serial.h>
#include <devices/printer.h>

#include <libraries/mui.h>
#undef   ListObject
#undef   ListviewObject
#define  ListObject      MUI_NewObject(list_class
#define  ListviewObject  MUI_NewObject(listview_class

#include <libraries/iffparse.h>
#include <libraries/locale.h>
#include <libraries/openurl.h>
#include <libraries/asl.h>

#include <proto/dos.h>
#include <clib/alib_protos.h>
#include <proto/utility.h>
#include <proto/locale.h>
#include <proto/iffparse.h>
#include <proto/asl.h>
#include <proto/exec.h>
#include <proto/muimaster.h>
#include <proto/openurl.h>

#include "debug.h"

//#include <mui/Popdevice_mcc.h>
#include <mui/Crawling_mcc.h>
#include <mui/Betterstring_mcc.h>

//#include "//vapor/textinput/textinput_mcc.h"
//#include <mui/textinput_mcc.h>

#include <mui/interlist.h>
#include <mui/mui.h>
#include <mui/nlist_mcc.h>
#include <mui/nlistview_mcc.h>
//#include <mui/monthnavigator_mcc.h>
#undef MUIA_Prop_DeltaFactor
#include <mui/muiundoc.h>


// MuiToolkit & WFMH Library

#include <libraries/wfmh.h>
#include <libraries/muitoolkit.h>
#include <proto/wfmh.h>
#include <proto/muitoolkit.h>

// Internal classes

#include "golem_bstring_mcc.h"
#include "golem_custlist_mcc.h"
#include "golem_unitlist_mcc.h"
#include "golem_paylist_mcc.h"
#include "golem_bankstring_mcc.h"
#include "golem_datestring_mcc.h"
//#include "golem_calendar_mcc.h"
#include "golem_prtlist_mcc.h"

// external (own) classes

#include "//vapor/tip/tipwindow_mcc.h"
#include "//vapor/popph/popplaceholder_mcc.h"


extern struct Library  *SysBase;
extern struct Library  *WfmhBase;
extern struct Library  *MuiToolkitBase;
