
// some MT replacements

#ifdef NO_MT_MACROS

// niektore klasy tego uzywaja (z racji nieuzywania muitoolkit)
#define settext(a,b)    set(a, MUIA_Text_Contents, b)
#define getstr(a)       (char *)xget((a), MUIA_String_Contents)
#define copystr(a,b)    strcpy((a), (char *)xget((b), MUIA_String_Contents))
#define getnumstr(a)    xget((a), MUIA_String_Integer)
#define gettext(a)      (char *)xget((a), MUIA_Text_Contents)
#define getcycle(a)     xget((a), MUIA_Cycle_Active)
#define getcheckmark(a) xget((a), MUIA_Selected)
#define _attachwin(a)   DoMethod(app, OM_ADDMEMBER, (a))
#define _detachwin(a)   DoMethod(app, OM_REMMEMBER, (a)); MUI_DisposeObject((a))

#else

#define getstr(a)       getstring( (a) )
#define copystr(a,b)    copystring(a,b)
#define getnumstr(a)    getnumstring( (a) )
#define _attachwin(a)   mt_WinAttach( app, (a) )
#define _detachwin(a)   mt_WinDetach( app, (a) )
#define getradio(obj)   xget( obj,MUIA_Radio_Active )
#define setradio(a,b)   set( a,MUIA_Radio_Active,b )

#endif


//#define _read(a,b)   {if(ID == (a)) {if(ReadChunkBytes(iff, (b), cn->cn_Size) != cn->cn_Size) {Error = IoErr(); break; } continue; }}
//#define _read(a,b)   {if(ID == (a)) {if(ReadChunkBytes(iff, (b), sizeof(cn->cn_Size)) != cn->cn_Size) {Error = IoErr(); break; } continue; }}
#define _read(a,b)        {if(ID == (a)) {if(ReadChunkBytes(iff, (b), sizeof(b)) != cn->cn_Size) {Errors++; continue;}}}
#define _read_debug(a,b)  {if(ID == (a)) {D(bug(" Chunk: %04lx  want to read: %ld\n", a, sizeof(b))); if(ReadChunkBytes(iff, (b), sizeof(b)) != cn->cn_Size) {Errors++; continue; }}}

#define _write(a,b)  {PushChunk(MyIFFHandle, _ID_TYPE, (a), IFFSIZE_UNKNOWN); WriteChunkBytes(MyIFFHandle, &(b), sizeof(b)); PopChunk(MyIFFHandle); }
//#define _write_size(a,b,c)  {PushChunk(MyIFFHandle, _ID_TYPE, (a), IFFSIZE_UNKNOWN); WriteChunkBytes(MyIFFHandle, &(b), sizeof(c)); PopChunk(MyIFFHandle); }
#define _write_size(a,b,c)  {PushChunk(MyIFFHandle, _ID_TYPE, (a), IFFSIZE_UNKNOWN); WriteChunkBytes(MyIFFHandle, b, sizeof(c)); PopChunk(MyIFFHandle); }

#define _read_size(a,b,c)    {if(ID == (a)) {if(ReadChunkBytes(iff, (b), c) != cn->cn_Size) {Errors++; continue;}}}
#define _sleep(a)  set(app, MUIA_Application_Sleep, (a))

#define WinOpen(a)      mt_WinOpen( (a) )


#define _disable(a)  set((a), MUIA_Disabled, TRUE)
#define _enable(a)   set((a), MUIA_Disabled, FALSE)

#define TextObject2  TextObject, TextFrame, TextBack

// wrapper do muitoolkit.library

#define  SH_NULL NULL
#define MSG_NULL NULL

#define _SH( a )  MUIA_ShortHelp, (a)
#define _SetHelp( obj, help)	set( obj, MUIA_ShortHelp, help )

#define _TextButtonWeight( str, weight )  (Object *)mt_ButtonWeight( MSG_##str, weight, SH_##str )
#define _TextButton( a )                  (Object *)mt_Button( MSG_##a, SH_##a )
#define _MakeString( len, str )           MakeStringSH( len, MSG_##str, SH_##str )
#define _MakeCycle( array, str )		  (Object *)mt_Cycle( array, MSG_##str, 0, SH_##str )
#define _MakeCheck( str )                 (Object *)mt_Checkmark( MSG_##str, FALSE, SH_##str )
#define _MakeNumericString( len, str )	  MakeNumericStringSH( len, MSG_##str, SH_##str )

#define _KeyNotify(obj,key,id)	  DoMethod(obj, MUIM_Notify, MUIA_Window_InputEvent, key, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, id)
#define	_CloseNotify(obj,id)      DoMethod(obj, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, id)
 
