
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <error.h>
#include <stdarg.h>
#include <stdlib.h>
#include <math.h>

#include <exec/exec.h>
#include <exec/types.h>
#include <dos/dos.h>
#include <dos/dostags.h>
#include <libraries/iffparse.h>

#include <proto/icon.h>
#include <proto/dos.h>
#include <proto/utility.h>
#include <proto/iffparse.h>
#include <proto/exec.h>

#include "golem_revision.h"
#include "golem.h"
#include "golem_structs.h"

#define buffer_size 2048

#define FALSE 0
#define TRUE  1


/// convert

/*
**  zamienia znak FROM na TO w string BUF
*/

void convert(char *buf, char from, char to)
{
int i;

    for(i=0; i<strlen(buf); i++)
       if(buf[i] == from)
           buf[i] = to;

}
//|

/// strchrs

/*
** przeszukuje string BUF, w poszukiwaniu NAJBLIZSZEGO
** wystapienia dowolnego ze znakow STOPS
** zwraca char * na ten znak lub NULL
*/

char *strchrs(char *buf, char *stops)
{
char *result = NULL;
int  i;

    for(i = 0; i <= strlen(stops); i++)
       {
       char *tmp = strchr(buf, stops[i]);

       if(tmp)
           {
           if(result)
               {
               if(tmp < result)
                   result = tmp;
               }
           else
               result = tmp;
           }
       }


    return(result);
}

//|
/// sgeti

/*
** parsuje string do napotkania pierwszej spacji, TABa lub LFa
** zwraca longa
*/

char *sgeti(char *buf, long *out)
{
char tmp[40] = "";
char *ret;

    memset(tmp, 0, sizeof(tmp));
    ret = strchrs(buf, "\t\n");
    memcpy(tmp, buf, ret - &buf[0]);
    sscanf(tmp, "%ld", out);

    return(ret+1);
}

//|
/// sgets
char *sgets(char *buf, char *out)
{
char tmp[256] = "";
char *ret;

    memset(tmp, 0, sizeof(tmp));
    ret = strchrs(buf, "\t\n");
    memcpy(tmp, buf, ret - &buf[0]);
    sprintf(out, tmp);
//    printf("'%s'\n", tmp);

    return(ret+1);
}
//|

/// ZapiszCentrale

char ZapiszCentrale(struct BankInfo *info)
{
char   FileName[200];
struct IFFHandle *MyIFFHandle;
BPTR   DirLock;


    if(strlen(info->Numer) > 3)
       return(0);


    sprintf( FileName, BankiDir "/%s", info->Numer );
        DirLock = CreateDir( FileName );
    if(DirLock)
       UnLock(DirLock);




    sprintf( FileName, BankiDir "/%s/%s.golem", info->Numer, info->Numer );

    if(MyIFFHandle = AllocIFF())
        {
        BPTR  FileHandle;

        if(FileHandle = Open( FileName, MODE_NEWFILE ))
           {
           MyIFFHandle->iff_Stream = FileHandle;
           InitIFFasDOS(MyIFFHandle);

           if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
               {
               struct BaseVersion version;

               PushChunk(MyIFFHandle, ID_BANK, ID_FORM, IFFSIZE_UNKNOWN);
/*
/*
               PushChunk(MyIFFHandle, ID_BANK, ID_VERS, IFFSIZE_UNKNOWN);
                   version.Version  = VERSION;
                   version.Revision = REVISION;
                   WriteChunkBytes( MyIFFHandle, &version, sizeof(version) );
               PopChunk(MyIFFHandle);
*/
*/

               PushChunk(MyIFFHandle, ID_BANK, ID_CENT, IFFSIZE_UNKNOWN);
               WriteChunkBytes( MyIFFHandle, info, sizeof(struct BankInfo) );
               PopChunk(MyIFFHandle);


               CloseIFF(MyIFFHandle);
               }
           else
               {
               DisplayBeep(0);
//               D(bug("*** OpenIFF() nie powiod�o si�\n"));
               }

           Close(FileHandle);
           }
        else
           {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
//           D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", MagazynFileName));
           }

        FreeIFF(MyIFFHandle);
        }
     else
        {
//        DisplayBeep(0);
//        D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
        }


    return(0);
}


//|
/// KonwertujCenrale

void KonwertujCentrale(void)
{
FILE *file;


     if(file = fopen("Golem:banki_centrale.txt", "rb+") )
       {
       char   buffer[2048];
       struct BankInfo info = {0};
       int    count = 0;
       int    spoldzielcze = 0;

       while(fgets(buffer, sizeof(buffer), file) != NULL)
           {
           char *buf = buffer;

           memset(&info, 0, sizeof(struct BankInfo));

           buf = sgets(buf, info.Numer);
           buf = sgets(buf, info.Nazwa);
           buf = sgets(buf, info.Skr�cona);
           buf = sgets(buf, info.Info);
           info.Flags = 0;

           ZapiszCentrale(&info);

           if(info.Numer[0] < '8')
               count++;
           else
               spoldzielcze++;
           }

       fclose(file);

       printf(" %ld bankow\n", count);
       printf(" %ld bankow spoldzielczych\n", spoldzielcze);
       }

}

//|

/// ZapiszBank

char ZapiszBank(struct Bank *info)
{
char   FileName[200];
struct IFFHandle *MyIFFHandle;


//    if(strlen(info->Numer) > 3)
//       return(0);


    if( info->Numer[0] < '8' )
       {
//       return(0);
       sprintf( FileName, BankiDir "/%3.3s/%s.golem", info->Numer, info->Numer );
       }
    else
       {
       return(0);
        sprintf( FileName, BankiDir "/%4.4s/%s.golem", info->Numer, info->Numer );
       }

    if(MyIFFHandle = AllocIFF())
        {
        BPTR  FileHandle;

        if(FileHandle = Open( FileName, MODE_NEWFILE ))
           {
           MyIFFHandle->iff_Stream = FileHandle;
           InitIFFasDOS(MyIFFHandle);

           if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
               {
               struct BaseVersion version;

               PushChunk(MyIFFHandle, ID_BANK, ID_FORM, IFFSIZE_UNKNOWN);
/*
               PushChunk(MyIFFHandle, ID_BANK, ID_VERS, IFFSIZE_UNKNOWN);
                   version.Version  = VERSION;
                   version.Revision = REVISION;
                   WriteChunkBytes( MyIFFHandle, &version, sizeof(version) );
               PopChunk(MyIFFHandle);
*/

               PushChunk(MyIFFHandle, ID_BANK, ID_BANK, IFFSIZE_UNKNOWN);
               WriteChunkBytes( MyIFFHandle, info, sizeof(struct Bank) );
               PopChunk(MyIFFHandle);


               CloseIFF(MyIFFHandle);
               }
           else
               {
               DisplayBeep(0);
//               D(bug("*** OpenIFF() nie powiod�o si�\n"));
               }

           Close(FileHandle);
           }
        else
           {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
//           D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", MagazynFileName));
           }

        FreeIFF(MyIFFHandle);
        }
     else
        {
//        DisplayBeep(0);
//        D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
        }


    return(0);
}


//|
/// KonwertujBank

void KonwertujBank(void)
{
FILE *file;


     if(file = fopen("Golem:banki_oddzialy.txt", "rb+") )
       {
       char   buffer[2048];
       struct Bank info = {0};
       int    count = 0;
       int    spoldzielcze = 0;

       while(fgets(buffer, sizeof(buffer), file) != NULL)
           {
           char *buf = buffer;

           memset(&info, 0, sizeof(struct Bank));

           buf = sgets(buf, info.Numer);
           buf = sgets(buf, info.Opis);
           buf = sgets(buf, info.Skr�cona);
           buf = sgets(buf, info.Ulica);
           buf = sgets(buf, info.Kod);
           buf = sgets(buf, info.Miasto);
           info.Flags = 0;

           ZapiszBank(&info);

           if(info.Numer[0] < '8')
               count++;
           else
               spoldzielcze++;
           }

       fclose(file);

       printf(" %ld oddzialow\n", count);
       printf(" %ld oddzialow spoldzielczych\n", spoldzielcze);
       }


}

//|


void main(char argc, char *argv[])
{

    printf("Generuje opisy bankow...\n");
    KonwertujCentrale();

    printf("Generuje opisy oddzialow...\n");
    KonwertujBank();


}

