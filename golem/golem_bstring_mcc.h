/*
*/


/*** Include stuff ***/


#ifndef GOLEM_BSTRING_MCC_H
#define GOLEM_BSTRING_MCC_H

#ifndef LIBRARIES_MUI_H
#include "libraries/mui.h"
#endif


/*** MUI Defines ***/

#define MUIC_Golem_BString  "Golem_BString.mcc"
#define MUIC_Golem_BStringP "Golem_BString.mcp"
#define Golem_BStringObject MUI_NewObject(MUIC_Golem_BString


/*** Methods ***/

//#define MUIM_Tron_Demo           0x8002000b


/*** Method structs ***/


/*** Special method values ***/


/*** Special method flags ***/


/*** Attributes ***/


/*** Special attribute values ***/


/*** Structures, Flags & Values ***/


/*** Configs ***/

//#define MUICFG_Tron_Pen1           0x80020005


#endif /* GOLEM_BSTRING_MCC_H */



