
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
a$Id: golem_prtlist_mcc.c,v 1.1 2003/01/01 20:40:52 carl-os Exp $.
*/

/// Includes

#include <clib/alib_protos.h>
#include <clib/graphics_protos.h>
#include <clib/utility_protos.h>
#include <clib/muimaster_protos.h>
#include <pragmas/muimaster_pragmas.h>
#include <libraries/mui.h>
#if MUIMASTER_VLATEST <= 14
#include <mui/mui33_mcc.h>
#endif
#include <mui/muiundoc.h>


#include <stdio.h>
#include <string.h>
#include <mui/interlist.h>
//#include "golem_strings.h"
#include "golem_structs.h"
#include "golem_prtlist_mcc.h"
#include <mui/nlist_mcc.h>

//#define __NAME      "Golem_CustList: "
#define CLASS       MUIC_Golem_PrtList
#define __NAME      CLASS ": "
#define SUPERCLASS  MUIC_List

void _XCEXIT(int code) {}

//|
/// Data
struct Data
{
   ULONG UsingNList;

   struct Hook DisplayHook;
};
//|

#include "golem_prtlist_revision.h"

#define UserLibID PRTLIST_VERSTAG " � 2000-2002 Marcin Orlowski <carlos@wfmh.org.pl>"
#define MASTERVERSION 14

#define VERSION  PRTLIST_VERSION
#define REVISION PRTLIST_REVISION

#define MYDEBUG 0
#define MCC_USES_LOCALE
#include "golem_mccheader_nlist.c"


#define MSG_PRT_LIST_DEL    ""
#define MSG_PRT_LIST_NAME   "\0338\033bNazwa"
#define MSG_PRT_LIST_DEVICE "\0338\033bSterownik"


/// Prt list hooks
/// HOOK: PrinterList_Display
void * __saveds __asm PrinterList_Display(register __a0 struct Hook *hook, register __a2 char **array, register __a1 struct PrinterList *node)
{
static char Del[20],
			Name[ PRINTER_DEV_LEN + 10],
			Device[ PRINTER_DEV_LEN + 15],
			attr[ 6*3 ];

struct Data *data = hook->h_Data;



   if(node)
	 {
	 struct Printer *printer = &node->pl_printer;

	 if( node->Deleted == FALSE )
	   {
	   Del[0] = 0;
	   }
	 else
	   {
	   Del[0] = '*';
	   Del[1] = 0;
	   }
//     sprintf(Del   , "%s" , ((node->Deleted==FALSE) ? " " : "*"));

	 *array++ = Del;

	 // drukarka domyslna jest boldowana
	 if( printer->ID == 0 )
	   strcpy(attr, "\033b");
	 else
	   attr[0] = 0;

	 // atrybuty
	 switch( node->pl_printer.Type )
	   {
	   case PRT_TYPE_FILE:
		   strcat(attr, "\0338");
		   break;
	   }



	 sprintf( Name  ,"%s%s", attr, printer->Name );


	 switch( node->pl_printer.Type )
	   {
	   case PRT_TYPE_FILE:
		 sprintf( Device , "%s%s", attr, " -> Zapis do pliku" );
		 break;

	   default:
		 sprintf( Device ,"%s%s / %ld", attr, printer->printer_device, printer->printer_unit );
		 break;
	   }

	 *array++ = Name;
	 *array++ = Device;
	 }
   else
	 {
	 *array++ = MSG_PRT_LIST_DEL;
	 *array++ = MSG_PRT_LIST_NAME;
	 *array++ = MSG_PRT_LIST_DEVICE;
	 }


   return(0);
}
//|
/// HOOK: PrinterList_CompareStr
LONG __saveds __asm PrinterList_CompareStr(register __a1 struct KlientList *Node1, register __a2 struct KlientList *Node2)
{
	return((LONG)StrnCmp(Locale, Node1->kl_klient.Nazwa1, Node2->kl_klient.Nazwa1, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG _New(struct IClass *cl, Object *obj, Msg msg)
{
//static const struct Hook PrinterList_DisplayHook     = { {NULL, NULL}, (VOID *)PrinterList_Display   ,NULL,NULL };
static const struct Hook PrinterList_CompareHookStr  = { {NULL, NULL}, (VOID *)PrinterList_CompareStr,NULL,NULL };

	D(bug( __NAME "OM_NEW\n"));

	obj = (Object *)DoSuperNew(cl, obj,
					InputListFrame,
					_MUIA_List_AutoVisible   , TRUE,
//                    _MUIA_List_DisplayHook   , &PrinterList_DisplayHook,
					_MUIA_List_CompareHook   , &PrinterList_CompareHookStr,
					_MUIA_List_Format        , "MIW=1 MAW=-1 P=\033r BAR,"
											   "MIW=1 MAW=60 BAR,"
											   "MIW=1 MAW=35 ",

					_MUIA_List_Title         , TRUE,

					MUIA_NList_TitleSeparator, TRUE,
					MUIA_NList_AutoCopyToClip, TRUE
					);


		  if(obj)
			 {
			 struct Data *data = INST_DATA(cl,obj);

			 data->UsingNList     = FALSE;

			 if( cl )
			   {
			   D(bug(__NAME "SuperClassID: %s\n", cl->cl_Super->cl_ID));

			   if( strcmp( cl->cl_Super->cl_ID, "NList.mcc" ) == 0)
				   data->UsingNList = TRUE;
			   }


			 data->DisplayHook.h_Data  = (APTR)data;
			 data->DisplayHook.h_Entry = (VOID *)PrinterList_Display;
			 set(obj, _MUIA_List_DisplayHook, &data->DisplayHook );


			 /* trick to set up arguments */

			 msg->MethodID = OM_SET;
			 DoMethodA(obj, (Msg)msg);
			 msg->MethodID = OM_NEW;
			 }

	  return((ULONG)obj);
}

//|
/// OM_SET

ULONG ASM _Set( REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg )
{
struct Data *data = INST_DATA(cl,obj);
struct TagItem *tags,*tag;


//    D(bug( __NAME "OM_SET\n" ));


	for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
	   {
//       D(bug(__NAME "%8lx\n", tag->ti_Tag ));

	   switch(tag->ti_Tag)
		  {
		  case MUIA_List_Active:
			   {
			   LONG active  = 0;
			   LONG set_active;
			   ULONG entries;


			   // let's do the move first...
			   SetSuperAttrs( cl, obj, MUIA_List_Active, tag->ti_Data, TAG_DONE );


			   // now let's check if there's something to be fixed...

			   get( obj, MUIA_List_Active , &active );
			   get( obj, MUIA_List_Entries, &entries );

			   if( active != MUIV_List_Active_Off )
				   {
				   struct KlientList *kl;

				   // is there any separator under active?
				   DoMethod( obj, MUIM_List_GetEntry, active, &kl );
				   if( kl->kl_klient.Separator )
					   {
					   set_active = active;

					   switch( tag->ti_Data )
						   {
						   case MUIV_List_Active_Up:
						   case MUIV_List_Active_Bottom:
						   case MUIV_List_Active_PageUp:
								set_active--;
								if( set_active < 0 )
								   set_active = 1;
								break;


						   case MUIV_List_Active_Down:
						   case MUIV_List_Active_Top:
						   case MUIV_List_Active_PageDown:
								set_active++;
								break;
						   }

					   D(bug( "Active: %ld\n", set_active ));

					   if( ( set_active < 0 ) || ( set_active > entries ) )
						   set_active = MUIV_List_Active_Off;

					   SetSuperAttrs( cl, obj, MUIA_List_Active, set_active, TAG_DONE );
					   }
				   }
			   }
			   return( TRUE );
		  }
	   }

	return( DoSuperMethodA(cl, obj, msg) );
}

//|
/// OM_GET
static ULONG ASM _Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ProductList_Data *data = INST_DATA(cl,obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

	switch(((struct opGet *)msg)->opg_AttrID)
	   {
	   case MUIA_Version:
			*store = PRTLIST_VERSION;
			return(TRUE);
			break;

	   case MUIA_Revision:
			*store = PRTLIST_REVISION;
			return(TRUE);
			break;

	   }

	return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher

__saveds __asm ULONG _Dispatcher( REG(a0) struct IClass *cl,
								  REG(a2) Object *obj,
								  REG(a1) Msg msg )
{

//    D(bug(__NAME "%8lx\n", msg->MethodID ));

	switch( msg->MethodID )
	   {
	   case OM_NEW      : return( _New   ( cl, obj, (APTR)msg ));
	   case OM_SET      : return( _Set   ( cl, obj, (APTR)msg ));
	   case OM_GET      : return( _Get   ( cl, obj, (APTR)msg ));
	   }

	return( DoSuperMethodA( cl,obj,msg ) );
}

//|

