
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 
                         
/*
$Id: golem_przelewy.c,v 1.1 2003/01/01 20:40:54 carl-os Exp $.
*/

#include "golem.h"

/// EDYCJA RODZAJU P�ATNO�CI STRINGS

#define MSG_WIRE_WIN_TITLE "Polecenie przelewu"

#define MSG_WIRE_D_TITLE  "D�u�nik"
#define MSG_WIRE_D_NAME   "N_azwa"
#define MSG_WIRE_D_ULICA  "A_dres"
#define MSG_WIRE_D_KOD    "_Kod"
#define MSG_WIRE_D_MIASTO "_Miasto"
#define MSG_WIRE_D_BANK   "_Bank"
#define MSG_WIRE_D_KONTO  "_Nr. konta"
#define  SH_WIRE_D        "Dane d�u�nika (nadawcy przelewu)"
#define  SH_WIRE_D_OWN    "Wpisuje dane firmy (jak podano w konfiguracji\nprogramu) w pola danych d�u�nika.\n\nSkr�t klawiaturowy 'F1'"

#define MSG_WIRE_W_TITLE  "Wierzyciel"
#define MSG_WIRE_W_NAME   "Na_zwa"
#define MSG_WIRE_W_ULICA  "Adres"
#define MSG_WIRE_W_KOD    "Kod"
#define MSG_WIRE_W_MIASTO "Miasto"
#define MSG_WIRE_W_BANK   "Bank"
#define MSG_WIRE_W_KONTO  "Nr. konta"
#define  SH_WIRE_W        "Dane wierzyciela (odbiorcy przelewu)"
#define  SH_WIRE_W_OWN    "Wpisuje dane firmy (jak podano w konfiguracji\nprogramu) w pola danych wierzyciela.\n\nSkr�t klawiaturowy 'F2'"
                                                                        
#define MSG_WIRE_OPIS     "_Tytu�em"
#define  SH_WIRE_OPIS     "Informacje dot. czego przelew dotyczy.\n\nWyszukiwarka faktur - skr�t 'F3'"
#define MSG_WIRE_DATA     "_Data"
#define MSG_WIRE_KWOTA    "K_wota"

#define MSG_WIRE_OK        "F10 - _Wydrukuj"
#define MSG_WIRE_CANCEL    "ESC - Ponie_chaj"

//|

/// Objects
static Object *PrzelewWindow,             /* EDYCJA RODZAJU P�ATNO�CI */

                          *ST_Wire_D_Nazwa_1,
                          *BT_Wire_D_Owner,
                          *BT_Wire_D_Select,
                          *ST_Wire_D_Nazwa_2,
                          *ST_Wire_D_Ulica,
                          *ST_Wire_D_Kod,
                          *ST_Wire_D_Miasto,
                          *BS_Wire_D_Bank,
                          *ST_Wire_D_Konto,

                          *ST_Wire_W_Nazwa_1,
                          *BT_Wire_W_Owner,
                          *BT_Wire_W_Select,
                          *ST_Wire_W_Nazwa_2,
                          *ST_Wire_W_Ulica,
                          *ST_Wire_W_Kod,
                          *ST_Wire_W_Miasto,
                          *BS_Wire_W_Bank,
                          *ST_Wire_W_Konto,

                          *ST_Wire_Data,
                          *BT_Wire_Data,

                          *ST_Wire_Opis,
                          *BT_Wire_FVSel,
                          *ST_Wire_Kwota,

                          *BT_Wire_Ok,
                          *BT_Wire_Cancel;
//|
/// PrzelewSetup
char PrzelewSetup( struct PrzelewPrint *pp )
{
char NoInput = FALSE;

///   PrzelewWindow

                                         PrzelewWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_WIRE_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_WIRE,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,

                                                   Child, ColGroup(2),

///                                 Dane dluznika
                                                                  Child, ColGroup(2),
                                                                                 GroupFrameT(MSG_WIRE_D_TITLE),
                                                                                 _SH( SH_WIRE_D ),

                                                                                 Child, MakeLabel2(MSG_WIRE_D_NAME),
                                                                                 Child, HGroup,
                                                                                                MUIA_Group_Spacing, 1,
                                                                                                Child, ST_Wire_D_Nazwa_1 = DoString(CUST_NAME_LEN, NULL, NoInput),
                                                                                                Child, BT_Wire_D_Owner   = MUI_MakeObject(MUIO_PopButton, MUII_ArrowLeft),
                                                                                                Child, BT_Wire_D_Select  = PopButton2(MUII_PopUp, MSG_WIRE_D_NAME),
                                                                                                End,

                                                                                 Child, EmptyLabel(),
                                                                                 Child, ST_Wire_D_Nazwa_2 = DoString(CUST_NAME_LEN, NULL, NoInput ),

                                                                                 Child, MakeLabel2(MSG_WIRE_D_ULICA),
                                                                                 Child, ST_Wire_D_Ulica = DoString(CUST_ADRES_LEN, MSG_WIRE_D_ULICA, NoInput),

                                                                                 Child, MakeLabel2(MSG_WIRE_D_KOD),
                                                                                 Child, HGroup,
                                                                                                Child, ST_Wire_D_Kod = DoStringAccept(CUST_KOD_LEN, MSG_WIRE_D_KOD, "0123456789-", NoInput ),

                                                                                                Child, MakeLabel2(MSG_WIRE_D_MIASTO),
                                                                                                Child, ST_Wire_D_Miasto = DoString(CUST_MIASTO_LEN, MSG_WIRE_D_MIASTO, NoInput),
                                                                                                End,

                                                                                 Child, MakeLabel2(MSG_WIRE_D_KONTO),
                                                                                 Child, ST_Wire_D_Konto = DoStringReject(CUST_KONTO_LEN, MSG_WIRE_D_KONTO, "!@#$%^&*()_+|\=[]{}'\";:,<>?", NoInput),
                                                                                 Child, MakeLabel2(MSG_WIRE_D_BANK),
                                                                                 Child, BS_Wire_D_Bank = Golem_BankStringObject, MUIA_BankString_NoInput, NoInput, End,

                                                                                 End,
//|
///                                 Dane wierzyciela
                                                                  Child, ColGroup(2),
                                                                                 GroupFrameT(MSG_WIRE_W_TITLE),
                                                                                 _SH( SH_WIRE_W ),

                                                                                 Child, MakeLabel2(MSG_WIRE_W_NAME),
                                                                                 Child, HGroup,
                                                                                                MUIA_Group_Spacing, 1,
                                                                                                Child, ST_Wire_W_Nazwa_1 = DoString(CUST_NAME_LEN, NULL, NoInput),
                                                                                                Child, BT_Wire_W_Owner   = MUI_MakeObject(MUIO_PopButton, MUII_ArrowLeft),
                                                                                                Child, BT_Wire_W_Select  = PopButton2(MUII_PopUp, MSG_WIRE_W_NAME),
                                                                                                End,

                                                                                 Child, EmptyLabel(),
                                                                                 Child, ST_Wire_W_Nazwa_2 = DoString(CUST_NAME_LEN, NULL, NoInput ),

                                                                                 Child, MakeLabel2(MSG_WIRE_W_ULICA),
                                                                                 Child, ST_Wire_W_Ulica = DoString(CUST_ADRES_LEN, MSG_WIRE_W_ULICA, NoInput),

                                                                                 Child, MakeLabel2(MSG_WIRE_W_KOD),
                                                                                 Child, HGroup,
                                                                                                Child, ST_Wire_W_Kod = DoStringAccept(CUST_KOD_LEN, MSG_WIRE_W_KOD, "0123456789-", NoInput ),

                                                                                                Child, MakeLabel2(MSG_WIRE_W_MIASTO),
                                                                                                Child, ST_Wire_W_Miasto = DoString(CUST_MIASTO_LEN, MSG_WIRE_W_MIASTO, NoInput),
                                                                                                End,

                                                                                 Child, MakeLabel2(MSG_WIRE_W_KONTO),
                                                                                 Child, ST_Wire_W_Konto = DoStringReject(CUST_KONTO_LEN, MSG_WIRE_W_KONTO, "!@#$%^&*()_+|\=[]{}'\";:,<>?", NoInput),
                                                                                 Child, MakeLabel2(MSG_WIRE_W_BANK),
                                                                                 Child, BS_Wire_W_Bank = Golem_BankStringObject, MUIA_BankString_NoInput, NoInput, End,

                                                                                 End,
//|

                                                                  End,

                                                   Child, VGroup,
                                                                  GroupFrame,

                                                                  Child, ColGroup(2),
                                                                                 Child, MakeLabel2( MSG_WIRE_OPIS ),
                                                                                 Child, HGroup,
                                                                                                MUIA_Group_Spacing, 1,
                                                                                                _SH( SH_WIRE_OPIS ),
                                                                                                Child, ST_Wire_Opis = MakeString( 128, MSG_WIRE_OPIS ),
                                                                                                Child, BT_Wire_FVSel = PopButton2(MUII_PopUp, MSG_WIRE_OPIS),
                                                                                                End,
                                                                                 End,

                                                                  Child, ColGroup(4),
                                                                                 Child, MakeLabel2( MSG_WIRE_DATA ),
                                                                                 Child, ST_Wire_Data = MakeString( DATE_LEN, MSG_WIRE_DATA ),

                                                                                 Child, MakeLabel2( MSG_WIRE_KWOTA ),
                                                                                 Child, ST_Wire_Kwota = MakeCashStringWeight( 10, MSG_WIRE_KWOTA, 25 ),
                                                                                 End,
                                                                  End,


                                                   Child, HGroup,
                                                                  Child, BT_Wire_Ok     = TextButton(MSG_WIRE_OK),
                                                                  Child, BT_Wire_Cancel = TextButton(MSG_WIRE_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|

        if( !PrzelewWindow )
           return(FALSE);

        // short helpy
        _SetHelp( BT_Wire_D_Owner, SH_WIRE_D_OWN );
        _SetHelp( BT_Wire_W_Owner, SH_WIRE_W_OWN );
 

        // settings

        setstring( ST_Wire_D_Nazwa_1, pp->Dluznik.Nazwa1 );
        setstring( ST_Wire_D_Nazwa_2, pp->Dluznik.Nazwa2 );
        setstring( ST_Wire_D_Ulica  , pp->Dluznik.Ulica );
        setstring( ST_Wire_D_Kod    , pp->Dluznik.Kod );
        setstring( ST_Wire_D_Miasto , pp->Dluznik.Miasto );
        setstring( BS_Wire_D_Bank   , pp->Dluznik.Bank );
        setstring( ST_Wire_D_Konto  , pp->Dluznik.Konto );

        setstring( ST_Wire_W_Nazwa_1, pp->Wierzyciel.Nazwa1 );
        setstring( ST_Wire_W_Nazwa_2, pp->Wierzyciel.Nazwa2 );
        setstring( ST_Wire_W_Ulica  , pp->Wierzyciel.Ulica );
        setstring( ST_Wire_W_Kod    , pp->Wierzyciel.Kod );
        setstring( ST_Wire_W_Miasto , pp->Wierzyciel.Miasto );
        setstring( BS_Wire_W_Bank   , pp->Wierzyciel.Bank );
        setstring( ST_Wire_W_Konto  , pp->Wierzyciel.Konto );

        setstring( ST_Wire_Data, wfmh_Date2Str( wfmh_GetCurrentTime() ));

        setstring( ST_Wire_Opis, pp->Tytulem );
        setstring( ST_Wire_Kwota, Price2String( pp->Kwota ) );


        // notyfikacja

        DoMethod( BT_Wire_D_Select, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_SELECT_0);
        DoMethod( BT_Wire_W_Select, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_SELECT_1);
        DoMethod( BT_Wire_FVSel   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_SELECT_2);
        DoMethod( BT_Wire_W_Owner, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_SELECT_3);
        DoMethod( BT_Wire_D_Owner, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_SELECT_4);

        DoMethod(ST_Wire_D_Konto, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, BS_Wire_D_Bank, 3, MUIM_Set, MUIA_BankString_Account, MUIV_TriggerValue);
        DoMethod(ST_Wire_W_Konto, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, BS_Wire_W_Bank, 3, MUIM_Set, MUIA_BankString_Account, MUIV_TriggerValue);

        _CloseNotify(PrzelewWindow, ID_CANCEL);
        _KeyNotify(PrzelewWindow, "f1", ID_SELECT_4);
        _KeyNotify(PrzelewWindow, "f2", ID_SELECT_3);
        _KeyNotify(PrzelewWindow, "f3", ID_SELECT_2);
        _KeyNotify(PrzelewWindow, "f10", ID_OK);

        DoMethod(BT_Wire_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Wire_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        _attachwin( PrzelewWindow );


        return(TRUE);
}
//|
/// PrzelewFinish
char PrzelewFinish( struct PrzelewPrint *pp )
{
/*
** sprawdza poprawno�� wprowadzonych danych
** dot. p�atno�ci. Zwraca FALSE, je�li co�
** jest nie tak, TRUE, je�li wszystkie
** elementy s� poprawne, oraz REFRESH,
** je�li nazwa produktu zosta�a zmieniona
** (istotne tylko podczas edycji produktu)
*/

char   result = TRUE;
struct DateStamp *datestamp;


        // dluznik
        if(strlen((char *)xget(ST_Wire_D_Nazwa_1, MUIA_String_Contents)) == 0)
           {
           set(PrzelewWindow, MUIA_Window_ActiveObject, ST_Wire_D_Nazwa_1);
           return(FALSE);
           }
        if(strlen((char *)xget(ST_Wire_D_Ulica, MUIA_String_Contents)) == 0)
           {
           set(PrzelewWindow, MUIA_Window_ActiveObject, ST_Wire_D_Ulica);
           return(FALSE);
           }
        if(strlen(getstr(ST_Wire_D_Miasto)) == 0)
           {
           set(PrzelewWindow, MUIA_Window_ActiveObject, ST_Wire_D_Miasto);
           return(FALSE);
           }
        if(strlen((char *)xget(BS_Wire_D_Bank, MUIA_String_Contents)) < 3)
           {
           set(PrzelewWindow, MUIA_Window_ActiveObject, BS_Wire_D_Bank);
           return(FALSE);
           }
        if(strlen((char *)xget(ST_Wire_D_Konto, MUIA_String_Contents)) < 10)
           {
           set(PrzelewWindow, MUIA_Window_ActiveObject, ST_Wire_D_Konto);
           return(FALSE);
           }


        // wierzyciel
        if(strlen((char *)xget(ST_Wire_W_Nazwa_1, MUIA_String_Contents)) == 0)
           {
           set(PrzelewWindow, MUIA_Window_ActiveObject, ST_Wire_W_Nazwa_1);
           return(FALSE);
           }
        if(strlen((char *)xget(ST_Wire_W_Ulica, MUIA_String_Contents)) == 0)
           {
           set(PrzelewWindow, MUIA_Window_ActiveObject, ST_Wire_W_Ulica);
           return(FALSE);
           }
        if(strlen(getstr(ST_Wire_W_Miasto)) == 0)
           {
           set(PrzelewWindow, MUIA_Window_ActiveObject, ST_Wire_W_Miasto);
           return(FALSE);
           }
        if(strlen((char *)xget(BS_Wire_W_Bank, MUIA_String_Contents)) < 3)
           {
           set(PrzelewWindow, MUIA_Window_ActiveObject, BS_Wire_W_Bank);
           return(FALSE);
           }
        if(strlen((char *)xget(ST_Wire_W_Konto, MUIA_String_Contents)) < 10)
           {
           set(PrzelewWindow, MUIA_Window_ActiveObject, ST_Wire_W_Konto);
           return(FALSE);
           }


        // kwota, opis data
        if(strlen((char *)xget(ST_Wire_Opis, MUIA_String_Contents)) < 3)
           {
           set(PrzelewWindow, MUIA_Window_ActiveObject, ST_Wire_Opis);
           return(FALSE);
           }

        if(!(datestamp = wfmh_Str2Date((char *)xget(ST_Wire_Data, MUIA_String_Contents))))
           {
           setstring(ST_Wire_Data, wfmh_Date2Str(datestamp));
           return(FALSE);
           }



        strcpy(pp->Dluznik.Nazwa1 , (char *)xget(ST_Wire_D_Nazwa_1, MUIA_String_Contents));
        strcpy(pp->Dluznik.Nazwa2 , (char *)xget(ST_Wire_D_Nazwa_2, MUIA_String_Contents));
        strcpy(pp->Dluznik.Ulica  , (char *)xget(ST_Wire_D_Ulica  , MUIA_String_Contents));
        strcpy(pp->Dluznik.Kod    , (char *)xget(ST_Wire_D_Kod    , MUIA_String_Contents));
        strcpy(pp->Dluznik.Miasto , (char *)xget(ST_Wire_D_Miasto , MUIA_String_Contents));
        strcpy(pp->Dluznik.Konto  , (char *)xget(ST_Wire_D_Konto  , MUIA_String_Contents));
        strcpy(pp->Dluznik.Bank   , (char *)xget(BS_Wire_D_Bank   , MUIA_String_Contents));

        strcpy(pp->Wierzyciel.Nazwa1 , (char *)xget(ST_Wire_W_Nazwa_1, MUIA_String_Contents));
        strcpy(pp->Wierzyciel.Nazwa2 , (char *)xget(ST_Wire_W_Nazwa_2, MUIA_String_Contents));
        strcpy(pp->Wierzyciel.Ulica  , (char *)xget(ST_Wire_W_Ulica  , MUIA_String_Contents));
        strcpy(pp->Wierzyciel.Kod    , (char *)xget(ST_Wire_W_Kod    , MUIA_String_Contents));
        strcpy(pp->Wierzyciel.Miasto , (char *)xget(ST_Wire_W_Miasto , MUIA_String_Contents));
        strcpy(pp->Wierzyciel.Konto  , (char *)xget(ST_Wire_W_Konto  , MUIA_String_Contents));
        strcpy(pp->Wierzyciel.Bank   , (char *)xget(BS_Wire_W_Bank   , MUIA_String_Contents));

        strcpy(pp->Tytulem, (char *)xget(ST_Wire_Opis, MUIA_String_Contents) );
        memcpy(&pp->DataPlatnosci, datestamp, sizeof(struct DateStamp));

        pp->Kwota = String2Price((char *)xget(ST_Wire_Kwota, MUIA_String_Contents));


        return(result);

}
//|
/// PrzelewSelector
char PrzelewSelector( struct PrzelewPrint *pp )
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

struct KlientList *wierzyciel = NULL;
struct KlientList *dluznik    = NULL;

        _sleep( TRUE );

        if( !PrzelewSetup( pp ) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }


        if(WinOpen(PrzelewWindow))
                {
                while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);
                  switch(ID)
                        {
                        case ID_SELECT_0:  // dluznik
                           {
                           struct Filtr sel_cust_filter =
                                   {
                                   TRUE,
                                   "#?",              // Nazwa

                                   1,                 // Sprzedawca
                                   1,                 // Nabywca
                                   1,                 // Kosztowiec
                                   0,                 // A
                                   0,                 // B
                                   0                  // C
                                   };

                           dluznik = KontrahentSelector(&sel_cust_filter, dluznik, 0 );

                           if( dluznik )
                                   {
                                   struct Klient    *klient    = &dluznik->kl_klient;

                                   setstring(ST_Wire_D_Nazwa_1, klient->Nazwa1);
                                   setstring(ST_Wire_D_Nazwa_2, klient->Nazwa2);
                                   setstring(ST_Wire_D_Ulica  , klient->Ulica);
                                   setstring(ST_Wire_D_Kod    , klient->Kod);
                                   setstring(ST_Wire_D_Miasto , klient->Miasto);
                                   setstring(ST_Wire_D_Konto  , klient->Konto);
                                   setstring(BS_Wire_D_Bank   , klient->Bank);
                                   }
                           }
                           break;


                        case ID_SELECT_1:  // wierzyciel
                           {
                           struct Filtr sel_cust_filter =
                                   {
                                   TRUE,
                                   "#?",              // Nazwa

                                   1,                 // Sprzedawca
                                   1,                 // Nabywca
                                   1,                 // Kosztowiec
                                   0,                 // A
                                   0,                 // B
                                   0                  // C
                                   };

                           wierzyciel = KontrahentSelector(&sel_cust_filter, wierzyciel, 0 );

                           if(wierzyciel)
                                   {
                                   struct Klient    *klient    = &wierzyciel->kl_klient;

                                   setstring(ST_Wire_W_Nazwa_1, klient->Nazwa1);
                                   setstring(ST_Wire_W_Nazwa_2, klient->Nazwa2);
                                   setstring(ST_Wire_W_Ulica  , klient->Ulica);
                                   setstring(ST_Wire_W_Kod    , klient->Kod);
                                   setstring(ST_Wire_W_Miasto , klient->Miasto);
                                   setstring(ST_Wire_W_Konto  , klient->Konto);
                                   setstring(BS_Wire_W_Bank   , klient->Bank);
                                   }
                           }
                           break;

                        // wybierak faktur
                        case ID_SELECT_2:
                           {
                           struct DocumentScan ds = {0};

                           if( DokumentSelector( &ds, DOKUMENTY_BIE��CE, FALSE, NULL ) )
                                   {
                                   char tmp[128];

                                   sprintf(tmp, "Nale�no�� za %s %s z dn. %s",
                                                           RodzajeDokument�wLVTable[ ds.RodzajDokumentu ],
                                                           ds.NumerDokumentu,
                                                           wfmh_Date2Str(&ds.DataWystawienia)
                                                  );
                                   setstring( ST_Wire_Opis, tmp );
                                   
                                        if( ds.TotalFound )
                                                setstring( ST_Wire_Kwota, Price2String( ds.TotalNetto + ds.TotalVat ) );
                                   }
                           }
                           break;

                        // Dane ownera do pola Wierzyciel
                        case ID_SELECT_3:
                           {
                           struct Klient *klient = &settings.wystawca;

                           setstring(ST_Wire_W_Nazwa_1, klient->Nazwa1);
                           setstring(ST_Wire_W_Nazwa_2, klient->Nazwa2);
                           setstring(ST_Wire_W_Ulica  , klient->Ulica);
                           setstring(ST_Wire_W_Kod    , klient->Kod);
                           setstring(ST_Wire_W_Miasto , klient->Miasto);
                           setstring(ST_Wire_W_Konto  , klient->Konto);
                           setstring(BS_Wire_W_Bank   , klient->Bank);
                           }
                           break;

                        // Dane ownera do pola Dluznika
                        case ID_SELECT_4:
                           {
                           struct Klient *klient = &settings.wystawca;

                           setstring(ST_Wire_D_Nazwa_1, klient->Nazwa1);
                           setstring(ST_Wire_D_Nazwa_2, klient->Nazwa2);
                           setstring(ST_Wire_D_Ulica  , klient->Ulica);
                           setstring(ST_Wire_D_Kod    , klient->Kod);
                           setstring(ST_Wire_D_Miasto , klient->Miasto);
                           setstring(ST_Wire_D_Konto  , klient->Konto);
                           setstring(BS_Wire_D_Bank   , klient->Bank);
                           }
                           break;

                        case ID_OK:
                           result = PrzelewFinish( pp );

                           if(result == FALSE)
                                 {
                                 DisplayBeep(0);
                                 }
                           else
                                 {
                                 running = FALSE;
                                 }
                           break;


                        case ID_CANCEL:
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

                set(PrzelewWindow, MUIA_Window_Open, FALSE);
                }
         else
                {
                DisplayBeep(NULL);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }

        _detachwin( PrzelewWindow );
        _sleep(FALSE);

        return(result);

}
//|

/// PrzelewLuzem
void PrzelewLuzem( void )
{
struct PrzelewPrint  pp = {0};


        if( PrzelewSelector( &pp ) )
           {
           // druczym
           struct DocumentPrint dp = {0};

                dp.pp = &pp;

                D(bug("n1: '%s'\n", pp.Dluznik.Nazwa1 ));

                dp.RodzajDokumentu = PRT_PRZELEW;
                WydrukujDokument( &dp );
           }
}
//|
