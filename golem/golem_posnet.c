
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

#include "Golem.h"

#define POSNET_SPEED 9600
#define POSNET_RETRY 3

#define ESCAPE '\033'
#define CR     "\x0d"
#define LF     "\x0a"

// Kody steruj�ce dopuszczalne przy poleceniu LBDSP

#define LBDSP_CR      CR
#define LBDSP_LF      LF
#define LBDSP_BS      "\x08"
#define LBDSP_FF      "\x0C"        /* skasowanie wy�wietlacza i kursor won HOME*/
#define LBDSP_CLRHOME LBDSP_FF

#define LBDSP_OPEN_DRAWER       1
#define LBDSP_SEND_TO_DISPLAY   2
#define LBDSP_SWITCH_TO_CLOCK   3
#define LBDSP_SWITCH_TO_COUNTER 4

#define POSNET_INIT_COMMAND {pnc[0] = ESCAPE; pnc[1] = 'P';}
#define POSNET_FINISH_COMMAND(x) {pnc[(x)] = ESCAPE; pnc[(x) + 1] = '\\';}

struct SerialHandle *sh = NULL;
char pnc[1024];

/// mazovia
UBYTE mazovia[] =
{
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
        0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
        0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27,
        0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f,
        0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
        0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f,
        0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47,
        0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f,
        0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57,
        0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f,
        0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67,
        0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f,
        0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77,
        0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f,
        0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
        0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
        0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
        0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
        0x20, 0xad, 0x9b, 0x9c, 0x5f, 0x9d, 0xb3, 0x15,
        0x5f, 0x63, 0xa6, 0xae, 0xaa, 0x2d, 0x72, 0xdf,
        0xf8, 0xf1, 0xfd, 0x33, 0x5f, 0xe6, 0x14, 0xfa,
        0x5f, 0x31, 0xa7, 0xaf, 0xac, 0xab, 0x5f, 0xa8,
        0x85, 0xa0, 0x8f, 0x41, 0x8e, 0x8f, 0x92, 0x80,
        0x8a, 0x90, 0x95, 0x90, 0x8d, 0xa1, 0x9c, 0xa5,
        0x44, 0xa5, 0x95, 0xa3, 0x98, 0x4f, 0x99, 0x78,
        0x4f, 0x97, 0xa0, 0xa1, 0x9a, 0x59, 0x50, 0xe1,
        0x85, 0xa0, 0x86, 0x61, 0x84, 0x86, 0x91, 0x87,
        0x8a, 0x82, 0x8d, 0x91, 0x8d, 0xa1, 0x92, 0xa4,
        0x64, 0xa4, 0x95, 0xa2, 0x9e, 0x6f, 0x94, 0xf6,
        0x6f, 0x97, 0xa6, 0xa7, 0x81, 0x79, 0x70, 0x98
};
//|
/// amigapl2mazovia
STRPTR amigapl2mazovia(STRPTR str)
{
STRPTR backup = str;

    while(*str)
       {
       *str = mazovia[*str];
       str++;
       }

    return(backup);
}
//|

/// posnet_send
UBYTE posnet_send(ULONG len)
{
UBYTE retry = POSNET_RETRY;

    while(retry--)
       {
       sh->ior->IOSer.io_Data = pnc;
       sh->ior->IOSer.io_Length = len;
        
       if(!DoIO((struct IORequest *)sh->ior))
           return(TRUE);
       }

    return(FALSE);
}
//|
/// close_serial
void close_serial(void)
{
    if(sh)
       {
       CloseDevice((struct IORequest *)sh->ior);

       if(sh->ior)
           DeleteIORequest(sh->ior);

       if(sh->port)
           DeleteMsgPort(sh->port);

       FreeVec(sh);
       sh = NULL;
       }
}
//|
/// init_serial
BOOL init_serial(void)
{

    if(sh = AllocVec(sizeof(struct SerialHandle), MEMF_ANY | MEMF_CLEAR))
       {
       if(sh->port = CreateMsgPort())
           {
           if(sh->ior = CreateIORequest(sh->port, sizeof(struct IOExtSer)))
               {
               /* we need XON/XOFF... */
               sh->ior->io_SerFlags &= ~SERF_XDISABLED;

               /* ...not CTS/RTS */
               sh->ior->io_SerFlags &= ~SERF_7WIRE;

               /* ...and shared mode */
               sh->ior->io_SerFlags |= SERF_SHARED;


               if(!OpenDevice(settings.serial_device, settings.serial_unit, (struct IORequest *)sh->ior, 0))
                   {
                   sh->ior->IOSer.io_Command = SDCMD_SETPARAMS;
                   sh->ior->io_Baud = POSNET_SPEED;

                   if(!DoIO((struct IORequest *)sh->ior))
                       {
                       sh->ior->IOSer.io_Command = CMD_WRITE;

                       return(TRUE);
                       }
                   else
                       {
                       CloseDevice((struct IORequest *)sh->ior);
                       }

                   }
               else
                   {
                   DisplayBeep(0);
                   D(bug("** posnet_init_serial: Can't OpenDevice\n"));
                   }                                             

               DeleteIORequest(sh->ior);
               }
           else
               {
               DisplayBeep(0);
               D(bug("** posnet_init_serial: Can't CreateIORequest\n"));
               }

           DeleteMsgPort(sh->port);
           }
       else
           {
           DisplayBeep(0);
           D(bug("** posnet_init_serial: Can't CreatePort\n"));
           }

       FreeVec(sh);
       }
    else
       {
       DisplayBeep(0);
       D(bug("** posnet_init_serial: Can't AllocVec\n"));
       }


    return(FALSE);
}
//|
/// posnet_calc_crc
void posnet_calc_checksum(ULONG len)
{
UBYTE checksum = 0xff;
ULONG i;

    for(i = 2; i < len; i++)
       checksum ^= pnc[i];

    sprintf(&pnc[len], "%02x", checksum);
}
//|

/// posnet_LBCSHREP

/*
** Drukowanie stanu kasy / zmiany
**
** input:
**  user   - nazwisko kasjera (maks. 32 znaki)
**  zmiana - tekst okr. zmian�    (maks.  8 znak�w)
*/

char posnet_LBCSHREP(char *kasjer, char *zmiana)
{

ULONG i = 5;

    POSNET_INIT_COMMAND;

    pnc[2] = 0 + 48; /* convert to ASCII */
    pnc[3] = '#';
    pnc[4] = 'k';

    i += sprintf(&pnc[i], "%s" CR, amigapl2mazovia(zmiana));
    i += sprintf(&pnc[i], "%s" CR, amigapl2mazovia(kasjer));

    posnet_calc_checksum(i);
    i += 2;

    POSNET_FINISH_COMMAND(i);

    posnet_send(i + 2);

    return(TRUE);
}
//|
/// posnet_LBCSHSTS

/*
** Drukowanie stanu kasy
*/

UBYTE posnet_LBCSHSTS(void)
{
ULONG i = 5;

    POSNET_INIT_COMMAND;

    pnc[2] = 0 + 48; /* convert to ASCII */
    pnc[3] = '#';
    pnc[4] = 't';

    posnet_calc_checksum(i);
    i += 2;

    POSNET_FINISH_COMMAND(i);

    posnet_send(i + 2);

    return(TRUE);
}
//|
/// posnet_LBDSP

/*
** Sterowanie wy�wietlaczem / szuflad� kasow�
**
**  command (LBDSP_#? - zdefiniowane wy�ej):
**   1 = otwarcie szuflady
**   2 = wys�anie napisu do wy�wietlacza
**   3 = prze��czenie wy�wietlacza na dat� i zegar
**   4 = prze��czenie wy�wietlacza w tryb kasowy
*/

UBYTE posnet_LBDSP(UBYTE command, STRPTR string)
{
ULONG i = 5;

    POSNET_INIT_COMMAND;

    pnc[2] = command + 48; /* convert to ASCII */
    pnc[3] = '$';
    pnc[4] = 'd';

    if(command == LBDSP_SEND_TO_DISPLAY && string)
       {
       strcpy(&pnc[5], string);
       i += strlen(string);
       }

    POSNET_FINISH_COMMAND(i);

    posnet_send(i + 2);

    return(TRUE);
}
//|
/// posnet_LBTRSHDR

/*
** Input:
**   amount - liczba pozycji do druku na paragonie (0..48)
**            je�li amount==0, to linie drukowane s� od razu (brak limitu 48)
**            w przeciwnym razie s� buforowane
**   line1
**   line2
**   line3  - dodatkowe 3 linie nag��wkowe (po maks. 40 znak�w ka�da DF-300,
**            Emar-Duo 24 znaki). DF-300 umieszcza te linie w nag��wku, za�
**            Emar-duo, na ko�cu paragonu.
**
*/

UBYTE posnet_LBTRSHDR(UBYTE amount, STRPTR line1, STRPTR line2, STRPTR line3)
{
ULONG i = 2;
UBYTE lines = 0;

    if(line1)
       lines++;

    if(line2)
       lines++;

    if(line3)
       lines++;

    POSNET_INIT_COMMAND;

    if(lines)
       {
       i += sprintf(&pnc[2], "%d;%d$h", amount, lines);

       if(line1)
           i += sprintf(&pnc[i], "%s" CR, amigapl2mazovia(line1));

       if(line2)
           i += sprintf(&pnc[i], "%s" CR, amigapl2mazovia(line2));

       if(line3)
           i += sprintf(&pnc[i], "%s" CR, amigapl2mazovia(line3));
       }
     else
       {
       i += sprintf(&pnc[2], "%d$h", amount);
       }

       posnet_calc_checksum(i);
       i += 2;

       POSNET_FINISH_COMMAND(i);

       posnet_send(i + 2);

       return(TRUE);
}
//|
/// posnet_LBTREXIT
UBYTE posnet_LBTREXIT(STRPTR kod_kasjera, double wp�ata, double total)
{
ULONG i = 2;

    POSNET_INIT_COMMAND;

    i += sprintf(&pnc[2], "1;0$e%3s" CR "%.2f/%.2f/", kod_kasjera, wp�ata, total);

    posnet_calc_checksum(i);
    i += 2;

    POSNET_FINISH_COMMAND(i);

    posnet_send(i + 2);

    return(TRUE);
}
//|
/// posnet_LBFSKREP
UBYTE posnet_LBFSKREP(char *start, char *end)
{
/*
** Wysy�a do drukarki ��danie wykonania rozliczenia
** okresowego. Data podana jako START i END musi
** by� w formacie %y;%m%d
*/

ULONG i = 2;

/*
char y1[3], m1[3], d1[3], y2[3], m2[3], d2[3];

    strcpy(d1, strtok(start, "-"));
    strcpy(m1, strtok(NULL,  "-"));
    strcpy(y1, strtok(NULL,  "-"));

    strcpy(d2, strtok(end,  "-"));
    strcpy(m2, strtok(NULL, "-"));
    strcpy(y2, strtok(NULL, "-"));
*/

    POSNET_INIT_COMMAND;

//    i += sprintf(&pnc[2], "%s;%s;%s;%s;%s;%s#o", y1, m1, d1, y2, m2, d2);
    i += sprintf(&pnc[2], "%s;%s#o", start, end);

    posnet_calc_checksum(i);
    i += 2;

    POSNET_FINISH_COMMAND(i);

    posnet_send(i + 2);

    return(TRUE);
}
//|
/// posnet_LBDAYREP
UBYTE posnet_LBDAYREP(void)
{
        ULONG i = 2;

        POSNET_INIT_COMMAND;

        i += sprintf(&pnc[2], "0#r");

        posnet_calc_checksum(i);
        i += 2;

        POSNET_FINISH_COMMAND(i);

        posnet_send(i + 2);

        return(TRUE);   
}
//|
/// posnet_LBTRSLN

/*
** Wysy�a linikj� do drukarki. Obcina nazw� do 40 znak�w i konwerci AmigaPL->Mazowia
*/

UBYTE posnet_LBTRSLN(UBYTE line_num, STRPTR name, double amount, UBYTE ptu, double brutto, double price)
{
ULONG i = 2;

    POSNET_INIT_COMMAND;

    i += sprintf(&pnc[2], "%d$l%-40.40s" CR "%16.2f" CR "%c/%.2f/%.2f/", line_num, amigapl2mazovia(name), amount, ptu, price, brutto);

    posnet_calc_checksum(i);
    i += 2;

    POSNET_FINISH_COMMAND(i);

    posnet_send(i + 2);

    return(TRUE);
}
//|


/// posnet_RaportZmiany

#define MSG_POS_RAPORT_ZMIANY     "\033cNa pewno chcesz wydrukowa�\naktualny raport zmiany?"
#define MSG_POS_RAPORT_ZMIANY_GAD "_Tak|*_Nie"

char posnet_RaportZmiany(void)
{

    D(bug("posnet_RaportZmiany\n"));

    if(settings.posnet_active)
       {
       if(MUI_Request(app, MainWindow, 0, TITLE, MSG_POS_RAPORT_ZMIANY_GAD, MSG_POS_RAPORT_ZMIANY))
           {
           if(init_serial())
               {
               posnet_LBCSHREP(logged_user->user.Nazwa, "");
               close_serial();

               docs.numer_paragon++;
               DateStamp(&docs.data_paragon);
               memcpy(&sys.last_operation, &docs.data_paragon, sizeof(struct DateStamp));

               ZapiszNumeracj�Dokument�w();
               }
           else
               {
               D(bug("** Can't open serial for posnet_RaportZmiany\n"));
               DisplayBeep(0);
               return(FALSE);
               }
           }
       }

    return(TRUE);
}
//|
/// posnet_StanKasy

#define MSG_POS_STAN_KASY       "\033cNa pewno chcesz wydrukowa�\naktualny raport o stanie kasy?"
#define MSG_POS_STAN_KASY_GAD   "_Tak|*_Nie"

char posnet_StanKasy(void)
{

    D(bug("posnet_StanKasy\n"));

    if(settings.posnet_active)
       {
       if(MUI_Request(app, MainWindow, 0, TITLE, MSG_POS_STAN_KASY_GAD, MSG_POS_STAN_KASY))
           {
           if(init_serial())
               {
               posnet_LBCSHSTS();
               close_serial();

               docs.numer_paragon++;
               DateStamp(&docs.data_paragon);
               memcpy(&sys.last_operation, &docs.data_paragon, sizeof(struct DateStamp));

               ZapiszNumeracj�Dokument�w();
               }
           else
               {
               D(bug("** Can't open serial for posnet_StanKasy\n"));
               DisplayBeep(0);
               return(FALSE);
               }
           }
       }

    return(TRUE);
}
//|
/// posnet_DisplayIdle

char posnet_DisplayIdle(void)
{

    D(bug("posnet_DisplayIdle\n"));

    if(settings.posnet_active)
       {
       if(init_serial())
           {
           posnet_LBDSP(LBDSP_SEND_TO_DISPLAY, LBDSP_CLRHOME);

           switch(settings.posnet_idle)
               {
               // nie r�b nic
               case 0:
                   break;

               // prze��cz na zegarek
               case 1:
                   posnet_LBDSP(LBDSP_SWITCH_TO_CLOCK, NULL);
                   break;

               // prze��cz na tryb kasowy
               case 2:
                   posnet_LBDSP(LBDSP_SWITCH_TO_COUNTER, NULL);
                   break;

               // tekst u�ytkownika
               case 3:
                   {
                   char tmp[(POSNET_LEN * 2) + 3];

                   sprintf(tmp, "%s" CR LF "%s", settings.posnet_idle_text1, settings.posnet_idle_text2);
                   posnet_LBDSP(LBDSP_SEND_TO_DISPLAY, amigapl2mazovia(tmp));
                   }
                   break;                                 
               }

           close_serial();
           }
       else
           {
           D(bug("** Can't open serial for posnet_DisplayIdle\n"));
           DisplayBeep(0);
           return(FALSE);
           }
       }

    return(TRUE);
}
//|
/// posnet_DisplayAbout

char posnet_DisplayAbout(void)
{
    if(settings.posnet_active)
       {
       if(init_serial())
           {
           posnet_LBDSP(LBDSP_SEND_TO_DISPLAY, LBDSP_CLRHOME);
           posnet_LBDSP(LBDSP_SEND_TO_DISPLAY, " Golem - Amiga " CR LF "(C)1999 W.F.M.H.");
//           posnet_LBDSP(LBDSP_SEND_TO_DISPLAY, amigapl2mazovia("Kocham Pani�!"));

           close_serial();
           }
       else
           {
           D(bug("** Can't open serial for posnet_DisplayAbout\n"));
           DisplayBeep(0);
           return(FALSE);
           }
       }

    return(TRUE);
}

//|
/// posnet_OpenDrawer

char posnet_OpenDrawer(void)
{

    if(settings.posnet_active)
       {
       if(init_serial())
           {
           posnet_LBDSP(LBDSP_OPEN_DRAWER, NULL);
           close_serial();
           }
       else
           {
           D(bug("** Can't open serial for posnet_OpenDrawer\n"));
           DisplayBeep(0);
           return(FALSE);
           }
       }

    return(TRUE);
}
//|

/// WydrukujParagon
char WydrukujParagon(struct DocumentPrint *dp)
{
double Total = 0;
char   vat_type;
struct ProductList *prod;
long   lv_total;
int    i;

char   result = TRUE;


       D(bug("\nWydrukuj paragon()\n"));


       if(!init_serial())
           {
           D(bug("** Can't open serial for posnet\n"));
           DisplayBeep(0);
           return(FALSE);
           }



       lv_total = xget(dp->lv_object, _MUIA_List_Entries);

       /* nag��wek... */

       if(settings.posnet_nadruki)
         posnet_LBTRSHDR(0, settings.posnet_nadruk_1, settings.posnet_nadruk_2, settings.posnet_nadruk_3);
       else
         posnet_LBTRSHDR(0, NULL, NULL, NULL);


       for(i=0;;i++)
           {
           double cena_sprzeda�y;
           double warto��_jednostkowa_netto;
           double warto��_jednostkowa_brutto;
           double warto��_brutto;


           DoMethod(dp->lv_object, _MUIM_List_GetEntry, i, &prod);
           if(!prod)
             break;

           switch(prod->pl_prod.Vat)
               {
               case 0: vat_type = *VatTablePosnet[settings.ptu_zw]; break;
               case 1: vat_type = *VatTablePosnet[settings.ptu_0];  break;
               case 2: vat_type = *VatTablePosnet[settings.ptu_7];  break;
               case 3: vat_type = *VatTablePosnet[settings.ptu_17]; break;
               case 4: vat_type = *VatTablePosnet[settings.ptu_22]; break;
               case 5: vat_type = *VatTablePosnet[settings.ptu_3]; break;
               }


           // warto�� towaru/us�ugi bez podatku

           cena_sprzeda�y = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
           warto��_jednostkowa_netto  = cena_sprzeda�y - CalcRabat(cena_sprzeda�y, prod->pl_prod.Rabat);

           // kwota podatku

           warto��_jednostkowa_brutto = warto��_jednostkowa_netto + CalcVat(warto��_jednostkowa_netto, prod->pl_prod.Vat);
           warto��_brutto = prod->pl_prod.Ilo�� * warto��_jednostkowa_brutto;

           // drukujemy linijk�

//           vat_type = 'A';

           {
           char vat[4];

           sprintf(vat, "%c", vat_type);
           D(bug("%ld: %s  ", i+1, prod->pl_prod.Nazwa));
           D(bug("wart_jedn_brt: %s \n", Price2String(warto��_jednostkowa_brutto)));
           D(bug("wart_brt: %s  vat: %s\n", Price2String(warto��_brutto), vat));
           }

           posnet_LBTRSLN(i+1, prod->pl_prod.Nazwa, prod->pl_prod.Ilo��, vat_type, warto��_brutto, warto��_jednostkowa_brutto);

           Total += warto��_brutto;
           }

       D(bug("Total: %s\n", Price2String(Total)));

       {
       char kasa[10];
       char terminal = 1;
       char kod_kasjera = logged_user->user.Nazwa[0];

       sprintf(kasa, "%02ld%c", terminal, kod_kasjera);

       D(bug("Got�wka wp�acona: %s\n", Price2String(dp->got�wka_wp�acona)));

       if(dp->reszta_aktywna)
           posnet_LBTREXIT(kasa, dp->got�wka_wp�acona, Total);
       else
           posnet_LBTREXIT(kasa, 0.00, Total);
       }

    close_serial();

    posnet_DisplayIdle();

    return(result);
}
//|

/// POSNET STRINGS

#define MSG_POS_TITLE       "Raport fiskalny"

#define MSG_POS_TITLE_2     "Raport okresowy"

#define MSG_POS_OD          "Od d_nia"
#define MSG_POS_DO          "_do"

#define MSG_POS_OK          "_Ok"
#define MSG_POS_CANCEL      "Ponie_chaj"

#define MSG_ERR_POS_DATE    "\033cData pocz�tkowa nie mo�e by�\nwcze�niejsza ni� data koncowa!"

//|
/// PosnetRaportSetup

static Object
       *PosnetRaportWindow,        /* POSNET RAPORT */
       *ST_Pos_Od,
       *ST_Pos_Do,
       *BT_Pos_Ok,
       *BT_Pos_Cancel;


char PosnetRaportSetup( void )
{
struct DateStamp *ds = wfmh_GetCurrentTime();

///   PosnetRaportWindow

         PosnetRaportWindow = WindowObject,
                        MUIA_Window_Title      , MSG_POS_TITLE,
                        MUIA_Window_ID         , ID_WIN_POSNET_RAPORT,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, HGroup,
                                  GroupFrameT(MSG_POS_TITLE_2),
                                  Child, MakeLabel2(MSG_POS_OD),
                                  Child, ST_Pos_Od = MakeString(DATE_LEN, MSG_POS_OD),
                                  Child, MakeLabel2(MSG_POS_DO),
                                  Child, ST_Pos_Do = MakeString(DATE_LEN, MSG_POS_DO),
                                  End,

                           Child, HGroup,
                                  Child, BT_Pos_Ok     = TextButton(MSG_POS_OK),
                                  Child, BT_Pos_Cancel = TextButton(MSG_POS_CANCEL),
                                  End,

                           End,
                      End;

//|


    if( !PosnetRaportWindow )
        return( FALSE );



    setstring(ST_Pos_Od, wfmh_Date2Str(ds));
    setstring(ST_Pos_Do, wfmh_Date2Str(ds));

    set(PosnetRaportWindow, MUIA_Window_ActiveObject, ST_Pos_Od);


    /* POSNET RAPORT */

    DoMethod(PosnetRaportWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

    DoMethod(BT_Pos_Ok       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
    DoMethod(BT_Pos_Cancel   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


    _attachwin( PosnetRaportWindow );
    return( TRUE );

}
//|
/// PosnetRaportFinish
char PosnetRaportFinish(struct PosnetRaportRequest *pr)
{
/*
** sprawdza poprawno�� wprowadzonych dat
** dot. drukowania. Zwraca FALSE, je�li co�
** jest nie tak, TRUE, je�li wszystkie
** elementy s� poprawne.
*/

struct DateStamp ds_od = {0};
struct DateStamp ds_do = {0};


    if(!wfmh_Str2Date(getstr(ST_Pos_Od)))
       {
       set(PosnetRaportWindow, MUIA_Window_ActiveObject, ST_Pos_Od);
       DisplayBeep(0);
       return(FALSE);
       }

    if(!wfmh_Str2Date(getstr(ST_Pos_Do)))
       {
       set(PosnetRaportWindow, MUIA_Window_ActiveObject, ST_Pos_Do);
       DisplayBeep(0);
       return(FALSE);
       }

    memcpy(&ds_od, wfmh_Str2Date(getstr(ST_Pos_Od)), sizeof(struct DateStamp));
    memcpy(&ds_do, wfmh_Str2Date(getstr(ST_Pos_Do)), sizeof(struct DateStamp));

    if(CompareDates(&ds_od, &ds_do) < 0)
       {
       DisplayBeep(0);
       MUI_Request(app, PosnetRaportWindow, 0, TITLE, MSG_OK, MSG_ERR_POS_DATE);
       return(FALSE);
       }

    strcpy(pr->data_od, wfmh_Date2StrFmt( "%y;%m;%d", &ds_od ));
    strcpy(pr->data_do, wfmh_Date2StrFmt( "%y;%m;%d", &ds_do ));

    return(TRUE);
}
//|
/// PosnetRaport

char PosnetRaport(struct PosnetRaportRequest *pr)
{
/*
** Requester dot. drukowania dokument�w.
** Zwraca TRUE, albo FALSE je�li u�ytkownik
** nie chce nic drukowa� b�d� -1 je�li chce
** wr�ci� do edycji dokumentu
*/

char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

    set(app, MUIA_Application_Sleep, TRUE);

    if( ! PosnetRaportSetup() )
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       _sleep(FALSE);
       return(NULL);
       }

    if(WinOpen(PosnetRaportWindow))
       {
       while(running)
          {
          switch (DoMethod(app, MUIM_Application_Input, &signal))
            {
            case ID_OK:
               if(PosnetRaportFinish(pr))
                   {
                   result = TRUE;
                   running = FALSE;
                   }
               break;


            // nie drukuj
            case ID_CANCEL:
               running = FALSE;
               break;

            }
          if(running && signal) Wait(signal);
          }
       }
    else
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       }

    _detachwin( PosnetRaportWindow );
    set(app, MUIA_Application_Sleep, FALSE);

    return(result);
}

//|

/// RaportFiskalny

#define MSG_POS_DAILY_QUEST     "\033cNa pewno chcesz wykona�\ndobowy raport fiskalny?"
#define MSG_POS_DAILY_QUEST_GAD "_Tak|*_Nie"

char RaportFiskalny(char TypRaportu)
{

    set(app, MUIA_Application_Sleep, TRUE);

    switch(TypRaportu)
       {
       case POSNET_RAPORT_DOBOWY:
            if(MUI_Request(app, MainWindow, 0, TITLE, MSG_POS_DAILY_QUEST_GAD, MSG_POS_DAILY_QUEST))
               {
               if(init_serial())
                  {
                  posnet_LBDAYREP();
                  close_serial();

                  docs.numer_paragon++;
                  DateStamp(&docs.data_paragon);
                  memcpy(&sys.last_operation, &docs.data_paragon, sizeof(struct DateStamp));

                  ZapiszNumeracj�Dokument�w();
                  }
               else
                  {
                  D(bug("** Can't open serial for posnet_Raport\n"));
                  DisplayBeep(0);
                  return(FALSE);
                  }
               }
            break;

       case POSNET_RAPORT_OKRESOWY:
            {
            struct PosnetRaportRequest pr = {0};

            if(PosnetRaport(&pr))
               {
               if(init_serial())
                  {
                  posnet_LBFSKREP(pr.data_od, pr.data_do);
                  close_serial();

                  docs.numer_paragon++;
                  DateStamp(&docs.data_paragon);
                  memcpy(&sys.last_operation, &docs.data_paragon, sizeof(struct DateStamp));

                  ZapiszNumeracj�Dokument�w();
                  }
               else
                  {
                  D(bug("** Can't open serial for posnet_Raport\n"));
                  DisplayBeep(0);
                  return(FALSE);
                  }
               }
            }
            break;                             
       }

    set(app, MUIA_Application_Sleep, FALSE);

}

//|

/*
/// WydrukujParagon
char WydrukujParagon(struct Faktura *faktura, struct List *prod_list)
{
double price, brutto, BRUTTO = 0;
ULONG amount, cnt;
struct Fak_prod *f_prod;
char vat_type;
char kasa[10];
char fak_number[40];

    if(amount = check_presence_other(prod_list))
       {
/*
                if(faktura->number)
                {
                        sprintf(fak_number, "NR %5d/655", faktura->number);
                        posnet_LBTRSHDR(amount, fak_number, NULL, NULL);
                }
                else
*/

       posnet_LBTRSHDR(amount, NULL, NULL, NULL);

       for(cnt = 0, f_prod = (struct Fak_prod*)prod_list->lh_Head; cnt < amount; )
           {
           if(f_prod->type != BENZYNY)
               {
               cnt++;

               price = f_prod->price_final;
               brutto = f_prod->price_final * f_prod->amount;

               BRUTTO += brutto;

               switch((ULONG)f_prod->VAT)
                   {
                   case 22:
                       vat_type = 'A';
                       break;

                   case 7:
                       vat_type = 'B';
                       break;

                   case 0:
                       vat_type = 'Z';
                       break;
                   }

               posnet_LBTRSLN(cnt, amigapl2mazovia(f_prod->name), f_prod->amount, vat_type, brutto, price);
               }
           f_prod = (struct Fak_prod*)f_prod->f_node.mln_Succ;
           }

       sprintf(kasa, "01%c", ActUser->name[0]);

       posnet_LBTREXIT(kasa, (double)0, BRUTTO);

       p_amount = update_paragon(BRUTTO);
       }


       close_serial();


       return(TRUE);
}
//|

/// print_posnet
UBYTE print_posnet(struct Faktura *faktura, struct List *prod_list)
{
double price, brutto, BRUTTO = 0;
ULONG amount, cnt;
struct Fak_prod *f_prod;
char vat_type;
char kasa[10];
char fak_number[40];

    if(amount = check_presence_other(prod_list))
       {
/*
                if(faktura->number)
                {
                        sprintf(fak_number, "NR %5d/655", faktura->number);
                        posnet_LBTRSHDR(amount, fak_number, NULL, NULL);
                }
                else
*/

       posnet_LBTRSHDR(amount, NULL, NULL, NULL);

       for(cnt = 0, f_prod = (struct Fak_prod*)prod_list->lh_Head; cnt < amount; )
           {
           if(f_prod->type != BENZYNY)
               {
               cnt++;

               price = f_prod->price_final;
               brutto = f_prod->price_final * f_prod->amount;

               BRUTTO += brutto;

               switch((ULONG)f_prod->VAT)
                   {
                   case 22:
                       vat_type = 'A';
                       break;

                   case 7:
                       vat_type = 'B';
                       break;

                   case 0:
                       vat_type = 'Z';
                       break;
                   }

               posnet_LBTRSLN(cnt, amigapl2mazovia(f_prod->name), f_prod->amount, vat_type, brutto, price);
               }
           f_prod = (struct Fak_prod*)f_prod->f_node.mln_Succ;
           }

       sprintf(kasa, "01%c", ActUser->name[0]);

       posnet_LBTREXIT(kasa, (double)0, BRUTTO);

       p_amount = update_paragon(BRUTTO);
       }

       return(TRUE);
}
//|
/// rozlicz_posnet
void rozlicz_posnet(void)
{
BOOL running = TRUE;
ULONG   signal;
struct DateStamp d_stamp, *ds;
struct DateTime d_time;
char date_buf[10], start[10], end[10];
char *d_ptr;

    ds = DateStamp(&d_stamp);
    memset(&d_time, 0, sizeof(struct DateTime));
    memcpy(&d_time.dat_Stamp, ds, sizeof(struct DateStamp));

    d_time.dat_StrDate = date_buf;
    d_time.dat_Format = FORMAT_CDN;
    DateToStr(&d_time);
    setstring(App->STR_POSNET_START, date_buf);
    setstring(App->STR_POSNET_END, date_buf);

    set(App->main_window, MUIA_Window_Sleep, TRUE);
    set(App->posnet_window, MUIA_Window_ActiveObject, App->STR_DAY_DAY);
    set(App->posnet_window, MUIA_Window_ActiveObject, App->STR_POSNET_START);

    set(App->posnet_window, MUIA_Window_Open, TRUE);

                while (running)
                {
                        switch (DoMethod(App->App,MUIM_Application_Input,&signal))
                                {
                                        case ID_OK:
                                        case ID_QUIT:
                                                running = FALSE;
                                                break;

                                        case ID_POSNET_DAILY:
                                                posnet_LBDAYREP();
                                                running = FALSE;
                                                break;

                                        case ID_POSNET_PRINT:
                                                        get(App->STR_POSNET_START, MUIA_String_Contents, &d_ptr);
                                                        if(!(ds = Str2Date(d_ptr)))
                                                        {
                                                                set(App->posnet_window, MUIA_Window_ActiveObject, App->STR_POSNET_START);
                                                                break;
                                                        }
                                                        else
                                                                strcpy(start, d_ptr);

                                                        get(App->STR_POSNET_END, MUIA_String_Contents, &d_ptr);
                                                        if(!(ds = Str2Date(d_ptr)))
                                                        {
                                                                set(App->posnet_window, MUIA_Window_ActiveObject, App->STR_POSNET_END);
                                                                break;
                                                        }
                                                        else
                                                                strcpy(end, d_ptr);

                                                        posnet_LBFSKREP(start, end);

                                                        running = FALSE;
                                                break;
                                }

                if (running && signal) Wait(signal);
                }

        set(App->posnet_window, MUIA_Window_Open, FALSE);
        set(App->main_window, MUIA_Window_Sleep, FALSE);
}
//|
*/
