
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 


/// Includes

#define GOLEM_GUI

#define NO_GUI_PROTOS_H
#include "golem.h"
#undef  NO_GUI_PROTOS_H
#include "golem_GUI.h"

#define SAVEDS __saveds
#define ASM __asm
#define REG(x) register __ ## x

#define USE_GOLEM_COLORS
#define USE_GOLEM_BODY
#include "images/golem_img.h"


/*
/// BETTERSTRING
#ifdef USE_BETTERSTRING
#undef StringObject
#define StringObject        MUI_NewObject("BetterString.mcc"
#endif
//|
*/

//|

char *UserLevel_Table[] = {"Administrator",
//                           "Sprzedawca + Magazynier",
//                           "Sprzedawca",
//                           "Magazynier",
                                                   NULL};


char *RodzajeDokument�wSpoolTable[] = {"VAT",
                                                                           "Upr",
                                                                           "FV-KO",
                                                                           "Upr-KO",
                                                                           "FV-Par",
                                                                           "Upr-Par",
                                                                           "Par",
                                                                           "Zam",
                                                                           "FV-Pro",
                                                                           "Odrecz",
                                                                                NULL};



char *GR_ZakE_Titles[]   = {MSG_ZAKE_PAGE1,
                                                        MSG_ZAKE_PAGE2,
                                                        MSG_ZAKE_PAGE3,
                                                        NULL};


char *Miesi�ceTable2[] = {"\033bAktualny miesi�c",
                                                 "Stycze�",
                                                 "Luty",
                                                 "Marzec",
                                                 "Kwiecie�",
                                                 "Maj",
                                                 "Czerwiec",
                                                 "Lipiec",
                                                 "Sierpie�",
                                                 "Wrzesie�",
                                                 "Pa�dziernik",
                                                 "Listopad",
                                                 "Grudzie�",
                                                 NULL};


/// SetupAboutScroll
char golem_about_text_left[] = "Programowanie:\n"
                                                   "\n"
                                                   "Wersja:\n"
                                                   "Data kompilacji:\n"
                                                   "\n"
                                                   "Testowanie:\n"
                                                   "\n\n\n\n\n\n\n\n\n\n\n"
                                                   "Producent:\n"
                                                   "\n"
                                                   "Telefon:\n"
                                                   "Fax:\n"
                                                   "E-Mail:\n"
                                                   "WWW:\n"
                                                   "\n"

                                                   "\n"
                                                   "Programowanie:\n"
                                                   "\n"
                                                   "Wersja:\n"
                                                   "Data kompilacji:"
                                                   "";

char golem_about_text_right[]   = "Marcin Or�owski\n"
                                                          "\n"
                                                          VERSIONREVISION " Open Source\n"
                                                          DATE "\n"
                                                          "\n"
                                                          "Patryk �ogiewa\n"
                                                          "Krzysztof Krzewiniak\n"
                                                          "Micha� Romanowski\n"
                                                          "Piotr Larkowski\n"
                                                          "Piotr Szudrowicz\n"
                                                          "Andrzej Szudrowicz\n"
                                                          "Marian Kwiatkowski\n"
                                                          "Piotr Ksi��ek\n"
                                                          "Filip D�b-Mirowski\n"
                                                          "Jacek Szelest\n"
                                                          "Marek Rybka\n"
                                                          "\n"
                                                          "W.F.M.H.\n"
                                                          "\n"
                                                          "(091) 4324211\n"
                                                          "(091) 4324212\n"
                                                          EMAIL "\n"
                                                          WWW "\n"
                                                          "\n"

                                                          "\n"
                                                          "Marcin Or�owski\n"
                                                          "\n"
                                                          VERSIONREVISION " Open Source\n"
                                                          DATE

                                                          "";

/// CreateMainWindow

/// STRINGS

#define MSG_TOOLBAR_MAIN                  "Menu g��wne"

#define MSG_TOOLBAR_SPRZEDAZ              "\033lF1 - _Sprzeda�"
#define  SH_TOOLBAR_SPRZEDAZ                "Przej�cie do menu dot. sprzeda�y"
#define   MSG_TOOLBAR_VAT                   "\033lF1 - _Faktura VAT"
#define    SH_TOOLBAR_VAT                     "Sprzeda� na fakture VAT"
#define   MSG_TOOLBAR_VAT_KOR               "\033lF2 - Faktura VAT - _korekta"
#define    SH_TOOLBAR_VAT_KOR                 "Wystawianie faktury koryguj�cej VAT"
#define   MSG_TOOLBAR_RACHUNEK              "\033lF3 - _Rachunek uproszczony"
#define    SH_TOOLBAR_RACHUNEK                "Sprzeda� na rachunek uproszczony"
#define   MSG_TOOLBAR_RACHUNEK_KOR          "\033lF4 - Rachunek upr. - k_orekta"
#define    SH_TOOLBAR_RACHUNEK_KOR            "Wystawianie korekty do rach. uproszczonego"
#define   MSG_TOOLBAR_VAT_PAR               "\033lF5 - Faktura VAT do _paragonu"
#define    SH_TOOLBAR_VAT_PAR                 "Wystawianie faktury VAT do paragonu fiskalnego"
#define   MSG_TOOLBAR_RACHUNEK_PAR          "\033lF6 - Rach. upr. do p_aragonu"
#define    SH_TOOLBAR_RACHUNEK_PAR            "Wystawianie rachunku uproszczonego\ndo paragonu fiskalnego"
#define   MSG_TOOLBAR_VAT_PROFORMA          "\033lF7 - Faktura VAT ProForma"
#define    SH_TOOLBAR_VAT_PROFORMA            "Wystawianie faktury PRO FORMA"
#define   MSG_TOOLBAR_PARAGON_POSNET        "\033lF8 - _Tylko paragon fiskalny"
#define    SH_TOOLBAR_PARAGON_POSNET          "Sprzeda� tylko na paragon fiskalny"
#define   MSG_TOOLBAR_PARAGON               "\033lF8 - _Tylko paragon"
#define    SH_TOOLBAR_PARAGON                 "Sprzeda� tylko na paragon"
#define   MSG_TOOLBAR_NIEUDOK               "\033lF9 - Sprzeda� o_dr�czna"
#define    SH_TOOLBAR_NIEUDOK                 "Dokument dot. nieudokumentowanej sprzeda�y odr�cznej"
#define   MSG_TOOLBAR_ORDER                 "\033lF10 - _Zam�wienie"
#define    SH_TOOLBAR_ORDER                   "Wystawienie dokumentu zam�wienia"
#define MSG_TOOLBAR_ZAKUPY                "\033lF2 - _Zakupy"
#define  SH_TOOLBAR_ZAKUPY                  "Przej�cie do menu zakup�w"
#define   MSG_TOOLBAR_ZAKUP_MAGAZYN         "\033lF1 - Zakup do _magazynu"
#define    SH_TOOLBAR_ZAKUP_MAGAZYN           ""
#define   MSG_TOOLBAR_ZAKUP_KOSZTY          "\033lF2 - Zakup - _koszty"
#define    SH_TOOLBAR_ZAKUP_KOSZTY            ""
#define MSG_TOOLBAR_REDAGOWANIE           "\033lF3 - _R��ne"
#define  SH_TOOLBAR_REDAGOWANIE             "Przej�cie do menu funkcji r��nych"
#define   MSG_TOOLBAR_RED_BIE��CE           "\033lF1 - Dokumenty _wystawione"
#define    SH_TOOLBAR_RED_BIE��CE             "Przeszukiwanie dokument�w wystawionych"
#define   MSG_TOOLBAR_RED_OD�O�ONE          "\033lF2 - Dokumenty _od�o�one"
#define    SH_TOOLBAR_RED_OD�O�ONE            "Przeszukiwanie dokument�w od�o�onych"
#define   MSG_TOOLBAR_RED_PRZELEW           "\033lF6 - _Przelewy bankowe"
#define    SH_TOOLBAR_RED_PRZELEW             "Wystawianie polecenia przelewu"
#define MSG_TOOLBAR_BAZYDANYCH            "\033lF4 - _Bazy danych"
#define  SH_TOOLBAR_BAZYDANYCH              "Przej�cie do menu zarz�dzania bazami danych"
#define MSG_TOOLBAR_MAGAZYN                 "\033lF1 - M_agazyn"
#define  SH_TOOLBAR_MAGAZYN                   "Edycja magazynu"
#define MSG_TOOLBAR_KONTRAHENCI             "\033lF2 - _Kontrahenci"
#define  SH_TOOLBAR_KONTRAHENCI               "Edycja bazy kontrahent�w"
#define MSG_TOOLBAR_USERBASE                "\033lF3 - _Dane u�ytkownik�w"
#define  SH_TOOLBAR_USERBASE                  "Edycja u�ytkownik�w systemu Golem"
#define MSG_TOOLBAR_SETTINGS_PAY            "\033lF4 - Rodzaje _p�atno�ci"
#define  SH_TOOLBAR_SETTINGS_PAY              "Edycja bazy rodzaj�w p�atno�ci"
#define MSG_TOOLBAR_SETTINGS_UNIT           "\033lF5 - _Jednostki miary"
#define  SH_TOOLBAR_SETTINGS_UNIT             "Edycja bazy jednostek miar"
#define MSG_TOOLBAR_ROZLICZENIA           "\033lF5 - R_ozliczenia"
#define  SH_TOOLBAR_ROZLICZENIA             "Przej�cie do menu funkcji rozliczeniowych"
#define MSG_TOOLBAR_ROZ_ZAMKNIJ             "\033lF1 - _Zamknij okres rachunkowy"
#define  SH_TOOLBAR_ROZ_ZAMKNIJ               "Zamyka aktualnie zako�czony okres rozliczeniowy"
#define   MSG_TOOLBAR_ROZ_NORM              "\033lF5 - _Rozliczenia bie��ce"
#define    SH_TOOLBAR_ROZ_NORM                ""
#define   MSG_TOOLBAR_ROZ_FISK_DOBOWY       "\033lF6 - Raport _dobowy"
#define    SH_TOOLBAR_ROZ_FISK_DOBOWY         "Wydruk raportu dobowego\ndla drukarki fiskalnej"
#define   MSG_TOOLBAR_ROZ_FISK_OKRESOWY     "\033lF7 - Raport _okresowy"
#define    SH_TOOLBAR_ROZ_FISK_OKRESOWY       "Wydruk raportu okresowego\ndla drukarki fiskalnej"
#define   MSG_TOOLBAR_ROZ_STAN_KASY         "\033lF8 - _Stan kasy"
#define    SH_TOOLBAR_ROZ_STAN_KASY           "Wydruk stanu kasu dla drukarki fiskalnej"
#define   MSG_TOOLBAR_ROZ_RAPORT_ZMIANY     "\033lF9 - Raport _zmiany"
#define    SH_TOOLBAR_ROZ_RAPORT_ZMIANY       ""
#define MSG_TOOLBAR_SYSTEM                "\033lF6 - S_ystem"
#define  SH_TOOLBAR_SYSTEM                  "Przej�cie do konfiguracji systemu Golem"
#define   MSG_TOOLBAR_SETTINGS              "\033lF1 - _Ustawienia"
#define    SH_TOOLBAR_SETTINGS                "Ustawienia programu"
#define   MSG_TOOLBAR_NUMERACJA             "\033lF2 - _Numeracja"
#define    SH_TOOLBAR_NUMERACJA               "Ustawienia dot. numeracji dokument�w"
#define   MSG_TOOLBAR_DRUKARKI              "\033lF3 - _Drukarki"
#define    SH_TOOLBAR_DRUKARKI                "Zarz�dzanie wydrukiem oraz drukarkami"
#define   MSG_TOOLBAR_SQL                   "\033lF3 - _SQL"
#define    SH_TOOLBAR_SQL                     ""
#define   MSG_TOOLBAR_TIP                   "\033lF6 - Czy _wiesz, �e..."
#define    SH_TOOLBAR_TIP                     "Sztuczki i kruczki dot. Golema"
#define   MSG_TOOLBAR_INSPEKTOR             "\033lF7 - Golem _Inspektor"
#define    SH_TOOLBAR_INSPEKTOR               "Sprawdzenie aktualno�ci komponent�w pakietu Golem"
#define   MSG_TOOLBAR_MUI                   "\033lF10 - Ustawienia _MUI"
#define    SH_TOOLBAR_MUI                     "Przej�cie do okna preferencji MUI dla Golema"
#define MSG_TOOLBAR_QUIT                  "\033lESC - S_ko�cz"
#define  SH_TOOLBAR_QUIT                    "Zako�czenie pracy z programem"

#define MSG_TOOLBAR_LOGIN                 "\033lF8 - _Has�o"
#define  SH_TOOLBAR_LOGIN                   "Pozwala na zalogowanie si� u�ytkownika\nw programie Golem"
#define MSG_TOOLBAR_LOGOUT                "\033lF8 - Z_ako�cz sesj�"
#define  SH_TOOLBAR_LOGOUT                  "Wylogowuje aktualnego u�ytkownika"

#define MSG_TOOLBAR_SPRZEDAZ_TITLE      "Sprzeda�"
#define MSG_TOOLBAR_ZAKUPY_TITLE        "Zakupy"
#define MSG_TOOLBAR_RED_TITLE           "Redagowanie"
#define MSG_TOOLBAR_RED_TITLE_2         "Przelewy"
#define MSG_TOOLBAR_ROZLICZENIA_TITLE   "Okres rachunkowy"
#define MSG_TOOLBAR_ROZLICZENIA_TITLE_1 "Rozliczenia"
#define MSG_TOOLBAR_ROZLICZENIA_TITLE_2 "Drukarka fiskalna"
#define MSG_TOOLBAR_BAZY_TITLE          "Bazy danych"
#define MSG_TOOLBAR_SYSTEM_TITLE        "System"
#define MSG_TOOLBAR_MUI_TITLE           "MUI"
#define MSG_TOOLBAR_MISC_TITLE          "R��ne"

#define MSG_TOOLBAR_MAINMENU    "\033lESC - G��wne _menu"
#define  SH_TOOLBAR_MAINMENU    "Powr�t do g��wnego menu programu"


#define MSG_TOOLBAR_MISC     "R��ne"
#define MSG_TOOLBAR_ABOUT    "\033lInformacje o _programie"
#define  SH_TOOLBAR_ABOUT    "Wy�wietla informacje o programie"

//|

Object *CreateMainWindow(void)
{
Object *MainWindow = NULL;

///       Main Window

                MainWindow = WindowObject,
                                                MUIA_Window_Title      , TITLE,
                                                MUIA_Window_ID         , ID_WIN_MAIN,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
//                        MUIA_Window_CloseGadget, FALSE,
                                                WindowContents, GR_ToolBar_Menus = GroupObject,

                                                                                MUIA_Group_SameWidth, TRUE,
                                                                                MUIA_Group_Spacing  , 2,
                                                                                GroupFrame,

                                                                                MUIA_Group_PageMode, TRUE,
///                                         Main
                                                                                Child, VGroup,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          GroupFrameT(MSG_TOOLBAR_MAIN),

                                                                                                          Child, BT_ToolBar_Sprzeda�   = _TextButton(TOOLBAR_SPRZEDAZ),
                                                                                                          Child, BT_ToolBar_Zakupy     = _TextButton(TOOLBAR_ZAKUPY),

                                                                                                          Child, BT_ToolBar_Redagowanie = _TextButton(TOOLBAR_REDAGOWANIE),
                                                                                                          Child, BT_ToolBar_BazyDanych  = _TextButton(TOOLBAR_BAZYDANYCH),
                                                                                                          Child, BT_ToolBar_Rozliczenia = _TextButton(TOOLBAR_ROZLICZENIA),
                                                                                                          Child, BT_ToolBar_System      = _TextButton(TOOLBAR_SYSTEM),
                                                                                                          End,

                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          GroupFrameT(MSG_TOOLBAR_MISC),

                                                                                                          Child, BT_ToolBar_About    = _TextButton(TOOLBAR_ABOUT),
                                                                                                          Child, GR_ToolBar_LoginLogout = GroupObject,
                                                                                                                         MUIA_Group_PageMode, TRUE,
                                                                                                                         Child, BT_ToolBar_Login  = _TextButton(TOOLBAR_LOGIN),
                                                                                                                         Child, BT_ToolBar_Logout = _TextButton(TOOLBAR_LOGOUT),
                                                                                                                         End,
                                                                                                          Child, BT_ToolBar_Quit = _TextButton(TOOLBAR_QUIT),

                                                                                                          End,
                                                                                           End,
//|
///                                         Sprzeda�
                                                                                Child, VGroup,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          GroupFrameT(MSG_TOOLBAR_SPRZEDAZ_TITLE),
                                                                                                          Child, BT_ToolBar_Vat     = _TextButton(TOOLBAR_VAT),
                                                                                                          Child, BT_ToolBar_Vat_Kor = _TextButton(TOOLBAR_VAT_KOR),
/*
                                                                                                          Child, BT_ToolBar_Upr     = _TextButton(TOOLBAR_RACHUNEK),
                                                                                                          Child, BT_ToolBar_Upr_Kor = _TextButton(TOOLBAR_RACHUNEK_KOR),
*/

                                                                                                          Child, BT_ToolBar_Vat_Par = _TextButton(TOOLBAR_VAT_PAR),
//                                                      Child, BT_ToolBar_Upr_Par = _TextButton(TOOLBAR_RACHUNEK_PAR),

                                                                                                          Child, BT_ToolBar_Vat_ProForma = _TextButton(TOOLBAR_VAT_PROFORMA),

                                                                                                          Child, BT_ToolBar_Paragon = _TextButton(TOOLBAR_PARAGON),
                                                                                                          Child, BT_ToolBar_Nieudokumentowana = _TextButton(TOOLBAR_NIEUDOK),


                                                                                                          Child, BT_ToolBar_Order   = _TextButton(TOOLBAR_ORDER),

                                                                                                          End,
/*
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,
                                                                                                          GroupBack,
                                                                                                          GroupFrame,
                                                                                                          Child, BT_ToolBar_Order   = _TextButton(TOOLBAR_ORDER),

                                                                                                          End,
*/

                                                                                           Child, HVSpace,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,
                                                                                                          MUIA_Group_Spacing  , 2,
                                                                                                          GroupFrameT(MSG_TOOLBAR_MISC),
                                                                                                          Child, BT_ToolBar_Sprzeda�_Back = _TextButton(TOOLBAR_MAINMENU),
                                                                                                          End,
                                                                                           End,
//|
///                                         Zakupy
                                                                                Child, VGroup,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          GroupFrameT(MSG_TOOLBAR_ZAKUPY_TITLE),
//                                                      Child, BT_ToolBar_Zakupy_Magazyn = _TextButton(TOOLBAR_ZAKUP_MAGAZYN),
                                                                                                          Child, BT_ToolBar_Zakupy_Koszty  = _TextButton(TOOLBAR_ZAKUP_KOSZTY),
                                                                                                          End,

                                                                                           Child, HVSpace,

                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,
                                                                                                          GroupFrameT(MSG_TOOLBAR_MISC),

                                                                                                          Child, BT_ToolBar_Zakupy_Back = _TextButton(TOOLBAR_MAINMENU),
                                                                                                          End,
                                                                                           End,
//|
///                                         R��ne
                                                                                Child, VGroup,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          GroupFrameT( MSG_TOOLBAR_RED_TITLE ),

                                                                                                          Child, BT_ToolBar_Red_Bie��ce  = _TextButton( TOOLBAR_RED_BIE��CE  ),
                                                                                                          Child, BT_ToolBar_Red_Od�o�one = _TextButton( TOOLBAR_RED_OD�O�ONE ),
                                                                                                          End,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          GroupFrameT( MSG_TOOLBAR_RED_TITLE_2 ),

                                                                                                          Child, BT_ToolBar_Red_Przelew = _TextButton( TOOLBAR_RED_PRZELEW ),
                                                                                                          End,

                                                                                           Child, HVSpace,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,
                                                                                                          GroupFrameT(MSG_TOOLBAR_MISC),

                                                                                                          Child, BT_ToolBar_Red_Back = _TextButton(TOOLBAR_MAINMENU),
                                                                                                          End,
                                                                                           End,
//|
///                                         Bazy danych
                                                                                Child, VGroup,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          GroupFrameT(MSG_TOOLBAR_BAZY_TITLE),
                                                                                                          Child, BT_ToolBar_Magazyn     = _TextButton(TOOLBAR_MAGAZYN),
                                                                                                          Child, BT_ToolBar_Kontrahenci = _TextButton(TOOLBAR_KONTRAHENCI),
                                                                                                          Child, BT_ToolBar_UserBase    = _TextButton(TOOLBAR_USERBASE),

                                                                                                          Child, BT_ToolBar_Settings_Pay  = _TextButton(TOOLBAR_SETTINGS_PAY),�
                                                                                                          Child, BT_ToolBar_Settings_Unit = _TextButton(TOOLBAR_SETTINGS_UNIT),
                                                                                                          End,

                                                                                           Child, HVSpace,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,
                                                                                                          GroupFrameT(MSG_TOOLBAR_MISC),
                                                                                                          Child, BT_ToolBar_Bazy_Back = _TextButton(TOOLBAR_MAINMENU),
                                                                                                          End,
                                                                                           End,
//|
///                                         Rozliczenia
                                                                                Child, VGroup,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,
                                                                                                          GroupFrameT(MSG_TOOLBAR_ROZLICZENIA_TITLE),
                                                                                                          GroupFrame,

                                                                                                          Child, BT_ToolBar_Rozliczenia_Zamknij = _TextButton(TOOLBAR_ROZ_ZAMKNIJ),
//                                                      Child, BT_ToolBar_Rozliczenia_ZamknijMiesiac = _TextButton(TOOLBAR_ROZ_ZAMKNIJ),
//                                                      Child, BT_ToolBar_Rozliczenia_ZamknijRok     = _TextButton(TOOLBAR_ROZ_ZAMKNIJ_ROK),
                                                                                                          End,

                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,
                                                                                                          GroupFrameT(MSG_TOOLBAR_ROZLICZENIA_TITLE_1),

                                                                                                          Child, BT_ToolBar_Rozliczenia_Norm  = _TextButton(TOOLBAR_ROZ_NORM),
                                                                                                          End,

                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,
                                                                                                          GroupFrameT(MSG_TOOLBAR_ROZLICZENIA_TITLE_2),

                                                                                                          Child, BT_ToolBar_Rozliczenia_Fisk_Dobowy       = _TextButton(TOOLBAR_ROZ_FISK_DOBOWY),
                                                                                                          Child, BT_ToolBar_Rozliczenia_Fisk_Okresowy     = _TextButton(TOOLBAR_ROZ_FISK_OKRESOWY),
                                                                                                          Child, BT_ToolBar_Rozliczenia_Fisk_StanKasy     = _TextButton(TOOLBAR_ROZ_STAN_KASY),
                                                                                                          Child, BT_ToolBar_Rozliczenia_Fisk_RaportZmiany = _TextButton(TOOLBAR_ROZ_RAPORT_ZMIANY),

                                                                                                          End,


                                                                                           Child, HVSpace,
                                                                                           Child, VGroup,
                                                                                                          GroupFrameT(MSG_TOOLBAR_MISC),
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          Child, BT_ToolBar_Rozliczenia_Back = _TextButton(TOOLBAR_MAINMENU),
                                                                                                          End,
                                                                                           End,
//|
///                                         System
                                                                                Child, VGroup,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          GroupFrameT(MSG_TOOLBAR_SYSTEM_TITLE),

                                                                                                          Child, BT_ToolBar_Settings  = _TextButton(TOOLBAR_SETTINGS),
                                                                                                          Child, BT_ToolBar_Numeracja = _TextButton(TOOLBAR_NUMERACJA),
                                                                                                          Child, BT_ToolBar_Drukarki  = _TextButton(TOOLBAR_DRUKARKI),
                                                                                                          End,

                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          GroupFrameT(MSG_TOOLBAR_MISC_TITLE),

                                                                                                          Child, BT_ToolBar_Tip = _TextButton(TOOLBAR_TIP),
                                                                                                          Child, BT_ToolBar_Inspektor = _TextButton(TOOLBAR_INSPEKTOR),
                                                                                                          End,


                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          GroupFrameT(MSG_TOOLBAR_MUI_TITLE),

                                                                                                          Child, BT_ToolBar_MUI       = _TextButton(TOOLBAR_MUI),
                                                                                                          End,

/*
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,

                                                                                                          GroupFrame,

                                                                                                          End,
*/

                                                                                           Child, HVSpace,
                                                                                           Child, VGroup,
                                                                                                          MUIA_Group_SameWidth, TRUE,
                                                                                                          GroupFrameT(MSG_TOOLBAR_MISC),

                                                                                                          Child, BT_ToolBar_System_Back = _TextButton(TOOLBAR_MAINMENU),
                                                                                                          End,
                                                                                           End,
//|
                                                                                End,
                                                End;

//|


        if( !MainWindow )
          return( 0 );


        _disable( BT_ToolBar_Vat_Kor );


           /* MAIN WINDOW    */

           DoMethod(MainWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, MUIV_Application_ReturnID_Quit);

           DoMethod(BT_ToolBar_Sprzeda�         , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_SPRZEDAZ);
                   DoMethod(BT_ToolBar_Vat          , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_VAT);
                   DoMethod(BT_ToolBar_Vat_Kor      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_KOREKTA);
//           DoMethod(BT_ToolBar_Upr          , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_UPROSZCZONY);
                   DoMethod(BT_ToolBar_Vat_Par      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_VAT_PAR);
//           DoMethod(BT_ToolBar_Upr_Par      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_UPROSZCZONY_PAR);
                   DoMethod(BT_ToolBar_Vat_ProForma , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_VAT_PROFORMA);
                   DoMethod(BT_ToolBar_Paragon      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_PARAGON);
                   DoMethod(BT_ToolBar_Order        , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ORDER);
                   DoMethod(BT_ToolBar_Nieudokumentowana, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_NIEUDOKUMENTOWANA);
                   DoMethod(BT_ToolBar_Sprzeda�_Back, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAINMENU);
           DoMethod(BT_ToolBar_Zakupy     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ZAKUPY);
                   DoMethod(BT_ToolBar_Zakupy_Koszty, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ZAKUPY_KOSZTY);
                   DoMethod(BT_ToolBar_Zakupy_Back  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAINMENU);
           DoMethod(BT_ToolBar_Redagowanie, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_REDAGOWANIE);
                   DoMethod(BT_ToolBar_Red_Bie��ce , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_RED_BIE��CE);
                   DoMethod(BT_ToolBar_Red_Od�o�one, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_RED_OD�O�ONE);
                   DoMethod(BT_ToolBar_Red_Przelew , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_RED_PRZELEW);
                   DoMethod(BT_ToolBar_Red_Back    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAINMENU);
           DoMethod(BT_ToolBar_BazyDanych , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_BAZYDANYCH);
                   DoMethod(BT_ToolBar_Magazyn    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAGAZYN);
                   DoMethod(BT_ToolBar_Kontrahenci, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_KONTRAHENCI);
                   DoMethod(BT_ToolBar_UserBase   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_USERS);
                   DoMethod(BT_ToolBar_Settings_Pay , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_SETTINGS_PAY);
                   DoMethod(BT_ToolBar_Settings_Unit, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_SETTINGS_UNIT);
                   DoMethod(BT_ToolBar_Bazy_Back, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAINMENU);
           DoMethod(BT_ToolBar_Rozliczenia, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ROZLICZENIA);
                   DoMethod(BT_ToolBar_Rozliczenia_Zamknij  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ZAMKNIJ);
//           DoMethod(BT_ToolBar_Rozliczenia_ZamknijMiesiac   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ZAMKNIJ_MIESIAC);
//           DoMethod(BT_ToolBar_Rozliczenia_ZamknijRok       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ZAMKNIJ_ROK);
                   DoMethod(BT_ToolBar_Rozliczenia_Fisk_Dobowy      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_DOBOWY);
                   DoMethod(BT_ToolBar_Rozliczenia_Fisk_Okresowy    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_OKRESOWY);
                   DoMethod(BT_ToolBar_Rozliczenia_Fisk_StanKasy    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_STAN_KASY);
                   DoMethod(BT_ToolBar_Rozliczenia_Fisk_RaportZmiany, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_ZMIANY);
                   DoMethod(BT_ToolBar_Rozliczenia_Back             , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAINMENU);
           DoMethod(BT_ToolBar_System, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_SYSTEM);
                   DoMethod(BT_ToolBar_Settings , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_SETTINGS );
                   DoMethod(BT_ToolBar_Numeracja, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_NUMERACJA );
                   DoMethod(BT_ToolBar_Drukarki , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_DRUKARKI );
                   DoMethod(BT_ToolBar_Tip      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_TIP );
                   DoMethod(BT_ToolBar_Inspektor, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_INSPEKTOR );
                   DoMethod(BT_ToolBar_MUI      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 1 ,MUIM_Application_OpenConfigWindow );
                   DoMethod(BT_ToolBar_System_Back, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAINMENU );
           DoMethod(BT_ToolBar_About      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ABOUT);
           DoMethod(BT_ToolBar_Login      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_LOGIN);
           DoMethod(BT_ToolBar_Logout     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_LOGOUT);
           DoMethod(BT_ToolBar_Quit       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, MUIV_Application_ReturnID_Quit);

        return( MainWindow );
}

//|
/// CreateApp
Object *CreateApp(void)
{
Object *app = NULL;

          app = ApplicationObject,
//      app = NewObject(CL_Application->mcc_Class, NULL,
                                 MUIA_Application_Title      , "Golem",
                                 MUIA_Application_Version    , VERSTAG,
                                 MUIA_Application_Copyright  , "� 1997-2003 W.F.M.H.",
                                 MUIA_Application_Author     , "Marcin Or�owski",
                                 MUIA_Application_Description, MSG_APP_DESC,
                                 MUIA_Application_Base       , "GOLEM",

///                StartupWindow
                MUIA_Application_Window, StartupWindow = WindowObject,
                                                        MUIA_Window_ID         , ID_WIN_STARTUP,

//                            MUIA_Window_Title      , TITLE,

                                                        MUIA_Window_ScreenTitle, ScreenTitle,
                                                        MUIA_Window_DragBar    , FALSE,
                                                        MUIA_Window_Borderless , TRUE,
                                                        MUIA_Window_CloseGadget, FALSE,
                                                        MUIA_Window_DepthGadget, FALSE,
                                                        MUIA_Window_SizeGadget , FALSE,
                                                        MUIA_Window_TopEdge    , MUIV_Window_TopEdge_Centered,
                                                        MUIA_Window_LeftEdge   , MUIV_Window_LeftEdge_Centered,

                                                        WindowContents, VGroup,
                                                                                GroupFrame,

///                                       Child, BodychunkObject,
                                   Child, BodychunkObject,
                                                  GroupFrame,
                                                  MUIA_Group_Spacing, 0,
                                                  MUIA_FixWidth             , GOLEM_WIDTH ,
                                                  MUIA_FixHeight            , GOLEM_HEIGHT,
                                                  MUIA_Bitmap_Width         , GOLEM_WIDTH ,
                                                  MUIA_Bitmap_Height        , GOLEM_HEIGHT,
                                                  MUIA_Bodychunk_Depth      , GOLEM_DEPTH ,
                                                  MUIA_Bodychunk_Body       , (UBYTE *) golem_body,
                                                  MUIA_Bodychunk_Compression, GOLEM_COMPRESSION,
                                                  MUIA_Bodychunk_Masking    , GOLEM_MASKING,
                                                  MUIA_Bitmap_SourceColors  , (ULONG *) golem_colors,
                                                  End,
//|
///                                       ScrollText
//                                        Child, TextObject,
//                                               MUIA_Font, MUIV_Font_Tiny,
//                                               MUIA_Text_PreParse, "\033c\033b",
//                                               MUIA_Text_Contents, "WFMH Golem v" VERSIONREVISION " (" DATE ") � 1997-2003 Marcin Or�owski\n",
//                                               End,

                                                                                Child, CR_Startup_Scroll = CrawlingObject,
                                                                                           MUIA_Virtgroup_Input, FALSE,
                                                                                           MUIA_FixHeightTxt, "\n\n\n",
                                                                                           MUIA_Font, MUIV_Font_Tiny,
                                                                                           Child, HGroup,
                                                                                                 Child, MUI_NewObject(MUIC_Text,
                                                                                                          MUIA_Text_PreParse, "\033r\033b",
                                                                                                          MUIA_Text_Contents, golem_about_text_left,
                                                                                                          End,

                                                                                                 Child, MUI_NewObject(MUIC_Text,
                                                                                                          MUIA_Text_PreParse, "\033l\0333",
                                                                                                          MUIA_Text_Contents, golem_about_text_right,
                                                                                                          End,
                                                                                                 End,
                                                                                           End,
//|

                                                                                Child, HGroup,
                                                                                           Child, GA_Startup_Info = GaugeObject,
                                                                                                          TextFrame,
                                                                                                          MUIA_Gauge_Horiz , TRUE,
                                                                                                          MUIA_Gauge_InfoText, "",
                                                                                                          MUIA_InputMode     , MUIV_InputMode_RelVerify,
                                                                                                          End,

                                                                                           Child, BT_Startup_Ok = TextButtonWeight(MSG_STARTUP_OK, 25),
                                                                                           End,

                                                                                End,
                                                        End,


//|

                                 MUIA_Application_Window, TOD_TipWindow = TipwindowObject, End,

                                 MUIA_Application_Window, MainWindow = CreateMainWindow(),

///                FiltrWindow

                                 MUIA_Application_Window,
                                         FiltrWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_FILTR_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_FILTR,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
//                        MUIA_Window_CloseGadget, FALSE,
                                                WindowContents,
                                                   VGroup,

                                                   Child, HGroup,
                                                                  GroupFrame,
                                                                  MUIA_Group_Columns, 2,

                                                                  Child, MakeLabel2(MSG_FILTR_NAME),
                                                                  Child, ST_Filtr_Nazwa = MakeString(FILTR_NAME_LEN, MSG_FILTR_NAME),


                                                                  End,

                                                   Child, HGroup,
                                                                  Child, BT_Filtr_Ok     = TextButton(MSG_FILTR_OK),
                                                                  Child, BT_Filtr_Cancel = TextButton(MSG_FILTR_CANCEL),
                                                                  End,

                                                   End,
                                          End,
//|
///                ProgressWindow

                                 MUIA_Application_Window,
                                         ProgressWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_PROG_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_PROG,
                                                MUIA_Window_ScreenTitle, ScreenTitle,

                                                MUIA_Window_CloseGadget, FALSE,
                                                MUIA_Window_SizeGadget , FALSE,
                                                MUIA_Window_TopEdge    , MUIV_Window_TopEdge_Centered,
                                                MUIA_Window_LeftEdge   , MUIV_Window_LeftEdge_Centered,
                                                MUIA_Window_Width      , MUIV_Window_Width_Visible(60),

                                                WindowContents,
                                                   VGroup,
                                                   GroupFrame,

                                                   Child, TX_Prog_Info = TextObject,
                                                                                                 MUIA_Text_PreParse, "\033l\0338",
                                                                                                 TextFrame, TextBack,
                                                                                                 End,

                                                   Child, GA_Prog_Info = GaugeObject,
                                                                                                 TextFrame,
                                                                                                 MUIA_Gauge_Horiz , TRUE,
                                                                                                 MUIA_Gauge_InfoText, "",
                                                                                                 End,

                                                   Child, ScaleObject, MUIA_Scale_Horiz, TRUE, End,
                                                   End,
                                          End,

//|

                                 // smieci z golem_zakupy.c
///                ZakupyWindow

                                 MUIA_Application_Window,
                                         ZakupyWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_ZAK_TITLE,
                                                MUIA_Window_ID         , ID_WIN_ZAKUPY,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,

                                                   Child, HGroup,
                                                                  Child, MakeLabel2(MSG_ZAK_MIESIAC),
                                                                  Child, CY_Zak_Miesiac = MakeCycle(Miesi�ceTable2, MSG_ZAK_MIESIAC),
                                                                  Child, BT_Zak_Load = TextButton(MSG_ZAK_LOAD),
                                                                  Child, BT_Zak_Save = TextButton(MSG_ZAK_SAVE),

                                                                  Child, MUI_MakeObject(MUIO_VBar,1),
                                                                  Child, BT_Zak_Index = TextButton(MSG_ZAK_INDEX),
                                                                  Child, BT_Zak_Sort  = TextButton(MSG_ZAK_SORT),
                                                                  End,

                                                   Child, VGroup,
                                                                  GroupFrame,
                                                                  Child, LV_Zak_Zakupy = ListviewObject,
                                                                                                MUIA_CycleChain, TRUE,
                                                                                                MUIA_Font, MUIV_Font_Tiny,
                                                                                                _MUIA_Listview_List, NewObject(CL_ZakupyList->mcc_Class, NULL, TAG_DONE),
                                                                                                End,

                                                                  Child, HGroup,
                                                                                 Child, BT_Zak_Add   = TextButton(MSG_ZAK_ADD),
                                                                                 Child, BT_Zak_Edit  = TextButton(MSG_ZAK_EDIT),
                                                                                 Child, BT_Zak_Del   = TextButton(MSG_ZAK_DEL),
                                                                                 Child, BT_Zak_Info  = TextButton(MSG_ZAK_INFO),
                                                                                 Child, BT_Zak_Print = TextButton(MSG_ZAK_PRINT),
                                                                                 End,
                                                                  End,


                                                   Child, HGroup,
                                                                  Child, BT_Zak_Ok     = TextButton(MSG_ZAK_OK),
                                                                  Child, BT_Zak_Cancel = TextButton(MSG_ZAK_CANCEL),
                                                                  End,

                                                   End,
                                          End,

//|
///                EdycjaZakupuWindow

                                 MUIA_Application_Window,
                                         EdycjaZakupuWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_ZAK_EDIT_TITLE,
                                                MUIA_Window_ID         , ID_WIN_ZAKUP_EDIT,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,

                                                   Child, GR_ZakE_Pages = RegisterGroup(GR_ZakE_Titles),
                                                                  Child, VGroup,
                                                                                 Child, ColGroup(2),
                                                                                                Child, MakeLabel2(MSG_ZAKE_P1),
                                                                                                Child, ST_ZakE_P1 = MakeNumericString(7, MSG_ZAKE_P1),
                                                                                                Child, MakeLabel2(MSG_ZAKE_P2),
                                                                                                Child, ST_ZakE_P2 = MakeString(STR_ZAK_DOW�D_LEN, MSG_ZAKE_P2),
                                                                                                Child, MakeLabel2(MSG_ZAKE_P3),
                                                                                                Child, ST_ZakE_P3 = MakeString(DATE_LEN, MSG_ZAKE_P3),
                                                                                                Child, MakeLabel2(MSG_ZAKE_P4),
                                                                                                Child, ST_ZakE_P4 = MakeString(DATE_LEN, MSG_ZAKE_P4),

                                                                                                Child, BT_ZakE_Kontrahent = TextButton(MSG_ZAKE_P5),
                                                                                                Child, TX_ZakE_P5         = TextObject, TextFrame, TextBack, End,
                                                                                                Child, MakeLabel2(NULL),
                                                                                                Child, TX_ZakE_P6         = TextObject, TextFrame, TextBack, End,
                                                                                                Child, MakeLabel2(NULL),
                                                                                                Child, TX_ZakE_P7         = TextObject, TextFrame, TextBack, End,
                                                                                                End,

                                                                                 Child, HVSpace,
                                                                                 Child, HGroup,
                                                                                                Child, HVSpace,
                                                                                                Child, BT_ZakE_Next1 = TextButtonWeight(MSG_ZAKE_NEXT, 5),
                                                                                                End,
                                                                                 End,

                                                                  /* Page #2 */

                                                                  Child, VGroup,
                                                                                 Child, TextObject, TextFrame, TextBack,
                                                                                                MUIA_Font, MUIV_Font_Tiny,
                                                                                                MUIA_Text_Contents, MSG_ZAKE_INFO1,
                                                                                                End,
                                                                                 Child, ColGroup(2),
                                                                                                Child, MakeLabel2(MSG_ZAKE_P9),
                                                                                                Child, ST_ZakE_P9  = MakeCashString(10, MSG_ZAKE_P9),
                                                                                                End,

                                                                                 Child, TextObject, TextFrame, TextBack,
                                                                                                MUIA_Font, MUIV_Font_Tiny,
                                                                                                MUIA_Text_Contents, MSG_ZAKE_INFO2,
                                                                                                End,
                                                                                 Child, ColGroup(2),
                                                                                                Child, MakeLabel2(MSG_ZAKE_P10),
                                                                                                Child, HGroup,
                                                                                                           Child, ST_ZakE_P10 = MakeCashString(10, MSG_ZAKE_P10),
                                                                                                           Child, MakeLabel2(MSG_ZAKE_VAT1),
                                                                                                           Child, TX_ZakE_P11 = TextObject, TextFrame, TextBack, End,
                                                                                                           Child, MakeLabel2(MSG_ZAKE_VAT2),
                                                                                                           End,
                                                                                                Child, MakeLabel2(MSG_ZAKE_P12),
                                                                                                Child, HGroup,
                                                                                                Child, ST_ZakE_P12 = MakeCashString(10, MSG_ZAKE_P12),
                                                                                                           Child, MakeLabel2(MSG_ZAKE_VAT1),
                                                                                                           Child, TX_ZakE_P13 = TextObject, TextFrame, TextBack, End,
                                                                                                           Child, MakeLabel2(MSG_ZAKE_VAT2),
                                                                                                           End,
                                                                                                End,
                                                                                 Child, HVSpace,
                                                                                 Child, HGroup,
                                                                                                Child, HVSpace,
                                                                                                Child, BT_ZakE_Next2 = TextButtonWeight(MSG_ZAKE_NEXT, 5),
                                                                                                End,
                                                                                 End,

                                                                  /* Page #3 */

                                                                  Child, VGroup,
                                                                                 Child, TextObject, TextFrame, TextBack,
                                                                                                MUIA_Font, MUIV_Font_Tiny,
                                                                                                MUIA_Text_Contents, MSG_ZAKE_INFO3,
                                                                                                End,

                                                                                 Child, ColGroup(2),
                                                                                                Child, MakeLabel2(MSG_ZAKE_P14),
                                                                                                Child, HGroup,
                                                                                                           Child, ST_ZakE_P14 = MakeCashString(10, MSG_ZAKE_P14),
                                                                                                           Child, MakeLabel2(MSG_ZAKE_VAT1),
                                                                                                           Child, TX_ZakE_P15 = TextObject, TextFrame, TextBack, End,
                                                                                                           Child, MakeLabel2(MSG_ZAKE_VAT2),
                                                                                                           End,
                                                                                                Child, MakeLabel2(MSG_ZAKE_P16),
                                                                                                Child, HGroup,
                                                                                                           Child, ST_ZakE_P16 = MakeCashString(10, MSG_ZAKE_P16),
                                                                                                           Child, MakeLabel2(MSG_ZAKE_VAT1),
                                                                                                           Child, TX_ZakE_P17 = TextObject, TextFrame, TextBack, End,
                                                                                                           Child, MakeLabel2(MSG_ZAKE_VAT2),
                                                                                                           End,
                                                                                                End,

                                                                                                Child, HGroup,
                                                                                                           Child, MakeLabel2(MSG_ZAKE_OPIS),
                                                                                                           Child, ST_ZakE_Opis = MakeString(STR_ZAK_OPIS_LEN, MSG_ZAKE_OPIS),
                                                                                                           End,

                                                                                 Child, HVSpace,
                                                                                 Child, HGroup,
                                                                                                Child, HVSpace,
                                                                                                Child, BT_ZakE_Next3 = TextButtonWeight(MSG_ZAKE_NEXT, 5),
                                                                                                End,
                                                                                 End,
                                                                  End,

/*
                                                   Child, ColGroup(2),
                                                                  Child, MakeLabel2(MSG_ZAKE_P8),
                                                                  Child, TX_ZakE_P8         = TextObject, TextFrame, TextBack, End,
                                                                  Child, MakeLabel2(MSG_ZAKE_P18),
                                                                  Child, TX_ZakE_P18        = TextObject, TextFrame, TextBack, End,
                                                                  End,
*/

                                                   Child, HGroup,
                                                                  Child, BT_ZakE_Ok     = TextButton(MSG_ZAKE_OK),
                                                                  Child, BT_ZakE_Cancel = TextButton(MSG_ZAKE_CANCEL),
                                                                  End,

                                                   End,
                                          End,

//|
///                InformacjeOZakupachWindow

                                 MUIA_Application_Window,
                                         InformacjeOZakupachWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_ZAK_INFO_TITLE,
                                                MUIA_Window_ID         , ID_WIN_ZAKUP_INFO,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,

                                                   Child, ColGroup(2),
                                                                  Child, MakeLabel2(MSG_ZAKI_COUNT),
                                                                  Child, TX_ZakI_Count = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033l", End,
                                                                  End,

                                                   Child, TextObject, TextFrame, TextBack,
                                                                  MUIA_Font, MUIV_Font_Tiny,
                                                                  MUIA_Text_Contents, MSG_ZAKI_INFO1,
                                                                  End,
                                                   Child, ColGroup(2),
                                                                  GroupFrame, GroupBack,
                                                                  Child, MakeLabel2(MSG_ZAKI_P9),
                                                                  Child, TX_ZakI_P9 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                                                  End,

                                                   Child, TextObject, TextFrame, TextBack,
                                                                  MUIA_Font, MUIV_Font_Tiny,
                                                                  MUIA_Text_Contents, MSG_ZAKE_INFO2,
                                                                  End,
                                                   Child, ColGroup(4),
                                                                  GroupFrame, GroupBack,
                                                                  Child, MakeLabel2(MSG_ZAKI_P10),
                                                                  Child, TX_ZakI_P10 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                                                  Child, MakeLabel2(MSG_ZAKI_P11),
                                                                  Child, TX_ZakI_P11 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                                                  Child, MakeLabel2(MSG_ZAKI_P12),
                                                                  Child, TX_ZakI_P12 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                                                  Child, MakeLabel2(MSG_ZAKI_P13),
                                                                  Child, TX_ZakI_P13 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                                                  End,

                                                   Child, TextObject, TextFrame, TextBack,
                                                                  MUIA_Font, MUIV_Font_Tiny,
                                                                  MUIA_Text_Contents, MSG_ZAKE_INFO3,
                                                                  End,
                                                   Child, ColGroup(4),
                                                                  GroupFrame, GroupBack,
                                                                  Child, MakeLabel2(MSG_ZAKI_P14),
                                                                  Child, TX_ZakI_P14 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                                                  Child, MakeLabel2(MSG_ZAKI_P15),
                                                                  Child, TX_ZakI_P15 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                                                  Child, MakeLabel2(MSG_ZAKI_P16),
                                                                  Child, TX_ZakI_P16 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                                                  Child, MakeLabel2(MSG_ZAKI_P17),
                                                                  Child, TX_ZakI_P17 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                                                  End,

                                                   Child, ColGroup(2),
                                                                  GroupFrame, GroupBack,
                                                                  Child, MakeLabel2(MSG_ZAKI_P8),
                                                                  Child, TX_ZakI_P8  = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                                                  Child, MakeLabel2(MSG_ZAKI_P18),
                                                                  Child, TX_ZakI_P18 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                                                  End,

                                                   Child, HGroup,
                                                                  Child, BT_ZakI_Ok     = TextButton(MSG_ZAKI_OK),
                                                                  End,

                                                   End,
                                          End,

//|
///                IndexRequesterWindow

                                 MUIA_Application_Window,
                                         IndexRequesterWindow = WindowObject,
                                                MUIA_Window_ID         , ID_WIN_INDEXREQUEST,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                MUIA_Window_Title, MSG_IDX_TITLE,
                                                WindowContents,
                                                   VGroup,

                                                   Child, HGroup,
                                                                  GroupFrameT(MSG_IDX_GTITLE),
                                                                  Child, ST_Idx_String = MakeNumericString(STR_REQ_MAXLEN, MSG_STR_STRING),
                                                                  End,

                                                   Child, HGroup,
                                                                  MUIA_Group_SameSize, TRUE,
                                                                  Child, BT_Idx_Ok     = TextButton(MSG_IDX_OK),
                                                                  Child, BT_Idx_Cancel = TextButton(MSG_IDX_CANCEL),
                                                                  End,

                                                   End,
                                          End,
//|


                                 End;



        if(app)
           {
           /* STARTUP WINDOW */

           DoMethod(StartupWindow  , MUIM_Notify, MUIA_Window_InputEvent, "esc", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_STARTUP_CLOSED);
           DoMethod(GA_Startup_Info, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_STARTUP_URL );
           DoMethod(BT_Startup_Ok  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_STARTUP_CLOSED);


           /* FILTR MAGAZYNOWY WINDOW */

           DoMethod(FiltrWindow    , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
           DoMethod(BT_Filtr_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
           DoMethod(BT_Filtr_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);





           /* ZAKUPY WINDOW */

           DoMethod(ZakupyWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
           DoMethod(BT_Zak_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
           DoMethod(BT_Zak_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

           DoMethod(ZakupyWindow , MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ADD);
           DoMethod(LV_Zak_Zakupy, MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_EDIT);

           DoMethod(ZakupyWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_EDIT);
           DoMethod(ZakupyWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);
           DoMethod(ZakupyWindow, MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_DELETE);

           DoMethod(BT_Zak_Add   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);
           DoMethod(BT_Zak_Edit  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_EDIT);
           DoMethod(BT_Zak_Del   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_DELETE);
           DoMethod(BT_Zak_Info  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_INFO);

           DoMethod(BT_Zak_Load  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_LOAD);
           DoMethod(BT_Zak_Save  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SAVE);
           DoMethod(BT_Zak_Index , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_INDEX);
           DoMethod(BT_Zak_Sort  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SORT);


           /* EDYCJA ZAKUPU WINDOW */

           DoMethod(EdycjaZakupuWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
           DoMethod(BT_ZakE_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
           DoMethod(BT_ZakE_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

           DoMethod(EdycjaZakupuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_0);
           DoMethod(BT_ZakE_Next1     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_PAGE_1);
           DoMethod(EdycjaZakupuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_1);
           DoMethod(BT_ZakE_Next2     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_PAGE_2);
           DoMethod(EdycjaZakupuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_2);
           DoMethod(BT_ZakE_Next3     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_PAGE_0);

           DoMethod(ST_ZakE_P10    , MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ZAKE_CALC_P11);
           DoMethod(ST_ZakE_P12    , MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ZAKE_CALC_P13);
           DoMethod(ST_ZakE_P14    , MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ZAKE_CALC_P15);
           DoMethod(ST_ZakE_P16    , MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ZAKE_CALC_P17);

           DoMethod(BT_ZakE_Kontrahent, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_KLIENT_WYBIERZ);


           /* INFORMACJE O ZAKUPACH WINNDOW */

           DoMethod(InformacjeOZakupachWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
           DoMethod(BT_ZakI_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);


           /* INDEX REQUESTER WINDOW */

           DoMethod(IndexRequesterWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
           DoMethod(BT_Idx_Ok           , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
           DoMethod(BT_Idx_Cancel       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


           }

        return(app);
}

//|
