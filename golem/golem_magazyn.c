
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/* $Id: golem_magazyn.c,v 1.1 2003/01/01 20:40:53 carl-os Exp $ */

#include "golem.h"


struct Filtr mag_filter =            /* G��wne okno edycji magazynu */
{
        FALSE,
        "#?"                              // Nazwa
};



/// CreateMagazynWindow

static Object *MagazynWindow,                /* MAGAZYN WINDOW       */
           *TX_Mag_Grupy,
           *BT_Mag_Grupy,
           *LV_Mag_Grupy,
           *PB_Mag_Grupy,

           *BT_Mag_DodajGrup�,
           *BT_Mag_Usu�Grup�,
           *BT_Mag_EdytujGrup�,
           *BT_Mag_Wydrukuj,
           *BT_Mag_Import,
           *LV_Mag_Produkty,
           *ST_Mag_Wybierz,
           *BT_Mag_Znajd�,
           *BT_Mag_Dodaj,
           *BT_Mag_Edytuj,
           *BT_Mag_Usu�,
           *BT_Mag_Przenie�,
           *BT_Mag_Filter,
           *BT_Mag_FilterAll,
           *BT_Mag_Ok,
           *BT_Mag_Cancel;


/// WindowFunc
SAVEDS ASM VOID WindowFunc(REG(a2) Object *pop,REG(a1) Object *win)
{
        set(win, MUIA_Window_DefaultObject, pop);
}
//|
///StrObjFunc - Mag
SAVEDS ASM LONG StrObjFunc_Mag(REG(a2) Object *pop,REG(a1) Object *tx)
{
struct GrupaList *node;
char *s;
int i;

        DoMethod(LV_Mag_Grupy, _MUIM_List_Clear, NULL);

        if(!IsListEmpty(&grupy))
           {
           for(node = (struct GrupaList *)grupy.lh_Head; node->gm_node.mln_Succ; node = (struct GrupaList *)node->gm_node.mln_Succ)
                   {
                   DoMethod(LV_Mag_Grupy, _MUIM_List_InsertSingle, node, _MUIV_List_Insert_Sorted);
                   }

           get(TX_Mag_Grupy, MUIA_Text_Contents, &s);

           set(LV_Mag_Grupy, _MUIA_List_Active, _MUIV_List_Active_Off);
           for(i=0;;i++)
                   {
                   DoMethod(LV_Mag_Grupy, _MUIM_List_GetEntry, i, &node);
                   if(!node)
                           break;
                   if(!stricmp(node->gm_grupa.Nazwa, s))
                          {
                          set(LV_Mag_Grupy, _MUIA_List_Active, i);
                          break;
                          }
                   }
           }

        return(TRUE);
}
//|
/// ObjStrFunc - Mag
SAVEDS ASM VOID ObjStrFunc_Mag(REG(a2) Object *pop,REG(a1) Object *tx)
{
struct GrupaList *grupa;

        DoMethod(LV_Mag_Grupy, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &grupa);
        if(grupa)
                set(TX_Mag_Grupy, MUIA_Text_Contents, grupa->gm_grupa.Nazwa);

}
//|

char CreateMagazynWindow(void)
{
static const struct Hook ObjStrHook_Mag = {{ NULL,NULL }, (VOID *)ObjStrFunc_Mag, NULL, NULL};
static const struct Hook StrObjHook_Mag = {{ NULL,NULL }, (VOID *)StrObjFunc_Mag, NULL, NULL};
static const struct Hook WindowHook     = {{ NULL,NULL }, (void *)WindowFunc    , NULL, NULL};

///    CreateMagazynWindow

//                 MUIA_Application_Window,
                                         MagazynWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_MAG_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_MAGAZYN,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
//                        MUIA_Window_CloseGadget, FALSE,
                                                WindowContents,
                                                   VGroup,

///                          Lista magazynowa
                                                                  Child, VGroup,
                                                                                 GroupFrame,
                                                                                 MUIA_Weight, 60,
                                                                                 Child, HGroup,
                                                                                                Child, HGroup,
                                                                                                           MUIA_Group_Spacing, 1,
                                                                                                           Child, MakeLabel2(MSG_MAG_GROUP),
                                                                                                           Child, TX_Mag_Grupy = TextObject2, End,
                                                                                                           Child, BT_Mag_Grupy = PopButton2(MUII_PopUp, MSG_MAG_GROUP),
                                                                                                           End,
 
                                                                                                Child, HGroup,
                                                                                                                MUIA_Group_Spacing, 1,
                                                                                                                MUIA_Weight, 1,
                                                                                                                Child, BT_Mag_DodajGrup�  = TextButtonWeight(MSG_MAG_GROUP_DODAJ , 1),
                                                                                                                Child, BT_Mag_Usu�Grup�   = TextButtonWeight(MSG_MAG_GROUP_SKASUJ, 1),
                                                                                                                Child, BT_Mag_EdytujGrup� = TextButtonWeight(MSG_MAG_GROUP_EDYTUJ, 1),
                                                                                                                End,

                                                                                                Child, MUI_MakeObject(MUIO_VBar,1),
                                                                                                Child, BT_Mag_Wydrukuj = _TextButtonWeight(MAG_PRINT, 1),
                                                                                                Child, MUI_MakeObject(MUIO_VBar,1),
                                                                                                Child, BT_Mag_Import   = _TextButtonWeight(MAG_IMPORT, 1),

                                                                                                End,

                                                                                 Child, LV_Mag_Produkty = _ListviewObject,
                                                                                                MUIA_CycleChain, TRUE,
                                                                                                _MUIA_Listview_MultiSelect, _MUIV_Listview_MultiSelect_Default,
                                                                                                _MUIA_Listview_List, NewObject(CL_ProductList->mcc_Class, NULL, TAG_DONE),
                                                                                                End,

                                                                                 Child, HGroup,
                                                                                                Child, MakeLabel2(MSG_MAG_SELECT),
                                                                                                Child, ST_Mag_Wybierz= MakeString(PROD_NAME_LEN, MSG_MAG_SELECT),
                                                                                                Child, BT_Mag_Znajd� = TextButtonWeight(MSG_MAG_FIND, 1),
                                                                                                End,

                                                                                 Child, HGroup,
//                                                Child, BT_Mag_Dodaj  = SizedButton(MSG_MAG_DODAJ),
                                                                                                Child, BT_Mag_Dodaj  = _TextButton( MAG_DODAJ ),
                                                                                                Child, BT_Mag_Edytuj = _TextButton( MAG_EDYTUJ ),
                                                                                                Child, BT_Mag_Usu�   = _TextButton( MAG_USUN ),

                                                                                                Child, MUI_MakeObject(MUIO_VBar,1),
                                                                                                Child, BT_Mag_Przenie� = _TextButton( MAG_PRZENIES ),
                                                                                                Child, MUI_MakeObject(MUIO_VBar,1),

                                                                                                Child, HGroup,
//                                                       GroupFrame,
                                                                                                           MUIA_Weight, 20,
                                                                                                           Child, BT_Mag_Filter    = TextButton(MSG_MAG_FILTER),
                                                                                                           Child, BT_Mag_FilterAll = TextButton(MSG_MAG_FILTER_ALL),
                                                                                                           End,
                                                                                                End,
                                                                                 End,
//|

                                                   Child, HGroup,
                                                                  Child, BT_Mag_Ok     = TextButton(MSG_MAG_OK),
                                                                  Child, BT_Mag_Cancel = TextButton(MSG_MAG_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|

        if(!MagazynWindow)
           return(FALSE);


           DoMethod(MagazynWindow  ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

           DoMethod(ST_Mag_Wybierz, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_WYBIERZ);
           set(ST_Mag_Wybierz, MUIA_String_AttachedList, LV_Mag_Produkty);
           DoMethod(BT_Mag_Znajd�   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_ZNAJD�);

           DoMethod(BT_Mag_DodajGrup�  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_DODAJ_GRUP�);
           DoMethod(BT_Mag_Usu�Grup�   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_USU�_GRUP�);
           DoMethod(BT_Mag_EdytujGrup� ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_EDYCJA_GRUPY);
           DoMethod(LV_Mag_Grupy, MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_ZMIE�_GRUP�);


           DoMethod(BT_Mag_Grupy    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_ZMIE�_GRUP�);


           DoMethod( BT_Mag_Wydrukuj, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_PRINT );
           DoMethod( BT_Mag_Import  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_IMPORT );

           DoMethod(BT_Mag_Dodaj    ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_DODAJ);
           DoMethod(BT_Mag_Usu�     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_SKASUJ);
           DoMethod(BT_Mag_Edytuj   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_EDYTUJ);
           DoMethod(BT_Mag_Przenie� ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MOVE);

           DoMethod(BT_Mag_Filter     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_FILTER);
           DoMethod(BT_Mag_FilterAll  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_FILTER_ALL);

           DoMethod(BT_Mag_Ok      ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
           DoMethod(BT_Mag_Cancel  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_DODAJ);
           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "shift f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_DODAJ_ALT);

           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_EDYTUJ);
           DoMethod(LV_Mag_Produkty      , MUIM_Notify, _MUIA_Listview_DoubleClick, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_EDYTUJ);
           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_SKASUJ);
           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "control del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_SKASUJ_AND_SKIP);
           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MOVE);
           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "f7", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_ZNAJD�);
           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);
//       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "esc", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CANCEL);

           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "shift left" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FIRST_GROUP);
           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "left" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PREV_GROUP);
           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "right", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_NEXT_GROUP);
           DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "shift right", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_LAST_GROUP);


           // attach

           DoMethod(app, OM_ADDMEMBER, MagazynWindow);

        return(TRUE);
}
//|
/// DisposeMagazynWindow
void DisposeMagazynWindow(void)
{

        DoMethod(app, OM_REMMEMBER, MagazynWindow);
        MUI_DisposeObject(MagazynWindow);

}
//|



#define OBETNIJ_KONCOWKE
/// ObetnijKo�c�wk�
double __inline ObetnijKo�c�wk�(double kwota)
{
#ifdef OBETNIJ_KONCOWKE

static char out[30];
char   *dot;

//    printf("ObetnijKoncowke()\n");
//    printf("kwota: %#.5f", kwota);

        sprintf(out, "%#.5f", kwota);
        dot = strchr(out, '.');
//    D(bug("Obetnij => %s\n", out));


        if(*(dot+3) >= '5')
           {
           kwota += 0.005;
           sprintf(out, "%#.5f", kwota);
           dot = strchr(out, '.');
           }

        *(dot+3) = '\0';

        return( atof(out) );

#else

        return(kwota);

#endif
}
//|
/// CalcVat
double __inline CalcVat(double _cena, char _vat)
{
double vat, tmp;

        vat = (_cena * VatValTable[_vat]) / 100.0;
        return( ObetnijKo�c�wk�(vat) );
}
//|
/// CalcMar�a

// liczy kwot� mar�y u�ywane podczas edycji produktu
// gdy u�ytkownik wpisze procent mar�y

double __inline CalcMar�a(double zakup, double mar�a)
{
double mar;

        mar = (zakup * mar�a) / 100.;

        // kwota mar�y
//    return(mar);
        return(ObetnijKo�c�wk�(mar));
}
//|
/// CalcMar�aProcent

// liczy procent mar�y jak� jest warto�� "Mar�a"
// w stosunku do warto�ci "zakup"

double __inline CalcMar�aProcent(double zakup, double mar�a)
{
double procent;

        if(zakup == 0)
           return(0);

        procent = (mar�a * 100.0) / zakup;

        // kwota mar�y
//    return(procent);
        return(ObetnijKo�c�wk�(procent));
}
//|
/// CalcBrutto

// Oblicza cen� sprzeda�y brutto

double __inline CalcBrutto(double zakup, double mar�a, char vat)
{
double sprzeda�;
double tmp;
double zl, gr;

//printf("Zakup: %#.3f   Mar�a: %#.3f\n", zakup, mar�a);
        sprzeda�  = zakup + mar�a;
//printf("vat: %#.3f", CalcVat(sprzeda�, vat));
        sprzeda� += CalcVat(sprzeda�, vat);
//printf("   total: %#.3f\n", sprzeda�, vat);

//    return(sprzeda�);
        return(ObetnijKo�c�wk�(sprzeda�));
}
//|
/// CalcRabat

// oblicza kwot� rabatu

double __inline CalcRabat(double cena, double rab)
{

        return( ObetnijKo�c�wk�( ObetnijKo�c�wk�(cena * rab) / 100.0) );
//    return((cena * rab) / 100.0);
}
//|


/// MakeSmartList

/*
char def_format[] = "MIW=1 MAW=-1 COL=0 BAR,"  /* del */
                                        "MIW=1 MAW=10 COL=1 BAR,"  /* indeks */
                                        "MIW=1 MAW=49 COL=2 BAR,"  /* nazwa */
                                        "MIW=1 MAW=10 COL=3 BAR,"  /* sww */
                                        "MIW=1 MAW=-1 COL=4 P=\033r BAR,"  /* netto */
                                        "MIW=1 MAW=-1 COL=5 P=\033r BAR,"  /* vat */
                                        "MIW=1 MAW=-1 COL=6 P=\033r BAR,"  /* brutto */
                                        "MIW=1 MAW=10 COL=7 P=\033r BAR,"  /* jedn */
                                        "MIW=1 MAW=-1 COL=8 P=\033r,"      /* amount */
                                        "MIW=1 MAW=-1 COL=9 P=\033l";      /* 9 amount (cz. u�amkowa)  */

*/

void MakeSmartList(Object *lista, char EditMode, char ForceCheck)
{
/*
** pokazuje tylko te kolumny listy magazynowj, ktore sa
** wykorzystywane. Chowa INDEX i SWW jesli zbedne.
** jesli EditMode == FALSE chowa takze DEL. jesli
** ForceCheck == TRUE
*/

char def_index[] =  "MIW=1 MAW=10 COL=1 BAR,";  /* indeks */
char def_sww[]   =  "MIW=1 MAW=10 COL=3 BAR,";  /* sww */

char format[256];


           if(!wfmh_isenv("Golem/NoSmartMagLists"))
                  {
                  format[0] = 0;

                  if(ForceCheck)
                        {
                        int sww = 0;
                        int index = 0;
                        int i;

                        for(i=0 ;; i++)
                           {
                           struct ProductList *prod;

                           DoMethod(lista, _MUIM_List_GetEntry, i, &prod);

                           if(!prod)
                                   break;

                           if((sww != 0) && (index != 0))
                                   break;

                           if(prod->pl_prod.Index[0] != 0)
                                   index++;

                           if(prod->pl_prod.SWW[0] != 0)
                                   sww++;
                           }

                        set(lista, MUIA_ProdList_IndexCount, index);
                        set(lista, MUIA_ProdList_SWWCount, sww);

                        }

                  if(EditMode)
                        strcat(format, "MIW=1 MAW=-1 COL=0 BAR,");  /* del */

                  if(xget(lista, MUIA_ProdList_IndexCount))
                        strcat(format, def_index);

                  strcat(format, "MIW=1 MAW=49 COL=2 BAR,");  /* nazwa */

                  if(xget(lista, MUIA_ProdList_SWWCount))
                        strcat(format, def_sww);


                  strcat(format, "MIW=1 MAW=-1 COL=4 P=\033r BAR,"  /* netto */
                                                 "MIW=1 MAW=-1 COL=5 P=\033r BAR,"  /* vat */
                                                 "MIW=1 MAW=-1 COL=6 P=\033r BAR,"  /* brutto */
                                                 "MIW=1 MAW=10 COL=7 P=\033r BAR,"  /* jedn */
                                                 "MIW=1 MAW=-1 COL=8 P=\033r,"      /* 8 amount (cz. ca�kowita) */
                                                 "MIW=1 MAW=-1 COL=9 P=\033l"       /* 9 amount (cz. u�amkowa)  */
                                                );

                  set(lista, _MUIA_List_Format, format);
                  }
}
//|

/// ApplyMagFilter
void ApplyMagFilter(Object *lista, struct GrupaList *grupa, struct Filtr *filtr, char EditMode)
{
/*
** Umieszcza na li�cie (LV) tylko te produkty danej grupy
** kt�re spe�niaj� podany w filtrze pattern
**
** jesli edit mode=true, smart list nie bedzie chowal pierwszej
** kolumny (Deleted)
*/

struct ProductList *node;
static char pattern[(FILTR_NAME_LEN*2)+2];
struct List *produkty = &grupa->produkty;

           if(!grupa) return;

           set(lista, _MUIA_List_Quiet, TRUE);

//       if(!wfmh_isenv("Golem/NoSmartMagLists"))
//           set(lista, _MUIA_List_Format, def_format);


           DoMethod(lista, MUIM_List_Clear, NULL);

           if(!IsListEmpty(produkty))
                 {
                 if(filtr->Aktywny)
                   {
                   if(ParsePatternNoCase(filtr->Nazwa, pattern, sizeof(pattern)) <1)
                           filtr->Aktywny = FALSE;
                   }


                 if(filtr->Aktywny)
                   {
                   for(node = (struct ProductList *)produkty->lh_Head; node->pl_node.mln_Succ; node = (struct ProductList *)node->pl_node.mln_Succ)
                         if(MatchPatternNoCase(pattern, node->pl_prod.Nazwa))
                                 {
                                 DoMethod(lista, _MUIM_List_InsertSingle, node, _MUIV_List_Insert_Bottom);
                                 }
                   }
                 else
                   {
                   for(node = (struct ProductList *)produkty->lh_Head; node->pl_node.mln_Succ; node = (struct ProductList *)node->pl_node.mln_Succ)
                         {
                         DoMethod(lista, _MUIM_List_InsertSingle, node, _MUIV_List_Insert_Bottom);
                         }
                   }
                 }

           MakeSmartList(lista, EditMode, FALSE);

           set(lista, _MUIA_List_Quiet, FALSE);
}
//|
/// FilterSetup
void FilterSetup(struct Filtr *filtr)
{

        set(ST_Filtr_Nazwa, MUIA_String_Contents, filtr->Nazwa);

}
//|
/// FilterFinish
char FilterFinish(struct Filtr *filtr)
{

        if(!strlen((char *)xget(ST_Filtr_Nazwa, MUIA_String_Contents)) )
           {
           set(FiltrWindow, MUIA_Window_ActiveObject, ST_Filtr_Nazwa);
           return(FALSE);
           }

        strcpy(filtr->Nazwa, (char *)xget(ST_Filtr_Nazwa, MUIA_String_Contents));
        return(TRUE);
}
//|
/// FiltrProdukty

char FiltrProdukty(Object *lista, struct Filtr *filtr)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

        set(app, MUIA_Application_Sleep, TRUE);

        FilterSetup(filtr);

        set(FiltrWindow, MUIA_Window_ActiveObject, ST_Filtr_Nazwa);

        if( WinOpen(FiltrWindow) )
                {
                while(running)
                  {
                  switch (DoMethod(app, MUIM_Application_Input, &signal))
                        {
                        case ID_OK:
                           if(FilterFinish(filtr))
                                 {
                                 filtr->Aktywny = TRUE;
                                 ApplyMagFilter(lista, grupa_aktywna, filtr, TRUE);

                                 running = FALSE;
                                 }
                           else
                                 {
                                 DisplayBeep(0);
                                 }
                           break;


                        case ID_CANCEL:
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

                set(FiltrWindow, MUIA_Window_Open, FALSE);
                }
         else
                {
                DisplayBeep(0);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


        set(app, MUIA_Application_Sleep, FALSE);

        return(result);
}

//|

/// Wy�wietlNazw�Grupy

void Wy�wietlNazw�Grupy(Object *obj, struct GrupaList *grupa)
{
/*
** Wy�wietlan nazw� podanej grupy w podany obiekcie
** dodaj�c odpowiedni atrybut w zale�no�ci
** od jej statusu (skasowana czy nie)
**
** Je�li grupa jest skasowana, blokowana jest LV_Mag_Produkty
** i odpowiednie batony
*/

char preparse[2+2+1] = "\0";

        if(grupa->Deleted)
           strcpy(preparse, "\0338");

        if( grupa->gm_grupa.Typ != TYP_TOWAR )
           strcat(preparse, "\033b");

        set( obj, MUIA_Text_PreParse, preparse );
        set( obj, MUIA_Text_Contents, grupa->gm_grupa.Nazwa );


        {
        long mode = (grupa->Deleted == TRUE);

        set(LV_Mag_Produkty      , MUIA_Disabled, mode);
        set(BT_Mag_Dodaj         , MUIA_Disabled, mode);
        set(BT_Mag_Edytuj        , MUIA_Disabled, mode);
        set(BT_Mag_Usu�          , MUIA_Disabled, mode);
        set(BT_Mag_Przenie�      , MUIA_Disabled, mode);
        set(BT_Mag_EdytujGrup�   , MUIA_Disabled, mode);
        set(BT_Mag_Wydrukuj      , MUIA_Disabled, mode);
        set(ST_Mag_Wybierz       , MUIA_Disabled, mode);
        set(BT_Mag_Znajd�        , MUIA_Disabled, mode);
        set(BT_Mag_Filter        , MUIA_Disabled, mode);
        set(BT_Mag_FilterAll     , MUIA_Disabled, mode);
        }

}

//|
/// UstawGrup�Aktywn�Magazyn

void UstawGrup�Aktywn�Magazyn(struct GrupaList *grupa)
{


//    if(grupa == grupa_aktywna)
//       return;


        grupa_aktywna = grupa;

        if(grupa)
           {
           Wy�wietlNazw�Grupy(TX_Mag_Grupy, grupa);

           mag_filter.Aktywny = FALSE;
           ApplyMagFilter(LV_Mag_Produkty, grupa_aktywna, &mag_filter, TRUE);
           }
        else
           {
//       DoMethod(LV_Mag_Produkty, _MUIM_List_Clear, NULL);

           set(TX_Mag_Grupy, MUIA_Text_PreParse, "");
           set(TX_Mag_Grupy, MUIA_Text_Contents, MSG_MAG_NO_GROUPS);
           }

        {
        char mode = (grupa_aktywna == NULL);
        char grp_del = mode;

        if(mode != TRUE)
           {
           mode = grupa_aktywna->Deleted;
           if(mode)
                   grp_del = FALSE;
           }

        set(LV_Mag_Produkty      , MUIA_Disabled, mode);
        set(BT_Mag_Dodaj         , MUIA_Disabled, mode);
        set(BT_Mag_Edytuj        , MUIA_Disabled, mode);
        set(BT_Mag_Usu�          , MUIA_Disabled, mode);
        set(BT_Mag_Przenie�      , MUIA_Disabled, mode);
        set(BT_Mag_Usu�Grup�     , MUIA_Disabled, grp_del);
        set(BT_Mag_EdytujGrup�   , MUIA_Disabled, mode);
        set(BT_Mag_Wydrukuj      , MUIA_Disabled, mode);
        set(ST_Mag_Wybierz       , MUIA_Disabled, mode);
        set(BT_Mag_Znajd�        , MUIA_Disabled, mode);
        set(BT_Mag_Filter        , MUIA_Disabled, mode);
        set(BT_Mag_FilterAll     , MUIA_Disabled, mode);
        }


}

//|
/// Znajd�Grup�ID
struct GrupaList *Znajd�Grup�ID(char ID)
{
/* Wyszukuj� grup� o podanym ID
** Zwraca GrupaList *, albo NULL,
** je�li brak takiej grupy
*/

struct GrupaList *node;

        if(!IsListEmpty(&grupy))
           {
           for(node = (struct GrupaList *)grupy.lh_Head; node->gm_node.mln_Succ; node = (struct GrupaList *)node->gm_node.mln_Succ)
           if(node->gm_grupa.ID == ID)
                   return(node);
           }

        return(NULL);
}
//|
/// Znajd�Grup�Nazwa
struct GrupaList *Znajd�Grup�Nazwa(char *Nazwa)
{
/* Wyszukuj� grup� o podanej nazwie
** Zwraca GrupaList *, albo NULL,
** je�li brak takiej grupy
*/

struct GrupaList *node;

        if(!IsListEmpty(&grupy))
           {
           for(node = (struct GrupaList *)grupy.lh_Head; node->gm_node.mln_Succ; node = (struct GrupaList *)node->gm_node.mln_Succ)
//       if(!StrnCmp(MyLocale, Nazwa, node->gm_grupa.Nazwa, -1, SC_COLLATE2))
           if(!stricmp(Nazwa, node->gm_grupa.Nazwa))
                   return(node);
           }

        return(NULL);
}
//|
/// WybierzIDGrupy

short WybierzIDGrupy(void)
{
/*
** Szuka wolnego ID grupy magazynowej
** zwraca -1 - jesli brak, albo wolny numer
*/

short ID = -1;
int  i;

        for(i=0; i<256; i++)
           {
           if(IDGrup[i] == 0)
                   {
                   ID = i;
                   break;
                   }
           }

        return(ID);

}

//|
/// DodajGrup�

int DodajGrup�(struct GrupaList *grupa, char NewGroup)
{
/*
** Dodaj� grup� do listy
** Alokuje jej ID w tablicy IDGrup
*/

struct GrupaList *node;
int i = 0;

        /* Alokujemy ID grupy i zerujemu list� produkt�w        */
        /* ale tylko dla nowo dodawanych grup. Przy grupach     */
        /* istniej�cych �apki precz, coby nie straci�produkt�w */


        if(NewGroup)
           {
           NewList(&grupa->produkty);
           IDGrup[grupa->gm_grupa.ID] = TRUE;
           }


        if(!IsListEmpty(&grupy))
           {
           for(node = (struct GrupaList *)grupy.lh_Head; node->gm_node.mln_Succ; node = (struct GrupaList *)node->gm_node.mln_Succ, i++)
                   {
                   if(StrnCmp(MyLocale, grupa->gm_grupa.Nazwa, node->gm_grupa.Nazwa, -1, SC_COLLATE2) < 0)
                           {
                           if((struct Node *)node->gm_node.mln_Pred)
                                   Insert(&grupy, (struct Node *)grupa, (struct Node *)node->gm_node.mln_Pred);
                           else
                                   AddHead(&grupy, (struct Node *)grupa);

                           return(i);
                           }
                   }
           }

        AddTail(&grupy, (struct Node *)grupa);

        return(MUIV_List_Insert_Bottom);
}
//|
/// Usu�Grup�
char Usu�Grup�(char ID)
{
/*
** Wyszukuj� i zaznacza jako
** skasowan� grup� o podanym ID
**
** Result: BOOL
*/

struct GrupaList *node, *next;

        if(!IsListEmpty(&grupy))
           {
           if(node = Znajd�Grup�ID(ID))
                 {
                 if(node->gm_grupa.ID == ID)
                         {
                         node->Deleted = TRUE;
                         return(TRUE);
                         }
                 }
           else
                 {
                 DisplayBeep(0);
                 }
           }

        return(FALSE);

}
//|

/// InitMagazyn
struct GrupaList *InitMagazyn(void)
{
/*
** Inicjalizuj� listy oraz struktury magazynowe
** Dodaje podstawowe grupy
** Nie usuwa �adnych element�w!!
*/

//struct GrupaList *grupa = calloc(1, sizeof(struct GrupaList));

        NewList(&grupy);

        memset(IDGrup, 0, sizeof(IDGrup));

//    return(grupa);
        return(0);

}
//|
/// Usu�Magazyn
void Usu�Magazyn(void)
{
/*
** Usuwa wszystkie grupy magazynowe
** oraz znajdujace si� w nich produkty
*/

struct GrupaList   *grupa  , *gr_next;
struct ProductList *produkt, *pr_next;

        grupa_aktywna = NULL;

        if(!IsListEmpty(&grupy))
           {
           for(grupa = (struct GrupaList *)grupy.lh_Head; grupa->gm_node.mln_Succ; )
                   {

                   /* Kasujemy wszystkie produkty grupy */

                   if(!IsListEmpty(&grupa->produkty))
                           {
                           for(produkt = (struct ProductList *)grupa->produkty.lh_Head; produkt->pl_node.mln_Succ; )
                                   {
                                   Remove((struct Node*)produkt);
                                   pr_next = (struct ProductList *)produkt->pl_node.mln_Succ;
                                   free(produkt);
                                   produkt = pr_next;
                                   }
                           }

                   /* oraz sam� grup� */

                   Remove((struct Node*)grupa);
                   gr_next = (struct GrupaList *)grupa->gm_node.mln_Succ;
                   free(grupa);
                   grupa = gr_next;

                   }

           memset(IDGrup, 0, sizeof(IDGrup));
           }
}
//|
/// Usu�SkasowaneElementyMagazynowe
void Usu�SkasowaneElementyMagazynowe(void)
{
/*
** Usuwa wszystkie skasowane produkty
** oraz grupy magazynowe zaznaczone jako
** skasowane (oraz ich wszystkie produkty)
*/

struct GrupaList *grupa  , *gr_next;
struct ProductList         *produkt, *pr_next;

        if(!IsListEmpty(&grupy))
           {
           for(grupa = (struct GrupaList *)grupy.lh_Head; grupa->gm_node.mln_Succ; )
                   {
                   if(grupa->Deleted == TRUE)
                           {
                           /* Kasujemy wszystkie produkty grupy */

                           if(!IsListEmpty(&grupa->produkty))
                                  {
                                  for(produkt = (struct ProductList *)grupa->produkty.lh_Head; produkt->pl_node.mln_Succ; )
                                         {
                                         pr_next = (struct ProductList *)produkt->pl_node.mln_Succ;
                                         Remove((struct Node*)produkt);
                                         free(produkt);
                                         produkt = pr_next;
                                         }
                                  }

                           /* oraz sam� grup� */

                           IDGrup[grupa->gm_grupa.ID] = FALSE;

                           gr_next = (struct GrupaList *)grupa->gm_node.mln_Succ;
                           Remove((struct Node*)grupa);
                           free(grupa);
                           grupa = gr_next;
                           }
                        else
                           {
                           /* Kasujemy tylko skasowane produkty grupy */

                           if(!IsListEmpty(&grupa->produkty))
                                  {
                                  for(produkt = (struct ProductList *)grupa->produkty.lh_Head; produkt->pl_node.mln_Succ; )
                                         {
                                         pr_next = (struct ProductList *)produkt->pl_node.mln_Succ;
                                         if(produkt->Deleted == TRUE)
                                           {
                                           Remove((struct Node*)produkt);
                                           free(produkt);
                                           grupa->Ilo��--;
                                           }
                                         produkt = pr_next;
                                         }
                                  }

                           grupa = (struct GrupaList *)grupa->gm_node.mln_Succ;
                           }
                   }
           }

}
//|

/// DodajProdukt

int DodajProdukt( struct ProductList *prod, char Quiet )
{
/*
** Dodaje produkt to listy produkt�w
** aktywnej grupy
** jesli QUIET == TRUE, nie rusza licznika grup
*/

struct ProductList *node;
struct List *produkty;
int i = 0;

        if(grupa_aktywna)
           {
           produkty = &grupa_aktywna->produkty;
           }
        else
           {
           DisplayBeep(0);
           MUI_Request(app, MagazynWindow, 0, TITLE, MSG_OK, MSG_MAG_NO_GROUP_ERROR);
           return(0);
           }

        if( !Quiet)
                grupa_aktywna->Ilo��++;

        if(!IsListEmpty(produkty))
           {
           for(node = (struct ProductList *)produkty->lh_Head; node->pl_node.mln_Succ; node = (struct ProductList *)node->pl_node.mln_Succ)
                   {
                   if(StrnCmp(MyLocale, prod->pl_prod.Nazwa, node->pl_prod.Nazwa, -1, SC_COLLATE2) < 0)
                           {
                           if((struct Node *)node->pl_node.mln_Pred)
                                   Insert(produkty, (struct Node *)prod, (struct Node *)node->pl_node.mln_Pred);
                           else
                                   AddHead(produkty, (struct Node *)prod);

                           return(i);
                           }
                   i++;
                   }
           }

           AddTail(produkty, (struct Node *)prod);

           return(MUIV_List_Insert_Bottom);
}
//|
/// Znajd�ProduktIndexWMagazynie
struct ProductList *Znajd�ProduktIndexWMagazynie(char *Index)
{
/*
** Przeszukuj� ca�y magazyn w poszukiwaniu produktu o podanym
** indeksie
*/

struct GrupaList   *grupa;
struct ProductList *produkt;

D(bug("index: %s\n", Index));

        if(!IsListEmpty(&grupy))
           {
           for(grupa = (struct GrupaList *)grupy.lh_Head; grupa->gm_node.mln_Succ; grupa = (struct GrupaList *)grupa->gm_node.mln_Succ)
                   {
                   if(!IsListEmpty(&grupa->produkty))
                           {
                           for(produkt = (struct ProductList *)grupa->produkty.lh_Head; produkt->pl_node.mln_Succ; produkt = (struct ProductList *)produkt->pl_node.mln_Succ)
                                   {
//                   if( StrnCmp(MyLocale, Index, produkt->pl_prod.Index, -1, SC_COLLATE2) == 0)
                                   if( stricmp(Index, produkt->pl_prod.Index) == 0)
                                          return(produkt);
                                   }
                           }
                   }
           }

        return(NULL);
}
//|
/// Znajd�ProduktNazwaWMagazynie
struct ProductList *Znajd�ProduktNazwaWMagazynie(char *Nazwa)
{
/*
** Przeszukuj� ca�u magazyn produktu o podanej nazwie
*/

struct GrupaList   *grupa;
struct ProductList *produkt;

D(bug("nazzwa: %s\n", Nazwa));

        if(!IsListEmpty(&grupy))
           {
           for(grupa = (struct GrupaList *)grupy.lh_Head; grupa->gm_node.mln_Succ; grupa = (struct GrupaList *)grupa->gm_node.mln_Succ)
                   {
                   if(!IsListEmpty(&grupa->produkty))
                           {
                           for(produkt = (struct ProductList *)grupa->produkty.lh_Head; produkt->pl_node.mln_Succ; produkt = (struct ProductList *)produkt->pl_node.mln_Succ)
                                   {
//                   if(!StrnCmp(MyLocale, Nazwa, produkt->pl_prod.Nazwa, -1, SC_COLLATE2))
                                   if(!stricmp(Nazwa, produkt->pl_prod.Nazwa))
                                          return(produkt);
                                   }
                           }
                   }
           }

        return(NULL);
}
//|
/// Znajd�ProduktIndex
struct ProductList *Znajd�ProduktIndex(char *Index)
{
/*
** Przeszukuj� aktywn� grup� w poszukiwaniu produktu
** o podanym indexie
*/

struct ProductList *node;
struct List *produkty = &grupa_aktywna->produkty;

        if(!IsListEmpty(produkty))
           {
           for(node = (struct ProductList *)produkty->lh_Head; node->pl_node.mln_Succ; node = (struct ProductList *)node->pl_node.mln_Succ)
//       if(!StrnCmp(MyLocale, Nazwa, node->pl_prod.Nazwa, -1, SC_COLLATE2))
           if(!stricmp(Index, node->pl_prod.Index))
                   return(node);
           }

        return(NULL);
}
//|
/// Znajd�ProduktNazwa
struct ProductList *Znajd�ProduktNazwa(char *Nazwa)
{
/*
** Przeszukuj� aktywn� grup� w poszukiwaniu produktu
** o podanej nazwie
*/

struct ProductList *node;
struct List *produkty = &grupa_aktywna->produkty;

        if(!IsListEmpty(produkty))
           {
           for(node = (struct ProductList *)produkty->lh_Head; node->pl_node.mln_Succ; node = (struct ProductList *)node->pl_node.mln_Succ)
//       if(!StrnCmp(MyLocale, Nazwa, node->pl_prod.Nazwa, -1, SC_COLLATE2))
           if(!stricmp(Nazwa, node->pl_prod.Nazwa))
                   return(node);
           }

        return(NULL);
}
//|
/// Znajd�ProduktListaAdres
int Znajd�ProduktListaAdres(Object *lv, struct ProductList *prod)
{
/*
** Przeszukuj� podana liste (Object *lv) w poszukiwaniu produktu
** o podanym adresie. Zwraca pozycje na ktorej znaleziono produkt.
** albo MUIV_List_Active_Off (-1)
*/

struct ProductList *node;
int result = MUIV_List_Active_Off;
int i;

        for(i=0 ;; i++)
           {
           DoMethod(lv, _MUIM_List_GetEntry, i, &node);

           if( !node )
                   break;

           if( node == prod )
                   {
                   result = i;
                   break;
                   }
           }

        return(result);
}
//|
/// Znajd�ProduktNaLi�cieIndex
long Znajd�ProduktNaLi�cieIndex(char *str, Object *list, char StartPos)
{
/*
** szuka produktu o podanym indeksie na podanej li�cie (Object *)
** pocz�wszy od zadanej pozycji
**
** je�li startowa pozycja wi�ksza ni� lista
** element�w na li�cie, zaczyna szuka� od
** pocz�tku listy
**
** Zwraca numer pozycji na kt�rej znaleziono produkt,
** lub -1 je�li brak
*/

static char pattern[(PROD_INDEX_LEN*2)+2 +4];
static char string[PROD_INDEX_LEN];
long   result = -1;


        if(xget(list, _MUIA_List_Entries) > 0)
           {
           sprintf(string, "#?%s#?", str);
           if(ParsePatternNoCase(string, pattern, sizeof(pattern)) == 1)
                  {
                  struct ProductList *prod;
                  int i = StartPos;

                  if(xget(list, _MUIA_List_Entries) < StartPos+1)
                         i = 0;

                  for(;; i++)
                         {
                         DoMethod(list, _MUIM_List_GetEntry, i, &prod);
                         if(!prod)
                           break;

                         if(MatchPatternNoCase(pattern, prod->pl_prod.Index))
                           {
                           set(list, _MUIA_List_Active, i);
                           result = i;
                           break;
                           }
                         }
                  }
           else
                  {
                  D(bug("ParsePatternNoCase() failed\n"));
                  DisplayBeep(0);
                  }
           }

        return(result);

}
//|
/// Znajd�ProduktNaLi�cie
long Znajd�ProduktNaLi�cie(char *str, Object *list, char StartPos)
{
/*
** szuka produktu o podanej nazwie na podanej li�cie (Object *)
** pocz�wszy od zadanej pozycji
**
** je�li startowa pozycja wi�ksza ni� lista
** element�w na li�cie, zaczyna szuka� od
** pocz�tku listy
**
** Zwraca numer pozycji na kt�rej znaleziono produkt,
** lub -1 je�li brak
*/

static char pattern[(PROD_NAME_LEN*2)+2 +4];
static char string[PROD_NAME_LEN];
long   result = -1;


        if(xget(list, _MUIA_List_Entries) > 0)
           {
           sprintf(string, "#?%s#?", str);
           if(ParsePatternNoCase(string, pattern, sizeof(pattern)) == 1)
                  {
                  struct ProductList *prod;
                  int i = StartPos;

                  if(xget(list, _MUIA_List_Entries) < StartPos+1)
                         i = 0;

                  for(;; i++)
                         {
                         DoMethod(list, _MUIM_List_GetEntry, i, &prod);
                         if(!prod)
                           break;

                         if(MatchPatternNoCase(pattern, prod->pl_prod.Nazwa))
                           {
                           set(list, _MUIA_List_Active, i);
                           result = i;
                           break;
                           }
                         }
                  }
           else
                  {
                  D(bug("ParsePatternNoCase() failed\n"));
                  DisplayBeep(0);
                  }
           }

        return(result);

}
//|


/// SprawdzLimityMagazynowe

enum { LIMIT_OFF=0, LIMIT_MIN, LIMIT_MAX };

ULONG SprawdzLimityMagazynowe( char mode )
{
/*
** Przeszukuj� ca�u magazyn i sprawdza czy zaden z elementow
** nie przekracza podanych limitow magazynowych.
** mode - jakie limity maja byc sprawdzane LIMIT_xxx
**
** zwraca liczne znalezionych elementow
*/

struct GrupaList   *grupa;
struct ProductList *produkt;
ULONG  count = 0;


        if( !IsListEmpty( &grupy ) )
           {
           for( grupa = (struct GrupaList *)grupy.lh_Head; grupa->gm_node.mln_Succ; grupa = (struct GrupaList *)grupa->gm_node.mln_Succ )
                   {
                   if( !IsListEmpty( &grupa->produkty ) )
                           {
                           for( produkt = (struct ProductList *)grupa->produkty.lh_Head; produkt->pl_node.mln_Succ; produkt = (struct ProductList *)produkt->pl_node.mln_Succ )
                                {
                                switch( mode )
                                        {
                                        case LIMIT_MIN:
                                                if( produkt->pl_prod.MinEn )
                                                        if( produkt->pl_prod.Ilo�� < produkt->pl_prod.StanMin )
                                                                {
                                                                count++;
                                                                }
                                                break;
                                        
                                        case LIMIT_MAX:
                                                if( produkt->pl_prod.MaxEn )
                                                        if( produkt->pl_prod.Ilo�� > produkt->pl_prod.StanMax )
                                                                {
                                                                count++;
                                                                }
                                                break;
                                        }
                                }
                           }
                   }
           }

        return(NULL);
}
//|

/// MAGAZYN SCAN STRINGS

#define MSG_MAGSC_WIN_TITLE  "Przeszukiwanie magazynu"

#define MSG_MAGSC_MODE       "Szukaj _wg."
#define MSG_MAGSC_PAGE1      "Nazwa"
#define MSG_MAGSC_PAGE2      "Indeks"

#define MSG_MAGSC_FIND       "F7 - _Szukaj"
#define  SH_MAGSC_FIND       "Wpisz wzorzec wyszukiwania, ci�g znak�w lub\nframent nazwy/indeksu towaru lub us�ugi,\nkt�r� chcesz odszuka�.\n\nW wyszukiwarce mo�esz stosowa� wszystkie\nznane z AmigaDOSu zamienniki (np. #?,\n[prze-dzia�y], etc.)."

#define MSG_MAGSC_OK         "F10 - _Ok"
#define MSG_MAGSC_CANCEL     "ESC - Ponie_chaj"

#define MSG_MAGSC_ERR_PATT   "\033cB��d podczas przetwarzania wzorca!\nSprawd� poprawno�� zapisu!"


//|
/// MagazynScanSetup


static Object
           *MagazynScanWindow,         /* EDYCJA PRODUKTU */
           *LV_MagScan_List,

           *CY_MagScan_Mode,
           *ST_MagScan_Pattern,

           *BT_MagScan_Find,

           *BT_MagScan_Ok,
           *BT_MagScan_Cancel;


char *CY_MagScan_Ed_Titles[] = {MSG_MAGSC_PAGE1,
                                                                MSG_MAGSC_PAGE2,
                                                                NULL};


char MagazynScanSetup(char *pattern, char IndexScan)
{

///   Create MagazynScanWindow

//                 MUIA_Application_Window,
                                         MagazynScanWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_MAGSC_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_MAGSCAN,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,

                                                   Child, VGroup,
                                                                  GroupFrame,
                                                                  Child, LV_MagScan_List = _ListviewObject,
                                                                                 MUIA_CycleChain, TRUE,
                                                                                 _MUIA_Listview_List, NewObject(CL_ProductList->mcc_Class, NULL,
/*
                                                                                 _MUIA_List_Format, "MIW=1 MAW=65 COL=1 BAR,"
                                                                                                                        "MIW=1 MAW=10 COL=2 BAR,"
                                                                                                                        "MIW=1 MAW=-1 COL=3 P=\033r BAR,"
                                                                                                                        "MIW=1 MAW=-1 COL=6 P=\033r",
*/
                                                                                                                        TAG_DONE),
                                                                  End,

                                                                  Child, HGroup,
                                                                  				 _SH( SH_MAGSC_FIND ),
                                                                                 Child, MakeLabel2(MSG_MAGSC_MODE),
                                                                                 Child, CY_MagScan_Mode    = MakeCycleWeight(CY_MagScan_Ed_Titles, MSG_MAGSC_MODE, 1),
                                                                                 Child, ST_MagScan_Pattern = MakeString(128, NULL),
                                                                                 Child, BT_MagScan_Find    = TextButtonWeight(MSG_MAGSC_FIND, 1),
                                                                                 End,
                                                                  End,

                                                   Child, HGroup,
                                                                  Child, BT_MagScan_Ok     = TextButton(MSG_MAGSC_OK),
                                                                  Child, BT_MagScan_Cancel = TextButton(MSG_MAGSC_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|

        if(!MagazynScanWindow)
           return(FALSE);


        // notifications

        DoMethod(MagazynScanWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(MagazynScanWindow , MUIM_Notify, MUIA_Window_InputEvent, "f7", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FIND);
        DoMethod(MagazynScanWindow , MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

        DoMethod(BT_MagScan_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_MagScan_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(BT_MagScan_Find   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FIND);

//    DoMethod(MagazynScanWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_0);

        DoMethod(LV_MagScan_List   , MUIM_Notify, _MUIA_Listview_DoubleClick, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);


        // contents

        setstring(ST_MagScan_Pattern, pattern);
        setcycle(CY_MagScan_Mode    , IndexScan);

        set(MagazynScanWindow, MUIA_Window_ActiveObject, ST_MagScan_Pattern);
//    set(MagazynScanWindow, MUIA_Window_ActiveObject, BT_MagScan_Find);

        _disable(BT_MagScan_Ok);

        // attach

        _attachwin(MagazynScanWindow);

        return(TRUE);
}
//|
/// MagazynScan

struct MagScan *MagazynScan(char *pattern, char IndexScan)
{
/*
** Przeszukuje magazyn w poszukiwaniu produktu pasujacego
** do podanego wzorca. jesli INDEXMODE==TRUE, przeszukuje
** wg. pola INDEX, w przeciwnym razie szuka po nazwie.
**
** Je�li EditMode == TRUE, to edytujemy
** istniej�cy ju� produkt
*/

char  running = TRUE;
ULONG signal  = 0;

static struct MagScan ms = {0};
struct MagScan *result  = NULL;


        _sleep(TRUE);

        if(!MagazynScanSetup(pattern, IndexScan))
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(FALSE);
           }


        MakeSmartList(LV_MagScan_List, FALSE, FALSE);

        if(WinOpen(MagazynScanWindow))
                {
                while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);

                  switch(ID)
                        {
                        case ID_FIND:
                           {
                           struct GrupaList   *grupa;
                           struct ProductList *produkt;

                           char   string[128 + 10];
                           char   FullPattern[256 + 2];
                           int    found = 0;


                           if(*(getstr(ST_MagScan_Pattern)) == 0)
                                   {
                                   set(MagazynScanWindow, MUIA_Window_ActiveObject, ST_MagScan_Pattern);
                                   break;
                                   }

                           _sleep(TRUE);


                           if(!IsListEmpty(&grupy))
                                  {
                                  sprintf(string, "%s#?" , getstr(ST_MagScan_Pattern));

                                  if(ParsePatternNoCase(string, FullPattern, sizeof(FullPattern)) == -1)
                                        {
                                        DisplayBeep(0);
                                        MUI_Request(app, MagazynScanWindow, 0, TITLE, MSG_OK, MSG_MAGSC_ERR_PATT);
                                        _sleep(FALSE);
                                        break;
                                        }

                                  set(LV_MagScan_List, _MUIA_List_Quiet, TRUE);

                                  DoMethod(LV_MagScan_List, _MUIM_List_Clear);


                                  if( getcycle(CY_MagScan_Mode) == 0)
                                         {
                                         for(grupa = (struct GrupaList *)grupy.lh_Head; grupa->gm_node.mln_Succ; grupa = (struct GrupaList *)grupa->gm_node.mln_Succ)
                                           {
                                           if(!IsListEmpty(&grupa->produkty))
                                                   {
                                                   for(produkt = (struct ProductList *)grupa->produkty.lh_Head; produkt->pl_node.mln_Succ; produkt = (struct ProductList *)produkt->pl_node.mln_Succ)
                                                           {
                                                           if( MatchPatternNoCase(FullPattern, produkt->pl_prod.Nazwa) != 0)
                                                                   {
                                                                   DoMethod(LV_MagScan_List, _MUIM_List_InsertSingle, produkt, _MUIV_List_Insert_Sorted);
                                                                   found++;
                                                                   }
                                                           }
                                                   }
                                           }
                                         }
                                  else
                                         {
                                         for(grupa = (struct GrupaList *)grupy.lh_Head; grupa->gm_node.mln_Succ; grupa = (struct GrupaList *)grupa->gm_node.mln_Succ)
                                           {
                                           if(!IsListEmpty(&grupa->produkty))
                                                   {
                                                   for(produkt = (struct ProductList *)grupa->produkty.lh_Head; produkt->pl_node.mln_Succ; produkt = (struct ProductList *)produkt->pl_node.mln_Succ)
                                                           {
                                                           D(bug("-> %s\n", produkt->pl_prod.Index));
                                                           if( MatchPatternNoCase(FullPattern, produkt->pl_prod.Index) != 0)
                                                                   {
                                                                   DoMethod(LV_MagScan_List, _MUIM_List_InsertSingle, produkt, _MUIV_List_Insert_Sorted);
                                                                   found++;
                                                                   }
                                                           }
                                                   }
                                           }
                                         }

                                  if(found > 0)
                                         {
                                         MakeSmartList(LV_MagScan_List, FALSE, FALSE);
                                         set(MagazynScanWindow, MUIA_Window_ActiveObject, LV_MagScan_List);
                                         set(LV_MagScan_List, _MUIA_List_Active, _MUIV_List_Active_Top);
                                         _enable(BT_MagScan_Ok);
                                         }
                                  else
                                         {
                                         set(MagazynScanWindow, MUIA_Window_ActiveObject, ST_MagScan_Pattern);
                                         _disable(BT_MagScan_Ok);
                                         }

                                  set(LV_MagScan_List, _MUIA_List_Quiet, FALSE);
                                  }
                           else
                                  {
                                  DisplayBeep(0);
                                  D(bug("Magazyn jest pusty!\n"));
                                  }


                           _sleep(FALSE);
                           }
                           break;




                        case ID_OK:
                           {
                           DoMethod(LV_MagScan_List, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &ms.prod);

                           if( ms.prod == NULL )
                                   {
                                   ms.grupa = NULL;
                                   }
                           else
                                   {
                                   ms.grupa = Znajd�Grup�ID(ms.prod->pl_prod.Grupa);
                                   }

                           // jesli nic nie wybrano, to zostawiamy OK=CANCEL
                           if(ms.grupa)
                                   result = &ms;

                           running = FALSE;
                           }
                           break;

                        case ID_CANCEL:
                           result  = NULL;
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

                set(MagazynScanWindow, MUIA_Window_Open, FALSE);
                }
         else
                {
                DisplayBeep(0);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


        _detachwin(MagazynScanWindow);
        _sleep(FALSE);

        return(result);
}

//|


/// EDYCJA GRUPY STRINGS

#define MSG_GROUP_WIN_TITLE "Edycja grupy magazynowej"

#define MSG_GROUP_TYP       "_Typ"
#define MSG_GROUP_T0        "Towary"
#define MSG_GROUP_T1        "Us�ugi"
#define MSG_GROUP_DEF_TITLE "Ustawienia domy�lne"
#define MSG_GROUP_NAME      "_Nazwa"
#define MSG_GROUP_JEDN      "_Jedn. miary"
#define MSG_GROUP_VAT       "Stawka _VAT"

#define MSG_GROUP_OK        "F10 - _Ok"
#define MSG_GROUP_DODAJ     "_Tylko dodaj"
#define MSG_GROUP_CANCEL    "ESC - Ponie_chaj"



#define MSG_GROUP_MAIN_NAME "Grupa g��wna"

//|
/// EdycjaGrupySetup


static Object *EdycjaGrupyWindow,            /* EDYCJA GRUPY               */
           *ST_Grupa_Nazwa,
           *CY_Grupa_Typ,
           *TX_Grupa_Jedn,
           *BT_Grupa_Wybierz,
           *CY_Grupa_Vat,

           *BT_Grupa_Ok,
           *BT_Grupa_Dodaj,
           *BT_Grupa_Cancel;

static char *GroupTypeArray[] = {MSG_GROUP_T0,
                                                                 MSG_GROUP_T1,
                                                                 NULL
                                                                };

char EdycjaGrupySetup(struct GrupaList *Grupa, char EditMode)
{
struct Grupa *grupa = &Grupa->gm_grupa;

///   Create EdycjaGrupyWindow

//                 MUIA_Application_Window,
                                         EdycjaGrupyWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_GROUP_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_GRUPA,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,

                                                   Child, ColGroup(2),
                                                                  GroupFrame, GroupBack,

                                                                  Child, MakeLabel2(MSG_GROUP_NAME),
                                                                  Child, ST_Grupa_Nazwa = MakeString(GROUP_NAME_LEN, MSG_GROUP_NAME),

                                                                  Child, MakeLabel2(MSG_GROUP_TYP),
                                                                  Child, CY_Grupa_Typ = MakeCycle(GroupTypeArray, MSG_GROUP_TYP),

                                                                  End,

                                                   Child, ColGroup(2),
                                                                  GroupFrameT(MSG_GROUP_DEF_TITLE),

                                                                  Child, MakeLabel2(MSG_GROUP_JEDN),
                                                                  Child, HGroup,
                                                                                 MUIA_Group_Spacing, 1,
                                                                                 Child, TX_Grupa_Jedn = TextObject, TextBack, TextFrame, End,
                                                                                 Child, BT_Grupa_Wybierz = PopButton2(MUII_PopUp, MSG_GROUP_JEDN),
                                                                                 End,

                                                                  Child, MakeLabel2(MSG_GROUP_VAT),
                                                                  Child, CY_Grupa_Vat = MakeCycle(VatTable, MSG_GROUP_VAT),

                                                                  End,

                                                   Child, HGroup,
                                                                  Child, BT_Grupa_Ok     = TextButton(MSG_GROUP_OK),
                                                                  Child, BT_Grupa_Dodaj  = TextButton(MSG_GROUP_DODAJ),
                                                                  Child, BT_Grupa_Cancel = TextButton(MSG_GROUP_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|


        if(!EdycjaGrupyWindow)
           return(FALSE);

        // notyfikacje

        DoMethod(EdycjaGrupyWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(EdycjaGrupyWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

        DoMethod(BT_Grupa_Wybierz , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT);
        DoMethod(BT_Grupa_Ok      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Grupa_Dodaj   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);
        DoMethod(BT_Grupa_Cancel  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);



        // values

        setstring(ST_Grupa_Nazwa, grupa->Nazwa);
        setcycle(CY_Grupa_Typ   , grupa->Typ);
        settext(TX_Grupa_Jedn   , grupa->Jedn.Nazwa);
        setcycle(CY_Grupa_Vat   , grupa->Vat);

        set(EdycjaGrupyWindow, MUIA_Window_ActiveObject, ST_Grupa_Nazwa);
        set(BT_Grupa_Dodaj, MUIA_Disabled, EditMode);


        // attach

        _attachwin(EdycjaGrupyWindow);


        return(TRUE);
}
//|
/// EdycjaGrupyFinish
char EdycjaGrupyFinish(struct GrupaList *Grupa, char EditMode)
{
struct Grupa *grupa = &Grupa->gm_grupa;

        if(!(strlen((char *)xget(ST_Grupa_Nazwa, MUIA_String_Contents))) )
           {
           set(EdycjaGrupyWindow, MUIA_Window_ActiveObject, ST_Grupa_Nazwa);
           return(FALSE);
           }

        if( grupa->Jedn.Nazwa[0] == 0 )
           {
           set(EdycjaGrupyWindow, MUIA_Window_ActiveObject, BT_Grupa_Wybierz);
           return(FALSE);
           }



        /* sprawdzamy czy nazwa grupy jest unikalna */
        {
        struct GrupaList *tmp;

        if(EditMode)
          {
          Remove((struct Node *)Grupa);
          tmp = Znajd�Grup�Nazwa((char *)xget(ST_Grupa_Nazwa, MUIA_String_Contents));
          DodajGrup�(Grupa, FALSE);
          }
        else
          {
          tmp = Znajd�Grup�Nazwa((char *)xget(ST_Grupa_Nazwa, MUIA_String_Contents));
          }

        if(tmp)
           {
           set(EdycjaGrupyWindow, MUIA_Window_ActiveObject, ST_Grupa_Nazwa);
           return(FALSE);
           }
        }

        strcpy(grupa->Nazwa    , (char *)xget(ST_Grupa_Nazwa, MUIA_String_Contents));

        grupa->Vat = getcycle(CY_Grupa_Vat);
        grupa->Typ = getcycle(CY_Grupa_Typ);

        return(TRUE);

}
//|
/// EdycjaGrupy
char EdycjaGrupy(struct GrupaList *grupa, char EditMode)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

struct Grupa gr;               // kopia na cancel;


        set(app, MUIA_Application_Sleep, TRUE);

        memcpy(&gr, &grupa->gm_grupa, sizeof(struct Grupa));

        if(!EdycjaGrupySetup(grupa, EditMode))
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           }

/*
        set(MagazynWindow, MUIA_Window_ActiveObject , LV_Mag_Produkty);
        set(MagazynWindow, MUIA_Window_DefaultObject, LV_Mag_Produkty);
*/

        if(WinOpen(EdycjaGrupyWindow))
                {
                while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);
                  switch(ID)
                        {
                        case ID_SELECT:
                           {
                           struct JednostkaList *jedn = JednostkaSelector(&grupa->gm_grupa.Jedn);

                           if(jedn)
                                   {
                                   memcpy(&grupa->gm_grupa.Jedn, &jedn->ul_jedn, sizeof(struct Jednostka));
                                   settext(TX_Grupa_Jedn, grupa->gm_grupa.Jedn.Nazwa);
                                   }
                           }
                           break;

                        case ID_OK:
                        case ID_ADD:
                           {
                           if(EdycjaGrupyFinish(grupa, EditMode))
                                   {
                                   result  = TRUE;
                                   running = FALSE;
                                   }
                           else
                                   {
                                   DisplayBeep(0);
                                   }

                           if(ID == ID_ADD)
                                   result = ADD_ONLY;

                           }
                           break;


                        case ID_CANCEL:
                           memcpy(&grupa->gm_grupa, &gr, sizeof(struct Grupa));
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

                set(EdycjaGrupyWindow, MUIA_Window_Open, FALSE);
                }
        else
          {
          DisplayBeep(0);
          MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
          }


        _detachwin(EdycjaGrupyWindow);


        set(app, MUIA_Application_Sleep, FALSE);

        return(result);

}
//|


/// MAG STOP CHUNKS
#define MAG_NUM_STOPS (sizeof(Mag_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Mag_Stops[] =
{
                ID_MAG, ID_CAT,
                ID_MAG, ID_VERS,
                ID_MAG, ID_GRUP,
                ID_MAG, ID_PROD,
                NULL, NULL,
};
//|


/// WczytajMagazyn

int WczytajMagazyn(void)
{

struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
char   ValidFile = FALSE;
int    Current = 0;                   // liczba wczytanych produkt�w
int    CurrentGauge = 1;

int    Errors = 0;
int    Error_Products = 0;            // produkty do kt�rych nie znaleziono grup
int    Error_Skipped_Groups = 0;      // grupy kt�re nie zosta�y wczytane
int    Error_Skipped_Products = 0;    // produkty kt�re nie zosta�y wczytane
int    Error_Duplicated_Groups = 0;   // powtarzaj�ce si� definicje grup
int    Dodane_Jednostki = 0;          // liczba przyimportowanych jednostek z magazynu


         set(app, MUIA_Application_Sleep, TRUE);

         set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_STOCK);
         set(GA_Startup_Info, MUIA_Gauge_Max, WczytajCount(MagazynCountFileName)/10);
         set(GA_Startup_Info, MUIA_Gauge_Current, 0);

         Usu�Magazyn();

         if(iff = AllocIFF())
           {
           if(iff->iff_Stream = Open(MagazynFileName, MODE_OLDFILE))
                   {
                   InitIFFasDOS(iff);

                   StopChunks(iff, Mag_Stops, MAG_NUM_STOPS);
//           StopOnExit(iff, ID_MAG, ID_FORM);

                   if(!OpenIFF(iff, IFFF_READ))
                           {
                           while(TRUE)
                                  {
                                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                                         break;

                                  if(cn = CurrentChunk(iff))
                                         {
                                         LONG ID = cn->cn_ID;

                                         if(!ValidFile)
                                                {
                                                if((ID == ID_CAT) && (cn->cn_Type == ID_MAG))
                                                   {
                                                   ValidFile = TRUE;
                                                   continue;
                                                   }

                                                break;
                                                }

///                       ID_VERS
                                         if(ID == ID_VERS)
                                                {
                                                struct BaseVersion version;

                                                if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                                                   {

                                                   }
                                                else
                                                   {
                                                   _RC = IoErr();
                                                   Errors++;
                                                   break;
                                                   }

                                                continue;
                                                }
//|
///                       ID_GRUP

                                         if(ID == ID_GRUP)
                                                {
                                                struct GrupaList *grupa = calloc(1, sizeof(struct GrupaList));

                                                if(grupa)
                                                   {
                                                   if(ReadChunkBytes(iff, &grupa->gm_grupa, sizeof(struct Grupa)) != cn->cn_Size)
                                                         {
                                                         Error_Skipped_Groups++;
//                             _RC = IoErr();
                                                         Errors++;
                                                         free(grupa);
                                                         continue;
                                                         }

                                                   if(!(Znajd�Grup�ID(grupa->gm_grupa.ID)))
                                                          {
                                                          DodajGrup�(grupa, TRUE);
                                                          grupa_aktywna = grupa;
                                                          Current++;
                                                          }
                                                   else
                                                          {
                                                          free(grupa);
                                                          Error_Duplicated_Groups++;
                                                          }
                                                   }
                                                continue;
                                                }
//|
///                       ID_PROD

                                         if(ID == ID_PROD)
                                                {
                                                struct ProductList *prod  = calloc(1, sizeof(struct ProductList));

                                                if(prod)
                                                   {
                                                   if(ReadChunkBytes(iff, &prod->pl_prod, sizeof(struct Product)) != cn->cn_Size)
                                                          {
                                                          Error_Skipped_Products++;
                                                          free(prod);
//                              _RC = IoErr();
                                                          Errors++;
                                                          continue;
                                                          }

                                                   if(grupa_aktywna)
                                                         {
                                                         if(prod->pl_prod.Grupa != grupa_aktywna->gm_grupa.ID)
                                                           {
                                                           grupa_aktywna = Znajd�Grup�ID(prod->pl_prod.Grupa);
                                                           }

                                                         if(grupa_aktywna)
                                                           {
                                                           DodajProdukt(prod, FALSE);

                                                           if( !Znajd�Jednostk�Nazwa( prod->pl_prod.Jedn.Nazwa) )
                                                                   {
                                                                   struct JednostkaList *jedn = calloc(1, sizeof(struct JednostkaList));

                                                                   if(jedn)
                                                                         {
                                                                         memcpy( &jedn->ul_jedn, &prod->pl_prod.Jedn, sizeof(struct Jednostka));

                                                                         jedn->Imported = TRUE;
                                                                         DodajJednostk�(jedn);

                                                                         Dodane_Jednostki++;
                                                                         }
                                                                   }
                                                           }
                                                         else
                                                           {
                                                           free(prod);
                                                           Error_Products++;
                                                           }

                                                         Current++;
                                                         }
                                                   else
                                                         {
                                                         free(prod);
                                                         Error_Products++;
                                                         }

                                                   if((Current%10) == 0)
                                                         {
                                                         set(GA_Startup_Info, MUIA_Gauge_Current, CurrentGauge++);
//                             D(bug("%ld\n", CurrentGauge));
                                                         }
                                                   }
                                                continue;
                                                }
//|

                                         }
                                  }

                           CloseIFF(iff);
                           }

                        Close(iff->iff_Stream);
                        }

//       if(_RC == IFFERR_EOF) _RC = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

           FreeIFF(iff);
           }


/*
           if( Dodane_Jednostki > 0 )
                   ZapiszJednostki();
*/


           set(GA_Startup_Info, MUIA_Gauge_Current, 0);
           set(app, MUIA_Application_Sleep, FALSE);


           if(Error_Products + Error_Skipped_Products + Error_Duplicated_Groups)
                   {
                   DisplayBeep(0);
                   MUI_Request(app, NULL, 0, TITLE, MSG_OK, MSG_ERR_LOAD_STOCK, Error_Skipped_Products, Error_Skipped_Groups, Error_Products, Error_Duplicated_Groups, Current);
                   }

           return(Error_Products + Error_Skipped_Products + Error_Duplicated_Groups + Error_Skipped_Groups);

}

//|
/// ZapiszMagazyn

int ZapiszMagazyn(void)
{

struct IFFHandle *MyIFFHandle;
long   Count = 0;

        set(app, MUIA_Application_Sleep, TRUE);

        if(MyIFFHandle = AllocIFF())
                {
                BPTR  FileHandle;

                if(FileHandle = Open(MagazynFileName, MODE_NEWFILE))
                   {
                   MyIFFHandle->iff_Stream = FileHandle;
                   InitIFFasDOS(MyIFFHandle);

                   if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
                           {
                           struct BaseVersion version;
                           struct ProductList *node;
                           struct GrupaList   *grupa;


                           PushChunk(MyIFFHandle, ID_MAG, ID_CAT, IFFSIZE_UNKNOWN);

                           PushChunk(MyIFFHandle, ID_MAG, ID_FORM, IFFSIZE_UNKNOWN);
                                   PushChunk(MyIFFHandle, ID_MAG, ID_VERS, IFFSIZE_UNKNOWN);
                                   version.Version = VERSION;
                                   version.Revision = REVISION;
                                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                                   PopChunk(MyIFFHandle);
/*
                                   PushChunk(MyIFFHandle, ID_MAG, ID_ANNO, IFFSIZE_UNKNOWN);
                                   WriteChunkBytes(MyIFFHandle, ScreenTitle, ScreenTitleLen-1);
                                   WriteChunkBytes(MyIFFHandle, " <", 2);
                                   WriteChunkBytes(MyIFFHandle, EMAIL, strlen(EMAIL));
                                   WriteChunkBytes(MyIFFHandle, ">, ", 3);
                                   WriteChunkBytes(MyIFFHandle, WWW, strlen(WWW));
                                   PopChunk(MyIFFHandle);
*/
                           PopChunk(MyIFFHandle);


                           for(grupa = (struct GrupaList *)grupy.lh_Head; grupa->gm_node.mln_Succ; grupa = (struct GrupaList *)grupa->gm_node.mln_Succ)
                                   {
                                   PushChunk(MyIFFHandle, ID_MAG, ID_FORM, IFFSIZE_UNKNOWN);

                                   PushChunk(MyIFFHandle, ID_MAG, ID_GRUP, IFFSIZE_UNKNOWN);
                                   WriteChunkBytes(MyIFFHandle, &grupa->gm_grupa, sizeof(struct Grupa));
                                   PopChunk(MyIFFHandle);

                                   Count++;

                                   for(node = (struct ProductList *)grupa->produkty.lh_Head; node->pl_node.mln_Succ; node = (struct ProductList *)node->pl_node.mln_Succ)
                                         {
                                         PushChunk(MyIFFHandle, ID_MAG, ID_PROD, IFFSIZE_UNKNOWN);
                                         WriteChunkBytes(MyIFFHandle, &node->pl_prod, sizeof(struct Product));
                                         PopChunk(MyIFFHandle);

                                         Count++;
                                         }

                                   PopChunk(MyIFFHandle);
                                   }

                           PopChunk(MyIFFHandle);
                           CloseIFF(MyIFFHandle);

                           ZapiszCount(MagazynCountFileName, Count);
                           }
                   else
                           {
                           DisplayBeep(0);
                           D(bug("*** OpenIFF() nie powiod�o si�\n"));
                           }

                   Close(FileHandle);
                   }
                else
                   {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
                   D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", MagazynFileName));
                   }

                FreeIFF(MyIFFHandle);
                }
         else
                {
                DisplayBeep(0);
                D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
                }

        set(app, MUIA_Application_Sleep, FALSE);

        return(0);
}



//|

/* nowy - z podzialem na sub-FORMy (Load nie bez zmian jeszcze)
/// WczytajMagazyn

int WczytajMagazyn(void)
{

struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
char   ValidFile = FALSE;
int    Current = 0;                   // liczba wczytanych produkt�w
int    CurrentGauge = 1;

int    Errors = 0;
int    Error_Products = 0;            // produkty do kt�rych nie znaleziono grup
int    Error_Skipped_Groups = 0;      // grupy kt�re nie zosta�y wczytane
int    Error_Skipped_Products = 0;    // produkty kt�re nie zosta�y wczytane
int    Error_Duplicated_Groups = 0;   // powtarzaj�ce si� definicje grup
int    Dodane_Jednostki = 0;          // liczba przyimportowanych jednostek z magazynu


         set(app, MUIA_Application_Sleep, TRUE);

         set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_STOCK);
         set(GA_Startup_Info, MUIA_Gauge_Max, WczytajCount(MagazynCountFileName)/10);
         set(GA_Startup_Info, MUIA_Gauge_Current, 0);

         Usu�Magazyn();

         if(iff = AllocIFF())
           {
           if(iff->iff_Stream = Open(MagazynFileName, MODE_OLDFILE))
                   {
                   InitIFFasDOS(iff);

                   StopChunks(iff, Mag_Stops, MAG_NUM_STOPS);
//           StopOnExit(iff, ID_MAG, ID_FORM);

                   if(!OpenIFF(iff, IFFF_READ))
                           {
                           while(TRUE)
                                  {
                                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                                         break;

                                  if(cn = CurrentChunk(iff))
                                         {
                                         LONG ID = cn->cn_ID;

                                         if(!ValidFile)
                                                {
                                                if((ID == ID_CAT) && (cn->cn_Type == ID_MAG))
                                                   {
                                                   ValidFile = TRUE;
                                                   continue;
                                                   }

                                                break;
                                                }

///                       ID_VERS
                                         if(ID == ID_VERS)
                                                {
                                                struct BaseVersion version;

                                                if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                                                   {

                                                   }
                                                else
                                                   {
                                                   _RC = IoErr();
                                                   Errors++;
                                                   break;
                                                   }

                                                continue;
                                                }
//|
///                       ID_GRUP

                                         if(ID == ID_GRUP)
                                                {
                                                struct GrupaList *grupa = calloc(1, sizeof(struct GrupaList));

                                                if(grupa)
                                                   {
                                                   if(ReadChunkBytes(iff, &grupa->gm_grupa, sizeof(struct Grupa)) != cn->cn_Size)
                                                         {
                                                         Error_Skipped_Groups++;
//                             _RC = IoErr();
                                                         Errors++;
                                                         free(grupa);
                                                         continue;
                                                         }

                                                   if(!(Znajd�Grup�ID(grupa->gm_grupa.ID)))
                                                          {
                                                          DodajGrup�(grupa, TRUE);
                                                          grupa_aktywna = grupa;
                                                          Current++;
                                                          }
                                                   else
                                                          {
                                                          free(grupa);
                                                          Error_Duplicated_Groups++;
                                                          }
                                                   }
                                                continue;
                                                }
//|
///                       ID_PROD

                                         if(ID == ID_PROD)
                                                {
                                                struct ProductList *prod  = calloc(1, sizeof(struct ProductList));

                                                if(prod)
                                                   {
                                                   if(ReadChunkBytes(iff, &prod->pl_prod, sizeof(struct Product)) != cn->cn_Size)
                                                          {
                                                          Error_Skipped_Products++;
                                                          free(prod);
//                              _RC = IoErr();
                                                          Errors++;
                                                          continue;
                                                          }

                                                   if(grupa_aktywna)
                                                         {
                                                         if(prod->pl_prod.Grupa != grupa_aktywna->gm_grupa.ID)
                                                           {
                                                           grupa_aktywna = Znajd�Grup�ID(prod->pl_prod.Grupa);
                                                           }

                                                         if(grupa_aktywna)
                                                           {
                                                           DodajProdukt(prod, FALSE);

                                                           if( !Znajd�Jednostk�Nazwa( prod->pl_prod.Jedn.Nazwa) )
                                                                   {
                                                                   struct JednostkaList *jedn = calloc(1, sizeof(struct JednostkaList));

                                                                   if(jedn)
                                                                         {
                                                                         memcpy( &jedn->ul_jedn, &prod->pl_prod.Jedn, sizeof(struct Jednostka));

                                                                         jedn->Imported = TRUE;
                                                                         DodajJednostk�(jedn);

                                                                         Dodane_Jednostki++;
                                                                         }
                                                                   }
                                                           }
                                                         else
                                                           {
                                                           free(prod);
                                                           Error_Products++;
                                                           }

                                                         Current++;
                                                         }
                                                   else
                                                         {
                                                         free(prod);
                                                         Error_Products++;
                                                         }

                                                   if((Current%10) == 0)
                                                         {
                                                         set(GA_Startup_Info, MUIA_Gauge_Current, CurrentGauge++);
//                             D(bug("%ld\n", CurrentGauge));
                                                         }
                                                   }
                                                continue;
                                                }
//|

                                         }
                                  }

                           CloseIFF(iff);
                           }

                        Close(iff->iff_Stream);
                        }

//       if(_RC == IFFERR_EOF) _RC = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

           FreeIFF(iff);
           }


/*
           if( Dodane_Jednostki > 0 )
                   ZapiszJednostki();
*/


           set(GA_Startup_Info, MUIA_Gauge_Current, 0);
           set(app, MUIA_Application_Sleep, FALSE);


           if(Error_Products + Error_Skipped_Products + Error_Duplicated_Groups)
                   {
                   DisplayBeep(0);
                   MUI_Request(app, NULL, 0, TITLE, MSG_OK, MSG_ERR_LOAD_STOCK, Error_Skipped_Products, Error_Skipped_Groups, Error_Products, Error_Duplicated_Groups, Current);
                   }

           return(Error_Products + Error_Skipped_Products + Error_Duplicated_Groups + Error_Skipped_Groups);

}

//|
/// ZapiszMagazyn

int ZapiszMagazyn(void)
{
  struct IFFHandle *MyIFFHandle;
  long   Count = 0;

        set(app, MUIA_Application_Sleep, TRUE);

        if(MyIFFHandle = AllocIFF())
                {
                BPTR  FileHandle;

                if(FileHandle = Open(MagazynFileName, MODE_NEWFILE))
                   {
                   MyIFFHandle->iff_Stream = FileHandle;
                   InitIFFasDOS(MyIFFHandle);

                   if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
                           {
                           struct BaseVersion version;
                           struct ProductList *node;
                           struct GrupaList   *grupa;


                           PushChunk(MyIFFHandle, ID_MAG, ID_CAT, IFFSIZE_UNKNOWN);

                           PushChunk(MyIFFHandle, ID_MAG, ID_FORM, IFFSIZE_UNKNOWN);
                                   PushChunk(MyIFFHandle, ID_MAG, ID_VERS, IFFSIZE_UNKNOWN);
                                   version.Version = VERSION;
                                   version.Revision = REVISION;
                                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                                   PopChunk(MyIFFHandle);
/*
                                   PushChunk(MyIFFHandle, ID_MAG, ID_ANNO, IFFSIZE_UNKNOWN);
                                   WriteChunkBytes(MyIFFHandle, ScreenTitle, ScreenTitleLen-1);
                                   WriteChunkBytes(MyIFFHandle, " <", 2);
                                   WriteChunkBytes(MyIFFHandle, EMAIL, strlen(EMAIL));
                                   WriteChunkBytes(MyIFFHandle, ">, ", 3);
                                   WriteChunkBytes(MyIFFHandle, WWW, strlen(WWW));
                                   PopChunk(MyIFFHandle);
*/
                           PopChunk(MyIFFHandle);


                           for(grupa = (struct GrupaList *)grupy.lh_Head; grupa->gm_node.mln_Succ; grupa = (struct GrupaList *)grupa->gm_node.mln_Succ)
                                   {
                                   PushChunk(MyIFFHandle, ID_MAG, ID_FORM, IFFSIZE_UNKNOWN);

                                   PushChunk(MyIFFHandle, ID_MAG, ID_GRUP, IFFSIZE_UNKNOWN);
                                   WriteChunkBytes(MyIFFHandle, &grupa->gm_grupa, sizeof(struct Grupa));
                                   PopChunk(MyIFFHandle);

                                   Count++;

                                   for(node = (struct ProductList *)grupa->produkty.lh_Head; node->pl_node.mln_Succ; node = (struct ProductList *)node->pl_node.mln_Succ)
                                         {
                                         PushChunk(MyIFFHandle, ID_MAG, ID_FORM, IFFSIZE_UNKNOWN);

                                         PushChunk(MyIFFHandle, ID_MAG, ID_PROD, IFFSIZE_UNKNOWN);
                                         WriteChunkBytes(MyIFFHandle, &node->pl_prod, sizeof(struct Product));
                                         PopChunk(MyIFFHandle);

                                         PopChunk(MyIFFHandle);

                                         Count++;
                                         }

                                   PopChunk(MyIFFHandle);
                                   }

                           PopChunk(MyIFFHandle);
                           CloseIFF(MyIFFHandle);

                           ZapiszCount(MagazynCountFileName, Count);
                           }
                   else
                           {
                           DisplayBeep(0);
                           D(bug("*** OpenIFF() nie powiod�o si�\n"));
                           }

                   Close(FileHandle);
                   }
                else
                   {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
                   D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", MagazynFileName));
                   }

                FreeIFF(MyIFFHandle);
                }
         else
                {
                DisplayBeep(0);
                D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
                }

        set(app, MUIA_Application_Sleep, FALSE);


  return(0);
}



//|
*/
/// ZapiszMagazynProgress
int ZapiszMagazynProgress( void )
{
int result;

        set(GA_Prog_Info, MUIA_Gauge_Current, 0);
        set(GA_Prog_Info, MUIA_Gauge_Max, 1);
        set(ProgressWindow, MUIA_Window_Open, TRUE);

        settext(TX_Prog_Info, MSG_PROG_MAG_SAVE);
        result = ZapiszMagazyn();

        set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);
        set(ProgressWindow, MUIA_Window_Open, FALSE);

        return( result );

}
//|


// doda� sprawdzanie poprawno�ci limit�w min max i czy
// max >= min !!

/// EDYCJA PRODUKTU STRINGS

#define MSG_PROD_WIN_TITLE_T "Edycja produktu"
#define MSG_PROD_WIN_TITLE_U "Edycja us�ugi"
#define MSG_PROD_WIN_TITLE_F "Sprzeda� z r�ki - Edycja elementu"

#define MSG_MAG_ED_PAGE1   "F1 - Dane"
#define MSG_MAG_ED_PAGE2   "F2 - R��ne"
#define MSG_MAG_ED_PAGE3   "F3 - Limity"


#define MSG_MAG_TITLE_1_T  "Dane produktu"
#define MSG_MAG_TITLE_1_U  "Dane us�ugi"
#define MSG_MAG_TITLE_1_E  "Dane"
#define MSG_MAG_INDEX      "In_deks"
#define  SH_MAG_INDEX      "Opcjonalny indeks produktu/us�ugi.\n\nIndeks musi by�jedynie unikalny w\nobr�bie danej grupy magazynowej"
#define MSG_MAG_NAME       "_Nazwa"
#define  SH_MAG_NAME       "Nazwa produktu/us�ugi"
#define MSG_MAG_SWW        "_SWW"
#define  SH_MAG_SWW        "Numer PKWiU (dawniej SWW) produktu/us�ugi\n(wymagany jedynie w przypadku obni�onej\nstawki podatku VAT dla produktu/us�ugi"
#define MSG_MAG_AMOUNT     "_Ilo��"
#define  SH_MAG_AMOUNT     NULL
#define MSG_MAG_JEDN       "_Jedn. miary"
#define  SH_MAG_JEDN       "Jednostka miary (wagi) dla danego elementu"
#define MSG_MAG_WYBIERZ    "_Wybierz"
#define MSG_MAG_ZAKUP_1    "Cena _zakupu"
#define MSG_MAG_ZAKUP_2    "Cena _netto"
#define MSG_MAG_MARZA      "Ma_r�a"
#define MSG_MAG_MARZA2     "z�  ("
#define MSG_MAG_MARZA3     "%)"
#define MSG_MAG_VAT        "Stawka _VAT"
#define MSG_MAG_SPRZ       "Cena _brutto"

#define MSG_MAG_CALC_TITLE "Mini kalkulator"
#define MSG_MAG_CALC_NETTO "Sprz_eda�"
#define MSG_MAG_CALC_COPY  "_^"
#define MSG_MAG_BRUTTO     "_Brutto"

#define MSG_MAG_TITLE_2    "Limity magazynowe"
#define MSG_MAG_ED_LIMITMINEN  "_Limit min."
#define MSG_MAG_ED_LIMITMIN    "Stan _min."
#define MSG_MAG_ED_LIMITMAXEN  "L_imit maks."
#define MSG_MAG_ED_LIMITMAX    "Stan m_aks."

#define MSG_MAG_TITLE_3    "Pozosta�e"
#define MSG_MAG_ED_GWEN    "G_warancja"
#define MSG_MAG_ED_GW      "Okres _gwarancyjny"
#define MSG_MAG_ED_GW2     "dni"

#define MSG_MAG_ED_OK      "F10 - _Ok"
#define MSG_MAG_ED_CANCEL  "ESC - Ponie_chaj"

#define MSG_MAG_ED_VATSWW  "\033cPrzy obni�onym podatku VAT musisz\npoda� co najmniej 4 pierwsze\ncyfry symbolu SWW!"

#define MSG_MAG_ED_UNIQIDX "\033cProdukt o takim indeksie ju� istnieje!\nNumer indeksowy towaru musi by� unikalny\nw obr�bie ca�ego magazynu!"

//|
/// EdycjaProduktuSetup

static Object
           *EdycjaProduktuWindow,         /* EDYCJA PRODUKTU */
           *GR_MagEd_Pages,

           *ST_MagEd_Index,
           *ST_MagEd_Nazwa,
           *ST_MagEd_SWW,
           *ST_MagEd_Ilo��,
           *TX_MagEd_Jedn,
           *BT_MagEd_JednWybierz,
           *ST_MagEd_Zakup,
           *ST_MagEd_Mar�a,
           *ST_MagEd_Mar�a_Procent,
           *ST_MagEd_Brutto,
           *CY_MagEd_Vat,

           *CH_MagEd_LimitMinEnabled,
           *ST_MagEd_LimitMin,
           *CH_MagEd_LimitMaxEnabled,
           *ST_MagEd_LimitMax,

           *CH_MagEd_Gwarancja,
           *ST_MagEd_OkresGwarancji,

           *BT_MagEd_Ok,
           *BT_MagEd_Cancel;


char *GR_MagEd_Ed_Titles[] = {
                                                          MSG_MAG_ED_PAGE1,
                                                          MSG_MAG_ED_PAGE2,
                                                          MSG_MAG_ED_PAGE3,
                                                          NULL
                                                         };


char EdycjaProduktuSetup(struct ProductList *prod)
{
struct Product *Product = &prod->pl_prod;
static char    *win_title;
char           *gr_title;
char           *bt_zakup;


        switch( Product->Typ )
           {
           case TYP_TOWAR:
                   win_title = MSG_PROD_WIN_TITLE_T;
                   gr_title  = MSG_MAG_TITLE_1_T;
                   bt_zakup  = MSG_MAG_ZAKUP_1;

                   // struktura Expanded jest uzywana tylko przy
                   // produktach ktore sa juz umieszczone na
                   // dokumencie, zatem nie ma co edytowac
                   // wartosci MIN/MAX mimo ze to produkt
                   if( prod->Expanded )
                          GR_MagEd_Ed_Titles[2] = NULL;
                   else
                          GR_MagEd_Ed_Titles[2] = MSG_MAG_ED_PAGE3;
                   break;

           case TYP_USLUGA:
                   {
                   win_title = MSG_PROD_WIN_TITLE_U;
                   gr_title  = MSG_MAG_TITLE_1_U;
                   bt_zakup  = MSG_MAG_ZAKUP_2;

                   GR_MagEd_Ed_Titles[2] = NULL;
                   }
                   break;

           case TYP_FREEHAND:
                   {
                   win_title = MSG_PROD_WIN_TITLE_F ;
                   gr_title  = MSG_MAG_TITLE_1_E ;
                   bt_zakup  = MSG_MAG_ZAKUP_2;

                   GR_MagEd_Ed_Titles[2] = NULL;
                   }
                   break;
           }



///   Create EdycjaProduktuWindow

//                 MUIA_Application_Window,
                                         EdycjaProduktuWindow = WindowObject,
                                                MUIA_Window_Title      , win_title,
                                                MUIA_Window_ID         , ID_WIN_EDYCJA_PROD,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
//                        MUIA_Window_CloseGadget, FALSE,
                                                WindowContents,
                                                   VGroup,

                                                   Child, GR_MagEd_Pages = RegisterGroup(GR_MagEd_Ed_Titles),
///                                 Edycja elementu

                                                                        Child, VGroup,
//                                           GroupFrame,
                                                                                   GroupFrameT(gr_title),

                                                                                   Child, ColGroup(2),

                                                                                                  Child, MakeLabel2(MSG_MAG_INDEX),
                                                                                                  Child, ST_MagEd_Index = _MakeString(PROD_INDEX_LEN, MAG_INDEX),

                                                                                                  Child, MakeLabel2(MSG_MAG_NAME),
                                                                                                  Child, ST_MagEd_Nazwa = _MakeString(PROD_NAME_LEN, MAG_NAME),

                                                                                                  Child, MakeLabel2(MSG_MAG_SWW),
                                                                                                  Child, ST_MagEd_SWW = _MakeString(PROD_SWW_LEN, MAG_SWW),

                                                                                                  Child, MakeLabel2(MSG_MAG_AMOUNT),
                                                                                                  Child, ST_MagEd_Ilo�� = MakeAmountString(10, MSG_MAG_AMOUNT),

                                                                                                  Child, MakeLabel2(MSG_MAG_JEDN),
                                                                                                  Child, HGroup,
                                                                                                                 MUIA_Group_Spacing, 1,
                                                                                                                 _SH( SH_MAG_JEDN ),
                                                                                                                 Child, TX_MagEd_Jedn = TextObject, TextFrame, TextBack, End,
                                                                                                                 Child, BT_MagEd_JednWybierz = PopButton2(MUII_PopUp, MSG_MAG_JEDN),
                                                                                                                 End,
                                                                                                  End,

                                                                                   Child, Bar,

                                                                                   Child, ColGroup(2),
                                                                                                  Child, MakeLabel2( bt_zakup ),
                                                                                                  Child, ST_MagEd_Zakup = MakeCashString(10, bt_zakup),

                                                                                                  Child, MakeLabel2(MSG_MAG_MARZA),
                                                                                                  Child, HGroup,
                                                                                                                 Child, ST_MagEd_Mar�a = MakeSignedCashString(10, MSG_MAG_MARZA),
                                                                                                                 Child, MakeLabel2(MSG_MAG_MARZA2),

                                                                                                                 Child, ST_MagEd_Mar�a_Procent = MakeSignedCashString(7, NULL),
                                                                                                                 Child, MakeLabel2(MSG_MAG_MARZA3),

                                                                                                                 End,

                                                                                                  Child, MakeLabel2(MSG_MAG_VAT),
                                                                                                  Child, CY_MagEd_Vat = MakeCycle(VatTable, MSG_MAG_VAT),
                                                                                                  End,

                                                                                   Child, Bar,

                                                                                   Child, ColGroup(2),
                                                                                                  Child, MakeLabel2( MSG_MAG_SPRZ ),
                                                                                                  Child, ST_MagEd_Brutto = MakeCashString( 10, MSG_MAG_SPRZ ),
                                                                                                  End,
                                                                                   End,
//|
///                                 R��ne

                                                                  Child, HGroup,
                                                                                 GroupFrameT(MSG_MAG_TITLE_3),
                                                                                 MUIA_Group_Columns, 2,

                                                                                 Child, MakeLabel2(MSG_MAG_ED_GWEN),
                                                                                 Child, HGroup,
                                                                                                Child, CH_MagEd_Gwarancja = MakeCheck(MSG_MAG_ED_GWEN),
                                                                                                Child, HVSpace,
                                                                                                End,

                                                                                 Child, MakeLabel2(MSG_MAG_ED_GW),
                                                                                 Child, HGroup,
                                                                                                Child, ST_MagEd_OkresGwarancji = MakeCashString(10, MSG_MAG_ED_GW),
                                                                                                Child, MakeLabel2(MSG_MAG_ED_GW2),
                                                                                                End,

                                                                                 Child, HVSpace,
                                                                                 End,
//|
///                                 Limity magazynowe

                                                                  Child, HGroup,
                                                                                 GroupFrameT(MSG_MAG_TITLE_2),
                                                                                 MUIA_Group_Columns, 2,

                                                                                 Child, MakeLabel2(MSG_MAG_ED_LIMITMINEN),
                                                                                 Child, HGroup,
                                                                                                Child, CH_MagEd_LimitMinEnabled = MakeCheck(MSG_MAG_ED_LIMITMINEN),
                                                                                                Child, HVSpace,
                                                                                                End,

                                                                                 Child, MakeLabel2(MSG_MAG_ED_LIMITMIN),
                                                                                 Child, ST_MagEd_LimitMin = MakeAmountString(10, MSG_MAG_ED_LIMITMIN),


                                                                                 Child, MakeLabel2(MSG_MAG_ED_LIMITMAXEN),
                                                                                 Child, HGroup,
                                                                                                Child, CH_MagEd_LimitMaxEnabled = MakeCheck(MSG_MAG_ED_LIMITMAXEN),
                                                                                                Child, HVSpace,
                                                                                                End,

                                                                                 Child, MakeLabel2(MSG_MAG_ED_LIMITMAX),
                                                                                 Child, ST_MagEd_LimitMax = MakeAmountString(10, MSG_MAG_ED_LIMITMAX),

                                                                                 Child, HVSpace,
                                                                                 End,
//|

                                                                  End,


                                                   Child, HGroup,
                                                                  Child, BT_MagEd_Ok     = TextButton(MSG_MAG_ED_OK),
                                                                  Child, BT_MagEd_Cancel = TextButton(MSG_MAG_ED_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|


        if(!EdycjaProduktuWindow)
           return(FALSE);



        // notifications

        set(BT_MagEd_JednWybierz, MUIA_CycleChain, FALSE);

        DoMethod(EdycjaProduktuWindow   ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(BT_MagEd_Ok     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_MagEd_Cancel ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(BT_MagEd_JednWybierz  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT);

        DoMethod(ST_MagEd_Zakup        , MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_ZAKUP_ACK);

//    if( (Product->Typ == TYP_TOWAR) || (Product->Typ == TYP_FREEHAND) )
           {
           DoMethod(ST_MagEd_Mar�a        , MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_MARZA_ACK);
           DoMethod(ST_MagEd_Mar�a_Procent, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_MARZA_PROCENT_ACK);
           }

        DoMethod(ST_MagEd_Brutto       , MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_BRUTTO_ACK);
        DoMethod(CY_MagEd_Vat          , MUIM_Notify, MUIA_Cycle_Active   , MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_VAT_ACK);

//     DoMethod(ST_MagEd_Zakup     ,MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_SPRZEDAZ);
//     DoMethod(ST_MagEd_Mar�a     ,MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_SPRZEDAZ);
//     DoMethod(ST_MagEd_Sprzeda�  ,MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_BRUTTO);
//     DoMethod(ST_MagEd_Calc_Netto,MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_BRUTTO_CALC);
//     DoMethod(BT_MagEd_Calc_Copy ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_COPY);
//     DoMethod(ST_MagEd_Brutto    ,MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_NETTO);
//     DoMethod(CY_MagEd_Vat       ,MUIM_Notify, MUIA_Cycle_Active   , MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_BRUTTO_VAT);

        DoMethod(EdycjaProduktuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_0);
        DoMethod(EdycjaProduktuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_1);

        if(Product->Typ == TYP_TOWAR )
           DoMethod(EdycjaProduktuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_2);

        DoMethod(EdycjaProduktuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

        DoMethod(CH_MagEd_LimitMinEnabled, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_MagEd_LimitMin      , 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue);
        DoMethod(CH_MagEd_LimitMaxEnabled, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_MagEd_LimitMax      , 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue);
        DoMethod(CH_MagEd_Gwarancja      , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_MagEd_OkresGwarancji, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue);


        // misc


        // ocho, czy edytujemy produkt z dokumentu...

        if( prod->Expanded )
           {
           // i owszem...
           struct FakProductList *fprod = (struct FakProductList *)prod;

           // zobaczmy co mozna edytowac...

           _enable ( ST_MagEd_Ilo�� );

           switch( Product->Typ )
                   {
/*
                   case TYP_FREEHAND:
                           {
//               _disable( ST_MagEd_Index );
//               _enable ( ST_MagEd_Nazwa );
//               _enable ( ST_MagEd_SWW   );
                           }
                           break;
*/

                   case TYP_USLUGA:
                           {
                           _disable( ST_MagEd_Mar�a );
                           _disable( ST_MagEd_Mar�a_Procent );
//               _disable( ST_MagEd_Index );
//               _enable ( ST_MagEd_Nazwa );
//               _enable ( ST_MagEd_SWW   );
                           }
                           break;

                   case TYP_TOWAR:
                           {
                           _disable( ST_MagEd_Index );
                           _disable( ST_MagEd_Nazwa );
//               _disable( ST_MagEd_SWW   );
                           }
                           break;
                   }
           }
        else
           {
           /* je�li us�uga, go�stujemy niekt�re gad�ety */

                if( (Product->Typ != TYP_TOWAR) || (Product->Typ == TYP_FREEHAND) )
                   {
                   DoMethod(ST_MagEd_Ilo��, MUIM_MultiSet, MUIA_Disabled, TRUE,
                                          ST_MagEd_Ilo��,
                                          ST_MagEd_Mar�a,
                                          ST_MagEd_Mar�a_Procent,
                                          CH_MagEd_LimitMinEnabled,
                                          ST_MagEd_LimitMin,
                                          CH_MagEd_LimitMaxEnabled,
                                          ST_MagEd_LimitMax,
                                          NULL);
                   }
           }



        // contents

        setstring(ST_MagEd_Index, Product->Index);
        setstring(ST_MagEd_Nazwa, Product->Nazwa);
        setstring(ST_MagEd_SWW  , Product->SWW);
        settext(TX_MagEd_Jedn   , Product->Jedn.Nazwa);

        nnsetstring(ST_MagEd_Zakup , Price2String(Product->Zakup));
        nnsetstring(ST_MagEd_Mar�a , Mar�a2String(Product->Mar�a));
        nnsetstring(ST_MagEd_Mar�a_Procent , Mar�a2String(CalcMar�aProcent(Product->Zakup, Product->Mar�a)));
        nnsetstring(ST_MagEd_Brutto, Price2String(CalcBrutto(Product->Zakup, Product->Mar�a, Product->Vat)));

        nnsetcycle(CY_MagEd_Vat    , Product->Vat);

        setcheckmark(CH_MagEd_Gwarancja, Product->GwarancjaEn);
        set(ST_MagEd_OkresGwarancji    , MUIA_String_Integer, Product->Gwarancja);

        setstring(ST_MagEd_Ilo��   , Price2String(Product->Ilo��));


        if( (Product->Typ == TYP_TOWAR ) )
          {
          setcheckmark(CH_MagEd_LimitMinEnabled, Product->MinEn);
          setstring(ST_MagEd_LimitMin          , Price2String(Product->StanMin));
          setcheckmark(CH_MagEd_LimitMaxEnabled, Product->MaxEn);
          setstring(ST_MagEd_LimitMax          , Price2String(Product->StanMax));
          }


        set(EdycjaProduktuWindow, MUIA_Window_ActiveObject, ST_MagEd_Index);


        // attach

        _attachwin( EdycjaProduktuWindow );

        return(TRUE);
}
//|
/// EdycjaProduktuFinish
char EdycjaProduktuFinish(struct ProductList *prod, char EditMode)
{
/*
** sprawdza poprawno�� wprowadzonych danych dot. produktu. Zwraca FALSE,
** je�li co� jest nie tak, TRUE, je�li wszystkie elementy s� poprawne,
** oraz REFRESH, je�li nazwa produktu zosta�a zmieniona
** (istotne tylko podczas edycji produktu)
**
** Je�li EditMode == TRUE, produkt jest edytowany, zatem mo�na go usun�� z
** listy przed poszukiwaniem duplikatu
*/

struct Product *Product = &prod->pl_prod;
char   result = TRUE;


        if( !(strlen(getstr(ST_MagEd_Nazwa))) )
           {
           set(EdycjaProduktuWindow, MUIA_Window_ActiveObject, ST_MagEd_Nazwa);
           return(FALSE);
           }


        if( Product->Jedn.Nazwa[0] == 0 )
           {
           set(EdycjaProduktuWindow, MUIA_Window_ActiveObject, BT_MagEd_JednWybierz);
           return(FALSE);
           }


        if(VatValTable[(char)xget(CY_MagEd_Vat, MUIA_Cycle_Active)] != 22)
           {
           if(strlen((char *)xget(ST_MagEd_SWW, MUIA_String_Contents)) < 4 )
                   {
                   MUI_Request(app, EdycjaProduktuWindow, 0, TITLE, MSG_OK, MSG_MAG_ED_VATSWW);
                   set(EdycjaProduktuWindow, MUIA_Window_ActiveObject, ST_MagEd_SWW);
                   return(FALSE);
                   }
           }


        // sprawdzamy unikalno�� produktu. Je�li podano index, sprawdzamy
        // jedynie czy jest on unikalny (nazwa jest bez znaczenia)
        // je�li podano jedynie nazw�, musi by� ona unikalna w obr�bie
        // grupy magazynowej

        // jesli prod->Expanded == TRUE, to nie sprawdzamy, bo poniewaz
        // to byla edycja elementu z faktury

   if( prod->Expanded == FALSE )
        {
        if( strlen(getstr(ST_MagEd_Index)) )
           {
           struct ProductList *tmp;

           if(EditMode)
                  {
                  Remove((struct Node *)prod);
                  tmp = Znajd�ProduktIndexWMagazynie(getstr(ST_MagEd_Index));
                  DodajProdukt(prod, TRUE);
                  }
           else
                  {
                  tmp = Znajd�ProduktIndexWMagazynie(getstr(ST_MagEd_Index));
                  }

           if(tmp)
                   {
                   MUI_Request(app, EdycjaProduktuWindow, 0, TITLE, MSG_OK, MSG_MAG_ED_UNIQIDX);
                   set(EdycjaProduktuWindow, MUIA_Window_ActiveObject, ST_MagEd_Index);
                   return(FALSE);
                   }
           }
        else
           {
           struct ProductList *tmp;

           if(EditMode)
                  {
                  Remove((struct Node *)prod);
                  tmp = Znajd�ProduktNazwa( getstr(ST_MagEd_Nazwa) );
                  DodajProdukt(prod, TRUE);
                  }
                else
                  {
                  tmp = Znajd�ProduktNazwa( getstr(ST_MagEd_Nazwa) );
                  }

                if(tmp)
                   {
                   set(EdycjaProduktuWindow, MUIA_Window_ActiveObject, ST_MagEd_Nazwa);
                   return(FALSE);
                   }
                }
        }


//    if(StrnCmp(MyLocale, Product->Nazwa, (char *)xget(ST_MagEd_Nazwa, MUIA_String_Contents), -1, SC_COLLATE2) != 0)
           result = REFRESH;



        // copy back...

        copystr(Product->Index, ST_MagEd_Index);
        strcpy(Product->SWW      , (char *)xget(ST_MagEd_SWW  , MUIA_String_Contents));
        strcpy(Product->Nazwa, (char *)xget(ST_MagEd_Nazwa, MUIA_String_Contents));


        // parsujemy nazwe via locale.library (jesli to jest usluga)
        // i wlasnie edytujemy cos albo TYP_FREEHAND albo Expanded == TRUE

        if( ((prod->Expanded) && (Product->Typ == TYP_USLUGA)) ||
                 (Product->Typ == TYP_FREEHAND)
          )
                {
                char parsed_name[ PROD_NAME_LEN * 5];

                if( wfmh_Date2StrFmtBuffer( parsed_name, Product->Nazwa, wfmh_GetCurrentTime() ) )
                  {
                  strncpy( Product->Nazwa, parsed_name,  PROD_NAME_LEN );
                  }
                else
                  {
                  DisplayBeep(0);
                  D(bug("** Can't parse product name!\n"));
                  }
                }

        // i reszta kopiowania

        Product->Ilo�� = String2Price((char *)xget(ST_MagEd_Ilo��, MUIA_String_Contents));
        Product->Zakup = String2Price((char *)xget(ST_MagEd_Zakup, MUIA_String_Contents));
        Product->Mar�a = ObetnijKo�c�wk�(String2Mar�a((char *)xget(ST_MagEd_Mar�a, MUIA_String_Contents)));
        Product->Vat = (char)xget(CY_MagEd_Vat, MUIA_Cycle_Active);

//    D(bug("Zakup: %s\n", Price2String(Product->Zakup)));
//    D(bug("Mar�a: %s\n", Price2String(Product->Mar�a)));

        Product->MinEn   = getcheckmark(CH_MagEd_LimitMinEnabled);
        Product->StanMin = String2Price( getstr(ST_MagEd_LimitMin));

        Product->MaxEn   = getcheckmark(CH_MagEd_LimitMaxEnabled);
        Product->StanMax = String2Price( getstr(ST_MagEd_LimitMax));

        Product->GwarancjaEn = getcheckmark(CH_MagEd_Gwarancja);
        Product->Gwarancja = getnumstr(ST_MagEd_OkresGwarancji);

        return(result);

}
//|
/// EdycjaProduktu

char EdycjaProduktu(struct ProductList *prod, char EditMode)
{
/*
** G�owna p�tla edycji produktu
** Zwraca to co EdycjaProduktuFinish()
** b�d� FALSE je�li przerwano edycj�
**
** Je�li EditMode == TRUE, to edytujemy
** istniej�cy ju� produkt
*/

char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

struct Product prod_tmp = {0};         // kopia, po cancel, resotrujemy z niej


        set(app, MUIA_Application_Sleep, TRUE);

        memcpy(&prod_tmp, &prod->pl_prod, sizeof(struct Product));

        if( !EdycjaProduktuSetup(prod) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(FALSE);
           }


        if( WinOpen(EdycjaProduktuWindow) )
                {
                while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);

                  switch(ID)
                        {
                        case ID_SELECT:
                           {
                           struct JednostkaList *jedn = JednostkaSelector(&prod->pl_prod.Jedn);

                           if(jedn)
                                   {
                                   memcpy(&prod->pl_prod.Jedn, &jedn->ul_jedn, sizeof(struct Jednostka));
                                   settext(TX_MagEd_Jedn, prod->pl_prod.Jedn.Nazwa);
                                   }
                           }
                           break;


                        case ID_PAGE_0:
                        case ID_PAGE_1:
                        case ID_PAGE_2:
                           set(GR_MagEd_Pages, MUIA_Group_ActivePage, ID - ID_PAGE_0);
                           break;

                        // zmiana w cenie zakupu, mar�y albo zmiana vatu (cycle)
                        case ID_MAG_ZAKUP_ACK:
                        case ID_MAG_MARZA_ACK:
                        case ID_MAG_VAT_ACK:
                           {
                           double zakup  = 0;
                           double mar�a_procent = 0;
                           double mar�a  = 0;
                           char   vat;

                           vat   = getcycle(CY_MagEd_Vat);
                           zakup = String2Price((char *)xget(ST_MagEd_Zakup, MUIA_String_Contents));
                           mar�a = String2Mar�a((char *)xget(ST_MagEd_Mar�a, MUIA_String_Contents));
                           mar�a_procent = String2Mar�a((char *)xget(ST_MagEd_Mar�a_Procent, MUIA_String_Contents));

                           if( ID != ID_MAG_ZAKUP_ACK )
                                   nnsetstring(ST_MagEd_Zakup, Price2String(zakup));
                           if( ID != ID_MAG_MARZA_ACK )
                                        nnsetstring(ST_MagEd_Mar�a, Price2String(mar�a));
                           if( ID == ID_MAG_MARZA_ACK )
                                  nnsetstring(ST_MagEd_Mar�a_Procent, Price2String(CalcMar�aProcent(zakup, mar�a)));
                           if( ID == ID_MAG_ZAKUP_ACK )
                                  {
                                  if( settings.sta�a_mar�a_procentowa )
                                        {
                                        mar�a = CalcMar�a( zakup, mar�a_procent );

                                        nnsetstring(ST_MagEd_Mar�a , Price2String(mar�a));
                                        nnsetstring(ST_MagEd_Brutto, Price2String(CalcBrutto(zakup, mar�a, vat)));
                                        }
                                  }

                           nnsetstring(ST_MagEd_Brutto, Price2String(CalcBrutto(zakup, mar�a, vat)));
                           }
                           break;


                        // zmiana w cenie brutto
                        case ID_MAG_BRUTTO_ACK:
                           {
                           double zakup  = 0;
                           double brutto = 0;
                           double mar�a  = 0;
                           char   vat;

                           vat    = getcycle(CY_MagEd_Vat);
                           zakup  = String2Price((char *)xget(ST_MagEd_Zakup , MUIA_String_Contents));
                           brutto = String2Price((char *)xget(ST_MagEd_Brutto, MUIA_String_Contents));

                           nnsetstring(ST_MagEd_Zakup, Price2String(zakup));


                           if( prod_tmp.Typ == TYP_TOWAR )
                                   prod_tmp.Mar�a = mar�a = ObetnijKo�c�wk�((brutto/(double)(1.0+(double)(VatValTable[vat]/100.0))) - zakup);
                           else
                                   prod_tmp.Mar�a = mar�a = ObetnijKo�c�wk�((brutto/(double)(1.0+(double)(VatValTable[vat]/100.0))) );


//               prod_tmp.Mar�a = mar�a = ((brutto/(double)(1.0+(double)(VatValTable[vat]/100.0))) - zakup);

                           if( prod_tmp.Typ == TYP_TOWAR )
                                 {
                                 nnsetstring( ST_MagEd_Mar�a, Price2String(mar�a) );
                                 nnsetstring(ST_MagEd_Mar�a_Procent, Price2String(CalcMar�aProcent(zakup, mar�a)));
                                 }
                           else
                                 {
                                 nnsetstring( ST_MagEd_Zakup, Price2String(mar�a) );
                                 }

                           }
                           break;


                        // zmiana w procentach marzy
                        case ID_MAG_MARZA_PROCENT_ACK:
                           {
                           double zakup         = 0;
                           double mar�a         = 0;
                           double mar�a_procent = 0;
                           char   vat;

                           vat   = getcycle(CY_MagEd_Vat);
                           zakup = String2Price((char *)xget(ST_MagEd_Zakup, MUIA_String_Contents));
                           mar�a_procent = String2Mar�a((char *)xget(ST_MagEd_Mar�a_Procent, MUIA_String_Contents));
                           mar�a = CalcMar�a(zakup, mar�a_procent);

                           nnsetstring(ST_MagEd_Zakup , Price2String(zakup));
                           nnsetstring(ST_MagEd_Mar�a , Price2String(mar�a));
                           nnsetstring(ST_MagEd_Brutto, Price2String(CalcBrutto(zakup, mar�a, vat)));
                           }
                           break;



                        case ID_OK:
                           {
                           result = EdycjaProduktuFinish(prod, EditMode);

                           if(result == FALSE)
                                  {
                                  DisplayBeep(0);
                                  }
                           else
                                  {
//                  prod->pl_prod.Mar�a = prod_tmp.Mar�a;
                                  running = FALSE;
                                  }
                           }
                           break;


                        case ID_CANCEL:
                           // memcpy(&prod->pl_prod, &prod_tmp, sizeof(struct Product));
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

                set(EdycjaProduktuWindow, MUIA_Window_Open, FALSE);
                }
         else
                {
                DisplayBeep(0);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


        _detachwin( EdycjaProduktuWindow );

        set(app, MUIA_Application_Sleep, FALSE);

        return(result);
}

//|

/// PreprocessCalyMagazyn

// przygotowuje do wydruku cennik z calego magazynu
// wpisuje podany rabat bezposrednio w pozycje magazynowe
// tak aby parserowe komendy mogly sobie rabat przeliczac
// Moze tak byc, poniewaz, to pole nie jest uzywane przy
// odczycie, jedynie podczas wystawiania faktury jest tam
// wpisywany rabat jakiego user udzielil klientowi, wiec
// zmiana (i ew. zapisanie tak zmienionego magazynu) nie
// boli

// INPUT: TylkoPrzekroczoneLimity LIMIT_xxx (NONE,MIN,MAX)
//        jesli != LIMIT_NONE tylko elementy przekraczajace
//        podany typ limitu beda uwzglednione przy sprawdzaniu

int PreprocessCalyMagazyn( Object *dest_lv, double limit, char UseLimit,
                                                   double rabat, char ProdCzyUslugi,
                                                   char PrzekroczoneLimity )
{
struct GrupaList   *grupa;
struct ProductList *produkt;
int    count = 0;


        if(!IsListEmpty( &grupy ))
          {
          for( grupa = (struct GrupaList *)grupy.lh_Head; grupa->gm_node.mln_Succ; grupa = (struct GrupaList *)grupa->gm_node.mln_Succ )
                {
                if( !IsListEmpty( &grupa->produkty ) && ( !grupa->Deleted ) )
                  {
                  for( produkt = (struct ProductList *)grupa->produkty.lh_Head; produkt->pl_node.mln_Succ; produkt = (struct ProductList *)produkt->pl_node.mln_Succ )
                        {
                        if( !(produkt->Deleted ) )
                                {
                                char Matched = TRUE;
                                                                                                        
 
                                // czy sprawdzamy z podanym przez uzytkownika minimalnym limitem?
                                if( UseLimit )
                                   if( produkt->pl_prod.Ilo�� <= limit )
                                          Matched = FALSE;

                                // czy sprawdzamy tylko limity magazynowe (przekroczone)
                                switch( PrzekroczoneLimity )
                                        {
                                        case LIMIT_MIN:
                                                if( produkt->pl_prod.MinEn )
                                                        if( produkt->pl_prod.Ilo�� >= produkt->pl_prod.StanMin )
                                                                Matched = FALSE;
                                                break;

                                        case LIMIT_MAX:
                                                if( produkt->pl_prod.MaxEn )
                                                        if( produkt->pl_prod.Ilo�� <= produkt->pl_prod.StanMax )
                                                                Matched = FALSE;
                                                break;
                                        }


                                // tylko wybrane typu 'zasobow' ;)
                                if( Matched )
                                   {
                                   switch( ProdCzyUslugi )
                                         {
                                         // to i to
                                         case 0:
                                                break;

                                         // tylko towary
                                         case 1:
                                                if( produkt->pl_prod.Typ != TYP_TOWAR )
                                                  Matched = FALSE;
                                                break;

                                         // tylko uslugi
                                         case 2:
                                                if( produkt->pl_prod.Typ != TYP_USLUGA )
                                                   Matched = FALSE;
                                                break;
                                         }
                                   }



                                if( Matched )
                                  {
                                  produkt->pl_prod.Rabat = rabat;
//                  produkt->RealIlo�� = produkt->pl_prod.Ilo��;
//                  produkt->pl_prod.Ilo�� = 1;

                                  DoMethod( dest_lv, _MUIM_List_InsertSingle, produkt, _MUIV_List_Insert_Bottom );
                                  count++;
                                  }
                                }
                        }
                  }
                }
          }

        return( count );

}
//|
/// PreprocessDisplayedList

// przygotowuje do wydruku cennik z aktualnie wyswietlonej listy
// wpisuje podany rabat bezposrednio w pozycje magazynowe
// tak aby parserowe komendy mogly sobie rabat przeliczac
// Moze tak byc, poniewaz, to pole nie jest uzywane przy
// odczycie, jedynie podczas wystawiania faktury tam jest
// wpisywany rabat jakiego user udzielil klientowi, wiec
// zmiana (i ew. zapisanie tak zmienionego magazynu) nie
// boli

int PreprocessDisplayedList( Object *src_lv, Object *dest_lv,
                                                         double limit, char UseLimit, double rabat,
                                                         char ProdCzyUslugi,
                                                         char PrzekroczoneLimity ) 
{
struct ProductList *prod;
int count = 0;
int i;

        for( i=0;;i++ )
          {
          char Matched = TRUE;

          DoMethod( src_lv, _MUIM_List_GetEntry, i, &prod);
          if( !prod )
                break;

          if( !(prod->Deleted) )
                {

                if( UseLimit )
                  if( prod->pl_prod.Ilo�� <= limit )
                        Matched = FALSE;

                        // czy sprawdzamy tylko limity magazynowe (przekroczone)
                        switch( PrzekroczoneLimity )
                                {
                                case LIMIT_MIN:
                                        if( prod->pl_prod.MinEn )
                                                if( prod->pl_prod.Ilo�� >= prod->pl_prod.StanMin )
                                                        Matched = FALSE;
                                        break;
 
                                case LIMIT_MAX:
                                        if( prod->pl_prod.MaxEn )
                                                if( prod->pl_prod.Ilo�� <= prod->pl_prod.StanMax )
                                                        Matched = FALSE;
                                        break;
                                }


                  // tylko wybrane typu 'zasobow' ;)
                  if( Matched )
                         {
                         switch( ProdCzyUslugi )
                           {
                           // to i to
                           case 0:
                                 break;

                           // tylko towary
                           case 1:
                                 if( prod->pl_prod.Typ != TYP_TOWAR )
                                   Matched = FALSE;
                                 break;

                           // tylko uslugi
                           case 2:
                                 if( prod->pl_prod.Typ != TYP_USLUGA )
                                   Matched = FALSE;
                                 break;
                           }
                         }



                if( Matched )
                  {
                  prod->pl_prod.Rabat = rabat;
//          prod->RealIlo�� = prod->pl_prod.Ilo��;
//          prod->pl_prod.Ilo�� = 1;

                  DoMethod( dest_lv, _MUIM_List_InsertSingle, prod, i );

                  count++;
                  }
                }

          }

        return( count );
}
//|
/*
/// RestoreProcessedEntries

// odkreca zmiany wprowadzone przy drukowaniu cennikow

int RestoreProcessedEntries( Object *src_lv )
{
struct ProductList *prod;
int count = 0;
int i;

        D(bug("RestoreProcessedEntries() "));

        for( i=0;;i++ )
          {
          DoMethod( src_lv, _MUIM_List_GetEntry, i, &prod );
          if( !prod )
                break;

          D(bug("."));
          if( !prod->Deleted )
                prod->pl_prod.Ilo�� = prod->RealIlo��;
          count++;
          }

        D(bug("\n  Done\n"));
        return( count );
}
//|
*/
/// EdycjaMagazynuMain

#define MAG_ZMIANY(x)  {ZmianyWMagazynie += x; set(BT_Mag_Ok, MUIA_Disabled, (ZmianyWMagazynie == 0));}

char EdycjaMagazynuMain(void)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;
long  ZmianyWMagazynie = 0;
char  temp_prod_name[PROD_NAME_LEN] = "";


        set(app, MUIA_Application_Sleep, TRUE);

        {
        struct GrupaList *node = NULL;

        if(!IsListEmpty(&grupy))
           {
           node = (struct GrupaList *)grupy.lh_Head;
           }

        UstawGrup�Aktywn�Magazyn(node);
        }


// Setting up

        set(ST_Mag_Wybierz, MUIA_String_Contents, "");

        set(BT_Mag_Ok     , MUIA_Disabled, TRUE);

        set(MagazynWindow    , MUIA_Window_ActiveObject , LV_Mag_Produkty);
        set(MagazynWindow    , MUIA_Window_DefaultObject, LV_Mag_Produkty);

        if(WinOpen(MagazynWindow))
                {
                while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);
                  switch(ID)
                        {
                        case ID_IMPORT:
                           if( FirmaImport() )
                                 MAG_ZMIANY(1)
                           break;


                        case ID_MAG_PRINT:
                           {
//               int entries = xget(LV_Mag_Produkty, _MUIA_List_Entries);

                           if(grupa_aktywna == NULL)
                                 {
                                 break;
                                 }
                           else
                                 {
                                 struct MagPrintRequester mpr = {0};
                                 struct DocumentPrint     dp  = {0};
                                 int codrukowac;
                                 Object *lv = NewObject(CL_ProductList->mcc_Class, NULL, TAG_DONE);

                                 if( lv )
                                   {
                                   mpr.spis_LimitAktywny = TRUE; // coby losie nie zapominaly wlaczyc i
                                                                                                 // nie drukowaly difoltowo uslug na spisie
                                                                                                 // z natury
                                   codrukowac = MagPrtRequester( &mpr );

                                   if( codrukowac )
                                         {
                                         dp.RodzajDokumentu = mpr.Mode + PRT_CENNIK_DETAL;
                                         strncpy(dp.OsobaWystawiaj�ca, logged_user->user.Nazwa, USER_NAME_LEN);
                                         dp.lv_object = lv;

                                         dp.UseCustomPrinter = TRUE;
                                         dp.PrinterID = mpr.PrtID;

                                         // przygotowalnia listy...
                                         switch( mpr.Mode )
                                           {
///                      cennik detaliczny
                                           case 0:
                                                   {
                                                   strcpy( dp.Nag��wekDokumentu, "Cennik detaliczny" );

                                                   switch( mpr.detal_CoDrukowa� )
                                                           {
                                                           case 0:     //caly magazyn
                                                           case 1:     //caly magazyn
                                                                   PreprocessCalyMagazyn( lv, mpr.detal_Limit, mpr.detal_LimitAktywny, 0.00, mpr.detal_ProdCzyUslugi, LIMIT_OFF );

                                                                   // sortujemy produkty?
                                                                   if( mpr.detal_CoDrukowa� == 1 )
                                                                           DoMethod( lv, _MUIM_List_Sort );
                                                                   break;

                                                           case 2:     // dokladnie to co wyswietlone aktualnie w liscie magazynowej
                                                                   PreprocessDisplayedList( LV_Mag_Produkty, lv, mpr.detal_Limit, mpr.detal_LimitAktywny, 0.00, mpr.detal_ProdCzyUslugi, LIMIT_OFF );
                                                                   break;
                                                           }
                                                   }
                                                   break;
//|
///                      cennik hurtowy
                                           case 1:
                                                   {
                                                   strcpy( dp.Nag��wekDokumentu, "Cennik hurtowy" );

                                                   switch( mpr.hurt_CoDrukowa� )
                                                           {
                                                           case 0:     //caly magazyn
                                                           case 1:
                                                                   PreprocessCalyMagazyn( lv, mpr.hurt_Limit, mpr.hurt_LimitAktywny, mpr.hurt_Rabat, mpr.hurt_ProdCzyUslugi, LIMIT_OFF );

                                                                   // sortujemy produkty?
                                                                   if( mpr.hurt_CoDrukowa� == 1 )
                                                                           DoMethod( lv, _MUIM_List_Sort );

                                                                   break;

                                                           case 2:     // dokladnie to co wyswietlone aktualnie w liscie magazynowej
                                                                   PreprocessDisplayedList( LV_Mag_Produkty, lv, mpr.hurt_Limit, mpr.hurt_LimitAktywny, mpr.hurt_Rabat, mpr.hurt_ProdCzyUslugi, LIMIT_OFF );
                                                                   break;
                                                           }
                                                   }
                                                   break;
//|
///                      stany magazynowe
                                           case 2:
                                                   strcpy( dp.Nag��wekDokumentu, "Stany magazynowe" );

                                                   switch( mpr.stan_CoDrukowa� )
                                                           {
                                                           case 0:     //caly magazyn
                                                           case 1:     //caly magazyn
                                                                   PreprocessCalyMagazyn( lv, mpr.stan_Limit, mpr.stan_LimitAktywny, 0.00, mpr.stan_ProdCzyUslugi, LIMIT_OFF );

                                                                   // sortujemy produkty?
                                                                   if( mpr.stan_CoDrukowa� == 1 )
                                                                           DoMethod( lv, _MUIM_List_Sort );
                                                                   break;

                                                           case 2:     // dokladnie to co wyswietlone aktualnie w liscie magazynowej
                                                                   PreprocessDisplayedList( LV_Mag_Produkty, lv, mpr.stan_Limit, mpr.stan_LimitAktywny, 0.00, mpr.stan_ProdCzyUslugi, LIMIT_OFF );
                                                                   break;
                                                           }
                                                   break;
//|
///                      spis z natury
                                           case 3:
                                                   strcpy( dp.Nag��wekDokumentu, "Spis z natury" );

                                                   switch( mpr.spis_CoDrukowa� )
                                                           {
                                                           case 0:     //caly magazyn
                                                           case 1:     //caly magazyn
                                                                   PreprocessCalyMagazyn( lv, mpr.spis_Limit, mpr.spis_LimitAktywny, 0.00, mpr.spis_ProdCzyUslugi, LIMIT_OFF );

                                                                   // sortujemy produkty?
                                                                   if( mpr.spis_CoDrukowa� == 1 )
                                                                           DoMethod( lv, _MUIM_List_Sort );
                                                                   break;

                                                           case 2:     // dokladnie to co wyswietlone aktualnie w liscie magazynowej
                                                                   PreprocessDisplayedList( LV_Mag_Produkty, lv, mpr.spis_Limit, mpr.spis_LimitAktywny, 0.00, mpr.spis_ProdCzyUslugi, LIMIT_OFF );
                                                                   break;
                                                           }
                                                   break;
//|
///                      limity
                                           case 4:
                                                   strcpy( dp.Nag��wekDokumentu, "Przekroczone limity magazynowe" );

                                                   switch( mpr.minmax_CoDrukowa� )
                                                           {
                                                           case 0:     //caly magazyn
                                                           case 1:     //caly magazyn
                                                                   PreprocessCalyMagazyn( lv, 0.00, FALSE, 0.00, mpr.minmax_ProdCzyUslugi, (LIMIT_MIN + mpr.minmax_TypLimitu) );

                                                                   // sortujemy produkty?
                                                                   if( mpr.minmax_CoDrukowa� == 1 )
                                                                           DoMethod( lv, _MUIM_List_Sort );
                                                                   break;

                                                           case 2:     // dokladnie to co wyswietlone aktualnie w liscie magazynowej
                                                                   PreprocessDisplayedList( LV_Mag_Produkty, lv, 0.00, FALSE, 0.00, mpr.spis_ProdCzyUslugi, (LIMIT_MIN + mpr.minmax_TypLimitu) );
                                                                   break;
                                                           }
                                                   break;
//|
                                           }


                                         DateStamp( &dp.aktualna_data );
                                         WydrukujDokument( &dp );


                                         // copy back
//                     RestoreProcessedEntries( lv );
                                         }

                                   DisposeObject( lv );
                                   }
                                 else
                                   {
                                   DisplayBeep(NULL);
                                   D(bug("Nie moge stworzyc obiektu listy!\n"));
                                   }
                                 }
                           }
                           break;


                        case ID_FIRST_GROUP:
                           if(grupa_aktywna == NULL) break;

                           if(grupa_aktywna != (struct GrupaList *)grupy.lh_Head)
                                 {
                                 UstawGrup�Aktywn�Magazyn((struct GrupaList *)grupy.lh_Head);
                                 }
                           break;

                        case ID_PREV_GROUP:
                           if(grupa_aktywna == NULL) break;

                           if(grupa_aktywna->gm_node.mln_Pred !=  (struct GrupaList *)(&grupy))
                                 {
                                 UstawGrup�Aktywn�Magazyn((struct GrupaList *)grupa_aktywna->gm_node.mln_Pred);
                                 }
                           break;

                        case ID_NEXT_GROUP:
                           if(grupa_aktywna == NULL) break;

                           if(grupa_aktywna->gm_node.mln_Succ != (struct GrupaList *)&grupy.lh_Tail)
                                 {
                                 UstawGrup�Aktywn�Magazyn((struct GrupaList *)grupa_aktywna->gm_node.mln_Succ);
                                 }
                           break;

/*
                        case ID_LAST_GROUP:
                           if(grupa_aktywna == NULL) break;

                           if(grupa_aktywna != (struct GrupaList *)grupy.lh_Tail)
                                 {
                                 UstawGrup�Aktywn�Magazyn((struct GrupaList *)grupy.lh_Tail);
                                 }
                           break;
*/





                        case ID_MAG_DODAJ_GRUP�:
                           {
                           struct GrupaList *grupa = calloc(1, sizeof(struct GrupaList));

                           if(grupa)
                                 {
                                 grupa->gm_grupa.ID = (char)WybierzIDGrupy();

                                 if(grupa->gm_grupa.ID >= 0)
                                   {
                                   grupa->gm_grupa.Vat = settings.def_vat;
/* *** JEDN *** */
//                   strcpy(grupa->gm_grupa.JednMiary, settings.def_miara);
//                   memcpy(&grupa->gm_grupa.Jedn, &settings.def_miara,  &jedn->ul_jedn, sizeof(struct Jednostka));


                                   switch(EdycjaGrupy(grupa, FALSE))
                                           {
                                           case TRUE:
                                                 DodajGrup�(grupa, TRUE);
                                                 UstawGrup�Aktywn�Magazyn(grupa);
                                                 MAG_ZMIANY(1)
                                                 break;

                                           case ADD_ONLY:
                                                 DodajGrup�(grupa, TRUE);
                                                 MAG_ZMIANY(1)
                                                 break;

                                           case FALSE:
                                                 free(grupa);
                                                 break;
                                           }
                                   }
                                 else
                                   {
                                   free(grupa);

                                   DisplayBeep(0);
                                   D(bug("No spare group IDs\n"));
                                   }
                                 }
                           else
                                 {
                                 DisplayBeep(0);
                                 D(bug("No memory for the new group\n"));
                                 }
                           }
                           break;


                        case ID_MAG_USU�_GRUP�:
                           {
                           if(grupa_aktywna)
                                  {
                                  grupa_aktywna->Deleted = ((grupa_aktywna->Deleted + 1) & 0x1);

                                  if(grupa_aktywna->Deleted)
                                        MAG_ZMIANY(1)
                                  else
                                        MAG_ZMIANY(-1)

                                  Wy�wietlNazw�Grupy(TX_Mag_Grupy, grupa_aktywna);
                                  }
                           }
                           break;



                        case ID_MAG_EDYCJA_GRUPY:
                           {
                           if(grupa_aktywna == NULL) break;

                           if(grupa_aktywna->Deleted)
                                   {
                                   MUI_Request(app, MagazynWindow, 0, TITLE, MSG_OK, MSG_MAG_GROUP_DELETED, grupa_aktywna->gm_grupa.Nazwa);
                                   break;
                                   }

                           if(EdycjaGrupy(grupa_aktywna, TRUE))
                                   {
                                   Remove((struct Node *)grupa_aktywna);
                                   Wy�wietlNazw�Grupy(TX_Mag_Grupy, grupa_aktywna);
                                   DodajGrup�(grupa_aktywna, FALSE);
                                   MAG_ZMIANY(1)
                                   }
                           }
                           break;


                        case ID_MAG_ZMIE�_GRUP�:
                           {
                           struct GrupaList *grupa = GroupSelector( grupa_aktywna );

                           if( grupa )
                                   UstawGrup�Aktywn�Magazyn( grupa );
                           }
                           break;









                        case ID_MAG_WYBIERZ:
                           {
                           struct ProductList *prod;
                           char   *str = (char *)xget(ST_Mag_Wybierz, MUIA_String_Contents);
                           int    i;

                           if(!strlen(str))
                                   {
                                   set(LV_Mag_Produkty, _MUIA_List_Active, _MUIV_List_Active_Top);
                                   break;
                                   }

                           for(i = 0; ; i++)
                                 {
                                 DoMethod(LV_Mag_Produkty, _MUIM_List_GetEntry, i, &prod);
                                 if(!prod)
                                   break;

                                   if(StrnCmp(MyLocale, prod->pl_prod.Nazwa, str, -1, SC_COLLATE2) >= 0)
                                         {
                                         set(LV_Mag_Produkty, _MUIA_List_Active, i);
                                         break;
                                         }
                                 }
                           }
                           break;


                        case ID_MAG_ZNAJD�:
                           {
                           if(grupa_aktywna == NULL) break;

                           if(StrnCmp(MyLocale, temp_prod_name, (char *)xget(ST_Mag_Wybierz, MUIA_String_Contents), -1, SC_COLLATE2) == 0)
                                 {
                                 Znajd�ProduktNaLi�cie((char *)xget(ST_Mag_Wybierz, MUIA_String_Contents), LV_Mag_Produkty, xget(LV_Mag_Produkty, _MUIA_List_Active)+1);
                                 }
                           else
                                 {
                                 strcpy(temp_prod_name, (char *)xget(ST_Mag_Wybierz, MUIA_String_Contents));
                                 if(Znajd�ProduktNaLi�cie(temp_prod_name, LV_Mag_Produkty, 0) == -1)
                                         Znajd�ProduktNaLi�cie((char *)xget(ST_Mag_Wybierz, MUIA_String_Contents), LV_Mag_Produkty, xget(LV_Mag_Produkty, _MUIA_List_Active)+1);
                                 }
                           }
                           break;




                        case ID_MAG_FILTER:
                           FiltrProdukty(LV_Mag_Produkty, &mag_filter);
                           break;

                        case ID_MAG_FILTER_ALL:
                           mag_filter.Aktywny = FALSE;
                           ApplyMagFilter(LV_Mag_Produkty, grupa_aktywna, &mag_filter, TRUE);
                           break;





                        case ID_MAG_DODAJ:
                        case ID_MAG_DODAJ_ALT: // alternate ADD - produkt dla grupy uslugowej i vice wers
                           {
                           /* dodaje nowy produkt do grupy */

                           struct ProductList *prod;

                           if(grupa_aktywna == NULL)  break;
                           if(grupa_aktywna->Deleted) break;

                           prod = calloc(1, sizeof(struct ProductList));

                           if(prod)
                                  {
                                  char Typ = grupa_aktywna->gm_grupa.Typ;

                                  prod->Deleted = FALSE;
                                  prod->pl_prod.Grupa = grupa_aktywna->gm_grupa.ID;
                                  prod->pl_prod.Vat = grupa_aktywna->gm_grupa.Vat;

                                  // alternate ADD switching...
                                  if( ID==ID_MAG_DODAJ_ALT )
                                        Typ = (Typ==TYP_TOWAR) ? TYP_USLUGA : TYP_TOWAR;

                                  prod->pl_prod.Typ = Typ;
//                  strcpy(prod->pl_prod.JednMiary, grupa_aktywna->gm_grupa.JednMiary);
                                  memcpy(&prod->pl_prod.Jedn, &grupa_aktywna->gm_grupa.Jedn, sizeof(struct Jednostka));



                                  if(EdycjaProduktu(prod, FALSE))
                                         {
                                         long pos = DodajProdukt(prod, FALSE);

                                         DoMethod(LV_Mag_Produkty, _MUIM_List_InsertSingle, prod, pos);
                                         MakeSmartList(LV_Mag_Produkty, TRUE, FALSE);
                                         set(LV_Mag_Produkty, _MUIA_List_Active, pos);
                                         MAG_ZMIANY(1)
                                         }
                                  else
                                         {
                                         free(prod);
                                         }
                                  }
                           else
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, MagazynWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                                  }
                           }
                           break;


                        case ID_MAG_EDYTUJ:
                           {
                           /* edycja produktu */

                           if(grupa_aktywna == NULL)  break;
                           if(grupa_aktywna->Deleted) break;

                           if(xget(LV_Mag_Produkty, _MUIA_List_Active) != _MUIV_List_Active_Off)
                                   {
                                   struct ProductList *prod;

                                   DoMethod(LV_Mag_Produkty, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &prod);

                                   if(prod->Deleted)
                                         {
                                         if(MUI_Request(app, MagazynWindow, 0, TITLE, MSG_MAG_PROD_DELETED_GAD , MSG_MAG_PROD_DELETED, prod->pl_prod.Nazwa))
                                           {
                                           prod->Deleted = FALSE;
                                           DoMethod(LV_Mag_Produkty, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                                           MAG_ZMIANY(-1)
                                           }
                                         else
                                           break;
                                         }

                                   switch(EdycjaProduktu(prod, TRUE))
                                         {
                                         case TRUE:
                                                 DoMethod(LV_Mag_Produkty, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                                                 MAG_ZMIANY(1)
                                                 break;

                                         case REFRESH:
                                                 {
                                                 long pos;
                                                 set(LV_Mag_Produkty, _MUIA_List_Quiet, TRUE);

                                                 DoMethod(LV_Mag_Produkty, _MUIM_List_Remove, _MUIV_List_Remove_Active);

                                                 Remove((struct Node*)prod);
                                                 pos = DodajProdukt(prod, TRUE);

                                                 DoMethod(LV_Mag_Produkty, _MUIM_List_InsertSingle, prod, pos);
                                                 set(LV_Mag_Produkty, _MUIA_List_Active, pos);

                                                 set(LV_Mag_Produkty, _MUIA_List_Quiet, FALSE);
                                                 MAG_ZMIANY(1)
                                                 }
                                                 break;
                                         }

                                   MakeSmartList(LV_Mag_Produkty, TRUE, TRUE);
                                   }
                           }
                           break;


                        case ID_MAG_SKASUJ:
                        case ID_MAG_SKASUJ_AND_SKIP:
                           {
                           if(grupa_aktywna == NULL)  break;
                           if(grupa_aktywna->Deleted) break;

                           if(xget(LV_Mag_Produkty, _MUIA_List_Active) != _MUIV_List_Active_Off)
                                   {
                                   struct ProductList *prod;

                                   DoMethod(LV_Mag_Produkty, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &prod);

                                   prod->Deleted = ((prod->Deleted + 1) & 0x1);

                                   if(prod->Deleted)
                                           MAG_ZMIANY(1)
                                   else
                                           MAG_ZMIANY(-1)

                                   DoMethod(LV_Mag_Produkty, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);

                                   if(ID == ID_MAG_SKASUJ_AND_SKIP)
                                           set(LV_Mag_Produkty, _MUIA_List_Active, _MUIV_List_Active_Down);
                                   }
                           }
                           break;



                        case ID_MOVE:
                           {
                           struct GrupaList *grupa = GroupSelector(grupa_aktywna);

                           if(grupa_aktywna == NULL)  break;
                           if(grupa_aktywna->Deleted) break;

                           if(grupa)
                                   {
                                   int    entries;

                                   get(LV_Mag_Produkty, _MUIA_List_Entries, &entries);

                                   if(entries)
                                           {
                                           long   selected;

                                           DoMethod(LV_Mag_Produkty, _MUIM_List_Select, _MUIV_List_Select_All, _MUIV_List_Select_Ask, &selected);

                                           if(selected)
                                                   {
                                                   struct ProductList *prod;
                                                   struct GrupaList *tmp = grupa_aktywna;

                                                   set(LV_Mag_Produkty, _MUIA_List_Quiet, TRUE);

                                                   for(; entries>0; entries--)
                                                          {
                                                          long state;

                                                          get(LV_Mag_Produkty, _MUIA_List_Active, &state);
                                                          if(state != _MUIV_List_Active_Off)
                                                                DoMethod(LV_Mag_Produkty, _MUIM_List_Select, state, _MUIV_List_Select_On, NULL);

                                                          DoMethod(LV_Mag_Produkty, _MUIM_List_Select, entries-1, _MUIV_List_Select_Ask, &state);

                                                          if(state == _MUIV_List_Select_On)
                                                                {
                                                                DoMethod(LV_Mag_Produkty, _MUIM_List_GetEntry, entries-1, &prod);

                                                                prod->pl_prod.Grupa = grupa->gm_grupa.ID;

                                                                Remove((struct Node *)prod);
                                                                grupa_aktywna = grupa;
                                                                DodajProdukt(prod, FALSE);
                                                                grupa_aktywna = tmp;

                                                                DoMethod(LV_Mag_Produkty, _MUIM_List_Remove, entries-1);

                                                                MAG_ZMIANY(1)

                                                                selected--;

                                                                if(selected == 0)
                                                                   break;
                                                                }
                                                          }

                                                   set(LV_Mag_Produkty, _MUIA_List_Quiet, FALSE);
                                                   }
                                           }
                                   }
                           }
                           break;          



                        case ID_OK:
                           if(ZmianyWMagazynie)
                                 {
                                 if(MUI_Request(app, MagazynWindow, 0, TITLE, MSG_MAG_SAVE_GAD, MSG_MAG_SAVE))
                                   {
                                   Usu�SkasowaneElementyMagazynowe();

                                   if(!wfmh_isenv("Golem/NieAktualizujJednostek"))
                                          ZaktualizujJednostkiWMagazynie( NULL );

                                   ZapiszMagazynProgress();
                                   result  = TRUE;
                                   running = FALSE;
                                   }
                                 }
                           break;


                        case ID_CANCEL:
                           if(ZmianyWMagazynie)
                                   {
                                   if(MUI_Request(app, MagazynWindow, 0, TITLE, MSG_MAG_CANCEL_REQ_GAD, MSG_MAG_CANCEL_REQ))
                                         {
                                         WczytajMagazyn();
                                         }
                                   else
                                         {
                                         break;
                                         }
                                   }
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

           set(MagazynWindow, MUIA_Window_Open, FALSE);
           DoMethod(LV_Mag_Produkty, _MUIM_List_Clear, NULL);
           }
        else
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           }

        set(app, MUIA_Application_Sleep, FALSE);

        return(result);

}
//|
/// EdycjaMagazynu
char EdycjaMagazynu(void)
{
char result = FALSE;


        if(CreateMagazynWindow())
           {
           result = EdycjaMagazynuMain();
           DisposeMagazynWindow();
           }
        else
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           }

        return(result);

}
//|
                                                                                                
 
