
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

#define NO_MT_MACROS
#include "golem.h"

#include "golem_inspektor_revision.h"

char *ScrTitle = "Golem Inspektor � " INSPEKTOR_VERSIONREVISION " (" INSPEKTOR_DATE ") � � 2000-2002 Marcin Or�owski <carlos@wfmh.org.pl>";


struct Library *MUIMasterBase  = NULL;

char *list_class     = MUIC_List;
char *listview_class = MUIC_Listview;

extern char *Title2;

Object *app, *MainWindow = NULL;

/// CheckNList

/*
** sprawdza czy s� dost�pne klasy NList(view) v19+
** jesli tak, to ustawia zmienne list_class i listview_class
** by wskazywalny na klasy NList
**
** zwraca BOOL
*/

char CheckNList(void)
{
Object *o1;
Object *o2;
char   result = FALSE;


    o1 = NListObject, End;
    o2 = NListviewObject, End;

    if( o1 && o2 )
       {
       ULONG ver;

       get( o1, MUIA_Version, &ver);

       if( ver >= 19 )
           {
           list_class     = MUIC_NList;
           listview_class = MUIC_NListview;

           result = TRUE;
           }
       }

    if( o1 )
       MUI_DisposeObject( o1 );

    if( o2 )
       MUI_DisposeObject( o2 );

    return(result);

}


//|
// main

void main( void )
{


    MUIMasterBase = (APTR) OpenLibrary( MUIMASTER_NAME, MUIMASTER_VMIN );


    if( MUIMasterBase )
       {
       CheckNList();

       if( ! ( CL_ResultList = Inspector_CreateClass( list_class ) ) )
           {
           printf("Nie mog� utworzy� wewn�trznej klasy MUI!\n");
           }
       else
           {
           // application object is a must ;)
           app = ApplicationObject,
                 MUIA_Application_Title      , "GolemInspektor",
                 MUIA_Application_Version    , VERSTAG,
                 MUIA_Application_Copyright  , "� 2000-2003 W.F.M.H.",
                 MUIA_Application_Author     , "Marcin Orlowski <carlos@wfmh.org.pl>",
                 MUIA_Application_Description, Title2,
                 MUIA_Application_Base       , "GOLEMINSPEKTOR",
                 End;


           // let's go
           if( app )
               {
               MUI_Request(app, NULL, 0, Title2, "*_Ok",
                   "\033c\033u\033b%s (%s)\033n\n\n"
                   "Ten program pozwoli Ci sprawdzi�\n"
                   "czy posiadasz zainstalowane wszystkie\n"
                   "niezb�dne do poprawnej pracy pakietu\n"
                   "Golem biblioteki oraz klasy MUI\n"
                   "\n"
                   "\033bhttp://golem-amiga.sf.net/\033n",
                   
                   VERS, DATE
                   );


               Inspektor( NULL );


               MUI_DisposeObject( app );
               }
           else
               {
               printf("Nie mog� utworzy� aplikacji MUI!\n");
               }
           }
       }
    else
       {
       printf("** Nie mog� otworzy� biblioteki %s, v%ld\n", MUIMASTER_NAME, MUIMASTER_VMIN );
       exit(20);
       }


    if( MUIMasterBase )  CloseLibrary( MUIMasterBase  );


    exit(0);
}
//|

