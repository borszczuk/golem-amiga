
/* $Id: golem_protos.h,v 1.1 2003/01/01 20:40:54 carl-os Exp $ */                  


#define SAVEDS __saveds
#define ASM __asm
#define REG(x) register __ ## x

extern struct SystemInfo sys;
extern struct Settings   settings;
extern struct PrtCodes   pcodes;

extern char BaseScreenTitle[];
extern char ScreenTitle[];

extern struct Locale  *MyLocale;
extern struct Catalog *MyCatalog;


extern char *list_class;
extern char *listview_class;
extern int  using_nlist;

extern Object *app;

extern char *EMPTY[];
extern char *VatTable[];
extern char *VatTablePosnet[];
extern char VatValTable[];

extern char *Spos�bZap�aty[];


extern struct DateStamp Today;


extern struct UserList *logged_user;

extern struct List         jedn_miary;
extern struct List         p�atno�ci;
extern struct List         kontrahenci;
extern struct List         u�ytkownicy;
extern struct List         grupy;
extern struct List         drukarki;
extern struct GrupaList    *grupa_aktywna;
extern char   IDGrup[256];
extern char   IDPrinter[256];

#ifdef SQL
extern struct MsqlConnection *co;
#endif


extern ULONG numer_miesiac;
extern ULONG numer_rok;


/* golem_readkey.c */

extern struct keyfile kf;

/* golem_keyparse.c */

char *key_getserial( void );
char *key_getowner( void );
int  IsRegistered( void );
char LoadKey(void);
char InitKeyName(void);
struct keyfile *GetKeyfileData(void);


/* golem_zakupy.c */

char Zakupy(void);


/* golem_importer.c */

char FirmaImport(void);


/* golem_przelewy.c */

void PrzelewLuzem( void );


/* golem_bank.c */

char WczytajBankInfo(char *NumerKonta, struct BankInfo *info);
char WczytajBank(char *NumerKonta, struct Bank *info);


/* golem_posnet.c */

char posnet_RaportZmiany(void);
char posnet_StanKasy(void);
char posnet_DisplayIdle(void);
char posnet_DisplayAbout(void);
char posnet_OpenDrawer(void);
char WydrukujParagon(struct DocumentPrint *dp);
char RaportFiskalny(char TypRaportu);


/* golem_parser.c */

void PrzeliczDokument(struct DocumentPrint *dp);
void WydrukujDokument(struct DocumentPrint *dp);


/* golem_requesters.c */

char StringRequest(struct StringRequest *req);
int  SortRequester(char TypRequestera, ULONG def);
int  DocRequester(char TypRequestera, ULONG def);
int  MagPrtRequester( struct MagPrintRequester *req );


/* golem_dokumenty.c */

char Sprawd�CzyKoniecMiesi�caRoku(void);
char ZamknijMiesiac(void);
char ZamknijRok(void);



/* golem_drukarka.c */

int WczytajDrukarki( void );
char ZapiszKodyDrukarki(void);
void SetSpoolerName(struct Printer *printer, char *name);

char OpenPrinter( struct Printer *printer );

void ClosePrinter(struct Printer *prt);
char CheckPrinter(struct Printer *prt, Object *win);
char PrintLine(struct DocumentPrint *dp, char *line);
char PrintLastPage(struct DocumentPrint *dp);

struct PrinterList *MatchPrinterForDocument( char DocType );
void SetDefPrtSettings( struct PrinterList *printer );
int DodajDrukark�( struct PrinterList *printer );
struct PrinterList *Znajd�Drukark�Nazwa(char *Nazwa);
struct PrinterList *Znajd�Drukark�ID( ULONG ID );
int Usu�Drukarki(char Flags);
char EdycjaDrukarki(struct PrinterList *printer, char Mode);
int ZapiszDrukarki( void );
short WybierzIDDrukarki(void);
char EdycjaDrukarek(void);



/* golem_redagowanie.c */

extern char *RodzajeDokument�wTable[];
char Redagowanie(char TypDokument�w);
long WczytajDokument(struct DocumentPrint *dp, char *nazwa);


/* golem_u�ytkownicy.c */

void DodajAdministratora(void);
char WczytajU�ytkownik�w(void);
char ZapiszU�ytkownik�w(void);
char U�ytkownicy(void);
struct UserList *Login(void);
void Logout(void);
void Usu�U�ytkownik�w(char DeleteAll);
void ToolBarLogin(char mode);


/* golem_p�atno�ci.c */

int    DodajP�atno��(struct P�atno��List *p�at);
struct P�atno��List *Znajd�P�atno��Nazwa(char *Nazwa);
struct P�atno��List *Znajd�P�atno��Numer(int Numer);
void   Usu�P�atno�ci(char DeleteAll);

char EdycjaP�atno�ci(struct P�atno��List *p�at, char EditMode);
long WczytajP�atno�ci(void);
char ZapiszP�atno�ci(void);



/* golem_jednostki.c */

int DodajJednostk�(struct JednostkaList *jedn);
struct JednostkaList *Znajd�Jednostk�Nazwa(char *Nazwa);
void Usu�Jednostki(char DeleteAll);
char EdycjaJednostki(struct JednostkaList *jedn, char EditMode);
int ZaktualizujJednostkiWMagazynie( struct JednostkaList *Jedn );

long WczytajJednostki(void);
char ZapiszJednostki(void);


/* Golem_Ustawienia.c */

long WczytajUstawienia(void);
char EdycjaUstawie�(void);
char EdycjaNumeracji(void);

char UstawieniaJedn(void);
char UstawieniaP�atno�ci(void);


/* Golem_GUI.c */

extern char *RodzajeDokument�wSpoolTable[];
extern char *RodzajeDokument�wLVTable[];
extern char *RA_Sort_Zakupy[];
extern char *RA_Sort_DocScan[];

void   SetupAboutScroll(void);
Object *CreateApp(void);


/* Golem.c */

extern struct MUI_CustomClass *CL_CustomerList;
extern struct MUI_CustomClass *CL_GroupList;
extern struct MUI_CustomClass *CL_ProductList;
extern struct MUI_CustomClass *CL_FakturaProductList;
extern struct MUI_CustomClass *CL_UsersList;
//extern struct MUI_CustomClass *CL_UnitList;
extern struct MUI_CustomClass *CL_PayList;
extern struct MUI_CustomClass *CL_DocumentList;
extern struct MUI_CustomClass *CL_OdbiorcyList;
extern struct MUI_CustomClass *CL_SmartCycle;
extern struct MUI_CustomClass *CL_BString;
extern struct MUI_CustomClass *CL_ZakupyList;

char *Price2String(double val);
double String2Price(char *string);
char *Mar�a2String(double val);
double String2Mar�a(char *string);


long WczytajCount(char *FileName);
char ZapiszCount(char *FileName, long Count);

long WczytajNumeracj�Dokument�w(void);
char ZapiszNumeracj�Dokument�w(void);

char WczytajSystemInfo(void);
char ZapiszSystemInfo(void);
char ZapiszLastOperationToday(void);


void DeleteClasses(void);
char SmartMakeDir(char *DirName);


/* Golem_faktura.c */

extern struct Documents docs;

char Sprzeda�(char RodzajDokumentu, char *filename);
void __saveds __asm FormatDate_Func(register __a1 char znak, register __a0 struct Hook *hook);
long Drukowanie(struct DrukowanieRequest *dr);
char *S�ownie(double number);
void ZbudujNumerDokumentu(struct DocumentPrint *dp);
char ZapiszDokument(struct DocumentPrint *dp, char Od���);
char DocumentExists(struct DocumentPrint *dp);

char Nag��wekDokumentu(struct DocumentPrint *main_dp);
void Zwi�kszNumeracj�(struct DocumentPrint *dp, struct DrukowanieRequest *dr);

void Korekta( void );


/* Golem_Magazyn.c */

long Znajd�ProduktNaLi�cie(char *str, Object *list, char StartPos);
struct ProductList *Znajd�ProduktIndex(char *Index);
struct ProductList *Znajd�ProduktNazwa(char *Nazwa);
struct ProductList *Znajd�ProduktNazwaWMagazynie(char *Nazwa);
struct ProductList *Znajd�ProduktIndexWMagazynie(char *Index);
int Znajd�ProduktListaAdres(Object *lv, struct ProductList *prod);
long Znajd�ProduktNaLi�cieIndex(char *str, Object *list, char StartPos);
char EdycjaProduktu(struct ProductList *prod, char EditMode);

int DodajProdukt( struct ProductList *prod, char Quiet );
int DodajGrup�(struct GrupaList *grupa, char NewGroup);
struct GrupaList *Znajd�Grup�Nazwa(char *Nazwa);
void UstawGrup�Aktywn�Magazyn(struct GrupaList *grupa);
short WybierzIDGrupy(void);






void ApplyMagFilter(Object *lista, struct GrupaList *grupa, struct Filtr *filtr, char EditMode);

char FiltrProdukty(Object *lista, struct Filtr *filtr);
//void Wy�wietlNazw�Grupy(Object *obj, struct GrupaList *grupa);
struct GrupaList *InitMagazyn(void);
void Usu�Magazyn(void);
void Usu�SkasowaneElementyMagazynowe(void);
int  WczytajMagazyn(void);
int  ZapiszMagazyn(void);
int  ZapiszMagazynProgress( void );
char EdycjaMagazynu(void);

double ObetnijKo�c�wk�(double kwota);
double CalcVat(double _cena, char _vat);
double CalcMar�a(double zakup, double mar�a);
double CalcMar�aProcent(double zakup, double mar�a);
double CalcBrutto(double zakup, double mar�a, char vat);
double CalcRabat(double cena, double rab);

struct MagScan *MagazynScan(char *pattern, char IndexScan);


/* Golem_Klienci.c */

struct KlientList *Znajd�KlientaNazwa(char *Nazwa);
int DodajKlienta(struct KlientList *klient);
char ZapiszKlient�w(void);
char EdycjaKlienta(struct KlientList *klient, char EditMode, char Flags);
int  ApplyCustFilter(Object *lista, struct Filtr *filtr, struct Klient *def_klient);
char CustFiltr(Object *lista, struct Filtr *filtr);
int  Usu�Klient�w(char Flags);
char WczytajKlient�w(void);
char EdycjaKontrahent�w(void);
char *Str2NIP(char *str);
char *NIP2Str(char *str, char Sp��kaCywilna);
void CopyDefCustomerSettings(struct Klient *klient);
char IsNIPValid( char *NIP );
char UpdateSeparators( struct Filtr *filtr );



/* Golem_Selectors.c */

struct GrupaList *GroupSelector(struct GrupaList *exclude);
char *OdbiorcaSelector(struct DocumentPrint *dp);
//struct Klient *KontrahentSelector(struct Filtr *sel_cust_filter, struct Klient *def_klient, char Flags);
struct KlientList *KontrahentSelector(struct Filtr *sel_cust_filter, struct KlientList *def_klient, char Flags);

struct JednostkaList *JednostkaSelector(struct Jednostka *def_jedn);
struct P�atno��List *P�atno��Selector(struct P�atno�� *def_jedn);
struct PrinterList *DrukarkaSelector(struct PrinterList *printer);
short  GetPrinterID( char *Nazwa );
char * GetPrinterName( short ID );
char *RequestAslFile( char *path, char DrawersOnly, char SaveMode );


char DokumentSelector( struct DocumentScan *ds,
                       char TypDokument�w,
                       char FVAT_Limit,
                       ULONG (*CallbackFunction)()
                     );


/* Golem_inspektor.c */

extern struct MUI_CustomClass *CL_ResultList;

struct MUI_CustomClass * Inspector_CreateClass( char *list_class );
char   Inspektor( Object *ParentWindow );


/* Golem_Misc.c */

void DumpDP(struct DocumentPrint *dp);

STRPTR GetString(LONG stringNum);
LONG xget(Object *obj, ULONG attribute);
void setnumstring(Object *obj, long val);

Object *MakeLabel2   (char *num);
Object *EmptyLabel(void);
Object *PopButton2(int img, char *key);
Object *MakeCheck(char *num);
Object *MakeCycle(char **array,char *num);
Object *MakeCycleWeight(char **array, char *num, long weight);
Object *MakeString(int maxlen, char *num);
Object *MakeStringSH( int maxlen, char *str, char *help );
Object *MakeCashStringWeight(int maxlen, char *num, int weight);
Object *MakeCashString(int maxlen, char *num);
Object *MakeSignedCashString(int maxlen, char *num);
Object *MakeStringAccept(int maxlen, char *num, char *accept);
Object *MakeStringAcceptWeight(int maxlen, char *num, char *accept, int weight);
Object *MakeStringReject(int maxlen, char *num, char *reject);
Object *MakeStringSecret(int maxlen, char *num);
Object *MakeNumericString(int maxlen, char *num);
Object *MakeNumericStringSH( int maxlen, char *str, char *help );
Object *MakeStringKey(int maxlen, char *key);
Object *TextButton(char *num);
Object *TextButtonWeight(char *num, int weight);
Object *TextButton2(char *num, char *num2);
Object *SizedButton(char *num);
Object *ToggleButton(char *num);
Object *MakeAmountString(int maxlen, char *num);
Object *MakeAmountStringWeight(int maxlen, char *num, int weight);

Object *DoString(int maxlen, char *num, ULONG NoInput);
Object *DoCashString( int maxlen, char *num, ULONG NoInput );
Object *DoStringAccept(int maxlen, char *num, char *accept, ULONG NoInput );
Object *DoStringReject(int maxlen, char *num, char *reject, ULONG NoInput);
Object *DoNumericString(int maxlen, char *num, ULONG NoInput);






/*
SAVEDS ASM LONG StrObjFunc_Mag(REG(a2) Object *pop,REG(a1) Object *tx);
SAVEDS ASM VOID ObjStrFunc_Mag(REG(a2) Object *pop,REG(a1) Object *tx);

SAVEDS ASM LONG StrObjFunc_Fak(REG(a2) Object *pop,REG(a1) Object *tx);
SAVEDS ASM VOID ObjStrFunc_Fak(REG(a2) Object *pop,REG(a1) Object *tx);

SAVEDS ASM LONG StrObjFunc_Odb_Cust(REG(a2) Object *pop,REG(a1) Object *tx);
SAVEDS ASM VOID ObjStrFunc_Odb_Cust(REG(a2) Object *pop,REG(a1) Object *tx);
*/

#ifndef NO_GUI_PROTOS_H
#define GUI_PROTOS_H

#include "golem_gui.h"

#undef GUI_PROTOS_H
#endif

