
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
** $Id: golem_selectors.c,v 1.1 2003/01/01 20:40:52 carl-os Exp $.
*/

#include "golem.h"


/// OdbiorcaSelector

char *OdbiorcaSelector(struct DocumentPrint *dp)
{
/*
** Otwiera okno wyboru osoby odbieraj�cej dokument.
** Zwraca STRPTR lub NULL
*/

Object *OdbiorcaSelectorWindow,    /* ODBIORCA SELECTOR */
           *LV_Pod_Odbiorcy,
           *BT_Pod_Ok,
           *BT_Pod_Add,
           *BT_Pod_Cancel;

char  running = TRUE;
ULONG signal  = 0;
char  *result = NULL;

static char  Odb1[CUST_NAME_LEN];
static char  Odb2[CUST_NAME_LEN];
static char  Odb3[CUST_NAME_LEN];
static char  Odb4[CUST_NAME_LEN];
char  NeedSave   = FALSE;          // czy by�y zmiany w danych odbiorc�w i trzeba zapisa� baz�?
char  ClearUpVat = FALSE;          // czy up. vat przeterminowane? je�li tak,
                                                                   // to przy zapisie bazy, trzeba je wy��czy� w ustawieniach klienta

struct Klient *klient = &dp->Odbiorca->kl_klient;

        set(app, MUIA_Application_Sleep, TRUE);

///   OdbiorcaSelectorWindow

//                 MUIA_Application_Window,
                                         OdbiorcaSelectorWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_ODB_TITLE,
                                                MUIA_Window_ID         , ID_WIN_ODBIORCASELECTOR,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
//                        MUIA_Window_CloseGadget, FALSE,
                                                WindowContents,
                                                   VGroup,

                                                   Child, VGroup,
                                                                  GroupFrameT(MSG_ODB_SEL_TITLE),

                                                                  Child, LV_Pod_Odbiorcy = _ListviewObject,
                                                                                                                   MUIA_CycleChain, TRUE,
                                                                                                                   _MUIA_Listview_List, _ListObject,
                                                                                                                                 MUIA_Frame, MUIV_Frame_InputList,
                                                                                                                                 End,
                                                                                                                   End,
                                                                  End,

                                                                  Child, HGroup,
                                                                                 Child, BT_Pod_Ok     = TextButton(MSG_ODB_OK),
                                                                                 Child, BT_Pod_Add    = TextButton(MSG_ODB_ADD),
                                                                                 Child, BT_Pod_Cancel = TextButton(MSG_ODB_CANCEL),
                                                                                 End,

                                                   End,
                                          End;
//|

        if(!OdbiorcaSelectorWindow)
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(FALSE);      // cancel
           }

        /* ODBIORCA SELECTOR */

        DoMethod(OdbiorcaSelectorWindow   ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(LV_Pod_Odbiorcy , MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(OdbiorcaSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ADD);
        DoMethod(BT_Pod_Ok       ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Pod_Add      ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);
        DoMethod(BT_Pod_Cancel   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        _attachwin(OdbiorcaSelectorWindow);


        strcpy(Odb1, klient->Odb1);
        strcpy(Odb2, klient->Odb2);
        strcpy(Odb3, klient->Odb3);
        strcpy(Odb4, klient->Odb4);


        DoMethod(LV_Pod_Odbiorcy, _MUIM_List_Clear, NULL);

        if(Odb1[0] != '\0') DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb1, _MUIV_List_Insert_Bottom);
        if(Odb2[0] != '\0') DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb2, _MUIV_List_Insert_Bottom);
        if(Odb3[0] != '\0') DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb3, _MUIV_List_Insert_Bottom);
        if(Odb4[0] != '\0') DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb4, _MUIV_List_Insert_Bottom);


//    set(LV_Pod_Odbiorcy, _MUIA_List_Active, _MUIV_List_Active_Top);
        set(BT_Pod_Add, MUIA_Disabled, (klient->DopisywanieDozwolone == FALSE));


        if(WinOpen(OdbiorcaSelectorWindow))
                {
                // sprawdzamy czy jest up. vat i czy jest jeszcze wa�ne
                if(klient->Upowa�nienieVAT)
                         {
                         if(klient->Upowa�nienieBezLimitu == FALSE)
                           {
                           struct DateStamp ds = {0};

                           ds.ds_Days = klient->UpVat_ExpireDate;

                           if( CompareDates( wfmh_GetCurrentTime(), &ds) > 0)
                                  {
                                  DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, settings.upvat_text, _MUIV_List_Insert_Top);
                                  }
                           else
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, OdbiorcaSelectorWindow, 0, TITLE, MSG_OK, MSG_ODB_UPVAT_EXP, wfmh_Date2Str(&ds));

                                  ClearUpVat = NeedSave = TRUE;
                                  }
                           }
                         else
                           {
                           DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, settings.upvat_text, _MUIV_List_Insert_Top);
                           }

                         }

                set(LV_Pod_Odbiorcy, _MUIA_List_Active, _MUIV_List_Active_Top);


                if( xget(LV_Pod_Odbiorcy, _MUIA_List_Entries) > 0 )
                  {
                  set(OdbiorcaSelectorWindow, MUIA_Window_ActiveObject, LV_Pod_Odbiorcy);
                  }
                else
                  {
                  set(OdbiorcaSelectorWindow, MUIA_Window_ActiveObject, BT_Pod_Add);
                  }


                while(running)
                  {
                  switch (DoMethod(app, MUIM_Application_Input, &signal))
                        {
                        case ID_ADD:
                           {
                           static struct StringRequest str={0};

                           str.WindowTitle = MSG_ODB_ADD_TITLE;
                           strcpy(str.String, "");
                           str.Flags       = STR_REQ_NOZERO;
                           str.MaxLen      = CUST_NAME_LEN;


                           if(StringRequest(&str))
                                   {
                                   if(Odb1[0] == '\0')
                                           {
                                           strcpy(Odb1, str.String);
                                           DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb1, _MUIV_List_Insert_Bottom);
                                           set(LV_Pod_Odbiorcy, _MUIA_List_Active, _MUIV_List_Active_Bottom);
                                           NeedSave = TRUE;
                                           break;
                                           }

                                   if(Odb2[0] == '\0')
                                           {
                                           strcpy(Odb2, str.String);
                                           DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb2, _MUIV_List_Insert_Bottom);
                                           set(LV_Pod_Odbiorcy, _MUIA_List_Active, _MUIV_List_Active_Bottom);
                                           NeedSave = TRUE;
                                           break;
                                           }

                                   if(Odb3[0] == '\0')
                                           {
                                           strcpy(Odb3, str.String);
                                           DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb3, _MUIV_List_Insert_Bottom);
                                           set(LV_Pod_Odbiorcy, _MUIA_List_Active, _MUIV_List_Active_Bottom);
                                           NeedSave = TRUE;
                                           break;
                                           }

                                   if(Odb4[0] == '\0')
                                           {
                                           strcpy(Odb4, str.String);
                                           DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb4, _MUIV_List_Insert_Bottom);
                                           set(LV_Pod_Odbiorcy, _MUIA_List_Active, _MUIV_List_Active_Bottom);
                                           NeedSave = TRUE;
                                           break;
                                           }

                                   if(klient->FIFO)
                                           {
                                           strcpy(Odb1, Odb2);
                                           strcpy(Odb2, Odb3);
                                           strcpy(Odb3, Odb4);
                                           strcpy(Odb4, str.String);

                                           NeedSave = TRUE;

                                           set(LV_Pod_Odbiorcy, _MUIA_List_Quiet, TRUE);
                                           DoMethod(LV_Pod_Odbiorcy, _MUIM_List_Clear, NULL);


                                           // sprawdzamy czy jest up. vat i czy jest jeszcze wa�ne
                                           if(klient->Upowa�nienieVAT)
                                                        {
                                                        if(klient->Upowa�nienieBezLimitu == FALSE)
                                                          {
                                                          struct DateStamp ds = {0};

                                                          ds.ds_Days = klient->UpVat_ExpireDate;

                                                          if(CompareDates( wfmh_GetCurrentTime(), &ds) > 0)
                                                                 {
                                                                 DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, settings.upvat_text, _MUIV_List_Insert_Top);
                                                                 }
                                                          else
                                                                 {
                                                                 DisplayBeep(0);
                                                                 MUI_Request(app, OdbiorcaSelectorWindow, 0, TITLE, MSG_OK, MSG_ODB_UPVAT_EXP, wfmh_Date2Str(&ds));

                                                                 ClearUpVat = NeedSave = TRUE;
                                                                 }
                                                          }
                                                        else
                                                          {
                                                          DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, settings.upvat_text, _MUIV_List_Insert_Top);
                                                          }
                                                        }

                                           DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb1, _MUIV_List_Insert_Bottom);
                                           DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb2, _MUIV_List_Insert_Bottom);
                                           DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb3, _MUIV_List_Insert_Bottom);
                                           DoMethod(LV_Pod_Odbiorcy, _MUIM_List_InsertSingle, Odb4, _MUIV_List_Insert_Bottom);
                                           set(LV_Pod_Odbiorcy, _MUIA_List_Active, _MUIV_List_Active_Bottom);
                                           set(LV_Pod_Odbiorcy, _MUIA_List_Quiet, FALSE);
                                           }
                                   else
                                           {
                                           DisplayBeep(0);
                                           if(MUI_Request(app, OdbiorcaSelectorWindow, 0, TITLE, MSG_ODB_NOFIFO_GAD, MSG_ODB_NOFIFO, str.String))
                                                   {
                                                   static char Jednoraz�wka[CUST_NAME_LEN];

                                                   strcpy(Jednoraz�wka, str.String);
                                                   result = Jednoraz�wka;

                                                   goto _sel_cust_ok;
                                                   }
                                           }

                                   set(OdbiorcaSelectorWindow, MUIA_Window_ActiveObject, BT_Pod_Ok);
                                   }
                           }
                           break;


                        case ID_OK:
                                 {
                                 DoMethod(LV_Pod_Odbiorcy, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &result);

_sel_cust_ok:
                                 if(NeedSave)
                                   {
                                   strcpy(klient->Odb1, Odb1);
                                   strcpy(klient->Odb2, Odb2);
                                   strcpy(klient->Odb3, Odb3);
                                   strcpy(klient->Odb4, Odb4);

                                   klient->Upowa�nienieVAT = FALSE;

                                   ZapiszKlient�w();
                                   }

                                 running = FALSE;
                                 }
                           break;


                        case ID_CANCEL:
                           running = FALSE;
                           break;                                            
                        }
                  if(running && signal) Wait(signal);
                  }
                }
         else
                {
                DisplayBeep(0);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }

        set(OdbiorcaSelectorWindow, MUIA_Window_Open, FALSE);
        DoMethod(LV_Pod_Odbiorcy, _MUIM_List_Clear, NULL);

        _detachwin(OdbiorcaSelectorWindow);

        set(app, MUIA_Application_Sleep, FALSE);

        return(result);
}

//|

/// GROUP SELECTOR STRINGS

#define MSG_SEL_WIN_TITLE    "Wybierz grup�"
#define MSG_SEL_SELECT_TITLE "Dost�pne grupy"
#define MSG_SEL_ACCEPT       "F10 - _Ok"
#define MSG_SEL_CANCEL       "ESC - Ponie_chaj"
//|
/// GroupSelectorSetup

static Object
           *GroupSelectorWindow,          /* GROUP SELECTOR */
           *LV_Sel_Grupy,
           *BT_Sel_Ok,
           *BT_Sel_Cancel;

char GroupSelectorSetup( struct GrupaList *exclude )
{
struct GrupaList *node;
int    count = 0;

///   GroupSelectorWindow

           GroupSelectorWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_SEL_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_GROUPSELECTOR,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
//                        MUIA_Window_CloseGadget, FALSE,
                                                WindowContents,
                                                   VGroup,

                                                   Child, VGroup,
                                                                  GroupFrameT(MSG_SEL_SELECT_TITLE),

                                                                  Child, LV_Sel_Grupy = ListviewObject,
                                                                                                                 MUIA_CycleChain, TRUE,
                                                                                                                _MUIA_Listview_List, NewObject(CL_GroupList->mcc_Class, NULL,
                                                                                                                                                                 _MUIA_List_Format, "MAW=75 COL=1 BAR,"
                                                                                                                                                                                                        "MAW=25 COL=2 P=\033r",
                                                                                                                                                                 TAG_DONE),
                                                                                                                End,
                                                                  End,

                                                   Child, HGroup,
                                                                  Child, BT_Sel_Ok     = TextButton(MSG_SEL_ACCEPT),
                                                                  Child, BT_Sel_Cancel = TextButton(MSG_SEL_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|


        if( ! GroupSelectorWindow )
           return(FALSE);


        // settings

        if(!IsListEmpty(&grupy))
           {
           for(node = (struct GrupaList *)grupy.lh_Head; node->gm_node.mln_Succ; node = (struct GrupaList *)node->gm_node.mln_Succ)
                   {
                   if(node->Deleted == FALSE)
                         if(node != exclude)
                           {
                           DoMethod(LV_Sel_Grupy, _MUIM_List_InsertSingle, node, _MUIV_List_Insert_Sorted);
                           count++;
                           }
                   }
           }

        set(BT_Sel_Ok, MUIA_Disabled, (count == 0));
        set(LV_Sel_Grupy, _MUIA_List_Active, 0);


        // notifications

        DoMethod(GroupSelectorWindow  ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(LV_Sel_Grupy , MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Sel_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Sel_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        if( count > 0 )
                DoMethod(GroupSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);


        set(GroupSelectorWindow, MUIA_Window_ActiveObject, LV_Sel_Grupy);




        _attachwin( GroupSelectorWindow );

        return(TRUE);
}
//|
/// GroupSelector

struct GrupaList *GroupSelector(struct GrupaList *exclude)
{
/*
** Otwiera okno wyboru grupy. Je�li exclude != NULL
** pomija podan� tak grup� na li�cie.
** Zwraca Struct GrupaList * lub NULL
*/

char  running = TRUE;
ULONG signal  = 0;
struct GrupaList *result = NULL;

        set(app, MUIA_Application_Sleep, TRUE);

        if( !GroupSelectorSetup( exclude ))
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }


        if(WinOpen(GroupSelectorWindow))
                {
                while(running)
                  {
                  switch (DoMethod(app, MUIM_Application_Input, &signal))
                        {
                        case ID_OK:
                                 DoMethod(LV_Sel_Grupy, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &result);
                                 running = FALSE;
                           break;


                        case ID_CANCEL:
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }
                }
         else
                {
                DisplayBeep(0);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


        _detachwin(GroupSelectorWindow);
        set(app, MUIA_Application_Sleep, FALSE);

        return(result);
}

//|

/// KONTRAHENT SELECTOR STRINGS

#define MSG_KON_TITLE     "Wybierz kontrahenta"
#define MSG_KON_SEL_TITLE "Wybierz kontrahenta"

#define MSG_KON_DODAJ     "F1 - Dod_aj"

#define MSG_KON_SEARCH    "_Szukaj"
#define MSG_KON_INFO      "_Info"
#define MSG_KON_SEARCH_BT "F7 - Szukaj"

#define MSG_KON_OK     "F10 - _Ok"
#define MSG_KON_CANCEL "ESC - Ponie_chaj"

#define MSG_KON_FILTER_S  "_S"
#define MSG_KON_FILTER_N  "_N"
#define MSG_KON_FILTER_K  "_K"
#define MSG_KON_FILTER_A  "_A"
#define MSG_KON_FILTER_B  "_B"
#define MSG_KON_FILTER_C  "_C"

#define MSG_KON_FILTER_SHOW  "O_d�wie�"


//|
/// KontrahentSelectorSetup

static Object
           *KontrahentSelectorWindow,  /* KONTRAHENT SELECTOR */
           *LV_Kon_Klienci,
           *ST_Kon_Search,
           *BT_Kon_Info,
           *BT_Kon_Add,
           *BT_Kon_Filter,
           *BT_Kon_FilterAll,
           *BT_Kon_Ok,
           *BT_Kon_Cancel;

char KontrahentSelectorSetup( struct Filtr *sel_cust_filter, struct KlientList *def_klientList )
{
struct Klient *def_klient = &def_klientList->kl_klient;


///   KontrahentSelectorWindow

                                         KontrahentSelectorWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_KON_TITLE,
                                                MUIA_Window_ID         , ID_WIN_KONTRAHENTSELECTOR,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,

                                                   Child, VGroup,
                                                                  GroupFrameT(MSG_KON_SEL_TITLE),

                                                                  Child, LV_Kon_Klienci = ListviewObject,
                                                                                                                  MUIA_CycleChain, TRUE,
                                                                                                                  _MUIA_Listview_List, Golem_CustListObject,
/*
                                                                                                                  _MUIA_List_Format, "MIW=1 MAW=49 BAR,"
                                                                                                                                                         "MIW=1 MAW=35 BAR,"
                                                                                                                                                         "MIW=1 MAW=-1 BAR,"
                                                                                                                                                         "MIW=1 MAW=-1 ",
*/

                                                                                                                                           End,
                                                                                                                  End,

                                                                  Child, HGroup,
                                                                                 Child, MakeLabel2( MSG_KON_SEARCH ),
                                                                                 Child, ST_Kon_Search = MakeString( CUST_NAME_LEN * 2 , MSG_KON_SEARCH ),
                                                                                 Child, BT_Kon_Info = TextButtonWeight( MSG_KON_INFO, 1 ),
                                                                                 End,

                                                                  Child, HGroup,
                                                                                 Child, BT_Kon_Add     = TextButton(MSG_KON_DODAJ),
                                                                                 Child, MUI_MakeObject(MUIO_VBar,1),
                                                                                 Child, HGroup,
                                                                                                MUIA_Weight, 20,
                                                                                                Child, BT_Kon_Filter    = TextButton(MSG_MAG_FILTER),
                                                                                                Child, BT_Kon_FilterAll = TextButton(MSG_MAG_FILTER_ALL),
                                                                                                End,
                                                                                 End,

                                                                  End,

                                                                  Child, HGroup,
                                                                                 Child, BT_Kon_Ok     = TextButton(MSG_KON_OK),
                                                                                 Child, BT_Kon_Cancel = TextButton(MSG_KON_CANCEL),
                                                                                 End,


                                                   End,
                                          End;
//|


        if( ! KontrahentSelectorWindow )
           return( FALSE );


        // values

        set( LV_Kon_Klienci, _MUIA_List_Active, ApplyCustFilter(LV_Kon_Klienci, sel_cust_filter, def_klient) );

        set(KontrahentSelectorWindow, MUIA_Window_ActiveObject , ST_Kon_Search);
        set(KontrahentSelectorWindow, MUIA_Window_DefaultObject, LV_Kon_Klienci);

        DoMethod(BT_Kon_Add, MUIM_MultiSet, MUIA_CycleChain, FALSE, BT_Kon_Add, BT_Kon_Filter, BT_Kon_FilterAll, NULL);


        // notyfikacje

        DoMethod(KontrahentSelectorWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(ST_Kon_Search   , MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_SEARCH);
//    DoMethod(ST_Kon_Search   , MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Window, MUIM_Set, MUIA_Window_ActiveObject, BT_Kon_Ok);
        DoMethod(ST_Kon_Search   , MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_SEARCH_ACK);
         set( ST_Kon_Search      , MUIA_String_AttachedList, LV_Kon_Klienci );
        DoMethod(BT_Kon_Info     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_INFO);


        DoMethod(BT_Kon_Add      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);
        DoMethod(BT_Kon_Filter   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_FILTER);
        DoMethod(BT_Kon_FilterAll, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_FILTER_ALL);

        DoMethod(KontrahentSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ADD);
        DoMethod(KontrahentSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);


        DoMethod(LV_Kon_Klienci  , MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);

        DoMethod(BT_Kon_Ok       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Kon_Cancel   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


        _attachwin( KontrahentSelectorWindow );


        return( TRUE );
}
//|
/// KontrahentSelector

struct KlientList *KontrahentSelector(struct Filtr *sel_cust_filter,
                                                                          struct KlientList *def_klient,
                                                                          char Flags)
{
/*
** Otwiera okno wyboru kontrahenta odbieraj�cego dokument.
** Zwraca Struct Klient * lub NULL
*/

char   running = TRUE;
ULONG  signal  = 0;
struct KlientList *result = NULL;


        set(app, MUIA_Application_Sleep, TRUE);

        if( ! KontrahentSelectorSetup( sel_cust_filter, def_klient ) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }


        if( WinOpen(KontrahentSelectorWindow) )
                {
                while(running)

                  {
                  switch (DoMethod(app, MUIM_Application_Input, &signal))
                        {
                        case ID_INFO:
                           {
                           struct KlientList *klient = NULL;

                           DoMethod(LV_Kon_Klienci, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &klient);

                           if(klient)
                                   {
                                   if( !(klient->kl_klient.Separator) )
                                           {
                                           EdycjaKlienta( klient, 2, 0 );
                                           }
                                   else
                                           {
                                           DisplayBeep( 0 );
                                           }
                                   }
                           }
                           break;


                        case ID_ADD:
                           {
                           struct KlientList *klient;

                           klient = calloc(1, sizeof(struct KlientList));

                           if(klient)
                                  {
                                  CopyDefCustomerSettings(&klient->kl_klient);
                                  klient->Deleted = FALSE;


                                  /* kopiujemy typ klienta z aktualnych ustawie� filtra */

                                  if(settings.clone_filter)
                                         {
                                         klient->kl_klient.Sprzedawca = sel_cust_filter->Sprzedawca;
                                         klient->kl_klient.Nabywca    = sel_cust_filter->Nabywca;
                                         klient->kl_klient.Kosztowiec = sel_cust_filter->Kosztowiec;
                                         klient->kl_klient.A          = sel_cust_filter->A;
                                         klient->kl_klient.B          = sel_cust_filter->B;
                                         klient->kl_klient.C          = sel_cust_filter->C;
                                         }

                                  if(EdycjaKlienta(klient, FALSE, Flags) != FALSE)
                                         {
                                         long pos = DodajKlienta(klient);

                                         UpdateSeparators( NULL );

                                         DoMethod(LV_Kon_Klienci, _MUIM_List_InsertSingle, klient, pos);


                                         if( klient->Tymczasowy == FALSE )
                                           {
                                           ZapiszKlient�w();
                                           }

                                         set(LV_Kon_Klienci, _MUIA_List_Active , pos);
                                         set(KontrahentSelectorWindow, MUIA_Window_ActiveObject, BT_Kon_Ok);
                                         }
                                  else
                                         {
                                         free(klient);
                                         }
                                  }
                           else
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, KontrahentSelectorWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                                  }
                           }
                           break;


                        case ID_CUST_FILTER:
                           CustFiltr(LV_Kon_Klienci, sel_cust_filter);
                           break;

                        case ID_CUST_FILTER_ALL:
                           sel_cust_filter->Aktywny = FALSE;
                           ApplyCustFilter(LV_Kon_Klienci, sel_cust_filter, NULL);
                           break;



                        case ID_CUST_SEARCH:
                           {
                           struct KlientList *node;
                           char   *str = (char *)xget( ST_Kon_Search, MUIA_String_Contents );
                           int    len;
                           char   CheckNIP = FALSE;
                           int    i;

                           len = strlen( str );

                           if( !len )
                                   {
                                   set( LV_Kon_Klienci, _MUIA_List_Active, _MUIV_List_Active_Top );
                                   break;
                                   }


                           // moze po NIPie warto by poszukac?

                           if( len <= 14 )
                                   {
                                   int l;

                                   CheckNIP = TRUE;

                                   for( l=0; l<len; l++ )
                                           {
                                           if( (str[ l ] != '-') && (isdigit( (int)str[ l ]) == FALSE) )
                                                   {
//                           printf("!");
                                                   CheckNIP = FALSE;
                                                   break;
                                                   }
                                           }
                                   }


                           // Szukamy...

                           for(i = 0; ; i++)
                                 {
                                 DoMethod( LV_Kon_Klienci, _MUIM_List_GetEntry, i, &node );

                                 if( !node )
                                   break;

                                 if( node->kl_klient.Separator )
                                   continue;

                                 if( CheckNIP )
                                   {
                                   if( strncmp( node->kl_klient.NIP, Str2NIP( str ), strlen(Str2NIP( str )) ) == 0 )
                                         {
                                         set( LV_Kon_Klienci, _MUIA_List_Active, i );
                                         break;
                                         }
                                   }
                                 else
                                   {
                                   if(StrnCmp( MyLocale, node->kl_klient.Nazwa1, str, -1, SC_COLLATE2) >= 0 )
                                         {
                                         set( LV_Kon_Klienci, _MUIA_List_Active, i );
                                         break;
                                         }
                                   }
                                 }
                           }
                           break;


                        case ID_CUST_SEARCH_ACK:
                                 {
                                 struct KlientList *klient = NULL;

                                 DoMethod(LV_Kon_Klienci, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &klient);

                                 if(klient)
                                   {
                                   if( !(klient->kl_klient.Separator) )
                                         {
                                         setstring(ST_Kon_Search, klient->kl_klient.Nazwa1);
                                         set(KontrahentSelectorWindow, MUIA_Window_ActiveObject, BT_Kon_Ok);
                                         }
                                   else
                                         {
                                         DisplayBeep(0);
                                         set(KontrahentSelectorWindow, MUIA_Window_ActiveObject, ST_Kon_Search);
                                         }
                                   }
                                 }
                           break;


                        case ID_OK:
                                 {
                                 struct KlientList *klient = NULL;

                                 DoMethod(LV_Kon_Klienci, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &klient);

                                 if(klient)
                                   {
                                   if( !(klient->kl_klient.Separator) )
                                         {
                                         result = klient;
                                         running = FALSE;
                                         }
                                   else
                                         {
                                         DisplayBeep(0);
                                         set(KontrahentSelectorWindow, MUIA_Window_ActiveObject, ST_Kon_Search);
                                         }
                                   }
                                 }
                           break;


                        case ID_CANCEL:
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }
                }
         else
                {
                DisplayBeep(0);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


        _detachwin( KontrahentSelectorWindow );
        set(app, MUIA_Application_Sleep, FALSE);

        return(result);
}

//|

/// P�ATNO�� SELECTOR STRINGS

#define MSG_PAY_TITLE    "Rodzaj p�atno�ci"

#define MSG_PAY_OK       "F10 - _Ok"
#define MSG_PAY_ADD      "F1 - Dod_aj"
#define MSG_PAY_CANCEL   "ESC - Ponie_chaj"

#define MSG_PAY_REQ_MAX      "\033cMo�esz zadeklarowa� maksymalnie\n%ld rodzaj�w p�atno�ci."

//|
/// P�atno��SelectorSetup

static Object *PaySelectorWindow,        /* P�ATNO�� SELECTOR */
                          *LV_PaySel_Unit,
                          *BT_PaySel_Ok,
                          *BT_PaySel_Add,
                          *BT_PaySel_Cancel;



char P�atno��SelectorSetup(struct P�atno�� *def_jedn)
{

///   Create P�atno��RequesterWindow

          PaySelectorWindow = WindowObject,
                                                MUIA_Window_ID         , ID_WIN_PAYSELECTOR,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                MUIA_Window_Title, MSG_PAY_TITLE,
                                                WindowContents,
                                                   VGroup,

                                                   Child, VGroup,
                                                                  GroupFrameT(MSG_PAY_TITLE),

                                                                  Child, LV_PaySel_Unit = _ListviewObject,
                                                                                                _MUIA_List_AutoVisible, TRUE,
                                                                                                _MUIA_Listview_List, Golem_PayListObject,
                                                                                                          _MUIA_List_Format, "MIW=1 MAW=98 COL=1 BAR,"
                                                                                                                                                 "MIW=1 MAW=-1 COL=2 P=\033c",
                                                                                                 End,
                                                                                 End,
                                                                  End,


                                                   Child, HGroup,
                                                                  MUIA_Group_SameSize, TRUE,
                                                                  Child, BT_PaySel_Ok     = TextButton(MSG_PAY_OK),
                                                                  Child, BT_PaySel_Add    = TextButton(MSG_PAY_ADD),
                                                                  Child, BT_PaySel_Cancel = TextButton(MSG_PAY_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|

        if( !PaySelectorWindow )
           return(FALSE);

        DoMethod(PaySelectorWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(LV_PaySel_Unit   , MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_PaySel_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_PaySel_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(BT_PaySel_Add    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);

        DoMethod(PaySelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ADD);
        DoMethod(PaySelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);


        _attachwin(PaySelectorWindow);

        return(TRUE);

}

//|
/// P�atno��Selector

struct P�atno��List *P�atno��Selector(struct P�atno�� *def_jedn)
{
/*
** Otwiera okno wyboru rodzaju p�atno�ci. Je�li def_jedn != NULL
** wybiera podan� p�atno�� na li�cie jako domy�ln�.
** Zwraca Struct P�atno��List * lub NULL
*/

char  running = TRUE;
ULONG signal  = 0;
struct P�atno��List *result = NULL;
struct P�atno��List *p�atno��;
int    count = 0;

        _sleep(TRUE);

        if(!P�atno��SelectorSetup(def_jedn))
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }


//    DoMethod(LV_PaySel_Unit, _MUIM_List_Clear);

        if( !IsListEmpty( &p�atno�ci ) )
           {
           for(p�atno�� = (struct P�atno��List *)p�atno�ci.lh_Head; p�atno��->pl_node.mln_Succ; p�atno�� = (struct P�atno��List *)p�atno��->pl_node.mln_Succ)
                 {
                 DoMethod(LV_PaySel_Unit, _MUIM_List_InsertSingle, p�atno��, _MUIV_List_Insert_Sorted);
                 count++;
                 }
           }


        set(BT_PaySel_Ok, MUIA_Disabled, (count == 0));

        // poprawi�!
        set(LV_PaySel_Unit, _MUIA_List_Active, 0);

        set(PaySelectorWindow, MUIA_Window_ActiveObject, LV_PaySel_Unit);

        if(WinOpen(PaySelectorWindow))
                {
                while(running)
                  {
                  switch (DoMethod(app, MUIM_Application_Input, &signal))
                        {


                        case ID_ADD:
                           {
                           struct P�atno��List *p�atno��;

                           if(xget(LV_PaySel_Unit, _MUIA_List_Entries) >= MAX_PAYS)
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, PaySelectorWindow, 0, TITLE, MSG_OK, MSG_PAY_REQ_MAX, MAX_PAYS);
                                  break;
                                  }

                           if((p�atno�� = calloc(1, sizeof(struct P�atno��List))) != NULL)
                                  {
                                  if
                                  (EdycjaP�atno�ci(p�atno��, FALSE))
                                        {
                                        long pos = DodajP�atno��(p�atno��);

                                        DoMethod(LV_PaySel_Unit, _MUIM_List_InsertSingle, p�atno��, pos);
                                        set(LV_PaySel_Unit, _MUIA_List_Active, pos);

                                        ZapiszP�atno�ci();

                                        _enable( BT_PaySel_Ok );
                                        }
                                  }
                           else
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, PaySelectorWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                                  }
                           }


                        case ID_OK:
                                 DoMethod(LV_PaySel_Unit, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &result);
                                 running = FALSE;
                           break;


                        case ID_CANCEL:
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }
                }
         else
                {
                DisplayBeep(0);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


//    set(PaySelectorWindow, MUIA_Window_Open, FALSE);
        _detachwin(PaySelectorWindow);
        _sleep(FALSE);

        return(result);
}

//|

/// JEDNOSTKA SELECTOR STRINGS

#define MSG_JEDN_TITLE    "Jednostki miary"

#define MSG_JEDN_OK       "F10 - _Ok"
#define MSG_JEDN_ADD      "F1 - Dod_aj"
#define MSG_JEDN_CANCEL   "ESC - Ponie_chaj"

#define MSG_UNIT_REQ_MAX     "\033cMo�esz zadeklarowa� maksymalnie\n%ld rodzaj�w jednostek miar."

//|
/// JednostkaSelectorSetup

static Object *JednSelectorWindow,        /* JEDNOSTKA SELECTOR */
                          *LV_JednSel_Unit,
                          *BT_JednSel_Ok,
                          *BT_JednSel_Add,
                          *BT_JednSel_Cancel;



char JednostkaSelectorSetup(struct Jednostka *def_jedn)
{

///   Create DocRequesterWindow

          JednSelectorWindow = WindowObject,
                                                MUIA_Window_ID         , ID_WIN_JEDNSELECTOR,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                MUIA_Window_Title, MSG_JEDN_TITLE,
                                                WindowContents,
                                                   VGroup,

                                                   Child, VGroup,
                                                                  GroupFrameT(MSG_JEDN_TITLE),

                                                                  Child, LV_JednSel_Unit = _ListviewObject,
//                                         MUIA_CycleChain, TRUE,
                                                                                 _MUIA_List_AutoVisible, TRUE,
                                                                                 _MUIA_Listview_List, Golem_UnitListObject,
                                                                                           _MUIA_List_Format, "MIW=1 MAW=98 COL=1 BAR,"
                                                                                                                                  "MIW=1 MAW=-1 COL=2 P=\033c",
                                                                                           End,
                                                                                 End,
                                                                  End,


                                                   Child, HGroup,
                                                                  MUIA_Group_SameSize, TRUE,
                                                                  Child, BT_JednSel_Ok     = TextButton(MSG_JEDN_OK),
                                                                  Child, BT_JednSel_Add    = TextButton(MSG_JEDN_ADD),
                                                                  Child, BT_JednSel_Cancel = TextButton(MSG_JEDN_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|

        if(!JednSelectorWindow)
           return(FALSE);

        DoMethod(JednSelectorWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(LV_JednSel_Unit   , MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_JednSel_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_JednSel_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(BT_JednSel_Add    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);

        DoMethod(JednSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ADD);
        DoMethod(JednSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);


        _attachwin(JednSelectorWindow);

        return(TRUE);

}

//|
/// JednostkaSelector

struct JednostkaList *JednostkaSelector(struct Jednostka *def_jedn)
{
/*
** Otwiera okno wyboru jednostki miary. Je�li def_jedn != NULL
** wybiera podan� jednostk� na li�cie jako domy�ln�.
** Zwraca Struct JednostkaList * lub NULL
*/

char  running = TRUE;
ULONG signal  = 0;
struct JednostkaList *result = NULL;
struct JednostkaList *jedn;
int    count = 0;

        _sleep(TRUE);

        if( !JednostkaSelectorSetup(def_jedn) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }


//    DoMethod(LV_JednSel_Unit, _MUIM_List_Clear);

        if(!IsListEmpty(&jedn_miary))
           {
           for(jedn = (struct JednostkaList *)jedn_miary.lh_Head; jedn->ul_node.mln_Succ; jedn = (struct JednostkaList *)jedn->ul_node.mln_Succ)
                 {
                 DoMethod(LV_JednSel_Unit, _MUIM_List_InsertSingle, jedn, _MUIV_List_Insert_Sorted);
                 count++;
                 }
           }


        set(BT_JednSel_Ok, MUIA_Disabled, (count == 0));

        // poprawi�!
        set(LV_JednSel_Unit, _MUIA_List_Active, 0);

        set(JednSelectorWindow, MUIA_Window_ActiveObject, LV_JednSel_Unit);

        if(WinOpen(JednSelectorWindow))
                {
                while(running)
                  {
                  switch (DoMethod(app, MUIM_Application_Input, &signal))
                        {
                        case ID_ADD:
                           {
                           struct JednostkaList *jedn;

                           if(xget(LV_JednSel_Unit, _MUIA_List_Entries) >= MAX_UNITS)
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, JednSelectorWindow, 0, TITLE, MSG_OK, MSG_UNIT_REQ_MAX, MAX_UNITS);
                                  break;
                                  }

                           if((jedn = calloc(1, sizeof(struct JednostkaList))) != NULL)
                                  {
                                  if
                                  (EdycjaJednostki(jedn, FALSE))
                                        {
                                        long pos = DodajJednostk�(jedn);

                                        DoMethod(LV_JednSel_Unit, _MUIM_List_InsertSingle, jedn, pos);
                                        set(LV_JednSel_Unit, _MUIA_List_Active, pos);

                                        ZapiszJednostki();

                                        _enable( BT_JednSel_Ok );
                                        }
                                  }
                           else
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, JednSelectorWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                                  }
                           }
                           break;

                        case ID_OK:
                                 DoMethod(LV_JednSel_Unit, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &result);
                                 running = FALSE;
                           break;


                        case ID_CANCEL:
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }
                }
         else
                {
                DisplayBeep(0);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


//    set(JednSelectorWindow, MUIA_Window_Open, FALSE);
        _detachwin(JednSelectorWindow);
        _sleep(FALSE);

        return(result);
}

//|

/// DRUKARKA SELECTOR STRINGS

#define MSG_PRINT_TITLE    "Drukarki"

#define MSG_PRINT_OK       "F10 - _Ok"
#define MSG_PRINT_ADD      "F1 - Dod_aj"
#define MSG_PRINT_CANCEL   "ESC - Ponie_chaj"

//|
/// DrukarkaSelectorSetup

static Object *DrukarkaSelectorWindow,        /* JEDNOSTKA SELECTOR */
                          *LV_PrtSel_Prt,
                          *BT_PrtSel_Ok,
                          *BT_PrtSel_Add,
                          *BT_PrtSel_Cancel;



char DrukarkaSelectorSetup(struct PrinterList *printer)
{
struct PrinterList *PrtL;
long   count = 0;

///   Create DocRequesterWindow

          DrukarkaSelectorWindow = WindowObject,
                                                MUIA_Window_ID         , ID_WIN_DRUKARKASELECTOR,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                MUIA_Window_Title      , MSG_PRINT_TITLE,
                                                WindowContents,
                                                   VGroup,

                                                   Child, VGroup,
                                                                  GroupFrameT( MSG_PRINT_TITLE ),

                                                                  Child, LV_PrtSel_Prt = _ListviewObject,
//                                         MUIA_CycleChain, TRUE,
                                                                                 _MUIA_List_AutoVisible, TRUE,
                                                                                 _MUIA_Listview_List, Golem_PrtListObject,
                                                                                           _MUIA_List_Format, "MIW=1 MAW=98 COL=1 BAR,"
                                                                                                                                  "MIW=1 MAW=-1 COL=2 P=\033c",
                                                                                           End,
                                                                                 End,
                                                                  End,


                                                   Child, HGroup,
                                                                  MUIA_Group_SameSize, TRUE,
                                                                  Child, BT_PrtSel_Ok     = TextButton(MSG_PRINT_OK),
                                                                  Child, BT_PrtSel_Add    = TextButton(MSG_PRINT_ADD),
                                                                  Child, BT_PrtSel_Cancel = TextButton(MSG_PRINT_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|

        if( !DrukarkaSelectorWindow )
           return( FALSE );


        if( !IsListEmpty( &drukarki ) )
           {
           for(PrtL = (struct PrinterList *)drukarki.lh_Head; PrtL->pl_node.mln_Succ; PrtL = (struct PrinterList *)PrtL->pl_node.mln_Succ)
                 {
                 DoMethod( LV_PrtSel_Prt, _MUIM_List_InsertSingle, PrtL, _MUIV_List_Insert_Sorted);
                 count++;
                 }
           }

        if( count == 0)
           _disable( BT_PrtSel_Ok );

        DoMethod(DrukarkaSelectorWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(LV_PrtSel_Prt         , MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_PrtSel_Ok          , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_PrtSel_Cancel      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(BT_PrtSel_Add         , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);

        DoMethod(DrukarkaSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ADD);
        DoMethod(DrukarkaSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

        _attachwin( DrukarkaSelectorWindow );

        return(TRUE);

}

//|
/// DrukarkaSelector


struct PrinterList *DrukarkaSelector(struct PrinterList *printer)
{
/*
** Otwiera okno wyboru prtostki miary. Je�li def_prt != NULL
** wybiera podan� prtostk� na li�cie jako domy�ln�.
** Zwraca Struct PrinterList * lub NULL
*/

char  running = TRUE;
ULONG signal  = 0;
struct PrinterList *result = NULL;
struct PrinterList *prt;
int    count = 0;


        _sleep(TRUE);

        if( !DrukarkaSelectorSetup( printer ) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }



        // poprawi�!
        set(LV_PrtSel_Prt, _MUIA_List_Active, 0);

        set(DrukarkaSelectorWindow, MUIA_Window_ActiveObject, LV_PrtSel_Prt);

        if(WinOpen(DrukarkaSelectorWindow))
                {
                while(running)
                  {
                  switch (DoMethod(app, MUIM_Application_Input, &signal))
                        {
                        case ID_ADD:
                           {
                           struct PrinterList *prt;
                           short  ID = WybierzIDDrukarki();

                           if( ID != -1 )
                                   {
                                   if((prt = calloc(1, sizeof(struct PrinterList))) != NULL)
                                          {
                                          SetDefPrtSettings( prt );
                                          prt->pl_printer.ID = ID;

                                          if( EdycjaDrukarki(prt, FALSE) )
                                                {
                                                long pos = DodajDrukark�(prt);

                                                DoMethod(LV_PrtSel_Prt, _MUIM_List_InsertSingle, prt, pos);
                                                set(LV_PrtSel_Prt, _MUIA_List_Active, pos);

                                                ZapiszDrukarki();

                                                IDPrinter[ ID ] = TRUE;

                                                _enable( BT_PrtSel_Ok );
                                                }
                                          }
                                   else
                                          {
                                          DisplayBeep(0);
                                          MUI_Request(app, DrukarkaSelectorWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                                          }
                                  }
                           else
                                  {
                                  DisplayBeep(0);
                                  // 256 drukarek to max!
                                  }
                           }
                           break;

                        case ID_OK:
                                 DoMethod(LV_PrtSel_Prt, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &result);
                                 running = FALSE;
                           break;


                        case ID_CANCEL:
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }
                }
         else
                {
                DisplayBeep(0);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


//    set(DrukarkaSelectorWindow, MUIA_Window_Open, FALSE);
        _detachwin(DrukarkaSelectorWindow);
        _sleep(FALSE);

        return(result);
}

//|


/// REDAGOWANIE WINDOW STRINGS

#define MSG_RED_WIN_TITLE_1 "Wyszukiwanie dokument�w wystawionych"
#define MSG_RED_WIN_TITLE_2 "Wyszukiwanie dokument�w od�o�onych"

#define MSG_RED_TYPE      "_Typ dokumentu"
#define MSG_RED_FIND_BY   "_Szukaj wg."
#define MSG_RED_NUMER     "_Numer"
#define MSG_RED_NAZWA     "_Nazwa"
#define MSG_RED_NIP       "_NIP"
#define MSG_RED_REGON     "Rego_n"
#define MSG_RED_DATA      "_Data"
#define MSG_RED_MIESIAC   "_M-c"
#define MSG_RED_DATA_TRYB "Try_b"

#define MSG_RED_DS_NAZWA  "Nazwa odbiorcy"
#define MSG_RED_DS_NIP    "NIP"
#define MSG_RED_DS_REGON  "Regon"
#define MSG_RED_DS_NUMER  "Numer dokumentu"
#define MSG_RED_DS_DATA   "Data wystawienia"

#define MSG_RED_ROK       "_Rok"

#define MSG_RED_FIND      "F7 - _Znajd�"

#define MSG_RED_GR_TITLE2 "R��ne"
#define MSG_RED_SORT       "_Posortuj"

#define MSG_RED_OK        "F10 - _Ok"
#define MSG_RED_CANCEL    "ESC - Ponie_chaj"


#define MSG_RED_ERR_PATT  "\033cB��d podczas przetwarzania wzorca!\nSprawd� poprawno�� zapisu!"
#define MSG_RED_ERR_DATE  "\033cB��d podczas przetwarzania daty!\nSprawd� poprawno�� zapisu!"


//|
/// SCAN STOP CHUNKS
#define SCAN_NUM_STOPS (sizeof(Scan_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Scan_Stops[] =
{
                ID_DOC, ID_CAT,
                ID_DOC, ID_VERS,

                ID_DOC, ID_DOCT,
                ID_DOC, ID_DOCN,
                ID_DOC, ID_CUST,
                ID_DOC, ID_CREA,
                ID_DOC, ID_SALE,
                ID_DOC, ID_PAYD,
                ID_DOC, ID_PAY,
//        ID_DOC, ID_PROD,
                ID_DOC, ID_RECE,
                ID_DOC, ID_WYST,
                
                ID_DOC, ID_TNET,
                ID_DOC, ID_TVAT,

                NULL, NULL,
};
//|
/// DokumentSelectorSetup

char *RodzajeDokument�wTable[] = {"Faktura VAT",
                                  "Rachunek uproszczony",
                                  "Faktura koryguj�ca VAT",
                                  "Rachunek koryguj�cy",
                                  "Faktura VAT do paragonu",
                                  "Rachunek upr. do paragonu",
                                  "Paragon",
                                  "Zam�wienie",
                                  "Faktura VAT - PRO FORMA",
                                  "Sprzeda� odr�czna",
                                  NULL};

char *RodzajeDokument�wTable_FVATLimit[] = {"Faktura VAT",
                                            NULL};

char *RodzajeDokument�wOd�o�oneTable[] = {"Faktura VAT",
                                          "Rachunek uproszczony",
                                          "Faktura koryguj�ca VAT",
                                          "Rachunek koryguj�cy",
                                          "Faktura VAT do paragonu",
                                          "Rachunek upr. do paragonu",
                                          "Faktura VAT - PRO FORMA",
                                          "Sprzeda� odr�czna",
                                          NULL};

char *RodzajeDokument�wLVTable[] = {"FV",
                                    "RU",
                                    "FVK",
                                    "RUK",
                                    "FVP",
                                    "RUP",
                                    "PAR",
                                    "ZAM",
                                    "FVPR",
                                    "ODRE",
                                    NULL};



char *RodzajePrzeszukiwaniaTable[] = {MSG_RED_DS_NUMER,
                                      MSG_RED_DS_NAZWA,
                                      MSG_RED_DS_NIP,
                                      MSG_RED_DS_REGON,
                                      MSG_RED_DS_DATA,
                                      NULL};


char *Miesi�ceTable[] = {"\033bAktualny",
                         "Stycze�",
                         "Luty",
                         "Marzec",
                         "Kwiecie�",
                         "Maj",
                         "Czerwiec",
                         "Lipiec",
                         "Sierpie�",
                         "Wrzesie�",
                         "Pa�dziernik",
                         "Listopad",
                         "Grudzie�",
                         "\0338Ca�y rok",
                         NULL};


char *WyszukiwanieWgDatyTable[] = {"dok�adnie",
                                   "wcze�niej",
                                   "p��niej",
                                   NULL};



static Object
           *FakturaSelectorWindow,                 /* REDAGOWANIE WINDOW */

           *LV_FSel_Dokumenty,
           *CY_FSel_Typ,

           *CY_FSel_Szukaj,
           *GR_FSel_Szukaj1,
           *GR_FSel_Szukaj2,
           *ST_FSel_Numer,
           *CY_FSel_Numer_Miesiace,
           *ST_FSel_Numer_Rok,
           *ST_FSel_Nazwa,
           *BT_FSel_NazwaWybierz,
           *CY_FSel_Nazwa_Miesiace,
           *ST_FSel_Nazwa_Rok,
           *ST_FSel_NIP,
           *BT_FSel_NIPWybierz,
           *CY_FSel_NIP_Miesiace,
           *ST_FSel_NIP_Rok,
           *ST_FSel_Regon,
           *BT_FSel_RegonWybierz,
           *CY_FSel_Regon_Miesiace,
           *ST_FSel_Regon_Rok,
           *ST_FSel_Data,
           *CY_FSel_DataAccuracy,

           *BT_FSel_Find,
           *BT_FSel_Sort,

           *BT_FSel_Ok,
           *BT_FSel_Cancel;



// TypDokumentow: biezace czy odlozone
char DokumentSelectorSetup( char TypDokument�w, char FVAT_Limit )
{
APTR table;
char *win_title;
//char *gr_title;
char *lv_format;
char lv_order;

char **RodzajeDokument�wTable_;


        if( FVAT_Limit )
           RodzajeDokument�wTable_ = RodzajeDokument�wTable_FVATLimit;
        else
           RodzajeDokument�wTable_ = RodzajeDokument�wTable;


        switch( TypDokument�w)
           {
           case DOKUMENTY_BIE��CE:
                   table     = RodzajeDokument�wTable_;
                   win_title = MSG_RED_WIN_TITLE_1;
//           gr_title  = MSG_RED_GR_TITLE_1;

                   lv_format = "MIW=1 MAW=-1 COL=0 BAR,"   /* Typ */
                               "MIW=1 MAW=40 COL=1 BAR,"   /* Odbiorca */
                               "MIW=1 MAW=-1 COL=2 BAR,"   /* Nip */
                               "MIW=1 MAW=-1 COL=3 BAR,"   /* Numer */
                               "MIW=1 MAW=-1 COL=4 BAR,"   /* data */
                               "MIW=1 MAW=-1 COL=5     ";  /* total */
                   lv_order  = 4;

                   break;


           case DOKUMENTY_OD�O�ONE:
                   table = RodzajeDokument�wOd�o�oneTable;
                   win_title = MSG_RED_WIN_TITLE_2;
//           gr_title  = MSG_RED_GR_TITLE_2;

                   lv_format = "MIW=1 MAW=-1 COL=0 BAR,"   /* Typ */
                               "MIW=1 MAW=40 COL=1 BAR,"   /* Odbiorca */
                               "MIW=1 MAW=-1 COL=2 BAR,"   /* Nip */
                               "MIW=1 MAW=-1 COL=3 BAR,"   /* Regon */
                               "MIW=1 MAW=-1 COL=4 BAR,"   /* data */
                               "MIW=1 MAW=-1 COL=5     ";  /* total */
                   lv_order  = 3;

                   break;
           }


///   FakturaSelectorWindow

          FakturaSelectorWindow = WindowObject,
                                                MUIA_Window_Title      , win_title,
                                                MUIA_Window_ID         , ID_WIN_RED,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                  VGroup,

///                                Strona #1

                                                   Child, VGroup,

                                                                 Child, HGroup,
///                                  Parametry wyszukiwania
                                                                         Child, ColGroup(2),
//                                         GroupFrameT(gr_title),
//                                         GroupFrame,

                                                                                 Child, MakeLabel2(MSG_RED_TYPE),
                                                                                 Child, CY_FSel_Typ = MakeCycle(table, MSG_RED_TYPE),

                                                                                 Child, MakeLabel2(MSG_RED_FIND_BY),
                                                                                 Child, CY_FSel_Szukaj = MakeCycle(RodzajePrzeszukiwaniaTable, MSG_RED_FIND_BY),

                                                                                 Child, GR_FSel_Szukaj1 = HGroup,
                                                                                                MUIA_Group_PageMode, TRUE,

                                                                                                Child, MakeLabel2(MSG_RED_NUMER),
                                                                                                Child, MakeLabel2(MSG_RED_NAZWA),
                                                                                                Child, MakeLabel2(MSG_RED_NIP),
                                                                                                Child, MakeLabel2(MSG_RED_REGON),
                                                                                                Child, MakeLabel2(MSG_RED_DATA),
                                                                                                End,

                                                                                 Child, GR_FSel_Szukaj2 = HGroup,
                                                                                                MUIA_Group_PageMode, TRUE,

//                                   Numer dokumentu
                                                                                                Child, HGroup,
                                                                                                           Child, ST_FSel_Numer = MakeStringAccept(40 * 2, MSG_RED_NUMER, "0123456789/[]#?*-~()|"),

                                                                                                           Child, HGroup,
                                                                                                                          MUIA_Weight, 1,
                                                                                                                          Child, MakeLabel2(MSG_RED_MIESIAC),
                                                                                                                          Child, CY_FSel_Numer_Miesiace = MakeCycle(Miesi�ceTable, MSG_RED_MIESIAC),
                                                                                                                          End,
                                                                                                           Child, HGroup,
                                                                                                                          Child, MakeLabel2(MSG_RED_ROK),
                                                                                                                          Child, ST_FSel_Numer_Rok = MakeStringAccept(4 + 1, MSG_RED_ROK, "0123456789"),
                                                                                                                          End,
                                                                                                           End,

//                                   Nazwa odbiorcy
                                                                                                Child, HGroup,
                                                                                                           Child, HGroup,
                                                                                                                          MUIA_Group_Spacing, 1,
                                                                                                                          Child, ST_FSel_Nazwa = MakeString(CUST_NAME_LEN * 3, MSG_RED_NAZWA),
                                                                                                                          Child, BT_FSel_NazwaWybierz = PopButton2(MUII_PopUp, NULL),
                                                                                                                          End,
                                                                                                           Child, HGroup,
                                                                                                                          MUIA_Weight, 1,
                                                                                                                          Child, MakeLabel2(MSG_RED_MIESIAC),
                                                                                                                          Child, CY_FSel_Nazwa_Miesiace = MakeCycle(Miesi�ceTable, MSG_RED_MIESIAC),
                                                                                                                          End,
                                                                                                           Child, HGroup,
                                                                                                                          Child, MakeLabel2(MSG_RED_ROK),
                                                                                                                          Child, ST_FSel_Nazwa_Rok = MakeStringAccept(4 + 1, MSG_RED_ROK, "0123456789"),
                                                                                                                          End,
                                                                                                           End,

//                                   NIP odbiorcy
                                                                                                Child, HGroup,
                                                                                                           Child, HGroup,
                                                                                                                          MUIA_Group_Spacing, 1,

                                                                                                                          Child, ST_FSel_NIP = MakeStringAccept(CUST_NIP_LEN * 3, MSG_RED_NIP, "0123456789-[]#?*-~()|"),
                                                                                                                          Child, BT_FSel_NIPWybierz = PopButton2(MUII_PopUp, NULL),
                                                                                                                          End,
                                                                                                           Child, HGroup,
                                                                                                                          MUIA_Weight, 1,
                                                                                                                          Child, MakeLabel2(MSG_RED_MIESIAC),
                                                                                                                          Child, CY_FSel_NIP_Miesiace = MakeCycle(Miesi�ceTable, MSG_RED_MIESIAC),
                                                                                                                          End,
                                                                                                           Child, HGroup,
                                                                                                                          MUIA_Weight, 100,
                                                                                                                          Child, MakeLabel2(MSG_RED_ROK),
                                                                                                                          Child, ST_FSel_NIP_Rok = MakeStringAccept(4 + 1, MSG_RED_ROK, "0123456789"),
                                                                                                                          End,
                                                                                                           End,

//                                   Regon odbiorcy
                                                                                                Child, HGroup,
                                                                                                           Child, HGroup,
                                                                                                                          MUIA_Group_Spacing, 1,
                                                                                                                          Child, ST_FSel_Regon = MakeStringAccept(CUST_REGON_LEN * 3 + 3, MSG_RED_REGON, "0123456789-[]#?*-~()|"),
                                                                                                                          Child, BT_FSel_RegonWybierz = PopButton2(MUII_PopUp, NULL),
                                                                                                                          End,
                                                                                                           Child, HGroup,
                                                                                                                          MUIA_Weight, 1,
                                                                                                                          Child, MakeLabel2(MSG_RED_MIESIAC),
                                                                                                                          Child, CY_FSel_Regon_Miesiace = MakeCycle(Miesi�ceTable, MSG_RED_MIESIAC),
                                                                                                                          End,
                                                                                                           Child, HGroup,
                                                                                                                          MUIA_Weight, 100,
                                                                                                                          Child, MakeLabel2(MSG_RED_ROK),
                                                                                                                          Child, ST_FSel_Regon_Rok = MakeStringAccept(4 + 1, MSG_RED_ROK, "0123456789"),
                                                                                                                          End,

                                                                                                           End,

//                                   Data wystawienia
                                                                                                Child, HGroup,
                                                                                                           Child, ST_FSel_Data = MakeString(DATE_LEN, MSG_RED_DATA),
                                                                                                           Child, HGroup,
                                                                                                                          MUIA_Weight, 25,
                                                                                                                          Child, MakeLabel2(MSG_RED_DATA_TRYB),
                                                                                                                          Child, CY_FSel_DataAccuracy = MakeCycle(WyszukiwanieWgDatyTable, MSG_RED_DATA_TRYB),
                                                                                                                          End,

                                                                                                           End,
                                                                                                End,

                                                                                 Child, EmptyLabel(),
                                                                                 Child, BT_FSel_Find = TextButtonWeight(MSG_RED_FIND, 1),

                                                                                 End,
//|
///                                  Operacje dodatkowe

                                                                  Child, VGroup,
                                                                                 GroupFrameT(MSG_RED_GR_TITLE2),
                                                                                 GroupBack,
                                                                                 MUIA_Weight, 1,

                                                                                 Child, BT_FSel_Sort = TextButtonWeight(MSG_RED_SORT, 1),

                                                                                 Child, HVSpace,
                                                                                 End,

//|
                                                                 End,


//                                  Child, VGroup,
//                                         GroupFrame,

                                                                                 Child, LV_FSel_Dokumenty = ListviewObject,
                                                                                                MUIA_CycleChain, TRUE,
                                                                                                _MUIA_Listview_List, NewObject( CL_DocumentList->mcc_Class, NULL,
                                                                                                          _MUIA_List_Format, lv_format,
                                                                                                           MUIA_DocumentList_Order, lv_order,
                                                                                                          TAG_DONE),
                                                                                                End,


                                                                  Child, HGroup,
                                                                                 Child, BT_FSel_Ok     = TextButton(MSG_RED_OK),
                                                                                 Child, BT_FSel_Cancel = TextButton(MSG_RED_CANCEL),
                                                                                 End,

                                                                  End,


//|

                                                  End,

                                                End;

//|


        if( ! FakturaSelectorWindow )
           return( FALSE );



        // settings,
           {
           struct DateStamp datestamp;
           char Rok[ 4 + 1 ];

           DateStamp( &datestamp );
           wfmh_Date2StrFmtBuffer( Rok, "%Y", &datestamp);

           setstring( ST_FSel_Numer_Rok, Rok );
           setstring( ST_FSel_Nazwa_Rok, Rok );
           setstring( ST_FSel_NIP_Rok, Rok );
           setstring( ST_FSel_Regon_Rok, Rok );
           }

        set(FakturaSelectorWindow, MUIA_Window_ActiveObject, CY_FSel_Typ);


        //notifications

        /* REDAGOWANIE WINDOW */

        DoMethod(FakturaSelectorWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(FakturaSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f7", MUIV_Notify_Application , 2, MUIM_Application_ReturnID, ID_FIND);
        DoMethod(FakturaSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_DELETE);
//        DoMethod(FakturaSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "esc", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(FakturaSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);


        DoMethod(CY_FSel_Szukaj, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, GR_FSel_Szukaj1, 3, MUIM_Set, MUIA_Group_ActivePage, MUIV_TriggerValue);
        DoMethod(CY_FSel_Szukaj, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, GR_FSel_Szukaj2, 3, MUIM_Set, MUIA_Group_ActivePage, MUIV_TriggerValue);
//       DoMethod(CY_FSel_Szukaj, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, MUIV_Notify_Application, 3, MUIM_Application_ReturnID, ID_RED_DOC_TYPE);
        DoMethod(LV_FSel_Dokumenty, MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);


        DoMethod(BT_FSel_NazwaWybierz, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_RED_NAZWAWYBIERZ);
        DoMethod(BT_FSel_NIPWybierz  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_RED_NIPWYBIERZ);
        DoMethod(BT_FSel_RegonWybierz, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_RED_REGONWYBIERZ);

        DoMethod(BT_FSel_Find      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FIND);
        DoMethod(BT_FSel_Sort      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SORT);
        DoMethod(BT_FSel_Ok        , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_FSel_Cancel    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);



        _attachwin( FakturaSelectorWindow );

        return( TRUE );

}
//|
/// DocumentSelectorFinish
char DocumentSelectorFinish( struct DocumentScan *ds )
{
struct DocumentScan *ds_tmp;

        if(xget(LV_FSel_Dokumenty, _MUIA_List_Active) == _MUIV_List_Active_Off)
           {
           set( FakturaSelectorWindow, MUIA_Window_ActiveObject, LV_FSel_Dokumenty );
           return(FALSE);
           }

        DoMethod( LV_FSel_Dokumenty, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &ds_tmp );
        if( ds_tmp )
           memcpy( ds, ds_tmp, sizeof(struct DocumentScan) );

        return(TRUE);

}
//|
/// _ScanDocHeader

char _ScanDocData( struct DocumentScan *ds, char *nazwa )
{
/*
** Wczytuje informacje o dokumencie (zamiast grzebac w filekomentach
*/

struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
long   Errors = 0;
char   ValidFile = FALSE;
long   Error_Skipped_Products = 0;
struct ProductList prod = {0};
double warto�� = 0;

char    TotalFound = 0;         // czy znalezione zostaly chunki ID_TVAT i ID_TNET. Jesli totalfound !=2 to ds->TotalFound = FALSE

struct Klient odbiorca = {0};



         set(app, MUIA_Application_Sleep, TRUE);

         if(iff = AllocIFF())
           {
           if(iff->iff_Stream = Open(nazwa, MODE_OLDFILE))
                   {
                   InitIFFasDOS(iff);

                   StopChunks(iff, Scan_Stops, SCAN_NUM_STOPS);

                   if(!OpenIFF(iff, IFFF_READ))
                           {

                           while(TRUE)
                                  {
                                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                                         break;

                                  if(cn = CurrentChunk(iff))
                                         {
                                         LONG ID = cn->cn_ID;

                                         if(!ValidFile)
                                                {
                                                if((ID == ID_CAT) && (cn->cn_Type == ID_DOC))
                                                   {
                                                   ValidFile = TRUE;
                                                   continue;
                                                   }

                                                break;
                                                }

///                    ID_VERS
                                         if(ID == ID_VERS)
                                                {
                                                struct BaseVersion version;

                                                if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                                                   {

                                                   }
                                                else
                                                   {
                                                   _RC = IoErr();
                                                   break;
                                                   }

                                                continue;
                                                }
//|

///                    ID_TNET
                                         if(ID == ID_TNET)
                                                {
                                                if(ReadChunkBytes(iff, &ds->TotalNetto, cn->cn_Size) == sizeof(double) )
                                                        TotalFound++;

                                                continue;
                                                }
//|
///                    ID_TVAT
                                         if(ID == ID_TVAT)
                                                {
                                                if(ReadChunkBytes(iff, &ds->TotalVat, cn->cn_Size) == sizeof(double) )
                                                        TotalFound++;

                                                continue;
                                                }
//|

                                         _read(ID_DOCT, &ds->RodzajDokumentu)
                                         _read(ID_DOCN, ds->NumerDokumentu)

//                     _read(ID_WYST, dp->OsobaWystawiaj�ca)
//                     _read(ID_RECE, dp->OsobaOdbieraj�ca)

                                         _read_size(ID_CREA, &ds->DataWystawienia, sizeof(struct DateStamp))
//                     _read_size(ID_SALE, &dp->DataSprzeda�y  , sizeof(struct DateStamp))
//                     _read_size(ID_PAYD, &dp->DataP�atno�ci  , sizeof(struct DateStamp))

//                     _read_size(ID_PAY , dp->TypP�atno�ci, PAY_NAME_LEN)

                                         // odbiorca
                                         _read_size(ID_CUST, &odbiorca, sizeof(struct Klient))
                                         strcpy( ds->Nazwa, odbiorca.Nazwa1 );
                                         strcpy( ds->NIP, odbiorca.NIP );
                                         }
                                  }
                                
 
                           CloseIFF(iff);
                                                                                                                                
                                // czy znalezione oba totale?
                                ds->TotalFound = (TotalFound==2) ? TRUE : FALSE;
                           }

                        Close(iff->iff_Stream);
                        }

//D(bug("errors: %ld\n", Errors));

//       if(_RC == IFFERR_EOF) Error = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

           FreeIFF(iff);
           }

           set(app, MUIA_Application_Sleep, FALSE);


           return(0);
}


//|
/// _ProcessAnchor


void _ProcessAnchor(struct AnchorPath *anchor, char RodzajDokumentu)
{
/*
** Wyci�ga wszystkie potrzebne informacje z aktualnego
** anchora i dodaje do listy znalezionych dokumentow
*/

static struct DocumentScan docscan;
char   TmpDir[100];
char   tmp[10];
int    i;

        memset(&docscan, 0, sizeof(struct DocumentScan));

        D(bug("file: '%s'\n", anchor->ap_Buf ));


        if(anchor->ap_Info.fib_DirEntryType < 0)
           {
           _ScanDocData( &docscan, anchor->ap_Buf );

           strcpy(docscan.FilePath, anchor->ap_Buf);
           DoMethod(LV_FSel_Dokumenty, _MUIM_List_InsertSingle, &docscan, _MUIV_List_Insert_Sorted);
           }
}
//|
/// DokumentSelector

char DokumentSelector( struct DocumentScan *ds,
                       char TypDokument�w,
                       char FVAT_Limit,
                       ULONG (*CallbackFunction)()
                     )
{
struct DateStamp datestamp;
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;
char  Miesiac[3];
char  Rok[5];

        _sleep(TRUE);

        if( ! DokumentSelectorSetup( TypDokument�w, FVAT_Limit ) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }


        if(WinOpen(FakturaSelectorWindow))
           {
           while(running)
                  {
                  switch (DoMethod(app, MUIM_Application_Input, &signal))
                        {
                        struct Filtr sel_cust_filter =
                                   {
                                   TRUE,
                                   "#?",              // Nazwa

                                   1,                 // Sprzedawca
                                   1,                 // Nabywca
                                   1,                 // Kosztowiec
                                   0,                 // A
                                   0,                 // B
                                   0                  // C
                                   };

///           case ID_FIND:
                        case ID_FIND:
                           {
                           static char pattern[(FILTR_NAME_LEN*2)+2];
                           char   FileNamePattern[35];      // bufor na wzorzec nazwy pliku
                           char   CommentPattern[81];       // bufor na wzorzec dla komentarzy
                           char   FullPattern[256];         // finalny wzorzec nazwy pliku ze �cie�k�
                           char   Error = FALSE;
                           char   RodzajDokumentu = getcycle(CY_FSel_Typ);
                           char   DokumentDirOffset = 0;
                           char   SzukajWg = getcycle(CY_FSel_Szukaj);
                           struct DateStamp user_datestamp = {0};
                           int    i;

                           // Aktualna data

                           switch( TypDokument�w )
                                   {
                                   case DOKUMENTY_BIE��CE:
                                           DokumentDirOffset = 0;
                                           break;

                                   case DOKUMENTY_OD�O�ONE:
                                           DokumentDirOffset = OD_FAKTURA_VAT;
                                           break;
                                   }


                           DateStamp(&datestamp);

                           wfmh_Date2StrFmtBuffer( Miesiac, "%m", &datestamp);
                           wfmh_Date2StrFmtBuffer( Rok    , "%Y", &datestamp);

//               FormatDateHook.h_Data = Miesiac;
//               FormatDate(MyLocale, "%m", &datestamp, &FormatDateHook);
//               FormatDateHook.h_Data = Rok;
//               FormatDate(MyLocale, "%Y", &datestamp, &FormatDateHook);



                           // sprawdzenie string�w i ew. wstawienie '*' je�li puste

                           if(*(char *)xget(ST_FSel_Numer, MUIA_String_Contents) == '\0') setstring(ST_FSel_Numer, "*");
                           if(*(char *)xget(ST_FSel_Nazwa, MUIA_String_Contents) == '\0') setstring(ST_FSel_Nazwa, "*");
                           if(*(char *)xget(ST_FSel_NIP,   MUIA_String_Contents) == '\0') setstring(ST_FSel_NIP  , "*");
                           if(*(char *)xget(ST_FSel_Regon, MUIA_String_Contents) == '\0') setstring(ST_FSel_Regon, "*");
                           if(*(char *)xget(ST_FSel_Data,  MUIA_String_Contents) == '\0') setstring(ST_FSel_Data, wfmh_Date2Str( wfmh_GetCurrentTime() ) );


                           // przygotowanie filtr�w

                           switch(SzukajWg)
                                   {
///                  Numer dokumentu

                                   case 0:
                                           {
                                           char miesiac = (char)getcycle(CY_FSel_Numer_Miesiace);

                                           sprintf(FileNamePattern, "??????????????%s", (char *)xget(ST_FSel_Numer, MUIA_String_Contents));

                                           for(i=0; i<sizeof(FileNamePattern); i++)
                                                 {
                                                 if(FileNamePattern[i] == 0)
                                                        break;
                                                 if(FileNamePattern[i] == '/')
                                                        FileNamePattern[i] = '-';
                                                 }

                                           switch(miesiac)
                                                   {
                                                   case  0:    // aktualny miesiac
                                                          break;

                                                   case 13:    // ca�y rok
                                                          strcpy(Miesiac, "??");
                                                          break;

                                                   default: // styczen-grudzien
                                                          sprintf(Miesiac, "%02ld", miesiac);
                                                          break;
                                                   }

                                           // Je�eli ROK nic nie ma, to ustawiamy rok aktualny
                                           if(*(char *)xget(ST_FSel_Numer_Rok, MUIA_String_Contents) == '\0')
                                                 setstring(ST_FSel_Numer_Rok, Rok);
                                           else
                                                 copystr(Rok, ST_FSel_Numer_Rok);

                                           }
                                           break;
//|
///                  Nazwa odbiorcy

                                   case 1:
                                           {
                                           char miesiac = (char)getcycle(CY_FSel_Nazwa_Miesiace);

                                           switch(miesiac)
                                                   {
                                                   case  0:    // aktualny miesiac
                                                          break;

                                                   case 13:    // ca�y rok
                                                          strcpy(Miesiac, "??");
                                                          break;

                                                   default: // styczen-grudzien
                                                          sprintf(Miesiac, "%02ld", miesiac);
                                                          break;
                                                   }


                                           // Je�eli ROK nic nie ma, to ustawiamy rok aktualny
                                           if(*(char *)xget(ST_FSel_Nazwa_Rok, MUIA_String_Contents) == '\0')
                                                 setstring(ST_FSel_Nazwa_Rok, Rok);
                                           else
                                                 copystr(Rok, ST_FSel_Nazwa_Rok);


                                           // sprawdzamy wszystkie dokumenty danego typu
                                           sprintf(FileNamePattern, "#?");
                                           sprintf(CommentPattern, "?????????????%s#?", (char *)xget(ST_FSel_Nazwa, MUIA_String_Contents));
                                           }
                                           break;
//|
///                  NIP odbiorcy

                                   case 2:
                                           {
                                           char miesiac = (char)getcycle(CY_FSel_NIP_Miesiace);

                                           sprintf(FileNamePattern, "????%s#?", Str2NIP((char *)xget(ST_FSel_NIP, MUIA_String_Contents)));

                                           switch(miesiac)
                                                   {
                                                   case  0:    // aktualny miesiac
                                                          break;

                                                   case 13:    // ca�y rok
                                                          strcpy(Miesiac, "??");
                                                          break;

                                                   default: // styczen-grudzien
                                                          sprintf(Miesiac, "%02ld", miesiac);
                                                          break;
                                                   }

                                           // Je�eli ROK nic nie ma, to ustawiamy rok aktualny
                                           if(*(char *)xget(ST_FSel_NIP_Rok, MUIA_String_Contents) == '\0')
                                                 setstring(ST_FSel_NIP_Rok, Rok);
                                           else
                                                 copystr(Rok, ST_FSel_NIP_Rok);

                                           }
                                           break;
//|
///                  Regon odbiorcy

                                   case 3:
                                           {
                                           char miesiac = (char)getcycle(CY_FSel_Regon_Miesiace);

                                           switch(miesiac)
                                                   {
                                                   case  0:    // aktualny miesiac
                                                          break;

                                                   case 13:    // ca�y rok
                                                          strcpy(Miesiac, "??");
                                                          break;

                                                   default: // styczen-grudzien
                                                          sprintf(Miesiac, "%02ld", miesiac);
                                                          break;
                                                   }


                                           // Je�eli ROK nic nie ma, to ustawiamy rok aktualny
                                           if(*(char *)xget(ST_FSel_Regon_Rok, MUIA_String_Contents) == '\0')
                                                 setstring(ST_FSel_Regon_Rok, Rok);
                                           else
                                                 copystr(Rok, ST_FSel_Regon_Rok);


                                           // sprawdzamy wszystkie dokumenty danego typu
                                           sprintf(FileNamePattern, "#?");
                                           sprintf(CommentPattern, "????%s#?", (char *)xget(ST_FSel_Regon, MUIA_String_Contents));
                                           }
                                           break;
//|
///                  Data wystawienia

                                   case 4:
                                           {
                                           strcpy(Miesiac, "??");
                                           strcpy(Rok, "????");
                                           sprintf(FileNamePattern, "#?");
                                           }
                                           break;
//|
                                   }


                                // Parsujemy dodatkowe patterny je�li potrzeba...

                                switch(SzukajWg)
                                   {
//                   case 0:
//                   case 2:

                                   // szukanie w.g. nazwy -> pattern dla filecommenta
                                   case 1:
                                   case 3:
                                           {
                                           if(ParsePatternNoCase(CommentPattern, pattern, sizeof(pattern)) == -1)
                                                 {
                                                 DisplayBeep(0);
                                                 MUI_Request(app, FakturaSelectorWindow, 0, TITLE, MSG_OK, MSG_RED_ERR_PATT);
                                                 Error = TRUE;
                                                 break;
                                                 }
                                           }
                                           break;

                                   case 4:
                                           {
                                           struct DateStamp *datestamp;

                                           if(!(datestamp = wfmh_Str2Date((char *)xget(ST_FSel_Data, MUIA_String_Contents))))
                                                 {
                                                 DisplayBeep(0);
                                                 MUI_Request(app, FakturaSelectorWindow, 0, TITLE, MSG_OK, MSG_RED_ERR_DATE);
                                                 Error = TRUE;
                                                 break;
                                                 }

                                           memcpy(&user_datestamp, datestamp, sizeof(struct DateStamp));
                                           }
                                           break;
                                   }


                                // je�li nie wszystko zosta�o przygotowane
                                // do wyszukiwania wracamy do g�ownej p�tli

                                if(Error)
                                   break;


                                // kompletny wzorzec nazw plik�w

                                sprintf(FullPattern, "%s/%s/%s/%02lx/%s", DatabasesDir,
                                                                                                                  Rok,
                                                                                                                  Miesiac,
                                                                                                                  RodzajDokumentu + DokumentDirOffset,
                                                                                                                  FileNamePattern);


                           // Szukamy...

                           set(app, MUIA_Application_Sleep, TRUE);
                           set(LV_FSel_Dokumenty, _MUIA_List_Quiet, TRUE);
                           DoMethod(LV_FSel_Dokumenty, _MUIM_List_Clear, NULL);

                           {
                           struct AnchorPath *anchor;

                           #define PATH_BUFFER 80

                           if(anchor = calloc(1, sizeof(struct AnchorPath)+PATH_BUFFER))
                                 {
                                 anchor->ap_Strlen = PATH_BUFFER;

                                 switch(SzukajWg)
                                   {
///                  W/g numer�w dokument�w / numer�w NIP
                                   case 0:
                                   case 2:
                                         {
                                         if(MatchFirst(FullPattern, anchor) == 0)
                                           {
                                           _ProcessAnchor(anchor, RodzajDokumentu);

                                           while( MatchNext(anchor) == 0 )
                                                 _ProcessAnchor(anchor, RodzajDokumentu);

                                           MatchEnd(anchor);
                                           }
                                         }
                                   break;
//|
///                  W/g nazwy odbiorcy / numer�w Regon
                                   case 1:
                                   case 3:
                                         {
                                         if(MatchFirst(FullPattern, anchor) == 0)
                                           {
                                           if(MatchPatternNoCase(pattern, anchor->ap_Info.fib_Comment))
                                                   _ProcessAnchor(anchor, RodzajDokumentu);

                                           while(MatchNext(anchor) == 0)
                                                 if(MatchPatternNoCase(pattern, anchor->ap_Info.fib_Comment))
                                                   _ProcessAnchor(anchor, RodzajDokumentu);

                                           MatchEnd(anchor);
                                           }
                                         }
                                   break;
//|
///                  W/g daty wystawienia
                                   case 4:
                                         {
                                         struct DateStamp doc_datestamp = {0};

                                         if(MatchFirst(FullPattern, anchor) == 0)
                                           {
                                           char MatchDateMode = (char)getcycle(CY_FSel_DataAccuracy);
                                           char tmp[2+4+1] = "0x";
                                           long result;

                                           memcpy(tmp, anchor->ap_Info.fib_FileName, 4);
                                           sscanf(tmp, "%lx", &doc_datestamp.ds_Days);

                                           result = CompareDates(&user_datestamp, &doc_datestamp);

                                           switch(MatchDateMode)
                                                   {
                                                   // ==
                                                   case 0:
                                                           if(result == 0)
                                                                  _ProcessAnchor(anchor, RodzajDokumentu);
                                                           break;

                                                   // >
                                                   case 1:
                                                           if(result < 0)
                                                                  _ProcessAnchor(anchor, RodzajDokumentu);
                                                           break;

                                                   // <
                                                   case 2:
                                                           if(result > 0)
                                                                  _ProcessAnchor(anchor, RodzajDokumentu);
                                                           break;
                                                   }


                                           while(MatchNext(anchor) == 0)
                                                   {
                                                   memcpy(tmp, anchor->ap_Info.fib_FileName, 4);
                                                   sscanf(tmp, "%lx", &doc_datestamp.ds_Days);

                                                   result = CompareDates(&user_datestamp, &doc_datestamp);

                                                   switch(MatchDateMode)
                                                           {
                                                           // ==
                                                           case 0:
                                                                   if(result == 0)
                                                                          _ProcessAnchor(anchor, RodzajDokumentu);
                                                                   break;

                                                           // >
                                                           case 1:
                                                                   if(result < 0)
                                                                          _ProcessAnchor(anchor, RodzajDokumentu);
                                                                   break;

                                                           // <
                                                           case 2:
                                                                   if(result > 0)
                                                                          _ProcessAnchor(anchor, RodzajDokumentu);
                                                                   break;
                                                           }
                                                   }

                                           MatchEnd(anchor);
                                           }
                                         }
                                         break;
//|
                                   }

                                 free(anchor);
                                 }
                           else
                                 {
                                 DisplayBeep(0);
                                 MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY );
                                 }
                           }
                           set(FakturaSelectorWindow, MUIA_Window_ActiveObject, LV_FSel_Dokumenty);

                           set(LV_FSel_Dokumenty, _MUIA_List_Quiet, FALSE);
                           set(app, MUIA_Application_Sleep, FALSE);

                           }
                           break;
//|
///           case ID_DELETE
                        case ID_DELETE:
                           {
                           if(xget(LV_FSel_Dokumenty, _MUIA_List_Active) != _MUIV_List_Active_Off)
                                  {
                                  DoMethod(LV_FSel_Dokumenty, _MUIM_List_Remove, _MUIV_List_Remove_Active);
                                  }
                           }
                           break;
//|
///           case ID_SORT:
                        case ID_SORT:
                           {
                           LONG  entries = xget(LV_FSel_Dokumenty, MUIA_List_Entries);
                           int   order = 1;

//               if(entries)
                                   {
                                   _sleep(TRUE);

                                   switch( TypDokument�w )
                                           {
                                           case DOKUMENTY_BIE��CE:
                                                   order = SortRequester(SORT_DOCSCAN, xget(LV_FSel_Dokumenty, MUIA_DocumentList_Order));
                                                   break;
                                           case DOKUMENTY_OD�O�ONE:
                                                   order = SortRequester(SORT_DOCSCAN_OD�O�ONE, xget(LV_FSel_Dokumenty, MUIA_DocumentList_Order));
                                                   break;
                                           }

                                   if(order != -1L)
                                          {
                                          switch( TypDokument�w )
                                                {
                                                case DOKUMENTY_OD�O�ONE:   // przeskakujemy NUMER, bo go nie ma w dokumentach od�o�onych
                                                   {
                                                   if( order >=4 )
                                                           order ++;
                                                   break;
                                                   }
                                                }

                                          set(LV_FSel_Dokumenty, MUIA_DocumentList_Order, order);
                                          DoMethod(LV_FSel_Dokumenty, MUIM_List_Sort);
//                      Uporz�dkujZakupy();
                                          }

                                   _sleep(FALSE);
                                   }
/*
                           else
                                   {
                                   DisplayBeep(0);
                                   }
*/
                           }
                           break;

//|

///           case ID_RED_NAZWAWYBIERZ:

                        case ID_RED_NAZWAWYBIERZ:
                           {
                           struct KlientList *klientList;

                           klientList = KontrahentSelector(&sel_cust_filter, NULL, 0);

//                   klient = KontrahentSelector(&sel_cust_filter, F_CUSTED_NIP_REQUIRED );

                           if( klientList )
                                   {
                                   struct Klient *klient = &klientList->kl_klient;

                                   setstring(ST_FSel_Nazwa, klient->Nazwa1);
//                   settext(TX_Nag_NIP    , NIP2Str(dp.Odbiorca->NIP, dp.Odbiorca->Sp��kaCywilna));
//                   settext(TX_Nag_Regon  , dp.Odbiorca->Regon);
                                   }
                           }
                           break;

//|
///           case ID_RED_NIPWYBIERZ:

                        case ID_RED_NIPWYBIERZ:
                           {
                           struct KlientList *klientList;

                           klientList = KontrahentSelector(&sel_cust_filter, NULL, F_CUSTED_NIP_REQUIRED );

                           if( klientList )
                                   {
                                   struct Klient *klient = &klientList->kl_klient;

                                   setstring(ST_FSel_NIP   , NIP2Str(klient->NIP, klient->Sp��kaCywilna));
//                   settext(TX_Nag_Regon  , dp.Odbiorca->Regon);
                                   }
                           }
                           break;

//|
///           case ID_RED_REGONWYBIERZ:

                        case ID_RED_REGONWYBIERZ:
                           {
                           struct KlientList *klientList;

                           klientList = KontrahentSelector(&sel_cust_filter, NULL, 0 );

                           if( klientList )
                                   {
                                   struct Klient *klient = &klientList->kl_klient;

                                   setstring(ST_FSel_Regon, klient->Regon);
                                   }
                           }
                           break;

//|

                        case ID_OK:
                           if( !DocumentSelectorFinish( ds ) )
                                 DisplayBeep(0);
                           else
                              {
                              if( CallbackFunction != NULL )
                                 {
                                 CallbackFunction( ds, TypDokument�w );
                                 }
                              else
                                 {
                                 running = FALSE;
                                 result = TRUE;
                                 }
                              }
                           break;

                        case ID_CANCEL:
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

           }
        else
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
           }

        _detachwin( FakturaSelectorWindow );

        _sleep(FALSE);

        return(result);

}
//|

