
#ifndef GOLEM_GUI_H
#define GOLEM_GUI_H

#ifdef GUI_PROTOS_H
extern Object *_Fake,
#else
Object *_Fake,
#endif

       *ToolBar,                      /* TOOLBAR OBJECT */

       *StartupWindow,                /* STARTUP WINDOW     */
       *GA_Startup_Info,
       *CR_Startup_Scroll,
       *BT_Startup_Ok,


       *MainWindow,                   /* TOOLBAR = MAIN WINDOW */

       *GR_ToolBar_Menus,

       *BT_ToolBar_KPiR,
       *GR_ToolBar_LoginLogout,
           *BT_ToolBar_Login,
           *BT_ToolBar_Logout,
       *BT_ToolBar_System,
           *BT_ToolBar_Settings,
           *BT_ToolBar_System_Back,
       *BT_ToolBar_About,
       *BT_ToolBar_Quit,




       *UstawieniaWindow,             /* USTAWIENIA WINDOW */
       *LV_Set_Strony,
       *GR_Set_Strony,


       *ST_Set_Own_Nazwa_1,
       *ST_Set_Own_Nazwa_2,
       *CH_Set_Own_SC,
       *ST_Set_Own_Ulica,
       *ST_Set_Own_Kod,
       *ST_Set_Own_Miasto,
       *ST_Set_Own_NIP,
       *ST_Set_Own_Regon,
       *ST_Set_Own_Bank,
       *ST_Set_Own_Konto,
       *ST_Set_Own_Tel,
       *ST_Set_Own_Fax,
       *ST_Set_Own_Email,

       *CY_Set_Num_FVat_Order,
       *CY_Set_Num_FVat_Type,
       *ST_Set_Num_FVat,
       *ST_Set_Num_FVat_Min,
       *CH_Set_Num_FVat_Rach,
       *CY_Set_Num_FVat_Kor_Order,
       *ST_Set_Num_FVat_Kor,
       *CY_Set_Num_FVat_Kor_Type,
       *CY_Set_Num_FVat_Par_Order,
       *ST_Set_Num_FVat_Par,
       *CY_Set_Num_FVat_Par_Type,

       *CY_Set_Num_Rach_Order,
       *CY_Set_Num_Rach_Type,
       *ST_Set_Num_Rach,
       *CY_Set_Num_Rach_Kor_Order,
       *ST_Set_Num_Rach_Kor,
       *CY_Set_Num_Rach_Kor_Type,
       *CY_Set_Num_Rach_Par_Order,
       *ST_Set_Num_Rach_Par,
       *CY_Set_Num_Rach_Par_Type,
       *ST_Set_Num_Paragon,

       *CH_Set_Spr_Reszta,

       *ST_Set_Prt_Device,
       *ST_Set_Prt_Unit,
       *ST_Set_Prt_Width_Wide,
       *ST_Set_Prt_Width_Normal,
       *ST_Set_Prt_Width_Cond,
       *CY_Set_Prt_Conv,

       *ST_Set_Prt2_Normal,
       *ST_Set_Prt2_Bold_On,
       *ST_Set_Prt2_Bold_Off,
       *ST_Set_Prt2_Italic_On,
       *ST_Set_Prt2_Italic_Off,
       *ST_Set_Prt2_Underline_On,
       *ST_Set_Prt2_Underline_Off,
       *ST_Set_Prt2_Condensed_On,
       *ST_Set_Prt2_Condensed_Off,

       *CH_Set_Pos_Active,
       *ST_Set_Pos_Device,
       *ST_Set_Pos_Unit,
       *CY_Set_Pos_A,
       *CY_Set_Pos_B,
       *CY_Set_Pos_C,
       *CY_Set_Pos_D,
       *CY_Set_Pos_Z,

       *CH_Set_Pos_Drawer,
       *CY_Set_Pos_Idle,
       *ST_Set_Pos_Idle_Txt1,
       *ST_Set_Pos_Idle_Txt2,
       *CH_Set_Pos_Nadruki,
       *ST_Set_Pos_Txt1,
       *ST_Set_Pos_Txt2,
       *ST_Set_Pos_Txt3,

       *ST_Set_Cust_Town,
       *CH_Set_Cust_UpVat,
       *ST_Set_Cust_UpVat_Expire,
       *CH_Set_Cust_UpVat_Unlimited,
       *CH_Set_Cust_AddEnabled,
       *CH_Set_Cust_FIFO,

       *CH_Set_Cust_Gr_1,
       *CH_Set_Cust_Gr_2,
       *CH_Set_Cust_Gr_3,
       *CH_Set_Cust_Gr_4,
       *CH_Set_Cust_Gr_5,
       *CH_Set_Cust_Gr_6,

       *CH_Set_Cust_Clone,

       *ST_Set_Prod_Jedn,
       *CY_Set_Prod_Vat,

       *ST_Set_Misc_Dot,

       *LV_Set_Unit,
       *BT_Set_Unit_Add,
       *BT_Set_Unit_Edit,
       *BT_Set_Unit_Del,

       *LV_Set_Pay,
       *BT_Set_Pay_Add,
       *BT_Set_Pay_Edit,
       *BT_Set_Pay_Del,


       *CH_Set_Doc_Print,
       *ST_Set_Doc_Original,
       *ST_Set_Doc_Copy,
       *ST_Set_Doc_Dupe,
       *CH_Set_Doc_Print_Dupe,
       *ST_Set_Doc_NoRece,
       *ST_Set_Doc_UpVat,
       *ST_Set_Doc_Copies,
       *CH_Set_Doc_SWW,
       *CH_Set_Doc_FF,

       *ST_Set_Doc2_TXT1,
       *ST_Set_Doc2_TXT2,
       *ST_Set_Doc2_TXT3,
       *ST_Set_Doc2_TXT4,
       *ST_Set_Doc2_TXT5,
       *ST_Set_Doc2_TXT6,
       *ST_Set_Doc2_TXT7,
       *ST_Set_Doc2_TXT8,
       *ST_Set_Doc2_TXT9,
       *ST_Set_Doc2_TXT0,

       *CH_Set_Com_Call,
       *ST_Set_Com_Sale,

       *BT_Set_MUI,


       *BT_Set_Ok,
       *BT_Set_Use,
       *BT_Set_Cancel,




       *ProgressWindow,                    /* PROGRESS BAR */
       *TX_Prog_Info,
       *GA_Prog_Info,


       *LoginWindow,                       /* LOGIN WINDOW */
       *ST_Login_Has�o,
       *BT_Login_Ok,
       *BT_Login_Cancel,



       *OdbiorcaSelectorWindow,    /* ODBIORCA SELECTOR */
       *LV_Pod_Odbiorcy,
       *BT_Pod_Ok,
       *BT_Pod_Add,
       *BT_Pod_Cancel,

/*
       *StringRequesterWindow,     /* STRING REQUESTER */
       *ST_Str_String,
       *BT_Str_Ok,
       *BT_Str_Cancel,                                   
*/



       *KontrahentSelectorWindow,  /* KONTRAHENT SELECTOR */
       *LV_Kon_Klienci,
       *BT_Kon_Add,
       *BT_Kon_Filter,
       *BT_Kon_FilterAll,
       *BT_Kon_Ok,
       *BT_Kon_Cancel,



       *ZakupyWindow,              /* ZAKUPY WINDOW */
       *CY_Zak_Miesiac,
       *BT_Zak_Load,
       *BT_Zak_Save,
       *BT_Zak_Index,
       *BT_Zak_Sort,
       *LV_Zak_Zakupy,
       *BT_Zak_Add,
       *BT_Zak_Edit,
       *BT_Zak_Del,
       *BT_Zak_Info,
       *BT_Zak_Print,
       *BT_Zak_Ok,
       *BT_Zak_Cancel,


       *EdycjaZakupuWindow,         /* EDYCJA ZAKUPU WINDOW */
       *GR_ZakE_Pages,
       *ST_ZakE_LP,

       *ST_ZakE_P1,
       *ST_ZakE_P2,
       *ST_ZakE_P3,
       *ST_ZakE_P4,
       *BT_ZakE_Kontrahent,
       *TX_ZakE_P5,
       *TX_ZakE_P6,
       *TX_ZakE_P7,
       *BT_ZakE_Next1,
       *ST_ZakE_P9,
       *ST_ZakE_P10,
       *TX_ZakE_P11,
       *ST_ZakE_P12,
       *TX_ZakE_P13,
       *BT_ZakE_Next2,
       *ST_ZakE_P14,
       *TX_ZakE_P15,
       *ST_ZakE_P16,
       *TX_ZakE_P17,
       *BT_ZakE_Next3,

//       *TX_ZakE_P8,
//       *TX_ZakE_P18,


       *BT_ZakE_Ok,
       *BT_ZakE_Cancel,


       *InformacjeOZakupachWindow,  /* INFORMACJE O ZAKUPACH */
       *TX_ZakI_Count,
       *TX_ZakI_P8,
       *TX_ZakI_P9,
       *TX_ZakI_P10,
       *TX_ZakI_P11,
       *TX_ZakI_P12,
       *TX_ZakI_P13,
       *TX_ZakI_P14,
       *TX_ZakI_P15,
       *TX_ZakI_P16,
       *TX_ZakI_P17,
       *TX_ZakI_P18,

       *BT_ZakI_Ok,


       *IndexRequesterWindow,       /* INDEX REQUESTER      */
       *ST_Idx_String,
       *BT_Idx_Ok,
       *BT_Idx_Cancel,


       *SortRequesterWindow,        /* SORT REQUESTER       */
       *GR_Sort_Order,
       *RA_Sort_Order,
       *CH_Sort_Reverse,
       *BT_Sort_Ok,
       *BT_Sort_Cancel,


       *FAKE;

#endif

