
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

#include "golem.h"

/// JEDN STOP CHUNKS
#define JEDN_NUM_STOPS (sizeof(Jedn_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Jedn_Stops[] =
{
        ID_UNIT, ID_CAT,
        ID_UNIT, ID_VERS,
        ID_UNIT, ID_UNIT,
        NULL, NULL,
};
//|

/// WczytajJednostki

long WczytajJednostki(void)
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
long   Errors = 0;
char   ValidFile = FALSE;


     set(app, MUIA_Application_Sleep, TRUE);

     if(iff = AllocIFF())
       {
       if(iff->iff_Stream = Open(JednostkiFileName, MODE_OLDFILE))
           {
           InitIFFasDOS(iff);

           StopChunks(iff, Jedn_Stops, JEDN_NUM_STOPS);

           if(!OpenIFF(iff, IFFF_READ))
               {

               while(TRUE)
                  {
                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                     break;

                  if(cn = CurrentChunk(iff))
                     {
                     LONG ID = cn->cn_ID;

                     if(!ValidFile)
                        {
                        if((ID == ID_CAT) && (cn->cn_Type == ID_UNIT))
                           {
                           ValidFile = TRUE;
                           continue;
                           }


                        break;
                        }

///                    ID_VERS
                     if(ID == ID_VERS)
                        {
                        struct BaseVersion version;

                        if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                           {

                           }
                        else
                           {
                           _RC = IoErr();
                           break;
                           }

                        continue;
                        }
//|
///                    ID_UNIT

                     if(ID == ID_UNIT)
                        {
                        struct JednostkaList *jedn = calloc(1, sizeof(struct JednostkaList));

                        if(jedn)
                           {
                           if(ReadChunkBytes(iff, &jedn->ul_jedn, sizeof(struct Jednostka)) != cn->cn_Size)
                              {
                              free(jedn);
                              _RC = IoErr();
                              break;
                              }

                           DodajJednostk�(jedn);
                           }

                        continue;
                        }
//|

                     }
                  }

               CloseIFF(iff);
               }

            Close(iff->iff_Stream);
            }


//       if(_RC == IFFERR_EOF) Error = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

       FreeIFF(iff);
       }

       set(app, MUIA_Application_Sleep, FALSE);


       return(0);
}

//|
/// ZapiszJednostki

char ZapiszJednostki(void)
{

struct IFFHandle *MyIFFHandle;

    set(app, MUIA_Application_Sleep, TRUE);

    if(MyIFFHandle = AllocIFF())
        {
        BPTR  FileHandle;

        if(FileHandle = Open(JednostkiFileName, MODE_NEWFILE))
           {
           MyIFFHandle->iff_Stream = FileHandle;
           InitIFFasDOS(MyIFFHandle);

           if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
               {
               struct BaseVersion version;
               struct JednostkaList *jedn;

               PushChunk(MyIFFHandle, ID_UNIT, ID_CAT, IFFSIZE_UNKNOWN);

               PushChunk(MyIFFHandle, ID_UNIT, ID_FORM, IFFSIZE_UNKNOWN);
                   PushChunk(MyIFFHandle, ID_UNIT, ID_VERS, IFFSIZE_UNKNOWN);
                   version.Version = VERSION;
                   version.Revision = REVISION;
                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                   PopChunk(MyIFFHandle);

                   PushChunk(MyIFFHandle, ID_UNIT, ID_ANNO, IFFSIZE_UNKNOWN);
                   WriteChunkBytes(MyIFFHandle, ScreenTitle, strlen(ScreenTitle));
                   WriteChunkBytes(MyIFFHandle, " <", 2);
                   WriteChunkBytes(MyIFFHandle, EMAIL, strlen(EMAIL));
                   WriteChunkBytes(MyIFFHandle, ">, ", 3);
                   WriteChunkBytes(MyIFFHandle, WWW, strlen(WWW));
                   PopChunk(MyIFFHandle);
               PopChunk(MyIFFHandle);


               PushChunk(MyIFFHandle, ID_UNIT, ID_FORM, IFFSIZE_UNKNOWN);

               if(!IsListEmpty(&jedn_miary))
                 {
                 for(jedn = (struct JednostkaList *)jedn_miary.lh_Head; jedn->ul_node.mln_Succ; jedn = (struct JednostkaList *)jedn->ul_node.mln_Succ)
                   {
                   PushChunk(MyIFFHandle, ID_UNIT, ID_UNIT, IFFSIZE_UNKNOWN);
                   WriteChunkBytes(MyIFFHandle, &jedn->ul_jedn, sizeof(struct Jednostka));
                   PopChunk(MyIFFHandle);
                   }
                 }

               PopChunk(MyIFFHandle);

               PopChunk(MyIFFHandle);
               CloseIFF(MyIFFHandle);
               }
           else
               {
               DisplayBeep(0);
               D(bug("*** OpenIFF() nie powiod�o si�\n"));
               }

           Close(FileHandle);
           }
        else
           {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
           D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", JednostkiFileName));
           }

        FreeIFF(MyIFFHandle);
        }
     else
        {
        DisplayBeep(0);
        D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
        }

    set(app, MUIA_Application_Sleep, FALSE);


    return(0);
}



//|

/// DodajJednostk�

int DodajJednostk�(struct JednostkaList *jedn)
{
/*
** Dodaje jednostk� do listy
*/

struct JednostkaList *node;
int i = 0;

    if(!IsListEmpty(&jedn_miary))
       {
       for(node = (struct JednostkaList *)jedn_miary.lh_Head; node->ul_node.mln_Succ; node = (struct JednostkaList *)node->ul_node.mln_Succ, i++)
           {
           if(StrnCmp(MyLocale, jedn->ul_jedn.Nazwa, node->ul_jedn.Nazwa, -1, SC_COLLATE2) < 0)
               {
               if((struct Node *)node->ul_node.mln_Pred)
                   Insert(&jedn_miary, (struct Node *)jedn, (struct Node *)node->ul_node.mln_Pred);
               else
                   AddHead(&jedn_miary, (struct Node *)jedn);

               return(i);
               }
           }
       }

       AddTail(&jedn_miary, (struct Node *)jedn);
       return(MUIV_List_Insert_Bottom);
}
//|
/// Znajd�Jednostk�Nazwa

struct JednostkaList *Znajd�Jednostk�Nazwa(char *Nazwa)
{
/*
** Przeszukuj� list� jednostek miary w poszukiwaniu
** tej o podanej nazwie
*/

struct JednostkaList *node;

    if(strlen(Nazwa))
       {
       if(!IsListEmpty(&jedn_miary))
           {
           for(node = (struct JednostkaList *)jedn_miary.lh_Head; node->ul_node.mln_Succ; node = (struct JednostkaList *)node->ul_node.mln_Succ)
               {
               if( StrnCmp(MyLocale, Nazwa, node->ul_jedn.Nazwa, -1, SC_COLLATE2) == 0 )
                  return(node);
               }
           }
       }

    return(NULL);
}

//|
/// Usu�Jednostki
void Usu�Jednostki(char DeleteAll)
{
/*
** Usuwa skasowane jednostki (je�li DeleteAll=FALSE)
** albo ca�� list� (je��i DeleteAll=TRUE)
*/

struct JednostkaList   *jedn, *ul_next;

    if(!IsListEmpty(&jedn_miary))
       {
       char Delete = FALSE;

       for(jedn = (struct JednostkaList *)jedn_miary.lh_Head; jedn->ul_node.mln_Succ; )
           {
           Delete = DeleteAll;

           if(DeleteAll == FALSE)
               Delete = jedn->Deleted;

           ul_next = (struct JednostkaList *)jedn->ul_node.mln_Succ;

           if(Delete)
               {
               Remove((struct Node*)jedn);
               free(jedn);
               }

           jedn = ul_next;
           }
       }
}
//|

/// ZaktualizujJednostkiWMagazynie
int ZaktualizujJednostkiWMagazynie( struct JednostkaList *Jedn )
{
/*
** Aktualizuje wszystkie jednostki miary uzywane przez magazyn
*/

struct GrupaList     *grupa;
struct ProductList   *produkt;
struct JednostkaList *jedn;
int    count = 0;


    if(!IsListEmpty(&grupy))
       {
       // smigamy po wszystkich grupach magazynowych...
       for(grupa = (struct GrupaList *)grupy.lh_Head; grupa->gm_node.mln_Succ; grupa = (struct GrupaList *)grupa->gm_node.mln_Succ)
           {
           // grupa zawiera cos aby?
           if(!IsListEmpty(&grupa->produkty))
               {
               // i owszem...
               for(produkt = (struct ProductList *)grupa->produkty.lh_Head; produkt->pl_node.mln_Succ; produkt = (struct ProductList *)produkt->pl_node.mln_Succ)
                   {
                   // czy aktualizujemy tylko konkretna jednostke?
                   if( Jedn )
                       {
                       // si.
                       if( StrnCmp(MyLocale, produkt->pl_prod.Jedn.Nazwa, Jedn->ul_jedn.Nazwa, -1, SC_COLLATE2) == 0 )
                           {
                           // to wlasnie ta...
                           if( jedn = Znajd�Jednostk�Nazwa( produkt->pl_prod.Jedn.Nazwa ) )
                              {
                              memcpy(&produkt->pl_prod.Jedn, &jedn->ul_jedn, sizeof(struct Jednostka) );
                              count++;
                              }
                           }
                       }
                   else
                       {
                       // lukap do listy definicji jednostek
                       if( jedn = Znajd�Jednostk�Nazwa( produkt->pl_prod.Jedn.Nazwa ) )
                           {
                           // i copy jak nalezy...
                           memcpy(&produkt->pl_prod.Jedn, &jedn->ul_jedn, sizeof(struct Jednostka) );
                           count++;
                           }
                       }

                   }
               }
           }
       }

    return( count );
}
//|


/// EDYCJA JEDNOSTKI MIARY STRINGS

#define MSG_ED_UNIT_WIN_TITLE "Jednostka miary"

#define MSG_ED_UNIT_UPDATE_REQ  "Czy na pewno chcesz zaktualizowa�\nwszystkie produkty w magazynie\nuzywaj�ce jednostki \"\033b%s\033n\"?"
#define MSG_ED_UNIT_UPDATE_GAD  "_Tak|*_Nie"

#define MSG_ED_UNIT_NAME   "_Nazwa"
#define MSG_ED_UNIT_FRAC   "Ca�ko_wita"
#define MSG_ED_UNIT_UPDATE "F5 - _Zaktualizuj magazyn"
#define MSG_ED_UNIT_OK     "F10 - _Ok"
#define MSG_ED_UNIT_CANCEL "ESC - Ponie_chaj"

//|

static Object *EdycjaJednostkiWindow,
              *ST_Unit_Nazwa,
              *CH_Unit_Frac,
              *BT_Unit_Update,
              *BT_Unit_Ok,
              *BT_Unit_Cancel;

/// EdycjaJednostkiSetup
char EdycjaJednostkiSetup(struct JednostkaList *Jedn)
{
struct Jednostka *jedn = &Jedn->ul_jedn;


///  EdycjaJednostkiMiary

                     EdycjaJednostkiWindow = WindowObject,
                        MUIA_Window_Title      , MSG_ED_UNIT_WIN_TITLE,
                        MUIA_Window_ID         , ID_WIN_ED_UNIT,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, ColGroup(2),
                                  GroupFrame,

                                  Child, MakeLabel2(MSG_ED_UNIT_NAME),
                                  Child, ST_Unit_Nazwa = MakeString(PROD_JEDN_LEN, MSG_ED_UNIT_NAME),

                                  Child, MakeLabel2(MSG_ED_UNIT_FRAC),
                                  Child, HGroup,
                                         Child, CH_Unit_Frac = MakeCheck(MSG_ED_UNIT_FRAC),
                                         Child, HVSpace,
                                         End,
                                  End,


                           Child, HGroup,
                                  GroupFrame,
                                  Child, BT_Unit_Update = TextButton(MSG_ED_UNIT_UPDATE),
                                  End,


                           Child, HGroup,
                                  MUIA_Group_SameSize, TRUE,
                                  Child, BT_Unit_Ok     = TextButton(MSG_ED_UNIT_OK),
                                  Child, BT_Unit_Cancel = TextButton(MSG_ED_UNIT_CANCEL),
                                  End,

                           End,
                      End;
//|


    if( !EdycjaJednostkiWindow)
       return(FALSE);


    // settings

    setstring(ST_Unit_Nazwa, jedn->Nazwa);
    set( CH_Unit_Frac, MUIA_Selected, jedn->Ca�kowita);


    // notifications

    DoMethod(EdycjaJednostkiWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

    DoMethod(EdycjaJednostkiWindow, MUIM_Notify, MUIA_Window_InputEvent, "f5" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ED_UNIT_UPDATE);
    DoMethod(EdycjaJednostkiWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

    DoMethod(BT_Unit_Update , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ED_UNIT_UPDATE);

    DoMethod(BT_Unit_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
    DoMethod(BT_Unit_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

//    set(CH_Unit_Frac, MUIA_CycleChain, FALSE);



    set(EdycjaJednostkiWindow, MUIA_Window_ActiveObject, ST_Unit_Nazwa);

    _attachwin( EdycjaJednostkiWindow );


    return(TRUE);

}
//|
/// EdycjaJednostkiFinish
char EdycjaJednostkiFinish(struct JednostkaList *Jedn, char EditMode)
{
/*
** sprawdza poprawno�� wprowadzonych danych dot. jednostki.
** Zwraca FALSE, je�li co� est nie tak, TRUE, je�li wszystkie
** elementy s� poprawne, oraz REFRESH, ** je�li nazwa produktu
** zosta�a zmieniona (istotne tylko podczas edycji produktu)
*/

struct Jednostka *jedn = &Jedn->ul_jedn;
char   result = TRUE;


    if(strlen((char *)xget(ST_Unit_Nazwa, MUIA_String_Contents)) == 0)
       {
       set(EdycjaJednostkiWindow, MUIA_Window_ActiveObject, ST_Unit_Nazwa);
       return(FALSE);
       }


    {
    struct JednostkaList *tmp;

    if(EditMode)
      {
      Remove((struct Node *)Jedn);
      tmp = Znajd�Jednostk�Nazwa((char *)xget(ST_Unit_Nazwa, MUIA_String_Contents));
      DodajJednostk�(Jedn);
      }
    else
      {
      tmp = Znajd�Jednostk�Nazwa((char *)xget(ST_Unit_Nazwa, MUIA_String_Contents));
      }

    if(tmp)
       {
       set(EdycjaJednostkiWindow, MUIA_Window_ActiveObject, ST_Unit_Nazwa);
       return(FALSE);
       }
    }

//    if(StrnCmp(MyLocale, jedn->Nazwa, (char *)xget(ST_Unit_Nazwa, MUIA_String_Contents), -1, SC_COLLATE2) != 0)
       result = REFRESH;

    strcpy(jedn->Nazwa    , (char *)xget(ST_Unit_Nazwa, MUIA_String_Contents));
// checkmark!!

    jedn->Ca�kowita = getcheckmark( CH_Unit_Frac );
    Jedn->Imported = FALSE;


    return(result);

}
//|
/// EdycjaJednostki

char EdycjaJednostki(struct JednostkaList *jedn, char EditMode)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;


    set(app, MUIA_Application_Sleep, TRUE);

    if( !EdycjaJednostkiSetup(jedn) )
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       _sleep(FALSE);
       return(NULL);
       }


    if(WinOpen( EdycjaJednostkiWindow ))
       {
        while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);
          switch(ID)
            {
            case ID_ED_UNIT_UPDATE:
               {
               if( MUI_Request(app, EdycjaJednostkiWindow, 0, TITLE, MSG_ED_UNIT_UPDATE_GAD, MSG_ED_UNIT_UPDATE_REQ, getstr(ST_Unit_Nazwa) ) )
                   {
                   int count;
                   _sleep(TRUE);
                   count = ZaktualizujJednostkiWMagazynie( NULL );

                   if( count )
                       {
                       ZapiszMagazynProgress();
                       }

                   _sleep(FALSE);
                   }
               }
               break;


            case ID_OK:
               result = EdycjaJednostkiFinish(jedn, EditMode);

               if(result == FALSE)
                 {
                 DisplayBeep(0);
                 }
               else
                 {
                 running = FALSE;
                 }
               break;


            case ID_CANCEL:
               running = FALSE;
               break;

            }
          if(running && signal) Wait(signal);
          }
       }
    else
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       }

    _detachwin( EdycjaJednostkiWindow );
    _sleep(FALSE);

    return(result);

}

//|


/// EDYCJA JEDN MIARY STRINGS

#define MSG_UNIT_WIN_TITLE "Jednostki miary"

#define MSG_UNIT_TITLE       "Jednostki miary"
#define MSG_UNIT_ADD         "F1 - Dod_aj"
#define MSG_UNIT_EDIT        "F2 - _Edytuj"
#define MSG_UNIT_DEL         "DEL - S_kasuj"
#define MSG_UNIT_MAX         "\033cMo�esz zadeklarowa� maksymalnie\n%ld rodzaj�w jednostek miar."

#define MSG_UNIT_DELETED_GAD "*_Tak|_Nie"
#define MSG_UNIT_DELETED     "Jednostka \"%s\"\njest aktualnie zaznaczona jako \033bSKASOWANA\033n,\ni nie mo�e by� modyfikowana. Czy chcesz j� teraz\nodkasowa� i przyst�pi� do edycji?"

#define MSG_UNIT_UPDATE_REQ  "Czy na pewno chcesz zaktualizowa�\njednostki miar wszystkich produkt�w\nznajduj�cych si� aktualnie w magazynie?"
#define MSG_UNIT_UPDATE_GAD  "_Tak|*_Nie"
#define MSG_UNIT_UPDATE      "F5 - Zaktualizuj magazyn"

#define MSG_UNIT_OK          "F10 - _Ok"
#define MSG_UNIT_CANCEL      "ESC - Ponie_chaj"

//|
/// UstawieniaJednSetup

static Object
       *UstawieniaJednWindow,
       *LV_Set_Unit,
       *BT_Set_Unit_Add,
       *BT_Set_Unit_Edit,
       *BT_Set_Unit_Del,
       *BT_Set_Unit_Update,
       *BT_Set_Unit_Ok,
       *BT_Set_Unit_Cancel;

char UstawieniaJednSetup(void)
{

///   Create Window

      UstawieniaJednWindow = WindowObject,
                        MUIA_Window_ID         , ID_WIN_UNITSETTINGS,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        MUIA_Window_Title, MSG_UNIT_WIN_TITLE,
                        WindowContents,
                           VGroup,

                           Child, VGroup,
                                  GroupFrameT(MSG_UNIT_TITLE),

                                  Child, LV_Set_Unit = _ListviewObject,
                                         _MUIA_List_AutoVisible, TRUE,
                                         _MUIA_Listview_List, Golem_UnitListObject, End,
                                         End,

                                  Child, HGroup,
                                         Child, BT_Set_Unit_Add  = TextButton(MSG_UNIT_ADD),
                                         Child, BT_Set_Unit_Edit = TextButton(MSG_UNIT_EDIT),
                                         Child, BT_Set_Unit_Del  = TextButton(MSG_UNIT_DEL),
                                         End,

                                  End,

                           Child, HGroup,
                                  GroupFrame,
                                  Child, BT_Set_Unit_Update = TextButton(MSG_UNIT_UPDATE),
                                  End,


                           Child, HGroup,
                                  MUIA_Group_SameSize, TRUE,
                                  Child, BT_Set_Unit_Ok     = TextButton(MSG_UNIT_OK),
                                  Child, BT_Set_Unit_Cancel = TextButton(MSG_UNIT_CANCEL),
                                  End,


                           End,
                      End;
//|


    if( !UstawieniaJednWindow )
       return(FALSE);



    // Jednostki miary

    {
    struct JednostkaList *jedn;

    if(!IsListEmpty(&jedn_miary))
       {
       for(jedn = (struct JednostkaList *)jedn_miary.lh_Head; jedn->ul_node.mln_Succ; jedn = (struct JednostkaList *)jedn->ul_node.mln_Succ)
         {
         DoMethod(LV_Set_Unit, _MUIM_List_InsertSingle, jedn, _MUIV_List_Insert_Sorted);
         }
       }
    }


    // notyfikacje

    DoMethod( UstawieniaJednWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL );

    DoMethod(LV_Set_Unit,        MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SET_UNIT_EDIT);
    DoMethod(BT_Set_Unit_Add,    MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SET_UNIT_ADD);
    DoMethod(BT_Set_Unit_Edit,   MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SET_UNIT_EDIT);
    DoMethod(BT_Set_Unit_Del,    MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SET_UNIT_DEL);
    DoMethod(BT_Set_Unit_Update, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_SET_UNIT_UPDATE);

    DoMethod(BT_Set_Unit_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
    DoMethod(BT_Set_Unit_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

    DoMethod(UstawieniaJednWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_SET_UNIT_ADD);
    DoMethod(UstawieniaJednWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_SET_UNIT_EDIT);
    DoMethod(UstawieniaJednWindow, MUIM_Notify, MUIA_Window_InputEvent, "f5" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_SET_UNIT_UPDATE);
    DoMethod(UstawieniaJednWindow, MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_SET_UNIT_DEL);

    DoMethod(UstawieniaJednWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

    _attachwin( UstawieniaJednWindow );

    set(UstawieniaJednWindow, MUIA_Window_ActiveObject, LV_Set_Unit);

    return( TRUE );

}
//|
/// UstawieniaJedn
char UstawieniaJedn(void)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

    _sleep(TRUE);


    if(!UstawieniaJednSetup())
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       _sleep(FALSE);
       return(NULL);
       }


    if(WinOpen(UstawieniaJednWindow))
        {
        while(running)
          {
          long ID = DoMethod(app, MUIM_Application_Input, &signal);
          switch(ID)
            {


///           JEDNOSTKI MIARY

            case ID_SET_UNIT_ADD:
               {
               struct JednostkaList *jedn;

               if(xget(LV_Set_Unit, _MUIA_List_Entries) >= MAX_UNITS)
                  {
                  DisplayBeep(0);
                  MUI_Request(app, UstawieniaJednWindow, 0, TITLE, MSG_OK, MSG_UNIT_MAX, MAX_UNITS);
                  break;
                  }

               if((jedn = calloc(1, sizeof(struct JednostkaList))) != NULL)
                  {
                  if
                  (EdycjaJednostki(jedn, FALSE))
                    {
                    long pos = DodajJednostk�(jedn);

                    DoMethod(LV_Set_Unit, _MUIM_List_InsertSingle, jedn, pos);
                    }
                  }
               else
                  {
                  DisplayBeep(0);
                  MUI_Request(app, UstawieniaJednWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                  }
               }
               break;



            case ID_SET_UNIT_EDIT:
               {
               if(xget(LV_Set_Unit, _MUIA_List_Active) != _MUIV_List_Active_Off)
                 {
                 struct JednostkaList *jedn;

                 DoMethod(LV_Set_Unit, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &jedn);

                 if(jedn->Deleted)
                   {
                   if(MUI_Request(app, UstawieniaJednWindow, 0, TITLE, MSG_UNIT_DELETED_GAD, MSG_UNIT_DELETED, jedn->ul_jedn.Nazwa))
                     {
                     jedn->Deleted = FALSE;
                     DoMethod(LV_Set_Unit, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                     }
                   else
                     break;
                   }


                 switch(EdycjaJednostki(jedn, TRUE))
                       {
                       case TRUE:
                            DoMethod(LV_Set_Unit, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                            break;

                       case REFRESH:
                            {
                            long pos;
                            set(LV_Set_Unit, _MUIA_List_Quiet, TRUE);

                            DoMethod(LV_Set_Unit, _MUIM_List_Remove, _MUIV_List_Remove_Active);

                            Remove((struct Node*)jedn);
                            pos = DodajJednostk�(jedn);

                            DoMethod(LV_Set_Unit, _MUIM_List_InsertSingle, jedn, pos);
                            set(LV_Set_Unit, _MUIA_List_Active, pos);

                            set(LV_Set_Unit, _MUIA_List_Quiet, FALSE);
                            }
                            break;
                       }
                 }
               }
               break;



            case ID_SET_UNIT_DEL:
               {
               if(xget(LV_Set_Unit, _MUIA_List_Active) != _MUIV_List_Active_Off)
                   {
                   struct JednostkaList *Jedn;

                   DoMethod(LV_Set_Unit, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &Jedn);

                   Jedn->Deleted = ((Jedn->Deleted + 1) & 0x1);

                   DoMethod(LV_Set_Unit, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                   }
               }
               break;


            case ID_SET_UNIT_UPDATE:
               {
               if(MUI_Request(app, UstawieniaJednWindow, 0, TITLE, MSG_UNIT_UPDATE_GAD, MSG_UNIT_UPDATE_REQ))
                   {
                   int count;
                   _sleep(TRUE);
                   count = ZaktualizujJednostkiWMagazynie( NULL );

                   if( count )
                       {
                       ZapiszMagazynProgress();
                       }

                   _sleep(FALSE);
                   }
               }
               break;

//|


            case ID_OK:
               {
               _sleep(TRUE);

               Usu�Jednostki(FALSE);
               ZapiszJednostki();

               _sleep(FALSE);
               result  = TRUE;
               running = FALSE;
               }
               break;


            case ID_CANCEL:
               {
               _sleep(TRUE);

               Usu�Jednostki(TRUE);
               WczytajJednostki();

               _sleep(FALSE);
               running = FALSE;
               }
               break;

            }
          if(running && signal) Wait(signal);
          }
        }
     else
        {
        DisplayBeep(NULL);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        }


    _detachwin(UstawieniaJednWindow);
    _sleep(FALSE);

    return(result);

}
//|

