
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
$Id: golem_redagowanie.c,v 1.1 2003/01/01 20:40:52 carl-os Exp $.
*/

#include "Golem.h"


/// REDAGOWANIE WINDOW STRINGS

#define MSG_RED_DS_NAZWA  "Nazwa odbiorcy"
#define MSG_RED_DS_NIP    "NIP"
#define MSG_RED_DS_REGON  "Regon"
#define MSG_RED_DS_NUMER  "Numer dokumentu"
#define MSG_RED_DS_DATA   "Data wystawienia"

#define MSG_RED_WYSWIETL  "F1 - _Wyswietl"
#define MSG_RED_USUN      "DEL - _Usu� z listy"
#define MSG_RED_OK        "F10 - Zak_o�cz redagowanie"

#define MSG_RED_PRINT     "F1 - Wy_drukuj duplikat"
#define MSG_RED_REEDIT    "F1 - Doko�cz _edycj�"
#define MSG_RED_EDIT_DOC  "F5 - _Edytuj"
#define MSG_RED_PRINT_DOC "F6 - _Wystaw dokument"

#define MSG_RED_ERR_PATT  "\033cB��d podczas przetwarzania wzorca!\nSprawd� poprawno�� zapisu!"
#define MSG_RED_ERR_DATE  "\033cB��d podczas przetwarzania daty!\nSprawd� poprawno�� zapisu!"

#define MSG_RED_GR_VIEW_TITLE "Przegl�danie i edycja dokument�w"

#define MSG_RED_VIEW_NAME      "Dokument"
#define MSG_RED_VIEW_DATA_WYST "Wystawiony"
#define MSG_RED_VIEW_DATA_PLAT "P�atno��"
#define MSG_RED_VIEW_WYST      "Wystawi�"
#define MSG_RED_VIEW_ODEBRAL   "Odebra�"
#define MSG_RED_VIEW_DOZAPLATY "Do zap�aty"

//|
/// RedagowanieSetup

static Object
       *RedagowanieWindow,                 /* REDAGOWANIE WINDOW */

       *TX_Red_View_Nazwa,
       *TX_Red_View_Data_Wyst,
       *TX_Red_View_Data_P�at,
       *TX_Red_View_Wyst,
       *GR_Red_View_Odebral,
       *TX_Red_View_Odebral,
       *GR_Red_View_Do_Zaplaty,
       *TX_Red_View_Do_Zaplaty,
       *LV_Red_View_Produkty,
//       *BT_Red_Redaguj,
//       *BT_Red_Anuluj,
//       *BT_Red_Korekta,
       *BT_Red_Wydrukuj,
       *BT_Red_PrintDoc,
       *BT_Red_Redaguj,

       *BT_Red_Ok;



char RedagowanieSetup( char TypDokument�w )
{
char *bt_text;       // duplikat / wroc do edycji

    switch( TypDokument�w)
       {
       case DOKUMENTY_BIE��CE:
           bt_text   = MSG_RED_PRINT;

           break;

       case DOKUMENTY_OD�O�ONE:
           bt_text   = MSG_RED_REEDIT;

           break;
       }


///   RedagowanieWindow

      RedagowanieWindow = WindowObject,
                        MUIA_Window_Title      , MSG_RED_GR_VIEW_TITLE,
                        MUIA_Window_ID         , ID_WIN_RED,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                          VGroup,

///                         Strona #2

                           Child, VGroup,
                                  Child, ColGroup(2),
                                         GroupFrameT(MSG_RED_GR_VIEW_TITLE),
                                         GroupFrame,

                                         Child, MakeLabel2(MSG_RED_VIEW_NAME),
                                         Child, TX_Red_View_Nazwa = TextObject,
                                                TextFrame, TextBack,
                                                End,

                                         Child, MakeLabel2(MSG_RED_VIEW_DATA_WYST),
                                         Child, HGroup,
                                                Child, TX_Red_View_Data_Wyst = TextObject,
                                                       TextFrame, TextBack,
                                                       End,

                                                Child, MakeLabel2(MSG_RED_VIEW_DATA_PLAT),
                                                Child, TX_Red_View_Data_P�at = TextObject,
                                                       TextFrame, TextBack,
                                                       End,
                                                End,

                                         Child, MakeLabel2(MSG_RED_VIEW_WYST),
                                         Child, HGroup,
                                                Child, TX_Red_View_Wyst = TextObject,
                                                       TextFrame, TextBack,
                                                       End,

                                                Child, GR_Red_View_Odebral = HGroup,
                                                       Child, MakeLabel2(MSG_RED_VIEW_ODEBRAL),
                                                       Child, TX_Red_View_Odebral = TextObject,
                                                              TextFrame, TextBack,
                                                              End,
                                                       End,

                                                Child, GR_Red_View_Do_Zaplaty = HGroup,
                                                       Child, MakeLabel2(MSG_RED_VIEW_DOZAPLATY),
                                                       Child, TX_Red_View_Do_Zaplaty = TextObject,
                                                              TextFrame, TextBack,
                                                              End,
                                                       End,
                                                End,
                                         End,

//                                  Child, VGroup,
//                                         GroupFrame,

                                         Child, LV_Red_View_Produkty = ListviewObject,
                                                MUIA_CycleChain, TRUE,
                                                _MUIA_Listview_List, NewObject(CL_FakturaProductList->mcc_Class, NULL, TAG_DONE),
                                                End,

/*
                                         Child, HGroup,
                                                Child, BT_Red_Redaguj  = TextButton(MSG_RED_REDAGUJ),
                                                Child, BT_Red_Anuluj   = TextButton(MSG_RED_ANULUJ),
                                                Child, BT_Red_Korekta  = TextButton(MSG_RED_KOREKTA),
                                                End,
*/

//                                         End,

                                  Child, HGroup,
                                         Child, BT_Red_Wydrukuj = TextButton(bt_text),
                                         Child, BT_Red_Redaguj  = TextButton(MSG_RED_EDIT_DOC),
                                         Child, BT_Red_PrintDoc = TextButton(MSG_RED_PRINT_DOC),

                                         Child, BT_Red_Ok = TextButton(MSG_RED_OK),
                                         End,
                                  End,

//|

                          End,
                        End;

//|


    if( ! RedagowanieWindow )
       return( FALSE );



    // settings,

//    set(RedagowanieWindow, MUIA_Window_ActiveObject, CY_Red_Typ);


    //notifications

    /* REDAGOWANIE WINDOW */

    DoMethod(RedagowanieWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

    DoMethod(RedagowanieWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application , 2, MUIM_Application_ReturnID, ID_RED_PRINT);
    DoMethod(RedagowanieWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);


//       DoMethod(BT_Red_Redaguj   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_RED);
//       DoMethod(BT_Red_Anuluj    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_CANCEL);
//       DoMethod(BT_Red_Korekta   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_KOREKTA);

    DoMethod(BT_Red_PrintDoc  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_PRINT_DOC);
    DoMethod(BT_Red_Redaguj   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_EDIT_DOC);

    DoMethod(BT_Red_Wydrukuj  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_PRINT);
    DoMethod(BT_Red_Ok        , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);

    _attachwin( RedagowanieWindow );

    return( TRUE );

}
//|

/// HOOK: ProductList_Construct
APTR __saveds __asm ProductList_Construct(register __a2 APTR pool, register __a1 struct ProductList *node)
{
struct  ProductList *new;

        if(new = calloc(1, sizeof(struct ProductList)))
                 memcpy(new, node, sizeof(struct ProductList));

        return(new);
}
//|
/// HOOK: ProductList_Destruct
VOID __saveds __asm ProductList_Destruct(register __a2 APTR pool, register __a1 struct ProductList *node)
{
        free(node);
}
//|

static const struct Hook ProductList_ConstructHook = { {NULL, NULL}, (VOID *)ProductList_Construct ,NULL,NULL };
static const struct Hook ProductList_DestructHook  = { {NULL, NULL}, (VOID *)ProductList_Destruct  ,NULL,NULL };

/// DOC STOP CHUNKS
#define DOC_NUM_STOPS (sizeof(Doc_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Doc_Stops[] =
{
        ID_DOC, ID_CAT,
        ID_DOC, ID_VERS,

        ID_DOC, ID_DOCT,
        ID_DOC, ID_DOCN,
        ID_DOC, ID_CUST,
        ID_DOC, ID_CREA,
        ID_DOC, ID_SALE,
        ID_DOC, ID_PAYD,
        ID_DOC, ID_PAY,
        ID_DOC, ID_PROD,
        ID_DOC, ID_RECE,
        ID_DOC, ID_WYST,

        NULL, NULL,
};
//|

/// WczytajDokument

/*
** wczytuje dokument o podanej nazwie (full path),
** wype�nia odpowienio podan� struktur� DocumentPrint
*/

long WczytajDokument(struct DocumentPrint *dp, char *nazwa)
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
long   Errors = 0;
char   ValidFile = FALSE;
long   Error_Skipped_Products = 0;
struct ProductList prod = {0};
double warto�� = 0;
double Total = 0;


     set(app, MUIA_Application_Sleep, TRUE);

     if(iff = AllocIFF())
       {
       if(iff->iff_Stream = Open(nazwa, MODE_OLDFILE))
           {
           InitIFFasDOS(iff);

           StopChunks(iff, Doc_Stops, DOC_NUM_STOPS);

           if(!OpenIFF(iff, IFFF_READ))
               {

               while(TRUE)
                  {
                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                     break;

                  if(cn = CurrentChunk(iff))
                     {
                     LONG ID = cn->cn_ID;

                     if(!ValidFile)
                        {
                        if((ID == ID_CAT) && (cn->cn_Type == ID_DOC))
                           {
                           ValidFile = TRUE;
                           continue;
                           }

                        break;
                        }

///                    ID_VERS
                     if(ID == ID_VERS)
                        {
                        struct BaseVersion version;

                        if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                           {

                           }
                        else
                           {
                           _RC = IoErr();
                           break;
                           }

                        continue;
                        }
//|
///                    ID_PROD
                     if(ID == ID_PROD)
                        {
                        if(ReadChunkBytes(iff, &prod.pl_prod, sizeof(struct Product)) != cn->cn_Size)
                           Error_Skipped_Products++;
                        else
                           DoMethod(dp->lv_object, _MUIM_List_InsertSingle, &prod, _MUIV_List_Insert_Bottom);

                           warto�� = prod.pl_prod.Zakup + prod.pl_prod.Mar�a;
                           warto�� = prod.pl_prod.Ilo�� * (warto�� - CalcRabat(warto��, prod.pl_prod.Rabat));
                           Total += warto�� + CalcVat(warto��, prod.pl_prod.Vat);

                        continue;
                        }
//|

                     _read(ID_DOCT, &dp->RodzajDokumentu)
                     _read(ID_DOCN, dp->NumerDokumentu)

                     _read(ID_WYST, dp->OsobaWystawiaj�ca)
                     _read(ID_RECE, dp->OsobaOdbieraj�ca)

                     _read_size(ID_CREA, &dp->DataWystawienia, sizeof(struct DateStamp))
                     _read_size(ID_SALE, &dp->DataSprzeda�y  , sizeof(struct DateStamp))
                     _read_size(ID_PAYD, &dp->DataP�atno�ci  , sizeof(struct DateStamp))

                     _read_size(ID_PAY , dp->TypP�atno�ci, PAY_NAME_LEN)

                     if( dp->Odbiorca )
                       {
                       _read_size(ID_CUST, &dp->Odbiorca->kl_klient, sizeof(struct Klient))
                       }
                     }
                  }
               dp->do_zap�aty = Total;
               CloseIFF(iff);
               }

            Close(iff->iff_Stream);
            }

//D(bug("errors: %ld\n", Errors));

//       if(_RC == IFFERR_EOF) Error = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

       FreeIFF(iff);
       }

       set(app, MUIA_Application_Sleep, FALSE);


       return(0);
}

//|

/// WystawDokumentDoParagonu
void WystawDokumentDoParagonu(struct DocumentPrint *dp,
                              char DocNewType,
                              char BaseDocType
                             )
{
struct DrukowanieRequest dr = {0};
struct DocumentPrint dp_tmp;
struct Printer *printer;

    printer = &MatchPrinterForDocument( DocNewType )->pl_printer;

    dr.LiczbaKopii  = printer->def_copies;
    dr.BezParagonu  = TRUE;
    dr.BezDokumentu = FALSE;


    // robimy kopi� i w niej mieszamy
    memcpy(&dp_tmp, dp, sizeof(struct DocumentPrint));


    // ustawi� co trzeba...

    switch( BaseDocType )
       {
       case PARAGON:
           strcpy(dp_tmp.TypP�atno�ci, "Got�wka");
           break;
       }

    memcpy(&dp_tmp.DataWystawienia, wfmh_GetCurrentTime(), sizeof(struct DateStamp));
    dp_tmp.RodzajDokumentu = DocNewType;

    sscanf( wfmh_Date2StrFmt("%m", &dp_tmp.DataWystawienia), "%ld", &dp_tmp.Miesiac);
    sscanf( wfmh_Date2StrFmt("%Y", &dp_tmp.DataWystawienia), "%ld", &dp_tmp.Rok);
    strcpy(dp_tmp.NumerParagonu, dp_tmp.NumerDokumentu);


    // dla kogo dokument?

    dp_tmp.User = dp_tmp.RodzajDokumentu;   // zeby zablokowa� edycj� paru p�l w nag��wku

    // tworzymy nowy dokument
    ZbudujNumerDokumentu(&dp_tmp);

    if(! Nag��wekDokumentu(&dp_tmp) )
       return;

    // czy wraca� do edycji dokumentu?

    dr.RodzajDokumentu = dp_tmp.RodzajDokumentu;

    if(Drukowanie(&dr) == -1)
       return;

//    if(dr.LiczbaKopii > 0)
       {
       int    i;


       set(GA_Prog_Info, MUIA_Gauge_Current, 0);
       set(GA_Prog_Info, MUIA_Gauge_Max, dr.LiczbaKopii + 2);
       set(ProgressWindow, MUIA_Window_Open, TRUE);


       // zapisujemy dokument

       settext(TX_Prog_Info, MSG_PROG_DOC_SAVE);
       ZapiszDokument(&dp_tmp, FALSE);
       set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);


       // druczymy

       settext(TX_Prog_Info, MSG_PROG_DOC_PRINT);

       PrzeliczDokument( &dp_tmp );

       if(dr.LiczbaKopii > 0)
           {
           dp_tmp.Duplikat = FALSE;
           for(i=1; i<=dr.LiczbaKopii; i++)
               {
               dp_tmp.NumerEgzemplarza = i;
               WydrukujDokument(&dp_tmp);
               set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);
               }
           }


       // zwi�kszamy numeracj� dokument�w i zapisujemy

       Zwi�kszNumeracj�(&dp_tmp, &dr);

       settext(TX_Prog_Info, MSG_PROG_DOC_SAVE);
       ZapiszNumeracj�Dokument�w();
       set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);


       set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);
       set(ProgressWindow, MUIA_Window_Open, FALSE);
       }
}
//|

/// Wy�wietl

#define TOTALSTRINGLENGTH   24

long Wy�wietl(char *file, char TypDokumentu)
{
//static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };
struct DateStamp datestamp;
char   running = TRUE;
ULONG  signal  = 0;
char   result  = FALSE;
char   Miesiac[3];
char   Rok[5];
char   TotalString[TOTALSTRINGLENGTH];
char   Zapytaki[] = "???";

struct KlientList odb = {0};
struct DocumentPrint dp = {0};
struct PrinterList *prt;


    _sleep(TRUE);

    if( ! RedagowanieSetup( TypDokumentu ) )
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       _sleep(FALSE);
       return(NULL);
       }


    if(WinOpen(RedagowanieWindow))
       {
       dp.RodzajDokumentu = 0;
       dp.lv_object       = LV_Red_View_Produkty;
       dp.Duplikat        = TRUE;
       dp.Odbiorca        = &odb;

       dp.prt = &( MatchPrinterForDocument( TypDokumentu ) ) -> pl_printer;

       strcpy(dp.NumerDokumentu   , Zapytaki);
       strcpy(dp.TypP�atno�ci     , Zapytaki);
       strcpy(dp.OsobaWystawiaj�ca, Zapytaki);
       strcpy(dp.OsobaOdbieraj�ca , Zapytaki);

       memset(&dp.DataWystawienia, 0, sizeof(struct DateStamp));
       memset(&dp.DataSprzeda�y  , 0, sizeof(struct DateStamp));
       memset(&dp.DataP�atno�ci  , 0, sizeof(struct DateStamp));

       set(LV_Red_View_Produkty, _MUIA_List_ConstructHook, &ProductList_ConstructHook);
       set(LV_Red_View_Produkty, _MUIA_List_DestructHook , &ProductList_DestructHook);

       WczytajDokument(&dp, file);

/*
D(bug("RED-Wczytaj dokument\n"));
D(bug(" Nazwa1: %s\n", dp.Odbiorca->Nazwa1));
D(bug(" Nazwa2: %s\n", dp.Odbiorca->Nazwa2));
*/


//    if(WczytajDokument(file))
       {
//       char Nag��wekDokumentu[40];

       if(strncmp("ProForma", dp.NumerDokumentu, 8) == 0)
         sprintf(dp.Nag��wekDokumentu, "%s PRO FORMA", RodzajeDokument�wTable[dp.RodzajDokumentu]);
       else
         sprintf(dp.Nag��wekDokumentu, "%s %s", RodzajeDokument�wTable[dp.RodzajDokumentu], dp.NumerDokumentu);
       settext(TX_Red_View_Nazwa, dp.Nag��wekDokumentu);

       settext(TX_Red_View_Data_Wyst, wfmh_Date2Str(&dp.DataWystawienia));
       settext(TX_Red_View_Data_P�at, wfmh_Date2Str(&dp.DataP�atno�ci));

       settext(TX_Red_View_Wyst   , dp.OsobaWystawiaj�ca);
       settext(TX_Red_View_Odebral, dp.OsobaOdbieraj�ca);

       // przygotowujemy i wstawiamy warto�� "do zap�aty" w odpowiedni gad�et
       strcpy(TotalString, "\033r");                        /* Na pocz�tek znacznik wyr�wnywania do prawej */
       strcat(TotalString, Price2String(dp.do_zap�aty));    /* potem w�a�ciwy string                       */
       if (strlen(TotalString) > TOTALSTRINGLENGTH - 1)     /* Je�eli znowu mamy hiperinflacj� */
           strcpy(TotalString, "\033c *** WARTO�� ***");    /* to nam si� nie zmie�ci...       */
       set(TX_Red_View_Do_Zaplaty, MUIA_Text_Contents, TotalString);

       // chowamy odbiorce je�li to nie Faktura Vat
       set(GR_Red_View_Odebral, MUIA_ShowMe, (dp.RodzajDokumentu == FAKTURA_VAT));


       switch(dp.RodzajDokumentu)
           {
           case PARAGON:
           case ORDER:
           case FVAT_PROFORMA:
               {
               _disable(BT_Red_Redaguj);

               _enable(BT_Red_PrintDoc);
               DoMethod(RedagowanieWindow, MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application , 2, MUIM_Application_ReturnID, ID_RED_PRINT_DOC);
               }
               break;

           default:
               {
               _enable(BT_Red_Redaguj);
               DoMethod(RedagowanieWindow, MUIM_Notify, MUIA_Window_InputEvent, "f5", MUIV_Notify_Application , 2, MUIM_Application_ReturnID, ID_RED_EDIT_DOC);

               _disable(BT_Red_PrintDoc);
               }
               break;
           }

       // do formatki
       DateStamp( &dp.aktualna_data );

       while(running)
          {
          switch (DoMethod(app, MUIM_Application_Input, &signal))
            {
///           case ID_RED_PRINT:
            case ID_RED_PRINT:         // duplikat albo reedycja
               {
               switch( TypDokumentu )
                 {
///                Dokumenty Bie��ce - wystawiamy duplikaty
                 case DOKUMENTY_BIE��CE:
                   {
                   struct DrukowanieRequest dr = {0};
                   char   BrakOdbierajacego = FALSE;

                   dr.LiczbaKopii = dp.prt->def_copies;

                   dr.BezParagonu = TRUE;
                   dr.BezDokumentu = FALSE;

/*
                   switch(dp.RodzajDokumentu)
                       {
                       case FAKTURA_VAT:
                       case RACHUNEK:
                       case FVAT_KOR:
                       case FVAT_PAR:
                       case RACH_KOR:
                       case RACH_PAR:
                            dr.BezDokumentu = FALSE;
                            dr.BezParagonu  = TRUE;
                            break;

                       case PARAGON:
                            dr.BezDokumentu = FALSE;
                            dr.BezParagonu  = TRUE;
                            break;
                       }
*/



                   // czy wraca� do edycji dokumentu?

                   dr.RodzajDokumentu = dp.RodzajDokumentu;
                   if(Drukowanie(&dr) == -1)
                       break;


                   // coby na wydruku nie straszyly "???"
                   if( stricmp( dp.OsobaOdbieraj�ca, Zapytaki) == 0 )
                       {
                       BrakOdbierajacego = TRUE;
                       strcpy( dp.OsobaOdbieraj�ca , dp.prt->norece_text );
                       }


                   if(dr.LiczbaKopii > 0)
                     {
                     int i;

                     for(i=0;;i++)
                      {
                      char chr;

                      chr = dp.Nag��wekDokumentu[i];

                      if(!chr)
                         break;
                      if(chr == '/')
                         chr = '\\';

                      dp.Nag��wekDokumentuSpool[i] = chr;
                      }

                     set(GA_Prog_Info, MUIA_Gauge_Current, 0);
                     set(GA_Prog_Info, MUIA_Gauge_Max, dr.LiczbaKopii);
                     set(ProgressWindow, MUIA_Window_Open, TRUE);

                     settext(TX_Prog_Info, MSG_PROG_DOC_PRINT);

//                     PrzeliczDokument(&dp);

                     for(i=1; i<=dr.LiczbaKopii; i++)
                       {
                       dp.NumerEgzemplarza = i;
                       WydrukujDokument(&dp);
                       set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);
                       }

                     set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);
                     set(ProgressWindow, MUIA_Window_Open, FALSE);
                     }

                     // Zapytaki nazad do wora
                     if( BrakOdbierajacego )
                       strcpy( dp.OsobaOdbieraj�ca , Zapytaki );

                   }
                   break;
//|
///                Dokumenty Od�o�one - reedycja
               case DOKUMENTY_OD�O�ONE:
                 {
                 Sprzeda�( 0, file );
                 }
                 break;
//|
                 }
               break;
               }
//|
///           case ID_RED_PRINT_DOC:

            // druk dokumentu do paragonu/fv pro forma albo
            // druk zamowienia
            case ID_RED_PRINT_DOC:
                 {
                 switch(dp.RodzajDokumentu)
                   {
                   case PARAGON:
                       {
                       int typ = DocRequester(0, 0);

                       switch(typ)
                           {
                           case 0: typ = FVAT_PAR; break;
                           case 1: typ = RACH_PAR; break;
                           }

                       if(typ != -1)
                           WystawDokumentDoParagonu(&dp, typ, dp.RodzajDokumentu);
                       }
                       break;


                   case ORDER:
                       {
                       int typ = DocRequester(1, 0);

                       switch(typ)
                           {
/*
                           case 0: typ = FAKTURA_VAT; break;
                           case 1: typ = RACHUNEK;    break;
                           case 2: typ = PARAGON;     break;
*/
                           case 0: typ = FAKTURA_VAT; break;
                           case 1: typ = PARAGON;     break;
                           }

                       if(typ != -1)
                           WystawDokumentDoParagonu(&dp, typ, dp.RodzajDokumentu);
                       }
                       break;

                   case FVAT_PROFORMA:
                       {
                       int typ = DocRequester(1, 0);

                       switch(typ)
                           {
/*
                           case 0: typ = FAKTURA_VAT; break;
                           case 1: typ = RACHUNEK;    break;
                           case 2: typ = PARAGON;     break;
*/
                           case 0: typ = FAKTURA_VAT; break;
                           case 1: typ = PARAGON;     break;
                           }

                       if(typ != -1)
                           WystawDokumentDoParagonu(&dp, typ, dp.RodzajDokumentu);
                       }

                       break;
                   }
                 }
               break;

//|
///           case ID_RED_EDIT_DOC:
            case ID_RED_EDIT_DOC:      // edycja naglowka dokumentu
                 {
                 if( Nag��wekDokumentu( &dp ) )
                   {

                   _sleep(TRUE);


                   // refresz displeya

                   if(strncmp("ProForma", dp.NumerDokumentu, 8) == 0)
                     sprintf(dp.Nag��wekDokumentu, "%s PRO FORMA", RodzajeDokument�wTable[dp.RodzajDokumentu]);
                   else
                     sprintf(dp.Nag��wekDokumentu, "%s %s", RodzajeDokument�wTable[dp.RodzajDokumentu], dp.NumerDokumentu);
                   settext(TX_Red_View_Nazwa, dp.Nag��wekDokumentu);

                   settext(TX_Red_View_Data_Wyst, wfmh_Date2Str(&dp.DataWystawienia));
                   settext(TX_Red_View_Data_P�at, wfmh_Date2Str(&dp.DataP�atno�ci));

                   settext(TX_Red_View_Wyst   , dp.OsobaWystawiaj�ca);
                   settext(TX_Red_View_Odebral, dp.OsobaOdbieraj�ca);



                   // usuwamy stary dokument

                   DeleteFile( file );



                   // zapisujemy nowy

                   ZapiszDokument( &dp, FALSE );


                   _sleep(FALSE);

                   }
                 }
                 break;
//|

            case ID_OK:
            case ID_CANCEL:
               running = FALSE;
               break;
            }
          if(running && signal) Wait(signal);
          }


       DoMethod(LV_Red_View_Produkty, _MUIM_List_Clear, NULL);

       DoMethod(RedagowanieWindow, MUIM_KillNotify, MUIA_Window_InputEvent);
       }
       /*
    else
       {
       DisplayBeep(0);
       D(bug("** Nie mog� wczyta� dokumentu\n"));
       }
       */


       }
    else
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
       }

    _detachwin( RedagowanieWindow );

    _sleep(FALSE);

//    return(result);

}
//|

/*
/// Redagowanie

char Redagowanie(char TypDokument�w)
{
//static struct Hook FormatDateHook = { {NULL, NULL}, (VOID *)FormatDate_Func, NULL,NULL };
struct DateStamp datestamp;
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;
char  Miesiac[3];
char  Rok[5];

    _sleep(TRUE);

    if( ! RedagowanieSetup( TypDokument�w ) )
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
       _sleep(FALSE);
       return(NULL);
       }


    if(WinOpen(RedagowanieWindow))
       {
       SetInputEvents(0);

       while(running)
          {
          switch (DoMethod(app, MUIM_Application_Input, &signal))
            {
            struct Filtr sel_cust_filter =
                   {
                   TRUE,
                   "#?",              // Nazwa

                   1,                 // Sprzedawca
                   1,                 // Nabywca
                   1,                 // Kosztowiec
                   0,                 // A
                   0,                 // B
                   0                  // C
                   };

///           case ID_FIND:
            case ID_FIND:
               {
               static char pattern[(FILTR_NAME_LEN*2)+2];
               char   FileNamePattern[35];      // bufor na wzorzec nazwy pliku
               char   CommentPattern[81];       // bufor na wzorzec dla komentarzy
               char   FullPattern[256];         // finalny wzorzec nazwy pliku ze �cie�k�
               char   Error = FALSE;
               char   RodzajDokumentu = getcycle(CY_Red_Typ);
               char   DokumentDirOffset = 0;
               char   SzukajWg = getcycle(CY_Red_Szukaj);
               struct DateStamp user_datestamp = {0};
               int    i;

               // Aktualna data

               switch( TypDokument�w )
                   {
                   case DOKUMENTY_BIE��CE:
                       DokumentDirOffset = 0;
                       break;

                   case DOKUMENTY_OD�O�ONE:
                       DokumentDirOffset = OD_FAKTURA_VAT;
                       break;
                   }


               DateStamp(&datestamp);

               wfmh_Date2StrFmtBuffer( Miesiac, "%m", &datestamp);
               wfmh_Date2StrFmtBuffer( Rok    , "%Y", &datestamp);

//               FormatDateHook.h_Data = Miesiac;
//               FormatDate(MyLocale, "%m", &datestamp, &FormatDateHook);
//               FormatDateHook.h_Data = Rok;
//               FormatDate(MyLocale, "%Y", &datestamp, &FormatDateHook);



               // sprawdzenie string�w i ew. wstawienie '*' je�li puste

               if(*(char *)xget(ST_Red_Numer, MUIA_String_Contents) == '\0') setstring(ST_Red_Numer, "*");
               if(*(char *)xget(ST_Red_Nazwa, MUIA_String_Contents) == '\0') setstring(ST_Red_Nazwa, "*");
               if(*(char *)xget(ST_Red_NIP,   MUIA_String_Contents) == '\0') setstring(ST_Red_NIP  , "*");
               if(*(char *)xget(ST_Red_Regon, MUIA_String_Contents) == '\0') setstring(ST_Red_Regon, "*");
               if(*(char *)xget(ST_Red_Data,  MUIA_String_Contents) == '\0') setstring(ST_Red_Data, wfmh_Date2Str( wfmh_GetCurrentTime() ) );


               // przygotowanie filtr�w

               switch(SzukajWg)
                   {
///                  Numer dokumentu

                   case 0:
                       {
                       char miesiac = (char)getcycle(CY_Red_Numer_Miesiace);

                       sprintf(FileNamePattern, "??????????????%s", (char *)xget(ST_Red_Numer, MUIA_String_Contents));

                       for(i=0; i<sizeof(FileNamePattern); i++)
                         {
                         if(FileNamePattern[i] == 0)
                            break;
                         if(FileNamePattern[i] == '/')
                            FileNamePattern[i] = '-';
                         }

                       switch(miesiac)
                           {
                           case  0:    // aktualny miesiac
                              break;

                           case 13:    // ca�y rok
                              strcpy(Miesiac, "??");
                              break;

                           default: // styczen-grudzien
                              sprintf(Miesiac, "%02ld", miesiac);
                              break;
                           }

                       // Je�eli ROK nic nie ma, to ustawiamy rok aktualny
                       if(*(char *)xget(ST_Red_Numer_Rok, MUIA_String_Contents) == '\0')
                         setstring(ST_Red_Numer_Rok, Rok);
                       else
                         copystr(Rok, ST_Red_Numer_Rok);

                       }
                       break;
//|
///                  Nazwa odbiorcy

                   case 1:
                       {
                       char miesiac = (char)getcycle(CY_Red_Nazwa_Miesiace);

                       switch(miesiac)
                           {
                           case  0:    // aktualny miesiac
                              break;

                           case 13:    // ca�y rok
                              strcpy(Miesiac, "??");
                              break;

                           default: // styczen-grudzien
                              sprintf(Miesiac, "%02ld", miesiac);
                              break;
                           }


                       // Je�eli ROK nic nie ma, to ustawiamy rok aktualny
                       if(*(char *)xget(ST_Red_Nazwa_Rok, MUIA_String_Contents) == '\0')
                         setstring(ST_Red_Nazwa_Rok, Rok);
                       else
                         copystr(Rok, ST_Red_Nazwa_Rok);


                       // sprawdzamy wszystkie dokumenty danego typu
                       sprintf(FileNamePattern, "#?");
                       sprintf(CommentPattern, "?????????????%s#?", (char *)xget(ST_Red_Nazwa, MUIA_String_Contents));
                       }
                       break;
//|
///                  NIP odbiorcy

                   case 2:
                       {
                       char miesiac = (char)getcycle(CY_Red_NIP_Miesiace);

                       sprintf(FileNamePattern, "????%s#?", Str2NIP((char *)xget(ST_Red_NIP, MUIA_String_Contents)));

                       switch(miesiac)
                           {
                           case  0:    // aktualny miesiac
                              break;

                           case 13:    // ca�y rok
                              strcpy(Miesiac, "??");
                              break;

                           default: // styczen-grudzien
                              sprintf(Miesiac, "%02ld", miesiac);
                              break;
                           }

                       // Je�eli ROK nic nie ma, to ustawiamy rok aktualny
                       if(*(char *)xget(ST_Red_NIP_Rok, MUIA_String_Contents) == '\0')
                         setstring(ST_Red_NIP_Rok, Rok);
                       else
                         copystr(Rok, ST_Red_NIP_Rok);

                       }
                       break;
//|
///                  Regon odbiorcy

                   case 3:
                       {
                       char miesiac = (char)getcycle(CY_Red_Regon_Miesiace);

                       switch(miesiac)
                           {
                           case  0:    // aktualny miesiac
                              break;

                           case 13:    // ca�y rok
                              strcpy(Miesiac, "??");
                              break;

                           default: // styczen-grudzien
                              sprintf(Miesiac, "%02ld", miesiac);
                              break;
                           }


                       // Je�eli ROK nic nie ma, to ustawiamy rok aktualny
                       if(*(char *)xget(ST_Red_Regon_Rok, MUIA_String_Contents) == '\0')
                         setstring(ST_Red_Regon_Rok, Rok);
                       else
                         copystr(Rok, ST_Red_Regon_Rok);


                       // sprawdzamy wszystkie dokumenty danego typu
                       sprintf(FileNamePattern, "#?");
                       sprintf(CommentPattern, "????%s#?", (char *)xget(ST_Red_Regon, MUIA_String_Contents));
                       }
                       break;
//|
///                  Data wystawienia

                   case 4:
                       {
                       strcpy(Miesiac, "??");
                       strcpy(Rok, "????");
                       sprintf(FileNamePattern, "#?");
                       }
                       break;
//|
                   }


                // Parsujemy dodatkowe patterny je�li potrzeba...

                switch(SzukajWg)
                   {
//                   case 0:
//                   case 2:

                   // szukanie w.g. nazwy -> pattern dla filecommenta
                   case 1:
                   case 3:
                       {
                       if(ParsePatternNoCase(CommentPattern, pattern, sizeof(pattern)) == -1)
                         {
                         DisplayBeep(0);
                         MUI_Request(app, RedagowanieWindow, 0, TITLE, MSG_OK, MSG_RED_ERR_PATT);
                         Error = TRUE;
                         break;
                         }
                       }
                       break;

                   case 4:
                       {
                       struct DateStamp *datestamp;

                       if(!(datestamp = wfmh_Str2Date((char *)xget(ST_Red_Data, MUIA_String_Contents))))
                         {
                         DisplayBeep(0);
                         MUI_Request(app, RedagowanieWindow, 0, TITLE, MSG_OK, MSG_RED_ERR_DATE);
                         Error = TRUE;
                         break;
                         }

                       memcpy(&user_datestamp, datestamp, sizeof(struct DateStamp));
                       }
                       break;
                   }


                // je�li nie wszystko zosta�o przygotowane
                // do wyszukiwania wracamy do g�ownej p�tli

                if(Error)
                   break;


                // kompletny wzorzec nazw plik�w

                sprintf(FullPattern, "%s/%s/%s/%02lx/%s", DatabasesDir,
                                                          Rok,
                                                          Miesiac,
                                                          RodzajDokumentu + DokumentDirOffset,
                                                          FileNamePattern);


               // Szukamy...

               set(app, MUIA_Application_Sleep, TRUE);
               set(LV_Red_Dokumenty, _MUIA_List_Quiet, TRUE);
               DoMethod(LV_Red_Dokumenty, _MUIM_List_Clear, NULL);

               {
               struct AnchorPath *anchor;

               #define PATH_BUFFER 80

               if(anchor = calloc(1, sizeof(struct AnchorPath)+PATH_BUFFER))
                 {
                 anchor->ap_Strlen = PATH_BUFFER;

                 switch(SzukajWg)
                   {
///                  W/g numer�w dokument�w / numer�w NIP
                   case 0:
                   case 2:
                     {
                     if(MatchFirst(FullPattern, anchor) == 0)
                       {
                       ProcessAnchor(anchor, RodzajDokumentu);

                       while( MatchNext(anchor) == 0 )
                         ProcessAnchor(anchor, RodzajDokumentu);

                       MatchEnd(anchor);
                       }
                     }
                   break;
//|
///                  W/g nazwy odbiorcy / numer�w Regon
                   case 1:
                   case 3:
                     {
                     if(MatchFirst(FullPattern, anchor) == 0)
                       {
                       if(MatchPatternNoCase(pattern, anchor->ap_Info.fib_Comment))
                           ProcessAnchor(anchor, RodzajDokumentu);

                       while(MatchNext(anchor) == 0)
                         if(MatchPatternNoCase(pattern, anchor->ap_Info.fib_Comment))
                           ProcessAnchor(anchor, RodzajDokumentu);

                       MatchEnd(anchor);
                       }
                     }
                   break;
//|
///                  W/g daty wystawienia
                   case 4:
                     {
                     struct DateStamp doc_datestamp = {0};

                     if(MatchFirst(FullPattern, anchor) == 0)
                       {
                       char MatchDateMode = (char)getcycle(CY_Red_DataAccuracy);
                       char tmp[2+4+1] = "0x";
                       long result;

                       memcpy(tmp, anchor->ap_Info.fib_FileName, 4);
                       sscanf(tmp, "%lx", &doc_datestamp.ds_Days);

                       result = CompareDates(&user_datestamp, &doc_datestamp);

                       switch(MatchDateMode)
                           {
                           // ==
                           case 0:
                               if(result == 0)
                                  ProcessAnchor(anchor, RodzajDokumentu);
                               break;

                           // >
                           case 1:
                               if(result < 0)
                                  ProcessAnchor(anchor, RodzajDokumentu);
                               break;

                           // <
                           case 2:
                               if(result > 0)
                                  ProcessAnchor(anchor, RodzajDokumentu);
                               break;
                           }


                       while(MatchNext(anchor) == 0)
                           {
                           memcpy(tmp, anchor->ap_Info.fib_FileName, 4);
                           sscanf(tmp, "%lx", &doc_datestamp.ds_Days);

                           result = CompareDates(&user_datestamp, &doc_datestamp);

                           switch(MatchDateMode)
                               {
                               // ==
                               case 0:
                                   if(result == 0)
                                      ProcessAnchor(anchor, RodzajDokumentu);
                                   break;

                               // >
                               case 1:
                                   if(result < 0)
                                      ProcessAnchor(anchor, RodzajDokumentu);
                                   break;

                               // <
                               case 2:
                                   if(result > 0)
                                      ProcessAnchor(anchor, RodzajDokumentu);
                                   break;
                               }
                           }

                       MatchEnd(anchor);
                       }
                     }
                     break;
//|
                   }

                 free(anchor);
                 }
               else
                 {
                 DisplayBeep(0);
                 MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY );
                 }
               }
               set(RedagowanieWindow, MUIA_Window_ActiveObject, LV_Red_Dokumenty);

               set(LV_Red_Dokumenty, _MUIA_List_Quiet, FALSE);
               set(app, MUIA_Application_Sleep, FALSE);

               }
               break;
//|
///           case ID_RED_VIEW:
            case ID_RED_VIEW:
               {
               if(xget(LV_Red_Dokumenty, _MUIA_List_Active) != _MUIV_List_Active_Off)
                  {
                  struct DocumentScan *doc;

                  DoMethod(LV_Red_Dokumenty, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &doc);

                  if(doc)
                    {
                    ClearInputEvents(0);
                    Wy�wietl(doc->FilePath, TypDokument�w);
                    SetInputEvents(0);
                    }
                  }
               }
               break;
//|
///           case ID_DELETE
            case ID_DELETE:
               {
               if(xget(LV_Red_Dokumenty, _MUIA_List_Active) != _MUIV_List_Active_Off)
                  {
                  DoMethod(LV_Red_Dokumenty, _MUIM_List_Remove, _MUIV_List_Remove_Active);
                  }
               }
               break;
//|
///           case ID_SORT:
            case ID_SORT:
               {
               LONG  entries = xget(LV_Red_Dokumenty, MUIA_List_Entries);
               int   order = 1;

//               if(entries)
                   {
                   _sleep(TRUE);

                   switch( TypDokument�w )
                       {
                       case DOKUMENTY_BIE��CE:
                           order = SortRequester(SORT_DOCSCAN, xget(LV_Red_Dokumenty, MUIA_DocumentList_Order));
                           break;
                       case DOKUMENTY_OD�O�ONE:
                           order = SortRequester(SORT_DOCSCAN_OD�O�ONE, xget(LV_Red_Dokumenty, MUIA_DocumentList_Order));
                           break;
                       }

                   if(order != -1L)
                      {
                      switch( TypDokument�w )
                        {
                        case DOKUMENTY_OD�O�ONE:   // przeskakujemy NUMER, bo go nie ma w dokumentach od�o�onych
                           {
                           if( order >=4 )
                               order ++;
                           break;
                           }
                        }

                      set(LV_Red_Dokumenty, MUIA_DocumentList_Order, order);
                      DoMethod(LV_Red_Dokumenty, MUIM_List_Sort);
//                      Uporz�dkujZakupy();
                      }

                   _sleep(FALSE);
                   }
/*
               else
                   {
                   DisplayBeep(0);
                   }
*/
               }
               break;

//|

///           case ID_RED_NAZWAWYBIERZ:

            case ID_RED_NAZWAWYBIERZ:
               {
               struct KlientList *klientList;

               klientList = KontrahentSelector(&sel_cust_filter, NULL, 0);

//                   klient = KontrahentSelector(&sel_cust_filter, F_CUSTED_NIP_REQUIRED );

               if( klientList )
                   {
                   struct Klient *klient = &klientList->kl_klient;

                   setstring(ST_Red_Nazwa, klient->Nazwa1);
//                   settext(TX_Nag_NIP    , NIP2Str(dp.Odbiorca->NIP, dp.Odbiorca->Sp��kaCywilna));
//                   settext(TX_Nag_Regon  , dp.Odbiorca->Regon);
                   }
               }
               break;

//|
///           case ID_RED_NIPWYBIERZ:

            case ID_RED_NIPWYBIERZ:
               {
               struct KlientList *klientList;

               klientList = KontrahentSelector(&sel_cust_filter, NULL, F_CUSTED_NIP_REQUIRED );

               if( klientList )
                   {
                   struct Klient *klient = &klientList->kl_klient;

                   setstring(ST_Red_NIP   , NIP2Str(klient->NIP, klient->Sp��kaCywilna));
//                   settext(TX_Nag_Regon  , dp.Odbiorca->Regon);
                   }
               }
               break;

//|
///           case ID_RED_REGONWYBIERZ:

            case ID_RED_REGONWYBIERZ:
               {
               struct KlientList *klientList;

               klientList = KontrahentSelector(&sel_cust_filter, NULL, 0 );

               if( klientList )
                   {
                   struct Klient *klient = &klientList->kl_klient;

                   setstring(ST_Red_Regon, klient->Regon);
                   }
               }
               break;

//|

            case ID_OK:
            case ID_CANCEL:
               running = FALSE;
               break;

            }
          if(running && signal) Wait(signal);
          }

//       ClearInputEvents(0);
       }
    else
       {
       DisplayBeep(0);
       MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
       }

    _detachwin( RedagowanieWindow );

    _sleep(FALSE);

    return(result);

}
//|
*/

/// Redagowanie

void RedagowanieCallback( struct DocumentScan *ds, char TypDokument�w )
{
    Wy�wietl( ds->FilePath, TypDokument�w );
}

char Redagowanie(char TypDokument�w)
{
struct DocumentScan ds = {0};
char   result = TRUE;


    _sleep(TRUE);

    DokumentSelector( &ds, TypDokument�w, FALSE, (void *)RedagowanieCallback );
/*
    while( DokumentSelector( &ds, TypDokument�w, FALSE ) != FALSE )
       {
       Wy�wietl( ds.FilePath, TypDokument�w );
       }
*/
    _sleep(FALSE);

    return(result);
}
//|

