
/*
$Id: golem_structs.h,v 1.1 2003/01/01 20:40:53 carl-os Exp $.
*/


/*     Default lengths                */


#define SQL_ID_LEN       (4 + 2 + 2)

// identyfikator: NNNNTTXXXXXX
//
//  NNN    - unikalny numer Golema
//  TT     - typ danych
//  XXXXX  - kolejny numer danego typu danych w tej kopii golema (hex)


#define DATE_LEN          11           // d�ugo�� stringa na wpisanie daty


#define LOGIN_ID_LEN      12
#define LOGIN_PASSWD_LEN  15
#define USER_NAME_LEN     30
#define USER_PASSWD_LEN   12

#define GROUP_NAME_LEN    32           // d�ugo�� nazwy grupy

#define PROD_NAME_LEN     50           // nazwa produktu
#define PROD_SWW_LEN      20           // sww produktu
#define PROD_JEDN_LEN      6           // d�ugo�� nazwy jedn. miary
#define PROD_INDEX_LEN    35           // d�ugo�� indeksu

#define FILTR_NAME_LEN        128      // d�ugo�� patternu do filtr�w
#define FILTR_CUST_NAME_LEN   128      // d�ugo�� patternu do filtra klient�w


#define CUST_NAME_LEN   50             // dane klienta
#define CUST_ADRES_LEN  50
#define CUST_KOD_LEN     (9 + 1)
#define CUST_MIASTO_LEN 30
#define CUST_NIP_LEN    (10 + 1)
#define CUST_REGON_LEN   (9 + 1)
#define CUST_BANK_LEN   80
#define CUST_KONTO_LEN  60
#define CUST_NOTE_LEN  80
/*
#define CUST_BANK_LEN   30
#define CUST_KONTO_LEN  30
*/

#define CUST_TEL_LEN    40
#define CUST_FAX_LEN    20
#define CUST_EMAIL_LEN  40

#define ODB_NAME_LEN  CUST_NAME_LEN   // nazwa osobu upowa�nionej do odbioru dokumentu


#define PRINTER_DEV_LEN  40           // d�ugo�� nazwy sterownika drukarki

#define PAY_NAME_LEN (11 + 1)         // d�ugo�� nazwy jednostki p�atno�ci

#define ORIGINAL_COPY_LEN   20        // d�ugo�� tekst�w ORYGINA� / KOPIA / DUPLIKAT
#define NORECE_LEN          40        // d�ugo�� tekst�w NO RECE
#define COMMAND_STRING_LEN 128        // d�ugo�� polece� (DOS/REXX etc).

#define DOC_NUMER_LEN 20

#define DOC_TEXT_LEN  130             // d�ugo�� tekstu u�ytkownika drukowanego na ko�cu dokumentu


#define MAX_UNITS 20                  // max liczba jednostek miar
#define MAX_PAYS  10                  // max liczba rodzaj�w p�atno�ci

#define PRT_STRING_LEN  35            // d�ugo�� kodu kontrolnego drukarki
#define PRT_PATH_LEN   256            // default path dla drukarki FILE

#define PARAGON_LEN 16                // d�ugo�� stringa z numerem paragonu

#define POSNET_LEN       40           // d�ugo�� tekstu do wy�wietlania na displeju posnetowym
#define POSNET_PRINT_LEN 40           // d�ugo�� nadruk�w dodatkowy na paragonie

#define URL_LEN 256                   // dlugosc URLa do strony klienta


/*     STRUCTS                        */

struct BaseVersion
{
  UWORD Version;
  UWORD Revision;
};

// serial handle - do posnetu

struct SerialHandle
{
	struct MsgPort *port;
	struct IOExtSer *ior;
};

// dane dot. u�ytkownik�w

enum{MUIV_UserLevel_Admin,
	 MUIV_UserLevel_SprzedawcaMagazynier,
	 MUIV_UserLevel_Sprzedawca,
	 MUIV_UserLevel_Magazynier,
	};


struct User
{
	char Nazwa[USER_NAME_LEN];
	long Has�o_CRC;
	char UserLevel;
};

struct UserList
{
	struct MinNode node;
	struct User    user;
	char   Deleted : 1,
		   LoggedIn: 1;
	char   sqlID[SQL_ID_LEN];
};

// jednostki miar

struct Jednostka
{
	char Nazwa[PROD_JEDN_LEN];
	char Ca�kowita;
};

struct JednostkaList
{
	struct MinNode  ul_node;
	struct Jednostka     ul_jedn;
	ULONG  Count;
	char   Deleted  : 1,
		   Imported : 1,           // czy jednostka zostala utworzona podczas wczytywania / importu magazynu?
		   _unused  : 6;
	char   sqlID[SQL_ID_LEN];
};


// dane produktu

struct Product
{
	char   Nazwa[PROD_NAME_LEN];
	char   SWW[PROD_SWW_LEN];
	char   Index[PROD_INDEX_LEN];

	double Ilo��;
	double Zakup;                  // cena netto zakupu towaru
	double Mar�a;                  // kwota mar�y
	char   Vat;                    // MUIV_Vat_xxxx
	struct Jednostka Jedn;

	char   Grupa;                  /* numer grupy do ktorej produkt nalezy */
	short  MinEn       : 1,        /* czy podano stan min? */
		   MaxEn       : 1,        /* czy podano stan max? */
		   GwarancjaEn : 1,        /* czy na produkt jest gwarancja? */
		   Min_Reach   : 1,        /* stan minimum osi�gni�to */
		   Min_Ack     : 1,        /* u�ytkownik przyj�� do wiadomo�ci przekroczenie stanu minimum */
		   Typ         : 2,        /* typ elementu TYP_TOWAR, TYP_US�UGA, TYP_FREEHAND */
//           FreeHand    : 1,        /* czy produkt/us�uga by�a dodana z r�ki. Potrzebne przy wycofywaniu pozycji kontynuowanego dokument */
		   fake        : 9;

	double Rabat;                  /* u�ywany przy wystawianiu faktur/rachunk�w        */

	long   Numer;                  // unikalny numer produktu - nie u�ywane p�ki co ;-)

// stany magazynowe

// UWAGA: StanMin jest wykorzystywany przy zapisywaniu dokument�w
//        od�o�onych, do przechowywania warto�ci RealIlo��!!!

	double StanMin;                        // stan minimalny
	double StanMax;                        // stan maksymalny

// gwarancja

	ULONG  Gwarancja;       /* okres gwarancyjny (np 360 dni) */

//  inne

	double Locked;          // liczba element�w zablokowanych przez np. od�o�one faktury

};

// dane dot produkt�w na dokumencie (list)

/*
struct FakProductList
{
	struct MinNode pl_node;
	struct Product pl_prod;
	char   Deleted  : 1,
		   Expanded : 1,               /* jesli true, to jest to rozszerzona struktura (np. FakProdList) */
		   _unused  : 6;

};
*/

struct ProductList
{
	struct MinNode pl_node;
	struct Product pl_prod;
	char   Deleted  : 1,
		   Expanded : 1,        /* jesli true, to jest to rozszerzona struktura (np. FakProdList) */
		   _unused  : 6;

	double RealIlo��;                  /* u�ywane j/w przy anulowaniu wpisu na fakturze    */
									   /* -> potrzebne, bo mo�na sprzeda� wi�cej ni� by�o  */
									   /* w magazynie, a wycofa� trzeba zgodnie ze stanem */

	char   gr_Typ     : 2,             /* typ grupy z jakiej pochodzi� element (prod/us�ugi/etc) */
		   _unused2   : 6;

};


struct Filtr
{
	char Aktywny;
	char Nazwa[FILTR_NAME_LEN];

	char Sprzedawca            : 1,
		 Nabywca               : 1,
		 Kosztowiec            : 1,
		 A                     : 1,
		 B                     : 1,
		 C                     : 1;

};





// grupa produkt�w

enum{ TYP_TOWAR = 0, TYP_USLUGA, TYP_FREEHAND };

struct Grupa
{

	struct Jednostka Jedn;
	char   Nazwa[GROUP_NAME_LEN];
	char   Vat;
	char   ID;

	double def_Min;
	double def_Max;
	long   def_gw_dni;              /* okres gwarancyjny (np 360 dni) */

	char   def_MinOn :  1,
		   def_MaxOn :  1,
		   def_GwOn  :  1,
		   Typ       :  2,          // typ grupy (co zawiera (prod/us�ugi/etc))
		   _unused   :  3;


};


struct GrupaList
{
	struct MinNode  gm_node;
	struct Grupa    gm_grupa;
	struct List     produkty;
	long   Ilo��;                   // liczba produkt�w w grupie
	char   Deleted;
	char   sqlID[SQL_ID_LEN];
};



// dane dot. kontrahenta

struct Klient
{

	short Sp��kaCywilna         : 1,   // sp��ka cywilna - NIP 3-2-2-3 inni 3-3-2-2
		  Upowa�nienieVAT       : 1,   // czy dodawa� "Upowa�nienie VAT" do listy odbiorc�w
		  Upowa�nienieBezLimitu : 1,   // czy upowa�nienie VAT jest bez daty wa�no�ci?
		  DopisywanieDozwolone  : 1,   // czy mo�na dopisywa� odbiorc�w podczas sprzeda�y?
		  FIFO                  : 1,   // czy odbiorcy s� usuwani zgodnie z FIFO, je�li dopisano nowego?

		  Sprzedawca            : 1,
		  Nabywca               : 1,
		  Kosztowiec            : 1,
		  A                     : 1,
		  B                     : 1,
		  C                     : 1,

		  Separator             : 1;   // czy ten rekord ma byc separatorem?

	char  Nazwa1[CUST_NAME_LEN];
	char  Nazwa2[CUST_NAME_LEN];
	char  Ulica[CUST_ADRES_LEN];
	char  Kod[CUST_KOD_LEN];
	char  Miasto[CUST_MIASTO_LEN];
	char  NIP[CUST_NIP_LEN];
	char  Regon[CUST_REGON_LEN];
	char  Bank[CUST_BANK_LEN];
	char  Konto[CUST_KONTO_LEN];

	char  Telefon[CUST_TEL_LEN];
	char  Fax[CUST_FAX_LEN];
	char  Email[CUST_EMAIL_LEN];

	long  UpVat_ExpireDate;            // ds_Days - data utraty wa�no�ci przez UpVat
	char  Odb1[CUST_NAME_LEN];
	char  Odb2[CUST_NAME_LEN];
	char  Odb3[CUST_NAME_LEN];
	char  Odb4[CUST_NAME_LEN];

	double def_Rabat;                  /* domy�lny rabat danego klienta - u�ywany przy wystawianiu faktur/rachunk�w        */
};


struct KlientExt
{
	char *Komentarz;
	char def_Payment[PAY_NAME_LEN];
	char *Uwagi;
};

struct KlientList
{
	struct MinNode   kl_node;
	struct Klient    kl_klient;
	struct KlientExt kl_klientExt;
//    struct List     kl_odbiorcy;
	char   Deleted    : 1,
		   Tymczasowy : 1,
		   _unused    : 6;
	long   numer;
//    char   sqlID[SQL_ID_LEN];

};




struct Odbiorca
{

	char Nazwa[ODB_NAME_LEN];

};


struct OdbiorcaList
{
	struct MinNode  ol_node;
	struct Klient   ol_odbiorca;
	char   Deleted;
	char   sqlID[SQL_ID_LEN];
};



// ustawienia programu

struct Settings
{
	char def_dot;

	char  def_miara[PROD_JEDN_LEN];
	char  def_vat;

	char  def_miasto[CUST_MIASTO_LEN];
	short def_expire_offset;
	char  def_upvat;
	char  def_unlimited;
	char  def_addenabled;
	char  def_fifo;

	char  def_sprzedawca;
	char  def_nabywca;
	char  def_kosztowiec;
	char  def_a;
	char  def_b;
	char  def_c;
	char  clone_filter;          // czy nowo dodany klient ma by� tego samego typu co aktualne ustaienia w filtrze wy�wietlania?

	char  reszta_aktywna;        // czy otwiera� ResztaWindow przy p�atno�ci got�wk�
	char  paragon_fvat;
	char  paragon_rach;
	char  paragon_par;
	char  paragon_inne;

	char  sta�a_mar�a_procentowa;  // przy edycji cen produktu w magazynie

	char posnet_active;
	char serial_device[PRINTER_DEV_LEN];
	char serial_unit;
	char ptu_22;                            // stawka A dla drukarki fiskalnej
	char ptu_17;
	char ptu_7;
	char ptu_0;
	char ptu_3;
	char ptu_zw;

	char posnet_open_drawer;
	char posnet_idle;                          // posnet idle - display action
	char posnet_idle_text1[POSNET_LEN];        // tekst do wy�wietlania jak nic si� nie dzieje
	char posnet_idle_text2[POSNET_LEN];
	char posnet_nadruki;                       // czy u�ywa� nadruk�w na paragonie?
	char posnet_nadruk_1[POSNET_PRINT_LEN];
	char posnet_nadruk_2[POSNET_PRINT_LEN];
	char posnet_nadruk_3[POSNET_PRINT_LEN];


	struct Klient wystawca;

	char   order_fvat;
	char   type_fvat;
	double fvat_min;
	char   sug_rach;
	char   order_fvat_kor;
	char   type_fvat_kor;
	char   order_fvat_par;
	char   type_fvat_par;

	char   order_rach;
	char   type_rach;
	char   order_rach_kor;
	char   type_rach_kor;
	char   order_rach_par;
	char   type_rach_par;

	char   order_order;
	char   type_order;

/*
	char doc_print;
	char original_text[ORIGINAL_COPY_LEN];
	char copy_text[ORIGINAL_COPY_LEN];
	char dupe_print;
	char dupe_text[ORIGINAL_COPY_LEN];
	char doc_sww;                              // czy zawsze drukowa� SWW
	char norece_text[NORECE_LEN];
	char formfeed;                             // czy wysuwa� stron� po wydruku
*/

	char upvat_text[CUST_NAME_LEN];

	char text_1[DOC_TEXT_LEN];
	char text_2[DOC_TEXT_LEN];
	char text_3[DOC_TEXT_LEN];
	char text_4[DOC_TEXT_LEN];
	char text_5[DOC_TEXT_LEN];
	char text_6[DOC_TEXT_LEN];
	char text_7[DOC_TEXT_LEN];
	char text_8[DOC_TEXT_LEN];
	char text_9[DOC_TEXT_LEN];
	char text_0[DOC_TEXT_LEN];

	char com_call;
	char com_sale[COMMAND_STRING_LEN];


	// mapowanie drukarek

	short map_fvat;
	short map_fvat_proforma;
	short map_paragon;
	short map_zamowienie;
	short map_cennik_detal;
	short map_cennik_hurt;
	short map_spis;
	short map_stany;
	short map_przelew;
};


//Rachunek.golem              3057 ----rwed 28-Gru-99    22:44:18
//RachunekDoParagonu.golem    3083 ----rwed 28-Gru-99    22:44:18


// identyfikatory do generowania uniqueID
enum{ ID_GRUPA = 0,
	  ID_PRODUKT,
	  ID_MIARA,
	  ID_PLATNOSC,
	  ID_KLIENT,

	  ID_COUNT
	};


// ustawienia SQL

#define SQL_HOST_NAME_LEN 64

struct SettingsSQL
{
	char  connect_type;                  /* 0 - mUSD, 1 - TCP/IP */
	char  host_name[SQL_HOST_NAME_LEN];
	int   host_port;
};


// sposoby p�atno�ci

struct P�atno��
{
	long Zw�oka;
	char Nazwa[PAY_NAME_LEN];
	char Unused  : 7,
		 Got�wka : 1;

};

struct P�atno��List
{
	struct MinNode   pl_node;
	struct P�atno��  pl_p�atno��;
	ULONG  Count;
	char   Deleted;
//    char   sqlID[SQL_ID_LEN];
};



// informacje o numerach dokument�w

struct Documents
{
	long   numer_fvat;                     // faktura vat
	struct DateStamp   data_fvat;          //   data wystawienia ostatniego dokumentu
	long   numer_fvat_kor;                 // fvat - korekta
	struct DateStamp   data_fvat_kor;
	long   numer_fvat_par;                 // fvat do paragonu
	struct DateStamp   data_fvat_par;

	long   numer_rach;
	struct DateStamp   data_rach;
	long   numer_rach_kor;
	struct DateStamp   data_rach_kor;
	long   numer_rach_par;
	struct DateStamp   data_rach_par;

	long   numer_paragon;
	struct DateStamp   data_paragon;

	long   numer_order;                    // zam�wienie (dla nas, od klienta)
	struct DateStamp   data_order;

	long   numer_proforma;                 // pro forma (numer wewnetrzny)
	struct DateStamp   data_proforma;

	long   numer_odreczna;                 // sprzedaz odreczna
	struct DateStamp   data_odreczna;

};


// informacje systemowe (np o dacie ostatniego uruchomienia)

struct SystemInfo
{
	struct DateStamp  last_start;     // data ostatniego uruchomienia
	struct DateStamp  last_operation; // data ostatniej operacji (sprzeda�/zakup/korekta)
};



// informacje o przeszukiwanych dokumentach

struct DocumentScan
{
    char Nazwa[CUST_NAME_LEN];        // nazwa klienta
    char NIP[CUST_NIP_LEN+4];
    char Regon[CUST_REGON_LEN];
    char NumerDokumentu[DOC_NUMER_LEN];

    struct DateStamp DataWystawienia; // data wystawienia

    char FilePath[60];
    char RodzajDokumentu;             // typ dokumentu

    char   TotalFound;                // czy wartosc dokumentu zostala znaleziona w pliku (poprzednie dokuemnty nie maja tego chunka)
    double TotalNetto;                // kwota netto dokumentu
    double TotalVat;                  // kwota podatku VAT
};


#define STATUS_CLOSED_B 0
#define STATUS_CLOSED   1<<STATUS_CLOSED_B


struct Status
{
	char rok;            // numer roku kt�rego plik dotyczy

	char miesiac[12];    // dane dotycz�ce poszczeg�lnych miesi�cy
};

/*
// informacje dot. os�b odbieraj�cych dokument

struct Odbieraj�cy
{
	char Nazwa[USER_NAME_LEN];
	char Specialne : 1,              // Odbiorca specjalny (b�dzie na pocz�tku listy)
		 Expires   : 1;              // Wa�no�� danego odbiorcy jest czasowa (np. upowa�nienie VAT)
	struct DateStamp DataWa�no�ci;   // Data wa�no�ci odbiorcy
};

struct Odbieraj�cyList
{
	struct MinNode     node;
	struct Odbieraj�cy odbieraj�cy;
	char   Deleted;
};
*/

// Informacje dotycz�ce dokumentu do wydrukowania/zapisania

struct DocumentPrint
{
	struct DateStamp DataWystawienia;
	struct DateStamp DataSprzeda�y;
	struct DateStamp DataP�atno�ci;

	char   Nag��wekDokumentu[64];
	char   NumerDokumentu[16];
	char   NumerParagonu[PARAGON_LEN];   // przy dokumentach do paragon�w

	char   Nag��wekDokumentuSpool[32];   // Nazwa u�ywana do plik�w Spoolera (np. VAT2\32_#)
	char   NumerDokumentuSpool[16];      // numer dokumentu do nazwy spoolfajla

	char   RodzajDokumentu;              // FAKTURA_VAT, RACH_KOR...
	char   Duplikat;                     // czy w�a�nie drukujemy duplikat dokumentu
	int    NumerEgzemplarza;             // 1,2,3,....
	int    Miesiac;                      // miesiac wystawienia (czy czego tam...)
	int    Rok;                          // rok (j/w)

	char   OsobaWystawiaj�ca[USER_NAME_LEN];
	struct KlientList *Odbiorca;             // dane odbiorcy
	char   OsobaOdbieraj�ca[USER_NAME_LEN];

	char   TypP�atno�ci[PAY_NAME_LEN];   // nazwa rodzaju p�atno�ci.

	Object *lv_object;                   // LV kt�ry zawiera poszczeg�lne elementy
	int    lv_index;                     // numer aktualnie odczytywanej poz. z (listy-1)

	short  NeedRepeat   : 1,             // czy dana linia zawiera komendy powtarzalne (np. @{lp}
		   Drukowa�SWW  : 1;
	double netto_22, vat_22;             // warto�ci globalne dla dokumentu
	double netto_17, vat_17;
	double netto_7,  vat_7;
	double netto_3,  vat_3;
	double netto_0,  vat_0;
	double netto_zw, vat_zw;

	double zakup_netto_total;            // suma wszystkich kwot zakupowych

	struct Printer *prt;

	char   reszta_aktywna;               // czy requester Reszta by� u�ywany?
	double do_zap�aty;                   // ��cznie do zap�aty
	double got�wka_wp�acona;             // kwota wp�acona przez klienta
	double reszta;                       // reszta do wydania


	long   aktualna_linia;               // kt�r� lini� na stronie w�a�nie drukujemy?
	long   liczba_linii;                 // liczba linii tekstu, kt�re wydrukowali�my (potrzebne do podzia�u na strony)
	long   aktualna_strona;              // numer strony, kt�r� w�a�nie drukujemy (liczone od zera (1sza strona))
	long   aktualna_linia_total;         // ktora to linii tekstu liczona od poczatku dokumentu
	long   liczba_linii_total;           // ile linii tekstu lacznie ma dokument
	long   MultiLineCnt;     		     // ktory kawalek nazwy (z podzialu wieloliniowego) bedziem teraz drukkowac - , zeby jego nazwa zmiescila sie w kilku liniach o danej szerokosci pola (Lamanie na wiele linii)
	char   DoMultiLine;                  // BOOL czy kawalkowac trzeba jeszcze dana nazwe na wiele linii czy nie

	long   User;                         // pole na przekazywanie r��nych �mieci do funkcji kt�re mieszaj� z DP,
										 // a kt�rym zb�dne specjalne pola

	char   UseCustomPrinter;             // czy uzywamy innej drukarki niz tak ustawiona w nastawach przez usera? BOOL
	short  PrinterID;                    // jesli tak, to ID tejze

	struct DateStamp aktualna_data;

	struct PrzelewPrint *pp;   // adres struktury z danymi do dokumentu 'Przelew'

};


// dane potrzebne do wystawienia dokumentu 'przelew'
struct PrzelewPrint
{
	struct Klient Dluznik;
	struct Klient Wierzyciel;

	double Kwota;
	struct DateStamp DataPlatnosci;
	char   Tytulem[512];
};


// Struktury dla parsera komend do templejt�w plik�w wydruk�w

enum{FONT_NORMAL = 0,
	 FONT_WIDE,
	 FONT_CONDENSED,
	};

#define ATTR_NORMAL 0<<0
#define ATTR_BOLD   1<<0
#define ATTR_ITAL   1<<1
#define ATTR_UNDER  1<<2


struct CmdArray
{
	char  *Command;
	ULONG (*Function)();
};


struct Command
{
	char FontType;             // FONT_WIDE, FONT_NORMAL, FONT_CONDENSED
	char Attr;                 // Atrybuty wydruku (BOLD...)
	char CenterOn;             // czy centrowanie w��czone

	char command[50];          // aktualnie przetwarzane polecenie (np. "akt_dzien")
	char width_str[10];        // z�dana szeroko�� pola (string => np.. "-30")
	int  width;                // abs(width_str) tylko jako INT nie string
	char zeros[10];            // je�li "0" to np %03ld. Je�li NULL to %3ld

	char *in;                  // adres bufora zawieraj�cego przetwarzan� lini�
	char *out;                 // adres bufora zaw. lini� po przetworzeniu
	int  in_index;             // offset kt�ry znak z IN przetwarzamy
	int  out_index;            // offset j/w ale do OUT

	struct CmdArray *CmdArray; // adres tablicy rozpoznawanych polece�
	char  *cmd_out;            // adres bufora w kt�rym wykonywane polecenie powinno zapisa� wynik swojego dzia�ania
	char  silent_command;      // polecenie powinno zostac wykonane, lecz nie powinno produkowa� zadnego rezultatu (np. polecenia =xxxx).

	struct DocumentPrint *dp;  // adres struktury DP dla dokumentu

};



// Struktury dla requester�w tekstowych/numerycznych...

#define STR_REQ_MAXLEN 128

#define STR_REQ_NOZERO  1<<0      // je�li ustawione, pusty string == CANCEL

struct StringRequest
{
	char *WindowTitle;            // nazwa okna requestera
	char String[STR_REQ_MAXLEN];  // tekst -> init oraz p��niej wynik
	char Flags;                   // dodatkowe flagi steruj�ce requesterem
	char MaxLen;                  // maks. dopuszczalna d�ugo�� tekstu
};


// Struktura dla requestera drukowania

struct DrukowanieRequest
{
// in
	char BezDokumentu;             // je�li TRUE, gad�et od kopii dokumentu jest zablokowany
	char BezParagonu;              // j/w ale dot. paragonu
	char RodzajDokumentu;          // typ dokumentu jaki drukujemy

// out
	char LiczbaKopii;              // {.SG}   przy S = domy�lna liczba kopii
	char DrukujParagon;            // {.SG}   przy S je�li == 0, gad�et jest zablokowany
};


// Struktura dla requestera dla przy raporcie okresowym fiskalnym

struct PosnetRaportRequest
{

	char data_od[20];              // data pocz�tkowa (powinien by� format %y;%m;%d)
	char data_do[20];              // data ko�cowa

};                           



struct MagScan
{
	struct GrupaList   *grupa;     // grupa do ktorej nalezy wybrany produkt
	struct ProductList *prod;      // wybrany przez uzytkownka produkt
};



// Zakupy

/*

#define STR_ZAK_DOW�D_LEN  35
#define STR_ZAK_OPIS_LEN  100
#define STR_ZAK_UWAGI_LEN  30

struct Zakup
{
	long   LP;                                         // 1
	struct DateStamp DataZdarzenia;                    // 2
	char   NumerDowoduKsi�gowego[STR_ZAK_DOW�D_LEN];   // 3
	struct Klient Kontrahent;                          // 4
	char   OpisZdarzenia[STR_ZAK_OPIS_LEN];            // 6
// Przych�d
	double Warto��SprzedanychTowar�w;                  // 7
	double Pozosta�ePrzychody;                         // 8

	double ZakupTowar�wHandlowych;                     // 10
	double KosztyUboczneZakupu;                        // 11

// Wydatki
	double KosztyReprezentacjiIReklamy;                // 12
	double Wynagrodzenia;                              // 13
	double Pozosta�eWydatki;                           // 14

	double P16;                                        // 16
	char   Uwagi[STR_ZAK_UWAGI_LEN];                   // 17

};

struct ZakupList
{
	struct MinNode za_node;
	struct Zakup   za_zakup;
	ULONG  Count;
	char   Deleted;
};

*/



#define STR_ZAK_DOW�D_LEN  40
#define STR_ZAK_OPIS_LEN  100
#define STR_ZAK_UWAGI_LEN  50

struct Zakup
{
	long   P1;                      // LP
	char   P2[STR_ZAK_DOW�D_LEN];   // numer dowodu ksiegowego
	struct DateStamp P3;            // data otrzymania
	struct DateStamp P4;            // data wystwienia

	struct Klient Sprzedawca;       // P5-P7

	double P9;                      // zakupy za kt�re nie przys�uguje odliczenie

	double P10;                     // wart. netto - 22 - do odliczenia
//    double P11;                     // vat - 22
	double P12;                     // wart. netto - 7 - do odliczenia
//    double P13;                     // vat - 7

	double P14;                     // wart. netto - 22 - zakupy s�u��ce sprz. opodatkowanej i zwolnionej
//    double P15;                     // vat - 22
	double P16;                     // wart. netto - 7 - do odliczenia
//    double P17;                     // vat - 7

	char   Opis[STR_ZAK_OPIS_LEN];  // opis zdarzenia gosp.

};

struct ZakupList
{
	struct MinNode za_node;
	struct Zakup   za_zakup;
	ULONG  Count;
	char   Deleted;
//    char   sqlID[SQL_ID_LEN];
};







/***************************************************
**                                                **
**  BANKI                                         **
**                                                **
***************************************************/

#define BANKC_NAME_LEN    (127 + 1)
#define BANKC_CODE_LEN    (  5 + 1)
#define BANKC_SHORT_LEN   ( 12 + 1)
#define BANKC_COMMENT_LEN ( 79 + 1)

// Informacje ogolne o danym typie bankow

struct BankInfo
{
	char Numer[BANKC_CODE_LEN];
	char Nazwa[BANKC_NAME_LEN];
	char Skr�cona[BANKC_SHORT_LEN];
	char Info[BANKC_COMMENT_LEN];
	char Flags;
};

// Informacje szczegolowe o oddziale banku

#define BANK_NUMBER_LEN  ( 15 + 1)
#define BANK_DESC_LEN    ( 79 + 1)
#define BANK_SHORT_LEN   ( 12 + 1)
#define BANK_ADRES_LEN   ( 49 + 1)
#define BANK_KOD_LEN     (  9 + 1)
#define BANK_MIASTO_LEN  ( 39 + 1)

struct Bank
{
	char Numer[BANK_NUMBER_LEN];
	char Opis[BANK_DESC_LEN];
	char Skr�cona[BANK_SHORT_LEN];
	char Ulica[BANK_ADRES_LEN];
	char Kod[BANK_KOD_LEN];
	char Miasto[BANK_MIASTO_LEN];

	char Flags;
};



/***************************************************
**                                                **
**   FORMATKI                                     **
**                                                **
***************************************************/

struct Formatka
{
	char ID[4 + 1];            // id typu dokumentu (jednoczesnie nazwa katalogu)
	char Kod;                  // kod liczbowy typu (kopatybility psia mac! ;(
	char *KodLV;               // skroty typow do wyswietlnia w roznych listach
	char *FullName;            // pelna nazwa opisowa typu
	char *SpoolName;           // nazwa uzywana do okreslania plikow w spoolerze
	char *Formatka;            // sciezna do foratki i jej nazwa (cza sprintfa robic albo z "" albo "-SWW" itp!)
};



#define DEFAULT_PRINTER 0

enum{ PRT_TYPE_PRINTER = 0,
	  PRT_TYPE_FILE,
	};


// dane potrzebne do uzycia danej drukarki
struct PrinterRuntime
{
	struct  MsgPort  *Port;
	struct  IOStdReq *IO;
	char    CLUT[256];           // character lookup table ;-)  u�ywana przy konwersji
};

struct FileRuntime
{
	BPTR   FileHandle;
	char   OutputFile[256];
	char   CLUT[256];           // character lookup table ;-)  u�ywana przy konwersji
};

// definicja drukarki
struct Printer
{

	APTR runtime;         // te dane nie sa zapisywane z definicjami drukarek
	char SilentMode;      // BOOL -> jesli TRUE, to fizycznie nic nie ma byc wydrukowane/zapisane

// =====


	short ID;                              // unikalny numer drukarki
										   // 0 - drukarka domyslna!!

	char  Type;                               // Typ drukarki PRT_TYPE_#?
	char  Name[ PRINTER_DEV_LEN ];

	char  printer_device[ PRINTER_DEV_LEN ];
	char  printer_unit;
	short printer_width_wide;                 // szeroko�� strony w znakach przy czcionce poszerzone
	short printer_width_normal;               // j/w przy normalnej
	short printer_width_cond;                 // j/w przy condensed
	char  printer_charset;
	char  def_copies;                         // domy�lna liczba egz. dokument�w


	char normal[PRT_STRING_LEN];              // kody drukarki

	char bold_on[PRT_STRING_LEN];
	char bold_off[PRT_STRING_LEN];
	char italics_on[PRT_STRING_LEN];
	char italics_off[PRT_STRING_LEN];
	char underline_on[PRT_STRING_LEN];
	char underline_off[PRT_STRING_LEN];

	char cond_on[PRT_STRING_LEN];
	char cond_off[PRT_STRING_LEN];

	char  page_mode;                           // dzielimy wydruki na strony?
	short page_height;                         // wysoko�� strony (w liniach)
	char  skip_1st_page_footer;                // czy drukowa� footer na dokumentach 1stronicowych

	char doc_print;
	char original_text[ORIGINAL_COPY_LEN];
	char copy_text[ORIGINAL_COPY_LEN];
	char dupe_print;
	char dupe_text[ORIGINAL_COPY_LEN];
	char doc_sww;                              // czy zawsze drukowa� SWW
	char norece_text[NORECE_LEN];
	char formfeed;                             // czy wysuwa� stron� po wydruku


	char default_path[ PRT_PATH_LEN ];         //default path dla drukarki FILE

};



// exec list node ze wszsytkimi zdefiniowanymi drukarkami
struct PrinterList
{
	struct MinNode        pl_node;
	struct Printer        pl_printer;    // definicja drukarki

	char   Deleted;
};


/****************************************
**
**   WYDRUKI Z MAGAZYNU
**
****************************************/

struct MagPrintRequester
{

	int Mode;         // co drukowac? (wynik z Cycle Gadzetu)  Detal/Hurt/Spis

	// output
	char  CSVFile[256];
	short PrtID;      // ID drukarki na jaka popychac (jesli na drukarke)

	// spis z natury


	// cennik detaliczny

	char detal_CoDrukowa�;   // czy drukowac caly magazyn czy tylko akt. grupe
	char detal_ProdCzyUslugi;// czy drukowac produkty, uslugi czy obie grupy
	char detal_LimitAktywny; // czy uzywamy limit�w
	double detal_Limit;

	// cennik hurtowy

	char hurt_CoDrukowa�;    // czy drukowac caly magazyn czy tylko akt. grupe
	char hurt_ProdCzyUslugi; // czy drukowac produkty, uslugi czy obie grupy
	char hurt_LimitAktywny;  // czy uzywamy limit�w
	double hurt_Limit;
	double hurt_Rabat;

	// stany magazynowe

	char stan_CoDrukowa�;    // czy drukowac caly magazyn czy tylko akt. grupe
	char stan_ProdCzyUslugi; // czy drukowac produkty, uslugi czy obie grupy
	char stan_LimitAktywny;  // czy uzywamy limit�w
	double stan_Limit;

	// spis z natury

	char spis_CoDrukowa�;    // czy drukowac caly magazyn czy tylko akt. grupe
	char spis_ProdCzyUslugi; // czy drukowac produkty, uslugi czy obie grupy
	char spis_LimitAktywny;  // czy uzywamy limit�w
	double spis_Limit;
	
	// wydruk limitow
	
	char minmax_CoDrukowa�;
	char minmax_ProdCzyUslugi;
	char minmax_TypLimitu;	//�przekroczenie ktorych limitow sprawdzamy


};





// Inspektor result list

struct Result
{
	char nazwa[128];
	char req_ver[30];
	char avail_ver[30];
	char fatal;
};

//

