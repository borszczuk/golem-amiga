
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

#include "Golem.h"

static long _akt_mc;
static long _akt_rok;
static long _mc;
static long _rok;

/// Sprawd�CzyKoniecMiesi�caRoku
char Sprawd�CzyKoniecMiesi�caRoku(void)
{

/*
** sprawdza czy nie nalezy zamknac miesiaca rozrachunkwego
** zwraca FALSE jesli nic sie nie zmienilo, 2 jesli trzeba
** zamknac rok rachunkowy, 1 jesli tylko ostatni miesiac
*/

char _data_str[32];

char result = FALSE;

    // sprawdzamy czy sko�czy� si� miesiac

    // aktualna data
    strcpy(_data_str, wfmh_Date2StrFmt( "%m %Y", wfmh_GetCurrentTime() ) );
    sscanf(_data_str, "%ld %ld", &_akt_mc, &_akt_rok);

    // data ostatnio sprawdzana
    strcpy(_data_str, wfmh_Date2StrFmt( "%m %Y", &sys.last_operation ) );
    sscanf(_data_str, "%ld %ld", &_mc, &_rok);


    // sprawdzamy czy rok ten sam

//    printf("akt rok: %ld   last: %ld\n", _akt_rok, _rok);
//    printf("akt mc : %ld   last: %ld\n", _akt_mc , _mc);

    if(_akt_rok > _rok)
       {
       // rok albo par� min��o

       MUI_Request(app, NULL, 0, TITLE, MSG_OK, MSG_KR_INFO);
       result = 2;
       }
    else
       {
       if(_akt_mc > _mc)
         {
         // miesi�c albo par� min��o

         MUI_Request(app, NULL, 0, TITLE, MSG_OK, MSG_KM_INFO);
         result = 1;
         }            
       }

    return(result);

}

//|
/// ZamknijMiesiac

char ZamknijMiesiac(void)
{

    if( settings.type_fvat     == 0 ) docs.numer_fvat = 1;
    if( settings.type_fvat_kor == 0 ) docs.numer_fvat_kor = 1;
    if( settings.type_fvat_par == 0 ) docs.numer_fvat_par = 1;

/*
        // Numeracja 2

        if( settings.type_rach     == 0 ) docs.numer_rach = 1;
        if( settings.type_rach_kor == 0 ) docs.numer_rach_kor = 1;
        if( settings.type_rach_par == 0 ) docs.numer_rach_par = 1;
*/

        // Numeracja 3

    if( settings.type_order == 0 ) docs.numer_order = 1;

    docs.numer_proforma = 1;
//    docs.numer_odreczna = 1;

    ZapiszNumeracj�Dokument�w();

    return(0);
}

//|
/// ZamknijRok

char ZamknijRok(void)
{

    // Numeracja 1

    docs.numer_fvat = 1;
    docs.numer_fvat_kor = 1;
    docs.numer_fvat_par = 1;

    // Numeracja 3

    docs.numer_order = 1;

    docs.numer_paragon = 1;

    docs.numer_proforma = 1;
    docs.numer_odreczna = 1;


    ZapiszNumeracj�Dokument�w();

    return(0);
}

//|

