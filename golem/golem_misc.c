
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
$Id: golem_misc.c,v 1.1 2003/01/01 20:40:54 carl-os Exp $.
*/

#include "golem.h"


/* Miscelaneous routines */

/*
//    Child, ST_Set_Own_Nazwa_1 = NewObject(CL_BString->mcc_Class, NULL, TAG_DONE),
*/


// Useful GUI macros and functions

/// Misc

/// xget
LONG xget(Object *obj, ULONG attribute)
{
LONG x;

	get(obj, attribute, &x);
	return(x);
}
//|
/// setnumstring
void setnumstring(Object *obj, long val)
{
static char buf[20];

	sprintf(buf, "%ld", val);
	set(obj, MUIA_String_Contents, buf);

}
//|

//|
/// GUI

#define HighliteLabel(num)         TextObject, MUIA_Weight, 1, MUIA_Text_Contents, num, MUIA_Text_PreParse, "\0333", End
#define HighliteLabelKey(num, key) TextObject, MUIA_Weight, 1, MUIA_Text_Contents, num, MUIA_Text_PreParse, "\0333", MUIA_Text_HiChar, (char)key[0], End
#define LabelKey(num, key)         TextObject, MUIA_Weight, 1, MUIA_Text_PreParse, "\033r", MUIA_Text_Contents, num, MUIA_Text_HiChar, (char)key[0], End

Object *MakeLabel2   (char *num)
{
// Etykieta

	  return(MUI_MakeObject(MUIO_Label    ,num, MUIO_Label_DoubleFrame));
}



Object *EmptyLabel(void)
{
// Puste miejsce

	  return(MUI_MakeObject(MUIO_Label,NULL, MUIO_Label_DoubleFrame));
}



Object *PopButton2(int img, char *key)
{
// PopButton z szortkatem

	   Object *obj = MUI_MakeObject(MUIO_PopButton, img);

	   if(obj)
		   {
		   set(obj, MUIA_CycleChain, TRUE);
		   set(obj, MUIA_ControlChar, wfmh_ParseHotkey(key));
		   }
	   return(obj);
}



Object *MakeCheck(char *num)
{
// Czekmark z szortkatem

		Object *obj = MUI_MakeObject(MUIO_Checkmark, num);
		if(obj)
		   set(obj, MUIA_CycleChain, TRUE);
		return(obj);
}


Object *MakeCycle(char **array, char *num)
{
// Sajkl z szortkatem

		Object *obj = MUI_MakeObject(MUIO_Cycle ,num, array);
		if(obj)
		   set(obj, MUIA_CycleChain, TRUE);

		return(obj);
}

Object *MakeCycleWeight(char **array, char *num, long weight)
{
// Sajkl z szortkatem

		Object *obj = MUI_MakeObject(MUIO_Cycle ,num, array);
		if(obj)
		   {
		   set(obj, MUIA_CycleChain, TRUE);
		   set(obj, MUIA_Weight, weight);
		   }

		return(obj);
}

Object *DoString(int maxlen, char *num, ULONG NoInput)
{
//ULONG InputTag = MUIA_Textinput_NoInput;
ULONG InputTag = MUIA_BetterString_NoInput;
ULONG Frame    = MUIV_Frame_Text;
Object *obj;


	if( NoInput == FALSE )
	   {
	   InputTag = TAG_IGNORE;
	   Frame    = MUIV_Frame_String;
	   }


	obj = StringObject, MUIA_Frame, Frame,
						MUIA_ControlChar, wfmh_ParseHotkey(num),
						MUIA_String_MaxLen, maxlen,
						MUIA_CycleChain, TRUE,
						MUIA_String_AdvanceOnCR, TRUE,
//                        MUIA_Textinput_DefaultPopup, TRUE,
						InputTag, NoInput,
						End;

	return( obj );
}

Object *MakeString(int maxlen, char *num)
{

	return( DoString( maxlen, num, FALSE ));

}
Object *MakeStringSH( int maxlen, char *str, char *help )
{
Object *obj = MakeString( maxlen, str );
	if( obj )
		set( obj, MUIA_ShortHelp, help);

	return( obj );
}

Object *MakeStringSecret(int maxlen, char *num)
{
/*
Object *obj = DoString( maxlen, num, FALSE );
*/

	Object *obj = MUI_NewObject(MUIC_String,
					   MUIA_Frame, MUIV_Frame_String,
					   MUIA_ControlChar, wfmh_ParseHotkey(num),
					   MUIA_String_MaxLen, maxlen,
					   MUIA_CycleChain, TRUE,
					   MUIA_String_AdvanceOnCR, TRUE,
					   MUIA_String_Secret, TRUE,

					   TAG_DONE );

	return( obj );
}

Object *DoCashString( int maxlen, char *num, ULONG NoInput )
{
static char accept[] = "?0123456789";
Object *obj;

	   accept[0] = settings.def_dot;

	   obj = DoString( maxlen, num, NoInput );

	   if(obj)
		   {
		   set( obj, MUIA_String_Accept, accept );
		   }

		return(obj);
}


Object *MakeCashString(int maxlen, char *num)
{
	   return( DoCashString( maxlen, num, FALSE ) );
}


Object *MakeCashStringWeight(int maxlen, char *num, int weight)
{
Object *obj;

	obj = DoCashString( maxlen, num, FALSE );

	if(obj)
	   {
	   set(obj, MUIA_Weight, weight);
	   }

	return(obj);
}




Object *MakeSignedCashString(int maxlen, char *num)
{
static char accept[] = "?-0123456789";
Object *obj;

	accept[0] = settings.def_dot;

	obj = DoString( maxlen, num, FALSE );

	if( obj )
	   set( obj, MUIA_String_Accept, accept );

	return(obj);
}



Object *MakeAmountString(int maxlen, char *num)
{

	return( MakeSignedCashString(maxlen, num) );

}

Object *MakeAmountStringWeight(int maxlen, char *num, int weight)
{
Object *obj;

	obj = MakeSignedCashString(maxlen, num);

	if(obj)
	   set(obj, MUIA_Weight, weight);

	return(obj);

}


Object *DoStringAccept(int maxlen, char *num, char *accept, ULONG NoInput )
{
Object *obj = DoString( maxlen, num, NoInput );

	if( obj )
	   set( obj, MUIA_String_Accept, accept );

	return(obj);
}
Object *MakeStringAccept(int maxlen, char *num, char *accept)
{
	return( DoStringAccept( maxlen, num, accept, FALSE ));
}
Object *MakeStringAcceptWeight(int maxlen, char *num, char *accept, int weight)
{
Object *obj;

	obj = DoStringAccept( maxlen, num, accept, FALSE );
	if(obj)
	   set(obj, MUIA_Weight, weight);

	return( obj );
}



Object *DoStringReject(int maxlen, char *num, char *reject, ULONG NoInput)
{
Object *obj = DoString( maxlen, num, NoInput );

	if( obj )
	   set( obj, MUIA_String_Reject, reject );

	return(obj);
}
Object *MakeStringReject(int maxlen, char *num, char *reject)
{
	return( DoStringReject(maxlen, num, reject, FALSE ) );
}


Object *DoNumericString(int maxlen, char *num, ULONG NoInput)
{
// string numeryczny

	return( DoStringAccept( maxlen, num, "0123456789", NoInput ) );
}

Object *MakeNumericString(int maxlen, char *num)
{
	return( DoNumericString( maxlen, num, FALSE ) );
}

Object *MakeNumericStringSH( int maxlen, char *str, char *help )
{
Object *obj = MakeNumericString( maxlen, str );

	if( obj )
			set( obj, MUIA_ShortHelp, help );

	return( obj );
}


Object *MakeStringKey(int maxlen, char *key)
{
Object *obj = MUI_MakeObject(MUIO_String, NULL, maxlen);

		if(obj)
		  {
		  SetAttrs(obj,
				   MUIA_CycleChain, TRUE,
				   MUIA_String_AdvanceOnCR, TRUE,
				   MUIA_ControlChar, key[0],
//                   MUIA_Textinput_DefaultPopup, TRUE,
				   TAG_DONE);
		  }

		return(obj);
}


Object *TextButton(char *num)
{
// buton textowy

		Object *obj = MUI_MakeObject(MUIO_Button,num);

		if(obj) set(obj, MUIA_CycleChain, TRUE);
		return(obj);
}

Object *TextButtonWeight(char *num, int weight)
{
// buton textowy z podan� wag�

		Object *obj = MUI_MakeObject(MUIO_Button,num);

		if(obj)
		  {
		  set(obj, MUIA_CycleChain, TRUE);
		  set(obj, MUIA_Weight    , weight);
		  }
		return(obj);
}

Object *TextButton2(char *num, char *num2)
{
		Object *obj = MUI_NewObject(MUIC_Text,
								ButtonFrame,
								MUIA_Text_PreParse, "\33c",
								MUIA_Text_Contents, num,
								MUIA_CycleChain   , TRUE,
								MUIA_InputMode    , MUIV_InputMode_RelVerify,
								MUIA_Background   , MUII_ButtonBack,
								MUIA_Text_HiChar  , (ULONG)(num2)[0],
								MUIA_ControlChar  , (ULONG)(num2)[0],
								TAG_DONE
								);
		return(obj);
}

Object *SizedButton(char *num)
{
	Object *obj = VGroup,
				  ButtonFrame,
				  MUIA_ControlChar  , wfmh_ParseHotkey(num),
				  MUIA_Background   , MUII_ButtonBack,
				  MUIA_InputMode    , MUIV_InputMode_RelVerify,
				  MUIA_CycleChain   , TRUE,
				  Child, HVSpace,
				  Child, HGroup,
						 Child, HVSpace,
						 Child, MakeLabel2(num),
						 Child, HVSpace,
						 End,

				  Child, HVSpace,
				  End;

	return(obj);
}



Object *ToggleButton(char *num)
{
// buton textowy typu toggle

/*
		Object *obj = MUI_NewObject(MUIC_Text,
								ButtonFrame,
								MUIA_Text_PreParse, "\33c",
								MUIA_Text_Contents, num,
								MUIA_CycleChain   , TRUE,
								MUIA_InputMode    , MUIV_InputMode_Toggle,
								MUIA_Background   , MUII_ButtonBack,
								MUIA_Text_HiChar  , (ULONG)wfmh_ParseHotkey(num),
								MUIA_ControlChar  , (ULONG)wfmh_ParseHotkey(num),
								TAG_DONE
								);
*/

		Object *obj = MUI_MakeObject(MUIO_Button,num);

		if(obj)
		   {
		   set(obj, MUIA_CycleChain, TRUE);
		   set(obj, MUIA_InputMode, MUIV_InputMode_Toggle);
		   }


		return(obj);
}


//|

/*
/// Debug support

void DumpDP(struct DocumentPrint *dp)
{

	printf("------[ DumpDP ]-------\n");
//    struct DateStamp DataWystawienia;
//    struct DateStamp DataSprzeda�y;
//    struct DateStamp DataP�atno�ci;

	printf("Nag�owekDokumentu: '%s'\n", dp->Nag��wekDokumentu);
	printf("NumerDokumentu   : '%s'\n", dp->NumerDokumentu);
	printf("NumerParagonu    : '%s'\n", dp->NumerParagonu);

	printf("Nag�owekDokumentuSpool: '%s'\n", dp->Nag��wekDokumentuSpool);
	printf("NumerDokumentuSpool   : '%s'\n", dp->NumerDokumentuSpool);

	printf("RodzajDokumentu   : '%c'\n", dp->RodzajDokumentu);
	printf("NumerEgzemplarza  : '%d'\n", dp->NumerEgzemplarza);

//    char   Duplikat;                     // czy w�a�nie drukujemy duplikat dokumentu
//    int    Miesiac;                      // miesiac wystawienia (czy czego tam...)
//    int    Rok;                          // rok (j/w)

	printf("OsobaWystawiaj�ca: '%s'\n", dp->OsobaWystawiaj�ca);

	printf("Odb-Nazwa1       : '%s'\n", dp->Odbiorca->Nazwa1);
	printf("Odb-Nazwa2       : '%s'\n", dp->Odbiorca->Nazwa2);
	printf("Odb-Ulica        : '%s'\n", dp->Odbiorca->Ulica);


//    struct Odbieraj�cy OsobaOdbieraj�ca;

	printf("TypP�atno�ci     : '%s'\n", dp->TypP�atno�ci);

	printf("Lv_Object        : $%08lx\n", dp->lv_object);

//    char   NeedRepeat  : 1,              // czy dana linia zawiera komendy powtarzalne (np. @{lp}
//           Drukowa�SWW : 1;
//    double netto_22, vat_22;             // warto�ci globalne dla dokumentu
//    double netto_17, vat_17;
//    double netto_7,  vat_7;
//    double netto_0,  vat_0;
//    double netto_zw, vat_zw;

	printf("prt              : $%08lx\n", dp->prt);

}
//|
*/
