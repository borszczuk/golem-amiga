/*
*/


/*** Include stuff ***/


#ifndef GOLEM_UNITLIST_MCC_H
#define GOLEM_UNITLIST_MCC_H

#ifndef LIBRARIES_MUI_H
#include "libraries/mui.h"
#endif


/*** MUI Defines ***/

#define MUIC_Golem_UnitList  "Golem_UnitList.mcc"
#define MUIC_Golem_UnitListP "Golem_UnitList.mcp"
#define Golem_UnitListObject MUI_NewObject(MUIC_Golem_UnitList


/*** Methods ***/


/*** Method structs ***/


/*** Special method values ***/


/*** Special method flags ***/


/*** Attributes ***/


/*** Special attribute values ***/


/*** Structures, Flags & Values ***/


/*** Configs ***/


#endif /* GOLEM_UNITLIST_MCC_H */



