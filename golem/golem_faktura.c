
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
** $Id: golem_faktura.c,v 1.1 2003/01/01 20:40:53 carl-os Exp $.
*/

#include "Golem.h"

// daty dot. dokumentu


//long miesiac = 0;
//long rok     = 0;


//#ifdef BETA
//char beta_watermark_4[] = BETA_WATERMARK;
//#endif


struct Documents  docs =
{
        1,                     // numer_fvat
        {0},                     // data_fvat
        1,                     // numer_fvat_kor (ekta)
        {0},
        1,                     // numer_fvat_par
        {0},

        1,                     // numer_rach
        {0},                     // data_rach
        1,                     // numer_rach_kor
        {0},
        1,                     // numer_rach_par
        {0},

        1,                     // numer_paragon
        {0},

        1,                     // numer_order
        {0},

        1,                     // faktura pro forma
        {0},

        1,                     // lp sprzedazy odrecznej
        {0}

};



struct Filtr fak_filter =            /* Faktura VAT                 */
{
        FALSE,
        "#?"                             // Nazwa
};


struct Filtr fak_cust_filter =       /* Faktura VAT - klienci       */
{
        FALSE,
        "#?"                             // Nazwa
};

static char WybierzPage  = 0;        // kt�ra strona string�w Wybierz jest aktualna


/// SPRZEDA� STRINGS


#define MSG_ERR_NO_STOCK     "\033cMagazyn jest pusty!\nWci�nij \"Kontynuuj\" je�li chcesz\nwystawi� dokument \"z r�ki\""
#define MSG_ERR_NO_PAYS      "Brak definicji rodzaj�w p�atno�ci!"

#define MSG_FAK_LAB_ODBIORCA "Kontrahent"
#define  SH_FAK_LAB_ODBIORCA "Informacje dotycz�ce odbiorcy dokumentu"
#define MSG_FAK_ODBIORCA     "F6 - N_ag��wek"
#define  SH_FAK_ODBIORCA     "Edycja nag��wka wystawianego dokumentu\n(dane odbiorcy, data sprzeda�y, p�atno�ci...)"
#define  SH_FAK_LIST         "Lista sprzedanych towar�w/us�ug" 

#define MSG_FAK_PRICE     "\0338Netto"
#define MSG_FAK_AMOUNT    "_Ilo��"
#define MSG_FAK_DISCOUNT  "Ra_bat %"

#define MSG_FAK_ACCEPT    "A_kceptuj�"
#define  SH_FAK_ACCEPT    "Dodaje aktualnie wybrany towar/us�ug� do dokumentu"
#define MSG_FAK_Z_REKI    "F1 - Z r�ki"
#define  SH_FAK_Z_REKI    "Pozwala na r�czne dodanie do dokumentu towaru/us�ugi\nkt�ry nie figuruje w magazynie"
#define MSG_FAK_NEXT      "Na_st�pny"
#define  SH_FAK_NEXT      NULL
#define MSG_FAK_USUN      "DEL - _Usu�"
#define  SH_FAK_USUN      "Wycofuje/usuwa zaznaczony element\nz aktualnie wystawianego dokumentu"
#define MSG_FAK_EDIT      "F5 - _Edytuj"
#define  SH_FAK_EDIT      "Pozwala na edycje zaznaczonej pozycji na dokumencie\n(dot. jedynie us�ug i element�w wprowadzonych \"z r�ki\")"


#define MSG_FAK_GONEXT     "_>>"
#define  SH_FAK_GONEXT     "Wyszukuje nast�pny element pasuj�cy\ndo podanego w \"Znajd�\" wzroca wyszukiwania"
#define MSG_FAK_FIND       "F7 - _Znajd�"
#define  SH_FAK_FIND       "Pozwala przeszukiwa� magazyn po podstawie\npodanych kryter��w (cz��ciowa nazwa,\nnumer indeksu etc.)"
#define MSG_FAK_FILTER     "_F"
#define  SH_FAK_FILTER     "Aktywacja filtra wy�wietlania\ndla aktualnej listy magazynowej"
#define MSG_FAK_FILTER_ALL "_*"
#define  SH_FAK_FILTER_ALL "Wy��cza aktywny filtr wy�wietlania"


#define MSG_FAK_TOTAL "\0338Do zap�aty z�"


//#define MSG_FAK_ADD_CUST_GAD "*_Tak|_Nie"
//#define MSG_FAK_ADD_CUST     "Klient \0338\"%s\"\0332\nnie figuruje w bazie danych programu!\nCzy chcesz go teraz dopisa�?"

#define MSG_FAK_WRONG_PROD_GAD "*_Ok"
#define MSG_FAK_WRONG_PROD     "\033cProdukt o podanej nazwie nie\nfiguruje w bazie magazynu!\nSprawd� jego nazw� b�d� wybierz\nprodukt z listy magazynowej."

#define MSG_FAK_WRONG_AMOUNT_1_GAD "K_ontynuuj|_Wszystko|*Ponie_chaj"
#define MSG_FAK_WRONG_AMOUNT_1     "\033cChcesz sprzeda� wi�cej ni� jest\nw magazynie (aktualny stan \0338%s\0332)!\n\nWybierz \"Kontynuuj\", je�li mimo wszystko\nchcesz sprzeda� wpisan� ilo�� towaru (\0338%s\0332),\n\"Wszystko\" aby sprzeda� pozosta�� w magazynie\nilo�� towaru (\0338%s\0332), b�d� \"Poniechaj\"\naby wr�ci� do edycji dokumentu"
#define MSG_FAK_WRONG_AMOUNT_2_GAD "K_ontynuuj|*Ponie_chaj"
#define MSG_FAK_WRONG_AMOUNT_2     "\033cChcesz sprzeda� wi�cej ni� jest\nw magazynie (aktualny stan \0338%s\0332)!\n\nWybierz \"Kontynuuj\", je�li mimo wszystko\nchcesz sprzeda� wpisan� ilo�� towaru (\0338%s\0332),\nb�d� \"Poniechaj\" aby wr�ci� do edycji dokumentu"

#define MSG_FAK_WRONG_DISCOUNT_GAD "*_Ok"
#define MSG_FAK_WRONG_DISCOUNT     "\033cRabat musi by� z przedzia�u od 0 do 100!"

#define MSG_FAK_REMOVE_GAD     "*_Tak|_Nie"
#define MSG_FAK_REMOVE         "\033cNa pewno chcesz wycofa�\n\0338\"%s\"\0332\nz aktualnego zam�wienia?"

#define MSG_FAK_NO_CUST        "\033cAby wystawi� dokument potrzebne\ns� pe�ne dane jego odbiorcy!"
#define MSG_FAK_NO_CUST_FREEZE "\033cMusisz najpierw okre�li� odbiorc� dokumentu!"
//#define MSG_FAK_NO_IDENT_GAD "Wystaw _rachunek upr.|*Wr�� do _edycji"
#define MSG_FAK_NO_IDENT       "\033cAby wystawi� dokument potrzebny jest\nprawid�owy numer \0338NIP\0332 b�d� \0338REGON\0332 odbiorcy!"
#define MSG_FAK_WRONG_IDENT    "Niepoprawny numer \0338%s\0332!"
#define MSG_FAK_WRONG_NIP      "NIP"
#define MSG_FAK_WRONG_REGON    "Regon"
#define MSG_FAK_NO_PAR         "Brak numeru paragonu,\nkt�rego dokument dotyczy!"
#define MSG_FAK_NO_NAME        "\033cAby wystawi� dokument potrzebne\ns� pe�ne dane osobowe odbiorcy!"
#define MSG_FAK_ERR_PAY        "Nie okre�li�e� rodzaju p�atno�ci!"

#define MSG_FAK_FILL_CUST_GAD  "*_Tak|_Nie"
#define MSG_FAK_FILL_CUST      "\033cCzy chcesz u�y� pozosta�ych danych\nklienta \0338\"%s\"\0332\nw dalszych polach dokumentu?"

#define MSG_FAK_CANCEL_REQ     "\033cNa pewno chcesz zako�czy� edycj�\ndokumentu i uniewa�ni� wszystkie\nwprowadzone do� zmiany?"
#define MSG_FAK_CANCEL_REQ_GAD "_Tak, zako�cz|*_Nie"

#define MSG_FAK_FREEZE_REQ     "Na pewno chcesz od�o�y� wystawianie\ntego dokumentu na p��niej?\n\nWa�ne: dokument od�o�ony blokuje umieszczone\nna nim produkty magazynowe w podanych na dokumencie\nilo�ciach, zdejmuj�c je tymczasowo ze stanu\nmagazynowego!\n\nDokumenty od�o�one dost�pne s� poprzez\nfunkcje w menu \"R��ne\"."
#define MSG_FAK_FREEZE_REQ_GAD "_Tak, od���|*_Nie"


#define MSG_FAK_ERR_DATA_WYST  "\033cData wystawienia dokumentu\nnie mo�e by� wcze�niejsza\nni� data sprzeda�y towar�w!"
#define MSG_FAK_ERR_DATA_PLAT  "\033cTermin p�atno�ci nie mo�e by� wcze�niejszy\nni� data wystawienia dokumentu!"

#define MSG_FAK_ERR_LOW        "\033cMinimalna dopuszczalna warto�� faktury VAT\nnie mo�e by� ni�sza ni� \0338%s z�\0332\n(obecna warto�� dokumentu: %s z�)"
#define MSG_FAK_ERR_LOW_GAD    "Wystaw _rachunek upr.|Wystaw _faktur�|*Wr�� do _edycji"

#define MSG_FAK_CANT_FREEZE_REQ "Ten typ dokumentu nie mo�e by� od�o�ony"

#define MSG_FAK_OK             "F10 - Zatwierd� dokument"
#define  SH_FAK_OK             "Ko�czy wystawianie aktualnego dokumentu,\nakceptuj�c wszystkie wprowadzone zmiany"
#define MSG_FAK_OD���          "O_d���"
#define  SH_FAK_OD���          "Wstrzymuje wystawianie dokumentu, odk�adaj�c\ngo w aktualnym stanie do specjalnej teczki\n\"Dokumenty od�o�one\" (patrz menu \"R��ne\")\n\nWa�ne: dokument od�o�ony blokuje umieszczone\nna nim produkty magazynowe w podanych na dokumencie\nilo�ciach, zdejmuj�c je tymczasowo ze stanu\nmagazynowego!"
#define MSG_FAK_CANCEL         "ESC - Przerwij wystawianie"
#define  SH_FAK_CANCEL         "Przerywa wystawianie dokumentu,\nanuluj�c wszytkie wprowadzone\npodczas wystawiania zmiany"

//|
/// Sprzeda�Setup

static Object
           *FakturaWindow,                /* FAKTURA VAT             */

           *ST_Fak_Nazwa_1,
           *ST_Fak_Nazwa_2,
           *ST_Fak_Ulica,
           *ST_Fak_Kod,
           *ST_Fak_Miasto,
           *ST_Fak_NIP,
           *ST_Fak_Regon,
           *TX_Fak_Data_Wyst,
           *ST_Fak_Data_Sprz,
           *CY_Fak_Typ_P�atno�ci,
           *ST_Fak_Data_P�at,


           *TX_Fak_Odbiorca,
           *BT_Fak_Odbiorca,

           *TX_Fak_Grupy,
           *BT_Fak_Grupy,

           *LV_Fak_Produkty,               // lista towarow w aktywnej grupie magazynowej
           *GR_Fak_Wybierz,
                   *BT_Fak_WybierzIndex,
                   *ST_Fak_WybierzIndex,       // index produktu
                   *BT_Fak_Wybierz,
                   *ST_Fak_Wybierz,            // nazwa produktu
           *ST_Fak_Ilo��,
           *ST_Fak_Rabat,
           *TX_Fak_Warto��,

           *BT_Fak_Akceptuj�,
           *BT_Fak_ZR�ki,
           *BT_Fak_Nast�pny,
           *TX_Fak_Total,

           *LV_Fak_FakturaProdukty,        // lista towarow na dokumencie
           *BT_Fak_Usu�,
           *BT_Fak_Edit,

           *BT_Fak_GoNext,
           *BT_Fak_Znajd�,

           *BT_Fak_Filter,
           *BT_Fak_FilterAll,

           *BT_Fak_Ok,
           *BT_Fak_Od���,
           *BT_Fak_Cancel;



char Sprzeda�Setup( void )
{

///   FakturaWindow

          FakturaWindow = WindowObject,
                                                // here
//                        MUIA_Window_SizeRight, TRUE,

                                                MUIA_Window_ID         , ID_WIN_FAKTURA,
//                        MUIA_Window_CloseGadget, FALSE,
                                                WindowContents,
                                                   VGroup,

                                                   Child, HGroup,
                                                                  _SH( SH_FAK_LAB_ODBIORCA ),
                                                                  Child, MakeLabel2(MSG_FAK_LAB_ODBIORCA),
                                                                  Child, TX_Fak_Odbiorca = TextObject, TextFrame, TextBack, MUIA_Text_SetMin, FALSE, End,
                                                                  Child, BT_Fak_Odbiorca = _TextButtonWeight( FAK_ODBIORCA, 2 ),

                                                                  End,
///                                 Lista dokumentu
                                                                  Child, VGroup,
//                                         GroupFrameT(MSG_FAK_GR_TITLE),
                                                                                 GroupFrame,
                                                                                 MUIA_Weight, 35,

                                                                                 Child, LV_Fak_FakturaProdukty = ListviewObject,
                                                                                                _SH( SH_FAK_LIST ),
                                                                                                MUIA_CycleChain, TRUE,
                                                                                                _MUIA_Listview_List, NewObject(CL_FakturaProductList->mcc_Class, NULL, TAG_DONE),
                                                                                                End,

                                                                                 Child, HGroup,
                                                                                                Child, MakeLabel2(MSG_FAK_TOTAL),
                                                                                                Child, TX_Fak_Total = TextObject, MUIA_Text_PreParse, "\033r\0338",
                                                                                                                 TextFrame, TextBack,
                                                                                                                 End,

                                                                                                Child, BT_Fak_Edit = _TextButton( FAK_EDIT ),
                                                                                                Child, BT_Fak_Usu� = _TextButton( FAK_USUN ),
                                                                                                End,


                                                                                 End,
//|
                                                                  Child, BalanceObject, End,
///                                 Lista magazynowa
                                                                  Child, VGroup,
//                                         GroupFrameT(MSG_FAK_GR_MAG_TITLE),
                                                                                 GroupFrame,
                                                                                 MUIA_Weight, 65,

                                                                                 Child, HGroup,
                                                                                                Child, MakeLabel2(MSG_MAG_GROUP),
                                                                                                Child, HGroup,
                                                                                                           MUIA_Group_Spacing, 1,
                                                                                                           Child, TX_Fak_Grupy = TextObject2, End,
                                                                                                           Child, BT_Fak_Grupy = PopButton2( MUII_PopUp, MSG_MAG_GROUP ),
                                                                                                           End,

                                                                                                Child, MUI_MakeObject(MUIO_VBar,1),
                                                                                                Child, HGroup,
//                                                     GroupFrame,
                                                                                                           MUIA_Weight, 20,
                                                                                                           Child, BT_Fak_Filter    = _TextButton( FAK_FILTER ),
                                                                                                           Child, BT_Fak_FilterAll = _TextButton( FAK_FILTER_ALL ),

                                                                                                           Child, MUI_MakeObject( MUIO_VBar, 1 ),

                                                                                                           Child, BT_Fak_Znajd� = _TextButtonWeight( FAK_FIND, 1 ),
                                                                                                           Child, BT_Fak_GoNext = _TextButtonWeight( FAK_GONEXT, 1 ),
                                                                                                           End,

                                                                                                End,


                                                                                 Child, LV_Fak_Produkty = ListviewObject,
                                                                                                MUIA_CycleChain, TRUE,
                                                                                                _MUIA_Listview_List, NewObject(CL_ProductList->mcc_Class, NULL,
                                                                                                                                         _MUIA_List_Format, "MIW=1 MAW=65 COL=1 BAR,"
                                                                                                                                                                                "MIW=1 MAW=10 COL=2 BAR,"
                                                                                                                                                                                "MIW=1 MAW=-1 COL=3 P=\033r BAR,"
                                                                                                                                                                                "MIW=1 MAW=-1 COL=7 P=\033r",
                                                                                                                                                                                TAG_DONE),
                                                                                                End,

                                                                                 Child, HGroup,
                                                                                                Child, GR_Fak_Wybierz = HGroup,
                                                                                                           MUIA_Group_PageMode, TRUE,
                                                                                                           Child, HGroup,
                                                                                                                          Child, BT_Fak_Wybierz = TextButtonWeight(MSG_MAG_SELECT, 1),
                                                                                                                          Child, ST_Fak_Wybierz = MakeString(PROD_NAME_LEN, NULL),
                                                                                                                          End,
                                                                                                           Child, HGroup,
                                                                                                                          Child, BT_Fak_WybierzIndex = TextButtonWeight(MSG_MAG_SELECTIDX, 1),
                                                                                                                          Child, ST_Fak_WybierzIndex = MakeString(PROD_INDEX_LEN, NULL),
                                                                                                                          End,
                                                                                                           End,


                                                                                                Child, MakeLabel2(MSG_FAK_AMOUNT),
                                                                                                Child, ST_Fak_Ilo�� = MakeCashStringWeight(10, MSG_FAK_AMOUNT, 30),
                                                                                                Child, MakeLabel2(MSG_FAK_DISCOUNT),
                                                                                                Child, ST_Fak_Rabat = MakeCashStringWeight(10, MSG_FAK_DISCOUNT, 30),
                                                                                                Child, MakeLabel2(MSG_FAK_PRICE),
                                                                                                Child, TX_Fak_Warto�� = TextObject, MUIA_Text_PreParse, "\033r\0338",
                                                                                                                                                TextFrame, TextBack,
                                                                                                                                                MUIA_Weight, 35,
                                                                                                                                                End,
                                                                                                End,

                                                                                 Child, HGroup,
                                                                                                Child, BT_Fak_ZR�ki     = _TextButton(FAK_Z_REKI),
                                                                                                Child, BT_Fak_Akceptuj� = _TextButton(FAK_ACCEPT),
                                                                                                Child, BT_Fak_Nast�pny  = _TextButton(FAK_NEXT),
                                                                                                End,


                                                                                 End,
//|

                                                   Child, HGroup,
                                                                  Child, BT_Fak_Ok     = _TextButton(FAK_OK),
                                                                  Child, BT_Fak_Od���  = _TextButtonWeight(FAK_OD���, 25),
                                                                  Child, BT_Fak_Cancel = _TextButton(FAK_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|


        if( !FakturaWindow )
           return( FALSE );


        /* FAKTURA WINDOW */

        DoMethod(FakturaWindow  , MUIM_Notify    , MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

//     Notyfikacja ustawiania w SMARTCYCLE/OM_SET!!!
//       DoMethod(CY_Fak_Typ_P�atno�ci, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_TYP_P�ATNO�CI);

        DoMethod(BT_Fak_Odbiorca, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_ODBIORCA);

        DoMethod(ST_Fak_Ilo��, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_CALC_WARTO��);
        DoMethod(ST_Fak_Rabat, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_CALC_WARTO��_2);
        DoMethod(ST_Fak_Rabat, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Window, 3 , MUIM_Set, MUIA_Window_ActiveObject, BT_Fak_Akceptuj�);

        DoMethod(ST_Fak_Wybierz, MUIM_Notify    , MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_WYBIERZ);
        DoMethod(ST_Fak_Wybierz, MUIM_Notify    , MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_PROD_ACK);
        set(ST_Fak_Wybierz     , MUIA_String_AttachedList, LV_Fak_Produkty);
        set(ST_Fak_WybierzIndex, MUIA_String_AttachedList, LV_Fak_Produkty);
        DoMethod(ST_Fak_WybierzIndex, MUIM_Notify    , MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_WYBIERZ_INDEX);
        DoMethod(ST_Fak_WybierzIndex, MUIM_Notify    , MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_PROD_ACK_INDEX);

        DoMethod(BT_Fak_GoNext  , MUIM_Notify    , MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_NEXT);
        DoMethod(BT_Fak_Znajd�  , MUIM_Notify    , MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_ZNAJD�);
//    set(BT_Fak_Nast�pny     , MUIA_CycleChain, FALSE);



        DoMethod(BT_Fak_Grupy    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_ZMIE�_GRUP�);
        DoMethod(LV_Fak_Produkty , MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_PROD_ACK_LV);

        DoMethod(LV_Fak_FakturaProdukty, MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_EDIT);
        DoMethod(BT_Fak_Usu�           , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_USU�);
        DoMethod(BT_Fak_Edit           , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_EDIT);

        DoMethod(BT_Fak_Filter   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_FILTER);
        DoMethod(BT_Fak_FilterAll, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_FILTER_ALL);

        DoMethod(BT_Fak_Ok       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Fak_Od���    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_OD���);
        DoMethod(BT_Fak_Cancel   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FAK_FREE_HAND);
        DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "f5", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FAK_EDIT);
        DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "f7", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_ZNAJD�);
        DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);
        DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FAK_USU�);

        DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "shift left" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FIRST_GROUP);
        DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "left" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PREV_GROUP);
        DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "right", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_NEXT_GROUP);
        DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "shift right", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_LAST_GROUP);

        DoMethod(BT_Fak_Akceptuj�, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ACCEPT);
        DoMethod(BT_Fak_Nast�pny , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ACTIVATE);
        DoMethod(BT_Fak_ZR�ki    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_FREE_HAND);


        DoMethod(BT_Fak_Wybierz     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_PAGE_1);
        DoMethod(BT_Fak_WybierzIndex, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_PAGE_0);


//       set(BT_Fak_Wybierz     , MUIA_CycleChain, FALSE);
//       set(BT_Fak_WybierzIndex, MUIA_CycleChain, FALSE);



        _attachwin( FakturaWindow );

        return( TRUE );
}

//|

/// Wy�wietlNazw�GrupyVat

void Wy�wietlNazw�GrupyVat(Object *obj, struct GrupaList *grupa)
{
/*
** Wy�wietlan nazw� podanej grupy w podany obiekcie
** dodaj�c odpowiedni atrybut w zale�no�ci
** od jej statusu (skasowana czy nie, us�ugi/towary etc)
*/

char preparse[2+2+1] = "\0";

        if(grupa->Deleted)
           strcpy(preparse, "\0338");

        if( grupa->gm_grupa.Typ != TYP_TOWAR )
           strcat(preparse, "\033b");

        set( obj, MUIA_Text_PreParse, preparse );
        set( obj, MUIA_Text_Contents, grupa->gm_grupa.Nazwa );

}
//|
/// UstawGrup�Aktywn�Vat

void UstawGrup�Aktywn�Vat(struct GrupaList *grupa, char Activate)
{

//    if(grupa == grupa_aktywna)
//       return;


        grupa_aktywna = grupa;

        if(grupa)
           {
           Wy�wietlNazw�GrupyVat(TX_Fak_Grupy, grupa);

           fak_filter.Aktywny = FALSE;
           ApplyMagFilter(LV_Fak_Produkty, grupa_aktywna, &fak_filter, FALSE);
           set(LV_Fak_Produkty, _MUIA_List_Active, 0);
           }
        else
           {
           DoMethod(LV_Fak_Produkty, _MUIM_List_Clear, NULL);
           set(TX_Fak_Grupy, MUIA_Text_Contents, MSG_MAG_NO_GROUPS);
           }


        // indeksy sa moze w tej grupie?
        if(xget(LV_Fak_Produkty, MUIA_ProdList_IndexCount) == 0)
           WybierzPage = 0;

        // czy nie jest czasem zly string widoczny wtedy kiedy nie trzeba?
        if( WybierzPage != xget(GR_Fak_Wybierz, MUIA_Group_ActivePage) )
           {
           // ano jest, wiec wylaczamy indeksy, bo ich tu nie ma
           set(GR_Fak_Wybierz, MUIA_Group_ActivePage, WybierzPage);
           }


        if(Activate)
           {
           if( WybierzPage == 0 )
                        DoMethod(app, MUIM_Application_PushMethod, FakturaWindow, 3, MUIM_Set,MUIA_Window_ActiveObject, ST_Fak_Wybierz);
           else
                        DoMethod(app, MUIM_Application_PushMethod, FakturaWindow, 3, MUIM_Set,MUIA_Window_ActiveObject, ST_Fak_WybierzIndex);
           }
}

//|
/// S�ownie
char *S�ownie(double number)
{
int pos, t, row, cnt=0;
char temp[20], c;
static char result[140];
char *np = result;
BOOL was = FALSE;

char *tho[11] =  {"tysi�cy ",
                                  "tysi�cy ",
                                  "tysi�ce ",
                                  "tysi�ce ",
                                  "tysi�ce ",
                                  "tysi�cy ",
                                  "tysi�cy ",
                                  "tysi�cy ",
                                  "tysi�cy ",
                                  "tysi�cy ",
                                  "tysi�c "};

char *mil[11] =  {"milion�w ",
                                  "milion�w ",
                                  "miliony ",
                                  "miliony ",
                                  "miliony ",
                                  "milion�w ",
                                  "milion�w ",
                                  "milion�w ",
                                  "milion�w ",
                                  "milion�w ",
                                  "milion "};

char *(ones[20]) = {"",
                                        "jeden ",
                                        "dwa ",
                                        "trzy ",
                                        "cztery ",
                                        "pi�� ",
                                        "sze�� ",
                                        "siedem ",
                                        "osiem ",
                                        "dziewi�� ",
                                        "dziesi�� ",
                                        "jedena�cie ",
                                        "dwana�cie ",
                                        "trzyna�cie ",
                                        "czterna�cie ",
                                        "pi�tna�cie ",
                                        "szesna�cie ",
                                        "siedemna�cie ",
                                        "osiemna�cie ",
                                        "dziewi�tna�cie "};

char *tens[10] = {"", "",
                                  "dwadzie�cia ",
                                  "trzydzie�ci ",
                                  "czterdzie�ci ",
                                  "pi��dziesi�t ",
                                  "sze��dziesi�t ",
                                  "siedemdziesi�t ",
                                  "osiemdziesi�t ",
                                  "dziewi��dziesi�t "};

char *hundreds[10] =  {"",
                                           "sto ",
                                           "dwie�cie ",
                                           "trzysta ",
                                           "czterysta ",
                                           "pi��set ",
                                           "sze��set ",
                                           "siedemset ",
                                           "osiemset ",
                                           "dziewi��set "};



                sprintf(temp, "%f", number);

                pos = strchr(temp, '.') - temp;

                if(pos > 0)
                   {
                   row = (pos - 1) / 3;
                   t = ((pos % 3) + 2) % 3;

                   while(row >= 0)
                           {
                           while(t >= 0)
                                   {
                                   c = temp[cnt++] - '0';
                                   switch(t)
                                           {
                                           case(2):
                                                   np = stpcpy(np, hundreds[c]);
                                                   break;

                                           case(1):
                                                   if(c == 1)
                                                         {
                                                         c = temp[cnt++] - '0' + 10;
                                                         t--;
                                                         np = stpcpy(np, ones[c]);
                                                         }
                                                   else
                                                         np = stpcpy(np, tens[c]);
                                                   break;

                                           case(0):
                                                   {
                                                   if(!was && (c == 1) && row)
                                                         c = 10;
                                                   else
                                                         np = stpcpy(np, ones[c]);
                                                   }
                                                   break;
                                           }
                                   t--;
                                   if(c)
                                           was = TRUE;
                                   }

                                   if(c > 10)
                                           c = 9;

                                   t = 2;

                                   if(was)
                                         switch(row)
                                           {
                                           case(2):
                                                  np = stpcpy(np, mil[c]);
                                                  break;

                                           case(1):
                                                  np = stpcpy(np, tho[c]);
                                                  break;

//                       case(0):
//                           np = stpcpy(np, "z�");
                                           }

                                        was = FALSE;
                                        row--;
                           }
                  }

                if(np == result)
                  {
                  np = stpcpy(np, "zero ");
                  }

                return(result);
}
//|

/// PRINT REQUESTER STRINGS

#define MSG_PRINT_WIN_TITLE "Drukowanie dokumentu"
#define MSG_PRINT_TITLE     "Wydruk dokumentu"
#define MSG_PRINT_AMOUNT    "_Liczba egzemplarzy"

#define MSG_PRINT_PARAGON   "_Paragon"

#define MSG_PRINT_OK        "_Wydrukuj"
#define MSG_PRINT_CANCEL    "_Nie drukuj"
#define MSG_PRINT_BACK      "Wr�� do _edycji"

//|
/// DrukowanieSetup

static Object
           *DrukowanieWindow,             /* DRUKOWANIE REQUESTER */
           *ST_Print_Amount,
           *CH_Print_Paragon,
           *BT_Print_Ok,
           *BT_Print_Cancel,
           *BT_Print_Back;


char DrukowanieSetup(struct DrukowanieRequest *dr)
{

///   DrukowanieWindow

          DrukowanieWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_PRINT_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_DRUKOWANIE,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,

                                                         Child, HGroup,
                                                                        GroupFrameT(MSG_PRINT_TITLE),

                                                                        Child, MakeLabel2(MSG_PRINT_AMOUNT),
                                                                        Child, ST_Print_Amount = MakeNumericString(3, MSG_PRINT_AMOUNT),

                                                                        Child, MakeLabel2(MSG_PRINT_PARAGON),
                                                                        Child, CH_Print_Paragon = MakeCheck(MSG_PRINT_PARAGON),
                                                                        End,

                                                         Child, HGroup,
                                                                        Child, BT_Print_Ok     = TextButton(MSG_PRINT_OK),
                                                                        Child, BT_Print_Cancel = TextButton(MSG_PRINT_CANCEL),
                                                                        Child, BT_Print_Back   = TextButton(MSG_PRINT_BACK),
                                                                        End,

                                                   End,
                                          End;
//|

        if( ! DrukowanieWindow )
           return( FALSE );


        if( ! settings.posnet_active )
                dr->BezParagonu = TRUE;

        set(ST_Print_Amount, MUIA_String_Integer, dr->LiczbaKopii);
        set(ST_Print_Amount, MUIA_Disabled      , dr->BezDokumentu);

        set(CH_Print_Paragon, MUIA_Disabled, dr->BezParagonu);
        if(dr->BezParagonu)
           set(CH_Print_Paragon, MUIA_Selected, FALSE);
        else
           {
           char swicz;

           switch(dr->RodzajDokumentu)
                   {
                   case FAKTURA_VAT:
                           swicz = settings.paragon_fvat;
                           break;

                   case RACHUNEK:
                           swicz = settings.paragon_rach;
                           break;

                   case PARAGON:
                           swicz = settings.paragon_par;
                           break;

                   default:
                           swicz = settings.paragon_inne;
                           break;
                   }

           set(CH_Print_Paragon, MUIA_Selected, swicz);
           }


        if(dr->BezDokumentu)
           set(DrukowanieWindow, MUIA_Window_ActiveObject, BT_Print_Ok);
        else
           set(DrukowanieWindow, MUIA_Window_ActiveObject, ST_Print_Amount);



        /* DRUKOWANIE REQUESTER */

        set(CH_Print_Paragon     , MUIA_CycleChain, FALSE);

        DoMethod(DrukowanieWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_BACK);
        DoMethod(BT_Print_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Print_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(BT_Print_Back   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_BACK);



        _attachwin( DrukowanieWindow );

        return( TRUE );

}
//|
/// DrukowanieFinish
char DrukowanieFinish(struct DrukowanieRequest *dr)
{
/*
** sprawdza poprawno�� wprowadzonych danych
** dot. drukowania. Zwraca FALSE, je�li co�
** jest nie tak, TRUE, je�li wszystkie
** elementy s� poprawne.
*/


        if(!(strlen((char *)xget(ST_Print_Amount, MUIA_String_Contents))) )
           {
           set(DrukowanieWindow, MUIA_Window_ActiveObject, ST_Print_Amount);
           DisplayBeep(0);
           return(FALSE);
           }

        dr->LiczbaKopii   = xget(ST_Print_Amount, MUIA_String_Integer);
        dr->DrukujParagon = getcheckmark(CH_Print_Paragon);

        return(TRUE);
}
//|
/// Drukowanie

long Drukowanie(struct DrukowanieRequest *dr)
{
/*
** Requester dot. drukowania dokument�w.
** Zwraca TRUE, albo FALSE je�li u�ytkownik
** nie chce nic drukowa� b�d� -1 je�li chce
** wr�ci� do edycji dokumentu
*/

char  running = TRUE;
ULONG signal  = 0;
long  result  = FALSE;

        set(app, MUIA_Application_Sleep, TRUE);

        if( ! DrukowanieSetup(dr) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }

        if(WinOpen(DrukowanieWindow))
           {
           while(running)
                  {
                  switch (DoMethod(app, MUIM_Application_Input, &signal))
                        {
                        case ID_OK:
                           if(DrukowanieFinish(dr))
                                   {
                                   result = TRUE;
                                   running = FALSE;
                                   }
                           break;


                        // nie drukuj
                        case ID_CANCEL:
                           dr->DrukujParagon = FALSE;
                           dr->LiczbaKopii   = 0;
                           running = FALSE;
                           break;

                        case ID_BACK:
                           result = -1;
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }
           }
        else
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           }

        _detachwin( DrukowanieWindow );
        set(app, MUIA_Application_Sleep, FALSE);

        return(result);
}

//|

/// RESZTA STRINGS

#define MSG_RES_TITLE   "Operacja kasowa"

#define MSG_RES_TOTAL   "Do zap�aty"
#define MSG_RES_PAID    "_Wp�ata"
#define MSG_RES_RESZTA  "Reszta"

#define MSG_RES_OK      "_Ok"
#define MSG_RES_CANCEL  "Ponie_chaj"
#define MSG_RES_BACK    "_Wr�c do edycji"

#define MSG_RES_ZA_MA�O "\033cWp�ata jest zbyt ma�a aby pokry�\nkwot� nale�n� do zap�aty\n(brakuje \0338%s\0332 z�)!"

//|
/// ResztaSetup

static Object
           *ResztaWindow,              /* RESZTA WINDOW */
           *TX_Res_DoZap�aty,
           *ST_Res_Wp�ata,
           *TX_Res_Reszta,
           *BT_Res_Ok,
           *BT_Res_Cancel,
           *BT_Res_Back;


char ResztaSetup(struct DocumentPrint *dp)
{

///   ResztaWindow

          ResztaWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_RES_TITLE,
                                                MUIA_Window_ID         , ID_WIN_RESZTA,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,

                                                   Child, ColGroup(2),
                                                                  GroupFrame,
//                                  GroupFrameT(MSG_POS_TITLE_2),
                                                                  MUIA_Font, MUIV_Font_Big,

                                                                  Child, MakeLabel2(MSG_RES_TOTAL),
                                                                  Child, TX_Res_DoZap�aty = TextObject, MUIA_Text_PreParse, "\033r", End,
                                                                  Child, MakeLabel2(MSG_RES_PAID),
                                                                  Child, ST_Res_Wp�ata = MakeString(11, MSG_RES_PAID),

                                                                  Child, Bar,
                                                                  Child, Bar,

                                                                  Child, MakeLabel2(MSG_RES_RESZTA),
                                                                  Child, TX_Res_Reszta = TextObject, MUIA_Text_PreParse, "\033r",End,
                                                                  End,

                                                   Child, HGroup,
                                                                  MUIA_Group_SameSize, TRUE,
                                                                  Child, BT_Res_Ok     = TextButton(MSG_RES_OK),
                                                                  Child, BT_Res_Cancel = TextButton(MSG_RES_CANCEL),
                                                                  Child, BT_Res_Back   = TextButton(MSG_RES_BACK),
                                                                  End,

                                                   End,
                                          End;

//|

        if( !ResztaWindow )
           return( FALSE );



        set(ResztaWindow, MUIA_Window_ActiveObject, ST_Res_Wp�ata);
        set(BT_Res_Ok, MUIA_Disabled, TRUE);

        settext(TX_Res_DoZap�aty, Price2String(dp->do_zap�aty));
        setstring(ST_Res_Wp�ata , Price2String(0.00));
//    settext(TX_Res_Reszta , Price2String(0.00));
        settext(TX_Res_Reszta , "");


        /* RESZTA WINDOW */

        DoMethod(ResztaWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_BACK);

        set(ST_Res_Wp�ata, MUIA_String_Format, MUIV_String_Format_Right);
        DoMethod(ST_Res_Wp�ata, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_STR_ACK);

        DoMethod(BT_Res_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Res_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(BT_Res_Back  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_BACK);


        _attachwin( ResztaWindow );

        return( TRUE );

}

//|
/// Reszta

char Reszta(struct DocumentPrint *dp)
{
/*
** Requester dot. wp�aconej nale�no�ci / reszty
** Zwraca TRUE, albo FALSE je�li u�ytkownik chce wr�ci� do edycji
*/

char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

        set(app, MUIA_Application_Sleep, TRUE);


        if( ! ResztaSetup( dp ) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }

        if(WinOpen(ResztaWindow))
           {
           while(running)
                  {
                  switch (DoMethod(app, MUIM_Application_Input, &signal))
                        {
                        case ID_STR_ACK:
                           {
                           char   *str = getstr(ST_Res_Wp�ata);
                           double wp�ata;

                           if(strlen(str) > 0)
                                   {
                                   wp�ata = String2Price(str);
                                   nnset(ST_Res_Wp�ata, MUIA_String_Contents, Price2String(wp�ata));
/*
                                   printf("kwota : %#.5f\n", dp->do_zap�aty);
                                   printf("wplata: %#.5f\n", wp�ata);


                                   D(bug("Reszta:\n Kwota: %s\n Wplata: %s\n", Price2String(dp->do_zap�aty), Price2String(wp�ata) ));
*/

                                   if(wp�ata >= dp->do_zap�aty)
                                           {
                                           dp->got�wka_wp�acona = wp�ata;
                                           dp->reszta = dp->do_zap�aty - wp�ata;
                                           settext(TX_Res_Reszta, Price2String(fabs(dp->reszta)));

                                           set(BT_Res_Ok, MUIA_Disabled, FALSE);
                                           set(ResztaWindow, MUIA_Window_ActiveObject, BT_Res_Ok);
                                           break;
                                           }
                                   else
                                           {
                                           char tmp[20];

                                           DisplayBeep(0);
                                           sprintf(tmp, "\0338%s", Price2String(wp�ata - dp->do_zap�aty));
                                           settext(TX_Res_Reszta, tmp);

                                           MUI_Request(app, ResztaWindow, 0, TITLE, MSG_OK, MSG_RES_ZA_MA�O, Price2String(dp->do_zap�aty - wp�ata));
                                           set(ResztaWindow, MUIA_Window_ActiveObject, ST_Res_Wp�ata);
                                           break;
                                           }
                                   }
                           else
                                   {
                                   DisplayBeep(0);
                                   set(ResztaWindow, MUIA_Window_ActiveObject, ST_Res_Wp�ata);
                                   }
                           }
                           break;

                        case ID_OK:
                           {
                           result = TRUE;
                           running = FALSE;
                           }
                           break;


                        // nie drukuj
                        case ID_CANCEL:
                           result = FALSE;
                           running = FALSE;
                           break;

                        case ID_BACK:
                           result = -1;
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

           }
        else
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           }

        _detachwin( ResztaWindow );
        set(app, MUIA_Application_Sleep, FALSE);

        return(result);
}

//|

/// DocumentExists

/*
** sprawdza czy taki dokument juz istnieje w bazie
** zwraca BOOL
*/

/*
        DataWystawienia (datestamp.ds_Days)
        |
        |   NIP
        |   |         Numer dokumentu
        |   |         |
        |   |         |                Znacznik podzialu nipu
        |   |         |                |
        |   |         |                |
        |   |         |                |
        0         1         2         3
        01234567890123456789012345678901

*/

char DocumentExists(struct DocumentPrint *dp)
{
char   FullPath[ 100 ];
//char   FileComment[80];

int    i;
char   numer[15];
char   Miesiac[3];
char   Rok[5];
char   _miesiac[15];

char   dir_offset = OD_FAKTURA_VAT;
char   result = FALSE;
int    DokumentDirOffset = 0;

static char pattern[(FILTR_NAME_LEN*2)+2];
char   FullPattern[256];         // finalny wzorzec nazwy pliku ze �cie�k�


        strcpy( Miesiac, wfmh_Date2StrFmt( "%m", &dp->DataWystawienia ) );
        strcpy( Rok    , wfmh_Date2StrFmt( "%Y", &dp->DataWystawienia ) );


        // poki co nie sprawdzmy poprawnosci numeracji zamowien


        switch( dp->RodzajDokumentu )
           {
           case ODRECZNA:
                   strcpy(_miesiac, Miesiac);
                   break;

           default:
                   strcpy(_miesiac, "??");
                   break;
           }



        // Zamieniamy znaki '/' z numeru dokumentu na '-'

        for(i=0; i<sizeof(numer); i++)
           {
           numer[i] = dp->NumerDokumentu[i];
           if(numer[i] == 0)
                   break;
/*
           if(!IsDigit(MyLocale, numer[i]))
                   numer[i] = '-';
*/
           if(numer[i] == '/')
                   numer[i] = '-';
           }

/*
        if(dp->Odbiorca)
           {
           sprintf(FullPath, "%s/%s/%s/%02lx/%04lx%10.10s%s", DatabasesDir,
                                                                                                                  Rok, Miesiac,
                                                                                                                  dp->RodzajDokumentu + dir_offset,
                                                                                                                  dp->DataWystawienia.ds_Days,
                                                                                                                  dp->Odbiorca->NIP,
                                                                                                                  numer);
           }
        else
           {
           sprintf(FullPath, "%s/%s/%s/%02lx/%04lx%10.10s%s", DatabasesDir,
                                                                                                                  Rok, Miesiac,
                                                                                                                  dp->RodzajDokumentu,
                                                                                                                  dp->DataWystawienia.ds_Days,
                                                                                                                  "          ",
                                                                                                                  numer);
           }

*/



        sprintf(FullPattern, "%s/%s/%s/%02lx/??????????????%-15.15s?",
                                                                                          DatabasesDir,
                                                                                          Rok,
                                                                                          _miesiac,
                                                                                          dp->RodzajDokumentu + DokumentDirOffset,
                                                                                          numer);


        D(bug("DocExists('%s')\n", FullPattern ));

        {
        struct AnchorPath *anchor;

        #define PATH_BUFFER 80

        if(anchor = calloc(1, sizeof(struct AnchorPath)+PATH_BUFFER))
          {
          anchor->ap_Strlen = PATH_BUFFER;

          if(MatchFirst(FullPattern, anchor) == 0)
                {
                MatchEnd(anchor);

                result = TRUE;
                }

          free(anchor);
          }
        else
          {
          DisplayBeep(0);
          MUI_Request( app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY );
          }
   }



   return( result );

}
//|
/// ZapiszDokument

char ZapiszDokument(struct DocumentPrint *dp, char Od���)
{

/*
** Zapisuje wystawiony dokument na dysku
** Zwraca TRUE/FALSE w zale�no�ci od powodzenia operacji
*/

/*
**  FORMAT NAZWY ZBIORU:

        DataWystawienia (datestamp.ds_Days)
        |
        |   NIP
        |   |         Numer dokumentu
        |   |         |
        |   |         |               Znacznik podzialu nipu
        |   |         |               |
        |   |         |               |Wolne (p�ki co)
        |   |         |               ||
        0         1         2         3
        01234567890123456789012345678901

*/

#ifndef DEMO

#define _ID_TYPE ID_DOC

char   FullPath[100];
char   FileComment[80];

struct IFFHandle *MyIFFHandle;
char   numer[15];
int    i;
char   First[2];
char   Miesiac[3];
char   Rok[5];

double TotalNetto = 0;   // wartosc dokumentu
double TotalVat   = 0;

char   result = TRUE;
char   dir_offset = OD_FAKTURA_VAT * Od���;

struct Klient *klient = &dp->Odbiorca->kl_klient;
                                                                                
 
        set(app, MUIA_Application_Sleep, TRUE);


        wfmh_Date2StrFmtBuffer( Miesiac, "%m", &dp->DataWystawienia );
        wfmh_Date2StrFmtBuffer( Rok    , "%Y", &dp->DataWystawienia );



        // Zamieniamy znaki '/' z numeru dokumentu na '-'

        for(i=0; i<sizeof(numer); i++)
           {
           numer[i] = dp->NumerDokumentu[i];
           if(numer[i] == 0)
                   break;
/*
           if(!IsDigit(MyLocale, numer[i]))
                   numer[i] = '-';
*/
           if(numer[i] == '/')
                   numer[i] = '-';
           }


        if(dp->Odbiorca)
           {
           sprintf(FullPath, "%s/%s/%s/%02lx/%04lx%10.10s%-15.15s%01d",
                                                                                        DatabasesDir,
                                                                                        Rok, Miesiac,
                                                                                        dp->RodzajDokumentu + dir_offset,
                                                                                        dp->DataWystawienia.ds_Days,
                                                                                        klient->NIP,
                                                                                        numer,
                                                                                        klient->Sp��kaCywilna
                                                                                        );



           sprintf(FileComment, "%04lx%9.9s%s", dp->DataP�atno�ci.ds_Days,
                                                                                        klient->Regon,
                                                                                        klient->Nazwa1
                                                                                        );

           }
        else
           {
           sprintf(FullPath, "%s/%s/%s/%02lx/%04lx%10.10s%-15.15s%01d",
                                                                                        DatabasesDir,
                                                                                        Rok, Miesiac,
                                                                                        dp->RodzajDokumentu,
                                                                                        dp->DataWystawienia.ds_Days,
                                                                                        "          ",
                                                                                        numer,
                                                                                        0  // Sp��ka Cywilna
                                                                                        );


           sprintf(FileComment, "%04lx%9.9s%s",
                                                                                  dp->DataP�atno�ci.ds_Days,
                                                                                  "", // Regon
                                                                                  "" // Nazwa1
                                                                                  );

           }


//    D(bug("FullPath: '%s'\n" , FullPath));
//    D(bug("Comment : '%s'\n" , FileComment));
//    D(bug("lv_obj  : 0x%lx\n", dp->lv_object ));



        if(MyIFFHandle = AllocIFF())
                {
                BPTR  FileHandle;

                if(FileHandle = Open(FullPath, MODE_NEWFILE))
                   {
                   MyIFFHandle->iff_Stream = FileHandle;
                   InitIFFasDOS(MyIFFHandle);

                   if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
                          {
                          struct BaseVersion version;

                          // kwota total dokumentu
                                {
                                struct ProductList *prod;

                                double cena_sprzeda�y;
                                double warto��;
                                double vat;

                                for(i=0;;i++)
                                   {
                                   DoMethod(dp->lv_object, _MUIM_List_GetEntry, i, &prod);
                                   if(!prod)
                                           break;

                                   cena_sprzeda�y = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
                                   warto��        = cena_sprzeda�y - CalcRabat( cena_sprzeda�y, prod->pl_prod.Rabat );
                                   TotalNetto    += prod->pl_prod.Ilo�� * warto��;
                                   TotalVat      += prod->pl_prod.Ilo�� * CalcVat( warto��, prod->pl_prod.Vat );
                                   }
                                }
                          
                          
                          PushChunk(MyIFFHandle, ID_DOC, ID_CAT, IFFSIZE_UNKNOWN);

                           // numer wersji

                           PushChunk(MyIFFHandle, ID_DOC, ID_FORM, IFFSIZE_UNKNOWN);
                                   PushChunk(MyIFFHandle, ID_DOC, ID_VERS, IFFSIZE_UNKNOWN);
                                   version.Version = VERSION;
                                   version.Revision = REVISION;
                                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                                   PopChunk(MyIFFHandle);
                           PopChunk(MyIFFHandle);

                           PushChunk(MyIFFHandle, ID_DOC, ID_FORM, IFFSIZE_UNKNOWN);


                           // dane sprzedawcy

                                 _write(ID_SELL, settings.wystawca)


                           // rodzaj dokumentu i numer

                                  _write(ID_DOCT, dp->RodzajDokumentu)
                                  _write(ID_DOCN, dp->NumerDokumentu)


                           // Odbiorca

                                  if(dp->Odbiorca)
                                        _write_size(ID_CUST, &dp->Odbiorca->kl_klient, struct Klient)

                           // osoba wystawiaj�ca i odbieraj�ca

                                  _write(ID_WYST, logged_user->user.Nazwa)
                                  if(dp->OsobaOdbieraj�ca[0] != '\0')
                                          _write(ID_RECE, dp->OsobaOdbieraj�ca)

                           // daty i spos�b p�atno�ci

                                  _write(ID_CREA, dp->DataWystawienia)
                                  _write(ID_SALE, dp->DataSprzeda�y)
                                  _write(ID_PAYD, dp->DataP�atno�ci)

                                  _write(ID_PAY, dp->TypP�atno�ci)

                                // rozne
                                
                                  _write(ID_TNET, TotalNetto )
                                  _write(ID_TVAT, TotalVat )
                                
 
                                  //

                           PopChunk(MyIFFHandle);


                           // pozycje faktury

                           PushChunk(MyIFFHandle, ID_DOC, ID_FORM, IFFSIZE_UNKNOWN);
                                 {
                                 struct ProductList *prod;

                                 for(i=0;;i++)
                                   {
                                   DoMethod(dp->lv_object, _MUIM_List_GetEntry, i, &prod);
                                   if(!prod)
                                           break;


                                   // zapami�tujemy real ilo��, je�li potrzeba
                                   if( Od��� )
                                           prod->pl_prod.StanMin = prod->RealIlo��;

                                   PushChunk(MyIFFHandle, ID_DOC, ID_PROD, IFFSIZE_UNKNOWN);
                                   WriteChunkBytes(MyIFFHandle, &prod->pl_prod, sizeof(struct Product));
                                   PopChunk(MyIFFHandle);
                                   }

                                 }
                           PopChunk(MyIFFHandle);

                          PopChunk(MyIFFHandle);

                          CloseIFF(MyIFFHandle);
                          }
                   else
                          {
                          DisplayBeep(0);
                          D(bug("*** OpenIFF() nie powiod�o si�\n"));
                          result = FALSE;
                          }

                   Close(FileHandle);

                   if(result)
                           SetComment(FullPath, FileComment);
                   }
                else
                   {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
                   DisplayBeep(0);
                   D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", FullPath));
                   result = FALSE;
                   }

                FreeIFF(MyIFFHandle);
                }
         else
                {
                DisplayBeep(0);
                D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
                result = FALSE;
                }




        set(app, MUIA_Application_Sleep, FALSE);

        return(result);

#else

        return(TRUE);

#endif

}



//|

/*
/// FREE HAND WINDOW STRINGS

#define MSG_FREE_TITLE "Sprzeda� \"z r�ki\""

#define MSG_FREE_ED_TITLE   "Dane produktu"
#define MSG_FREE_NAME       "_Nazwa"
#define MSG_FREE_SWW        "_SWW"
#define MSG_FREE_AMOUNT     "_Ilo��"
#define MSG_FREE_JEDN       "_Jedn. miary"
#define MSG_FREE_ZAKUP      "Cena _zakupu"
#define MSG_FREE_MARZA      "Ma_r�a"
#define MSG_FREE_MARZA2     "z�  ("
#define MSG_FREE_MARZA3     "%)"
#define MSG_FREE_VAT        "Stawka _VAT"
#define MSG_FREE_SPRZ       "Cena _brutto"

#define MSG_FREE_CALC_TITLE "Mini kalkulator"
#define MSG_FREE_CALC_NETTO "Sprz_eda�"
#define MSG_FREE_CALC_COPY  "_^"
#define MSG_FREE_BRUTTO     "_Brutto"

#define MSG_FREE_OK         "_Ok"
#define MSG_FREE_CANCEL     "Ponie_chaj"

#define MSG_FREE_ED_VATSWW  "\033cPrzy obni�onym podatku VAT musisz\npoda� co najmniej 4 pierwsze\ncyfry symbolu SWW!"
//|
*/

/// NAG��WEK DOKUMENTU STRINGS

#define MSG_NAG_WIN_TITLE  "Nag��wek dokumentu"

// labele
#define MSG_NAG_NAME       "N_azwa"
#define MSG_NAG_ED_ULICA   "Adres"
#define MSG_NAG_ED_KOD     "Kod"
#define MSG_NAG_ED_MIASTO  "Miasto"
#define MSG_NAG_ED_NIP     "NIP"
#define MSG_NAG_ED_REGON   "Regon"

#define MSG_NAG_ED_WYBIERZ "F6 - _Wybierz"
#define  SH_NAG_ED_WYBIERZ "Wyb�r odbiorcy z bazy kontrahent�w"


#define MSG_NAG_ODB      "Dane odbiorcy"
#define  SH_NAG_ODB      "Dane odbiorcy dokumentu"
#define MSG_NAG_DATA     "Data"
#define MSG_NAG_WYST     "Wystawienia"
#define MSG_NAG_SPRZ     "_Sprzeda�y"
#define  SH_NAG_SPRZ       "Data sprzeda�y (us�ugi/towaru)"
#define MSG_NAG_TYP_PLAT "_P�atno��"
#define MSG_NAG_PLAT     "_Termin"
#define  SH_NAG_PLAT       "Termin p�atno�ci"

#define MSG_NAG_PARAGON_TITLE "Numer paragonu"
#define MSG_NAG_PARAGON       "_Paragon"
#define  SH_NAG_PARAGON       "Numer paragonu do kt�rego jest\nwystawiany aktualny dokument"

#define MSG_NAG_ODBIORCA_TITLE "Odbiorca"
#define MSG_NAG_ODBIORCA       "O_dbiorca"
#define  SH_NAG_ODBIORCA       "Imi� i nazwisko osoby dobieraj�cej dokument"


#define MSG_NAG_OK       "F10 - _Ok"
#define  SH_NAG_OK       "Zaakceptuj wprowadzone dane"
#define MSG_NAG_CANCEL   "ESC - Ponie_chaj"
#define  SH_NAG_CANCEL   "Anuluj wszystkie wprowadzone zmiany"

//|
/// Nag�owekDokumentuSetup

static Object *Nag��wekDokumentuWindow,     /* NAG��WEK DOKUMENTU WINDOW */

           *TX_Nag_Nazwa_1,
           *TX_Nag_Nazwa_2,
           *TX_Nag_Ulica,
           *TX_Nag_Kod,
           *TX_Nag_Miasto,
           *TX_Nag_NIP,
           *TX_Nag_Regon,
           *BT_Nag_Wybierz,

           *ST_Nag_Nazwa_1,
           *ST_Nag_Nazwa_2,
           *ST_Nag_Ulica,
           *ST_Nag_Kod,
           *ST_Nag_Miasto,
           *ST_Nag_NIP,
           *ST_Nag_Regon,
           *TX_Nag_Data_Wyst,
           *ST_Nag_Data_Sprz,
           *TX_Nag_P�atno��,
           *BT_Nag_P�atno��,
           *ST_Nag_Data_P�at,

           *ST_Nag_Paragon,

           *ST_Nag_Odbiorca,
           *BT_Nag_Odbiorca,

           *BT_Nag_Ok,
           *BT_Nag_Cancel;


char Nag�owekDokumentuSetup(struct DocumentPrint *dp)
{

//    set(FakturaWindow, MUIA_Window_ScreenTitle, dp->Nag��wekDokumentu);
//    set(FakturaWindow, MUIA_Window_Title      , dp->Nag��wekDokumentu);


struct Klient *klient;


///   Create Nag��wekDokumentuWindow

   Nag��wekDokumentuWindow = WindowObject,
                                                // here
//                        MUIA_Window_SizeRight, TRUE,

                                                MUIA_Window_ID    , ID_WIN_ODBIORCA_DOKUMENTU,
//                        MUIA_Window_Title , MSG_NAG_WIN_TITLE,

                                                MUIA_Window_ScreenTitle, dp->Nag��wekDokumentu,
                                                MUIA_Window_Title      , dp->Nag��wekDokumentu,

                                                WindowContents,
                                                   VGroup,

///                          Lista kontrahent�w

                                                   Child, VGroup,
                                                                  GroupFrameT(MSG_NAG_ODB),
                                                                  MUIA_ShortHelp, SH_NAG_ODB,

                                                                  MUIA_Group_Columns, 2,

                                                                  Child, MakeLabel2(MSG_NAG_NAME),
                                                                  Child, HGroup,
                                                                                 Child, TX_Nag_Nazwa_1 = TextObject, TextFrame, TextBack, End,
                                                                                 Child, BT_Nag_Wybierz = _TextButtonWeight(NAG_ED_WYBIERZ, 10),
                                                                                 End,

                                                                  Child, EmptyLabel(),
                                                                  Child, TX_Nag_Nazwa_2 = TextObject, TextFrame, TextBack, End,

                                                                  Child, MakeLabel2(MSG_NAG_ED_ULICA),
                                                                  Child, TX_Nag_Ulica = TextObject, TextFrame, TextBack, End,

                                                                  Child, MakeLabel2(MSG_NAG_ED_KOD),
                                                                  Child, HGroup,
                                                                                 Child, TX_Nag_Kod = TextObject, TextFrame, TextBack, End,

                                                                                 Child, MakeLabel2(MSG_NAG_ED_MIASTO),
                                                                                 Child, TX_Nag_Miasto = TextObject, TextFrame, TextBack, End,
                                                                                 End,

                                                                  Child, MakeLabel2(MSG_NAG_ED_NIP),
                                                                  Child, HGroup,
                                                                                 Child, TX_Nag_NIP = TextObject, TextFrame, TextBack, End,

                                                                                 Child, MakeLabel2(MSG_NAG_ED_REGON),
                                                                                 Child, TX_Nag_Regon = TextObject, TextFrame, TextBack, End,
                                                                                 End,
                                                                  End,

//|
///                          Daty

                                                                  /* Daty sprzeda�y, p�atno�ci etc */

                                                   Child, VGroup,
                                                                  GroupFrameT(MSG_NAG_DATA),
                                                                  MUIA_Weight, 35,

                                                                  Child, HGroup,
                                                                                 MUIA_Group_Columns, 4,

                                                                                 Child, MakeLabel2(MSG_NAG_WYST),
                                                                                 Child, TX_Nag_Data_Wyst = TextObject2, End,

                                                                                 Child, MakeLabel2(MSG_NAG_SPRZ),
                                                                                 Child, ST_Nag_Data_Sprz = _MakeString(DATE_LEN, NAG_SPRZ),


                                                                                 Child, MakeLabel2( MSG_NAG_TYP_PLAT ),
                                                                                 Child, HGroup,
                                                                                                MUIA_Group_Spacing, 1,

                                                                                                Child, TX_Nag_P�atno�� = TextObject2, End,
                                                                                                Child, BT_Nag_P�atno�� = PopButton2(MUII_PopUp, MSG_NAG_TYP_PLAT),
                                                                                                End,

                                                                                 Child, MakeLabel2(MSG_NAG_PLAT),
                                                                                 Child, ST_Nag_Data_P�at = _MakeString(DATE_LEN, NAG_PLAT),

                                                                                 End,


                                                                  End,
//|
///                          Paragon i odbiorca
                                                   Child, HGroup,
                                                                  Child, HGroup,
                                                                                 GroupFrameT(MSG_NAG_PARAGON_TITLE),
                                                                                 MUIA_ShortHelp, SH_NAG_PARAGON,
                                                                                 Child, MakeLabel2(MSG_NAG_PARAGON),
                                                                                 Child, ST_Nag_Paragon = _MakeString(PARAGON_LEN, NAG_PARAGON),
                                                                                 End,
                                                                  Child, HGroup,
                                                                                 GroupFrameT(MSG_NAG_ODBIORCA_TITLE),
                                                                                 MUIA_ShortHelp, SH_NAG_ODBIORCA,
//                                         Child, MakeLabel2(MSG_NAG_ODBIORCA),
                                                                                 Child, ST_Nag_Odbiorca = _MakeString(ODB_NAME_LEN, NULL),
                                                                                 Child, BT_Nag_Odbiorca = _TextButtonWeight(NAG_ODBIORCA, 10),
                                                                                 End,

                                                                  End,
//|

                                                   Child, HGroup,
                                                                  Child, BT_Nag_Ok     = _TextButton( NAG_OK ),
                                                                  Child, BT_Nag_Cancel = _TextButton( NAG_CANCEL ),
                                                                  End,

                                                   End,
                                          End;


//|


        if(!Nag��wekDokumentuWindow)
           return(FALSE);

        if( dp->Odbiorca )
           klient = &dp->Odbiorca->kl_klient;


        // setting up

        if(dp->Odbiorca)
           {
           settext(TX_Nag_Nazwa_1, klient->Nazwa1);
           settext(TX_Nag_Nazwa_2, klient->Nazwa2);
           settext(TX_Nag_Ulica  , klient->Ulica);
           settext(TX_Nag_Kod    , klient->Kod);
           settext(TX_Nag_Miasto , klient->Miasto);
           settext(TX_Nag_NIP    , NIP2Str(klient->NIP, klient->Sp��kaCywilna));
           settext(TX_Nag_Regon  , klient->Regon);
           }
        else
           {
           settext(TX_Nag_Nazwa_1, "");
           settext(TX_Nag_Nazwa_2, "");
           settext(TX_Nag_Ulica  , "");
           settext(TX_Nag_Kod    , "");
           settext(TX_Nag_Miasto , "");
           settext(TX_Nag_NIP    , "");
           settext(TX_Nag_Regon  , "");
           }

        setstring(ST_Nag_Odbiorca , dp->OsobaOdbieraj�ca);
        setstring(ST_Nag_Paragon, dp->NumerParagonu);



        // domy�lne daty i terminy (sprzeda�, p�atno��)

        settext(TX_Nag_Data_Wyst  , wfmh_Date2Str(&dp->DataWystawienia) );
        setstring(ST_Nag_Data_Sprz, wfmh_Date2Str(&dp->DataSprzeda�y));

        settext(TX_Nag_P�atno��, dp->TypP�atno�ci);

        D(bug( "typ platnosci: %lx\n", dp->TypP�atno�ci ));

        if( dp->TypP�atno�ci )
           {
           struct P�atno��List *p�atno�� = NULL;

           if(p�atno�� = Znajd�P�atno��Nazwa( dp->TypP�atno�ci ))
                   {
                   struct DateStamp *datestamp;

                   if(p�atno��->pl_p�atno��.Got�wka == FALSE)
                         dp->DataP�atno�ci.ds_Days = dp->DataWystawienia.ds_Days + p�atno��->pl_p�atno��.Zw�oka;

                   setstring(ST_Nag_Data_P�at, wfmh_Date2Str(&dp->DataP�atno�ci));
                   }
           else
                  {
//          DisplayBeep(0);
                  D(bug("** Nie mog� znale�� p�atno�ci o tym numerze!!\n"));
                  }
          }


        // je�li dok. dot. paragonu odblokowujemy odp. pole

        switch(dp->RodzajDokumentu)
           {
           case FVAT_PAR:
           case RACH_PAR:
                   _enable(ST_Nag_Paragon);
                   break;

           case ORDER:
                   _disable(ST_Nag_Data_P�at);
                   _disable(ST_Nag_Data_Sprz);
                   _disable(TX_Nag_P�atno��);
                   _disable(BT_Nag_P�atno��);
                   _disable(ST_Nag_Paragon);
                   break;

           default:
                   _disable(ST_Nag_Paragon);
                   _enable(ST_Nag_Data_P�at);
                   _enable(ST_Nag_Data_Sprz);
                   _enable(TX_Nag_P�atno��);
                   _enable(BT_Nag_P�atno��);
                   break;
           }



        // je�li dok. wymaga podpisu (faktura) odbiorcy, odblokowujemy odp. pole

        _disable(ST_Nag_Odbiorca);
        _disable(BT_Nag_Odbiorca);

        if(dp->Odbiorca)
           {
           switch(dp->RodzajDokumentu)
                  {
                  case FAKTURA_VAT:
                  case FVAT_KOR:
                  case FVAT_PAR:
                        _enable(ST_Nag_Odbiorca);
                        _enable(BT_Nag_Odbiorca);
                        break;
                  }
           }


        // sprawdzamy czy co� w polu User dla nas jest
        // robimy to na samym koncu, tak zeby juz wszystkie
        // inne sprawdzenia zostaly zrobione

        switch( dp->User )
           {
           case FVAT_PAR:          // znaczy wystawiamy dokument do istniej�cego paragonu
           case RACH_PAR:          // z okna redagowania. Nalezy co� tam zablokowa�, bo ju�
                                                           // dokument (paragon) istnieje i musi si� par� rzeczy zgadza�

                        _disable(ST_Nag_Paragon);
                        _disable(ST_Nag_Data_P�at);
                        _disable(ST_Nag_Data_Sprz);
                        _disable(TX_Nag_P�atno��);
                        _disable(BT_Nag_P�atno��);

                        _enable(ST_Nag_Odbiorca);
                        _enable(BT_Nag_Odbiorca);

                        break;
           }




        // notyfikacje

        DoMethod(Nag��wekDokumentuWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(BT_Nag_Wybierz , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_KLIENT_WYBIERZ);
        DoMethod(Nag��wekDokumentuWindow , MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_KLIENT_WYBIERZ );
        DoMethod(Nag��wekDokumentuWindow , MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK );

        DoMethod(BT_Nag_Odbiorca, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_KLIENT_ODBIORCA);
        DoMethod(BT_Nag_P�atno��, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT);

        DoMethod(BT_Nag_Ok      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Nag_Cancel  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


        _attachwin(Nag��wekDokumentuWindow);

        return(TRUE);               

}
//|
/// Nag��wekDokumentu

char Nag��wekDokumentu(struct DocumentPrint *main_dp)
{

struct DocumentPrint dp;
char   running     = TRUE;
ULONG  signal      = 0;
char   result      = FALSE;
char   NIPRequired = FALSE;            // czy dokument dla kt�rego to okno
                                                                           // zosta�o wywo�ane, wymaga NIPu

// Setting up


        _sleep(TRUE);

        memcpy(&dp, main_dp, sizeof(struct DocumentPrint));

        if( !Nag�owekDokumentuSetup(&dp) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }



        // spr. czy NIP jest wymagany

        switch(dp.RodzajDokumentu)
           {
           case FAKTURA_VAT:
           case FVAT_PAR:
                   NIPRequired = TRUE;
                   break;

           default:
                   NIPRequired = FALSE;
                   break;
           }




        DoMethod(app, MUIM_Application_PushMethod, Nag��wekDokumentuWindow, 3, MUIM_Set,MUIA_Window_ActiveObject, BT_Nag_Wybierz);


        if( WinOpen(Nag��wekDokumentuWindow) )
           {
           while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);
                  switch(ID)
                        {
                        case ID_KLIENT_WYBIERZ:
                           {
                           struct KlientList *klientList;
                           struct Filtr sel_cust_filter =
                                   {
                                   TRUE,
                                   "#?",              // Nazwa

                                   0,                 // Sprzedawca
                                   1,                 // Nabywca
                                   0,                 // Kosztowiec
                                   0,                 // A
                                   0,                 // B
                                   0                  // C
                                   };



                           if( NIPRequired )
                                   klientList = KontrahentSelector(&sel_cust_filter, dp.Odbiorca, F_CUSTED_NIP_REQUIRED );
                           else
                                   klientList = KontrahentSelector(&sel_cust_filter, dp.Odbiorca, 0 );


                           if(klientList)
                                   {
                                   struct Klient    *klient    = &klientList->kl_klient;
                                   struct KlientExt *klientExt = &klientList->kl_klientExt;

                                   dp.Odbiorca = klientList;

                                   settext(TX_Nag_Nazwa_1, klient->Nazwa1);
                                   settext(TX_Nag_Nazwa_2, klient->Nazwa2);
                                   settext(TX_Nag_Ulica  , klient->Ulica);
                                   settext(TX_Nag_Kod    , klient->Kod);
                                   settext(TX_Nag_Miasto , klient->Miasto);
                                   settext(TX_Nag_NIP    , NIP2Str(klient->NIP, klient->Sp��kaCywilna));
                                   settext(TX_Nag_Regon  , klient->Regon);

                                   set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, BT_Nag_Ok);


                                   strcpy(dp.OsobaOdbieraj�ca, "");
                                   setstring(ST_Nag_Odbiorca , "");


                                   // sprawdzamy czy klient ma podany poprawny domyslny sposob platnosci
                                   {
                                   if( klientExt->def_Payment[0] != 0 )
                                         {
                                         struct P�atno��List *p�atno�� = Znajd�P�atno��Nazwa( klientExt->def_Payment );

                                         if( p�atno�� )
                                           {
                                           struct DateStamp *datestamp;

                                           strncpy(dp.TypP�atno�ci, p�atno��->pl_p�atno��.Nazwa, PAY_NAME_LEN);
                                           settext(TX_Nag_P�atno��, p�atno��->pl_p�atno��.Nazwa);

                                           if(datestamp = wfmh_Str2Date((char *)xget(TX_Nag_Data_Wyst, MUIA_Text_Contents)))
                                                   {
                                                   if(p�atno��->pl_p�atno��.Got�wka == FALSE)
                                                         datestamp->ds_Days += p�atno��->pl_p�atno��.Zw�oka;
                                                   setstring(ST_Nag_Data_P�at, wfmh_Date2Str(datestamp));

                                                   set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, BT_Nag_Odbiorca);
                                                   }
                                                 else
                                                   {
                                                   DisplayBeep(0);
                                                   }
                                           }
                                         }
                                   else
                                         {
                                         settext(TX_Nag_P�atno��, "");
                                         }
                                   }


                                   // je�li dok. wymaga podpisu (faktura) odbiorcy, odblokowujemy odp. pole

                                   switch( dp.RodzajDokumentu )
                                         {
                                         case FAKTURA_VAT:
                                         case FVAT_KOR:
                                         case FVAT_PAR:
                                         case RACHUNEK:
                                           _enable( ST_Nag_Odbiorca );
                                           _enable( BT_Nag_Odbiorca );

                                           if( *(char *)xget( TX_Nag_P�atno��, MUIA_Text_Contents ) == 0 )
                                                   set( Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, BT_Nag_P�atno�� );
                                           else
                                                   set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, BT_Nag_Odbiorca);

                                           break;

                                         default:
                                           _disable(BT_Nag_Odbiorca);
                                           break;
                                         }
                                   }
                           }
                           break;



                        // sposob platnosci
                        case ID_SELECT:
                           {
                           struct P�atno��List *p�atno�� = P�atno��Selector( NULL );

                           if( p�atno�� )
                                 {
                                 struct DateStamp *datestamp;

                                 strncpy(dp.TypP�atno�ci, p�atno��->pl_p�atno��.Nazwa, PAY_NAME_LEN);
                                 settext(TX_Nag_P�atno��, p�atno��->pl_p�atno��.Nazwa);

                                 if(datestamp = wfmh_Str2Date((char *)xget(TX_Nag_Data_Wyst, MUIA_Text_Contents)))
                                   {
                                   if(p�atno��->pl_p�atno��.Got�wka == FALSE)
                                         datestamp->ds_Days += p�atno��->pl_p�atno��.Zw�oka;
                                   setstring(ST_Nag_Data_P�at, wfmh_Date2Str(datestamp));

                                   set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, BT_Nag_Odbiorca);
                                   }
                                 else
                                   {
                                   DisplayBeep(0);
                                   }
                                 }
                           }
                           break;



                        case ID_FAK_KLIENT_ODBIORCA:
                           {
                           char *odb = OdbiorcaSelector(&dp);

                           if(odb)
                                   {
                                   strncpy(dp.OsobaOdbieraj�ca, odb, USER_NAME_LEN);
                                   setstring(ST_Nag_Odbiorca, odb);

                                   set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, BT_Nag_Ok);
                                   }
                           }
                           break;





                        /* Ko�czymy edycj�... */

                        case ID_OK:
                           {
                           struct DateStamp *datestamp;
                           struct Klient *klient;

                           copystr(dp.NumerParagonu, ST_Nag_Paragon);

                           if(dp.Odbiorca == NULL)
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_NO_CUST);
                                  set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, BT_Nag_Wybierz);
                                  break;
                                  }
                           klient = &dp.Odbiorca->kl_klient;


                           if(klient->Nazwa1[0] == '\0')
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_NO_NAME);
                                  set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_Nazwa_1);
                                  break;
                                  }

                           if(klient->Ulica[0] == '\0')
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_NO_NAME);
                                  set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_Ulica);
                                  break;
                                  }

/*
                           if(klient->Kod[0] == '\0')
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_NO_NAME);
                                  set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_Kod);
                                  break;
                                  }
*/

                           if(klient->Miasto[0] == '\0')
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_NO_NAME);
                                  set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_Miasto);
                                  break;
                                  }


                           // spr. czy jest nip lub regon
                           if((klient->NIP[0] == '\0') && (klient->Regon[0] == '\0'))
                                  {
                                  if( NIPRequired )
                                        {
                                        DisplayBeep(0);
                                        MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_NO_IDENT);
                                        set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, BT_Nag_Wybierz);
                                        break;
/*
                                        if(MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_FAK_NO_IDENT_GAD, MSG_FAK_NO_IDENT))
                                           {
                                           Prze��czNaRachunek = TRUE;
                                           }
                                        else
                                           {
                                           set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_NIP);
                                           break;
                                           }
*/
                                        }
                                  }
                           else
                                  {
                                  if(klient->NIP[0] != '\0')
                                        {
                                        if(strlen(klient->NIP) != 10)
                                          {
                                          DisplayBeep(0);
                                          MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_WRONG_IDENT, MSG_FAK_WRONG_NIP);
                                          set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_NIP);
                                          break;
                                          }
                                        }

                                  if(klient->Regon[0] != '\0')
                                        {
                                        if(strlen(klient->Regon) != 9)
                                          {
                                          DisplayBeep(0);
                                          MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_WRONG_IDENT, MSG_FAK_WRONG_REGON);
                                          set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_Regon);
                                          break;
                                          }
                                        }
                                  }


                                  /* PARAGON */

                                  if((dp.RodzajDokumentu == FVAT_PAR) || (dp.RodzajDokumentu == RACH_PAR))
                                         {
                                         if(dp.NumerParagonu[0] == '\0')
                                                {
                                                DisplayBeep(0);
                                                MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_NO_PAR);
                                                set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_Paragon);
                                                break;
                                                }
                                         }


                                  /* DATA */

                                  if(!(datestamp = wfmh_Str2Date((char *)xget(ST_Nag_Data_Sprz, MUIA_String_Contents))))
                                        {
                                        DisplayBeep(0);
                                        set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_Data_Sprz);
                                        break;
                                        }
                                  else
                                        memcpy(&dp.DataSprzeda�y, datestamp, sizeof(struct DateStamp));


                                  datestamp = wfmh_Str2Date((char *)xget(TX_Nag_Data_Wyst, MUIA_Text_Contents));
                                  memcpy(&dp.DataWystawienia, datestamp, sizeof(struct DateStamp));


                                  if(!(datestamp = wfmh_Str2Date((char *)xget(ST_Nag_Data_P�at, MUIA_String_Contents))))
                                        {
                                        DisplayBeep(0);
                                        set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_Data_P�at);
                                        break;
                                        }
                                  else
                                        memcpy(&dp.DataP�atno�ci, datestamp, sizeof(struct DateStamp));

                                  if(CompareDates(&dp.DataWystawienia, &dp.DataSprzeda�y) >0)
                                        {
                                        DisplayBeep(0);
                                        MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_ERR_DATA_WYST);
                                        set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_Data_Sprz);
                                        break;
                                        }

                                  if( !strlen(gettext(TX_Nag_P�atno��)) )
                                        {
                                        DisplayBeep(0);
                                        MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_ERR_PAY);
                                        set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, BT_Nag_P�atno��);
                                        break;
                                        }

                                  if(CompareDates(&dp.DataWystawienia, &dp.DataP�atno�ci) <0)
                                        {
                                        DisplayBeep(0);
                                        MUI_Request(app, Nag��wekDokumentuWindow, 0, TITLE, MSG_OK, MSG_FAK_ERR_DATA_PLAT);
                                        set(Nag��wekDokumentuWindow, MUIA_Window_ActiveObject, ST_Nag_Data_P�at);
                                        break;
                                        }



// wywalic strcpy!!!
//               strcpy(dp.TypP�atno�ci, gettext(TX_Nag_P�atno��));
                           memcpy(main_dp, &dp, sizeof(struct DocumentPrint));

                           result  = TRUE;
                           running = FALSE;
                           }
                           break;


                        case ID_CANCEL:
                           running = FALSE;
                           break;
                        }


                  if(running && signal) Wait(signal);
                  }
           }
        else
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           }



        _detachwin(Nag��wekDokumentuWindow);
        _sleep(FALSE);

        return(result);
}
//|

/// ZbudujNumerDokumentu

void ZbudujNumerDokumentu(struct DocumentPrint *dp)
{
char rodzaj;
long numer   = 0;

int  miesiac           = dp->Miesiac;
int  rok               = dp->Rok;
char DokumentBezNumeru = FALSE;    // czy to my numerujemy czy nie (np. drukarka fiskalna)

        sprintf(dp->Nag��wekDokumentu     , "%s ", RodzajeDokument�wTable[dp->RodzajDokumentu]);
        sprintf(dp->Nag��wekDokumentuSpool, "%s ", RodzajeDokument�wSpoolTable[dp->RodzajDokumentu]);

        switch(dp->RodzajDokumentu)
           {
           case FAKTURA_VAT:
                   rodzaj = settings.order_fvat;
                   numer = docs.numer_fvat;
                   break;

           case RACHUNEK:
                   rodzaj = settings.order_rach;
                   numer = docs.numer_rach;
                   break;

           case FVAT_KOR:
                   rodzaj = settings.order_fvat_kor;
                   numer = docs.numer_fvat_kor;
                   break;

           case FVAT_PAR:
                   rodzaj = settings.order_fvat_par;
                   numer = docs.numer_fvat_par;
                   break;

           case RACH_KOR:
                   rodzaj = settings.order_rach_kor;
                   numer = docs.numer_rach_kor;
                   break;

           case RACH_PAR:
                   rodzaj = settings.order_rach_par;
                   numer = docs.numer_rach_par;
                   break;

           case PARAGON:
                   DokumentBezNumeru = TRUE;
                   numer = docs.numer_paragon;
                   break;

           case ORDER:
                   rodzaj = settings.order_order;
                   numer = docs.numer_order;
                   break;

           case FVAT_PROFORMA:
                   DokumentBezNumeru = TRUE;
                   numer = docs.numer_proforma;
                   break;

           case ODRECZNA:
                   DokumentBezNumeru = TRUE;
                   numer = docs.numer_odreczna;
                   break;
           }


        if(DokumentBezNumeru)
           {
           switch(dp->RodzajDokumentu)
                   {
                   case PARAGON:
                   case FVAT_PROFORMA:
                   case ODRECZNA:
                         sprintf(dp->NumerDokumentu, "%ld", numer);
                         sprintf(dp->NumerDokumentuSpool, "%ld", numer);
                         break;
                   }
           }
        else
           {
           switch(rodzaj)
                   {
                   case NR_RRRR:
                                sprintf(dp->NumerDokumentu, "%ld/%ld", numer, rok);
                                sprintf(dp->NumerDokumentuSpool, "%ld\\%ld", numer, rok);
                                break;

                   case NR_RR:
                                sprintf(dp->NumerDokumentu, "%ld/%ld", numer, rok-((rok/100)*100));
                                sprintf(dp->NumerDokumentuSpool, "%ld\\%ld", numer, rok-((rok/100)*100));
                                break;

                   case NR_MM_RRRR:
                                sprintf(dp->NumerDokumentu, "%ld/%02ld/%ld", numer, miesiac, rok);
                                sprintf(dp->NumerDokumentuSpool, "%ld\\%02ld\\%ld", numer, miesiac, rok);
                                break;

                   case NR_MM_RR:
                                sprintf(dp->NumerDokumentu, "%ld/%02ld/%ld", numer, miesiac, rok-((rok/100)*100));
                                sprintf(dp->NumerDokumentuSpool, "%ld\\%02ld\\%ld", numer, miesiac, rok-((rok/100)*100));
                                break;

                   case RRRR_NR:
                                sprintf(dp->NumerDokumentu, "%ld/%ld", rok, numer);
                                sprintf(dp->NumerDokumentuSpool, "%ld\\%ld", rok, numer);
                                break;

                   case RR_NR:
                                sprintf(dp->NumerDokumentu, "%ld/%ld", rok-((rok/100)*100), numer);
                                sprintf(dp->NumerDokumentuSpool, "%ld\\%ld", rok-((rok/100)*100), numer);
                                break;

                   case RRRR_MM_NR:
                                sprintf(dp->NumerDokumentu, "%ld/%02ld/%ld", rok, miesiac, numer);
                                sprintf(dp->NumerDokumentuSpool, "%ld\\%02ld\\%ld", rok, miesiac, numer);
                                break;

                   case RR_MM_NR:
                                sprintf(dp->NumerDokumentu, "%ld/%02ld/%ld", rok-((rok/100)*100), miesiac, numer);
                                sprintf(dp->NumerDokumentuSpool, "%ld\\%02ld\\%ld", rok-((rok/100)*100), miesiac, numer);
                                break;
                   }
           }


   dp->NumerEgzemplarza = numer;

   strcat(dp->Nag��wekDokumentu     , dp->NumerDokumentu);
   strcat(dp->Nag��wekDokumentuSpool, dp->NumerDokumentuSpool);

}
//|
/// Zwi�kszNumeracj�

void Zwi�kszNumeracj�(struct DocumentPrint *dp, struct DrukowanieRequest *dr)
{

        switch(dp->RodzajDokumentu)
           {
           case FAKTURA_VAT:
                        docs.numer_fvat++;
                        DateStamp(&docs.data_fvat);
                        if(!dr->BezParagonu && dr->DrukujParagon)
                           {
                           docs.numer_paragon++;
                           memcpy(&docs.data_paragon, &docs.data_fvat, sizeof(struct DateStamp));
                           }
                        memcpy(&sys.last_operation, &docs.data_fvat, sizeof(struct DateStamp));
                        break;

           case RACHUNEK:
                        docs.numer_rach++;
                        DateStamp(&docs.data_rach);
                        if(!dr->BezParagonu && dr->DrukujParagon)
                           {
                           docs.numer_paragon++;
                           memcpy(&docs.data_paragon, &docs.data_rach, sizeof(struct DateStamp));
                           }
                        memcpy(&sys.last_operation, &docs.data_rach, sizeof(struct DateStamp));
                        break;

           case FVAT_KOR:
                        docs.numer_fvat_kor++;
                        DateStamp(&docs.data_fvat_kor);
                        memcpy(&sys.last_operation, &docs.data_fvat_kor, sizeof(struct DateStamp));
                        break;

           case RACH_KOR:
                        docs.numer_rach_kor++;
                        DateStamp(&docs.data_rach_kor);
                        memcpy(&sys.last_operation, &docs.data_rach_kor, sizeof(struct DateStamp));
                        break;

           case FVAT_PAR:
                        docs.numer_fvat_par++;
                        DateStamp(&docs.data_fvat_par);
                        memcpy(&sys.last_operation, &docs.data_fvat_par, sizeof(struct DateStamp));
                        break;

           case RACH_PAR:
                        docs.numer_rach_par++;
                        DateStamp(&docs.data_rach_par);
                        memcpy(&sys.last_operation, &docs.data_rach_par, sizeof(struct DateStamp));
                        break;

           case PARAGON:
//            if(!dr->BezParagonu && dr->DrukujParagon)
                        if( !dr->BezParagonu )
                           {
                           docs.numer_paragon++;
                           DateStamp(&docs.data_paragon);
                           memcpy(&sys.last_operation, &docs.data_paragon, sizeof(struct DateStamp));
                           }
                        break;

           case ORDER:
                        docs.numer_order++;
                        DateStamp(&docs.data_order);
                        memcpy(&sys.last_operation, &docs.data_order, sizeof(struct DateStamp));
                        break;

           case FVAT_PROFORMA:
                        docs.numer_proforma++;
                        DateStamp(&docs.data_proforma);
                        memcpy(&sys.last_operation, &docs.data_proforma, sizeof(struct DateStamp));
                        break;

           case ODRECZNA:
                        docs.numer_odreczna++;
                        DateStamp(&docs.data_odreczna);
                        memcpy(&sys.last_operation, &docs.data_odreczna, sizeof(struct DateStamp));
                        break;
           }
}

//|

/// Sprzeda�

#define FAK_ZMIANY(x)  {ZmianyNaFakturze += x; set(BT_Fak_Ok, MUIA_Disabled, (ZmianyNaFakturze == 0)); set(BT_Fak_Usu�, MUIA_Disabled, (ZmianyNaFakturze == 0)); set(BT_Fak_Edit, MUIA_Disabled, (ZmianyNaFakturze == 0)); set(BT_Fak_Od���, MUIA_Disabled, (ZmianyNaFakturze == 0)); }

char Sprzeda�(char RodzajDokumentu, char *filename)
{
struct DocumentPrint dp={0};
struct StringRequest paragon_req = {0};       // je�li dokument jest do paragonu, tutaj jest
                                                                                          // numer paragonu wpisany przez u�ytkownika

char   running                       = TRUE;
ULONG  signal                        = 0;
char   result                        = FALSE;
long   ZmianyNaFakturze              = 0;
char   data_str[]                    = "00.00.0000";
//struct DateStamp datestamp;
double Total = 0;                             // ��cznie brutto do zap�acenia
char   Prze��czNaRachunek = FALSE;            // czy FVAT prze��czamy przed wydrukiem
                                                                                          // na rachunek upr. (ze wzgl. na zbyt
                                                                                          // ma�� kwot� sprzeda�y)?
char freehandonly = FALSE;                    // czy mo�na wystawia� tylko z r�ki (brak magazynu etc)?
char bezmagazynu  = FALSE;                    // czy sprzeda� ma zmniejsza� stan magazynowy
                                                                                          // (potrzebne do dokument�w do paragon�w)
char kontynuacja_dokumentu = FALSE;           // czy kontynuujemy wystawianie od�o�onego dokumentu?
char wystawiamy = TRUE;                       // czy wystawiamy dokument? jesli FALSE, przerywamy operacje -
                                                                                          //  uzywane przy wykrywaniu duplikatow dokumentow i innych
                                                                                          //  tego typu warunkow poczatkowych
char bezodbiorcy = FALSE;                     // czy dokument ma odbiorce?

        _sleep(TRUE);


        // tworzymy okno sprzeda�y

        if( ! Sprzeda�Setup() )
           {
           DisplayBeep(0);
           MUI_Request( app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW );
           _sleep(FALSE);
           return(NULL);
           }

        dp.lv_object = LV_Fak_FakturaProdukty;


        // wczytujemy dokument od�o�ony je�li potrzeba...

        if( filename != NULL )
           {
           // nie sprawdzamy czy sie uda�o!

           dp.Odbiorca = malloc( sizeof( struct KlientList ));
           WczytajDokument(&dp, filename);

           kontynuacja_dokumentu = TRUE;
           }
        else
           {
           dp.RodzajDokumentu = RodzajDokumentu;
           }

        strncpy( dp.OsobaWystawiaj�ca, logged_user->user.Nazwa, USER_NAME_LEN );


        // czy dokument ma zaczepia� ma stany magazynowe?
        switch(dp.RodzajDokumentu)
           {
           case ORDER:
           case FVAT_PROFORMA:
                   bezmagazynu = TRUE;
                   break;
           }



        // sprawdzamy czy jest co� w magazynie etc.
        if(IsListEmpty(&grupy))
           {
           DisplayBeep(0);

           if(MUI_Request(app, MainWindow, 0, TITLE, MSG_CONT_CANCEL, MSG_ERR_NO_STOCK)==0)
                 {
                 set(MainWindow, MUIA_Window_ActiveObject, BT_ToolBar_Magazyn);
                 _sleep(FALSE);
                 return(FALSE);
                 }
           else
                 {
                 freehandonly = TRUE;
                 }
           }


         DoMethod(BT_ToolBar_Login, MUIM_MultiSet, MUIA_Disabled, freehandonly,
                                   BT_Fak_Grupy,
                                   LV_Fak_Produkty,
                                   ST_Fak_Wybierz,   ST_Fak_Ilo��,
                                   ST_Fak_Rabat,
                                   BT_Fak_Akceptuj�, BT_Fak_Nast�pny,
                                   BT_Fak_Znajd�,
                                   BT_Fak_Filter,    BT_Fak_FilterAll,
                                   NULL);





        // ustawienie grup magazynowych
        {
        struct GrupaList *node = NULL;

        if(!IsListEmpty(&grupy))
           node = (struct GrupaList *)grupy.lh_Head;

        UstawGrup�Aktywn�Vat(node, TRUE);
        }


        // czy dokument wymaga odbiorcy?
        switch( dp.RodzajDokumentu )
           {
           case PARAGON:
           case ODRECZNA:
                   _disable( BT_Fak_Odbiorca );
                   bezodbiorcy = TRUE;
                   break;

           default:
                   DoMethod(FakturaWindow, MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FAK_ODBIORCA);
                   break;
           }



        // aktualny miesiac i rok
        {
        struct DateStamp *datestamp = wfmh_GetCurrentTime();

        sscanf( wfmh_Date2StrFmt( "%m %Y", datestamp), "%ld %ld", &dp.Miesiac , &dp.Rok  );

        memcpy(&dp.DataSprzeda�y  , datestamp, sizeof(struct DateStamp));
        memcpy(&dp.DataP�atno�ci  , datestamp, sizeof(struct DateStamp));
        memcpy(&dp.DataWystawienia, datestamp, sizeof(struct DateStamp));
        }



        // Setting up

        setstring(ST_Fak_Ilo��  , "");
        setstring(ST_Fak_Rabat  , "");

        if( kontynuacja_dokumentu == FALSE )
           {
           set(TX_Fak_Odbiorca, MUIA_Text_Contents, "");
           set(TX_Fak_Total  , MUIA_Text_Contents, Price2String(0.0));

           _disable(BT_Fak_Usu�);
           _disable(BT_Fak_Edit);
           _disable(BT_Fak_Ok);
           _disable(BT_Fak_Od���);
           }
        else
           {
           struct Klient *klient = &dp.Odbiorca->kl_klient;

           char buffer[CUST_ADRES_LEN + CUST_KOD_LEN + CUST_MIASTO_LEN + 1];
           int prod_count = xget(LV_Fak_FakturaProdukty, _MUIA_List_Entries);
           int i;
           double brutto_total = 0.00;
           struct ProductList *prod;


           // dane odbiorcy

           sprintf(buffer, "%s, %s, %s %s", klient->Nazwa1, klient->Ulica, klient->Kod, klient->Miasto);
           set(TX_Fak_Odbiorca, MUIA_Text_Contents, buffer);



           // liczymy aktualn� warto�� brutto oraz przywracamy RealIlo��

           for( i = 0; i<prod_count; i++)
                   {
                   double warto��;

                   DoMethod(LV_Fak_FakturaProdukty, _MUIM_List_GetEntry, i, &prod);
                   if(!prod)
                           break;

                   prod->RealIlo�� = prod->pl_prod.StanMin ;

                   warto�� = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
                   warto�� = prod->pl_prod.Ilo�� * (warto�� - CalcRabat(warto��, prod->pl_prod.Rabat));
                   Total += warto�� + CalcVat(warto��, prod->pl_prod.Vat);
                   }

           set(TX_Fak_Total  , MUIA_Text_Contents, Price2String(Total));

           FAK_ZMIANY( prod_count )
           }

        set(TX_Fak_Warto��, MUIA_Text_Contents, Price2String(0.0));


        set(FakturaWindow, MUIA_Window_DefaultObject, LV_Fak_Produkty);
        set(FakturaWindow, MUIA_Window_ActiveObject , LV_Fak_Produkty);


        WybierzPage  = 0;
        set(GR_Fak_Wybierz, MUIA_Group_ActivePage, WybierzPage);



        //  Nag��wek i numer dokumentu

        ZbudujNumerDokumentu(&dp);
        set(FakturaWindow, MUIA_Window_ScreenTitle, dp.Nag��wekDokumentu);
        set(FakturaWindow, MUIA_Window_Title      , dp.Nag��wekDokumentu);


        //  sprawdzamy dokument o takim numerze czasem nie byl juz wystawiany
        switch( dp.RodzajDokumentu )
          {
          // tych nie sprawdzamy
          case FVAT_PROFORMA:
                break;

          // reszte i owszem
          default:
                if( DocumentExists( &dp ) )
                  {
                  // byl...

                  DisplayBeep(0);
                  if( !MUI_Request(app, MainWindow, 0, TITLE, MSG_ERR_DOC_EXISTS_GAD, MSG_ERR_DOC_EXISTS, dp.Nag��wekDokumentu ))
                          wystawiamy = FALSE;
                  }
                break;
          }


        // jadziem?
        if( wystawiamy )
          {
          if(WinOpen(FakturaWindow))
                 {
                 // otwieramy automatycznie okno wyboru kontrahenta
                 // (o ile uzytkownik nie zabronil, lub nie jest to
                 // kontynuacja wystawiania odlozonego dokumentu
                 if( (!filename) && (!wfmh_isenv("Golem/ManualCustomerSelector")) && (!bezodbiorcy) )
                   {
                   DoMethod(app, MUIM_Application_PushMethod, app,
                           2, MUIM_Application_ReturnID, ID_FAK_ODBIORCA );
                   }


                 while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);
                  switch(ID)
                         {
///            case ID_FIRST_GROUP / LAST / NEXT / PREV
                        case ID_FIRST_GROUP:
                           if(grupa_aktywna == NULL) break;

                           if(grupa_aktywna != (struct GrupaList *)grupy.lh_Head)
                                 {
                                 UstawGrup�Aktywn�Vat((struct GrupaList *)grupy.lh_Head, FALSE);
                                 }
                           break;

                        case ID_PREV_GROUP:
                           if(grupa_aktywna == NULL) break;

                           if(grupa_aktywna->gm_node.mln_Pred !=  (struct GrupaList *)(&grupy))
                                 {
                                 UstawGrup�Aktywn�Vat((struct GrupaList *)grupa_aktywna->gm_node.mln_Pred, FALSE);
                                 }
                           break;

                        case ID_NEXT_GROUP:
                           if(grupa_aktywna == NULL) break;

                           if(grupa_aktywna->gm_node.mln_Succ != (struct GrupaList *)&grupy.lh_Tail)
                                 {
                                 UstawGrup�Aktywn�Vat((struct GrupaList *)grupa_aktywna->gm_node.mln_Succ, FALSE);
                                 }
                           break;

/*
                        case ID_LAST_GROUP:
                           if(grupa_aktywna == NULL) break;

                           if(grupa_aktywna != (struct GrupaList *)grupy.lh_Tail)
                                 {
                                 UstawGrup�Aktywn�Vat((struct GrupaList *)grupy.lh_Tail, FALSE);
                                 }
                           break;
*/
//|

///            case ID_MAG_ZMIE�_GRUP�:

                        case ID_MAG_ZMIE�_GRUP�:
                           {
                           struct GrupaList *grupa = GroupSelector( grupa_aktywna );

                           if( grupa )
                                   UstawGrup�Aktywn�Vat( grupa, FALSE );
                           }
                           break;
//|

///            case ID_ACTIVATE:
                        case ID_ACTIVATE:
                           {
                           if( WybierzPage == 0)
                                 set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_Wybierz);
                           else
                                 set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_WybierzIndex);
                           }
                           break;
//|

///            case ID_MAG_WYBIERZ / WYBIERZ_INDEX:

                        case ID_MAG_WYBIERZ:
                           {
                           struct ProductList *prod;
                           char   *str = (char *)xget(ST_Fak_Wybierz, MUIA_String_Contents);
                           int    i;

                           if(!strlen(str))
                                   {
                                   set(LV_Fak_Produkty, _MUIA_List_Active, _MUIV_List_Active_Top);
                                   break;
                                   }

                           for(i = 0; ; i++)
                                 {
                                 DoMethod(LV_Fak_Produkty, _MUIM_List_GetEntry, i, &prod);
                                 if(!prod)
                                   break;

                                   if(StrnCmp(MyLocale, prod->pl_prod.Nazwa, str, -1, SC_COLLATE2) >= 0)
                                         {
                                         set(LV_Fak_Produkty, _MUIA_List_Active, i);
                                         break;
                                         }
                                 }
                           }
                           break;



                        case ID_MAG_WYBIERZ_INDEX:
                           {
                           struct ProductList *prod;
                           char   *str = (char *)getstr(ST_Fak_WybierzIndex);
                           int    i;

                           if(!strlen(str))
                                   {
                                   set(LV_Fak_Produkty, _MUIA_List_Active, _MUIV_List_Active_Top);
                                   break;
                                   }

                           for(i = 0; ; i++)
                                 {
                                 DoMethod(LV_Fak_Produkty, _MUIM_List_GetEntry, i, &prod);

                                 if(!prod)
                                   break;

                                 if(StrnCmp(MyLocale, prod->pl_prod.Index, str, -1, SC_COLLATE2) >= 0)
                                   {
                                   set(LV_Fak_Produkty, _MUIA_List_Active, i);
                                   break;
                                   }
                                 }
                           }
                           break;
//|


                        // przechodzi do nastepnego elementu na liscie ktory zawiera
                        // string wpisany w ST_Fak_WybierzXXX
///            case ID_NEXT:
                        case ID_NEXT:
                           {
                           char   temp_prod_name[PROD_NAME_LEN * 2] = "";

                           if(grupa_aktywna == NULL) break;

                           if( WybierzPage == 0)
                                   {
                                   if(StrnCmp(MyLocale, temp_prod_name, getstr(ST_Fak_Wybierz), -1, SC_COLLATE2) == 0)
                                         {
                                         Znajd�ProduktNaLi�cie(getstr(ST_Fak_Wybierz), LV_Fak_Produkty, xget(LV_Fak_Produkty, _MUIA_List_Active)+1);
                                         }
                                   else
                                         {
                                         strcpy(temp_prod_name, (char *)xget(ST_Fak_Wybierz, MUIA_String_Contents));

                                         // szukamy od akt. pozycji...
                                         if(Znajd�ProduktNaLi�cie(temp_prod_name, LV_Fak_Produkty, 0) == -1)
                                           {
                                           // a jak nic nie ma, to ponownie od poczatku do akt. pozycji
                                           // coby cala liste przeszukac
                                           Znajd�ProduktNaLi�cie( getstr(ST_Fak_Wybierz), LV_Fak_Produkty, xget(LV_Fak_Produkty, _MUIA_List_Active)+1 );
                                           }
                                         }
                                   }
                           else
                                   {
                                   if(StrnCmp(MyLocale, temp_prod_name, getstr(ST_Fak_WybierzIndex), -1, SC_COLLATE2) == 0)
                                         {
                                         Znajd�ProduktNaLi�cieIndex( getstr(ST_Fak_WybierzIndex), LV_Fak_Produkty, xget(LV_Fak_Produkty, _MUIA_List_Active)+1);
                                         }
                                   else
                                         {
                                         strcpy( temp_prod_name, getstr(ST_Fak_WybierzIndex) );
                                         if(Znajd�ProduktNaLi�cieIndex(temp_prod_name, LV_Fak_Produkty, 0) == -1)
                                                 Znajd�ProduktNaLi�cieIndex( getstr(ST_Fak_WybierzIndex), LV_Fak_Produkty, xget(LV_Fak_Produkty, _MUIA_List_Active)+1 );
                                         }
                                   }
                           }
                           break;
//|

///            case ID_MAG_ZNAJD�:

                        case ID_MAG_ZNAJD�:
                           {
                           char *pattern;
                           char IndexScan = FALSE;
                           struct MagScan *ms;

                           if(grupa_aktywna == NULL) break;


                           if(WybierzPage == 0)
                                   {
                                   pattern = getstr(ST_Fak_Wybierz);
                                   }
                           else
                                   {
                                   pattern = getstr(ST_Fak_WybierzIndex);
                                   IndexScan = TRUE;
                                   }

                           ms = MagazynScan(pattern, IndexScan);

                           if(ms)
                                   {
                                   struct ProductList *prod = ms->prod;

                                   if(ms->grupa != grupa_aktywna)
                                           {
                                           UstawGrup�Aktywn�Vat(ms->grupa, FALSE);

                                           if(prod)
                                                   {
                                                   set(LV_Fak_Produkty, _MUIA_List_Active, Znajd�ProduktListaAdres(LV_Fak_Produkty, prod));
                                                   }
                                           }

                                   set(FakturaWindow, MUIA_Window_ActiveObject, LV_Fak_Produkty);
                                   }
                           }
                           break;
//|

///            case ID_MAG_FILTER / FILTER_ALL

                        case ID_MAG_FILTER:
                           FiltrProdukty(LV_Fak_Produkty, &fak_filter);
                           break;

                        case ID_MAG_FILTER_ALL:
                           fak_filter.Aktywny = FALSE;
                           ApplyMagFilter(LV_Fak_Produkty, grupa_aktywna, &fak_filter, FALSE);
                           break;
//|

///            case ID_FAK_PROD_ACK / ACK_INDEX / ACK_LV:
                        case ID_FAK_PROD_ACK:
                        case ID_FAK_PROD_ACK_INDEX:
                        case ID_FAK_PROD_ACK_LV:
                           if(xget(LV_Fak_Produkty, _MUIA_List_Active) >= 0)
                                   {
                                   struct ProductList *prod;

                                   DoMethod(LV_Fak_Produkty, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &prod);

                                   if(prod)
                                           {
                                           double rabat = 0.00;

                                           nnset( ST_Fak_Wybierz, MUIA_String_Contents, (char *)prod->pl_prod.Nazwa );
                                           nnset( ST_Fak_WybierzIndex, MUIA_String_Contents, (char *)prod->pl_prod.Index );

                                           nnset( ST_Fak_Ilo��, MUIA_String_Contents, Price2String( 1.00 ) );
                                           if( dp.Odbiorca )
                                                  rabat = dp.Odbiorca->kl_klient.def_Rabat;
                                           nnset( ST_Fak_Rabat, MUIA_String_Contents, Price2String(rabat) );
                                           }

                                   if(ID == ID_FAK_PROD_ACK_LV)
                                           {
                                           if(WybierzPage == 0)
                                                   set( FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_Wybierz );
                                           else
                                                   set( FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_WybierzIndex );
                                           }
                                   else
                                           {
                                           set( FakturaWindow, MUIA_Window_ActiveObject, MUIV_Window_ActiveObject_None );
                                           set( FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_Ilo�� );
                                           }
                                   }
                           break;
//|

///            case ID_FAK_CALC_WARTO��:
                   case ID_FAK_CALC_WARTO��:
                   case ID_FAK_CALC_WARTO��_2:
                           {
                           char   *towar = (char *)xget(ST_Fak_Wybierz, MUIA_String_Contents);
                           double ilo�� = String2Price((char *)xget(ST_Fak_Ilo��, MUIA_String_Contents));
                           double rabat;
                           double warto��;
                           struct ProductList *prod;

                           if( ID == ID_FAK_CALC_WARTO�� )
                                   {
                                   if( dp.Odbiorca )
                                          {
                                          rabat = dp.Odbiorca->kl_klient.def_Rabat;
                                          nnset(ST_Fak_Rabat, MUIA_String_Contents, Price2String(rabat));
                                          }
                                   }

                           rabat = String2Price((char *)xget(ST_Fak_Rabat, MUIA_String_Contents));
                           nnset(ST_Fak_Rabat, MUIA_String_Contents, Price2String(rabat));


                           // czy ilo�� podana? Je�li pusty gad�et, to robimy 1
                           if( *getstr(ST_Fak_Ilo��) == '\0' )
                                   ilo�� = 1;

                           set(ST_Fak_Ilo��, MUIA_String_Contents, Price2String(ilo��));


                           /* Czy rabat sensowny? */
                           if((rabat > 100.0) || (rabat < 0.0))
                                   {
                                   MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_WRONG_DISCOUNT_GAD, MSG_FAK_WRONG_DISCOUNT);
                                   set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_Rabat);
                                   break;
                                   }


                           /* Czy produkt istnieje? */
                           if(!(prod = Znajd�ProduktNazwa(towar)))
                                   {
                                   MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_WRONG_PROD_GAD, MSG_FAK_WRONG_PROD);
                                   set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_Wybierz);
                                   break;
                                   }


                           warto�� = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
                           warto�� = ilo�� * ( warto�� - CalcRabat(warto��, rabat) );

                           set(TX_Fak_Warto��, MUIA_Text_Contents, Price2String(warto��));
                           }
                           break;
//|

///            case ID_ACCEPT:

                        // produkt zaakceptowany -> dodajemy do dokumentu

                        case ID_ACCEPT:
                           {
                           char   *towar = getstr(ST_Fak_Wybierz);
                           char   *index = getstr(ST_Fak_WybierzIndex);
                           double ilo�� = String2Price(getstr(ST_Fak_Ilo��));
                           double rabat = String2Price(getstr(ST_Fak_Rabat));
                           struct ProductList *prod;


                           // czy nazwa badz index podany?
                           if((*towar == 0) && (*index == 0))
                                   {
                                   DisplayBeep(0);

                                   if( WybierzPage == 0)
                                           set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_Wybierz);
                                   else
                                           set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_WybierzIndex);

                                   break;         
                                   }

/*
                           /* Czy index podany? */
                           if(*index == 0)
                                   {
                                   DisplayBeep(0);
                                   set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_WybierzIndex);
                                   break;
                                   }


                           /* Czy nazwa podana? */
                           if(*towar == '\0')
                                   {
                                   DisplayBeep(0);
                                   set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_Wybierz);
                                   break;
                                   }
*/


                           /* Czy produkt istnieje? - najpierw szukamy po indexie jesli takowy jest */

                           if(*index != 0)
                                   {
                                   if(!(prod = Znajd�ProduktIndex(index)))
                                           {
                                           if(!(prod = Znajd�ProduktIndexWMagazynie(index)))
                                                   {
                                                   MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_WRONG_PROD_GAD, MSG_FAK_WRONG_PROD);
                                                   set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_WybierzIndex);
                                                   break;
                                                   }
                                           }
                                   }
                           else
                                   {
                                   if(!(prod = Znajd�ProduktNazwa(towar)))
//                      if(!(prod = Znajd�ProduktNazwaWMagazynie(towar)))
                                           {
                                           MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_WRONG_PROD_GAD, MSG_FAK_WRONG_PROD);
                                           set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_Wybierz);
                                           break;
                                           }
                                   }

                           /* Czy ilo�� podana? */
                           if(ilo�� == 0)
                                   {
                                   DisplayBeep(0);
                                   set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_Ilo��);
                                   break;
                                   }

                           /* Ucinamy cz��� u�amkow�, je�li nie mo�na sprzedawa� po kawa�ku */
                           if(prod->pl_prod.Jedn.Ca�kowita == TRUE)
                                   {
                                   ilo�� = floor(ilo��);
                                   nnset(ST_Fak_Ilo��, MUIA_String_Contents, Price2String(ilo��));
                                   }


                           /* Czy jest tyle towaru ile trzeba w magazynie */

                           if( (bezmagazynu == FALSE) && (prod->pl_prod.Typ == TYP_TOWAR) )
                                 {
                                 if(ilo�� > prod->pl_prod.Ilo��)
                                   {
                                   char t1[20];
                                   char req_result;

                                   strcpy(t1, Price2String(prod->pl_prod.Ilo��));
                                   DisplayBeep(0);


                                   // ktos chce sprzedac za duzo...

                                   if( prod->pl_prod.Ilo�� >= 0 )
                                           req_result = MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_WRONG_AMOUNT_1_GAD, MSG_FAK_WRONG_AMOUNT_1, t1, Price2String(ilo��), t1);
                                   else
                                           req_result = MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_WRONG_AMOUNT_2_GAD, MSG_FAK_WRONG_AMOUNT_2, t1, Price2String(ilo��));


                                   switch( req_result )
                                           {
                                           case 1: // wpisana ilo��
                                                   break;

                                           case 2:
                                                   ilo�� = prod->pl_prod.Ilo��;
                                                   setstring( ST_Fak_Ilo��, Price2String(ilo��));
                                                   break;


                                           default:
                                                   set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_Ilo��);
                                                   continue;
                                           }
                                   }
                                 }

                           /* Czy rabat sensowny? */
                           if((rabat > 100) || (rabat <0))
                                   {
                                   MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_WRONG_DISCOUNT_GAD, MSG_FAK_WRONG_DISCOUNT);
                                   set(FakturaWindow, MUIA_Window_ActiveObject, ST_Fak_Rabat);
                                   break;
                                   }

                                 set(ST_Fak_Rabat, MUIA_String_Contents, Price2String(rabat));


                                 {
                                 struct ProductList fak_prod = {0};
                                 double warto��;

                                 memcpy( &fak_prod, prod, sizeof(struct ProductList) );

                                 fak_prod.pl_prod.Ilo�� = ilo��;
                                 fak_prod.RealIlo�� = ilo��;
                                 fak_prod.pl_prod.Rabat = rabat;
                                 fak_prod.Expanded = TRUE;


                                 // parsujemy nazwe via locale.library (jesli to jest usluga)

                                 if(fak_prod.pl_prod.Typ == TYP_USLUGA)
                                   {
                                   char parsed_name[ PROD_NAME_LEN * 5 ];

                                   if( wfmh_Date2StrFmtBuffer( parsed_name, prod->pl_prod.Nazwa, wfmh_GetCurrentTime() ) )
                                           {
                                           strncpy( fak_prod.pl_prod.Nazwa, parsed_name,  PROD_NAME_LEN );
                                           }
                                   else
                                           {
                                           DisplayBeep(0);
                                           D(bug("** Can't parse product name!\n"));
                                           }
                                   }


                                 DoMethod( LV_Fak_FakturaProdukty, _MUIM_List_InsertSingle, &fak_prod, _MUIV_List_Insert_Bottom );
                                 DoMethod( LV_Fak_FakturaProdukty, _MUIM_List_Jump, xget(LV_Fak_FakturaProdukty, _MUIA_List_Entries) );

                                 FAK_ZMIANY(1)

                                 // rodzaj grupy z jakiej wzieli�my produkt

                                 if(bezmagazynu == FALSE)
                                   {
                                   // je�li to towar, to warto zdj�� sztuki z magazynu

                                   if(fak_prod.pl_prod.Typ != TYP_USLUGA)
                                           {
                                           prod->pl_prod.Ilo�� -= ilo��;
//                       if(prod->pl_prod.Ilo�� < 0)
//                           prod->pl_prod.Ilo�� = 0;

                                           DoMethod(LV_Fak_Produkty, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                                           }
                                   }


                                 warto�� = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
                                 warto�� = ilo�� * (warto�� - CalcRabat(warto��, rabat));
                                 Total += warto�� + CalcVat(warto��, fak_prod.pl_prod.Vat);

                                 set(TX_Fak_Total, MUIA_Text_Contents, Price2String(Total));

                                 setstring(ST_Fak_Wybierz     , "");
                                 setstring(ST_Fak_WybierzIndex, "");
                                 setstring(ST_Fak_Ilo��       , "");
                                 setstring( ST_Fak_Rabat      , "" );

                                 set(TX_Fak_Warto��, MUIA_Text_Contents  , Price2String(0.00));
                                 set(FakturaWindow , MUIA_Window_ActiveObject, BT_Fak_Nast�pny);
                                 }
                           }
                           break;
//|

///            case ID_FAK_USU�:

                   // wycofujemy produkt z faktury

                   case ID_FAK_USU�:
                           {
                           if(xget(LV_Fak_FakturaProdukty, _MUIA_List_Active) != _MUIV_List_Active_Off)
                                  {
                                  struct ProductList *fak_prod;

                                  DoMethod(LV_Fak_FakturaProdukty, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &fak_prod);

                                  if(!(MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_REMOVE_GAD, MSG_FAK_REMOVE, fak_prod->pl_prod.Nazwa)))
                                        break;


                                  if(fak_prod)
                                        {
                                        struct ProductList *prod = NULL;
                                        double warto��;
                                        long pos;

                                        // wycofujemy towar do magazynu, tylko wtedy, gdy
                                        // nie by� sprzedany z r�ki, albo nie robimy
                                        // sprzeda�y do paragonu albo to jest us�uga

                                        if( (fak_prod->pl_prod.Typ == TYP_TOWAR) )
                                           {
                                           prod = (struct ProductList *)Znajd�ProduktNazwaWMagazynie( fak_prod->pl_prod.Nazwa );

                                           if(prod)
                                                 {
                                                 prod->pl_prod.Ilo�� += fak_prod->RealIlo��;
                                                 }
                                           else
                                                 {
                                                 DisplayBeep(0);
                                                 D(bug("** Faktura remove - can't find product\n"));
                                                 }
                                           }


                                        warto�� = (fak_prod->pl_prod.Zakup + fak_prod->pl_prod.Mar�a);
                                        warto�� = fak_prod->pl_prod.Ilo�� * (warto�� - CalcRabat(warto��, fak_prod->pl_prod.Rabat));
                                        Total -= warto�� + CalcVat(warto��, fak_prod->pl_prod.Vat);

                                        // ??? eek?
                                        if(Total == 0) Total = 0;

                                        set(TX_Fak_Total, MUIA_Text_Contents, Price2String(Total));

                                        if( (fak_prod->pl_prod.Typ == TYP_FREEHAND) || (fak_prod->pl_prod.Typ != TYP_USLUGA) )
                                           {
                                           pos = Znajd�ProduktNaLi�cie(prod->pl_prod.Nazwa, LV_Fak_Produkty, 0);
                                           if(pos >= 0)
                                                  DoMethod(LV_Fak_Produkty, _MUIM_List_Redraw, pos);
                                           }

                                        if(xget(LV_Fak_FakturaProdukty, _MUIA_List_Entries) == 0)
                                          set(FakturaWindow, MUIA_Window_ActiveObject, BT_Fak_Nast�pny);

                                         DoMethod(LV_Fak_FakturaProdukty, _MUIM_List_Remove, _MUIV_List_Remove_Active);
                                         FAK_ZMIANY(-1)
                                         }
                                  else
                                         {
                                         DisplayBeep(0);
                                         D(bug("Nie mog� znale�� produktu na dokumencie!?\n"));
                                         }

                                        set(FakturaWindow, MUIA_Window_ActiveObject, BT_Fak_Nast�pny);
                                        }
                                  else
                                        {
                                        DisplayBeep(0);
                                        }
                           }
                           break;
//|

///            case ID_FAK_EDIT:

                   /* edytujemy co mozna we wpisie na dokumencie */

                   case ID_FAK_EDIT:
                           {
                           if(xget(LV_Fak_FakturaProdukty, _MUIA_List_Active) != _MUIV_List_Active_Off)
                                  {
                                  struct ProductList *fak_prod;

                                  DoMethod( LV_Fak_FakturaProdukty, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &fak_prod );

                                  if( fak_prod )
                                        {
                                        double old_warto��;

                                        // odkladamy poprzednie wartosci pieniezne...

                                        old_warto�� = fak_prod->pl_prod.Zakup + fak_prod->pl_prod.Mar�a;
                                        old_warto�� = fak_prod->pl_prod.Ilo�� * (old_warto�� - CalcRabat(old_warto��, fak_prod->pl_prod.Rabat));
                                        old_warto�� = old_warto�� + CalcVat(old_warto��, fak_prod->pl_prod.Vat);


                                        // edytujmy...

                                        if( EdycjaProduktu( (struct ProductList *)fak_prod, FALSE ) != FALSE )
                                           {
                                           double warto��;

                                           // odejmujemy poprzednia wartosc elementu...

                                           Total = Total - old_warto��;

                                           // i dodajemy nowa...

                                           warto�� = fak_prod->pl_prod.Zakup + fak_prod->pl_prod.Mar�a;
                                           warto�� = fak_prod->pl_prod.Ilo�� * (warto�� - CalcRabat(warto��, fak_prod->pl_prod.Rabat));
                                           Total += warto�� + CalcVat(warto��, fak_prod->pl_prod.Vat);

                                           // refresz listy i innych tam...

                                           DoMethod( LV_Fak_FakturaProdukty, _MUIM_List_Redraw, _MUIV_List_Redraw_Active );
                                           set(TX_Fak_Total, MUIA_Text_Contents, Price2String( Total ));
                                           }
                                        }
                                  else
                                        {
                                        D(bug("** Can't edit fak_prod\n"));
                                        DisplayBeep(0);
                                        }
                                  }
                           else
                                  {
                                  DisplayBeep( 0 );
                                  }
                           }
                           break;
//|

///            case ID_FAK_FREE_HAND:

                   /* produkt z r�ki */

                   case ID_FAK_FREE_HAND:
                           {
                           struct ProductList fak_prod = {0};

                           char   *towar = getstr(ST_Fak_Wybierz);
                           char   *index = getstr(ST_Fak_WybierzIndex);
                           double ilo�� = String2Price((char *)xget(ST_Fak_Ilo��, MUIA_String_Contents));
                           double rabat = String2Price((char *)xget(ST_Fak_Rabat, MUIA_String_Contents));

                           /* Czy nazwa podana? */
                           if(*towar != '\0')
                                         strcpy(fak_prod.pl_prod.Nazwa, towar);

                           /* Czy index podany? */
                           if(*index != '\0')
                                         strcpy(fak_prod.pl_prod.Index, index);


                           /* Czy ilo�� podana? */
                           if(ilo�� != 0)
                                         fak_prod.pl_prod.Ilo�� = ilo��;

                           /* domy�lny VAT */
                           if(grupa_aktywna == NULL)
                                   {
                                   fak_prod.pl_prod.Vat = settings.def_vat;
                                   }
                           else
                                   {
                                   fak_prod.pl_prod.Vat = grupa_aktywna->gm_grupa.Vat;
                                   }

                           fak_prod.pl_prod.Typ = TYP_FREEHAND;
                           fak_prod.Expanded    = TRUE;


                           if( EdycjaProduktu( (struct ProductList *)&fak_prod, FALSE ) != FALSE )
                                   {
                                   double warto��;

                                   
//                   fak_prod.RealIlo�� = prod->pl_prod.Ilo��;
//                   fak_prod.pl_prod.Rabat = rabat;

                                   DoMethod(LV_Fak_FakturaProdukty, _MUIM_List_InsertSingle, &fak_prod, _MUIV_List_Insert_Bottom);
                                   FAK_ZMIANY(1)

                                   warto�� = fak_prod.pl_prod.Zakup + fak_prod.pl_prod.Mar�a;
                                   warto�� = fak_prod.pl_prod.Ilo�� * (warto�� - CalcRabat(warto��, fak_prod.pl_prod.Rabat));
                                   Total += warto�� + CalcVat(warto��, fak_prod.pl_prod.Vat);

                                   set(TX_Fak_Total, MUIA_Text_Contents, Price2String(Total));

                                   setstring(ST_Fak_Wybierz, "");
                                   setstring(ST_Fak_WybierzIndex, "");
                                   setstring(ST_Fak_Ilo��  , "");
                                   setstring(ST_Fak_Rabat  , "");
                                   set(TX_Fak_Warto��, MUIA_Text_Contents  , Price2String(0.00));

                                   set(FakturaWindow , MUIA_Window_ActiveObject, BT_Fak_Nast�pny);
                                   }
                           }
                           break;
//|

///            case ID_FAK_ODBIORCA:
                        case ID_FAK_ODBIORCA:
                           {
                           if(Nag��wekDokumentu(&dp))
                                   {
                                   char buffer[CUST_ADRES_LEN + CUST_KOD_LEN + CUST_MIASTO_LEN + 1];

                                   sprintf(buffer, "%s, %s, %s %s", dp.Odbiorca->kl_klient.Nazwa1, dp.Odbiorca->kl_klient.Ulica, dp.Odbiorca->kl_klient.Kod, dp.Odbiorca->kl_klient.Miasto);
                                   set(TX_Fak_Odbiorca, MUIA_Text_Contents, buffer);
                                   }

                           set(FakturaWindow , MUIA_Window_ActiveObject, BT_Fak_Nast�pny);
                           }
                           break;
//|

///            case ID_PAGE_0 / 1
                        // prze��czamy miedzy stringami NAzwa/Index
                        case ID_PAGE_0:
                           {
                           set(FakturaWindow , MUIA_Window_ActiveObject, MUIV_Window_ActiveObject_None);
                           set(GR_Fak_Wybierz, MUIA_Group_ActivePage, 0);
                           set(FakturaWindow , MUIA_Window_ActiveObject, ST_Fak_Wybierz);
                           WybierzPage = 0;
                           }
                           break;

                        case ID_PAGE_1:
                           {
                           if(xget(LV_Fak_Produkty, MUIA_ProdList_IndexCount) > 0)
                                   {
                                   set(FakturaWindow , MUIA_Window_ActiveObject, MUIV_Window_ActiveObject_None);
                                   set(GR_Fak_Wybierz, MUIA_Group_ActivePage, 1);
                                   set(FakturaWindow , MUIA_Window_ActiveObject, ST_Fak_WybierzIndex);
                                   WybierzPage = 1;
                                   }

                           else
                                   {
                                   DisplayBeep(0);
                                   }
                           }
                           break;
//|


                        /*                         */
                        /*    Ko�czymy edycj�...   */
                        /*                         */

///            case ID_OK:
                        case ID_OK:
                           {
                           struct DateStamp *datestamp;
                           struct PrinterList *printer;

                           int max_steps = 3;
                           struct DrukowanieRequest dr = {0};
                           char   ZawszeGot�wka = FALSE;


                           DateStamp( &dp.aktualna_data );


                           if(!ZmianyNaFakturze)
                                   break;

                           // czy potrzebny odbiorca?
                           if( !bezodbiorcy && (dp.Odbiorca == NULL) )
                                   {
                                   // tak
                                   DisplayBeep(0);
                                   MUI_Request(app, FakturaWindow, 0, TITLE, MSG_OK, MSG_FAK_NO_CUST);
                                   set(FakturaWindow, MUIA_Window_ActiveObject, BT_Fak_Odbiorca);
                                   break;
                                   }

/*
                           // sprawdzamy czy wart. sprzeda�y wystarczy na wystawienie faktury

                           if(dp.RodzajDokumentu == FAKTURA_VAT)
                                   {
                                   if(Total < settings.fvat_min)
                                         {
                                         char kwota1[20];
                                         char *kwota2;

                                         strcpy(kwota1, Price2String(settings.fvat_min));
                                         kwota2 = Price2String(Total);

                                         DisplayBeep(0);

                                         if(settings.sug_rach)
                                           {
                                           switch(MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_ERR_LOW_GAD, MSG_FAK_ERR_LOW, kwota1, kwota2))
                                                  {
                                                  case 1:
                                                          Prze��czNaRachunek = TRUE;
                                                          break;

                                                  case 2:
                                                          break;

                                                  default:
                                                          continue;
                                                  }
                                           }
/*
                                         else
                                           {
                                           MUI_Request(app, FakturaWindow, 0, TITLE, MSG_OK, MSG_FAK_ERR_LOW, kwota1, kwota2);
                                           break;
                                           }
*/
                                         }
                                   }
*/





                           switch(dp.RodzajDokumentu)
                                   {
                                   case FAKTURA_VAT:
                                   case RACHUNEK:
                                                dr.BezParagonu  = FALSE;
                                                dr.BezDokumentu = FALSE;
                                                dr.DrukujParagon = TRUE;
                                                break;

                                   case FVAT_KOR:
                                   case FVAT_PAR:
                                   case RACH_KOR:
                                   case RACH_PAR:
                                   case ORDER:
                                   case FVAT_PROFORMA:
                                                dr.BezParagonu  = TRUE;
                                                dr.BezDokumentu = FALSE;
                                                break;

                                   case PARAGON:
                                                dr.BezParagonu   = FALSE;
                                                dr.BezDokumentu  = TRUE;
                                                dr.DrukujParagon = TRUE;
                                                ZawszeGot�wka    = TRUE;
                                                break;

                                   case ODRECZNA:
                                                dr.BezParagonu = TRUE;
                                                dr.BezDokumentu = TRUE;
                                                dr.DrukujParagon = FALSE;
                                                ZawszeGot�wka = TRUE;
                                                break;
                                   }


                           // okno wp�ata/reszta

                           dp.do_zap�aty = ObetnijKo�c�wk�(Total);

                           if(dp.RodzajDokumentu != ORDER)
                                   {
                                   if(ZawszeGot�wka)
                                           {
                                           char res = Reszta(&dp);

                                           dp.reszta_aktywna = TRUE;

                                           if(res == FALSE)
                                                   {
                                                   dp.reszta_aktywna = FALSE;
                                                   }
                                           else
                                                   {
                                                   if(res == -1)
                                                           break;
                                                   }
                                           }
                                   else
                                           {
                                           if(Znajd�P�atno��Nazwa(dp.TypP�atno�ci)->pl_p�atno��.Got�wka)
                                                   {
                                                   if(dp.reszta_aktywna = settings.reszta_aktywna)
                                                           {
                                                           char res = Reszta(&dp);

                                                           if(res == FALSE)
                                                                  {
                                                                  dp.reszta_aktywna = FALSE;
                                                                  }
                                                          else
                                                                  {
                                                                  if(res == -1)
                                                                          break;
                                                                  }

                                                           }
                                                   }
                                           }
                                   }



                           // ile kopii (i czy ew. wraca� do edycji dokumentu)?

                           printer = MatchPrinterForDocument( dp.RodzajDokumentu );
                           dr.LiczbaKopii = printer->pl_printer.def_copies;

                           dr.RodzajDokumentu = dp.RodzajDokumentu;


                           if( !(dr.BezDokumentu && dr.BezParagonu) )
                                   {
                                   if(Drukowanie(&dr) == -1)
                                          break;
                                   }


                           set(app, MUIA_Application_Sleep, TRUE);

                           if(dr.BezDokumentu == FALSE)
                                   max_steps += dr.LiczbaKopii;
                           if((dr.BezParagonu == FALSE) && dr.DrukujParagon)
                                   max_steps++;

                           set(GA_Prog_Info, MUIA_Gauge_Current, 0);
                           set(GA_Prog_Info, MUIA_Gauge_Max, max_steps);
                           set(ProgressWindow, MUIA_Window_Open, TRUE);

                           if(!bezmagazynu)
                                   {
                                   settext(TX_Prog_Info, MSG_PROG_MAG_SAVE);

/*** TU ***/
                                   ZapiszMagazyn();
/*** TU ***/
                                   }
                           set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);

/*
                           // prze��czamy na rachunek uproszczony je�li trzeba
                           if(Prze��czNaRachunek)
                                   {
                                   dp.RodzajDokumentu = RACHUNEK;
                                   ZbudujNumerDokumentu(&dp);
                                   set(FakturaWindow, MUIA_Window_ScreenTitle, dp.Nag��wekDokumentu);
                                   set(FakturaWindow, MUIA_Window_Title      , dp.Nag��wekDokumentu);
                                   }
*/
//               if(dp.lv_object)
//                   PrzeliczDokument(&dp);


                           if((dr.BezDokumentu == FALSE) && (dr.LiczbaKopii > 0))
                                   {
                                   int i;

                                   settext(TX_Prog_Info, MSG_PROG_DOC_PRINT);

                                   for(i=1; i<=dr.LiczbaKopii; i++)
                                           {
                                           dp.NumerEgzemplarza = i;
                                           WydrukujDokument(&dp);
                                           set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);
                                           }
                                   }






                           // zapisujemy dokument (musimy przed drukowaniem paragonu, bo konwersja pl->maz jest in-place
                           ZapiszDokument(&dp, FALSE);
                           set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);


                           // drukujemy paragonik...

                           if(!dr.BezParagonu && dr.DrukujParagon)
                                   {
                                   // czy mozemy wydrukowac fiskalny?
                                   if( settings.posnet_active )
                                         {
                                         settext(TX_Prog_Info, MSG_PROG_POS_PRINT);
                                         WydrukujParagon(&dp);
                                         }
                                   else
                                         {
                                         // no to printujemy zwykly, z drukary
                                         }

                                   set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);
                                   }


                           // ... i otwieramy szufladk�

                           if(dp.RodzajDokumentu != ORDER)
                                   if(settings.posnet_active && settings.posnet_open_drawer)
                                          posnet_OpenDrawer();



                           // zwi�ksz numeracj� (numer dokumentu++, coby nast�pny by� ok)

                           Zwi�kszNumeracj�(&dp, &dr);

                           settext(TX_Prog_Info, MSG_PROG_DOC_SAVE);
                           ZapiszNumeracj�Dokument�w();
                           set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);



//             wywolujemy zdarzenie
//             !! zrobi� oddzielne dla ka�dego rodzaju dokumentu! !!

                           if(settings.com_call)
                                   {
                                   if(settings.com_sale[0] != '\0')
                                           {
                                           char termin_plat[] = "00.00.0000";
                                           char command[256];

                                           strcpy(termin_plat, wfmh_Date2Str(&dp.DataP�atno�ci));

                                           sprintf(command, "%s %s %s \"%s\" \"%s\" %s", settings.com_sale,
                                                                                                                                        wfmh_Date2Str(&dp.DataWystawienia),
                                                                                                                                        termin_plat,
                                                                                                                                        dp.Nag��wekDokumentu,
                                                                                                                                        dp.Odbiorca->kl_klient.Nazwa1,
                                                                                                                                        Price2String(Total));

                                           settext(TX_Prog_Info, MSG_PROG_COM_CALL);
                                           system(command);
                                           }
                                   }



                           // ... konczymy

                           set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);
                           set(ProgressWindow, MUIA_Window_Open, FALSE);

                           set(app, MUIA_Application_Sleep, FALSE);

                           result  = TRUE;
                           running = FALSE;
                           }
                           break;
//|

                        /*                         */
                        /*  Przerywamy edycj�...   */
                        /*                         */

///            case ID_CANCEL:
                        case ID_CANCEL:
                           {
                           if(ZmianyNaFakturze)
                                   {
                                   if(!MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_CANCEL_REQ_GAD, MSG_FAK_CANCEL_REQ))
                                         break;

                                   WczytajMagazyn();
                                   }

                           running = FALSE;
                           }
                           break;
//|

                        /*                         */
                        /*  Odk�adamy dokument...  */
                        /*                         */

///            case ID_FAK_OD���:
                        case ID_FAK_OD���:
                           {
                           if(ZmianyNaFakturze)
                                   {

                                   switch(dp.RodzajDokumentu)
                                           {
                                           case PARAGON:
                                           case ORDER:
                                                   {
                                                   DisplayBeep(0);
                                                   MUI_Request(app, FakturaWindow, 0, TITLE, MSG_OK, MSG_FAK_CANT_FREEZE_REQ);
                                                   }
                                                   continue;
                                           }





                                   // musi by� podany odbiorca

                                   if(dp.Odbiorca == NULL)
                                           {
                                           DisplayBeep(0);
                                           MUI_Request(app, FakturaWindow, 0, TITLE, MSG_OK, MSG_FAK_NO_CUST_FREEZE);
                                           set(FakturaWindow, MUIA_Window_ActiveObject, BT_Fak_Odbiorca);
                                           continue;
                                           }

                                   // na pewno frizowa�?

                                   if(!MUI_Request(app, FakturaWindow, 0, TITLE, MSG_FAK_FREEZE_REQ_GAD, MSG_FAK_FREEZE_REQ))
                                         break;


                                   // je�li frizujemy sfrizowany uprzednio dokument,
                                   // warto skasowa� jego poprzednie wcielenie

                                   if( kontynuacja_dokumentu )
                                           {
                                           if( DeleteFile( filename ) == FALSE )
                                                   {
                                                   DisplayBeep(0);
                                                   D(bug("** Nie moge skasowa� pliku \"%s\"!\n", filename ));
                                                   }
                                           }


                                   // progress window  - steps: save doc, save mag

                                   _sleep(TRUE);

                                   set(GA_Prog_Info, MUIA_Gauge_Current, 0);
                                   set(GA_Prog_Info, MUIA_Gauge_Max, 2);
                                   set(ProgressWindow, MUIA_Window_Open, TRUE);


//                   // zmieniamy typ na typ do od�o�enia

//                   dp.RodzajDokumentu += OD_FAKTURA_VAT - FAKTURA_VAT;;


                                   // i zapisujemy dokument

                                   sprintf( dp.NumerDokumentu, "ProForma%03lx%03lx", wfmh_GetCurrentTime()->ds_Minute, wfmh_GetCurrentTime()->ds_Tick );

                                   ZapiszDokument(&dp, TRUE);
                                   set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);

                                   // oraz magazyn

                                   if(!bezmagazynu)
                                           {
                                           settext(TX_Prog_Info, MSG_PROG_MAG_SAVE);
/*** TU ***/
                                           ZapiszMagazyn();
/*** TU ***/
                                           }
                                   set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);


                                   set(GA_Prog_Info, MUIA_Gauge_Current, xget(GA_Prog_Info, MUIA_Gauge_Current)+1);
                                   set(ProgressWindow, MUIA_Window_Open, FALSE);

                                   _sleep(FALSE);


                                   }

                           running = FALSE;
                           }
                           break;
//|
                         }

                  Prze��czNaRachunek = FALSE;  // je�li co� nie wyjdzie podczas dalszego

                  // sprawdzania, wracamy do trybu faktura

                  if(running && signal) Wait(signal);
                 }
           }
        else
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           }

//    set(FakturaWindow, MUIA_Window_Open, FALSE);
//    DoMethod(LV_Fak_Produkty       , _MUIM_List_Clear, NULL);
//    DoMethod(LV_Fak_FakturaProdukty, _MUIM_List_Clear, NULL);
        }

        _detachwin( FakturaWindow );
        _sleep(FALSE);

        return(result);


}
//|

/// Korekta
void Korekta()
{
struct DocumentScan ds = {0};

        if( DokumentSelector( &ds, DOKUMENTY_BIE��CE, TRUE, NULL ) )
           {
           }
}
//|
