
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
$Id: golem_importer.c,v 1.1 2003/01/01 20:40:53 carl-os Exp $.
*/

#include "golem.h"


/// DRUKARKA SELECTOR STRINGS

#define MSG_MI_WIN_TITLE       "TSS Firma 6.x - importer danych magazynowych"

#define MSG_MI_FILE_T   "Baza danych magazynowych"
#define MSG_MI_FILE     "_Baza"
#define MSG_MI_SETT_T   "Ustawienia importu"
#define MSG_MI_MERGE    "_Do��cz opis do nazwy produktu"

#define MSG_MI_OK       "F10 - _Importuj"
#define MSG_MI_CANCEL   "ESC - Ponie_chaj"

//|
/// FirmaImportSetup


static Object *FirmaImportWindow,        /* JEDNOSTKA SELECTOR */

			  *ST_MI_File,
			  *BT_MI_File,

			  *CH_MI_MergeComments,

			  *BT_MI_Ok,
			  *BT_MI_Cancel;



char FirmaImportSetup( void )
{

///   Create FirmaImportWindow

	  FirmaImportWindow = WindowObject,
						MUIA_Window_ID         , ID_WIN_IMPORT_FIRMA,
						MUIA_Window_ScreenTitle, ScreenTitle,
						MUIA_Window_Title      , MSG_MI_WIN_TITLE,
						WindowContents,
						   VGroup,

						   // Plik
						   Child, HGroup,
								  GroupFrameT( MSG_MI_FILE_T ),
								  MUIA_Group_Spacing, 1,
								  Child, MakeLabel2( MSG_MI_FILE ),
								  Child, ST_MI_File = MakeString( 256, NULL),
								  Child, BT_MI_File = PopButton2( MUII_PopDrawer, MSG_MI_FILE ),
								  End,

						   // settings
						   Child, HGroup,
								  GroupFrameT( MSG_MI_SETT_T ),
								  Child, HVSpace,
								  Child, HGroup,
										 MUIA_Group_Spacing, 4,
										 Child, MakeLabel2( MSG_MI_MERGE ),
										 Child, CH_MI_MergeComments = MakeCheck( MSG_MI_MERGE ),
										 End,
								  End,

						   Child, HGroup,
								  MUIA_Group_SameSize, TRUE,
								  Child, BT_MI_Ok     = TextButton(MSG_MI_OK),
								  Child, BT_MI_Cancel = TextButton(MSG_MI_CANCEL),
								  End,

						   End,

					  End;
//|

	if( !FirmaImportWindow )
	   return( FALSE );

	DoMethod( FirmaImportWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL );
	DoMethod(BT_MI_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);
	DoMethod(BT_MI_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CANCEL);

	DoMethod( BT_MI_File, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_SELECT );

	DoMethod(FirmaImportWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

	_attachwin( FirmaImportWindow );

	return(TRUE);

}

//|

/// FirmaDataImport
char FirmaDataImport( char *file, char merge_comments  )
{
char buffer[512];
FILE *fh;
char result = FALSE;

struct GrupaList *grupa;
char   grupa_name[] = "--Firma-6.x--";
char   grupa_id = (char)WybierzIDGrupy();


	D(bug("FirmaDataImport: '%s'\n", file));

	if( (grupa = Znajd�Grup�Nazwa( grupa_name )) )
	  {
	  grupa_id = grupa->gm_grupa.ID;
	  }
	else
	  {
	  grupa = calloc(1, sizeof(struct GrupaList));

	  if( grupa )
		{
		grupa->gm_grupa.ID = grupa_id;
		strcpy( grupa->gm_grupa.Nazwa, grupa_name );
		grupa->gm_grupa.Vat = settings.def_vat;

		DodajGrup�( grupa, TRUE );
		}
	  else
		{
		DisplayBeep(0);
		D(bug("Can't alloc new group\n"));
		return( FALSE );
		}
	  }


	D(bug(" GroupID: %ld\n", grupa_id));
	UstawGrup�Aktywn�Magazyn( grupa );


	if( fh=fopen( file, "rb+" ) )
	   {
	   int i=0;

	   // title
	   fgets(buffer, sizeof(buffer), fh);
	   wfmh_convert( buffer, 10, 0 );
	   D(bug("Match?: '%s'\n", buffer));

	   // + 3 spare line won
	   for(i=0; i < 3; i++)
		   fgets(buffer, sizeof(buffer), fh);

	   // jadziem
	   // != 138/9
	   while( fgets(buffer, sizeof(buffer), fh) != NULL )
		   {
		   struct ProductList *prod;
		   char   tmp[64];
		   char   *next1 = buffer;
		   char   *next2 = buffer;
		   int    len;
		   double cena_sprzedazy;
		   char   *non_space;

		   if( prod = calloc(1, sizeof(struct ProductList) ) )
			   {
			   prod->pl_prod.Grupa = grupa_id;

			   // LF won
			   wfmh_convert( buffer, 10, 0 );

//           D(bug(" Buffer: '%s'\n", buffer));

			   // skipujemy lp ichni
			   next1 = wfmh_strchrs( next1, "|") + 1;

			   // nazwa
			   next2 = wfmh_strchrs( next1, "|");
			   len   = next2 - next1;
			   memcpy( tmp, next1, len );
			   tmp[len] = 0;
			   if( non_space = wfmh_notstrnchrrev( tmp, " ", -1) )
				   non_space[1]=0;
			   strncpy( prod->pl_prod.Nazwa, tmp, PROD_NAME_LEN );
			   next1 = next2+1;

			   // sww
			   next2 = wfmh_strchrs( next1, "|");
			   len   = next2 - next1;
			   memcpy( tmp, next1, len );
			   tmp[len] = 0;
			   if( non_space = wfmh_notstrnchrrev( tmp, " ", -1) )
				   non_space[1]=0;
			   strncpy( prod->pl_prod.SWW, tmp, PROD_SWW_LEN );
			   next1 = next2+1;

			   // jm
			   {
			   next2 = wfmh_strchrs( next1, "|");
			   len   = next2 - next1;
			   memcpy( tmp, next1, len );
			   tmp[len] = 0;
			   if( non_space = wfmh_notstrnchrrev( tmp, " ", -1) )
				   non_space[1]=0;
			   strncpy( prod->pl_prod.Jedn.Nazwa, tmp, PROD_JEDN_LEN );

			   if( !Znajd�Jednostk�Nazwa( prod->pl_prod.Jedn.Nazwa) )
				 {
				 struct JednostkaList *jedn = calloc(1, sizeof(struct JednostkaList));

				 if(jedn)
				   {
				   memcpy( &jedn->ul_jedn, &prod->pl_prod.Jedn, sizeof(struct Jednostka));

				   jedn->Imported = TRUE;
				   DodajJednostk�(jedn);
				   }
				 }
			   next1 = next2+1;
			   }


			   // skipujemy pusty |
			   next1 = wfmh_strchrs( next1, "|") + 1;

			   // vat+
			   {
			   int svat;
			   int vat;

			   next2 = wfmh_strchrs( next1, "|");
			   len   = next2 - next1;
			   memcpy( tmp, next1, len );
			   tmp[len] = 0;
			   sscanf( tmp, "%ld", &svat );
			   switch( svat )
				   {
				   case -1:
					   vat = 0;
					   break;
				   case 17:
					   vat = 3;
					   break;
				   case  7:
					   vat = 2;
					   break;
				   case  3:
					   vat = 5;
					   break;
				   case  0:
					   vat = 1;
					   break;

				   case 22:
				   default:
					   vat = 4;
					   break;
				   }
			   prod->pl_prod.Vat = vat;
			   next1 = next2+1;
			   }

			   // ilosc
			   next2 = wfmh_strchrs( next1, "|");
			   len   = next2 - next1;
			   memcpy( tmp, next1, len );
			   tmp[len] = 0;
			   prod->pl_prod.Ilo�� = String2Price( tmp );
			   next1 = next2+1;

			   // skipujemy ilosc p
			   next1 = wfmh_strchrs( next1, "|") + 1;

			   // cena sprzedazy
			   next2 = wfmh_strchrs( next1, "|");
			   len   = next2 - next1;
			   memcpy( tmp, next1, len );
			   tmp[len] = 0;
			   cena_sprzedazy = String2Price( tmp );
			   next1 = next2+1;

			   // cena zakupu
			   next2 = wfmh_strchrs( next1, "|");
			   len   = next2 - next1;
			   memcpy( tmp, next1, len );
			   tmp[len] = 0;
			   prod->pl_prod.Zakup = String2Price( tmp );
			   next1 = next2+1;

			   // skipujemy ID
			   next1 = wfmh_strchrs( next1, "|") + 1;
			   // skipujemy vat-
			   next1 = wfmh_strchrs( next1, "|") + 1;
			   // skipujemy min&akt
			   next1 = wfmh_strchrs( next1, "|") + 1;
			   // skipujemy kod
			   next1 = wfmh_strchrs( next1, "|") + 1;

			   // uwagi
			   next2 = wfmh_strchrs( next1, "|");
			   len   = next2 - next1;
			   memcpy( tmp, next1, len );
			   tmp[len] = 0;
			   if( non_space = wfmh_notstrnchrrev( tmp, " ", -1) )
				   non_space[1]=0;
			   D(bug("%s\n", tmp));
			   if( merge_comments && (strlen(tmp)>0) )
				   {
				   char buf[128];

				   sprintf( buf, "%s %s", prod->pl_prod.Nazwa, tmp );
				   strncpy( prod->pl_prod.Nazwa, buf, PROD_NAME_LEN );
				   }
			   next1 = next2+1;

			   // obliczamy marze
			   prod->pl_prod.Mar�a = cena_sprzedazy - prod->pl_prod.Zakup;


			   DodajProdukt( prod, FALSE );
			   }
		   else
			   {
			   DisplayBeep(0);
			   D(bug("No memory for new product\n"));
			   }
		   }


	   fclose( fh );

	   result = TRUE;
	   }
	 else
	   {
	   DisplayBeep(0);
	   }

	UstawGrup�Aktywn�Magazyn( grupa );

	return( result );
}
//|

/// FirmaImport


char FirmaImport( void )
{
/*
** Frontend do importera danych magazynowych
*/

char  running = TRUE;
ULONG signal  = 0;
int   count = 0;
char  result = FALSE;


	_sleep(TRUE);

	if( !FirmaImportSetup( ) )
	   {
	   DisplayBeep(0);
	   MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
	   _sleep(FALSE);
	   return(NULL);
	   }



	// poprawi�!
//    set(DrukarkaSelectorWindow, MUIA_Window_ActiveObject, LV_PrtSel_Prt);

	if(WinOpen( FirmaImportWindow ))
		{
		while(running)
		  {
		  switch (DoMethod(app, MUIM_Application_Input, &signal))
			{
			case ID_SELECT:
			   {
			   char *file;

				_sleep( TRUE );
			   file = RequestAslFile( (char *)xget(ST_MI_File, MUIA_String_Contents), FALSE, FALSE );

			   if( file )
				   setstring( ST_MI_File, file );
				   
				_sleep( FALSE );
			   }
			   break;

			case ID_OK:
				 {
				 char *file = (char *)xget(ST_MI_File, MUIA_String_Contents);
				 if( file[0] != 0 )
				   {
				   _sleep(TRUE);
				   result = FirmaDataImport( file, getcheckmark( CH_MI_MergeComments ) );
				   _sleep(FALSE);
				   running = FALSE;
				   }
				 else
				   {
				   DisplayBeep(0);
				   }
				 }
			   break;


			case ID_CANCEL:
			   running = FALSE;
			   break;

			}
		  if(running && signal) Wait(signal);
		  }
		}
	 else
		{
		DisplayBeep(0);
		MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
		}


	_detachwin( FirmaImportWindow );
	_sleep(FALSE);

	return(result);
}

//|

