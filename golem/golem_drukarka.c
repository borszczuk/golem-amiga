
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/* $Id: golem_drukarka.c,v 1.1 2003/01/01 20:40:53 carl-os Exp $ */

#include "golem.h"

UWORD   Result;                     //byte - serial,  byte parallel

// tablice u�ywane przy konwersji podczas wydruku
char  Plain[]   = "acelnoszzACELNOSZZ";
char  AmigaPL[] = "������������������";
char  ISO[]     = "����󶼿��ʣ�Ӧ��";
char  Latin2[]  = "����䢘�����������";
char  Mazovia[] = "������������������";
char  Suxx95[]  = "����䢘�����������";
char  Ogonki[]  = ",',/''''-,',/,'''-";


char   IDPrinter[ 256 ];     // WczytajDrukarki() inicjalizuje tablic�!


//#ifdef BETA
//char beta_watermark_2[] = BETA_WATERMARK;
//#endif

/// PRT STOP CHUNKS
#define PRT_NUM_STOPS (sizeof(Prt_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Prt_Stops[] =
{
   ID_PRT, ID_CAT,
   ID_PRT, ID_VERS,

   ID_PRT, ID_ID,
   ID_PRT, ID_NAME,
   ID_PRT, ID_TYPE,

   ID_PRT, ID_PATH,

   ID_PRT, ID_PRTD,
   ID_PRT, ID_PRTU,
   ID_PRT, ID_WIDW,
   ID_PRT, ID_WIDN,
   ID_PRT, ID_WIDC,
   ID_PRT, ID_CONV,
   ID_PRT, ID_PGON,
   ID_PRT, ID_PGHE,
   ID_PRT, ID_PGFO,

   ID_PRT, ID_PRT,
   ID_PRT, ID_NORM,
   ID_PRT, ID_BDON,
   ID_PRT, ID_BDOF,
   ID_PRT, ID_ITON,
   ID_PRT, ID_ITOF,
   ID_PRT, ID_UNON,
   ID_PRT, ID_UNOF,
   ID_PRT, ID_CDON,
   ID_PRT, ID_CDOF,

   ID_PRT, ID_PRIN,
   ID_PRT, ID_ORIG,
   ID_PRT, ID_COPY,
   ID_PRT, ID_DUPE,
   ID_PRT, ID_DPTX,
   ID_PRT, ID_SWW ,
   ID_PRT, ID_RECE,
   ID_PRT, ID_FEED,
   ID_PRT, ID_PCS ,

   NULL, NULL
};
//|

/// SetSpoolerName
/*
**
** This example shows how to use the
** PrintManager v39 API
**
** Currently only @{PMCMD:NAMEJOB/}
** is available
**
*/


void SetSpoolerName(struct Printer *printer, char *name)
{
struct Device   *dev;
static char   _name[30];
struct PrinterRuntime *prt = printer->runtime;

     if(wfmh_isenv("Golem/DisableSpoolerNames"))
          return;

     // no spooler for non-printers!
     if( printer->Type != PRT_TYPE_PRINTER )
        return;

     if(prt == NULL)
        {
        DisplayBeep(0);
        D(bug("SetSpoolName: PRT is NULL!\n"));
        return;
        }

     dev = (struct Device *)FindName( &((struct ExecBase *)SysBase)->DeviceList, "spool.device" );

     /*
     **      to avoid trouble with pm 2.0 by Nicola Salmoria
     **      we do a version check here
     */

     if( dev && ((struct Library *)dev)->lib_Version >= 40L )
        {
        /*
        **      now send the name
        **      the name is limited to 25 chars and MUST NOT
        **      contain ':/#?*' ...
        */

        sprintf(_name, "@{PMCMD:NAMEJOB/%s}", name);

        D(bug(_name));

        prt->IO->io_Command = PRD_RAWWRITE;
        prt->IO->io_Length = -1L;
        prt->IO->io_Data = _name;

        if(wfmh_isenv("Golem/FakePrinter") == NULL)
             DoIO((struct IORequest *)prt->IO);
        }
}
//|

/// RequestAslFile
char *RequestAslFile( char *path, char DrawersOnly, char SaveMode )
{
struct Window *win = NULL;
struct FileRequester *req = NULL;

static char buf[ 512 ];
char *result = NULL;
char *path_part;


        if( (req = MUI_AllocAslRequest( ASL_FileRequest, TAG_DONE )) == NULL )
             {
             DisplayBeep(0);
             D(bug("Can't AllocAslRequest()!\n"));
             return( result );
             }


          path_part = PathPart( path );
          CopyMem( path, buf, path_part - path );
          buf[ path_part - path ] = 0;


          if( MUI_AslRequestTags( req,
//               ASLFR_Window       , win,
                  ASLFR_InitialFile  , FilePart( path ),
                  ASLFR_InitialDrawer, buf,
                  ASLFR_DrawersOnly  , DrawersOnly,
                  ASLFR_DoPatterns   , TRUE,
                  ASLFR_DoSaveMode   , SaveMode,
                  TAG_DONE ) )
                     {
                     switch( DrawersOnly )
                       {
                       case FALSE:
                            if( req->fr_File[0] != 0 )
                                {
                                stccpy(  buf, req->fr_Drawer, sizeof( buf ) );
                                AddPart( buf, req->fr_File  , sizeof( buf ) );

                                result = buf;
                                }
                            break;

                       case TRUE:
                            stccpy(  buf, req->fr_Drawer, sizeof( buf ) );
                            result = buf;
                            break;
                       }
                     }

          if( req )
             MUI_FreeAslRequest( req );

          return( result );

}
//|

/// OpenPrinter_Printer
char OpenPrinter_Printer( struct Printer *printer )
{


//  if(!wfmh_isenv("Golem/FakePrinter"))
     {
     if( printer->runtime = calloc(1, sizeof( struct PrinterRuntime )) )
       {
       struct PrinterRuntime *prt = printer->runtime;

       if(prt->Port = CreateMsgPort())
          {
          prt->Port->mp_Node.ln_Name = NULL;
//        prt->Port->mp_Node.ln_Name = "Golem-PrinterRuntime";

          if(prt->IO = CreateIORequest(prt->Port, sizeof(struct IOStdReq)))
             {
             int i;

             D(bug( "OpenPrinter: %s / %ld\n", printer->printer_device, printer->printer_unit ) );

             OpenDevice( printer->printer_device, printer->printer_unit, (struct IORequest *)prt->IO, NULL );
             prt->IO->io_Message.mn_ReplyPort = prt->Port;


             for(i=0; i<256; i++)
                  prt->CLUT[i]=i;

             switch(printer->printer_charset)
                  {
                  case PRINT_AMIGAPL:
                       break;

                  case PRINT_NOPL:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = Plain[i];
                       break;

                  case PRINT_CHR8:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = Plain[i];
                       break;

                  case PRINT_ISO:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = ISO[i];
                       break;

                  case PRINT_LATIN2:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = Latin2[i];
                       break;

                  case PRINT_MAZOVIA:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = Mazovia[i];
                       break;

                  case PRINT_SUXX95:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = Suxx95[i];
                       break;
                  }

             D(bug("OpenPrinter: $%08lx\n", prt));
             D(bug("  PrtDevice: %ld (%s)\n", printer->ID, printer->Name ));

             return( TRUE );
             }
          else
             {
             DisplayBeep(0);
             D(bug("** Can't CreateIORequest() for printer\n"));
             }

          DeleteMsgPort(prt->Port);
          }
       else
          {
          DisplayBeep(0);
          D(bug("** Can't create printer port\n"));
          }

       free( printer->runtime );
       }
     else
       {
       DisplayBeep(0);
       D(bug("** Can't allocate PrinterRuntime\n"));
       }


     }

     D(bug("OpenPrinter: Can't open printer\n"));

     return( FALSE );

}
//|
/// OpenPrinter_File
char OpenPrinter_File( struct Printer *printer )
{

char *file = {"nil:"};


     if( !printer->SilentMode )
        {
        _sleep(TRUE);
        file = RequestAslFile( printer->default_path, FALSE, TRUE );
        _sleep(FALSE);
        }


     if( file == NULL )
        return( FALSE );


//  if(!wfmh_isenv("Golem/FakePrinter"))
     {
     if( printer->runtime = calloc(1, sizeof( struct FileRuntime )) )
       {
       struct FileRuntime *prt = printer->runtime;

       D(bug("OpenPrinter: FileRuntime 0x%08lx\n", printer->runtime ));

       if( (prt->FileHandle = Open( file, MODE_NEWFILE )) )
           {
           int i;

           D(bug( "             FileHandle  0x%08lx (%s)\n", prt->FileHandle, file ));


             for(i=0; i<256; i++)
                  prt->CLUT[i]=i;

             switch(printer->printer_charset)
                  {
                  case PRINT_AMIGAPL:
                       break;

                  case PRINT_NOPL:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = Plain[i];
                       break;

                  case PRINT_CHR8:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = Plain[i];
                       break;

                  case PRINT_ISO:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = ISO[i];
                       break;

                  case PRINT_LATIN2:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = Latin2[i];
                       break;

                  case PRINT_MAZOVIA:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = Mazovia[i];
                       break;

                  case PRINT_SUXX95:
                       for(i=0; i<strlen(AmigaPL); i++)
                            prt->CLUT[AmigaPL[i]] = Suxx95[i];
                       break;
                  }


           return( TRUE );
           }
       else
           {
           DisplayBeep(0);
           D(bug("** Can't open Output file\n"));
           }

       free( printer->runtime );
       }
     else
       {
       DisplayBeep(0);
       D(bug("** Can't allocate FileRuntime\n"));
       }


     }
     D(bug("OpenPrinter: Can't open printer\n"));

     return( FALSE );

}
//|
/// OpenPrinter
char OpenPrinter( struct Printer *printer )
{
/*
** Inicjalizuj� drukark� i przygotowuj�
** tablic� konwersji polskich znak�w.
*/

char result = FALSE;


     switch( printer->Type )
        {
        case PRT_TYPE_PRINTER:
             result = OpenPrinter_Printer( printer );
             break;

        case PRT_TYPE_FILE:
             result = OpenPrinter_File( printer );
             break;
        }

     return( result );

}
//|

/// ClosePrinter_Printer
void ClosePrinter_Printer( struct Printer *printer )
{
/*
** Ko�czy prac� z drukark�, zwalnia wszystkie
** zaalokowane struktury
*/

struct PrinterRuntime *prt = printer->runtime;
//    if(!wfmh_isenv("Golem/FakePrinter"))
        {
        if(prt->IO)
            {
            CloseDevice((struct IORequest *)prt->IO);
            DeleteIORequest((struct IORequest *)prt->IO);
            }

        if(prt->Port)
           DeleteMsgPort(prt->Port);

        if(prt)
           free(prt);
        }

}
//|
/// ClosePrinter_File
void ClosePrinter_File( struct Printer *printer )
{
/*
** Ko�czy prac� z pliko-drukark�, zwalnia wszystkie
** zaalokowane struktury
*/

struct FileRuntime *prt = (struct FileRuntime *)printer->runtime;
//    if(!wfmh_isenv("Golem/FakePrinter"))
        {
        D(bug("ClosePrinter: FileRuntime 0x%08lx\n", prt ));
        D(bug( "             FileHandle  0x%08lx\n", prt->FileHandle ));

        if( prt )
           Close( prt->FileHandle );

        if( prt )
           free( prt );
        }

}
//|
/// ClosePrinter
void ClosePrinter(struct Printer *printer)
{
/*
** Ko�czy prac� z drukark�, zwalnia wszystkie
** zaalokowane struktury
*/

     switch( printer->Type )
        {
        case PRT_TYPE_PRINTER:
             ClosePrinter_Printer( printer );
             break;

        case PRT_TYPE_FILE:
             ClosePrinter_File( printer );
             break;
        }

}
//|

/// CheckPrinter
/*
** Sprawdza rodzaj drukarki i jej stan (QUERY)
** zwraca:
**    0 - je�li wszystko ok,
**    1 - je�li drukarka jest off-line
**    2 - je�li nie ma papieru
*/


char CheckPrinterRaw(struct PrinterRuntime *prt)
{
short Result = 0;

     D(bug("CheckPrinterRaw: prt=$%08lx\n", prt));

     prt->IO->io_Command = PRD_QUERY;
     prt->IO->io_Data = &Result;
     DoIO((struct IORequest *)prt->IO);

     switch(prt->IO->io_Actual)
        {
        case 1:     // Parallel device
             {
             if(((Result>>8) & 3) == 0)
                 {
                 prt->IO->io_Command = CMD_RESET;
                 DoIO((struct IORequest *)prt->IO);

                 return(0);
                 }
             else
                 {
                 if((Result>>8) & 01)
                      return(1);
                 else
                      return(2);
                 }
             }
             break;

        case 2:     // serial device
             {
             prt->IO->io_Command = CMD_RESET;
             DoIO((struct IORequest *)prt->IO);

             return(0);
             }
             break;
        }

}


/*
** Sprawdza czy drukarka w trybie on-line i czy
** jest za�o�ony papier
** zwraca:
**    FALSE - je�li co� nie tak i u�ytkownik chce przerwa� operacj�
**    TRUE  - je�li wszytko ok (resetuje drukark�)
*/

char CheckPrinter(struct Printer *printer, Object *win)
{
char result;

struct PrinterRuntime *prt = printer->runtime;


     D(bug("CheckPrinter: prt=$%08lx\n", prt));


     if( printer->Type == PRT_TYPE_FILE )
        return( TRUE );



     if(wfmh_isenv("Golem/FakePrinter"))
        return(TRUE);

     if(!wfmh_isenv("Golem/NoPrinterCheck"))
        return(TRUE);


     while((result = CheckPrinterRaw(prt)) != 0)
        {
        switch(result)
             {
             case 1:
                  if(!(MUI_Request(app, win, 0, TITLE, "*_Pon�w|P_rzerwij", "\033cDrukarka nie jest w��czona\nlub jest w trybie \0338\"off-line!\"\0332")))
                       {
                       return(FALSE);
                       }
                  break;

             case 2:
                  if(!(MUI_Request(app, win, 0, TITLE, "*_Pon�w|P_rzerwij", "\033cW��� papier nim zaczniesz drukowa�!")))
                       return(FALSE);
                  break;
             }
        }

     return(TRUE);

}
//|


/// PrintLineMain_Printer
char PrintLineMain_Printer(struct Printer *printer, char *line)
{
/*
** Drukuje podany tekst na drukarce/pliku, z ew. konwersj� polskich znak�w
** Mo�e si� wyr�n�� na serialnej drukarce, ale wali mnie to!
**
** IN: DocumentPrint
**     line - string do wydrukowania (+ do ew. konwersji)
**     dp->prt->Type = typ urzadzenia wyjsciowego
*/

char    TempBuffer[512] = "\0";
ULONG   StringLen, Index;
int     i;
struct PrinterRuntime *prt = printer->runtime;


//    D(bug("PrintLine\n"));

     StringLen = strlen(line);

     prt->IO->io_Command = CMD_WRITE;
     prt->IO->io_Length = -1;
     prt->IO->io_Data = TempBuffer;


     // konwersja znakow lub nadrukowanie ogonk�w
     if( printer->printer_charset == PRINT_CHR8 )
        {
        long dest  = 0;
        long Index = 0;
        char znak;

        for(i=0; i<=StringLen; i++)
             {
             znak = line[i];
             TempBuffer[dest++] = prt->CLUT[znak];

             for(Index=0; Index<strlen(AmigaPL); Index++)
                  {
                  if(znak == AmigaPL[Index])
                       {
                       TempBuffer[dest-1] = Plain[Index];
                       TempBuffer[dest++] = 8;
                       TempBuffer[dest++] = Ogonki[Index];
                       }
                  }
             }
        }
     else
        for(i=0; i<=StringLen; i++)
             TempBuffer[i] = prt->CLUT[ line[i] ];

     if(wfmh_isenv("Golem/FakePrinter") == NULL)
     {
        DoIO((struct IORequest *)prt->IO);
     }
     else
     {
        D(bug(prt->IO->io_Data));
     }

     return(TRUE);
}
//|
/// PrintLineMain_File
char PrintLineMain_File(struct Printer *printer, char *line)
{
/*
** Drukuje podany tekst do pliku, z ew. konwersj� polskich znak�w
*/

char    TempBuffer[512] = "\0";
ULONG   StringLen, Index;
int     i;
struct  FileRuntime *prt = printer->runtime;


//    D(bug("PrintLine\n"));

     StringLen = strlen(line);

     // konwersja znakow lub nadrukowanie ogonk�w
     if( printer->printer_charset == PRINT_CHR8 )
        {
        long dest  = 0;
        long Index = 0;
        char znak;

        for(i=0; i<=StringLen; i++)
             {
             znak = line[i];
             TempBuffer[dest++] = prt->CLUT[znak];

             for(Index=0; Index<strlen(AmigaPL); Index++)
                  {
                  if(znak == AmigaPL[Index])
                       {
                       TempBuffer[dest-1] = Plain[Index];
                       TempBuffer[dest++] = 8;
                       TempBuffer[dest++] = Ogonki[Index];
                       }
                  }
             }
        }
     else
        for(i=0; i<=StringLen; i++)
             TempBuffer[i] = prt->CLUT[ line[i] ];

     if(wfmh_isenv("Golem/FakePrinter") == NULL)
     {
        FPuts( prt->FileHandle, TempBuffer );
     }
     else
     {
        D(bug(TempBuffer));
     }

     return(TRUE);

}
//|
/// PrintLineMain
char PrintLineMain(struct Printer *printer, char *line)
{

     if( printer->SilentMode )
        return(TRUE);

     switch( printer->Type )
        {
        case PRT_TYPE_PRINTER:
               PrintLineMain_Printer( printer, line );
               break;

        case PRT_TYPE_FILE:
               PrintLineMain_File( printer, line );
               break;

        default:
               D(bug("Unknown dp->prt->Type!\n"));
               DisplayBeep(0);
               break;
        }

     return(TRUE);

}
//|

/// CountLF

int CountLF( char *str )
{
int i=0;
int cnt = 0;

     for(i; i<strlen(str); i++)
        {
        if( str[i] == '\n' )
             cnt++;
        }

     return( cnt );
}
//|
/// PrintLine
char PrintLine(struct DocumentPrint *dp, char *line)
{
int step = CountLF( line );

     dp->aktualna_linia += step;
     dp->liczba_linii += step;
     dp->aktualna_linia_total += step;


     // drukujemy w�a�ciw� lini� tekstu...

     PrintLineMain(dp->prt, line);


     // ...i sprawdzamy czy nie czas by zmieni� stron�?

     if( (dp->prt->page_mode) && (dp->prt->SilentMode != TRUE))
     {
        // czas na stopke aby?
          if( dp->aktualna_linia >= ( dp->prt->page_height - PAGE_FOOTER_HEIGHT ))
          {
               char footer = TRUE;

               char buffer[80];

               sprintf(buffer, "\n--[%s, strona %ld]--", dp->Nag��wekDokumentu,
                                                                   dp->aktualna_strona + 1);
               PrintLineMain(dp->prt, buffer);

               // form feed (ASCII 12)
               PrintLineMain(dp->prt, "\014");
               dp->aktualna_linia = 0;
               dp->aktualna_strona++;
          }
     }
     return(TRUE);
}
//|
/// PrintLastPage

// drukuje ostatni� stron� (je�li stronicowanie jest w��czone
// dope�niaj�c do pe�nej wysoko�ci i umieszczaj�c stopk� na ko�cu

char PrintLastPage(struct DocumentPrint *dp)
{

     // czy aby nie za 'kr�tki' dokument na stopk� (1 strona tylko?)?
     if( dp->prt->skip_1st_page_footer == TRUE)
        {
        // pomijanie stopki na 1-no stronicowych dokumentach wlaczone
        // wiec sprawdzamy czy aby nie 1sza strona...

        if(dp->aktualna_strona == 0)
             return(TRUE);
        }

//    if( ( (dp->aktualna_linia_total + PAGE_FOOTER_HEIGHT) >= dp->liczba_linii_total ) &&
//        ( dp->prt->page_mode )
//      )
        {
        char buffer[100];
        int i;

        // dope�niamy do ko�ca

        for(i=dp->aktualna_linia; i<( dp->prt->page_height - PAGE_FOOTER_HEIGHT ); i++)
             PrintLineMain(dp->prt, "\n");

        sprintf(buffer, "\n--[%s, strona %ld, ostatnia]--",
                                                              dp->Nag��wekDokumentu,
                                                              dp->aktualna_strona + 1);
        PrintLineMain(dp->prt, buffer);

/*
        // line feed (ASCII 12)
        PrintLineMain(dp, "\014");
*/

        dp->aktualna_linia = 0;
        dp->aktualna_strona++;
        }

     return(TRUE);

}
//|

/// MatchPrinterForDocument

// zwraca pointer na drukarke, ktora nalezy wydrukowac
// dany typ dokumentu

struct PrinterList * MatchPrinterForDocument( char DocType )
{
short ID;

     switch( DocType )
        {
        case FAKTURA_VAT:
        case OD_FAKTURA_VAT:
               ID = settings.map_fvat;
               break;
        case FVAT_PROFORMA:
        case OD_FVAT_PROFORMA:
               ID = settings.map_fvat_proforma;
               break;
        case PARAGON:
               ID = settings.map_paragon;
               break;
        case ORDER:
               ID = settings.map_zamowienie;
               break;

        case PRT_CENNIK_DETAL:
               ID = settings.map_cennik_detal;
               break;
        case PRT_CENNIK_HURT:
               ID = settings.map_cennik_hurt;
               break;
        case PRT_SPIS_Z_NATURY:
               ID = settings.map_spis;
               break;


/*
        case RACHUNEK:
        case FVAT_KOR:
        case RACH_KOR:
        case FVAT_PAR:
        case RACH_PAR:

        case OD_RACHUNEK:
        case OD_FVAT_KOR:
        case OD_RACH_KOR:
        case OD_FVAT_PAR:
        case OD_RACH_PAR:
*/

        default:
             ID = DEFAULT_PRINTER;

        }

     return( Znajd�Drukark�ID( ID ) );
}
//|

/// SetDefPrtSettings
void SetDefPrtSettings( struct PrinterList *printer )
{

struct Printer *prt = &printer->pl_printer;

     strcpy(prt->Name, "Wpisz nazw� drukarki");

     strcpy(prt->normal, "\033[0m");
     strcpy(prt->bold_on, "\033[1m");
     strcpy(prt->bold_off, "\033[22m");

     strcpy(prt->italics_on, "\033[3m");
     strcpy(prt->italics_off, "\033[23m");
     strcpy(prt->underline_on, "\033[4m");
     strcpy(prt->underline_off, "\033[24m");

     strcpy(prt->cond_on, "\033[4w");
     strcpy(prt->cond_off, "\033[3w");


     strcpy(prt->printer_device, "printer.device");
     prt->printer_unit = 0;

     prt->printer_width_wide = 38;       // page_width (wide)
     prt->printer_width_normal = 70;     // page_width (normal)
     prt->printer_width_cond = 120;      // page_width (cond)
     prt->printer_charset = PRINT_CHR8;  // printer charset
     prt->def_copies = 2;                // print_copies

     prt->page_mode = TRUE;              // page_mode?
     prt->page_height = 65;              // page_height
     prt->skip_1st_page_footer = FALSE;           // page footer

     prt->doc_print = 1;                     // doc_print
     strcpy(prt->original_text, "ORYGINA�"); // original_text
     strcpy(prt->copy_text, "KOPIA" );       // copy_text
     prt->dupe_print = 1;                    // dupe_print
     strcpy(prt->dupe_text, "DUPLIKAT");     // dupe_text
     prt->doc_sww = 0;                       // doc_sww
     strcpy(prt->norece_text, "Czytelny podpis odbiorcy");    // norece_text
     prt->formfeed = 1;                      // formfeed

}
//|
/// AddDefPrinter
void AddDefPrinter( void )
{

     if( !Znajd�Drukark�ID( DEFAULT_PRINTER ) )
        {
        struct PrinterList *prt = calloc(1, sizeof( struct PrinterList ));
        if( prt )
             {
             SetDefPrtSettings( prt );

             strcpy(prt->pl_printer.Name, "Drukarka domy�lna");
             prt->pl_printer.ID = DEFAULT_PRINTER;

             DodajDrukark�( prt );

             IDPrinter[ DEFAULT_PRINTER ] = TRUE;
             }
        }

}
//|

/// WczytajDrukarki
int WczytajDrukarki( void )
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
char   ValidFile = FALSE;

int    Errors = 0;


     set(app, MUIA_Application_Sleep, TRUE);


     // inicjalizacja tablicy
     memset( IDPrinter, 0, sizeof( IDPrinter ) );

     // wszystkie drukarki won
     Usu�Drukarki( TRUE );


      if(iff = AllocIFF())
        {
        if(iff->iff_Stream = Open(DrukarkiFileName, MODE_OLDFILE))
             {
             InitIFFasDOS(iff);

             StopChunks(iff, Prt_Stops, PRT_NUM_STOPS);
             StopOnExit(iff, ID_PRT, ID_FORM);

             if(!OpenIFF(iff, IFFF_READ))
                  {
                  struct PrinterList *prt = NULL;

                  while(TRUE)
                      {
                      _RC = ParseIFF(iff, IFFPARSE_SCAN);

                      if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                          break;

                      if(cn = CurrentChunk(iff))
                          {
                          LONG ID = cn->cn_ID;

                          if(!ValidFile)
                              {
                              if((ID == ID_CAT) && (cn->cn_Type == ID_PRT))
                                 {
                                 ValidFile = TRUE;
                                 continue;
                                 }

                              break;
                              }

///                       ID_VERS
                          if(ID == ID_VERS)
                              {
                              struct BaseVersion version;

                              if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                                 {

                                 }
                              else
                                 {
                                 _RC = IoErr();
                                 Errors++;
                                 break;
                                 }

                              continue;
                              }
//|
///                       ID_ID

                          if(ID == ID_ID)
                              {
                              prt = calloc(1, sizeof(struct PrinterList));

//                        D(bug(" ID: prt calloc() = 0x%lx\n", prt));

                              if( prt )
                                 {
//                           SetDefPrtSettings( prt );

                                 if(ReadChunkBytes(iff, &prt->pl_printer.ID, sizeof( short )) != cn->cn_Size)
                                    {
                                    _RC = IoErr();
                                    Errors++;
                                    D(bug("Nie moge wczytac ID drukarki klienta! Zla dlugosc chunka!\n"));
                                    break;
                                    }

                                 DodajDrukark�( prt );
                                 IDPrinter[ prt->pl_printer.ID ] = TRUE;
                                 }
                              else
                                 {
                                 D(bug("** can't malloc() mem for new PrinterList\n"));
                                 }
                              continue;
                              }
//|

                          _read(ID_NAME, prt->pl_printer.Name)
                          _read_size(ID_TYPE, &prt->pl_printer.Type, sizeof( char ))


                          // nastawy
                          _read(ID_PATH, prt->pl_printer.default_path)

                          _read(ID_PRTD, prt->pl_printer.printer_device)
                          _read_size(ID_PRTU, &prt->pl_printer.printer_unit, sizeof( char ))

                          _read_size(ID_WIDW, &prt->pl_printer.printer_width_wide, sizeof(short))
                          _read_size(ID_WIDN, &prt->pl_printer.printer_width_normal, sizeof(short))
                          _read_size(ID_WIDC, &prt->pl_printer.printer_width_cond, sizeof(short))
                          _read_size(ID_CONV, &prt->pl_printer.printer_charset, sizeof(char))

                          _read_size(ID_PGON, &prt->pl_printer.page_mode, sizeof(char))
                          _read_size(ID_PGHE, &prt->pl_printer.page_height, sizeof(short))
                          _read_size(ID_PGFO, &prt->pl_printer.skip_1st_page_footer, sizeof(char))


                          // kody
                          _read(ID_NORM, prt->pl_printer.normal)

                          _read(ID_BDON, prt->pl_printer.bold_on)
                          _read(ID_BDOF, prt->pl_printer.bold_off)
                          _read(ID_ITON, prt->pl_printer.italics_on)
                          _read(ID_ITOF, prt->pl_printer.italics_off)
                          _read(ID_UNON, prt->pl_printer.underline_on)
                          _read(ID_UNOF, prt->pl_printer.underline_off)
                          _read(ID_UNOF, prt->pl_printer.underline_off)

                          _read(ID_CDON, prt->pl_printer.cond_on)
                          _read(ID_CDOF, prt->pl_printer.cond_off)


                          // nadruki
                          _read_size(ID_PRIN, &prt->pl_printer.doc_print, sizeof(char))
                          _read(ID_ORIG, prt->pl_printer.original_text)
                          _read(ID_COPY, prt->pl_printer.copy_text)
                          _read_size(ID_DUPE, &prt->pl_printer.dupe_print, sizeof(char))
                          _read(ID_DPTX, prt->pl_printer.dupe_text)
                          _read(ID_RECE, prt->pl_printer.norece_text)
                          _read_size(ID_PCS , &prt->pl_printer.def_copies, sizeof(char))


                          _read_size(ID_SWW , &prt->pl_printer.doc_sww, sizeof(char))
                          _read_size(ID_FEED, &prt->pl_printer.formfeed, sizeof(char))
                          }
                      }

                  CloseIFF(iff);
                  }

               Close(iff->iff_Stream);
               }
           else
             {
             D(bug("Nie moge otworzyc pliku '%s' do odczytu\n", DrukarkiFileName));
             }

//       if(_RC == IFFERR_EOF) Error = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

        FreeIFF(iff);
        }

        set(app, MUIA_Application_Sleep, FALSE);




     // dodajemy drukarke domy�ln� jesli nie ma takowej
     AddDefPrinter();


     return(0);

}
//|
/// ZapiszDrukarki
int ZapiszDrukarki( void )
{

#define _ID_TYPE ID_PRT
#define _write(a,b)    {PushChunk(MyIFFHandle, _ID_TYPE, (a), IFFSIZE_UNKNOWN); WriteChunkBytes(MyIFFHandle, &(b), sizeof(b)); PopChunk(MyIFFHandle); }


struct IFFHandle *MyIFFHandle;

     set(app, MUIA_Application_Sleep, TRUE);

     if(MyIFFHandle = AllocIFF())
          {
          BPTR  FileHandle;

          if(FileHandle = Open( DrukarkiFileName, MODE_NEWFILE ))
             {
             MyIFFHandle->iff_Stream = FileHandle;
             InitIFFasDOS(MyIFFHandle);

             if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
                  {
                  struct BaseVersion version;
                  struct PrinterList *prt;

                  PushChunk(MyIFFHandle, _ID_TYPE, ID_CAT, IFFSIZE_UNKNOWN);

                  PushChunk(MyIFFHandle, _ID_TYPE, ID_FORM, IFFSIZE_UNKNOWN);
                       PushChunk(MyIFFHandle, _ID_TYPE, ID_VERS, IFFSIZE_UNKNOWN);
                       version.Version = VERSION;
                       version.Revision = REVISION;
                       WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                       PopChunk(MyIFFHandle);

                       PushChunk(MyIFFHandle, _ID_TYPE, ID_ANNO, IFFSIZE_UNKNOWN);
                       WriteChunkBytes(MyIFFHandle, ScreenTitle, strlen(ScreenTitle));
                       WriteChunkBytes(MyIFFHandle, " <", 2);
                       WriteChunkBytes(MyIFFHandle, EMAIL, strlen(EMAIL));
                       WriteChunkBytes(MyIFFHandle, ">, ", 3);
                       WriteChunkBytes(MyIFFHandle, WWW, strlen(WWW));
                       PopChunk(MyIFFHandle);
                  PopChunk(MyIFFHandle);


                  for( prt = (struct PrinterList *)drukarki.lh_Head; prt->pl_node.mln_Succ; prt = (struct PrinterList *)prt->pl_node.mln_Succ )
                       {
                       // Drukarka

                       struct Printer *printer = &prt->pl_printer;

                       PushChunk(MyIFFHandle, _ID_TYPE, ID_FORM, IFFSIZE_UNKNOWN);

                          // id/nazwa

                          _write(ID_ID  , printer->ID)
                          _write(ID_NAME, printer->Name)
                          _write(ID_TYPE, printer->Type)


                          // nastawy
                          _write(ID_PATH, printer->default_path)

                          _write(ID_PRTD, printer->printer_device)
                          _write(ID_PRTU, printer->printer_unit)
                          _write(ID_WIDW, printer->printer_width_wide)
                          _write(ID_WIDN, printer->printer_width_normal)
                          _write(ID_WIDC, printer->printer_width_cond)
                          _write(ID_CONV, printer->printer_charset)

                          _write(ID_PGON, printer->page_mode)
                          _write(ID_PGHE, printer->page_height)
                          _write(ID_PGFO, printer->skip_1st_page_footer)


                          // kody
                          _write(ID_NORM, printer->normal)

                          _write(ID_BDON, printer->bold_on)
                          _write(ID_BDOF, printer->bold_off)
                          _write(ID_ITON, printer->italics_on)
                          _write(ID_ITOF, printer->italics_off)
                          _write(ID_UNON, printer->underline_on)
                          _write(ID_UNOF, printer->underline_off)
                          _write(ID_UNOF, printer->underline_off)

                          _write(ID_CDON, printer->cond_on)
                          _write(ID_CDOF, printer->cond_off)

                          // nadruki

                          _write(ID_PRIN, printer->doc_print)
                          _write(ID_ORIG, printer->original_text)
                          _write(ID_COPY, printer->copy_text)
                          _write(ID_DUPE, printer->dupe_print)
                          _write(ID_DPTX, printer->dupe_text)
                          _write(ID_RECE, printer->norece_text)
                          _write(ID_PCS , printer->def_copies)


                          _write(ID_SWW , printer->doc_sww)
                          _write(ID_FEED, printer->formfeed)


                       PopChunk(MyIFFHandle);

                      }

                  CloseIFF(MyIFFHandle);
                  }
             else
                  {
                  DisplayBeep(0);
                  D(bug("*** OpenIFF() nie powiod�o si�\n"));
                  }

             Close(FileHandle);
             }
          else
             {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
             D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", DrukarkiFileName));
             }

          FreeIFF(MyIFFHandle);
          }
      else
          {
          DisplayBeep(0);
          D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
          }

     set(app, MUIA_Application_Sleep, FALSE);

     return(0);

}
//|

/// DodajDrukark�

int DodajDrukark�( struct PrinterList *printer )
{
/*
** Dodaje drukarke to listy drukarek -> sortuje alfabetycznie
*/

struct PrinterList *node;
int i = 0;


     if( !IsListEmpty(&drukarki) )
        {
        IDPrinter[ printer->pl_printer.ID ] = TRUE;

        for(node = (struct PrinterList *)drukarki.lh_Head; node->pl_node.mln_Succ; node = (struct PrinterList *)node->pl_node.mln_Succ, i++)
             {
             if( StrnCmp(MyLocale, printer->pl_printer.Name, node->pl_printer.Name, -1, SC_COLLATE2) < 0)
                {
                if((struct Node *)node->pl_node.mln_Pred)
                  Insert(&drukarki, (struct Node *)printer, (struct Node *)node->pl_node.mln_Pred);
                else
                  AddHead(&drukarki, (struct Node *)printer);

                return(i);
                }
             }
        }


     AddTail(&drukarki, (struct Node *)printer);
     return(MUIV_List_Insert_Bottom);

}
//|
/// GetPrinterID
// Zwraca ID drukarki o podanej nazwie
// jesli nie ma takiej drukarki, zwraca DEFAULT_PRINTER

short GetPrinterID( char *Nazwa )
{
struct PrinterList *prt;
short ID = DEFAULT_PRINTER;

     prt = Znajd�Drukark�Nazwa( Nazwa );
     if( prt )
        {
        ID = prt->pl_printer.ID;
        }

     return( ID );
}
//|
/// GetPrinterName
// Zwraca Nazwe drukarki o podanym ID

char * GetPrinterName( short ID )
{
struct PrinterList *prt;

     prt = Znajd�Drukark�ID( ID );
     if( !prt )
        Znajd�Drukark�ID( DEFAULT_PRINTER );

     return( prt->pl_printer.Name );
}
//|
/// Znajd�Drukark�Nazwa
struct PrinterList *Znajd�Drukark�Nazwa(char *Nazwa)
{
/*
** Przeszukuj� list� drukarek w poszukiwaniu tej
** o podanej nazwie
*/

struct PrinterList *node;

     if( Nazwa )
        {
        if( !IsListEmpty(&drukarki) )
             {
             for(node = (struct PrinterList *)drukarki.lh_Head; node->pl_node.mln_Succ; node = (struct PrinterList *)node->pl_node.mln_Succ)
             if( !stricmp(Nazwa, node->pl_printer.Name) )
                 return(node);
             }
        }

     return(NULL);
}
//|
/// Znajd�Drukark�ID
struct PrinterList *Znajd�Drukark�ID( ULONG ID )
{
/*
** Przeszukuj� list� drukarek w poszukiwaniu tej
** o podanym ID
*/

struct PrinterList *node;

     if( !IsListEmpty(&drukarki) )
        {
        for(node = (struct PrinterList *)drukarki.lh_Head; node->pl_node.mln_Succ; node = (struct PrinterList *)node->pl_node.mln_Succ)
        if( ID == node->pl_printer.ID )
             return(node);
        }

     return(NULL);
}
//|
/// WybierzIDDrukarki

short WybierzIDDrukarki(void)
{
/*
** Szuka wolnego ID drukarki
** zwraca -1 - jesli brak, albo wolny numer
*/

short ID = -1;
int  i;

     for( i=0; i<256; i++ )
        {
        if( IDPrinter[i] == 0)
             {
             ID = i;
             break;
             }
        }

     return(ID);

}

//|
/// Usu�Drukarki
int Usu�Drukarki(char Flags)
{
/*
** FLAGS:
**    UK_REMOVE_ALL     - wszystkie won
**    UK_REMOVE_DELETED - won tylko te Deleted
**
** Usuwa skasowane drukarki (je�li DeleteAll=FALSE)
** albo ca�� list� drukarek (je�li DeleteAll=TRUE)
**
** zwraca liczbe usuni�tych rekord�w
*/

struct PrinterList   *printer, *prt_next;
char   DeleteAll = ( Flags == UK_REMOVE_ALL );
int    removed = 0;

     if( !IsListEmpty( &drukarki ) )
        {
        char Delete = FALSE;

        for(printer = (struct PrinterList *)drukarki.lh_Head; printer->pl_node.mln_Succ; )
             {
             Delete = DeleteAll;

             if( DeleteAll == FALSE )
                  Delete = printer->Deleted;


             prt_next = (struct PrinterList *)printer->pl_node.mln_Succ;


             if(Delete)
                  {
                  IDPrinter[ printer->pl_printer.ID ] = FALSE;

                  Remove( (struct Node*)printer );

                  free(printer);
                  removed++;
                  }

             printer = prt_next;
             }
        }

     return( removed );
}
//|

/// Tablica kod�w kontrolnych ASCII

char   ASCIIControls[0x20] [0x04] = "NUL",  // $00
                                             "SOH",  // $01
                                             "STX",  // $02
                                             "ETX",  // $03
                                             "EOT",  // $04
                                             "ENQ",  // $05
                                             "ACK",  // $06
                                             "BEL",  // $07
                                             "BS",   // $08
                                             "HT",   // $09
                                             "LF",   // $0a
                                             "VT",   // $0b
                                             "FF",   // $0c
                                             "CR",   // $0d
                                             "SO",   // $0e
                                             "SI",   // $0f
                                             "DLE",  // $10
                                             "DC1",  // $11
                                             "DC2",  // $12
                                             "DC3",  // $13
                                             "DC4",  // $14
                                             "NAK",  // $15
                                             "SYN",  // $16
                                             "ETB",  // $17
                                             "CAN",  // $18
                                             "EM",   // $19
                                             "SUB",  // $1a
                                             "ESC",  // $1b
                                             "FS",   // $1c
                                             "GS",   // $1d
                                             "RS",   // $1e
                                             "US";   // $1f
//|
/// setprintercode
void setprintercode(Object *obj, char *src)
{
extern char ASCIIControls [0x20] [0x04];
char bajcik, buf[(5*PRT_STRING_LEN)+2];  // 5x bo kody steruj�ce mog� mie� posta� "<xxx>" - 5 bajt�w
int i, j=0, k;

     for(i=0;i<PRT_STRING_LEN;i++)
        {
        bajcik = src[i];
        if(bajcik == 0 || bajcik > 0x1f)
             {
             buf[j] = bajcik;
             if (bajcik == 0)
                  break;
             if (bajcik == '<')
                  {
                  buf[j+1] = '<';
                  j++;
                  }
             }
        else
             {
             k=0;
             buf[j] = '<';
             j++;
             while (ASCIIControls [bajcik] [k] != 0)
                  {
                  buf[j+k] = ASCIIControls [bajcik] [k];
                  k++;
                  }
             j=j+k;
             buf[j] = '>';
             }
        j++;
        }
     setstring(obj, buf);
}
//|
/// getprintercode
char * getprintercode(Object *obj, char *dest)
{
char bajcik, buf[(5*PRT_STRING_LEN)+2];  // 5x bo kody steruj�ce mog� mie� posta� "<xxx>" - 5 bajt�w
char kodzik[5];
int i, j, k, l;

static char error_msg[130];
static char *result = NULL;


     copystr(buf, obj);
     j=0;
     for(i=0;i<(PRT_STRING_LEN+2);i++)
     {
        if(buf[j] == 0)
             break;

        if(buf[j] == '<')
        {
             if(buf[j+1] == '<')
             {
                  buf[i] = '<';
                  j++;
             }
             else
             {
                  for(k=0; k<5; k++)
                  {
                       if (buf[j+1+k] == '>')
                            break;
                       else
                            kodzik[k] = buf[j+1+k];
                  }
                  kodzik[k] = 0;
                  j=j+1+k;

                  // za duzo znakow w < > (maks. 3)
                  if (k>3)
                  {
                       sprintf(error_msg, "\033b<%s\033n... (zbyt d�ugi kod)", kodzik);
                       result = error_msg;
                       goto exit;
                  }
                  else
                  {
                       for(l=1; l<0x20; l++)
                       {
                            if(strcmp(kodzik, ASCIIControls[l]) == 0)
                                 break;
                       }
                       // czy bledny kod?
                       if(strcmp(kodzik, ASCIIControls[l]) == 0)
                            buf[i] = l;
                       else
                       {
                       // siur. wypad z baru
                       sprintf(error_msg, "\033b<%s>\033n", kodzik);
                       result = error_msg;
                       goto exit;
                       }
                  }
             }
        }
        else
        {
             buf[i] = buf[j];
        }
        j++;
     }
     buf[i] = 0;
     strcpy(dest, buf);

exit:
     return( result );
}
//|

/// EDYCJA DRUKARKI WINDOW

#define MSG_PRT_ED_WIN_TITLE "Edycja parametr�w drukarki"

#define MSG_PRT_ED_OK     "F10 - _Ok"
#define MSG_PRT_ED_CANCEL "ESC - Ponie_chaj"


#define MSG_SET_PRINT_NAME_TITLE   "Nazwa opisu drukarki"
#define MSG_SET_PRINT_NAME         "Nazwa drukarki"
#define MSG_SET_PRINT_TYPE         "Typ"

#define MSG_SET_PRINT_PAGE         "Og�lne"
#define MSG_SET_PRINT_TITLE        "Ustawienia drukarki"
#define MSG_SET_PRINT_DEVICE       "S_terownik"
#define MSG_SET_PRINT_DEF_PATH     "_Domy�lny katalog"
#define MSG_SET_PRINT_UNIT         "_Jednostka"
#define MSG_SET_PRINT_WIDTH_TITLE  "Szeroko�� strony (znaki)"
#define MSG_SET_PRINT_WIDTH_WIDE   "Czcionka sz_eroka"
#define MSG_SET_PRINT_WIDTH_NORMAL "Czcionka _normalna"
#define MSG_SET_PRINT_WIDTH_COND   "Czcionka _w�ska"
#define MSG_SET_PRINT_CONV_TITLE   "Wydruk znak�w narodowych"
#define MSG_SET_PRINT_CONVERSION   "_Znaki narodowe"

#define MSG_SET_PRINT2_PAGE        "Kody"
#define MSG_SET_PRINT2_TITLE       "Kody steruj�ce drukarki"
#define MSG_SET_PRINT2_INFO        "\033c\0338Nazwy kod�w kontrolnych ASCII nale�y umieszcza�\nwewn�trz znak�w \"<>\" np.: <ESC>, <CAN>, <SOH> etc."
#define MSG_SET_PRINT2_ON          "W�."
#define MSG_SET_PRINT2_OFF         "Wy�."
#define MSG_SET_PRINT2_NORMAL      "Druk normalny"
#define MSG_SET_PRINT2_BOLD        "Wyt�uszczenie"
#define MSG_SET_PRINT2_ITALIC      "Pochylenie"
#define MSG_SET_PRINT2_UNDERLINE   "Podkre�lenie"
#define MSG_SET_PRINT2_CONDENSED   "Tryb Condensed"

#define MSG_SET_PRINT3_PAGE        "Uk�ad"
#define MSG_SET_PRINT3_PAGE_TITLE  "Stronicowanie wydruk�w"
#define MSG_SET_PRINT3_PAGE_SWITCH "_Dziel wydruki na strony"
#define MSG_SET_PRINT3_HEIGHT1     "Wysoko�� s_trony:"
#define MSG_SET_PRINT3_HEIGHT2     "linii"
#define MSG_SET_PRINT3_FOOTER      "_Nie drukuj stopki na\nwydrukach jednostronicowych"

#define MSG_SET_DOC_PAGE_1       "Wydruk"
#define MSG_SET_DOC_TITLE_1      "Drukowanie dokument�w"
#define MSG_SET_DOC_PRINT        "_Drukuj ORYGINA�/KOPIA"
#define MSG_SET_DOC_ORIGINAL     "Tekst _ORYGINA�"
#define MSG_SET_DOC_COPY         "Tekst _KOPIA"
#define MSG_SET_DOC_PRINT_DUPE   "D_rukuj tekst DUPLIKAT"
#define MSG_SET_DOC_DUPE         "Tekst DU_PLIKAT"
#define MSG_SET_DOC_NORECE       "Domy�lny _tekst ODBIORCA"
#define MSG_SET_DOC_UPVAT        "Upowa�nienie _VAT"
#define MSG_SET_DOC_COPIES       "_Liczba egzemplarzy"

#define MSG_SET_DOC_PAGE_2       "Wydruk 2"
#define MSG_SET_DOC_TITLE_2      "Drukowanie dokument�w 2"
#define MSG_SET_DOC_SWW          "_Zawsze drukuj pole PKWiU"
#define MSG_SET_DOC_FF           "W_ysuwaj stron�"

#define MSG_SET_ERR_CODE "Nieprawid�owy kod kontrolny: %s"
#define MSG_SET_ERR_OK   "*_Ok"

//|
/// EdycjaDrukarkiSetup

static Object
        *EdycjaDrukarkiWindow,      /* EDYCJA KONTRAHENTA        */

        *LV_Prt_Ed_Strony,
        *GR_Prt_Ed_Pages,

        *ST_Prt_Ed_Name,
        *CY_Prt_Ed_Type,

        *ST_Prt_Ed_Device,
        *ST_Prt_Ed_Unit,
        *GR_Prt_Ed_DevicePath,
        *ST_Prt_Ed_Default_Path,
        *BT_Prt_Ed_Default_Path,

        *ST_Prt_Ed_Width_Wide,
        *ST_Prt_Ed_Width_Normal,
        *ST_Prt_Ed_Width_Cond,
        *CH_Prt_Ed_PageMode,
        *ST_Prt_Ed_Height,
        *CY_Prt_Ed_Conv,

        *ST_Prt_Ed_Normal,
        *ST_Prt_Ed_Bold_On,
        *ST_Prt_Ed_Bold_Off,
        *ST_Prt_Ed_Italic_On,
        *ST_Prt_Ed_Italic_Off,
        *ST_Prt_Ed_Underline_On,
        *ST_Prt_Ed_Underline_Off,
        *ST_Prt_Ed_Condensed_On,
        *ST_Prt_Ed_Condensed_Off,

        *CH_Prt_Ed_PageMode,
        *ST_Prt_Ed_PageHeight,
        *CH_Prt_Ed_FooterOnLast,


        *CH_Prt_Ed_Print,
        *ST_Prt_Ed_Original,
        *ST_Prt_Ed_Copy,
        *ST_Prt_Ed_Dupe,
        *CH_Prt_Ed_Print_Dupe,
        *ST_Prt_Ed_NoRece,
        *ST_Prt_Ed_Copies,
        *CH_Prt_Ed_SWW,
        *CH_Prt_Ed_FF,


        *BT_Prt_Ed_Ok,
        *BT_Prt_Ed_Cancel;


static char *Print_ConversionTable[] =
{
     "AmigaPL",
     "Bez ogonk�w",
     "Nadrukowywanie",
     "ISO-8859-2",
     "IBM CP-852",
     "Mazovia",
     "Windows 95",
     NULL
};

static char *PrinterType[] =
{
     "Drukarka",
     "Plik",
     NULL
};

static const char *PrinterCodesArray[] =
{
//    "<NUL>",
     "<SOH>", "<STX>", "<ETX>", "<EOT>", "<ENQ>",
     "<ACK>", "<BEL>", "<BS>", "<HT>", "<LF>",
     "<VT>", "<FF>", "<CR>", "<SO>", "<SI>", "<DLE>",
     "<DC1>", "<DC2>", "<DC3>", "<DC4>", "<NAK>",
     "<SYN>", "<ETB>", "<CAN>", "<EM>", "<SUB>",
     "<ESC>", "<FS>", "<GS>", "<RS>", "<US>",

     NULL
};

static char *Prt_strony_nazwy[] =
           {
           MSG_SET_PRINT_PAGE,
           MSG_SET_PRINT3_PAGE,
           MSG_SET_PRINT2_PAGE,
           MSG_SET_DOC_PAGE_1,
           MSG_SET_DOC_PAGE_2,
           NULL
           };


char EdycjaDrukarkiSetup(struct PrinterList *PrinterList, char Mode)
{
struct Printer     *printer = &PrinterList->pl_printer;
ULONG  NoInput = FALSE;


#define PPH(a) PopphObject, MUIA_Popph_Array, PrinterCodesArray, MUIA_Popph_SingleColumn, TRUE, End
#define HVSPACE


///   EdycjaDrukarkiWindow

       if( Mode == 2 )
          NoInput = TRUE;

       EdycjaDrukarkiWindow = WindowObject,
                              MUIA_Window_Title      , MSG_PRT_ED_WIN_TITLE,
                              MUIA_Window_ID         , ID_WIN_DRUKARKA_EDIT,
                              MUIA_Window_ScreenTitle, ScreenTitle,
                              WindowContents,
                                 VGroup,

                                 Child, HGroup,
                                        GroupFrame, GroupBack,
//                                  GroupFrameT(MSG_SET_PRINT_NAME_TITLE),

                                        Child, MakeLabel2(MSG_SET_PRINT_NAME),
                                        Child, ST_Prt_Ed_Name = MakeString( PRINTER_DEV_LEN, MSG_SET_PRINT_NAME),

                                        Child, HGroup,
                                               MUIA_Weight, 1,

                                               Child, MakeLabel2(MSG_SET_PRINT_TYPE),
                                               Child, CY_Prt_Ed_Type = MakeCycle(PrinterType, MSG_SET_PRINT_TYPE),

                                               End,
                                        End,

                                 Child, HGroup,
                                        Child, LV_Prt_Ed_Strony = _ListviewObject,
                                               MUIA_Weight, 25,
                                               MUIA_CycleChain, TRUE,
                                               _MUIA_Listview_List, _ListObject,
                                                               InputListFrame,
                                                               _MUIA_List_AutoVisible, TRUE,
                                                               _MUIA_List_AdjustWidth, TRUE,
                                                               _MUIA_List_Format, "MIW=1 MAW=-1",
                                                               End,
                                               End,

                                        Child, BalanceObject, End,

//                                  Child, GR_Prt_Ed_Pages = RegisterGroup(GR_Prt_Ed_Ed_Titles),
                                        Child, GR_Prt_Ed_Pages = VGroup,
                                               MUIA_Weight, 75,
                                               MUIA_Group_PageMode, TRUE,

///                                        Drukarka 1
                                          Child, VGroup,
//                                         GroupFrameT(MSG_PRT_ED_TITLE_1),

                                                   Child, GR_Prt_Ed_DevicePath = HGroup,
                                                            GroupFrameT(MSG_SET_PRINT_TITLE),
                                                            MUIA_Group_PageMode, TRUE,

                                                            Child, HGroup,
                                                                    MUIA_Group_Columns, 2,
                                                                    Child, MakeLabel2(MSG_SET_PRINT_DEVICE),
                                                                    Child, ST_Prt_Ed_Device = MakeString(PRINTER_DEV_LEN, MSG_SET_PRINT_DEVICE),
                                                                    Child, MakeLabel2(MSG_SET_PRINT_UNIT),
                                                                    Child, ST_Prt_Ed_Unit   = MakeNumericString(4, MSG_SET_PRINT_UNIT),
                                                                    End,
                                                            Child, HGroup,
                                                                    Child, MakeLabel2(MSG_SET_PRINT_DEF_PATH),
                                                                    Child, HGroup,
                                                                             MUIA_Group_Spacing, 1,
                                                                             Child, ST_Prt_Ed_Default_Path = MakeString(PRT_PATH_LEN, NULL),
                                                                             Child, BT_Prt_Ed_Default_Path = PopButton2( MUII_PopDrawer, MSG_SET_PRINT_DEF_PATH ),
                                                                             End,
                                                                    End,
                                                            End,

                                                   Child, HGroup,
                                                            GroupFrameT(MSG_SET_PRINT_CONV_TITLE),
                                                            MUIA_Group_Columns, 2,

                                                            Child, MakeLabel2(MSG_SET_PRINT_CONVERSION),
                                                            Child, CY_Prt_Ed_Conv = MakeCycle(Print_ConversionTable, MSG_SET_PRINT_CONVERSION),

                                                            End,


                                                   Child, HVSpace,
                                                   End,
//|
///                                        Drukarka 2

                                          Child, VGroup,

                                                   Child, HGroup,
                                                            GroupFrameT(MSG_SET_PRINT_WIDTH_TITLE),
                                                            MUIA_Group_Columns, 2,

                                                            Child, MakeLabel2(MSG_SET_PRINT_WIDTH_WIDE),
                                                            Child, ST_Prt_Ed_Width_Wide   = MakeNumericString(3, MSG_SET_PRINT_WIDTH_WIDE),
                                                            Child, MakeLabel2(MSG_SET_PRINT_WIDTH_NORMAL),
                                                            Child, ST_Prt_Ed_Width_Normal   = MakeNumericString(3, MSG_SET_PRINT_WIDTH_NORMAL),
                                                            Child, MakeLabel2(MSG_SET_PRINT_WIDTH_COND),
                                                            Child, ST_Prt_Ed_Width_Cond = MakeNumericString(4, MSG_SET_PRINT_WIDTH_COND),

                                                            End,


                                                   Child, VGroup,
                                                            GroupFrameT(MSG_SET_PRINT3_PAGE_TITLE),

                                                            Child, ColGroup(2),
                                                                    Child, MakeLabel2(MSG_SET_PRINT3_PAGE_SWITCH),
                                                                    Child, HGroup,
                                                                             Child, CH_Prt_Ed_PageMode = MakeCheck(MSG_SET_PRINT3_PAGE_SWITCH),
                                                                             Child, HVSpace,
                                                                             End,

                                                                    Child, MakeLabel2(MSG_SET_PRINT3_HEIGHT1),
                                                                    Child, HGroup,
                                                                             Child, ST_Prt_Ed_PageHeight = MakeNumericString(4, MSG_SET_PRINT3_HEIGHT1),
                                                                             Child, MakeLabel2(MSG_SET_PRINT3_HEIGHT2),
                                                                             End,

                                                                    Child, MakeLabel2(MSG_SET_PRINT3_FOOTER),
                                                                    Child, HGroup,
                                                                             Child, CH_Prt_Ed_FooterOnLast = MakeCheck(MSG_SET_PRINT3_FOOTER),
                                                                             Child, HVSpace,
                                                                             End,

                                                                    End,

                                                             End,

                                                   Child, HVSpace,
                                                   End,
//|
///                                        Drukarka 3

                                          Child, VGroup,


                                                              Child, BarTitle(MSG_SET_PRINT2_TITLE),

/*
                                                              Child, TextObject,
                                                                       TextFrame, TextBack,
                                                                       MUIA_Text_Contents, MSG_SET_PRINT2_INFO,
                                                                       MUIA_Font, MUIV_Font_Tiny,
                                                                       End,
*/

                                                              Child, VGroup,
                                                                       GroupFrameT(MSG_SET_PRINT2_NORMAL),

//                                                         Child, BarTitle(MSG_SET_PRINT2_NORMAL),
                                                                       Child, ColGroup(2),
                                                                                Child, MakeLabel2(MSG_SET_PRINT2_ON),
//                                                                Child, ST_Prt_Ed_Normal = MakeString(PRT_STRING_LEN, NULL),
                                                                                Child, ST_Prt_Ed_Normal = PPH( PRT_STRING_LEN ),



                                                                                End,

                                                                       Child, BarTitle(MSG_SET_PRINT2_BOLD),
                                                                       Child, ColGroup(4),
                                                                                Child, MakeLabel2(MSG_SET_PRINT2_ON),
                                                                                Child, ST_Prt_Ed_Bold_On = PPH( PRT_STRING_LEN ),
                                                                                Child, MakeLabel2(MSG_SET_PRINT2_OFF),
                                                                                Child, ST_Prt_Ed_Bold_Off = PPH( PRT_STRING_LEN ),
                                                                                End,

                                                                       Child, BarTitle(MSG_SET_PRINT2_ITALIC),
                                                                       Child, ColGroup(4),
                                                                                Child, MakeLabel2(MSG_SET_PRINT2_ON),
                                                                                Child, ST_Prt_Ed_Italic_On = PPH( PRT_STRING_LEN ),
                                                                                Child, MakeLabel2(MSG_SET_PRINT2_OFF),
                                                                                Child, ST_Prt_Ed_Italic_Off = PPH( PRT_STRING_LEN ),
                                                                                End,

                                                                       Child, BarTitle(MSG_SET_PRINT2_UNDERLINE),
                                                                       Child, ColGroup(4),
                                                                                Child, MakeLabel2(MSG_SET_PRINT2_ON),
                                                                                Child, ST_Prt_Ed_Underline_On = PPH( PRT_STRING_LEN ),
                                                                                Child, MakeLabel2(MSG_SET_PRINT2_OFF),
                                                                                Child, ST_Prt_Ed_Underline_Off = PPH( PRT_STRING_LEN ),
                                                                                End,

                                                                       Child, BarTitle(MSG_SET_PRINT2_CONDENSED),
                                                                       Child, ColGroup(4),
                                                                                Child, MakeLabel2(MSG_SET_PRINT2_ON),
                                                                                Child, ST_Prt_Ed_Condensed_On = PPH( PRT_STRING_LEN ),
                                                                                Child, MakeLabel2(MSG_SET_PRINT2_OFF),
                                                                                Child, ST_Prt_Ed_Condensed_Off = PPH( PRT_STRING_LEN ),
                                                                                End,

                                                                       End,


                                                   Child, HVSpace,
                                                   End,
//|
///                                        Wydruki

                                                     Child, VGroup,
                                                          Child, HGroup,
                                                                   GroupFrameT(MSG_SET_DOC_TITLE_1),
                                                                   MUIA_Group_Columns, 2,

                                                                   Child, MakeLabel2(MSG_SET_DOC_PRINT),
                                                                   Child, HGroup,
                                                                            Child, CH_Prt_Ed_Print = MakeCheck(MSG_SET_DOC_PRINT),
                                                                            Child, HVSpace,
                                                                            End,

                                                                   Child, MakeLabel2(MSG_SET_DOC_ORIGINAL),
                                                                   Child, ST_Prt_Ed_Original = MakeString(ORIGINAL_COPY_LEN, MSG_SET_DOC_ORIGINAL),

                                                                   Child, MakeLabel2(MSG_SET_DOC_COPY),
                                                                   Child, ST_Prt_Ed_Copy     = MakeString(ORIGINAL_COPY_LEN, MSG_SET_DOC_COPY),

                                                                   Child, MakeLabel2(MSG_SET_DOC_PRINT_DUPE),
                                                                   Child, HGroup,
                                                                            Child, CH_Prt_Ed_Print_Dupe = MakeCheck(MSG_SET_DOC_PRINT_DUPE),
                                                                            Child, HVSpace,
                                                                            End,
                                                                   Child, MakeLabel2(MSG_SET_DOC_DUPE),
                                                                   Child, ST_Prt_Ed_Dupe = MakeString(ORIGINAL_COPY_LEN, MSG_SET_DOC_DUPE),

                                                                   Child, MakeLabel2(MSG_SET_DOC_NORECE),
                                                                   Child, ST_Prt_Ed_NoRece = MakeString(NORECE_LEN, MSG_SET_DOC_NORECE),

                                                                   Child, MakeLabel2(MSG_SET_DOC_COPIES),
                                                                   Child, ST_Prt_Ed_Copies = MakeNumericString(2, MSG_SET_DOC_COPIES),
                                                                   End,

#ifdef HVSPACE
                                                              Child, HVSpace,
#endif
                                                              End,
//|
///                                        Wydruki 2

                                                     Child, VGroup,
                                                          Child, HGroup,
                                                                   GroupFrameT(MSG_SET_DOC_TITLE_2),
                                                                   MUIA_Group_Columns, 2,

                                                                   Child, MakeLabel2(MSG_SET_DOC_SWW),
                                                                   Child, HGroup,
                                                                            Child, CH_Prt_Ed_SWW = MakeCheck(MSG_SET_DOC_SWW),
                                                                            Child, HVSpace,
                                                                            End,

                                                                   Child, MakeLabel2(MSG_SET_DOC_FF),
                                                                   Child, HGroup,
                                                                            Child, CH_Prt_Ed_FF = MakeCheck(MSG_SET_DOC_FF),
                                                                            Child, HVSpace,
                                                                            End,
                                                                   End,

#ifdef HVSPACE
                                                                   Child, HVSpace,
#endif
                                                              End,
//|

                                                   End,
                                          End,

                                 Child, HGroup,
                                          Child, BT_Prt_Ed_Ok     = TextButton(MSG_PRT_ED_OK),
                                          Child, BT_Prt_Ed_Cancel = TextButton(MSG_PRT_ED_CANCEL),
                                          End,

                                 End,
                           End;
//|


     if( ! EdycjaDrukarkiWindow )
        return( FALSE );


     // nie mozna edytowac nazwy drukarki domyslnej
     if( printer->ID == DEFAULT_PRINTER )
        _disable( ST_Prt_Ed_Name );



     DoMethod(LV_Prt_Ed_Strony, MUIM_List_Insert, Prt_strony_nazwy, -1, MUIV_List_Insert_Bottom);
     DoMethod(LV_Prt_Ed_Strony, MUIM_Notify, _MUIA_List_Active, MUIV_EveryTime, GR_Prt_Ed_Pages, 3, MUIM_Set, MUIA_Group_ActivePage, MUIV_TriggerValue);
     set(LV_Prt_Ed_Strony, _MUIA_List_Active, 0);

     DoMethod(CH_Prt_Ed_Print, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_Prt_Ed_Original, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue);
     DoMethod(CH_Prt_Ed_Print, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_Prt_Ed_Copy, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue);
     DoMethod(CH_Prt_Ed_Print_Dupe, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_Prt_Ed_Dupe, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue);

     DoMethod( CY_Prt_Ed_Type, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, GR_Prt_Ed_DevicePath, 3, MUIM_Set, MUIA_Group_ActivePage, MUIV_TriggerValue );

     DoMethod( BT_Prt_Ed_Default_Path, MUIM_Notify, MUIA_Pressed, FALSE, app, 2, MUIM_Application_ReturnID, ID_SELECT );


     // view mode?
     if( Mode == 2 )
        {
        _disable( CH_Prt_Ed_PageMode );
        _disable( CY_Prt_Ed_Conv );
        _disable( CH_Prt_Ed_PageMode );
        _disable( CH_Prt_Ed_FooterOnLast );

        _disable( BT_Prt_Ed_Ok );

        set( EdycjaDrukarkiWindow, MUIA_Window_ActiveObject, LV_Prt_Ed_Strony );
        }
     else
        {
        set( EdycjaDrukarkiWindow, MUIA_Window_ActiveObject, ST_Prt_Ed_Name );
        }


     // values

     setstring( ST_Prt_Ed_Name, printer->Name );
     setcycle(  CY_Prt_Ed_Type, printer->Type );

     // Drukarka

     setstring(ST_Prt_Ed_Default_Path   , printer->default_path);

     setstring(ST_Prt_Ed_Device         , printer->printer_device);
     setnumstring(ST_Prt_Ed_Unit        , printer->printer_unit);
     setcycle(CY_Prt_Ed_Conv            , printer->printer_charset);


     // Drukarka 2

     setprintercode(ST_Prt_Ed_Normal, printer->normal);
     setprintercode(ST_Prt_Ed_Bold_On, printer->bold_on);
     setprintercode(ST_Prt_Ed_Bold_Off, printer->bold_off);
     setprintercode(ST_Prt_Ed_Italic_On, printer->italics_on);
     setprintercode(ST_Prt_Ed_Italic_Off, printer->italics_off);
     setprintercode(ST_Prt_Ed_Underline_On, printer->underline_on);
     setprintercode(ST_Prt_Ed_Underline_Off, printer->underline_off);

     setprintercode(ST_Prt_Ed_Condensed_On, printer->cond_on);
     setprintercode(ST_Prt_Ed_Condensed_Off, printer->cond_off);


     // Drukarka 3

     setnumstring(ST_Prt_Ed_Width_Wide  , printer->printer_width_wide);
     setnumstring(ST_Prt_Ed_Width_Normal, printer->printer_width_normal);
     setnumstring(ST_Prt_Ed_Width_Cond  , printer->printer_width_cond);

     setcheckmark(CH_Prt_Ed_PageMode    , printer->page_mode);
     setnumstring(ST_Prt_Ed_PageHeight  , printer->page_height);
     setcheckmark(CH_Prt_Ed_FooterOnLast, printer->skip_1st_page_footer);


     // Wydruki

     setcheckmark(CH_Prt_Ed_Print     , printer->doc_print);
     setstring(ST_Prt_Ed_Original     , printer->original_text);
     setstring(ST_Prt_Ed_Copy         , printer->copy_text);
     setcheckmark(CH_Prt_Ed_Print_Dupe, printer->dupe_print);
     setstring(ST_Prt_Ed_Dupe         , printer->dupe_text);
     setstring(ST_Prt_Ed_NoRece       , printer->norece_text);
     setnumstring(ST_Prt_Ed_Copies    , printer->def_copies);

     // Wydruki 2

     setcheckmark(CH_Prt_Ed_SWW       , printer->doc_sww);
     setcheckmark(CH_Prt_Ed_FF        , printer->formfeed);




     /* EDYCJA DRUKAREK */

     DoMethod(EdycjaDrukarkiWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

     DoMethod(EdycjaDrukarkiWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_0);
     DoMethod(EdycjaDrukarkiWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_1);
     DoMethod(EdycjaDrukarkiWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_2);
     DoMethod(EdycjaDrukarkiWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

     DoMethod(BT_Prt_Ed_Ok          , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
     DoMethod(BT_Prt_Ed_Cancel      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


     _attachwin( EdycjaDrukarkiWindow );

     return( TRUE );

}
//|
/// EdycjaDrukarkiFinish
char EdycjaDrukarkiFinish(struct PrinterList *PrinterList, char Mode )
{
/*
** sprawdza poprawno�� wprowadzonych danych
** dot. produktu. Zwraca FALSE, je�li co�
** jest nie tak, TRUE, je�li wszystkie
** elementy s� poprawne, oraz REFRESH,
** je�li nazwa produktu zosta�a zmieniona
** (istotne tylko podczas edycji produktu)
*/

struct Printer  *printer = &PrinterList->pl_printer;
char   result = TRUE;
ULONG  pos_counter = 0;

#define _set_error(a)  {char *code = a; pos_counter++; if( code != NULL ) { MUI_Request( app, EdycjaDrukarkiWindow, 0, TITLE, MSG_OK, MSG_SET_ERR_CODE, code ); return( FALSE ); } }


     if(strlen((char *)xget(ST_Prt_Ed_Name, MUIA_String_Contents)) == 0)
        {
        set(GR_Prt_Ed_Pages, MUIA_Group_ActivePage, 0);
        set(EdycjaDrukarkiWindow, MUIA_Window_ActiveObject, ST_Prt_Ed_Name);
        return(FALSE);
        }


     // czy drukarka o takiej nazwie juz istnieje?
     {
     struct PrinterList *tmp;
     char * nazwa = (char *)xget(ST_Prt_Ed_Name, MUIA_String_Contents);

     if(Mode)
       {
       Remove( (struct Node *)PrinterList );
       tmp = Znajd�Drukark�Nazwa( nazwa );
       DodajDrukark�( PrinterList );
       }
     else
       {
       tmp = Znajd�Drukark�Nazwa( nazwa );
       }

     if(tmp)
        {
        set(GR_Prt_Ed_Pages, MUIA_Group_ActivePage, 0);
        set(EdycjaDrukarkiWindow, MUIA_Window_ActiveObject, ST_Prt_Ed_Name);
        return(FALSE);
        }
     }


     // validacja kod�w (lame, trza poprawic moze kiedys...

     {
     char tmp[ PRT_STRING_LEN ];

     _set_error( getprintercode(ST_Prt_Ed_Normal, tmp ) )
     _set_error( getprintercode(ST_Prt_Ed_Normal, tmp ) )
     _set_error( getprintercode(ST_Prt_Ed_Bold_On, tmp ) )
     _set_error( getprintercode(ST_Prt_Ed_Bold_Off, tmp ) )
     _set_error( getprintercode(ST_Prt_Ed_Italic_On, tmp ) )
     _set_error( getprintercode(ST_Prt_Ed_Italic_Off, tmp ) )
     _set_error( getprintercode(ST_Prt_Ed_Underline_On, tmp ) )
     _set_error( getprintercode(ST_Prt_Ed_Underline_Off, tmp  ) )

     _set_error( getprintercode(ST_Prt_Ed_Condensed_On, tmp ) )
     _set_error( getprintercode(ST_Prt_Ed_Condensed_Off, tmp ) )
     }



     // copy back

     copystr( printer->Name, ST_Prt_Ed_Name );
     printer->Type = getcycle( CY_Prt_Ed_Type );


     // Drukarka

     copystr(printer->printer_device, ST_Prt_Ed_Device);
     printer->printer_unit = getnumstr(ST_Prt_Ed_Unit);

     copystr(printer->default_path, ST_Prt_Ed_Default_Path);


     // Drukarka 2

     getprintercode(ST_Prt_Ed_Normal, printer->normal );
     getprintercode(ST_Prt_Ed_Normal, printer->normal );
     getprintercode(ST_Prt_Ed_Bold_On, printer->bold_on);
     getprintercode(ST_Prt_Ed_Bold_Off, printer->bold_off);
     getprintercode(ST_Prt_Ed_Italic_On, printer->italics_on);
     getprintercode(ST_Prt_Ed_Italic_Off, printer->italics_off);
     getprintercode(ST_Prt_Ed_Underline_On, printer->underline_on);
     getprintercode(ST_Prt_Ed_Underline_Off, printer->underline_off);

     getprintercode(ST_Prt_Ed_Condensed_On, printer->cond_on);
     getprintercode(ST_Prt_Ed_Condensed_Off, printer->cond_off);


     // Drukarka 3

     printer->printer_width_wide   = getnumstr(ST_Prt_Ed_Width_Wide);
     printer->printer_width_normal = getnumstr(ST_Prt_Ed_Width_Normal);
     printer->printer_width_cond   = getnumstr(ST_Prt_Ed_Width_Cond);
     printer->printer_charset      = getcycle(CY_Prt_Ed_Conv);

     printer->page_mode            = getcheckmark(CH_Prt_Ed_PageMode);
     printer->page_height          = getnumstr(ST_Prt_Ed_PageHeight);
     printer->skip_1st_page_footer = getcheckmark(CH_Prt_Ed_FooterOnLast);


     // Wydruki

     printer->doc_print = getcheckmark(CH_Prt_Ed_Print);
     copystr(printer->original_text, ST_Prt_Ed_Original);
     copystr(printer->copy_text, ST_Prt_Ed_Copy);
     printer->dupe_print = getcheckmark(CH_Prt_Ed_Print_Dupe);
     copystr(printer->dupe_text, ST_Prt_Ed_Dupe);
     copystr(printer->norece_text, ST_Prt_Ed_NoRece);
     printer->def_copies = getnumstr(ST_Prt_Ed_Copies);


     // Wydruki 2

     printer->doc_sww = getcheckmark(CH_Prt_Ed_SWW);
     printer->formfeed = getcheckmark(CH_Prt_Ed_FF);




     return(result);
}
//|
/// EdycjaDrukarki
char EdycjaDrukarki(struct PrinterList *printer, char Mode)
{
/*
** Mode:
**    0 - create - tworzenie nowego printera
**    1 - edit   - edycja istniejacych danych
**    2 - view   - przegladanie (bez edycji) danych
*/

char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;


     _sleep( TRUE );

     if( ! EdycjaDrukarkiSetup(printer, Mode) )
        {
        DisplayBeep(0);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        _sleep(FALSE);
        return(NULL);
        }

/*
     set(MagazynWindow  , MUIA_Window_ActiveObject, LV_Mag_Produkty);
     set(MagazynWindow  , MUIA_Window_DefaultObject, LV_Mag_Produkty);
*/

     if(WinOpen(EdycjaDrukarkiWindow))
          {
          while(running)
            {
            long ID = DoMethod(app, MUIM_Application_Input, &signal);
            switch(ID)
               {
               case ID_PAGE_0:
                  set(GR_Prt_Ed_Pages, MUIA_Group_ActivePage, 0);
                  break;
               case ID_PAGE_1:
                  set(GR_Prt_Ed_Pages, MUIA_Group_ActivePage, 1);
                  break;
               case ID_PAGE_2:
                  set(GR_Prt_Ed_Pages, MUIA_Group_ActivePage, 2);
                  break;


               case ID_SELECT:
                  {
                  char *file = RequestAslFile( (char *)xget(ST_Prt_Ed_Default_Path, MUIA_String_Contents), TRUE, TRUE );

                  if( file )
                       setstring( ST_Prt_Ed_Default_Path, file );
                  }
                  break;


               case ID_OK:
                  result = EdycjaDrukarkiFinish( printer, Mode );

                  if(result == FALSE)
                     {
                     DisplayBeep(0);
                     }
                  else
                     {
                     running = FALSE;
                     }
                  break;


               case ID_CANCEL:
                  running = FALSE;
                  break;

               }
            if(running && signal) Wait(signal);
            }

          }
      else
          {
          DisplayBeep(NULL);
          MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
          }


     _detachwin( EdycjaDrukarkiWindow );
     _sleep( FALSE );

     return(result);

}
//|

/// DRUKARKI WINDOW STRINGS

#define MSG_PRT_WIN_TITLE "Drukarki"

#define MSG_PRT_DODAJ   "F1 - Dod_aj"
#define MSG_PRT_EDYTUJ  "F2 - _Edytuj"
#define MSG_PRT_SKASUJ  "DEL - _Usu�"

#define MSG_PRT_OK     "F10 - Zapisz definicje drukarek"
#define MSG_PRT_CANCEL "ESC - Anuluj wszystkie zmiany"

#define MSG_PRT_SAVE     "\033cNa pewno chcesz zapisa� aktualny\nstan listy drukarek?"
#define MSG_PRT_SAVE_GAD "*_Tak, zapisz|_Nie"

#define MSG_PRT_CANCEL_REQ     "\033cNa pewno chcesz zako�czy� edycj�\nbazy drukarek i uniewa�ni�\nwszystkie wprowadzone do� zmiany?"
#define MSG_PRT_CANCEL_REQ_GAD "_Tak, zako�cz|*_Nie"

#define MSG_PRT_DELETED "Dane drukarki \"%s\"\ns� aktualnie zaznaczone jako \033bSKASOWANE\033n,\ni nie mog� by� modyfikowane! Je�li chcesz dokona�\nw nich jakichkolwiek zmian, musisz je najpierw odkasowa�.\nCzy chcesz to zrobi� teraz?"
#define MSG_PRT_DELETED_GAD "*_Tak, odkasuj|_Nie"

//|
/// EdycjaDrukarekSetup

static Object
        *DrukarkiWindow,

        *LV_Prt_Drukarki,
        *BT_Prt_Dodaj,
        *BT_Prt_Usu�,
        *BT_Prt_Edytuj,

        *BT_Prt_Ok,
        *BT_Prt_Cancel;


char EdycjaDrukarekSetup( void )
{
struct PrinterList *PrtL;

///   DrukarkiWindow

       DrukarkiWindow = WindowObject,
                              MUIA_Window_Title      , MSG_PRT_WIN_TITLE,
                              MUIA_Window_ID         , ID_WIN_DRUKARKI,
                              MUIA_Window_ScreenTitle, ScreenTitle,
                              WindowContents,
                                 VGroup,

///                          Lista drukarek
                                          Child, VGroup,
                                                   GroupFrame,
                                                   MUIA_Weight, 60,
                                                   Child, LV_Prt_Drukarki = _ListviewObject,
//                                         MUIA_CycleChain, TRUE,
                                                          _MUIA_List_AutoVisible, TRUE,
                                                          _MUIA_Listview_List, Golem_PrtListObject,
                                                               End,
                                                          End,


                                                   Child, HGroup,
                                                            Child, BT_Prt_Dodaj  = TextButton(MSG_PRT_DODAJ),
                                                            Child, BT_Prt_Edytuj = TextButton(MSG_PRT_EDYTUJ),
                                                            Child, BT_Prt_Usu�   = TextButton(MSG_PRT_SKASUJ),

                                                            End,

                                                   End,
//|

                                 Child, HGroup,
                                          Child, BT_Prt_Ok     = TextButton(MSG_PRT_OK),
                                          Child, BT_Prt_Cancel = TextButton(MSG_PRT_CANCEL),
                                          End,

                                 End,
                           End;
//|

     if( ! DrukarkiWindow )
        return( FALSE );


     if( !IsListEmpty( &drukarki ) )
        {
        for(PrtL = (struct PrinterList *)drukarki.lh_Head; PrtL->pl_node.mln_Succ; PrtL = (struct PrinterList *)PrtL->pl_node.mln_Succ)
           DoMethod( LV_Prt_Drukarki, _MUIM_List_InsertSingle, PrtL, _MUIV_List_Insert_Sorted);
        }


     // values


     _disable(BT_Prt_Ok);

     set(DrukarkiWindow, MUIA_Window_ActiveObject , LV_Prt_Drukarki);
     set(DrukarkiWindow, MUIA_Window_DefaultObject, LV_Prt_Drukarki);



     // notifications

     DoMethod(DrukarkiWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
     DoMethod(BT_Prt_Ok        , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
     DoMethod(BT_Prt_Cancel    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

     DoMethod(BT_Prt_Dodaj     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);
     DoMethod(BT_Prt_Edytuj    ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_EDIT);
     DoMethod(BT_Prt_Usu�      ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_DELETE);
     DoMethod(DrukarkiWindow ,MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ADD);
     DoMethod(DrukarkiWindow ,MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_EDIT);
     DoMethod(LV_Prt_Drukarki   ,MUIM_Notify, _MUIA_Listview_DoubleClick, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_EDIT);
     DoMethod(DrukarkiWindow ,MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_DELETE);
     DoMethod(DrukarkiWindow ,MUIM_Notify, MUIA_Window_InputEvent, "control del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_DELETE_AND_SKIP);

     DoMethod(DrukarkiWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);
//    DoMethod(DrukarkiWindow, MUIM_Notify, MUIA_Window_InputEvent, "esc", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CANCEL);


     _attachwin( DrukarkiWindow );
     return( TRUE );

}
//|
/// EdycjaDrukarek

#define PRT_ZMIANY(x)  {ZmianyWDrukarkach += x; set(BT_Prt_Ok, MUIA_Disabled, (ZmianyWDrukarkach == 0)); }

char EdycjaDrukarek(void)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;
long  ZmianyWDrukarkach = 0;

     set(app, MUIA_Application_Sleep, TRUE);

     if( ! EdycjaDrukarekSetup() )
        {
        DisplayBeep(0);
        MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
        _sleep(FALSE);
        return(NULL);
        }


     if(WinOpen(DrukarkiWindow))
          {
          while(running)
            {
            long ID = DoMethod(app, MUIM_Application_Input, &signal);
            switch(ID)
               {
               case ID_ADD:
                  {
                  struct PrinterList *prt;
                  short  ID = WybierzIDDrukarki();

                  if( ID != -1 )
                       {
                       if((prt = calloc(1, sizeof(struct PrinterList))) != NULL)
                           {
                           SetDefPrtSettings( prt );
                           prt->pl_printer.ID = ID;

                           if( EdycjaDrukarki(prt, FALSE) )
                              {
                              long pos = DodajDrukark�(prt);

                              DoMethod(LV_Prt_Drukarki, _MUIM_List_InsertSingle, prt, pos);
                              set(LV_Prt_Drukarki, _MUIA_List_Active, pos);

                              IDPrinter[ ID ] = TRUE;

                              PRT_ZMIANY(1)
                              }
                           }
                       else
                           {
                           DisplayBeep(0);
                           MUI_Request(app, DrukarkiWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                           }
                      }
                  else
                      {
                      DisplayBeep(0);
                      // 256 drukarek to max!
                      }
                  }
                  break;


               case ID_EDIT:
                  {
                  if(xget(LV_Prt_Drukarki, _MUIA_List_Active) != _MUIV_List_Active_Off)
                       {
                       struct PrinterList *prt;

                       DoMethod(LV_Prt_Drukarki, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &prt);


                       if(prt->Deleted)
                          {
                          if(MUI_Request(app, DrukarkiWindow, 0, TITLE, MSG_PRT_DELETED_GAD, MSG_PRT_DELETED, prt->pl_printer.Name))
                            {
                            prt->Deleted = FALSE;
                            DoMethod(LV_Prt_Drukarki, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                            PRT_ZMIANY(-1)
                            }
                          else
                            break;
                          }

                       switch(EdycjaDrukarki(prt, TRUE))
                            {
                            case TRUE:
                                   DoMethod(LV_Prt_Drukarki, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                                   PRT_ZMIANY(1)
                                   break;

                            case REFRESH:
                                   {
                                   long pos;
                                   set(LV_Prt_Drukarki, _MUIA_List_Quiet, TRUE);

                                   DoMethod(LV_Prt_Drukarki, _MUIM_List_Remove, _MUIV_List_Remove_Active);

                                   Remove((struct Node*)prt);
                                   pos = DodajDrukark�(prt);

                                   DoMethod(LV_Prt_Drukarki, _MUIM_List_InsertSingle, prt, pos);
                                   set(LV_Prt_Drukarki, _MUIA_List_Active, pos);

                                   set(LV_Prt_Drukarki, _MUIA_List_Quiet, FALSE);
                                   PRT_ZMIANY(1)
                                   }
                                   break;
                            }
                          }
                  }
                  break;

               case ID_DELETE:
               case ID_DELETE_AND_SKIP:
                  {
                  if(xget(LV_Prt_Drukarki, _MUIA_List_Active) != _MUIV_List_Active_Off)
                       {
                       struct PrinterList *prt;

                       DoMethod(LV_Prt_Drukarki, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &prt);

                       prt->Deleted = ((prt->Deleted + 1) & 0x1);

                       if(prt->Deleted)
                          PRT_ZMIANY(1)
                       else
                          PRT_ZMIANY(-1)

                       DoMethod(LV_Prt_Drukarki, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);

                       if(ID == ID_DELETE_AND_SKIP)
                            set(LV_Prt_Drukarki, _MUIA_List_Active, _MUIV_List_Active_Down);
                       }
                  }
                  break;


               case ID_OK:
                  if(ZmianyWDrukarkach)
                     {
                     if(MUI_Request(app, DrukarkiWindow, 0, TITLE, MSG_PRT_SAVE_GAD, MSG_PRT_SAVE))
                       {
                       Usu�Drukarki( UK_REMOVE_DELETED );
                       ZapiszDrukarki();

                       // dodajemy drukarke domy�ln� jesli zosta�a usuni�ta
                       AddDefPrinter();

                       result  = TRUE;
                       running = FALSE;
                       }
                     }
                  break;


               case ID_CANCEL:
                  if(ZmianyWDrukarkach)
                       {
                       if(MUI_Request(app, DrukarkiWindow, 0, TITLE, MSG_PRT_CANCEL_REQ_GAD, MSG_PRT_CANCEL_REQ))
                          WczytajDrukarki();
                       else
                          break;
                       }
                  running = FALSE;
                  break;

               }
            if(running && signal) Wait(signal);
            }

          }
      else
          {
          DisplayBeep(NULL);
          MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
          }

     _detachwin( DrukarkiWindow );
     set(app, MUIA_Application_Sleep, FALSE);

     return(result);

}
//|

