
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 


/*** includes ***/

#include <clib/alib_protos.h>
#include <clib/graphics_protos.h>
#include <clib/utility_protos.h>
#include <clib/muimaster_protos.h>
#include <pragmas/muimaster_pragmas.h>
#include <libraries/mui.h>
#if MUIMASTER_VLATEST <= 14
#include <mui/mui33_mcc.h>
#endif

#define MYDEBUG 0

//#include <mui/betterstring_mcc.h>
#include <mui/muiundoc.h>

#include "string.h"
#include "stdio.h"
#include "golem_windowid.h"
#include "golem_datestring_mcc.h"
#include "golem_calendar_mcc.h"
#include "golem_structs.h"

#define NO_MT_MACROS
#include "golem_macros.h"
#include "golem_protos.h"
#include <mui/mui.h>


#define __NAME      "Golem_DateString"
#define CLASS       MUIC_Golem_DateString
#define SUPERCLASS  MUIC_Group

void _XCEXIT(int code) {}


struct Data
{
	Object *ST_Data;
	Object *BT_Calendar;
	Object *WI_Calendar;
	char   Buffer[3 * DATE_LEN];

};

#include "golem_datestring_revision.h"

#define UserLibID VERSTAG " � 1998-2002 Marcin Orlowski <carlos@wfmh.org.pl>"
#define MASTERVERSION 14

#define MCC_USES_IFFPARSE
#include "mccheader.c"

#define TITLE "WFMH Golem"

enum{ ID_OK = 50000,
	  ID_CANCEL,

	};

/// xget
LONG xget(Object *obj, ULONG attribute)
{
LONG x;

	get(obj, attribute, &x);
	return(x);
}
//|

/// OpenCalendar
ULONG ASM _OpenCalendar(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) struct opSet *msg)
{
struct Data *data = INST_DATA(cl,obj);

Object *app = (Object *)xget(obj, MUIA_ApplicationObject);
Object *BT_Ok, *BT_Cancel;

Object *win = WindowObject,
			   MUIA_Window_ID   , ID_WIN_CALENDAR,
			   MUIA_Window_Title, TITLE,

				 WindowContents,
					VGroup,
					Child, Golem_CalendarObject, End,

					Child, HGroup,
						   Child, BT_Ok     = MUI_MakeObject(MUIO_Button, "_Ok"),
						   Child, BT_Cancel = MUI_MakeObject(MUIO_Button, "Ponie_chaj"),
						   End,
					End,


				 End;


	if( win )
	   {
	   char  running = TRUE;
	   ULONG signal  = 0;
	   char  result  = FALSE;

	   set(BT_Ok    , MUIA_CycleChain, TRUE);
	   set(BT_Cancel, MUIA_CycleChain, TRUE);

	   _attachwin( win );

	   DoMethod(BT_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, obj, 2 ,MUIM_Application_ReturnID, ID_OK);
	   DoMethod(BT_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, obj, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


	   _sleep(TRUE);
	   set( win, MUIA_Window_Open, TRUE );

	   while(running)
		  {
		  long ID = DoMethod(app, MUIM_Application_Input, &signal);

		  switch(ID)
			{
			case ID_OK:
			   running = FALSE;
			   break;

			case ID_CANCEL:
			   running = FALSE;
			   break;
			}
		  if(running && signal) Wait(signal);
		  }

	   _detachwin( win );
	   _sleep(FALSE);
	   }
	else
	   {
//       DisplayBeep(0);
	   }



	return(0);
}
//|

/// OM_NEW

ULONG ASM _New(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) struct opSet *msg)
{
struct Data *data;

Object *string;
Object *button;


	obj = (Object *)DoSuperNew(cl,obj,
					MUIA_Group_Horiz, TRUE,
					MUIA_Group_Spacing, 1,

					Child, string = StringObject, StringFrame,
//                                    MUIA_ControlChar, ParseHotKey(num),
									  MUIA_String_MaxLen, DATE_LEN,
									  MUIA_CycleChain, TRUE,
									  MUIA_String_AdvanceOnCR, TRUE,
									End,

					Child, button = MUI_MakeObject(MUIO_PopButton, MUII_PopUp),

					TAG_DONE);

		if( obj )
		   {
		   /*** init data ***/
		   data = INST_DATA(cl, obj);

		   data->ST_Data     = string;
		   data->BT_Calendar = button;

		   data->Buffer[0] = 0;


		   set(button, MUIA_CycleChain, TRUE);
		   _disable(button);


		   // let's find out if Calendar class can be used
		   {
		   Object *o1;
		   char   result = FALSE;

		   D(bug("Checking for Calendar.mcc...\n"));

		   o1 = Golem_CalendarObject, End;

		   D(bug("  Calendar.mcc opened...\n"));


		   if( o1 )
			   {
			   // month navigator present. let's give it a try...

			   DoMethod( button, MUIM_Notify, MUIA_Pressed, FALSE, obj, 1, MUIM_DateString_OpenCalendar );
			   _enable(button);

			   MUI_DisposeObject( o1 );
			   }
		   }



		   /*** trick to set arguments ***/
		   msg->MethodID = OM_SET;
		   DoMethodA(obj, (Msg)msg);
		   msg->MethodID = OM_NEW;
		   }


		return((ULONG)obj);
}
//|
/*
/// OM_SET

ULONG ASM _Set(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct Data *data = INST_DATA(cl,obj);
struct TagItem *tags,*tag;


	for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
	   {
	   switch(tag->ti_Tag)
		  {

//        DoMethod(data->ST_Numer, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, obj, 3, MUIM_Set, MUIA_BankString_Account, MUIV_TriggerValue);
/*
		  case MUIA_BankString_Account:
			   {
			   strncpy( data->Numer, (char *)tag->ti_Data, CUST_BANK_LEN );

			   DoMethod(obj, MUIM_BankString_Validate);

			   return(TRUE);
			   }
		   break;
*/

		   }
		}

	return(DoSuperMethodA(cl, obj, msg));
}

//|
*/
// SET

/// Dispatcher
ULONG ASM SAVEDS _Dispatcher(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{

	switch (msg->MethodID)
	   {
	   case OM_NEW: return(_New (cl, obj, (APTR)msg));
//       case OM_SET: return(_Set (cl, obj, (APTR)msg));
//       case OM_GET: return(_Get (cl, obj, (APTR)msg));

	   case MUIM_DateString_OpenCalendar : return(_OpenCalendar (cl, obj, (APTR)msg));

	   }

	return((ULONG)DoSuperMethodA(cl,obj,msg));

}
//|

