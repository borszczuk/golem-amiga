
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
** $Id: banki_convert.c,v 1.1 2003/01/01 20:40:53 carl-os Exp $
*/

#include <stdio.h>

#define buffer_size 2048

#define FALSE 0
#define TRUE  1

void main(char argc, char *argv[])
{
FILE *in, *out;
int spacja_won = 0;
int in_count = 0, out_count = 0;
int converted = 0;



        if(argc != 3)
                {
                printf("Usage: %s IN OUT\n", argv[0]);
                exit(20);
                }

        if( in = fopen(argv[1], "rb+") )
                {
            setvbuf(in, NULL, _IOFBF, buffer_size);

                if( out = fopen(argv[2], "wb+") )
                        {
                        char magic_count = 0;
                        char spacja_byla = FALSE;
                        int lame_buffer;
                        char was_printable;

                        setvbuf(out, NULL, _IOFBF, buffer_size);


                        while( (lame_buffer = fgetc( in )) != EOF )
                                {
                                char isprintable;

                                in_count++;

                                if( lame_buffer == 136 && magic_count == 0 )
                                   {
                                   magic_count++;
                                   continue;
                                   }
                                else
                                   if( lame_buffer == 12 && magic_count == 1 )
                                     {
                                     magic_count++;
                                     continue;
                                     }
                                   else
                                     if( lame_buffer == 8 && magic_count == 2 )
                                       {
                                       magic_count++;
                                       continue;
                                       }
                                     else
                                       {
                                       if( lame_buffer == 0 && magic_count == 3 )
                                        {
                                        magic_count++;
                                        continue;
                                        }
                                       }

                                if( magic_count == 4 )
                                  {
                                  // recod begins...

                                  }


                                if( lame_buffer >=32 )
                                  isprintable = TRUE;
                                else
                                  isprintable = FALSE;


                                if( isprintable )
                                   {
                                   fputc( lame_buffer, out );
                                   was_printable = TRUE;
                                   }
                                else
                                   {
                                   if( was_printable == TRUE )
                                      {
                                      was_printable = FALSE;
                                      fprintf(out, "\n");
                                      }
                                   else
                                      {
                                      // skipping next printable

                                      }
                                   }

                                }


                        fclose( out );
                        }
                     else
                      {
                      printf( "can't open OUT file: %s\n", argv[2] );
                      exit(20);
                      }


                fclose( in );


                printf( "%ld znak�w wesz�o, %ld wysz�o\n", in_count, out_count );
                printf( "%ld spacji (og��em) polecia�o precz\n", spacja_won );
                printf( "%ld twardych spacji przemianowano na zwyk�e\n", converted );
                }
             else
               {
               printf( "can't open IN file: %s\n", argv[1] );
               exit(20);
               }

}
