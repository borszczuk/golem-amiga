
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 
/*
$Id: golem_parser.c,v 1.1 2003/01/01 20:40:53 carl-os Exp $.
*/

#include "golem.h"

char temp_buffer[1024];

/// cmd_liczbapozycji
void cmd_liczbapozycji(struct Command *cmd)
{
                sprintf(cmd->cmd_out, "%ld", xget(cmd->dp->lv_object, _MUIA_List_Entries) );
}
//|


/// cmd_normal
void cmd_normal(struct Command *cmd)
{
                cmd->FontType = FONT_NORMAL;
                cmd->Attr     = ATTR_NORMAL;
                sprintf(cmd->cmd_out, cmd->dp->prt->normal);
}
//|
/// cmd_boldon
void cmd_boldon(struct Command *cmd)
{
                cmd->Attr = cmd->Attr | ATTR_BOLD;
                sprintf(cmd->cmd_out, cmd->dp->prt->bold_on);
}
//|
/// cmd_boldoff
void cmd_boldoff(struct Command *cmd)
{
                cmd->Attr = cmd->Attr & (0xff - ATTR_BOLD);
                sprintf(cmd->cmd_out, cmd->dp->prt->bold_off);
}
//|
/// cmd_condon
void cmd_condon(struct Command *cmd)
{
                cmd->FontType = FONT_CONDENSED;
                sprintf(cmd->cmd_out, cmd->dp->prt->cond_on);
}
//|
/// cmd_condoff
void cmd_condoff(struct Command *cmd)
{
                cmd->FontType = FONT_NORMAL;
                sprintf(cmd->cmd_out, cmd->dp->prt->cond_off);
}
//|
/// cmd_iton
void cmd_iton(struct Command *cmd)
{
                cmd->Attr = cmd->Attr | ATTR_ITAL;
                sprintf(cmd->cmd_out, cmd->dp->prt->italics_on);
}
//|
/// cmd_itoff
void cmd_itoff(struct Command *cmd)
{
                cmd->Attr = cmd->Attr & (0xff - ATTR_ITAL);
                sprintf(cmd->cmd_out, cmd->dp->prt->italics_off);
}
//|
/// cmd_underon
void cmd_underon(struct Command *cmd)
{
                cmd->Attr = cmd->Attr | ATTR_UNDER;
                sprintf(cmd->cmd_out, cmd->dp->prt->underline_on);
}
//|
/// cmd_underoff
void cmd_underoff(struct Command *cmd)
{
                cmd->Attr = cmd->Attr & (0xff - ATTR_UNDER);
                sprintf(cmd->cmd_out, cmd->dp->prt->underline_off);
}
//|
/// cmd_centeron
void cmd_centeron(struct Command *cmd)
{
                cmd->CenterOn = TRUE;
}
//|
/// cmd_centeroff
void cmd_centeroff(struct Command *cmd)
{
                cmd->CenterOn = FALSE;
}
//|

/// cmd_wartnettozw
void cmd_wartnettozw(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk� (cmd->dp->netto_zw)));
}
//|
/// cmd_wartnetto0
void cmd_wartnetto0(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_0)));
}
//|
/// cmd_wartnetto3
void cmd_wartnetto3(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_3)));
}
//|
/// cmd_wartnetto7
void cmd_wartnetto7(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_7)));
}
//|
/// cmd_wartnetto17
void cmd_wartnetto17(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_17)));
}
//|
/// cmd_wartnetto22
void cmd_wartnetto22(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_22)));
}
//|
/// cmd_wartvatzw
void cmd_wartvatzw(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->vat_zw)));
}
//|
/// cmd_wartvat0
void cmd_wartvat0(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->vat_0)));
}
//|
/// cmd_wartvat3
void cmd_wartvat3(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->vat_3)));
}
//|
/// cmd_wartvat7
void cmd_wartvat7(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->vat_7)));
}
//|
/// cmd_wartvat17
void cmd_wartvat17(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->vat_17)));
}
//|
/// cmd_wartvat22
void cmd_wartvat22(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->vat_22)));
}
//|
/// cmd_wartbruttozw
void cmd_wartbruttozw(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_zw + cmd->dp->vat_zw)));
}
//|
/// cmd_wartbrutto0
void cmd_wartbrutto0(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_0 + cmd->dp->vat_0)));
}
//|
/// cmd_wartbrutto3
void cmd_wartbrutto3(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_3 + cmd->dp->vat_3)));
}
//|
/// cmd_wartbrutto7
void cmd_wartbrutto7(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_7 + cmd->dp->vat_7)));
}
//|
/// cmd_wartbrutto17
void cmd_wartbrutto17(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_17 + cmd->dp->vat_17)));
}
//|
/// cmd_wartbrutto22
void cmd_wartbrutto22(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�( cmd->dp->netto_22 + cmd->dp->vat_22)) );
}
//|

/// cmd_wartnetto
void cmd_wartnetto(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_zw +
                                                                                                                                                  cmd->dp->netto_0 +
                                                                                                                                                  cmd->dp->netto_3 +
                                                                                                                                                  cmd->dp->netto_7 +
                                                                                                                                                  cmd->dp->netto_17 +
                                                                                                                                                  cmd->dp->netto_22)) );
}
//|
/// cmd_wartvat
void cmd_wartvat(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->vat_zw +
                                                                                                                                                  cmd->dp->vat_0  +
                                                                                                                                                  cmd->dp->vat_3  +
                                                                                                                                                  cmd->dp->vat_7  +
                                                                                                                                                  cmd->dp->vat_17 +
                                                                                                                                                  cmd->dp->vat_22)) );
}
//|
/// cmd_wartbrutto
void cmd_wartbrutto(struct Command *cmd)
{
                strcpy(cmd->cmd_out, Price2String(ObetnijKo�c�wk�(cmd->dp->netto_zw + cmd->dp->vat_zw +
                                                                                                                                                  cmd->dp->netto_0  + cmd->dp->vat_0  +
                                                                                                                                                  cmd->dp->netto_3  + cmd->dp->vat_3  +
                                                                                                                                                  cmd->dp->netto_7  + cmd->dp->vat_7  +
                                                                                                                                                  cmd->dp->netto_17 + cmd->dp->vat_17 +
                                                                                                                                                  cmd->dp->netto_22 + cmd->dp->vat_22)) );
}
//|

/// cmd_wystawca
void cmd_wystawca(struct Command *cmd)
{
                strcpy(cmd->cmd_out, cmd->dp->OsobaWystawiaj�ca);
}
//|
/// cmd_odbiorca
void cmd_odbiorca(struct Command *cmd)
{

                if(cmd->dp->OsobaOdbieraj�ca[0] != '\0')
                   strcpy(cmd->cmd_out, cmd->dp->OsobaOdbieraj�ca);
                else
                   strcpy(cmd->cmd_out, cmd->dp->prt->norece_text);

}
//|

/// cmd_info1
void cmd_info1(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.text_1);
}
//|
/// cmd_info2
void cmd_info2(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.text_2);
}
//|
/// cmd_info3
void cmd_info3(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.text_3);
}
//|
/// cmd_info4
void cmd_info4(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.text_4);
}
//|
/// cmd_info5
void cmd_info5(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.text_5);
}
//|
/// cmd_info6
void cmd_info6(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.text_6);
}
//|
/// cmd_info7
void cmd_info7(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.text_7);
}
//|
/// cmd_info8
void cmd_info8(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.text_8);
}
//|
/// cmd_info9
void cmd_info9(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.text_9);
}
//|
/// cmd_info10
void cmd_info10(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.text_0);
}
//|

/// cmd_sp_nazwa1
void cmd_spnazwa1(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.wystawca.Nazwa1);
}
//|
/// cmd_sp_nazwa2
void cmd_spnazwa2(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.wystawca.Nazwa2);
}
//|
/// cmd_sp_ulica
void cmd_spulica(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.wystawca.Ulica);
}
//|
/// cmd_sp_kod
void cmd_spkod(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.wystawca.Kod);
}
//|
/// cmd_sp_miasto
void cmd_spmiasto(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.wystawca.Miasto);
}
//|
/// cmd_sp_adres
void cmd_spadres(struct Command *cmd)
{
char tmp[CUST_ADRES_LEN + CUST_KOD_LEN + CUST_MIASTO_LEN + 10];

                sprintf(tmp, "%s, %s %s", settings.wystawca.Ulica, settings.wystawca.Kod, settings.wystawca.Miasto);
                strcpy(cmd->cmd_out, tmp);
}
//|
/// cmd_sp_tel
void cmd_sptel(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.wystawca.Telefon);
}
//|
/// cmd_sp_fax
void cmd_spfax(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.wystawca.Fax);
}
//|
/// cmd_sp_telfax
void cmd_sptelfax(struct Command *cmd)
{
char tmp[CUST_TEL_LEN + CUST_FAX_LEN + 10] = {0};
char w=0;


                w += (settings.wystawca.Telefon[0] != '\0') * 1;
                w += (settings.wystawca.Fax[0] != '\0') * 2;

                if(w)
                   {
/*
                   switch(w)
                                   {
                                   case 1:
                                                                sprintf(line[line_index], "Telefon   : ");
                                                                break;
                                   case 2:
                                                                sprintf(line[line_index], "Fax       : ");
                                                                break;
                                   case 3:
                                                                sprintf(line[line_index], "Tel./Fax  : ");
                                                                break;
                                   }
*/

                                if(w & 1)
                                   sprintf(tmp, "%s", settings.wystawca.Telefon);

                                if(w & 2)
                                   {
                                   if(w & 1)
                                                   strcat(tmp, ", ");

                                   strcat(tmp, settings.wystawca.Fax);
                                   }
                   }

                strcpy(cmd->cmd_out, tmp);
}
//|
/// cmd_sp_email
void cmd_spemail(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.wystawca.Email);
}
//|
/// cmd_sp_nip
void cmd_spnip(struct Command *cmd)
{
                strcpy(cmd->cmd_out, NIP2Str(settings.wystawca.NIP, settings.wystawca.Sp��kaCywilna));
}
//|
/// cmd_sp_regon
void cmd_spregon(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.wystawca.Regon);
}
//|
/// cmd_sp_bank
void cmd_spbank(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.wystawca.Bank);
}
//|
/// cmd_sp_konto
void cmd_spkonto(struct Command *cmd)
{
                strcpy(cmd->cmd_out, settings.wystawca.Konto);
}
//|

/// cmd_nab_nazwa1
void cmd_nabnazwa1(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klient.Nazwa1);
}
//|
/// cmd_nab_nazwa2
void cmd_nabnazwa2(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klient.Nazwa2);
}
//|
/// cmd_nab_ulica
void cmd_nabulica(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klient.Ulica);
}
//|
/// cmd_nab_kod
void cmd_nabkod(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klient.Kod);
}
//|
/// cmd_nab_miasto
void cmd_nabmiasto(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klient.Miasto);
}
//|
/// cmd_nab_adres
void cmd_nabadres(struct Command *cmd)
{
char tmp[ CUST_ADRES_LEN + CUST_KOD_LEN + CUST_MIASTO_LEN + 10 ];

                if(cmd->dp->Odbiorca)
                                {
                                sprintf(tmp, "%s, %s %s", cmd->dp->Odbiorca->kl_klient.Ulica, cmd->dp->Odbiorca->kl_klient.Kod, cmd->dp->Odbiorca->kl_klient.Miasto);
                                strcpy(cmd->cmd_out, tmp);
                                }
}
//|
/// cmd_nab_tel
void cmd_nabtel(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klient.Telefon);
}
//|
/// cmd_nab_fax
void cmd_nabfax(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klient.Fax);
}
//|
/// cmd_nab_telfax
void cmd_nabtelfax(struct Command *cmd)
{
char tmp[CUST_TEL_LEN + CUST_FAX_LEN + 10] = {0};
char w=0;

                if(cmd->dp->Odbiorca)
                   {
                   w += (cmd->dp->Odbiorca->kl_klient.Telefon[0] !='\0') * 1;
                   w += (cmd->dp->Odbiorca->kl_klient.Fax[0] !='\0') * 2;

                   if(w)
                                   {
/*
                                   switch(w)
                                                   {
                                                   case 1:
                                                                                sprintf(line[line_index], "Telefon   : ");
                                                                                break;
                                                   case 2:
                                                                                sprintf(line[line_index], "Fax       : ");
                                                                                break;
                                                   case 3:
                                                                                sprintf(line[line_index], "Tel./Fax  : ");
                                                                                break;
                                                   }
*/

                                                if(w & 1)
                                                   sprintf(tmp, cmd->dp->Odbiorca->kl_klient.Telefon);

                                                if(w & 2)
                                                   {
                                                   if(w & 1)
                                                                   strcat(tmp, ", ");

                                                   strcat(tmp, cmd->dp->Odbiorca->kl_klient.Fax);
                                                   }
                                   }

                   strcpy(cmd->cmd_out, tmp);
                   }
}
//|
/// cmd_nab_email
void cmd_nabemail(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klient.Email);
}
//|
/// cmd_nab_nip
void cmd_nabnip(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, NIP2Str(cmd->dp->Odbiorca->kl_klient.NIP, cmd->dp->Odbiorca->kl_klient.Sp��kaCywilna));
}
//|
/// cmd_nab_regon
void cmd_nabregon(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klient.Regon);
}
//|
/// cmd_nab_bank
void cmd_nabbank(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klient.Bank);
}
//|
/// cmd_nab_konto
void cmd_nabkonto(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klient.Konto);
}
//|
/// cmd_nab_uwagi
void cmd_nabuwagi(struct Command *cmd)
{
                if(cmd->dp->Odbiorca)
                                if( cmd->dp->Odbiorca->kl_klientExt.Uwagi )
                                   strcpy(cmd->cmd_out, cmd->dp->Odbiorca->kl_klientExt.Uwagi);
}
//|

/// cmd_lp
void cmd_lp(struct Command *cmd)
{
        if( cmd->dp->MultiLineCnt == 0 )
                sprintf(cmd->cmd_out, "%ld", cmd->dp->lv_index+1);
        cmd->dp->NeedRepeat = TRUE;
}
//|
/// cmd_index
void cmd_index(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   sprintf(cmd->cmd_out, "%s", prod->pl_prod.Index);
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_sww
void cmd_sww(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   strcpy(cmd->cmd_out, prod->pl_prod.SWW);
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_nazwa
void cmd_nazwa(struct Command *cmd)
{
struct ProductList *prod;
char   *nazwa;
char   format[15];
long   offset;
long   nazwa_len;
 
        DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
        
        if( prod )
                {
                nazwa  = prod->pl_prod.Nazwa;
                offset = cmd->dp->MultiLineCnt * cmd->width;
                nazwa_len = strlen( nazwa );
                 
                sprintf( format, "%%.%lds", cmd->width );
                sprintf( cmd->cmd_out, format, (char *)(nazwa+offset) );
                
//                D(bug( "CMD:nazwa: format: %s  Offset: %ld\n", format, offset ));
//                D(bug( "              out: '%s'\n", cmd->cmd_out ));
                 
 
                // sprawdzamy czy nie trza pokawalkowac na wiele linii
                
                cmd->dp->DoMultiLine = FALSE;
                 
                if( nazwa_len > cmd->width )
                        {
                        if( offset < nazwa_len )
                                {
                                
                                // sprawdzamy czy to nie byla ostatnia paczka, coby nie drukowac
                                // pustych linii. sprawdzamy nastepny offset czy nie poza zakresem
                                if( (offset + cmd->width) <= nazwa_len )
                                        cmd->dp->DoMultiLine = TRUE;
                                }
                        }

                cmd->dp->NeedRepeat = TRUE;
                }
}
//|
/*
/// cmd_nazwa
void cmd_nazwa(struct Command *cmd)
{
struct ProductList *prod;

                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   strcpy(cmd->cmd_out, prod->pl_prod.Nazwa);

                cmd->dp->NeedRepeat = TRUE;

}
//|
*/
/// cmd_jm
void cmd_jm(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   strcpy(cmd->cmd_out, prod->pl_prod.Jedn.Nazwa);
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_ilo��
void cmd_ilo��(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);

                if(prod)
                   {
                   if( prod->pl_prod.Jedn.Ca�kowita == TRUE )
                                 {
                                 sprintf(cmd->cmd_out, "%ld   ", (long)ceil(prod->pl_prod.Ilo��));  // 3spacje zamiast cz��ci u�amkowej ".xx"
                                 }
                   else
                                 {
                                 strcpy(cmd->cmd_out, Price2String(prod->pl_prod.Ilo��));
                                 }
                   }
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_cenajedn
void cmd_cenajedn(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   strcpy(cmd->cmd_out, Price2String(prod->pl_prod.Zakup + prod->pl_prod.Mar�a));
                }
                 
        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_cenajednrabat
void cmd_cenajednrabat(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   {
                   double warto�� = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;

                   strcpy(cmd->cmd_out, Price2String( warto�� - CalcRabat(warto��, prod->pl_prod.Rabat) ) );

//       printf("wart: %s, rabat: %s\n", Price2String(warto��), Price2String(CalcRabat(warto��, prod->pl_prod.Rabat)) );
                   }
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_cenajednbrutto
void cmd_cenajednbrutto(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   {
                   double warto��;
                   double vat;

                   warto�� = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
                   vat = CalcVat(warto��, prod->pl_prod.Vat);
                   strcpy(cmd->cmd_out, Price2String(warto�� + vat) );
                   }
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_cenajednbruttorabat
void cmd_cenajednbruttorabat(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   {
                   double cena_sprzeda�y;
                   double warto��;
                   double vat;

                   cena_sprzeda�y = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
                   warto�� = cena_sprzeda�y - CalcRabat(cena_sprzeda�y, prod->pl_prod.Rabat);
                   vat = CalcVat(warto��, prod->pl_prod.Vat);
                   strcpy(cmd->cmd_out, Price2String(warto�� + vat) );
                   }
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|

/// cmd_zakupcenajedn

// jednostkowa cena zakupu netto
void cmd_zakupcenajedn(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   strcpy(cmd->cmd_out, Price2String(prod->pl_prod.Zakup));
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_zakupkwotajedn

// kwota zakupu netto (*ilosc)
void cmd_zakupkwotajedn(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   strcpy(cmd->cmd_out, Price2String( prod->pl_prod.Zakup * prod->pl_prod.Ilo�� ));
                }
                 
        cmd->dp->NeedRepeat = TRUE;
}
//|
/// cmd_zakupkwotatotal

// kwota zakupu netto (*ilosc)
void cmd_zakupkwotatotal(struct Command *cmd)
{
struct ProductList *prod;

        strcpy(cmd->cmd_out, Price2String( cmd->dp->zakup_netto_total ));
}
//|

/// cmd_rabat
void cmd_rabat(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   strcpy(cmd->cmd_out, Price2String(prod->pl_prod.Rabat));
                }
                 
        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_rabatkwota
void cmd_rabatkwota(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   {
                   double warto�� = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;

                   strcpy( cmd->cmd_out, Price2String( CalcRabat(warto��, prod->pl_prod.Rabat) ) );
                   }
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_netto
void cmd_netto(struct Command *cmd)
{
/*
** warto�� netto
*/

struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   {
                   double cena_sprzeda�y = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
                   double warto��_netto;

                   warto��_netto = cena_sprzeda�y - CalcRabat(cena_sprzeda�y, prod->pl_prod.Rabat);
                   warto��_netto = prod->pl_prod.Ilo�� * warto��_netto;
                   strcpy(cmd->cmd_out, Price2String(warto��_netto) );
                   }
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_vat
void cmd_vat(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   strcpy(cmd->cmd_out, VatTable[prod->pl_prod.Vat]);
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_kwotavat
void cmd_kwotavat(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   {
                   double warto��        = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
                   double cena_sprzeda�y = prod->pl_prod.Ilo�� * ( warto�� - CalcRabat(warto��, prod->pl_prod.Rabat) );

                   strcpy(cmd->cmd_out, Price2String(CalcVat(cena_sprzeda�y, prod->pl_prod.Vat) ) );
                   }
                }
                
        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_kwotajednvat
void cmd_kwotajednvat(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   {
                   double warto��        = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
                   double cena_sprzeda�y = ( warto�� - CalcRabat(warto��, prod->pl_prod.Rabat) );

                   strcpy(cmd->cmd_out, Price2String(CalcVat(cena_sprzeda�y, prod->pl_prod.Vat) ) );
                   }
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|
/// cmd_brutto

// kwota brutto = ilosc * cena_netto - rabat + podatek
void cmd_brutto(struct Command *cmd)
{
struct ProductList *prod;

        if( cmd->dp->MultiLineCnt == 0 )
                {
                DoMethod(cmd->dp->lv_object, _MUIM_List_GetEntry, cmd->dp->lv_index, &prod);
                if(prod)
                   {
                   double cena_sprzeda�y;
                   double warto��;
                   double vat;

                   cena_sprzeda�y = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
                   warto�� = cena_sprzeda�y - CalcRabat(cena_sprzeda�y, prod->pl_prod.Rabat);
                   warto�� = prod->pl_prod.Ilo�� * warto��;
                   vat = CalcVat(warto��, prod->pl_prod.Vat);
                   strcpy(cmd->cmd_out, Price2String(warto�� + vat) );
                   }
                }

        cmd->dp->NeedRepeat = TRUE;

}
//|

/// cmd_datawyst
void cmd_datawyst(struct Command *cmd)
{
                strcpy(cmd->cmd_out, wfmh_Date2Str(&cmd->dp->DataWystawienia));
}
//|
/// cmd_datasprz
void cmd_datasprz(struct Command *cmd)
{
                strcpy(cmd->cmd_out, wfmh_Date2Str(&cmd->dp->DataSprzeda�y));
}
//|
/// cmd_datap�at
void cmd_datap�at(struct Command *cmd)
{
                strcpy(cmd->cmd_out, wfmh_Date2Str(&cmd->dp->DataP�atno�ci));
}
//|
/// cmd_typp�at
void cmd_typp�at(struct Command *cmd)
{
//   sprintf(cmd->cmd_out, "'%s'", cmd->dp->TypP�atno�ci);
   strcpy(cmd->cmd_out, cmd->dp->TypP�atno�ci);
}
//|

/// cmd_aktualnadata
void cmd_aktualnadata(struct Command *cmd)
{
   strcpy( cmd->cmd_out, wfmh_Date2Str( &cmd->dp->aktualna_data ) );
}
//|

/// cmd_s�wartbruttoz�
void cmd_s�wartbruttoz�(struct Command *cmd)
{
double total = cmd->dp->netto_zw + cmd->dp->vat_zw +
                                                   cmd->dp->netto_0  + cmd->dp->vat_0  +
                                                   cmd->dp->netto_7  + cmd->dp->vat_7  +
                                                   cmd->dp->netto_3  + cmd->dp->vat_3  +
                                                   cmd->dp->netto_17 + cmd->dp->vat_17 +
                                                   cmd->dp->netto_22 + cmd->dp->vat_22;

                D(bug("brutto s�ownie z�: %s\n", S�ownie( floor(total) ) ));

                strcpy( cmd->cmd_out, S�ownie( floor(total) ) );
}
//|
/// cmd_s�wartbruttogr
void cmd_s�wartbruttogr(struct Command *cmd)
{
double total = cmd->dp->netto_zw + cmd->dp->vat_zw +
                                                   cmd->dp->netto_0  + cmd->dp->vat_0  +
                                                   cmd->dp->netto_3  + cmd->dp->vat_3  +
                                                   cmd->dp->netto_7  + cmd->dp->vat_7  +
                                                   cmd->dp->netto_17 + cmd->dp->vat_17 +
                                                   cmd->dp->netto_22 + cmd->dp->vat_22;


                total = ObetnijKo�c�wk�(total);

                D(bug("brutto s�ownie gr: %s  (total=%s)\n", S�ownie( ((total-floor(total))*100) ), Price2String(total) ));

                strcpy( cmd->cmd_out, S�ownie( ((total - floor(total))*100) ) );
}
//|

/// cmd_docnr
void cmd_docnr(struct Command *cmd)
{
                strcpy(cmd->cmd_out, cmd->dp->NumerDokumentu);
}
//|
/// cmd_doctyp
void cmd_doctyp(struct Command *cmd)
{
// Kopia/Orygina� + ew. Duplikat

                // czy drukowa� co� takiego
                if(cmd->dp->prt->doc_print)
                   {
                   strcpy(cmd->cmd_out, "\0");

                   // orygina� czy kopia?
                   if(cmd->dp->NumerEgzemplarza == 1)
                                  strcpy(cmd->cmd_out, cmd->dp->prt->original_text);
                   else
                                  strcpy(cmd->cmd_out, cmd->dp->prt->copy_text);

                   // duplikat?
                   if(cmd->dp->Duplikat)
                                   {
                                   if(cmd->dp->prt->dupe_print)
                                                   {
                                                   // co� ju� wcze�niej do bufora posz�o, wi�c doklejamy spacj�
                                                   if(*cmd->cmd_out != 0)
                                                                  strcat(cmd->cmd_out, " ");

                                                   strcat(cmd->cmd_out, cmd->dp->prt->dupe_text);
                                                   }
                                   }

                   }
}
//|
/// cmd_paragonnr
void cmd_paragonnr(struct Command *cmd)
{
                strcpy(cmd->cmd_out, cmd->dp->NumerParagonu);
}
//|

/// cmd_golem
void cmd_golem(struct Command *cmd)
{
                sprintf(cmd->cmd_out, MSG_CMD_GOLEM, VERSIONREVISION);
}
//|



char dluz_naz0[256];
char dluz_naz1[256];
/// cmd_M_dluznaz
void cmd_M_dluznaz(struct Command *cmd)
{
int len = 0;
int total_len;

                memset( dluz_naz0, 0, sizeof(dluz_naz0) );
                memset( dluz_naz1, 0, sizeof(dluz_naz1) );

                sprintf( temp_buffer, "%s %s", cmd->dp->pp->Dluznik.Nazwa1,
                                                                                                                                   cmd->dp->pp->Dluznik.Nazwa2 );

                total_len = strlen( temp_buffer );
                memcpy( dluz_naz0, temp_buffer, min( cmd->width, total_len ) );
                strcat( dluz_naz0, "" );

                if( total_len > cmd->width )
                   {
                   memcpy( dluz_naz1, &temp_buffer[ cmd->width ], total_len - cmd->width );
                   strcat( dluz_naz1, "" );
                   }

                cmd->silent_command = TRUE;        // stealth mode
}
//|
/// cmd_dluznaz0
void cmd_dluznaz0( struct Command *cmd )
{
                strcpy( cmd->cmd_out, dluz_naz0 );
}
//|
/// cmd_dluznaz1
void cmd_dluznaz1( struct Command *cmd )
{
                strcpy( cmd->cmd_out, dluz_naz1 );
}
//|
char wier_naz0[256];
char wier_naz1[256];
/// cmd_M_wiernaz
void cmd_M_wiernaz(struct Command *cmd)
{
int len = 0;
int total_len;

                memset( wier_naz0, 0, sizeof(wier_naz0) );
                memset( wier_naz1, 0, sizeof(wier_naz1) );

                sprintf( temp_buffer, "%s %s", cmd->dp->pp->Wierzyciel.Nazwa1,
                                                                                                                                   cmd->dp->pp->Wierzyciel.Nazwa2 );

                total_len = strlen( temp_buffer );
                memcpy( wier_naz0, temp_buffer, min( cmd->width, total_len ) );
                strcat( wier_naz0, "" );

                if( total_len > cmd->width )
                   {
                   memcpy( wier_naz1, &temp_buffer[ cmd->width ], total_len - cmd->width );
                   strcat( wier_naz1, "" );
                   }

                cmd->silent_command = TRUE;        // stealth mode
}
//|
/// cmd_wiernaz0
void cmd_wiernaz0( struct Command *cmd )
{
                strcpy( cmd->cmd_out, wier_naz0 );
}
//|
/// cmd_wiernaz1
void cmd_wiernaz1( struct Command *cmd )
{
                strcpy( cmd->cmd_out, wier_naz1 );
}
//|

char dluz_adr0[256];
char dluz_adr1[256];
/// cmd_M_dluzadr
void cmd_M_dluzadr(struct Command *cmd)
{
int len = 0;
int total_len;

                memset( dluz_adr0, 0, sizeof(dluz_adr0) );
                memset( dluz_adr1, 0, sizeof(dluz_adr1) );

                strcpy( temp_buffer, cmd->dp->pp->Dluznik.Ulica );

                total_len = strlen( temp_buffer );
                memcpy( dluz_adr0, temp_buffer, min( cmd->width, total_len ) );
                strcat( dluz_adr0, "" );

                if( total_len > cmd->width )
                   {
                   memcpy( dluz_adr1, &temp_buffer[ cmd->width ], total_len - cmd->width );
                   strcat( dluz_adr1, "" );
                   }

                cmd->silent_command = TRUE;        // stealth mode
}
//|
/// cmd_dluzadr0
void cmd_dluzadr0( struct Command *cmd )
{
                strcpy( cmd->cmd_out, dluz_adr0 );
}
//|
/// cmd_dluzadr1
void cmd_dluzadr1( struct Command *cmd )
{
                strcpy( cmd->cmd_out, dluz_adr1 );
}
//|
/// cmd_dluzkod
void cmd_dluzkod(struct Command *cmd)
{
                strcpy(cmd->cmd_out, cmd->dp->pp->Dluznik.Kod);
}
//|
/// cmd_dluzmiasto
void cmd_dluzmiasto(struct Command *cmd)
{
                strcpy(cmd->cmd_out, cmd->dp->pp->Dluznik.Miasto);
}
//|

char wier_adr0[256];
char wier_adr1[256];
/// cmd_M_wieradr
void cmd_M_wieradr(struct Command *cmd)
{
int len = 0;
int total_len;

                memset( wier_adr0, 0, sizeof(wier_adr0) );
                memset( wier_adr1, 0, sizeof(wier_adr1) );

                strcpy( temp_buffer, cmd->dp->pp->Wierzyciel.Ulica );

                total_len = strlen( temp_buffer );
                memcpy( wier_adr0, temp_buffer, min( cmd->width, total_len ) );
                strcat( wier_adr0, "" );

                if( total_len > cmd->width )
                   {
                   memcpy( wier_adr1, &temp_buffer[ cmd->width ], total_len - cmd->width );
                   strcat( wier_adr1, "" );
                   }

                cmd->silent_command = TRUE;        // stealth mode
}
//|
/// cmd_wieradr0
void cmd_wieradr0( struct Command *cmd )
{
                strcpy( cmd->cmd_out, wier_adr0 );
}
//|
/// cmd_wieradr1
void cmd_wieradr1( struct Command *cmd )
{
                strcpy( cmd->cmd_out, wier_adr1 );
}
//|
/// cmd_wierkod
void cmd_wierkod(struct Command *cmd)
{
                strcpy(cmd->cmd_out, cmd->dp->pp->Wierzyciel.Kod);
}
//|
/// cmd_dluzmiasto
void cmd_wiermiasto(struct Command *cmd)
{
                strcpy(cmd->cmd_out, cmd->dp->pp->Wierzyciel.Miasto);
}
//|

char dluz_bank0[256];
char dluz_bank1[256];
/// cmd_M_dluzbank
void cmd_M_dluzbank(struct Command *cmd)
{
int len = 0;
int total_len;

                memset( dluz_bank0, 0, sizeof(dluz_bank0) );
                memset( dluz_bank1, 0, sizeof(dluz_bank1) );

                sprintf( temp_buffer, "%s", cmd->dp->pp->Dluznik.Bank);

                total_len = strlen( temp_buffer );
                memcpy( dluz_bank0, temp_buffer, min( cmd->width, total_len ) );
                strcat( dluz_bank0, "" );

                if( total_len > cmd->width )
                   {
                   memcpy( dluz_bank1, &temp_buffer[ cmd->width ], total_len - cmd->width );
                   strcat( dluz_bank1, "" );
                   }

                cmd->silent_command = TRUE;        // stealth mode
}
//|
/// cmd_dluzbank0
void cmd_dluzbank0( struct Command *cmd )
{
                strcpy( cmd->cmd_out, dluz_bank0 );
}
//|
/// cmd_dluzbank1
void cmd_dluzbank1( struct Command *cmd )
{
                strcpy( cmd->cmd_out, dluz_bank1 );
}
//|
char wier_bank0[256];
char wier_bank1[256];
/// cmd_M_wierbank
void cmd_M_wierbank(struct Command *cmd)
{
int len = 0;
int total_len;

                memset( wier_bank0, 0, sizeof(wier_bank0) );
                memset( wier_bank1, 0, sizeof(wier_bank1) );

                sprintf( temp_buffer, "%s", cmd->dp->pp->Wierzyciel.Bank);

                total_len = strlen( temp_buffer );
                memcpy( wier_bank0, temp_buffer, min( cmd->width, total_len ) );
                strcat( wier_bank0, "" );

                if( total_len > cmd->width )
                   {
                   memcpy( wier_bank1, &temp_buffer[ cmd->width ], total_len - cmd->width );
                   strcat( wier_bank1, "" );
                   }

                cmd->silent_command = TRUE;        // stealth mode
}
//|
/// cmd_wierbank0
void cmd_wierbank0( struct Command *cmd )
{
                strcpy( cmd->cmd_out, wier_bank0 );
}
//|
/// cmd_wierbank1
void cmd_wierbank1( struct Command *cmd )
{
                strcpy( cmd->cmd_out, wier_bank1 );
}
//|

char dluz_konto0[256];
char dluz_konto1[256];
/// cmd_M_dluzkonto
void cmd_M_dluzkonto(struct Command *cmd)
{
int len = 0;
int total_len;

                memset( dluz_konto0, 0, sizeof(dluz_konto0) );
                memset( dluz_konto1, 0, sizeof(dluz_konto1) );

                sprintf( temp_buffer, "%s", cmd->dp->pp->Dluznik.Konto );

                total_len = strlen( temp_buffer );
                memcpy( dluz_konto0, temp_buffer, min( cmd->width, total_len ) );
                strcat( dluz_konto0, "" );

                if( total_len > cmd->width )
                   {
                   memcpy( dluz_konto1, &temp_buffer[ cmd->width ], total_len - cmd->width );
                   strcat( dluz_konto1, "" );
                   }

                cmd->silent_command = TRUE;        // stealth mode
}
//|
/// cmd_dluzkonto0
void cmd_dluzkonto0( struct Command *cmd )
{
                strcpy( cmd->cmd_out, dluz_konto0 );
}
//|
/// cmd_dluzkonto1
void cmd_dluzkonto1( struct Command *cmd )
{
                strcpy( cmd->cmd_out, dluz_konto1 );
}
//|
char wier_konto0[256];
char wier_konto1[256];
/// cmd_M_wierkonto
void cmd_M_wierkonto(struct Command *cmd)
{
int len = 0;
int total_len;

                memset( wier_konto0, 0, sizeof(wier_konto0) );
                memset( wier_konto1, 0, sizeof(wier_konto1) );

                sprintf( temp_buffer, "%s", cmd->dp->pp->Wierzyciel.Konto);

                total_len = strlen( temp_buffer );
                memcpy( wier_konto0, temp_buffer, min( cmd->width, total_len ) );
                strcat( wier_konto0, "" );

                if( total_len > cmd->width )
                   {
                   memcpy( wier_konto1, &temp_buffer[ cmd->width ], total_len - cmd->width );
                   strcat( wier_konto1, "" );
                   }

                cmd->silent_command = TRUE;        // stealth mode
}
//|
/// cmd_wierkonto0
void cmd_wierkonto0( struct Command *cmd )
{
                strcpy( cmd->cmd_out, wier_konto0 );
}
//|
/// cmd_wierkonto1
void cmd_wierkonto1( struct Command *cmd )
{
                strcpy( cmd->cmd_out, wier_konto1 );
}
//|

char tytulem0[256];
char tytulem1[256];
/// cmd_M_tytulem
void cmd_M_tytulem(struct Command *cmd)
{
int len = 0;
int total_len;

                memset( tytulem0, 0, sizeof(tytulem0) );
                memset( tytulem1, 0, sizeof(tytulem1) );

                sprintf( temp_buffer, "%s", cmd->dp->pp->Tytulem);

                total_len = strlen( temp_buffer );
                memcpy( tytulem0, temp_buffer, min( cmd->width, total_len ) );
                strcat( tytulem0, "" );

                if( total_len > cmd->width )
                   {
                   memcpy( tytulem1, &temp_buffer[ cmd->width ], total_len - cmd->width );
                   strcat( tytulem1, "" );
                   }

                cmd->silent_command = TRUE;        // stealth mode
}
//|
/// cmd_tytulem0
void cmd_tytulem0( struct Command *cmd )
{
                strcpy( cmd->cmd_out, tytulem0 );
}
//|
/// cmd_tytulem1
void cmd_tytulem1( struct Command *cmd )
{
                strcpy( cmd->cmd_out, tytulem1 );
}
//|

/// cmd_kwota
void cmd_kwota(struct Command *cmd)
{
   strcpy(cmd->cmd_out, Price2String( cmd->dp->pp->Kwota ) );

}
//|
/// cmd_dataprzel
void cmd_dataprzel(struct Command *cmd)
{
//   strcpy( cmd->cmd_out, wfmh_Date2Str( &cmd->dp->pp->DataPlatnosci ) );

}
//|



/// commands
struct CmdArray commands[] =
{
///   dane produktu
                {"lp"                   , (void *)cmd_lp},
                {"index"                , (void *)cmd_index},
                {"nazwa"                , (void *)cmd_nazwa},
                {"jm"                   , (void *)cmd_jm},
                {"ilo��"                , (void *)cmd_ilo��},
                {"cenajedn"             , (void *)cmd_cenajedn},
                {"cenajednrabat"        , (void *)cmd_cenajednrabat},
                {"cenajednbrutto"       , (void *)cmd_cenajednbrutto},
                {"cenajednbruttorabat"  , (void *)cmd_cenajednbruttorabat},
                {"rabat"                , (void *)cmd_rabat},
                {"rabatkwota"           , (void *)cmd_rabatkwota},
                {"netto"                , (void *)cmd_netto},
                {"vat"                  , (void *)cmd_vat},
                {"kwotavat"             , (void *)cmd_kwotavat},
                {"kwotajednvat"         , (void *)cmd_kwotajednvat},
                {"brutto"               , (void *)cmd_brutto},
                {"sww"                  , (void *)cmd_sww},
                {"pkwiu"                , (void *)cmd_sww},
                {"zakupcenajedn"        , (void *)cmd_zakupcenajedn},
                {"zakupkwotajedn"       , (void *)cmd_zakupkwotajedn},
                {"zakupkwotatotal"      , (void *)cmd_zakupkwotatotal},
//|
///   kody kontrolne
                {"normal"               , (void *)cmd_normal},
                {"bold_on"              , (void *)cmd_boldon},
                {"bold_off"             , (void *)cmd_boldoff},
                {"cond_on"              , (void *)cmd_condon},
                {"cond_off"             , (void *)cmd_condoff},
                {"it_on"                , (void *)cmd_iton},
                {"it_off"               , (void *)cmd_itoff},
                {"italic_on"            , (void *)cmd_iton},
                {"italic_off"           , (void *)cmd_itoff},
                {"under_on"             , (void *)cmd_underon},
                {"under_off"            , (void *)cmd_underoff},
                {"center_on"            , (void *)cmd_centeron},
                {"center_off"           , (void *)cmd_centeroff},
//|

                {"doc_nr"               , (void *)cmd_docnr},
                {"doc_typ"              , (void *)cmd_doctyp},
                {"paragon_nr"           , (void *)cmd_paragonnr},

                {"s�_wartbrutto_z�"     , (void *)cmd_s�wartbruttoz�},
                {"s�_wartbrutto_gr"     , (void *)cmd_s�wartbruttogr},

                {"datawyst"             , (void *)cmd_datawyst},
                {"datasprz"             , (void *)cmd_datasprz},
                {"datap�at"             , (void *)cmd_datap�at},
                {"typp�at"              , (void *)cmd_typp�at},
                
                {"LiczbaPozycji"        , (void *)cmd_liczbapozycji},

///   Sprzedawca
                {"sp_nazwa1"            , (void *)cmd_spnazwa1},
                {"sp_nazwa2"            , (void *)cmd_spnazwa2},
                {"sp_ulica"             , (void *)cmd_spulica},
                {"sp_kod"               , (void *)cmd_spkod},
                {"sp_miasto"            , (void *)cmd_spmiasto},
                {"sp_adres"             , (void *)cmd_spadres},
                {"sp_tel"               , (void *)cmd_sptel},
                {"sp_fax"               , (void *)cmd_spfax},
                {"sp_telfax"            , (void *)cmd_sptelfax},
                {"sp_email"             , (void *)cmd_spemail},
                {"sp_nip"               , (void *)cmd_spnip},
                {"sp_regon"             , (void *)cmd_spregon},
                {"sp_bank"              , (void *)cmd_spbank},
                {"sp_konto"             , (void *)cmd_spkonto},
//|
///   Nabywca
                {"nab_nazwa1"           , (void *)cmd_nabnazwa1},
                {"nab_nazwa2"           , (void *)cmd_nabnazwa2},
                {"nab_ulica"            , (void *)cmd_nabulica},
                {"nab_kod"              , (void *)cmd_nabkod},
                {"nab_miasto"           , (void *)cmd_nabmiasto},
                {"nab_adres"            , (void *)cmd_nabadres},
                {"nab_tel"              , (void *)cmd_nabtel},
                {"nab_fax"              , (void *)cmd_nabfax},
                {"nab_telfax"           , (void *)cmd_nabtelfax},
                {"nab_email"            , (void *)cmd_nabemail},
                {"nab_nip"              , (void *)cmd_nabnip},
                {"nab_regon"            , (void *)cmd_nabregon},
                {"nab_bank"             , (void *)cmd_nabbank},
                {"nab_konto"            , (void *)cmd_nabkonto},
                {"nab_uwagi"            , (void *)cmd_nabuwagi},
//|
///   Wartosci
                {"wartnettozw"          , (void *)cmd_wartnettozw},
                {"wartnetto0"           , (void *)cmd_wartnetto0},
                {"wartnetto3"           , (void *)cmd_wartnetto3},
                {"wartnetto7"           , (void *)cmd_wartnetto7},
                {"wartnetto17"          , (void *)cmd_wartnetto17},
                {"wartnetto22"          , (void *)cmd_wartnetto22},
                {"wartvatzw"            , (void *)cmd_wartvatzw},
                {"wartvat0"             , (void *)cmd_wartvat0},
                {"wartvat3"             , (void *)cmd_wartvat3},
                {"wartvat7"             , (void *)cmd_wartvat7},
                {"wartvat17"            , (void *)cmd_wartvat17},
                {"wartvat22"            , (void *)cmd_wartvat22},
                {"wartbruttozw"         , (void *)cmd_wartbruttozw},
                {"wartbrutto0"          , (void *)cmd_wartbrutto0},
                {"wartbrutto3"          , (void *)cmd_wartbrutto3},
                {"wartbrutto7"          , (void *)cmd_wartbrutto7},
                {"wartbrutto17"         , (void *)cmd_wartbrutto17},
                {"wartbrutto22"         , (void *)cmd_wartbrutto22},

                {"wartnetto"            , (void *)cmd_wartnetto},
                {"wartvat"              , (void *)cmd_wartvat},
                {"wartbrutto"           , (void *)cmd_wartbrutto},
//|

                {"wystawca"             , (void *)cmd_wystawca},
                {"odbiorca"             , (void *)cmd_odbiorca},

///   Info
                {"info1"                , (void *)cmd_info1},
                {"info2"                , (void *)cmd_info2},
                {"info3"                , (void *)cmd_info3},
                {"info4"                , (void *)cmd_info4},
                {"info5"                , (void *)cmd_info5},
                {"info6"                , (void *)cmd_info6},
                {"info7"                , (void *)cmd_info7},
                {"info8"                , (void *)cmd_info8},
                {"info9"                , (void *)cmd_info9},
                {"info10"               , (void *)cmd_info10},
//|

                {"aktualnadata"         , (void *)cmd_aktualnadata},

                {"golem"                , (void *)cmd_golem},


                // przelewy

                {"=dluz_naz"         , (void *)cmd_M_dluznaz},
                {"dluz_naz0"         , (void *)cmd_dluznaz0},
                {"dluz_naz1"         , (void *)cmd_dluznaz1},
                {"=wier_naz"         , (void *)cmd_M_wiernaz},
                {"wier_naz0"         , (void *)cmd_wiernaz0},
                {"wier_naz1"         , (void *)cmd_wiernaz1},
                {"=dluz_adr"         , (void *)cmd_M_dluzadr},
                {"dluz_adr0"         , (void *)cmd_dluzadr0},
                {"dluz_adr1"         , (void *)cmd_dluzadr1},
                {"dluz_kod"          , (void *)cmd_dluzkod},
                {"dluz_miasto"       , (void *)cmd_dluzmiasto},
                {"=wier_adr"         , (void *)cmd_M_wieradr},
                {"wier_adr0"         , (void *)cmd_wieradr0},
                {"wier_adr1"         , (void *)cmd_wieradr1},
                {"wier_kod"          , (void *)cmd_wierkod},
                {"wier_miasto"       , (void *)cmd_wiermiasto},

                {"dluz_miasto"       , (void *)cmd_dluzmiasto},
                {"=dluz_bank"        , (void *)cmd_M_dluzbank},
                {"dluz_bank0"        , (void *)cmd_dluzbank0},
                {"dluz_bank1"        , (void *)cmd_dluzbank1},
                {"=wier_bank"        , (void *)cmd_M_wierbank},
                {"wier_bank0"        , (void *)cmd_wierbank0},
                {"wier_bank1"        , (void *)cmd_wierbank1},
                {"=dluz_konto"       , (void *)cmd_M_dluzkonto},
                {"dluz_konto0"       , (void *)cmd_dluzkonto0},
                {"dluz_konto1"       , (void *)cmd_dluzkonto1},
                {"=wier_konto"       , (void *)cmd_M_wierkonto},
                {"wier_konto0"       , (void *)cmd_wierkonto0},
                {"wier_konto1"       , (void *)cmd_wierkonto1},
                {"=tytulem"          , (void *)cmd_M_tytulem},
                {"tytulem0"          , (void *)cmd_tytulem0},
                {"tytulem1"          , (void *)cmd_tytulem1},
                {"kwota"             , (void *)cmd_kwota},
                {"dataprzel"         , (void *)cmd_dataprzel},

                {NULL, NULL}
};
//|



/// RemoveSpaces
void RemoveSpaces(char *buf)
{
int in, out;

                in = out = 0;

                while(buf[in] == ' ' && buf[in] != 0)
                                   in++;

                while(buf[in] != ' ' && buf[in] != 0)
                   buf[out++] = buf[in++];

                buf[out++] = 0;

}
//|
/// ExecuteCommand

char *ExecuteCommand(struct Command *cmd)
{
int i = 0;
struct CmdArray *array = cmd->CmdArray;
static char output_buffer[300];
static char align_buffer[300];

                if(cmd->command[0]   != 0)  RemoveSpaces(cmd->command);
                if(cmd->width_str[0] != 0)  RemoveSpaces(cmd->width_str);
                if(cmd->zeros[0]     != 0)  RemoveSpaces(cmd->zeros);

                output_buffer[0] = 0;

/*
                if(cmd->width_str[0]   != 0)
                   {
                   D(bug("cmd: %s width_str %s\n", cmd->command, cmd->width_str));
                   }
*/

                // sprawdzamy czy polecenie jest znane i wykonujemy je je�li tak
                while(array[i].Command != NULL)
                   {
                   if(stricmp(array[i].Command, cmd->command) == 0)
                                   {
                                   cmd->cmd_out = output_buffer;

#pragma msg 154 ignore push
                                   (*array[i].Function)(cmd);
#pragma msg 154 pop

                                   // komenda nic nie zwraca.
                                   if( cmd->silent_command )
                                                   {
                                                   cmd->silent_command = FALSE;
                                                   output_buffer[0] = 0;
                                                   return(output_buffer);
                                                   }


                                   // przycinamy do zadanej szeroko�ci i wyr�wnujemy je�li trzeba

                                   if(cmd->width_str[0] != 0)
                                                   {
                                                   char format[25];

                                                   if(cmd->width_str[0] == '-')
                                                                   sprintf(format, "%%%s.%ss", &cmd->width_str[1], &cmd->width_str[1]);
                                                   else
                                                                   sprintf(format, "%%-%s.%ss", cmd->width_str, cmd->width_str);

                                                   sprintf(align_buffer, format, output_buffer);
                                                   return(align_buffer);
                                                   }
                                   else
                                                   return(output_buffer);
                                   }

                   i++;
                   }

                // polecenie nieznane - princimy jakgdyby nigdy nic ;-)
                sprintf(output_buffer, "@{%s", cmd->command);
                if(cmd->width_str[0] != 0)
                   {
                   strcat(output_buffer, ",");
                   strcat(output_buffer, cmd->width_str);
                   }
                if(cmd->zeros[0] != 0)
                   {
                   strcat(output_buffer, ",");
                   strcat(output_buffer, cmd->zeros);
                   }
                strcat(output_buffer, "}");

                return(output_buffer);

}

//|
/// ParseCommand

// parsuje stringa w poszukiwaniu znanej komendy GPLa

long ParseCommand(struct Command *cmd)
{
char *command_ptr = NULL;
char *width_ptr   = NULL;     // gdzie zaczyna si� arg. 'width' polecenia
char *zeros_ptr   = NULL;
char *end_ptr     = NULL;     // adres zamykaj�cej klamry
char *tmp_ptr     = NULL;

int i = cmd->in_index;

char cmd_len = 0;             // d�ugo�� polecenia


#define CMD_MIN_LEN  2
#define CMD_MAX_LEN 130

                memset(&cmd->command  , 0, sizeof(cmd->command));
                memset(&cmd->width_str, 0, sizeof(cmd->width_str));
                memset(&cmd->zeros    , 0, sizeof(cmd->zeros));

                if( cmd->in[ i+1 ] == '{' )        // spr. czy to polecenie @{xxxx}
                   {
                   command_ptr = cmd->in + i + 2;  // tu powinno zaczyna� si� polecenie

//       if((end_ptr = strchr(command_ptr, '}')) == NULL) // brak zamykaj�cej klamry
//           return(0);

                   if((end_ptr = wfmh_strnchr(command_ptr, '}', CMD_MAX_LEN)) == NULL) // czy jaka� spacja po drodze?
                                   {
                                   D(bug("** nie znaleziono '}' zamykaj�cej\n"));
                                   return(0);
                                   }

                   if(command_ptr+CMD_MIN_LEN >= end_ptr) // czy jest co najmniej kilka znak�w mi�dzy klamrami
                                   {
                                   D(bug("** polecenie za kr�tkie\n"));
                                   return(0);
                                   }

                   // szukamy ko�ca polecenia

                   tmp_ptr = wfmh_strnchr(command_ptr, ',', (end_ptr-command_ptr));
                   if(tmp_ptr)
                                   {
                                   memcpy(cmd->command, command_ptr, tmp_ptr-command_ptr);
                                   width_ptr = tmp_ptr+1;
                                   }
                   else
                                   {
                                   memcpy(&cmd->command, command_ptr, end_ptr-command_ptr);
                                   goto _parsecommand_exit;  // koniec polecenia - nie by�o przecinka, brak dalszych argument�w
                                   }


                   // szukamy ko�ca argumentu width

                   tmp_ptr = wfmh_strnchr(width_ptr, ',', (end_ptr-width_ptr));
                   if(tmp_ptr)
                                   {
                                   // sprawdzamy czy jest wpisany '!' i liczymy sami szeroko�� pola
                                   if(*width_ptr == '!')
                                                 sprintf(cmd->width_str, "%ld", (int)(end_ptr-(cmd->in+cmd->in_index)+1));
                                   else
                                                 memcpy(cmd->width_str, width_ptr, tmp_ptr-width_ptr);

                                   // width_str=>width
                                   sscanf(cmd->width_str, "%ld", &cmd->width);
                                   cmd->width = abs(cmd->width);

                                   zeros_ptr = tmp_ptr+1;
                                   }
                   else
                                   {
                                   // sprawdzamy czy jest wpisany '!' i liczymy sami szeroko�� pola
                                   if(*width_ptr == '!')
                                                 sprintf(cmd->width_str, "%ld", (int)(end_ptr-(cmd->in+cmd->in_index)+1));
                                   else
                                                 memcpy(&cmd->width_str, width_ptr, end_ptr-width_ptr);

                                   // width_str=>width
                                   sscanf(cmd->width_str, "%ld", &cmd->width);
                                   cmd->width = abs(cmd->width);

                                   goto _parsecommand_exit;  // koniec polecenia - nie by�o przecinka, brak dalszych argument�w
                                   }




                   // kopiujemy argument ZEROS

                   memcpy(cmd->zeros, zeros_ptr, end_ptr-zeros_ptr);
                   }


_parsecommand_exit:

                cmd_len = end_ptr - command_ptr;
                return(cmd_len);

}
//|
/// ParseLine

// parsuje aktualna linie formatki, wyciagajac zen
// polecenia GPL
void ParseLine(struct DocumentPrint *dp, struct Command *cmd)
{
char *input   = cmd->in;
char *out     = cmd->out;
int in_index;
int out_index = 0;
int len       = strlen(input);


                for(in_index=0; in_index<=len; in_index++)
                   {
                   // komendy GPLa rozpoczynaja sie od @
                   if(input[in_index] != '@')
                                   {
                                   out[out_index++] = input[in_index];
                                   }
                   else
                                   {
                                   int cmd_len;

                                   cmd->in_index = in_index;
                                   cmd->out_index = out_index;

                                   // sprawdzamy cze po @ jest jakas sensowna komenda GPLa
                                   if((cmd_len = ParseCommand(cmd)) != 0)
                                                   {
                                                   char *cmd_result;
                                                   int   cmd_result_len;

                                                   in_index += cmd_len +2;
                                                   cmd->in_index += cmd_len +2;

                                                   cmd_result = ExecuteCommand(cmd);
                                                   cmd_result_len = strlen(cmd_result);

                                                   memcpy(&out[out_index], cmd_result, cmd_result_len);
                                                   out_index += cmd_result_len;
                                                   }
                                   else
                                                   {
                                                   // po @ nie bylo zadnej znanej komendy, wiec
                                                   // puszczamy to na wyjscie
                                                   out[out_index++] = '@';
                                                   fprintf(stderr,">>: '%s'\n", cmd->in);
                                                   }
                                   }
                   }


                if(cmd->CenterOn)
                   {
                   char _buff[512];
                   int _len = strlen(out);
                   int _width = cmd->dp->prt->printer_width_normal;

                   switch(cmd->FontType)
                                   {
//           case FONT_NORMAL:
//              _width = settings.printer_width_normal;
//              break;

                                   case FONT_WIDE:
                                                  _width = cmd->dp->prt->printer_width_wide;
                                                  break;

                                   case FONT_CONDENSED:
                                                  _width = cmd->dp->prt->printer_width_cond;
                                                  break;                              
                                   }


                   memset(_buff, 0, sizeof(_buff));
                   memset(_buff, 32, sizeof(_buff)/2);
                   strcpy(&_buff[(_width - _len)/2], out);
                   strcpy(out, _buff);
                   }


}
//|

/// Formatki

#define _ID_FVAT  "FVAT"       // faktura vat
#define _ID_RUPR  "RUPR"       // rachunek uproszczony
#define _ID_FVKO  "FVKO"       // korekta fvat
#define _ID_RUKO  "RUKO"       // korekta rachunku uproszczonego
#define _ID_FV2P  "FV2P"       // faktura vat do paragonu
#define _ID_RU2P  "RU2P"       // rachunek upr. do paragonu
#define _ID_PARN  "PARN"       // paragon (niefiskalny)
#define _ID_PARF  "PARF"       // paragon (fiskalny)
#define _ID_PARK  "PARK"       // kopia paragonu (juz na noramlnej drukarce)
#define _ID_ORD   "ZAMO"       // zamowienie
#define _ID_FVPR  "FVPR"       // faktura vat pro forma

#define _ID_DETL  "DETL"       // cennik detaliczny
#define _ID_HURT  "HURT"       // cennik hurtowy
#define _ID_STAN  "STAN"       // stany magazynowe
#define _ID_SPIS  "SPIS"       // spis z natury
#define _ID_LIMI  "LIMI"       // spis z przekroczonymi limitami

#define _ID_ODRE  "ODRE"       // sprzedaz odreczna

#define _ID_WIRE  "WIRE"       // przelew


struct Formatka formatki[] =
{
                { _ID_FVAT, 0x00, "VAT"        , "FV" , "Faktura VAT"              , GolemTemplateDir "/Faktura%s" },
                { _ID_RUPR, 0x01, "Upr"        , "RU" , "Rachunek uproszczony"     , GolemTemplateDir "/Rachunek%s" },
                { _ID_FVKO, 0x02, "FV-KO"      , "FVK", "Faktura koryguj�ca VAT"   , GolemTemplateDir "/Faktura-Korekta%s" },
                { _ID_RUKO, 0x03, "Upr-KO"     , "RUK", "Rachunek koryguj�cy"      , GolemTemplateDir "/Rachunek-Korekta%s" },
                { _ID_FV2P, 0x04, "FV-Par"     , "FVP", "Faktura VAT do paragonu"  , GolemTemplateDir "/FakturaDoParagonu%s" },
                { _ID_RU2P, 0x05, "Upr-Par"    , "RUP", "Rachunek upr. do paragonu", GolemTemplateDir "/RachDoParagonu%s" },
                { _ID_PARF, 0x06, "Par-POSNET" , "POS", "Paragon fiskalny"         , GolemTemplateDir "/Paragon-Posnet%s" },
                { _ID_ORD , 0x07, "Zam"        , "ZAM", "Zam�wienie"               , GolemTemplateDir "/Zam�wienie%s" },
                { _ID_FVPR, 0x08, "FV-Pro"     , "PRO", "Faktura PRO FORMA"        , GolemTemplateDir "/Faktura-ProForma%s" },
//    { _ID_PARN, 0x08, "Paragon"    , "PAR", "Paragon niefiskanly"      , GolemTemplateDir "/Paragon%s" },
//    { _ID_PARK, 0x09, "Par-Kopia"  , "PKO", "Paragon - kopia"          , GolemTemplateDir "/Paragon-Kopia%s" },
                { _ID_ODRE, 0x0A, "Odreczna"   , "ODR", "Sprzeda� odr�czna"        , GolemTemplateDir "/Odreczna%s" },

                { _ID_DETL, 0xf0, "Cennik Dtl" , "DTL", "Cennik detaliczny"        , GolemTemplateDir "/CennikDetal%s" },
                { _ID_HURT, 0xf1, "Cennik Hrt" , "HRT", "Cennik hurtowy"           , GolemTemplateDir "/CennikHurt%s" },
                { _ID_STAN, 0xf2, "Stan Mag. " , "STN", "Stany magazynowe"         , GolemTemplateDir "/StanyMagazynowe%s" },
                { _ID_SPIS, 0xf3, "Spis Z Nat" , "SZN", "Spis z natury"            , GolemTemplateDir "/SpisZNatury%s" },
                { _ID_LIMI, 0xf4, "Limity"     , "LIM", "Przekroczone limity mag." , GolemTemplateDir "/LimityMagazynowe%s" },

                { _ID_WIRE, 0xf5, "Przelew"    , "WIR", "Przelew bankowy"          , GolemTemplateDir "/Przelew" },

                { NULL, NULL, NULL }

};
char FormatkaFileName[256];
//|
/// GetTemplateName

char *GetTemplateName(struct DocumentPrint *dp)
{
/*
** Zwraca char * na nazw� wzorca dokumentu
** do wydrukowania zale�nie od jego typu.
*/

int i;


                D(bug( "GetTemplateName: Dokument: 0x%lx\n", dp->RodzajDokumentu ));

                for(i=0;;i++)
                   {
                   if( formatki[i].KodLV == NULL )     // <-- !!!!! POPRAWIC !!!!!
                                   break;

                   if( formatki[i].Kod == dp->RodzajDokumentu )
                                   {
                                   if( dp->prt->doc_sww || dp->Drukowa�SWW)
                                                   sprintf( FormatkaFileName, formatki[i].Formatka, "-SWW" );
                                   else
                                                   sprintf( FormatkaFileName, formatki[i].Formatka, "" );

                                   }
                   }

                D(bug( "       FileName: %s\n", FormatkaFileName ));

                return( FormatkaFileName );
}
//|

/// PrzeliczDokument

/*
** liczy wszystkie warto�ci globalne dokumentu
** na podstawie listy produkt�w podanych w dp->lv_object
** wyniki umieszcza w odp. polach dp.
*/

void PrzeliczDokument(struct DocumentPrint *dp)
{
double netto_22, vat_22;
double netto_17, vat_17;
double netto_7,  vat_7;
double netto_3,  vat_3;
double netto_0,  vat_0;
double netto_zw, vat_zw;

double zakup_netto_total;

int i;


                D(bug("Przelicz dokument\n"));

                if( ! (dp->lv_object || dp->prt ) )
                   {
//       DisplayBeep(0);
                   return;
                   }

                // sprawdzamy czy na dokumencie potrzebne jest pole SWW

                if( dp->prt->doc_sww == FALSE )
                   {

                   for(i=0;;i++)
                                  {
                                  struct ProductList *prod;

                                  DoMethod(dp->lv_object, _MUIM_List_GetEntry, i, &prod);
                                  if(!prod)
                                                 break;

                                  if(prod->pl_prod.SWW[0] != '\0')
                                                 {
                                                 dp->Drukowa�SWW = TRUE;
                                                 break;
                                                 }
                                  }
                   }
                 else
                   {
                   dp->Drukowa�SWW = TRUE;
                   }


                // obliczamy wszystkie mo�liwe warto�ci na podstawie dokumentu

                 netto_22 = vat_22 = 0;
                 netto_17 = vat_17 = 0;
                 netto_7  = vat_7  = 0;
                 netto_3  = vat_3  = 0;
                 netto_0  = vat_0  = 0;
                 netto_zw = vat_zw = 0;
                 zakup_netto_total = 0;

                 for(i=0;;i++)
                   {
                   struct ProductList *prod;
                   double cena_sprzeda�y, warto��_netto, podatek;

                   DoMethod(dp->lv_object, _MUIM_List_GetEntry, i, &prod);
                   if(!prod)
                                   break;


                   // warto�� towaru/us�ugi bez podatku
                   cena_sprzeda�y = prod->pl_prod.Zakup + prod->pl_prod.Mar�a;
                   warto��_netto = cena_sprzeda�y - CalcRabat(cena_sprzeda�y, prod->pl_prod.Rabat);
                   warto��_netto = prod->pl_prod.Ilo�� * warto��_netto;

                   // kwota podatku
                   podatek = CalcVat(warto��_netto, prod->pl_prod.Vat);

                   // suma zakupow (do np. spisu z natury)
                   zakup_netto_total += prod->pl_prod.Ilo�� * prod->pl_prod.Zakup;

                   switch(prod->pl_prod.Vat)
                                   {
                                   /* zwolnione */
                                   case 0:
                                                   netto_zw += warto��_netto;
                                                   vat_zw   += podatek;
                                                   break;

                                   /*        0% */
                                   case 1:
                                                   netto_0 += warto��_netto;
                                                   vat_0   += podatek;
                                                   break;

                                   /*        7% */
                                   case 2:
                                                   netto_7 += warto��_netto;
                                                   vat_7   += podatek;
                                                   break;

                                   /*       17% */
                                   case 3:
                                                   netto_17 += warto��_netto;
                                                   vat_17   += podatek;
                                                   break;

                                   /*       22% */
                                   case 4:
                                                   netto_22 += warto��_netto;
                                                   vat_22   += podatek;
                                                   break;

                                   case 5:
                                                   netto_3 += warto��_netto;
                                                   vat_3   += podatek;
                                                   break;
                                   }
                   }

                 dp->netto_22 = netto_22;
                 dp->vat_22   = vat_22;
                 dp->netto_17 = netto_17;
                 dp->vat_17   = vat_17;
                 dp->netto_7  = netto_7;
                 dp->vat_7    = vat_7;
                 dp->netto_3  = netto_3;
                 dp->vat_3    = vat_3;
                 dp->netto_0  = netto_0;
                 dp->vat_0    = vat_0;
                 dp->netto_zw = netto_zw;
                 dp->vat_zw   = vat_zw;

                 dp->zakup_netto_total = zakup_netto_total;

}


//|

/// WydrukujDokument_Printer
void WydrukujDokument_Printer( struct DocumentPrint *dp )
{
struct Command cmd = {0};

char in[1024];
char out[600];
FILE *template;

int lv_total = 1;                  // liczba element�w na li�cie (1 dla demo wersji)



                // jadziem
                if(( OpenPrinter( dp->prt )) )
                   {
                   D(bug("WydrukujDokument_RP: $%08lx\n", dp->prt));


                   if(CheckPrinter(dp->prt, MainWindow) == FALSE)
                                  {
                                  D(bug(" CheckPrinter: fail\n"));
                                  ClosePrinter(dp->prt);
                                  return;
                                  }


                   // set SpoolFileName
                   if(dp->Duplikat)
                                   sprintf(in, "%s_D_%ld", dp->Nag��wekDokumentuSpool, dp->NumerEgzemplarza);
                   else
                                   sprintf(in, "%s_%ld", dp->Nag��wekDokumentuSpool, dp->NumerEgzemplarza);
                   SetSpoolerName(dp->prt, in);


                   // no to jadziem
                   if(template = fopen( GetTemplateName(dp), "rb" ))
                                {
                                   lv_total = xget(dp->lv_object, _MUIA_List_Entries);

                                  dp->aktualna_linia     = 0;
                                  dp->liczba_linii       = 0;
                                  dp->liczba_linii_total = 0;
                                  dp->aktualna_strona    = 0;

                                  cmd.CmdArray = commands;
                                  cmd.dp       = dp;
                                  cmd.in       = in;
                                  cmd.out      = out;

                                  fseek(  template, 0, SEEK_SET);  // przewijamy na poczatek
                                  while( fgets(in, sizeof(in), template) != NULL )
                                        {
                                        dp->lv_index = 0;                       // pozycja na liscie produktow do drukowania (poczatek)

                                        dp->DoMultiLine  = FALSE;    // podzial nazw na linie potem sie wyjasni

                                        do
                                                {
                                                dp->NeedRepeat = FALSE;
                                                dp->MultiLineCnt = 0;

                                                do
                                                        {
                                                        ParseLine(dp, &cmd);
                                                        PrintLine(dp, out);
                                                        
                                                        if( dp->DoMultiLine )                   // nastepna linia z multilinii raz...
                                                                dp->MultiLineCnt++;

                                                        }
                                                while( dp->DoMultiLine == TRUE );       // krecimy linie jesli jest podzial...

                                                dp->lv_index++;
                                                }
                                        while( (dp->NeedRepeat == TRUE) && (dp->lv_index < lv_total) );
                                        }


                                        if(dp->prt)
                                                {
                                                PrintLastPage(dp);
                                                }


                                  // FormFeed
                                  if( dp->prt->formfeed )
                                                  PrintLine(dp, "\014");

                                  PrintLine(dp, dp->prt->normal);
                                  PrintLine(dp, dp->prt->cond_off);

                                  fclose(template);
                                }
                                else
                                {
                                  DisplayBeep(0);
                                  MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_TEMPLATE);
                                }


                                if(dp->prt)
                                {
                                   ClosePrinter(dp->prt);
                                }

                   }
                else
                {
                   DisplayBeep(0);
                   MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_PRT_INIT);
                }


                D(bug("Wydrukuj dokument - koniec\n"));

}
//|
/// WydrukujDokument
void WydrukujDokument( struct DocumentPrint *dp )
{
struct PrinterList * prt;




                // na jakiej drukarce drukowac?
                if( dp->UseCustomPrinter == TRUE )
                   {
                   if( ! (prt = Znajd�Drukark�ID( dp->PrinterID )) )
                                   prt = Znajd�Drukark�ID( DEFAULT_PRINTER );
                   }
                else
                   prt = MatchPrinterForDocument( dp->RodzajDokumentu );


                // na tej drukarce ma�emy
                dp->prt = &prt->pl_printer;
                D(bug("dp->prt->Type: %ld\n",  dp->prt->Type ));


                // przeliczamy (nie mozna wczesniej bo przeliczanie
                // zalezne od typu drukarki
                PrzeliczDokument(dp);



                // liczymy ile linii ma dokument <-- LAME!!
                {
                                dp->prt->SilentMode = TRUE;
                                WydrukujDokument_Printer( dp );
                }

                D(bug("Linii total: %ld\n", dp->aktualna_linia_total ));
                dp->liczba_linii_total = dp->aktualna_linia_total;

                // druczymy na serio
                dp->prt->SilentMode = FALSE;
                if ((dp->liczba_linii_total <= dp->prt->page_height) && (dp->prt->skip_1st_page_footer))
                {
                                char old_page_mode = dp->prt->page_mode;
                                dp->prt->page_mode = FALSE;
                                WydrukujDokument_Printer( dp );
                                dp->prt->page_mode = old_page_mode;
                }
                else
                                WydrukujDokument_Printer( dp );  
}
//|

