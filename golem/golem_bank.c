
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

#include "Golem.h"

/// BANK INFO STOP CHUNKS
#define BANK_INFO_NUM_STOPS (sizeof(Bank_Info_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Bank_Info_Stops[] =
{
        ID_BANK, ID_FORM,
        ID_BANK, ID_VERS,
        ID_BANK, ID_CENT,
        NULL, NULL,
};
//|
/// WczytajBankInfo
char WczytajBankInfo(char *NumerKonta, struct BankInfo *info)
{
/*
** Wczytuje strukture BankInfo
** Zwraca BOOL w zaleznosci czy plik z opisem banku zostal znaleziony
** czy tez nie
*/

struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
char   ValidFile = FALSE;

int    Errors = 0;
int    Error_Products = 0;
int    Error_Duplicated_Groups = 0;


char   FileFound = TRUE;
char   FileName[100];
char   len;


     // 3 pierwsze cyfry to kod banku

     len = strlen( NumerKonta );

     if( len < 3 )
       return( FALSE );


     if( NumerKonta[0] < '5' && len >= 3)
       {
       sprintf(FileName, BankiDir "/%3.3s/%3.3s.golem", NumerKonta, NumerKonta);
       }
     else
       {
       if( NumerKonta[0] >= '8' && len >= 4)
           {
           sprintf(FileName, BankiDir "/%4.4s/%4.4s.golem", NumerKonta, NumerKonta);
           }
        else
           {
           return( FALSE );
           }
       }


     if(iff = AllocIFF())
       {
       if(iff->iff_Stream = Open(FileName, MODE_OLDFILE))
           {
           InitIFFasDOS(iff);

           StopChunks(iff, Bank_Info_Stops, BANK_INFO_NUM_STOPS);
           StopOnExit(iff, ID_BANK, ID_FORM);

           if(!OpenIFF(iff, IFFF_READ))
               {
               while(TRUE)
                  {
                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                     break;

                  if(cn = CurrentChunk(iff))
                     {
                     LONG ID = cn->cn_ID;

                     if(!ValidFile)
                        {
                        if((ID == ID_FORM) && (cn->cn_Type == ID_BANK))
                           {
                           ValidFile = TRUE;
                           continue;
                           }

                        break;
                        }

///                       ID_VERS
                     if(ID == ID_VERS)
                        {
                        struct BaseVersion version;

                        if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                           {

                           }
                        else
                           {
                           _RC = IoErr();
                           Errors++;
                           break;
                           }

                        continue;
                        }
//|
///                       ID_CENT

                     if(ID == ID_CENT)
                        {
                        char   Failed = FALSE;

                        if(ReadChunkBytes(iff, info, sizeof(struct BankInfo)) != cn->cn_Size)
                           {
                           _RC = IoErr();
                           Failed = TRUE;
                           Errors++;
                           break;
                           }

                        continue;
                        }
//|

                     }
                  }

               CloseIFF(iff);
               }

            Close(iff->iff_Stream);
            }
         else
            {
            FileFound = FALSE;
            }

       FreeIFF(iff);
       }




       return( FileFound );
}

//|

/// BANK STOP CHUNKS
#define BANK_NUM_STOPS (sizeof(Bank_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Bank_Stops[] =
{
        ID_BANK, ID_FORM,
        ID_BANK, ID_VERS,
        ID_BANK, ID_BANK,
        NULL, NULL,
};
//|
/// WczytajBank
char WczytajBank(char *NumerKonta, struct Bank *info)
{
/*
** Wczytuje strukture Bank
** Zwraca BOOL w zaleznosci czy plik z opisem banku zostal znaleziony
** czy tez nie
*/

struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
char   ValidFile = FALSE;

int    Errors = 0;
int    Error_Products = 0;
int    Error_Duplicated_Groups = 0;


char   FileFound = TRUE;
char   FileName[100];
char   len;


     // 8 cyfr to numer banku i oddzialu

     len = strlen( NumerKonta );

     if( len < 8 )
       return( FALSE );


     if( NumerKonta[0] < '5')
       {
       sprintf(FileName, BankiDir "/%3.3s/%8.8s.golem", NumerKonta, NumerKonta);
       }
     else
       {
       if( NumerKonta[0] >= '8')
           {
           sprintf(FileName, BankiDir "/%4.4s/%8.8s.golem", NumerKonta, NumerKonta);
           }
        else
           {
           return( FALSE );
           }
       }


     if(iff = AllocIFF())
       {
       if(iff->iff_Stream = Open(FileName, MODE_OLDFILE))
           {
           InitIFFasDOS(iff);

           StopChunks(iff, Bank_Stops, BANK_NUM_STOPS);
           StopOnExit(iff, ID_BANK, ID_FORM);

           if(!OpenIFF(iff, IFFF_READ))
               {
               while(TRUE)
                  {
                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                     break;

                  if(cn = CurrentChunk(iff))
                     {
                     LONG ID = cn->cn_ID;

                     if(!ValidFile)
                        {
                        if((ID == ID_FORM) && (cn->cn_Type == ID_BANK))
                           {
                           ValidFile = TRUE;
                           continue;
                           }

                        break;
                        }

///                       ID_VERS
                     if(ID == ID_VERS)
                        {
                        struct BaseVersion version;

                        if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                           {

                           }
                        else
                           {
                           _RC = IoErr();
                           Errors++;
                           break;
                           }

                        continue;
                        }
//|
///                       ID_BANK

                     if(ID == ID_BANK)
                        {
                        char   Failed = FALSE;

                        if(ReadChunkBytes(iff, info, sizeof(struct Bank)) != cn->cn_Size)
                           {
                           _RC = IoErr();
                           Failed = TRUE;
                           Errors++;
                           break;
                           }

                        continue;
                        }
//|

                     }
                  }

               CloseIFF(iff);
               }

            Close(iff->iff_Stream);
            }
         else
            {
            FileFound = FALSE;
            }

       FreeIFF(iff);
       }



       return( FileFound );
}

//|

