
/// Includes

#define NO_GUI_PROTOS_H
#include "golem.h"
#undef  NO_GUI_PROTOS_H

#define KPIR_GUI
#include "kpir_gui.h"

#define SAVEDS __saveds
#define ASM __asm
#define REG(x) register __ ## x

#define USE_KPIR_COLORS
#define USE_KPIR_BODY
#include "images/kpir_img.h"


//|


/// WindowFunc
SAVEDS ASM VOID WindowFunc(REG(a2) Object *pop,REG(a1) Object *win)
{
        set(win, MUIA_Window_DefaultObject, pop);
}
//|

static const struct Hook ObjStrHook_Odb_Cust = {{ NULL,NULL }, (VOID *)ObjStrFunc_Odb_Cust, NULL, NULL};
static const struct Hook StrObjHook_Odb_Cust = {{ NULL,NULL }, (VOID *)StrObjFunc_Odb_Cust, NULL, NULL};

static const struct Hook ObjStrHook_Fak = {{ NULL,NULL }, (VOID *)ObjStrFunc_Fak, NULL, NULL};
static const struct Hook StrObjHook_Fak = {{ NULL,NULL }, (VOID *)StrObjFunc_Fak, NULL, NULL};

static const struct Hook ObjStrHook_Mag = {{ NULL,NULL }, (VOID *)ObjStrFunc_Mag, NULL, NULL};
static const struct Hook StrObjHook_Mag = {{ NULL,NULL }, (VOID *)StrObjFunc_Mag, NULL, NULL};
static const struct Hook WindowHook     = {{ NULL,NULL }, (VOID *)WindowFunc   , NULL, NULL};


char *UserLevel_Table[] = {"Administrator",
//                           "Sprzedawca + Magazynier",
//                           "Sprzedawca",
//                           "Magazynier",
                           NULL};

char *RodzajeDokument�wTable[] = {"Faktura VAT",
                                  "Rachunek uproszczony",
                                  "Faktura koryguj�ca VAT",
                                  "Rachunek koryguj�cy",
                                  "Faktura VAT do paragonu",
                                  "Rachunek upr. do paragonu",
                                  "Paragon",
                                  NULL};

char *RodzajeDokument�wSpoolTable[] = {"VAT",
                                       "Upr",
                                       "FV-KO",
                                       "Upr-KO",
                                       "FV-Par",
                                       "Upr-Par",
                                       "Par",
                                        NULL};


char *RodzajeDokument�wLVTable[] = {"FV",
                                    "RU",
                                    "FVK",
                                    "RUK",
                                    "FVP",
                                    "RUP",
                                    "PAR",
                                    NULL};

char *RodzajePrzeszukiwaniaTable[] = {
                                      MSG_SORT_RA_DS_NUMER,
                                      MSG_SORT_RA_DS_NAZWA,
                                      MSG_SORT_RA_DS_NIP,
                                      MSG_SORT_RA_DS_REGON,
                                      MSG_SORT_RA_DS_DATA,
                                      NULL};


char *Miesi�ceTable[] = {"\033bAktualny miesi�c",
                         "Stycze�",
                         "Luty",
                         "Marzec",
                         "Kwiecie�",
                         "Maj",
                         "Czerwiec",
                         "Lipiec",
                         "Sierpie�",
                         "Wrzesie�",
                         "Pa�dziernik",
                         "Listopad",
                         "Grudzie�",
                         "\0338Ca�y rok",
                         NULL};

char *Miesi�ceTable2[] = {"\033bAktualny miesi�c",
                         "Stycze�",
                         "Luty",
                         "Marzec",
                         "Kwiecie�",
                         "Maj",
                         "Czerwiec",
                         "Lipiec",
                         "Sierpie�",
                         "Wrzesie�",
                         "Pa�dziernik",
                         "Listopad",
                         "Grudzie�",
                         NULL};


char *WyszukiwanieWgDatyTable[] = {"dok�adnie",
                                   "wcze�niej",
                                   "p��niej",
                                   NULL};

char *GR_Cust_Ed_Titles[] = {MSG_CUST_ED_PAGE1,
                             MSG_CUST_ED_PAGE2,
                             MSG_CUST_ED_PAGE3,
                             NULL};

char *GR_Mag_Ed_Titles[] = {MSG_MAG_ED_PAGE1,
                            MSG_MAG_ED_PAGE2,
                            NULL};

char *GR_ZakE_Titles[]   = {MSG_ZAKE_PAGE1,
                            MSG_ZAKE_PAGE2,
                            MSG_ZAKE_PAGE3,
                            NULL};

char *RA_Sort_Zakupy[] =  {MSG_SORT_RA_LP,
                           MSG_SORT_RA_NUMER,
                           MSG_SORT_RA_DATA,
                           NULL};

char *RA_Sort_DocScan[] = {MSG_SORT_RA_DS_NAZWA,
                           MSG_SORT_RA_DS_NIP,
                           MSG_SORT_RA_DS_REGON,
                           MSG_SORT_RA_DS_NUMER,
                           MSG_SORT_RA_DS_DATA,
                           NULL};


//Object *CreateApp(void)
Object *CreateApp(void)
{
Object *app = NULL;

      app = ApplicationObject,
//      app = NewObject(CL_Application->mcc_Class, NULL,
                 MUIA_Application_Title      , "Golem-KPiR",
                 MUIA_Application_Version    , VERSTAG,
                 MUIA_Application_Copyright  , "� 1997-1998 W.F.M.H.",
                 MUIA_Application_Author     , "Marcin Or�owski",
                 MUIA_Application_Description, MSG_APP_DESC,
                 MUIA_Application_Base       , "GOLEM-KPIR",

///                StartupWindow
        MUIA_Application_Window, StartupWindow = WindowObject,
                            MUIA_Window_ID         , ID_WIN_STARTUP,

//                            MUIA_Window_Title      , TITLE,

                            MUIA_Window_ScreenTitle, ScreenTitle,
                            MUIA_Window_DragBar    , FALSE,
                            MUIA_Window_Borderless , TRUE,
                            MUIA_Window_CloseGadget, FALSE,
                            MUIA_Window_DepthGadget, FALSE,
                            MUIA_Window_SizeGadget , FALSE,
                            MUIA_Window_TopEdge    , MUIV_Window_TopEdge_Centered,
                            MUIA_Window_LeftEdge   , MUIV_Window_LeftEdge_Centered,

                            WindowContents, VGroup,
                                        GroupFrame,

///                                       Child, BodychunkObject,
                   Child, BodychunkObject,
                          GroupFrame,
                          MUIA_Group_Spacing, 0,
                          MUIA_FixWidth             , KPIR_WIDTH ,
                          MUIA_FixHeight            , KPIR_HEIGHT,
                          MUIA_Bitmap_Width         , KPIR_WIDTH ,
                          MUIA_Bitmap_Height        , KPIR_HEIGHT,
                          MUIA_Bodychunk_Depth      , KPIR_DEPTH ,
                          MUIA_Bodychunk_Body       , (UBYTE *) kpir_body,
                          MUIA_Bodychunk_Compression, KPIR_COMPRESSION,
                          MUIA_Bodychunk_Masking    , KPIR_MASKING,
                          MUIA_Bitmap_SourceColors  , (ULONG *) kpir_colors,
                          End,
//|
///                                       ScrollText

                                        Child, CR_Startup_Scroll = CrawlingObject,
                                               MUIA_Virtgroup_Input, FALSE,
                                               MUIA_FixHeightTxt, "\n\n\n",
                                               MUIA_Font, MUIV_Font_Tiny,
                                               Child, HGroup,
                                                 Child, MUI_NewObject(MUIC_Text,
                                                      MUIA_Text_PreParse, "\033r\033b",
                                                      MUIA_Text_Contents, "Programowanie:\n"
                                                                          "\n"
                                                                          "Wersja:\n"
                                                                          "Data kompilacji:\n"
#ifdef BETA
                                                                          "\n"
                                                                          "U�ytkownik:\n\n\n"
#endif
                                                                          "\n"
                                                                          "Testowanie:\n"
                                                                          "\n"
                                                                          "\n"
                                                                          "\n"
                                                                          "\n"
                                                                          "\n"
                                                                          "\n"
                                                                          "\n"
                                                                          "\n"
//                                                                          "Podr�cznik:\n"
//                                                                          "\n"
                                                                          "Producent:\n"
                                                                          "\n"
                                                                          "\n"
                                                                          "\n"
                                                                          "Telefon:\n"
                                                                          "E-Mail:\n"
                                                                          "WWW:\n"
                                                                          "\n"

                                                                          "\n"
                                                                          "Programowanie:\n"
                                                                          "\n"
                                                                          "Wersja:\n"
                                                                          "Data kompilacji:"
                                                                          "",
                                                      End,


                                                 Child, MUI_NewObject(MUIC_Text,
                                                      MUIA_Text_PreParse, "\033l\0333",
                                                      MUIA_Text_Contents, "Marcin Or�owski\n"
                                                                          "\n"
#ifdef DEMO
                                                                          VERSIONREVISION " BETA-DEMO\n"
#else
#ifdef BETA
                                                                          VERSIONREVISION " BETA\n"
#else
                                                                          VERSIONREVISION "\n"
#endif
#endif
                                                                          DATE "\n"

#ifdef BETA
                                                                          "\n"
                                                                          BETA_USER_1 "\n"
                                                                          BETA_USER_2 "\n"
                                                                          BETA_USER_3 "\n"
#endif

                                                                          "\n"
                                                                          "Krzysztof Krzewiniak\n"
                                                                          "Marian Kwiatkowski\n"
                                                                          "Mi�os�aw Smyk\n"
                                                                          "Patryk �ogiewa\n"
                                                                          "Micha� Romanowski\n"
                                                                          "Piotr Larkowski\n"
                                                                          "Piotr Szudrowicz\n"
                                                                          "Andrzej Szudrowicz\n"
                                                                          "\n"
//                                                                          "Marcin Or�owski\n"
//                                                                          "\n"
                                                                          "W.F.M.H.\n"
                                                                          "ul. Radomska 38\n"
                                                                          "71-002 Szczecin\n"
                                                                          "\n"
                                                                          "(091) 4836-425\n"
                                                                          EMAIL "\n"
                                                                          WWW "\n"
                                                                          "\n"

                                                                          "\n"
                                                                          "Marcin Or�owski\n"
                                                                          "\n"
#ifdef DEMO
                                                                          VERSIONREVISION " BETA-DEMO\n"
#else
                                                                          VERSIONREVISION "\n"
#endif
                                                                          DATE
                                                                          "",
                                                      End,
                                                 End,
                                               End,
//|

                                        Child, HGroup,
                                               Child, GA_Startup_Info = GaugeObject,
                                                      TextFrame,
                                                      MUIA_Gauge_Horiz , TRUE,
                                                      MUIA_Gauge_InfoText, "",
                                                      End,

                                               Child, BT_Startup_Ok = TextButtonWeight(MSG_STARTUP_OK, 25),
                                               End,

                                        End,
                            End,

//|

///                Main Window

        MUIA_Application_Window, MainWindow = WindowObject,
                        MUIA_Window_Title      , TITLE,
                        MUIA_Window_ID         , ID_WIN_MAIN,
                        MUIA_Window_ScreenTitle, ScreenTitle,
//                        MUIA_Window_CloseGadget, FALSE,
                        WindowContents, GR_ToolBar_Menus = GroupObject,

                                        MUIA_Group_SameWidth, TRUE,
                                        MUIA_Group_Spacing  , 2,
                                        GroupFrame,

                                        MUIA_Group_PageMode, TRUE,
///                                         Main
                                        Child, VGroup,
                                               Child, VGroup,
                                                      MUIA_Group_SameWidth, TRUE,

                                                      GroupFrameT(MSG_TOOLBAR_MAIN),

                                                      Child, BT_ToolBar_Kpir   = TextButton(MSG_TOOLBAR_KPIR),
                                                      Child, BT_ToolBar_System = TextButton(MSG_TOOLBAR_SYSTEM),
                                                      End,

                                               Child, VGroup,
                                                      MUIA_Group_SameWidth, TRUE,

                                                      GroupFrameT(MSG_TOOLBAR_MISC),

                                                      Child, BT_ToolBar_About    = TextButton(MSG_TOOLBAR_ABOUT),
                                                      Child, GR_ToolBar_LoginLogout = GroupObject,
                                                             MUIA_Group_PageMode, TRUE,
                                                             Child, BT_ToolBar_Login = TextButton(MSG_TOOLBAR_LOGIN),
                                                             Child, BT_ToolBar_Logout = TextButton(MSG_TOOLBAR_LOGOUT),
                                                             End,
                                                      Child, BT_ToolBar_Quit = TextButton(MSG_TOOLBAR_QUIT),

                                                      End,
                                               End,
//|
/*
///                                         Zakupy
                                        Child, VGroup,
                                               Child, VGroup,
                                                      MUIA_Group_SameWidth, TRUE,

                                                      GroupFrameT(MSG_TOOLBAR_ZAKUPY_TITLE),
                                                      Child, BT_ToolBar_Kpir_Zakupy   = TextButton(MSG_TOOLBAR_KPIR_ZAKUPY),
                                                      Child, BT_ToolBar_Kpir_Sprzeda� = TextButton(MSG_TOOLBAR_KPIR_SPRZEDAZ),
                                                      End,

                                               Child, HVSpace,

                                               Child, VGroup,
                                                      MUIA_Group_SameWidth, TRUE,
                                                      GroupFrameT(MSG_TOOLBAR_MISC),

                                                      Child, BT_ToolBar_Kpir_Back = TextButton(MSG_TOOLBAR_MAINMENU),
                                                      End,
                                               End,
//|
*/
///                                         System
                                        Child, VGroup,
                                               Child, VGroup,
                                                      MUIA_Group_SameWidth, TRUE,

                                                      GroupFrameT(MSG_TOOLBAR_SYSTEM_TITLE),

                                                      Child, BT_ToolBar_Settings = TextButton(MSG_TOOLBAR_SETTINGS),
                                                      End,

                                               Child, HVSpace,
                                               Child, VGroup,
                                                      MUIA_Group_SameWidth, TRUE,
                                                      GroupFrameT(MSG_TOOLBAR_MISC),

                                                      Child, BT_ToolBar_System_Back = TextButton(MSG_TOOLBAR_MAINMENU),
                                                      End,
                                               End,
//|
                                        End,
                        End,

//|
///                ProgressWindow

                 MUIA_Application_Window,
                     ProgressWindow = WindowObject,
                        MUIA_Window_Title      , MSG_PROG_WIN_TITLE,
                        MUIA_Window_ID         , ID_WIN_PROG,
                        MUIA_Window_ScreenTitle, ScreenTitle,

                        MUIA_Window_CloseGadget, FALSE,
                        MUIA_Window_SizeGadget , FALSE,
                        MUIA_Window_TopEdge    , MUIV_Window_TopEdge_Centered,
                        MUIA_Window_LeftEdge   , MUIV_Window_LeftEdge_Centered,
                        MUIA_Window_Width      , MUIV_Window_Width_Visible(60),

                        WindowContents,
                           VGroup,
                           GroupFrame,

                           Child, TX_Prog_Info = TextObject,
                                                 MUIA_Text_PreParse, "\033l\0338",
                                                 TextFrame, TextBack,
                                                 End,

                           Child, GA_Prog_Info = GaugeObject,
                                                 TextFrame,
                                                 MUIA_Gauge_Horiz , TRUE,
                                                 MUIA_Gauge_InfoText, "",
                                                 End,

                           Child, ScaleObject, MUIA_Scale_Horiz, TRUE, End,
                           End,
                      End,

//|
///                LoginWindow

                 MUIA_Application_Window,
                     LoginWindow = WindowObject,
                        MUIA_Window_Title      , MSG_LOGIN_WIN_TITLE,
                        MUIA_Window_ID         , ID_WIN_LOGIN,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, HGroup,
                                  GroupFrame,
                                  MUIA_Group_Columns, 2,

                                  Child, MakeLabel2(MSG_USER_ED_PASSWD),
                                  Child, ST_Login_Has�o = MakeStringSecret(USER_PASSWD_LEN, MSG_LOGIN_PASSWD),

                                  End,

                           Child, HGroup,
                                  MUIA_Group_SameSize, TRUE,
                                  Child, BT_Login_Ok     = TextButton(MSG_LOGIN_OK),
                                  Child, BT_Login_Cancel = TextButton(MSG_LOGIN_CANCEL),
                                  End,

                           End,
                      End,
//|
///                OdbiorcaSelectorWindow

                 MUIA_Application_Window,
                     OdbiorcaSelectorWindow = WindowObject,
                        MUIA_Window_Title      , MSG_ODB_TITLE,
                        MUIA_Window_ID         , ID_WIN_ODBIORCASELECTOR,
                        MUIA_Window_ScreenTitle, ScreenTitle,
//                        MUIA_Window_CloseGadget, FALSE,
                        WindowContents,
                           VGroup,

                           Child, VGroup,
                                  GroupFrameT(MSG_ODB_SEL_TITLE),

                                  Child, LV_Pod_Odbiorcy = _ListviewObject,
                                                           MUIA_CycleChain, TRUE,
                                                           _MUIA_Listview_List, _ListObject,
                                                                 MUIA_Frame, MUIV_Frame_InputList,
                                                                 End,
                                                           End,
                                  End,

                                  Child, HGroup,
                                         Child, BT_Pod_Ok     = TextButton(MSG_ODB_OK),
                                         Child, BT_Pod_Add    = TextButton(MSG_ODB_ADD),
                                         Child, BT_Pod_Cancel = TextButton(MSG_ODB_CANCEL),
                                         End,

                           End,
                      End,
//|
///                StringRequestWindow

                 MUIA_Application_Window,
                     StringRequesterWindow = WindowObject,
                        MUIA_Window_ID         , ID_WIN_STRINGREQUEST,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, HGroup,
                                  GroupFrameT(MSG_STR_TITLE),
                                  Child, ST_Str_String = MakeString(STR_REQ_MAXLEN, MSG_STR_STRING),
                                  End,

                           Child, HGroup,
                                  MUIA_Group_SameSize, TRUE,
                                  Child, BT_Str_Ok     = TextButton(MSG_STR_OK),
                                  Child, BT_Str_Cancel = TextButton(MSG_STR_CANCEL),
                                  End,

                           End,
                      End,
//|
///                KontrahentSelectorWindow

                 MUIA_Application_Window,
                     KontrahentSelectorWindow = WindowObject,
                        MUIA_Window_Title      , MSG_KON_TITLE,
                        MUIA_Window_ID         , ID_WIN_KONTRAHENTSELECTOR,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, VGroup,
                                  GroupFrameT(MSG_KON_SEL_TITLE),

                                  Child, LV_Kon_Klienci = _ListviewObject,
                                                          MUIA_CycleChain, TRUE,
//                                                          _MUIA_Listview_List, NewObject(CL_CustomerList->mcc_Class, NULL,
                                                          _MUIA_Listview_List, Golem_CustListObject,
//                                                                      _MUIA_List_Format, "COL=2",
//                                                                       TAG_DONE),
                                                                       End,
                                                          End,

                                  Child, HGroup,
                                         Child, BT_Kon_Add     = TextButton(MSG_KON_DODAJ),
                                         Child, MUI_MakeObject(MUIO_VBar,1),
                                         Child, HGroup,
                                                MUIA_Weight, 20,
                                                Child, BT_Kon_Filter    = TextButton(MSG_MAG_FILTER),
                                                Child, BT_Kon_FilterAll = TextButton(MSG_MAG_FILTER_ALL),
                                                End,
                                         End,

                                  End,

                                  Child, HGroup,
                                         Child, BT_Kon_Ok     = TextButton(MSG_KON_OK),
                                         Child, BT_Kon_Cancel = TextButton(MSG_KON_CANCEL),
                                         End,


                           End,
                      End,
//|

///                ZakupyWindow

                 MUIA_Application_Window,
                     ZakupyWindow = WindowObject,
                        MUIA_Window_Title      , MSG_ZAK_TITLE,
                        MUIA_Window_ID         , ID_WIN_ZAKUPY,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, HGroup,
                                  Child, MakeLabel2(MSG_ZAK_MIESIAC),
                                  Child, CY_Zak_Miesiac = MakeCycle(Miesi�ceTable2, MSG_ZAK_MIESIAC),
                                  Child, BT_Zak_Load = TextButton(MSG_ZAK_LOAD),
                                  Child, BT_Zak_Save = TextButton(MSG_ZAK_SAVE),

                                  Child, MUI_MakeObject(MUIO_VBar,1),
                                  Child, BT_Zak_Index = TextButton(MSG_ZAK_INDEX),
                                  Child, BT_Zak_Sort  = TextButton(MSG_ZAK_SORT),
                                  End,

                           Child, VGroup,
                                  GroupFrame,
                                  Child, LV_Zak_Zakupy = _ListviewObject,
                                                MUIA_CycleChain, TRUE,
                                                MUIA_Font, MUIV_Font_Tiny,
                                                _MUIA_Listview_List, NewObject(CL_ZakupyList->mcc_Class, NULL, TAG_DONE),
                                                End,

                                  Child, HGroup,
                                         Child, BT_Zak_Add   = TextButton(MSG_ZAK_ADD),
                                         Child, BT_Zak_Edit  = TextButton(MSG_ZAK_EDIT),
                                         Child, BT_Zak_Del   = TextButton(MSG_ZAK_DEL),
                                         Child, BT_Zak_Info  = TextButton(MSG_ZAK_INFO),
                                         Child, BT_Zak_Print = TextButton(MSG_ZAK_PRINT),
                                         End,
                                  End,


                           Child, HGroup,
                                  Child, BT_Zak_Ok     = TextButton(MSG_ZAK_OK),
                                  Child, BT_Zak_Cancel = TextButton(MSG_ZAK_CANCEL),
                                  End,

                           End,
                      End,

//|
/*
///                EdycjaZakupuWindow

                 MUIA_Application_Window,
                     EdycjaZakupuWindow = WindowObject,
                        MUIA_Window_Title      , MSG_ZAK_EDIT_TITLE,
                        MUIA_Window_ID         , ID_WIN_ZAKUP_EDIT,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, GR_ZakE_Pages = RegisterGroup(GR_ZakE_Titles),
                                  Child, ColGroup(2),
                                         Child, MakeLabel2(MSG_ZAKE_P1),
                                         Child, ST_ZakE_LP = MakeNumericString(7, MSG_ZAKE_P1),
                                         Child, MakeLabel2(MSG_ZAKE_P2),
                                         Child, ST_ZakE_Data = MakeString(DATE_LEN, MSG_ZAKE_P2),
                                         Child, MakeLabel2(MSG_ZAKE_P3),
                                         Child, ST_ZakE_Numer = MakeString(STR_ZAK_DOW�D_LEN, MSG_ZAKE_P3),

                                         Child, BT_ZakE_Kontrahent = TextButton(MSG_ZAKE_P4),
                                         Child, TX_Zake_P4         = TextObject, TextFrame, TextBack, End,
                                         Child, MakeLabel2(NULL),
                                         Child, TX_Zake_P5         = TextObject, TextFrame, TextBack, End,
                                         End,

                                  Child, ColGroup(2),
                                         Child, MakeLabel2(MSG_ZAKE_P6),
                                         Child, ST_ZakE_Opis = MakeString(STR_ZAK_OPIS_LEN, MSG_ZAKE_P6),

                                         Child, MakeLabel2(MSG_ZAKE_P7),
                                         Child, ST_ZakE_P7 = MakeCashString(10, MSG_ZAKE_P7),
                                         Child, MakeLabel2(MSG_ZAKE_P8),
                                         Child, ST_ZakE_P8 = MakeCashString(10, MSG_ZAKE_P8),

                                         Child, MakeLabel2(MSG_ZAKE_P10),
                                         Child, ST_ZakE_P10 = MakeCashString(10, MSG_ZAKE_P10),
                                         Child, MakeLabel2(MSG_ZAKE_P11),
                                         Child, ST_ZakE_P11 = MakeCashString(10, MSG_ZAKE_P11),
                                         End,

                                  Child, ColGroup(2),
                                         Child, MakeLabel2(MSG_ZAKE_P12),
                                         Child, ST_ZakE_P12 = MakeCashString(10, MSG_ZAKE_P12),
                                         Child, MakeLabel2(MSG_ZAKE_P13),
                                         Child, ST_ZakE_P13 = MakeCashString(10, MSG_ZAKE_P13),
                                         Child, MakeLabel2(MSG_ZAKE_P14),
                                         Child, ST_ZakE_P14 = MakeCashString(10, MSG_ZAKE_P14),

                                         Child, MakeLabel2(MSG_ZAKE_P16),
                                         Child, ST_ZakE_P16 = MakeCashString(10, MSG_ZAKE_P16),
                                         Child, MakeLabel2(MSG_ZAKE_P17),
                                         Child, ST_ZakE_P17 = MakeString(STR_ZAK_UWAGI_LEN, MSG_ZAKE_P17),
                                         End,

                                  End,

                           Child, HGroup,
                                  Child, BT_ZakE_Ok     = TextButton(MSG_ZAKE_OK),
                                  Child, BT_ZakE_Cancel = TextButton(MSG_ZAKE_CANCEL),
                                  End,

                           End,
                      End,

//|
*/
///                EdycjaZakupuWindow

                 MUIA_Application_Window,
                     EdycjaZakupuWindow = WindowObject,
                        MUIA_Window_Title      , MSG_ZAK_EDIT_TITLE,
                        MUIA_Window_ID         , ID_WIN_ZAKUP_EDIT,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, GR_ZakE_Pages = RegisterGroup(GR_ZakE_Titles),
                                  Child, VGroup,
                                         Child, ColGroup(2),
                                                Child, MakeLabel2(MSG_ZAKE_P1),
                                                Child, ST_ZakE_P1 = MakeNumericString(7, MSG_ZAKE_P1),
                                                Child, MakeLabel2(MSG_ZAKE_P2),
                                                Child, ST_ZakE_P2 = MakeString(STR_ZAK_DOW�D_LEN, MSG_ZAKE_P2),
                                                Child, MakeLabel2(MSG_ZAKE_P3),
                                                Child, ST_ZakE_P3 = MakeString(DATE_LEN, MSG_ZAKE_P3),
                                                Child, MakeLabel2(MSG_ZAKE_P4),
                                                Child, ST_ZakE_P4 = MakeString(DATE_LEN, MSG_ZAKE_P4),

                                                Child, BT_ZakE_Kontrahent = TextButton(MSG_ZAKE_P5),
                                                Child, TX_ZakE_P5         = TextObject, TextFrame, TextBack, End,
                                                Child, MakeLabel2(NULL),
                                                Child, TX_ZakE_P6         = TextObject, TextFrame, TextBack, End,
                                                Child, MakeLabel2(NULL),
                                                Child, TX_ZakE_P7         = TextObject, TextFrame, TextBack, End,
                                                End,

                                         Child, HVSpace,
                                         Child, HGroup,
                                                Child, HVSpace,
                                                Child, BT_ZakE_Next1 = TextButtonWeight(MSG_ZAKE_NEXT, 5),
                                                End,
                                         End,

                                  /* Page #2 */

                                  Child, VGroup,
                                         Child, TextObject, TextFrame, TextBack,
                                                MUIA_Font, MUIV_Font_Tiny,
                                                MUIA_Text_Contents, MSG_ZAKE_INFO1,
                                                End,
                                         Child, ColGroup(2),
                                                Child, MakeLabel2(MSG_ZAKE_P9),
                                                Child, ST_ZakE_P9  = MakeCashString(10, MSG_ZAKE_P9),
                                                End,

                                         Child, TextObject, TextFrame, TextBack,
                                                MUIA_Font, MUIV_Font_Tiny,
                                                MUIA_Text_Contents, MSG_ZAKE_INFO2,
                                                End,
                                         Child, ColGroup(2),
                                                Child, MakeLabel2(MSG_ZAKE_P10),
                                                Child, HGroup,
                                                       Child, ST_ZakE_P10 = MakeCashString(10, MSG_ZAKE_P10),
                                                       Child, MakeLabel2(MSG_ZAKE_VAT1),
                                                       Child, TX_ZakE_P11 = TextObject, TextFrame, TextBack, End,
                                                       Child, MakeLabel2(MSG_ZAKE_VAT2),
                                                       End,
                                                Child, MakeLabel2(MSG_ZAKE_P12),
                                                Child, HGroup,
                                                Child, ST_ZakE_P12 = MakeCashString(10, MSG_ZAKE_P12),
                                                       Child, MakeLabel2(MSG_ZAKE_VAT1),
                                                       Child, TX_ZakE_P13 = TextObject, TextFrame, TextBack, End,
                                                       Child, MakeLabel2(MSG_ZAKE_VAT2),
                                                       End,
                                                End,
                                         Child, HVSpace,
                                         Child, HGroup,
                                                Child, HVSpace,
                                                Child, BT_ZakE_Next2 = TextButtonWeight(MSG_ZAKE_NEXT, 5),
                                                End,
                                         End,

                                  /* Page #3 */

                                  Child, VGroup,
                                         Child, TextObject, TextFrame, TextBack,
                                                MUIA_Font, MUIV_Font_Tiny,
                                                MUIA_Text_Contents, MSG_ZAKE_INFO3,
                                                End,

                                         Child, ColGroup(2),
                                                Child, MakeLabel2(MSG_ZAKE_P14),
                                                Child, HGroup,
                                                       Child, ST_ZakE_P14 = MakeCashString(10, MSG_ZAKE_P14),
                                                       Child, MakeLabel2(MSG_ZAKE_VAT1),
                                                       Child, TX_ZakE_P15 = TextObject, TextFrame, TextBack, End,
                                                       Child, MakeLabel2(MSG_ZAKE_VAT2),
                                                       End,
                                                Child, MakeLabel2(MSG_ZAKE_P16),
                                                Child, HGroup,
                                                       Child, ST_ZakE_P16 = MakeCashString(10, MSG_ZAKE_P16),
                                                       Child, MakeLabel2(MSG_ZAKE_VAT1),
                                                       Child, TX_ZakE_P17 = TextObject, TextFrame, TextBack, End,
                                                       Child, MakeLabel2(MSG_ZAKE_VAT2),
                                                       End,
                                                End,
                                         Child, HVSpace,
                                         Child, HGroup,
                                                Child, HVSpace,
                                                Child, BT_ZakE_Next3 = TextButtonWeight(MSG_ZAKE_NEXT, 5),
                                                End,
                                         End,
                                  End,

/*
                           Child, ColGroup(2),
                                  Child, MakeLabel2(MSG_ZAKE_P8),
                                  Child, TX_ZakE_P8         = TextObject, TextFrame, TextBack, End,
                                  Child, MakeLabel2(MSG_ZAKE_P18),
                                  Child, TX_ZakE_P18        = TextObject, TextFrame, TextBack, End,
                                  End,
*/

                           Child, HGroup,
                                  Child, BT_ZakE_Ok     = TextButton(MSG_ZAKE_OK),
                                  Child, BT_ZakE_Cancel = TextButton(MSG_ZAKE_CANCEL),
                                  End,

                           End,
                      End,

//|
///                InformacjeOZakupachWindow

                 MUIA_Application_Window,
                     InformacjeOZakupachWindow = WindowObject,
                        MUIA_Window_Title      , MSG_ZAK_INFO_TITLE,
                        MUIA_Window_ID         , ID_WIN_ZAKUP_INFO,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        WindowContents,
                           VGroup,

                           Child, ColGroup(2),
                                  Child, MakeLabel2(MSG_ZAKI_COUNT),
                                  Child, TX_ZakI_Count = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033l", End,
                                  End,

                           Child, TextObject, TextFrame, TextBack,
                                  MUIA_Font, MUIV_Font_Tiny,
                                  MUIA_Text_Contents, MSG_ZAKI_INFO1,
                                  End,
                           Child, ColGroup(2),
                                  GroupFrame, GroupBack,
                                  Child, MakeLabel2(MSG_ZAKI_P9),
                                  Child, TX_ZakI_P9 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                  End,

                           Child, TextObject, TextFrame, TextBack,
                                  MUIA_Font, MUIV_Font_Tiny,
                                  MUIA_Text_Contents, MSG_ZAKE_INFO2,
                                  End,
                           Child, ColGroup(4),
                                  GroupFrame, GroupBack,
                                  Child, MakeLabel2(MSG_ZAKI_P10),
                                  Child, TX_ZakI_P10 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                  Child, MakeLabel2(MSG_ZAKI_P11),
                                  Child, TX_ZakI_P11 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                  Child, MakeLabel2(MSG_ZAKI_P12),
                                  Child, TX_ZakI_P12 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                  Child, MakeLabel2(MSG_ZAKI_P13),
                                  Child, TX_ZakI_P13 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                  End,

                           Child, TextObject, TextFrame, TextBack,
                                  MUIA_Font, MUIV_Font_Tiny,
                                  MUIA_Text_Contents, MSG_ZAKE_INFO3,
                                  End,
                           Child, ColGroup(4),
                                  GroupFrame, GroupBack,
                                  Child, MakeLabel2(MSG_ZAKI_P14),
                                  Child, TX_ZakI_P14 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                  Child, MakeLabel2(MSG_ZAKI_P15),
                                  Child, TX_ZakI_P15 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                  Child, MakeLabel2(MSG_ZAKI_P16),
                                  Child, TX_ZakI_P16 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                  Child, MakeLabel2(MSG_ZAKI_P17),
                                  Child, TX_ZakI_P17 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                  End,

                           Child, ColGroup(2),
                                  GroupFrame, GroupBack,
                                  Child, MakeLabel2(MSG_ZAKI_P8),
                                  Child, TX_ZakI_P8  = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                  Child, MakeLabel2(MSG_ZAKI_P18),
                                  Child, TX_ZakI_P18 = TextObject, TextFrame, TextBack, MUIA_Text_PreParse, "\033r", End,
                                  End,

                           Child, HGroup,
                                  Child, BT_ZakI_Ok     = TextButton(MSG_ZAKI_OK),
                                  End,

                           End,
                      End,

//|
///                IndexRequesterWindow

                 MUIA_Application_Window,
                     IndexRequesterWindow = WindowObject,
                        MUIA_Window_ID         , ID_WIN_INDEXREQUEST,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        MUIA_Window_Title, MSG_IDX_TITLE,
                        WindowContents,
                           VGroup,

                           Child, HGroup,
                                  GroupFrameT(MSG_IDX_GTITLE),
                                  Child, ST_Idx_String = MakeNumericString(STR_REQ_MAXLEN, MSG_STR_STRING),
                                  End,

                           Child, HGroup,
                                  MUIA_Group_SameSize, TRUE,
                                  Child, BT_Idx_Ok     = TextButton(MSG_IDX_OK),
                                  Child, BT_Idx_Cancel = TextButton(MSG_IDX_CANCEL),
                                  End,

                           End,
                      End,
//|
///                SortRequesterWindow

                 MUIA_Application_Window,
                     SortRequesterWindow = WindowObject,
                        MUIA_Window_ID         , ID_WIN_SORTREQUEST,
                        MUIA_Window_ScreenTitle, ScreenTitle,
                        MUIA_Window_Title, MSG_SORT_TITLE,
                        WindowContents,
                           VGroup,

                           Child, HGroup,
                                  GroupFrameT(MSG_SORT_GTITLE),
                                  Child, HVSpace,
                                  Child, GR_Sort_Order = VGroup,
                                         End,
                                  Child, HVSpace,
                                  End,

                           Child, HGroup,
                                  GroupFrameT(MSG_SORT_GTITLE2),
                                  Child, HVSpace,
                                  Child, MakeLabel2(MSG_SORT_ZA),
                                  Child, CH_Sort_Reverse = MakeCheck(MSG_SORT_ZA),
                                  Child, HVSpace,
                                  End,

                           Child, HGroup,
                                  MUIA_Group_SameSize, TRUE,
                                  Child, BT_Sort_Ok     = TextButton(MSG_IDX_OK),
                                  Child, BT_Sort_Cancel = TextButton(MSG_IDX_CANCEL),
                                  End,

                           End,
                      End,
//|


                 End;



    if(app)
       {

       /* STARTUP WINDOW */

       DoMethod(BT_Startup_Ok, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_STARTUP_CLOSED);


       /* MAIN WINDOW    */

       DoMethod(MainWindow         ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, MUIV_Application_ReturnID_Quit);

       DoMethod(BT_ToolBar_Sprzeda�         , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_SPRZEDAZ);
           DoMethod(BT_ToolBar_Vat          , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_VAT);
           DoMethod(BT_ToolBar_Upr          , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_UPROSZCZONY);
           DoMethod(BT_ToolBar_Vat_Par      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_VAT_PAR);
           DoMethod(BT_ToolBar_Upr_Par      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_UPROSZCZONY_PAR);
           DoMethod(BT_ToolBar_Paragon      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_PARAGON);
           DoMethod(BT_ToolBar_Sprzeda�_Back, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAINMENU);
       DoMethod(BT_ToolBar_Zakupy     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ZAKUPY);
           DoMethod(BT_ToolBar_Zakupy_Koszty, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ZAKUPY_KOSZTY);
           DoMethod(BT_ToolBar_Zakupy_Back  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAINMENU);
       DoMethod(BT_ToolBar_Redagowanie, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_REDAGOWANIE);
       DoMethod(BT_ToolBar_BazyDanych , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_BAZYDANYCH);
           DoMethod(BT_ToolBar_Magazyn    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAGAZYN);
           DoMethod(BT_ToolBar_Kontrahenci, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_KONTRAHENCI);
           DoMethod(BT_ToolBar_UserBase   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_USERS);
           DoMethod(BT_ToolBar_Bazy_Back, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAINMENU);
       DoMethod(BT_ToolBar_Rozliczenia, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ROZLICZENIA);
           DoMethod(BT_ToolBar_Rozliczenia_Fisk_Dobowy  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_DOBOWY);
           DoMethod(BT_ToolBar_Rozliczenia_Fisk_Okresowy, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_OKRESOWY);
           DoMethod(BT_ToolBar_Rozliczenia_Fisk_StanKasy, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_STAN_KASY);
           DoMethod(BT_ToolBar_Rozliczenia_Fisk_RaportZmiany , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_POSNET_RAPORT_ZMIANY);
           DoMethod(BT_ToolBar_Rozliczenia_Back         , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAINMENU);
       DoMethod(BT_ToolBar_System, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_SYSTEM);
           DoMethod(BT_ToolBar_Settings   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_SETTINGS);
           DoMethod(BT_ToolBar_System_Back, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_MAINMENU);
       DoMethod(BT_ToolBar_About      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_ABOUT);
       DoMethod(BT_ToolBar_Login      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_LOGIN);
       DoMethod(BT_ToolBar_Logout     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_TOOLBAR_LOGOUT);
       DoMethod(BT_ToolBar_Quit       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, MUIV_Application_ReturnID_Quit);


       /* MAGAZYN WINDOW */

//       set(PB_Mag_Grupy, MUIA_CycleChain, TRUE);

       DoMethod(MagazynWindow  ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(ST_Mag_Wybierz, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_WYBIERZ);
       set(ST_Mag_Wybierz, MUIA_String_AttachedList, LV_Mag_Produkty);
       DoMethod(BT_Mag_Znajd�   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_ZNAJD�);

       DoMethod(BT_Mag_DodajGrup�  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_DODAJ_GRUP�);
       DoMethod(BT_Mag_Usu�Grup�   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_USU�_GRUP�);
       DoMethod(BT_Mag_EdytujGrup� ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_EDYCJA_GRUPY);
       DoMethod(LV_Mag_Grupy, MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_ZMIE�_GRUP�);


       DoMethod(BT_Mag_Wydrukuj ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_PRINT);
       set(BT_Mag_Wydrukuj, MUIA_Disabled, TRUE);

       DoMethod(BT_Mag_Dodaj    ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_DODAJ);
       DoMethod(BT_Mag_Usu�     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_SKASUJ);
       DoMethod(BT_Mag_Edytuj   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_EDYTUJ);
       DoMethod(BT_Mag_Przenie� ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MOVE);

       DoMethod(BT_Mag_Filter     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_FILTER);
       DoMethod(BT_Mag_FilterAll  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_FILTER_ALL);

       DoMethod(BT_Mag_Ok      ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Mag_Cancel  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_DODAJ);
       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_EDYTUJ);
       DoMethod(LV_Mag_Produkty      , MUIM_Notify, _MUIA_Listview_DoubleClick, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_EDYTUJ);
       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_SKASUJ);
       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "control del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_SKASUJ_AND_SKIP);
       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MOVE);
       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "f7", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_MAG_ZNAJD�);
       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);
//       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "esc", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "shift left" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FIRST_GROUP);
       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "left" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PREV_GROUP);
       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "right", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_NEXT_GROUP);
       DoMethod(MagazynWindow        , MUIM_Notify, MUIA_Window_InputEvent, "shift right", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_LAST_GROUP);


       /* EDYCJA PRODUKTU WINDOW */

       DoMethod(EdycjaProduktuWindow   ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_Mag_Ed_Ok     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Mag_Ed_Cancel ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(ST_Mag_Zakup        , MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_ZAKUP_ACK);
       DoMethod(ST_Mag_Mar�a        , MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_MARZA_ACK);
//       DoMethod(ST_Mag_Mar�a_Procent, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_MARZA_PROCENT_ACK);
       DoMethod(ST_Mag_Brutto       , MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_BRUTTO_ACK);
       DoMethod(CY_Mag_Vat          , MUIM_Notify, MUIA_Cycle_Active   , MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_VAT_ACK);

//       DoMethod(ST_Mag_Zakup     ,MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_SPRZEDAZ);
//       DoMethod(ST_Mag_Mar�a     ,MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_SPRZEDAZ);
//       DoMethod(ST_Mag_Sprzeda�  ,MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_BRUTTO);
//       DoMethod(ST_Mag_Calc_Netto,MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_BRUTTO_CALC);
//       DoMethod(BT_Mag_Calc_Copy ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_COPY);
//       DoMethod(ST_Mag_Brutto    ,MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_NETTO);
//       DoMethod(CY_Mag_Vat       ,MUIM_Notify, MUIA_Cycle_Active   , MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_CALC_BRUTTO_VAT);

       DoMethod(EdycjaProduktuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_0);
       DoMethod(EdycjaProduktuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_1);


       /* FILTR MAGAZYNOWY WINDOW */

       DoMethod(FiltrWindow    , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_Filtr_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Filtr_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


       /* GRUPY WINDOW */

       DoMethod(EdycjaGrupyWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_Grupa_Ok      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Grupa_Dodaj   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);
       DoMethod(BT_Grupa_Cancel  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);



       /* KONTRAHENCI WINDOW */

       DoMethod(KontrahenciWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_Cust_Ok        , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Cust_Cancel    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(BT_Cust_Dodaj     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_DODAJ);
       DoMethod(BT_Cust_Edytuj    ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_EDYTUJ);
       DoMethod(BT_Cust_Usu�      ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_USU�);
       DoMethod(KontrahenciWindow ,MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_DODAJ);
       DoMethod(KontrahenciWindow ,MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_EDYTUJ);
       DoMethod(LV_Cust_Klienci   ,MUIM_Notify, _MUIA_Listview_DoubleClick, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_EDYTUJ);
       DoMethod(KontrahenciWindow ,MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_USU�);
       DoMethod(KontrahenciWindow ,MUIM_Notify, MUIA_Window_InputEvent, "control del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_USU�_AND_SKIP);

       DoMethod(KontrahenciWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);
//       DoMethod(KontrahenciWindow, MUIM_Notify, MUIA_Window_InputEvent, "esc", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(BT_Cust_Filter     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_FILTER);
       DoMethod(BT_Cust_FilterAll  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_FILTER_ALL);



       /* EDYCJA KONTRAHENT�W */

       DoMethod(EdycjaKontrahentaWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(EdycjaKontrahentaWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_0);
       DoMethod(EdycjaKontrahentaWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_1);
       DoMethod(EdycjaKontrahentaWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_2);

       DoMethod(CH_Cust_SC             , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_SC);
       set(CH_Cust_SC, MUIA_CycleChain, FALSE);

       DoMethod(CH_Cust_UpVat          , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_UPVAT);
       DoMethod(CH_Cust_UpVat_Unlimited, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_Cust_UpVat_Expire, 3, MUIM_Set, MUIA_Disabled, MUIV_TriggerValue);

       DoMethod(CH_Cust_AddEnabled     , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, CH_Cust_FIFO, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue);

       DoMethod(BT_Cust_Ed_Ok          , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Cust_Ed_Cancel      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


       /* FILTR KLIENT�W WINDOW */

       DoMethod(CustFiltrWindow     , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_Cust_Filtr_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Cust_Filtr_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);



       /* FAKTURA WINDOW */

       DoMethod(FakturaWindow  , MUIM_Notify    , MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

//     Notyfikacja ustawiania w SMARTCYCLE/OM_SET!!!
//       DoMethod(CY_Fak_Typ_P�atno�ci, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_TYP_P�ATNO�CI);

       DoMethod(BT_Fak_Odbiorca, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_ODBIORCA);

       DoMethod(ST_Fak_Ilo��, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_CALC_WARTO��);
       DoMethod(ST_Fak_Rabat, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_CALC_WARTO��);

       DoMethod(ST_Fak_Wybierz, MUIM_Notify             , MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_WYBIERZ);
       DoMethod(ST_Fak_Wybierz  , MUIM_Notify    , MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_PROD_ACK);
       set(ST_Fak_Wybierz     , MUIA_String_AttachedList, LV_Fak_Produkty);
       DoMethod(BT_Fak_Znajd� , MUIM_Notify             , MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_ZNAJD�);
       set(BT_Fak_Znajd�      , MUIA_CycleChain         , FALSE);

       DoMethod(ST_Fak_Rabat, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Window, 3 , MUIM_Set, MUIA_Window_ActiveObject, BT_Fak_Akceptuj�);


       DoMethod(LV_Fak_Grupy, MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_ZMIE�_GRUP�);
       DoMethod(LV_Fak_Produkty , MUIM_Notify    , _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_PROD_ACK_LV);

       DoMethod(LV_Fak_FakturaProdukty, MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_USU�);
       DoMethod(BT_Fak_Usu�           , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_USU�);

       DoMethod(BT_Fak_Filter   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_FILTER);
       DoMethod(BT_Fak_FilterAll, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_MAG_FILTER_ALL);

       DoMethod(BT_Fak_Ok       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Fak_Cancel   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FAK_FREE_HAND);
       DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "f6", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FAK_ODBIORCA);
       DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);
       DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FAK_USU�);

       DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "shift left" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_FIRST_GROUP);
       DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "left" , MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PREV_GROUP);
       DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "right", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_NEXT_GROUP);
       DoMethod(FakturaWindow   , MUIM_Notify, MUIA_Window_InputEvent, "shift right", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_LAST_GROUP);

       DoMethod(BT_Fak_Akceptuj�, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ACCEPT);
       DoMethod(BT_Fak_Nast�pny , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Window, 3 ,MUIM_Set, MUIA_Window_ActiveObject, ST_Fak_Wybierz);
       DoMethod(BT_Fak_ZR�ki    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_FREE_HAND);


       /* NAG�OWEK DOKUMENTU - KONTRAHENT */

       DoMethod(OdbiorcaDokumentuWindow  ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(BT_Odb_Wybierz , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_KLIENT_WYBIERZ);
       DoMethod(BT_Odb_Odbiorca, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FAK_KLIENT_ODBIORCA);

       DoMethod(BT_Odb_Ok      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Odb_Cancel  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


       /* GROUP SELECTOR */

       DoMethod(GroupSelectorWindow  ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(LV_Sel_Grupy , MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);

       DoMethod(BT_Sel_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Sel_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


       /* DRUKOWANIE REQUESTER */

       DoMethod(DrukowanieWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_BACK);

       set(CH_Print_Paragon, MUIA_CycleChain, FALSE);

       DoMethod(BT_Print_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Print_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_Print_Back   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_BACK);



       /* USERS WINDOW */

       DoMethod(UserWindow      ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(LV_User_Employers, MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_USER_EDIT);

       DoMethod(BT_User_Add     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_USER_ADD);
       DoMethod(BT_User_Edit    ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_USER_EDIT);
       DoMethod(BT_User_Delete  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_USER_DEL);

       DoMethod(UserWindow      , MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_USER_ADD);
       DoMethod(UserWindow      , MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_USER_EDIT);
       DoMethod(UserWindow      , MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);
       DoMethod(UserWindow      , MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_USER_DEL);

       DoMethod(BT_User_Ok      ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_User_Cancel  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


       /* EDYCJA U�YTKOWNIKA */

       DoMethod(EdycjaU�ytkownikaWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(BT_UserEdit_Access , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ACCESS);

       DoMethod(BT_UserEdit_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_UserEdit_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


       /* EDYCJA JEDNOSTKI MIARY */

       DoMethod(EdycjaJednostkiWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(BT_Unit_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Unit_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       set(CH_Unit_Frac, MUIA_CycleChain, FALSE);

       /* EDYCJA RODZAJU P�ATNO�CI */

       DoMethod(EdycjaP�atno�ciWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       set(CH_Pay_Cash     , MUIA_CycleChain, FALSE);
       DoMethod(CH_Pay_Cash, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_Pay_Delay, 3, MUIM_Set, MUIA_Disabled, MUIV_TriggerValue);

       DoMethod(BT_Pay_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Pay_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);




       /* LOGIN WINDOW */

       DoMethod(LoginWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

//       DoMethod(ST_Login_Has�o , MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);

       DoMethod(BT_Login_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Login_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


       /* REDAGOWANIE WINDOW */

//       DoMethod(RedagowanieWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);

       DoMethod(CY_Red_Szukaj, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, GR_Red_Szukaj1, 3, MUIM_Set, MUIA_Group_ActivePage, MUIV_TriggerValue);
       DoMethod(CY_Red_Szukaj, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, GR_Red_Szukaj2, 3, MUIM_Set, MUIA_Group_ActivePage, MUIV_TriggerValue);
//       DoMethod(CY_Red_Szukaj, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, MUIV_Notify_Application, 3, MUIM_Application_ReturnID, ID_RED_DOC_TYPE);
       DoMethod(LV_Red_Dokumenty, MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_VIEW);


       DoMethod(BT_Red_Redaguj   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_RED);
       DoMethod(BT_Red_Anuluj    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_CANCEL);
       DoMethod(BT_Red_Korekta   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_KOREKTA);
       DoMethod(BT_Red_Wyswietl  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_VIEW);
       DoMethod(BT_Red_Wydrukuj  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_RED_PRINT);
       DoMethod(BT_Red_Usu�      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_DELETE);
       DoMethod(BT_Red_Find      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FIND);
       DoMethod(BT_Red_Sort      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SORT);
       DoMethod(BT_Red_Ok        , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);

       DoMethod(BT_Red_View_Back , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_BACK);


       /* FREE HAND WINDOW */

       DoMethod(FreeHandWindow   ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(BT_Free_Ok       ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Free_Cancel   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(ST_Free_Zakup        , MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FREE_ZAKUP_ACK);
       DoMethod(ST_Free_Mar�a        , MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FREE_MARZA_ACK);
//       DoMethod(ST_Free_Mar�a_Procent, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FREE_MARZA_PROCENT_ACK);
       DoMethod(ST_Free_Brutto       , MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FREE_BRUTTO_ACK);
       DoMethod(CY_Free_Vat          , MUIM_Notify, MUIA_Cycle_Active   , MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_FREE_VAT_ACK);


       /* ODBIORCA SELECTOR */

       DoMethod(OdbiorcaSelectorWindow   ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(LV_Pod_Odbiorcy , MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);

       DoMethod(OdbiorcaSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ADD);

       DoMethod(BT_Pod_Ok       ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Pod_Add      ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);
       DoMethod(BT_Pod_Cancel   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


       /* STRING REQUESTER */

       DoMethod(StringRequesterWindow   ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(BT_Str_Ok       ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Str_Cancel   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       /* KONTRAHENT REQUESTER */

       DoMethod(KontrahentSelectorWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(BT_Kon_Add      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);
       DoMethod(BT_Kon_Filter   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_FILTER);
       DoMethod(BT_Kon_FilterAll, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_FILTER_ALL);

       DoMethod(KontrahentSelectorWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ADD);

       DoMethod(LV_Kon_Klienci  , MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);

       DoMethod(BT_Kon_Ok       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Kon_Cancel   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


       /* POSNET RAPORT */

       DoMethod(PosnetRaportWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(BT_Pos_Ok       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Pos_Cancel   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


       /* RESZTA WINDOW */

       DoMethod(ResztaWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       set(ST_Res_Wp�ata, MUIA_String_Format, MUIV_String_Format_Right);
       DoMethod(ST_Res_Wp�ata, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_STR_ACK);

       DoMethod(BT_Res_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Res_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_Res_Back  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_BACK);


       /* ZAKUPY WINDOW */

       DoMethod(ZakupyWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_Zak_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Zak_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(ZakupyWindow , MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_ADD);
       DoMethod(LV_Zak_Zakupy, MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_EDIT);

       DoMethod(ZakupyWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_EDIT);
       DoMethod(ZakupyWindow, MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_DELETE);

       DoMethod(BT_Zak_Add   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ADD);
       DoMethod(BT_Zak_Edit  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_EDIT);
       DoMethod(BT_Zak_Del   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_DELETE);
       DoMethod(BT_Zak_Info  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_INFO);

       DoMethod(BT_Zak_Load  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_LOAD);
       DoMethod(BT_Zak_Save  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SAVE);
       DoMethod(BT_Zak_Index , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_INDEX);
       DoMethod(BT_Zak_Sort  , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SORT);


       /* EDYCJA ZAKUPU WINDOW */

       DoMethod(EdycjaZakupuWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_ZakE_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_ZakE_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       DoMethod(EdycjaZakupuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_0);
       DoMethod(BT_ZakE_Next1     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_PAGE_1);
       DoMethod(EdycjaZakupuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_1);
       DoMethod(BT_ZakE_Next2     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_PAGE_2);
       DoMethod(EdycjaZakupuWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_2);
       DoMethod(BT_ZakE_Next3     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_PAGE_0);

       DoMethod(ST_ZakE_P10    , MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ZAKE_CALC_P11);
       DoMethod(ST_ZakE_P12    , MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ZAKE_CALC_P13);
       DoMethod(ST_ZakE_P14    , MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ZAKE_CALC_P15);
       DoMethod(ST_ZakE_P16    , MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_ZAKE_CALC_P17);

       DoMethod(BT_ZakE_Kontrahent, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_KLIENT_WYBIERZ);


       /* INFORMACJE O ZAKUPACH WINNDOW */

       DoMethod(InformacjeOZakupachWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_ZakI_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);


       /* INDEX REQUESTER WINDOW */

       DoMethod(IndexRequesterWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_Idx_Ok           , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Idx_Cancel       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


       /* INDEX REQUESTER WINDOW */

       DoMethod(SortRequesterWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
       DoMethod(BT_Sort_Ok         , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
       DoMethod(BT_Sort_Cancel     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

       }

    return(app);
}

