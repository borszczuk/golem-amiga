

/*
** $Id: golem_dirs.h,v 1.1 2003/01/01 20:40:54 carl-os Exp $
*/


#define GolemTemplateDir      "PROGDIR:wfmh_wzorce"

#define GolemConfigDir        "PROGDIR:wfmh_ustawienia"
#define UstawieniaFileName    GolemConfigDir "/ustawienia.golem"
//#define KodyDrukarkiFileName  GolemConfigDir "/drukarka.golem"
#define DrukarkiFileName      GolemConfigDir "/drukarki.golem"
//#define UstawieniaSQLFileName GolemConfigDir "/sql.golem"

#define DatabasesDir          "PROGDIR:wfmh_dane"
#define MagazynFileName       DatabasesDir "/magazyn.golem"
#define MagazynCountFileName  DatabasesDir "/magazyn_info.golem"
#define MagazynFileNameBak    MagazynFileName ".bak"
#define KlienciFileName       DatabasesDir "/klienci.golem"
#define KlienciCountFileName  DatabasesDir "/klienci_info.golem"
#define NumeracjaFileName     DatabasesDir "/numeracja.golem"
#define JednostkiFileName     DatabasesDir "/jednostki.golem"
#define P�atno�ciFileName     DatabasesDir "/platnosci.golem"
#define SystemFileName        DatabasesDir "/system.golem"
#define U�ytkownicyFileName   DatabasesDir "/pracownicy.golem"
#define UIDFileName           DatabasesDir "/uid.golem"

#define BankiDir              "PROGDIR:wfmh_banki"

#define StatusFileName        "status.golem"
#define ZakupyFileName        "zakupy.golem"


