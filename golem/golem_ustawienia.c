
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
$Id: golem_ustawienia.c,v 1.1 2003/01/01 20:40:52 carl-os Exp $.
*/

#include "golem.h"

#define HVSPACE

/// PROGRAM SETTINGS

struct Settings settings =
{

        '.',                            // def_dot

        "",                             // def_miara
        4,                              // def_vat    0-zw, 1-0%, 2-7%, 3-17%, 4-22%

        "",                             // def_miasto
        360,                            // def_expire_offset
        0,                              // def_upvat
        1,                              // def_unlimited
        1,                              // def_addenabled
        1,                              // def_fifo

        0,                              // def_sprzedawca
        1,                              // def_nabywca
        0,                              // def_kosztowiec
        0,                              // def_a
        0,                              // def_b
        0,                              // def_c
        1,                              // clone_filter

        1,                              // reszta_aktywna
        1,                              // paragon_fvat
        1,                              // paragon_rach
        1,                              // paragon_par
        1,                              // paragon_inne

        0,                              // sta�a mar�a procentowa



        0,                              // posnet_active
        "serial.device",                // serial_Device
        0,                              // device unit
        4,                              // ptu_a (22)
        3,                              // ptu_b (17)
        1,                              // ptu_c ( 7)
        2,                              // ptu_d ( 0)
        5,                              // ptu_e ( 3)
        0,                              // ptu_z (zw)

        0,                              // open_drawer
        0,                              // posnet_idle
        " Golem - Amiga",               // posnet_idle_txt1
        "(C)2002 W.F.M.H.",             // posnet_idle_txt2
        0,                              // posnet_nadruki
        "Dzi�kujemy!",                  // posnet_nadruk_1
        "-----------",                  // posnet_nadruk_2
        "Zapraszamy!",                  // posnet_nadruk_3


        0,                              // wystawca
        0,
        1,
        1,
        1,
        0,                              // sprzedawca
        0,                              // nabywca
        0,                              // kosztowiec
        0,                              // a
        0,                              // b
        0,                              // c
        0,                              // separator



        "W.F.M.H.",
        "",
        "P.O. Box 91",
        "71-507",
        "Szczecin 5",
        "8521005435",
        "",
        "BPH o/o Szczecin",
        "10601624-320000240441",
        "(091) 4836954 w.221",
        "(091) 4324212",
        "info@amiga.com.pl",

        0,
        "Marcin Or�owski",
        "Patryk �ogiewa",
        "",
        "",



        0.0,                            // def_Rabat


        0,                              // order_fvat
        1,                              // type_fvat
        5.0,                            // fvat_min
        1,                              // sug_rach
        0,                              // order_fvat_kor
        1,                              // type_fvat_kor
        0,                              // order_fvat_par
        1,                              // type_fvat_par

        0,                              // order_rach
        1,                              // type_rach
        0,                              // order_rach_kor
        1,                              // type_rach_kor
        0,                              // order_rach_par
        1,                              // type_rach_par

        0,                              // order_order
        1,                              // type_order


        "Upowa�nienie VAT",             // def_upvat_text

        "",                             // text_fv1
        "",                             // text_fv2
        "",                             // text_fv3
        "",                             // text_fv4
        "",                             // text_fv5
        "",                             // text_fv6
        "",                             // text_fv7
        "",                             // text_fv8
        "",                             // text_fv9
        "",                             // text_fv0


        0,                                         // com_call
        "SYS:Rexxc/RC ARexx/DodajZdarzenie.golem", // com_sale


        DEFAULT_PRINTER,                // map_fvat;
        DEFAULT_PRINTER,                // map_fvat_proforma;
        DEFAULT_PRINTER,                // short map_paragon;
        DEFAULT_PRINTER,                // short map_zamowienie;

        DEFAULT_PRINTER,                // cennik detaliczny
        DEFAULT_PRINTER,                // cennik hurtowy
        DEFAULT_PRINTER,                // spis z natury

        DEFAULT_PRINTER,                // stany magazynowe

        DEFAULT_PRINTER                 // Przelew bankowy

};


//|

/// SET STOP CHUNKS
#define SET_NUM_STOPS (sizeof(Set_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Set_Stops[] =
{
                ID_SETT, ID_CAT,
                ID_SETT, ID_VERS,

                ID_SETT, ID_POSA,
                ID_SETT, ID_SERD,
                ID_SETT, ID_SERU,
                ID_SETT, ID_PTUA,
                ID_SETT, ID_PTUB,
                ID_SETT, ID_PTUC,
                ID_SETT, ID_PTUD,
                ID_SETT, ID_PTUE,
                ID_SETT, ID_PTUZ,

                ID_SETT, ID_DRAW,
                ID_SETT, ID_IDLE,
                ID_SETT, ID_IDL1,
                ID_SETT, ID_IDL2,
                ID_SETT, ID_NADR,
                ID_SETT, ID_NAD1,
                ID_SETT, ID_NAD2,
                ID_SETT, ID_NAD3,

                ID_SETT, ID_TOWN,
                ID_SETT, ID_UVAT,
                ID_SETT, ID_EXPI,
                ID_SETT, ID_UNLI,
                ID_SETT, ID_ADD ,
                ID_SETT, ID_FIFO,

                ID_SETT, ID_GR1 ,
                ID_SETT, ID_GR2 ,
                ID_SETT, ID_GR3 ,
                ID_SETT, ID_GR4 ,
                ID_SETT, ID_GR5 ,
                ID_SETT, ID_GR6 ,
                ID_SETT, ID_CLON,

                ID_SETT, ID_FVOR,
                ID_SETT, ID_FVTP,
                ID_SETT, ID_FMIN,
                ID_SETT, ID_FSRU,
                ID_SETT, ID_FVKO,
                ID_SETT, ID_FVKP,
                ID_SETT, ID_FVPO,
                ID_SETT, ID_FVPP,

                ID_SETT, ID_RUOR,
                ID_SETT, ID_RUTP,
                ID_SETT, ID_RUKO,
                ID_SETT, ID_RUKP,
                ID_SETT, ID_RUPO,
                ID_SETT, ID_RUPP,

                ID_SETT, ID_OROR,
                ID_SETT, ID_ORTP,

                ID_SETT, ID_VAT ,
                ID_SETT, ID_UNIT,
                ID_SETT, ID_MARZ,

                ID_SETT, ID_CUST,

                ID_SETT, ID_DOT ,

                ID_SETT, ID_CALL,
                ID_SETT, ID_SALE,

                ID_SETT, ID_MAP0,
                ID_SETT, ID_MAP1,
                ID_SETT, ID_MAP2,
                ID_SETT, ID_MAP3,
                ID_SETT, ID_MAP4,
                ID_SETT, ID_MAP5,
                ID_SETT, ID_MAP6,
                ID_SETT, ID_MAP7,
                ID_SETT, ID_MAP8,
                ID_SETT, ID_UPVT,

                ID_SETT, ID_TXT1,
                ID_SETT, ID_TXT2,
                ID_SETT, ID_TXT3,
                ID_SETT, ID_TXT4,
                ID_SETT, ID_TXT5,
                ID_SETT, ID_TXT6,
                ID_SETT, ID_TXT7,
                ID_SETT, ID_TXT8,
                ID_SETT, ID_TXT9,
                ID_SETT, ID_TXT0,

                ID_SETT, ID_REST,
                ID_SETT, ID_PARD,
                ID_SETT, ID_PARR,
                ID_SETT, ID_PARP,
                ID_SETT, ID_PARI,


                NULL, NULL,
};
//|

/// WczytajUstawienia

long WczytajUstawienia(void)
{
struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
long   Errors = 0;
char   ValidFile = FALSE;

         set(app, MUIA_Application_Sleep, TRUE);

         if(iff = AllocIFF())
           {
           if(iff->iff_Stream = Open(UstawieniaFileName, MODE_OLDFILE))
                   {
                   InitIFFasDOS(iff);

                   StopChunks(iff, Set_Stops, SET_NUM_STOPS);

                   if(!OpenIFF(iff, IFFF_READ))
                           {

                           while(TRUE)
                                  {
                                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                                         break;

                                  if(cn = CurrentChunk(iff))
                                         {
                                         LONG ID = cn->cn_ID;

                                         if(!ValidFile)
                                                {
                                                if((ID == ID_CAT) && (cn->cn_Type == ID_SETT))
                                                   {
                                                   ValidFile = TRUE;
                                                   continue;
                                                   }

                                                break;
                                                }

///                    ID_VERS
                                         if(ID == ID_VERS)
                                                {
                                                struct BaseVersion version;

                                                if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                                                   {

                                                   }
                                                else
                                                   {
                                                   _RC = IoErr();
                                                   break;
                                                   }

                                                continue;
                                                }
//|

///                    Sprzedawca

                                         _read_size(ID_CUST, &settings.wystawca, sizeof(struct Klient))

//|

///                    Numeracja dokument�w 1

                                         _read(ID_FVOR, &settings.order_fvat)
                                         _read(ID_FVTP, &settings.type_fvat)
                                         _read(ID_FMIN, &settings.fvat_min)
                                         _read(ID_FSRU, &settings.sug_rach)
                                         _read(ID_FVKO, &settings.order_fvat_kor)
                                         _read(ID_FVKP, &settings.type_fvat_kor)
                                         _read(ID_FVPO, &settings.order_fvat_par)
                                         _read(ID_FVPP, &settings.type_fvat_par)

//|
///                    Numeracja dokument�w 2

                                         _read(ID_RUOR, &settings.order_rach)
                                         _read(ID_RUTP, &settings.type_rach)
                                         _read(ID_RUKO, &settings.order_rach_kor)
                                         _read(ID_RUKP, &settings.type_rach_kor)
                                         _read(ID_RUPO, &settings.order_rach_par)
                                         _read(ID_RUPP, &settings.type_rach_par)

//|
///                    Numeracja dokument�w 3

                                         _read(ID_OROR, &settings.order_order)
                                         _read(ID_ORTP, &settings.type_order)

//|
///                    Sprzeda�

                                         _read(ID_REST, &settings.reszta_aktywna)
                                         _read(ID_PARD, &settings.paragon_fvat)
                                         _read(ID_PARR, &settings.paragon_rach)
                                         _read(ID_PARP, &settings.paragon_par)
                                         _read(ID_PARI, &settings.paragon_inne)


//|
///                    Posnet

                                         _read(ID_POSA, &settings.posnet_active)
                                         _read(ID_SERD, settings.serial_device)
                                         _read(ID_SERU, &settings.serial_unit)
                                         _read(ID_PTUA, &settings.ptu_22)
                                         _read(ID_PTUB, &settings.ptu_17)
                                         _read(ID_PTUC, &settings.ptu_7)
                                         _read(ID_PTUD, &settings.ptu_0)
                                         _read(ID_PTUE, &settings.ptu_3)
                                         _read(ID_PTUZ, &settings.ptu_zw)

//|
///                    Posnet 2

                                         _read(ID_DRAW, &settings.posnet_open_drawer)
                                         _read(ID_IDLE, &settings.posnet_idle)
                                         _read(ID_IDL1, &settings.posnet_idle_text1)
                                         _read(ID_IDL2, &settings.posnet_idle_text2)

                                         _read(ID_NADR, &settings.posnet_nadruki)
                                         _read(ID_NAD1, settings.posnet_nadruk_1)
                                         _read(ID_NAD2, settings.posnet_nadruk_2)
                                         _read(ID_NAD3, settings.posnet_nadruk_3)

//|
///                    Klient

                                         _read(ID_TOWN, settings.def_miasto)

                                         _read(ID_UVAT, &settings.def_upvat)
                                         _read(ID_EXPI, &settings.def_expire_offset)
                                         _read(ID_UNLI, &settings.def_unlimited)
                                         _read(ID_ADD , &settings.def_addenabled)
                                         _read(ID_FIFO, &settings.def_fifo)

                                         _read(ID_GR1 , &settings.def_sprzedawca)
                                         _read(ID_GR2 , &settings.def_nabywca)
                                         _read(ID_GR3 , &settings.def_kosztowiec)
                                         _read(ID_GR4 , &settings.def_a)
                                         _read(ID_GR5 , &settings.def_b)
                                         _read(ID_GR6 , &settings.def_c)

                                         _read(ID_CLON, &settings.clone_filter)

//|
///                    Magazyn

                                         _read(ID_VAT,  &settings.def_vat)
                                         _read(ID_UNIT, settings.def_miara)

                                         _read(ID_MARZ, &settings.sta�a_mar�a_procentowa)

//|
///                    R��ne

                                         _read(ID_DOT,  &settings.def_dot)

//|
///                    Wydruki

                                        _read(ID_MAP0, &settings.map_fvat)
                                        _read(ID_MAP1, &settings.map_fvat_proforma)
                                        _read(ID_MAP2, &settings.map_paragon)
                                        _read(ID_MAP3, &settings.map_zamowienie)
                                        _read(ID_MAP4, &settings.map_cennik_detal)
                                        _read(ID_MAP5, &settings.map_cennik_hurt)
                                        _read(ID_MAP6, &settings.map_spis)
                                        _read(ID_MAP7, &settings.map_stany)
                                        _read(ID_MAP8, &settings.map_przelew)

                                        _read(ID_UPVT, settings.upvat_text)
//|
///                    Nadruki

                                        _read(ID_TXT1, settings.text_1)
                                        _read(ID_TXT2, settings.text_2)
                                        _read(ID_TXT3, settings.text_3)
                                        _read(ID_TXT4, settings.text_4)
                                        _read(ID_TXT5, settings.text_5)
                                        _read(ID_TXT6, settings.text_6)
                                        _read(ID_TXT7, settings.text_7)
                                        _read(ID_TXT8, settings.text_8)
                                        _read(ID_TXT9, settings.text_9)
                                        _read(ID_TXT0, settings.text_0)


//|
///                    Zdarzenia

                                        _read(ID_CALL, &settings.com_call)
                                        _read(ID_SALE, settings.com_sale)

//|

                                         }
                                  }

                           CloseIFF(iff);
                           }

                        Close(iff->iff_Stream);
                        }


           if(_RC == IFFERR_EOF) _RC = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

           FreeIFF(iff);
           }

           set(app, MUIA_Application_Sleep, FALSE);


           return(0);
}

//|
/// ZapiszUstawienia

char ZapiszUstawienia(void)
{
#define _ID_TYPE ID_SETT
//#define _write(a,b)    {PushChunk(MyIFFHandle, _ID_TYPE, (a), IFFSIZE_UNKNOWN); WriteChunkBytes(MyIFFHandle, &(b), sizeof(b)); PopChunk(MyIFFHandle); }

//#define _write(a,b)    {PushChunk(MyIFFHandle, _ID_TYPE, (a), IFFSIZE_UNKNOWN); WriteChunkBytes(MyIFFHandle, &(b), sizeof(b)); D(bug("size: %ld\n", sizeof(b))); PopChunk(MyIFFHandle); }

struct IFFHandle *MyIFFHandle;


        set(app, MUIA_Application_Sleep, TRUE);

        if(MyIFFHandle = AllocIFF())
                {
                BPTR  FileHandle;

                if(FileHandle = Open(UstawieniaFileName, MODE_NEWFILE))
                   {
                   MyIFFHandle->iff_Stream = FileHandle;
                   InitIFFasDOS(MyIFFHandle);

                   if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
                           {
                           struct BaseVersion version;

                           PushChunk(MyIFFHandle, ID_SETT, ID_CAT, IFFSIZE_UNKNOWN);

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);
                                   PushChunk(MyIFFHandle, ID_SETT, ID_VERS, IFFSIZE_UNKNOWN);
                                   version.Version = VERSION;
                                   version.Revision = REVISION;
                                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                                   PopChunk(MyIFFHandle);


                                   PushChunk(MyIFFHandle, ID_SETT, ID_ANNO, IFFSIZE_UNKNOWN);
                                   WriteChunkBytes(MyIFFHandle, ScreenTitle, strlen(ScreenTitle));
                                   WriteChunkBytes(MyIFFHandle, " <", 2);
                                   WriteChunkBytes(MyIFFHandle, EMAIL, strlen(EMAIL));
                                   WriteChunkBytes(MyIFFHandle, ">, ", 3);
                                   WriteChunkBytes(MyIFFHandle, WWW, strlen(WWW));
                                   PopChunk(MyIFFHandle);

                           PopChunk(MyIFFHandle);



                           // Sprzedawca

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);

                                 _write(ID_CUST, settings.wystawca)

                           PopChunk(MyIFFHandle);

                           // Format numeracji

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);

                                 _write(ID_FVOR, settings.order_fvat)
                                 _write(ID_FVTP, settings.type_fvat)
                                 _write(ID_FMIN, settings.fvat_min)
                                 _write(ID_FSRU, settings.sug_rach)
                                 _write(ID_FVKO, settings.order_fvat_kor)
                                 _write(ID_FVKP, settings.type_fvat_kor)
                                 _write(ID_FVPO, settings.order_fvat_par)
                                 _write(ID_FVPP, settings.type_fvat_par)

                                 _write(ID_RUOR, settings.order_rach)
                                 _write(ID_RUTP, settings.type_rach)
                                 _write(ID_RUKO, settings.order_rach_kor)
                                 _write(ID_RUKP, settings.type_rach_kor)
                                 _write(ID_RUPO, settings.order_rach_par)
                                 _write(ID_RUPP, settings.type_rach_par)

                                 _write(ID_OROR, settings.order_order)
                                 _write(ID_ORTP, settings.type_order)

                           PopChunk(MyIFFHandle);


                           // Sprzeda�

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);

                                 _write(ID_REST, settings.reszta_aktywna)
                                 _write(ID_PARD, settings.paragon_fvat)
                                 _write(ID_PARR, settings.paragon_rach)
                                 _write(ID_PARP, settings.paragon_par)
                                 _write(ID_PARI, settings.paragon_inne)

                           PopChunk(MyIFFHandle);


                           // Klient

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);

                                 _write(ID_TOWN, settings.def_miasto)
                                 _write(ID_UVAT, settings.def_upvat)
                                 _write(ID_EXPI, settings.def_expire_offset)
                                 _write(ID_UNLI, settings.def_unlimited)
                                 _write(ID_ADD , settings.def_addenabled)
                                 _write(ID_FIFO, settings.def_fifo)

                                 _write(ID_GR1 , settings.def_sprzedawca)
                                 _write(ID_GR2 , settings.def_nabywca)
                                 _write(ID_GR3 , settings.def_kosztowiec)
                                 _write(ID_GR4 , settings.def_a)
                                 _write(ID_GR5 , settings.def_b)
                                 _write(ID_GR6 , settings.def_c)

                                 _write(ID_CLON, settings.clone_filter)

                           PopChunk(MyIFFHandle);

                           // Posnet

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);

                                 _write(ID_POSA, settings.posnet_active)
                                 _write(ID_SERD, settings.serial_device)
                                 _write(ID_SERU, settings.serial_unit)
                                 _write(ID_PTUA, settings.ptu_22)
                                 _write(ID_PTUB, settings.ptu_17)
                                 _write(ID_PTUC, settings.ptu_7)
                                 _write(ID_PTUD, settings.ptu_0)
                                 _write(ID_PTUE, settings.ptu_3)
                                 _write(ID_PTUZ, settings.ptu_zw)

                           PopChunk(MyIFFHandle);

                           // Posnet 2

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);

                                 _write(ID_DRAW, settings.posnet_open_drawer)
                                 _write(ID_IDLE, settings.posnet_idle)
                                 _write(ID_IDL1, settings.posnet_idle_text1)
                                 _write(ID_IDL2, settings.posnet_idle_text2)

                                 _write(ID_NADR, settings.posnet_nadruki)
                                 _write(ID_NAD1, settings.posnet_nadruk_1)
                                 _write(ID_NAD2, settings.posnet_nadruk_2)
                                 _write(ID_NAD3, settings.posnet_nadruk_3)

                           PopChunk(MyIFFHandle);


                           // Magazyn

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);

                                 _write(ID_VAT , settings.def_vat)
                                 _write(ID_UNIT, settings.def_miara)

                                 _write( ID_MARZ, settings.sta�a_mar�a_procentowa )

                           PopChunk(MyIFFHandle);


                           // R��ne

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);

                                 _write(ID_DOT , settings.def_dot)

                           PopChunk(MyIFFHandle);


                           // Wydruki

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);

                                 _write(ID_MAP0, settings.map_fvat)
                                 _write(ID_MAP1, settings.map_fvat_proforma)
                                 _write(ID_MAP2, settings.map_paragon)
                                 _write(ID_MAP3, settings.map_zamowienie)
                                 _write(ID_MAP4, settings.map_cennik_detal)
                                 _write(ID_MAP5, settings.map_cennik_hurt)
                                 _write(ID_MAP6, settings.map_spis)
                                 _write(ID_MAP7, settings.map_stany)
                                 _write(ID_MAP8, settings.map_przelew)

                                 _write(ID_UPVT, settings.upvat_text)

                           PopChunk(MyIFFHandle);

                           // Nadruki

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);

                                 if(settings.text_1[0] != 0) _write(ID_TXT1, settings.text_1)
                                 if(settings.text_2[0] != 0) _write(ID_TXT2, settings.text_2)
                                 if(settings.text_3[0] != 0) _write(ID_TXT3, settings.text_3)
                                 if(settings.text_4[0] != 0) _write(ID_TXT4, settings.text_4)
                                 if(settings.text_5[0] != 0) _write(ID_TXT5, settings.text_5)
                                 if(settings.text_6[0] != 0) _write(ID_TXT6, settings.text_6)
                                 if(settings.text_7[0] != 0) _write(ID_TXT7, settings.text_7)
                                 if(settings.text_8[0] != 0) _write(ID_TXT8, settings.text_8)
                                 if(settings.text_9[0] != 0) _write(ID_TXT9, settings.text_9)
                                 if(settings.text_0[0] != 0) _write(ID_TXT0, settings.text_0)

                           PopChunk(MyIFFHandle);

                           // Programy zewn�trzne

                           PushChunk(MyIFFHandle, ID_SETT, ID_FORM, IFFSIZE_UNKNOWN);

                                 _write(ID_CALL, settings.com_call)
                                 _write(ID_SALE, settings.com_sale)

                           PopChunk(MyIFFHandle);



                           //------------


                           PopChunk(MyIFFHandle);
                           CloseIFF(MyIFFHandle);
                           }
                   else
                           {
                           DisplayBeep(0);
                           D(bug("*** OpenIFF() nie powiod�o si�\n"));
                           }

                   Close(FileHandle);
                   }
                else
                   {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
                   D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", UstawieniaFileName));
                   }

                FreeIFF(MyIFFHandle);
                }
         else
                {
                DisplayBeep(0);
                D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
                }

        set(app, MUIA_Application_Sleep, FALSE);


        return(0);
}



//|


/// CreateNumeracjaWindow


/// STRINGS
#define MSG_SET_NUM_WIN_TITLE       "Ustawienia numeracji dokument�w"

#define MSG_SET_NUMERACJA_PAGE_1    "Faktury"
#define MSG_SET_NUM_FVAT_TITLE      "Faktura VAT"
#define MSG_SET_NUM_FVAT_ORDER      "_Format numeru"
#define MSG_SET_NUM_FVAT_TYPE       "_Numeracja"
#define MSG_SET_NUM_FVAT            "_Kolejny numer"
#define MSG_SET_NUM_FVAT_KOR_TITLE  "Faktura VAT - korekta"
#define MSG_SET_NUM_FVAT_KOR_ORDER  "F_ormat numeru"
#define MSG_SET_NUM_FVAT_KOR_TYPE   "Nu_meracja"
#define MSG_SET_NUM_FVAT_KOR        "Ko_lejny numer"
#define MSG_SET_NUM_FVAT_PAR_TITLE  "Faktura VAT do paragonu"
#define MSG_SET_NUM_FVAT_PAR_ORDER  "Forma_t numeru"
#define MSG_SET_NUM_FVAT_PAR_TYPE   "Num_eracja"
#define MSG_SET_NUM_FVAT_PAR        "Kole_jny numer"
/*
#define MSG_SET_NUM_FVAT_MIN_TITLE  "Warto�� minimalna dla faktur VAT"
#define MSG_SET_NUM_FVAT_MIN        "_Warto�� min."
#define MSG_SET_NUM_FVAT_RACH       "Sugeruj _rachunek"
*/

/*
#define MSG_SET_NUMERACJA_PAGE_2    "Rachunki"
#define MSG_SET_NUM_RACH_TITLE      "Rachunek uproszczony"
#define MSG_SET_NUM_RACH_ORDER      "_Format numeru"
#define MSG_SET_NUM_RACH_TYPE       "_Numeracja"
#define MSG_SET_NUM_RACH            "_Kolejny numer"
#define MSG_SET_NUM_RACH_KOR_TITLE  "Rachunek upr. - korekta"
#define MSG_SET_NUM_RACH_KOR_ORDER  "F_ormat numeru"
#define MSG_SET_NUM_RACH_KOR_TYPE   "Nu_meracja"
#define MSG_SET_NUM_RACH_KOR        "Ko_lejny numer"
#define MSG_SET_NUM_RACH_PAR_TITLE  "Rachunek upr. do paragonu"
#define MSG_SET_NUM_RACH_PAR_ORDER  "Forma_t numeru"
#define MSG_SET_NUM_RACH_PAR_TYPE   "Num_eracja"
#define MSG_SET_NUM_RACH_PAR        "Kole_jny numer"
*/

#define MSG_SET_NUMERACJA_PAGE_3    "Inne"
#define MSG_SET_NUM_ORD_TITLE       "Zam�wienie"
#define MSG_SET_NUM_ORD_ORDER       "_Format numeru"
#define MSG_SET_NUM_ORD_TYPE        "_Numeracja"
#define MSG_SET_NUM_ORD             "_Kolejny numer"
#define MSG_SET_NUM_PARAGON_TITLE   "Paragon fiskalny"
#define MSG_SET_NUM_PARAGON         "Kolejn_y numer"

#define MSG_SET_NUM_OK               "F10 - Ok"
#define MSG_SET_NUM_CANCEL           "ESC - Poniechaj"

//|
/// static Object *...
static Object
           *NumeracjaWindow,             /* USTAWIENIA WINDOW */
           *LV_SetN_Strony,
           *GR_SetN_Strony,

           *CY_SetN_Num_FVat_Order,
           *CY_SetN_Num_FVat_Type,
           *ST_SetN_Num_FVat,
//       *ST_SetN_Num_FVat_Min,
//       *CH_SetN_Num_FVat_Rach,
           *CY_SetN_Num_FVat_Kor_Order,
           *ST_SetN_Num_FVat_Kor,
           *CY_SetN_Num_FVat_Kor_Type,
           *CY_SetN_Num_FVat_Par_Order,
           *ST_SetN_Num_FVat_Par,
           *CY_SetN_Num_FVat_Par_Type,
/*
           *CY_SetN_Num_Rach_Order,
           *CY_SetN_Num_Rach_Type,
           *ST_SetN_Num_Rach,
           *CY_SetN_Num_Rach_Kor_Order,
           *ST_SetN_Num_Rach_Kor,
           *CY_SetN_Num_Rach_Kor_Type,
           *CY_SetN_Num_Rach_Par_Order,
           *ST_SetN_Num_Rach_Par,
           *CY_SetN_Num_Rach_Par_Type,
*/

           *CY_SetN_Num_Order_Order,
           *ST_SetN_Num_Order,
           *CY_SetN_Num_Order_Type,
           *ST_SetN_Num_Paragon,

           *BT_SetN_Num_Ok,
           *BT_SetN_Num_Cancel;


char *NumeracjaDokument�wTable[] = {"NR/RRRR",
                                                                        "NR/RR",
                                                                        "NR/MM/RRRR",
                                                                        "NR/MM/RR",
                                                                        "RRRR/NR",
                                                                        "RR/NR",
                                                                        "RRRR/MM/NR",
                                                                        "RR/MM/NR",
                                                                        NULL};

char *RodzajNumeracjiTable[] = {"Miesi�czna",
                                                                "Roczna",
                                                                NULL};
//|


char CreateNumeracjaWindow(void)
{
char result = FALSE;


///   Create NumeracjaWindow

//                 MUIA_Application_Window,
                                         NumeracjaWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_SET_NUM_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_USTAWIENIA_NUM,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,
                                                         Child, HGroup,
                                                                        Child, LV_SetN_Strony = _ListviewObject,
                                                                                   MUIA_Weight, 25,
                                                                                         MUIA_CycleChain, TRUE,
                                                                                         _MUIA_Listview_List, _ListObject,
                                                                                                   InputListFrame,
                                                                                                   _MUIA_List_AutoVisible, TRUE,
                                                                                                   _MUIA_List_AdjustWidth, TRUE,
                                                                                                   _MUIA_List_Format, "MIW=1 MAW=-1",
                                                                                                   End,
                                                                                         End,

                                                                        Child, BalanceObject, End,

                                                                        Child, GR_SetN_Strony = GroupObject,
                                                                                   MUIA_Weight, 75,
                                                                                   MUIA_Group_PageMode, TRUE,

///                                          Numeracja dokument�w 1

                                                                                   Child, VGroup,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_NUM_FVAT_TITLE),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_FVAT_ORDER),
                                                                                                                 Child, CY_SetN_Num_FVat_Order = MakeCycle(NumeracjaDokument�wTable, MSG_SET_NUM_FVAT_ORDER),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_FVAT),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, ST_SetN_Num_FVat = MakeNumericString(5, MSG_SET_NUM_FVAT),

                                                                                                                                Child, MakeLabel2(MSG_SET_NUM_FVAT_TYPE),
                                                                                                                                Child, CY_SetN_Num_FVat_Type = MakeCycle(RodzajNumeracjiTable, MSG_SET_NUM_FVAT_TYPE),
                                                                                                                                End,
                                                                                                                 End,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_NUM_FVAT_KOR_TITLE),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_FVAT_KOR_ORDER),
                                                                                                                 Child, CY_SetN_Num_FVat_Kor_Order = MakeCycle(NumeracjaDokument�wTable, MSG_SET_NUM_FVAT_KOR_ORDER),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_FVAT_KOR),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, ST_SetN_Num_FVat_Kor = MakeNumericString(5, MSG_SET_NUM_FVAT_KOR),

                                                                                                                                Child, MakeLabel2(MSG_SET_NUM_FVAT_KOR_TYPE),
                                                                                                                                Child, CY_SetN_Num_FVat_Kor_Type = MakeCycle(RodzajNumeracjiTable, MSG_SET_NUM_FVAT_KOR_TYPE),
                                                                                                                                End,
                                                                                                                 End,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_NUM_FVAT_PAR_TITLE),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_FVAT_PAR_ORDER),
                                                                                                                 Child, CY_SetN_Num_FVat_Par_Order = MakeCycle(NumeracjaDokument�wTable, MSG_SET_NUM_FVAT_PAR_ORDER),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_FVAT_PAR),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, ST_SetN_Num_FVat_Par = MakeNumericString(5, MSG_SET_NUM_FVAT_PAR),

                                                                                                                                Child, MakeLabel2(MSG_SET_NUM_FVAT_PAR_TYPE),
                                                                                                                                Child, CY_SetN_Num_FVat_Par_Type = MakeCycle(RodzajNumeracjiTable, MSG_SET_NUM_FVAT_PAR_TYPE),
                                                                                                                                End,
                                                                                                                 End,
/*
                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_NUM_FVAT_MIN_TITLE),
                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_FVAT_MIN),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, ST_SetN_Num_FVat_Min = MakeCashString(10, MSG_SET_NUM_FVAT_MIN),
                                                                                                                                Child, MakeLabel2(MSG_SET_NUM_FVAT_RACH),
                                                                                                                                Child, CH_SetN_Num_FVat_Rach = MakeCheck(MSG_SET_NUM_FVAT_RACH),
//                                                                Child, HVSpace,
                                                                                                                                End,
                                                                                                                 End,
*/

#ifdef HVSPACE
                                                                                                  Child, HVSpace,
#endif
                                                                                                  End,
//|
/*
///                                          Numeracja dokument�w 2

                                                                                   Child, VGroup,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_NUM_RACH_TITLE),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_RACH_ORDER),
                                                                                                                 Child, CY_SetN_Num_Rach_Order = MakeCycle(NumeracjaDokument�wTable, MSG_SET_NUM_RACH_ORDER),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_RACH),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, ST_SetN_Num_Rach = MakeNumericString(5, MSG_SET_NUM_RACH),

                                                                                                                                Child, MakeLabel2(MSG_SET_NUM_RACH_TYPE),
                                                                                                                                Child, CY_SetN_Num_Rach_Type = MakeCycle(RodzajNumeracjiTable, MSG_SET_NUM_RACH_TYPE),
                                                                                                                                End,
                                                                                                                 End,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_NUM_RACH_KOR_TITLE),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_RACH_KOR_ORDER),
                                                                                                                 Child, CY_SetN_Num_Rach_Kor_Order = MakeCycle(NumeracjaDokument�wTable, MSG_SET_NUM_RACH_KOR_ORDER),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_RACH_KOR),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, ST_SetN_Num_Rach_Kor = MakeNumericString(5, MSG_SET_NUM_RACH_KOR),

                                                                                                                                Child, MakeLabel2(MSG_SET_NUM_RACH_KOR_TYPE),
                                                                                                                                Child, CY_SetN_Num_Rach_Kor_Type = MakeCycle(RodzajNumeracjiTable, MSG_SET_NUM_RACH_KOR_TYPE),
                                                                                                                                End,
                                                                                                                 End,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_NUM_RACH_PAR_TITLE),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_RACH_PAR_ORDER),
                                                                                                                 Child, CY_SetN_Num_Rach_Par_Order = MakeCycle(NumeracjaDokument�wTable, MSG_SET_NUM_RACH_PAR_ORDER),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_RACH_PAR),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, ST_SetN_Num_Rach_Par = MakeNumericString(5, MSG_SET_NUM_RACH_PAR),

                                                                                                                                Child, MakeLabel2(MSG_SET_NUM_RACH_PAR_TYPE),
                                                                                                                                Child, CY_SetN_Num_Rach_Par_Type = MakeCycle(RodzajNumeracjiTable, MSG_SET_NUM_RACH_PAR_TYPE),
                                                                                                                                End,
                                                                                                                 End,


#ifdef HVSPACE
                                                                                                  Child, HVSpace,
#endif
                                                                                                  End,
//|
*/
///                                          Numeracja dokument�w 3

                                                                                   Child, VGroup,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_NUM_ORD_TITLE),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_ORD_ORDER),
                                                                                                                 Child, CY_SetN_Num_Order_Order = MakeCycle(NumeracjaDokument�wTable, MSG_SET_NUM_ORD_ORDER),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_ORD),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, ST_SetN_Num_Order = MakeNumericString(5, MSG_SET_NUM_ORD),

                                                                                                                                Child, MakeLabel2(MSG_SET_NUM_ORD_TYPE),
                                                                                                                                Child, CY_SetN_Num_Order_Type = MakeCycle(RodzajNumeracjiTable, MSG_SET_NUM_ORD_TYPE),
                                                                                                                                End,
                                                                                                                 End,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_NUM_PARAGON_TITLE),

                                                                                                                 Child, MakeLabel2(MSG_SET_NUM_PARAGON),
                                                                                                                 Child, ST_SetN_Num_Paragon = MakeNumericString(5, MSG_SET_NUM_PARAGON),

                                                                                                                 End,
#ifdef HVSPACE
                                                                                                  Child, HVSpace,
#endif
                                                                                                  End,
//|

                                                                                   End,

                                                                        End,

                                                         Child, HGroup,
                                                                        Child, BT_SetN_Num_Ok     = TextButton(MSG_SET_NUM_OK),
                                                                        Child, BT_SetN_Num_Cancel = TextButton(MSG_SET_NUM_CANCEL),
                                                                        End,

                                                         End,
                                                End;
//|


        if(NumeracjaWindow)
           {
           /* USTAWIENIA WINDOW */

           DoMethod(NumeracjaWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

           {
           static char *str[] =
                 {
                 MSG_SET_NUMERACJA_PAGE_1,
//         MSG_SET_NUMERACJA_PAGE_2,
                 MSG_SET_NUMERACJA_PAGE_3,
                 NULL
                 };

           DoMethod(LV_SetN_Strony, MUIM_List_Insert, str, -1, MUIV_List_Insert_Bottom);
           }

           DoMethod(LV_SetN_Strony, MUIM_Notify, _MUIA_List_Active, MUIV_EveryTime, GR_SetN_Strony, 3, MUIM_Set, MUIA_Group_ActivePage, MUIV_TriggerValue);
           set(    LV_SetN_Strony, _MUIA_List_Active, 0);

           DoMethod(BT_SetN_Num_Ok     ,   MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
           DoMethod(BT_SetN_Num_Cancel ,   MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

           DoMethod(NumeracjaWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);


           _attachwin(NumeracjaWindow);


           result = TRUE;
           }

        return(result);

}

//|
/// EdycjaNumeracjiSetup



void EdycjaNumeracjiSetup(void)
{
char Tmp[10];

        // Numeracja 1

        setcycle(CY_SetN_Num_FVat_Order, settings.order_fvat);
        setnumstring(ST_SetN_Num_FVat, docs.numer_fvat);
        setcycle(CY_SetN_Num_FVat_Type, settings.type_fvat);
//    setstring(ST_SetN_Num_FVat_Min,  Price2String(settings.fvat_min));
//    setcheckmark(CH_SetN_Num_FVat_Rach, settings.sug_rach);

        setcycle(CY_SetN_Num_FVat_Kor_Order, settings.order_fvat_kor);
        setnumstring(ST_SetN_Num_FVat_Kor, docs.numer_fvat_kor);
        setcycle(CY_SetN_Num_FVat_Kor_Type, settings.type_fvat_kor);
        setcycle(CY_SetN_Num_FVat_Par_Order, settings.order_fvat_par);
        setnumstring(ST_SetN_Num_FVat_Par, docs.numer_fvat_par);
        setcycle(CY_SetN_Num_FVat_Par_Type, settings.type_fvat_par);

/*
        // Numeracja 2

        setcycle(CY_SetN_Num_Rach_Order, settings.order_rach);
        setnumstring(ST_SetN_Num_Rach, docs.numer_rach);
        setcycle(CY_SetN_Num_Rach_Type, settings.type_rach);

        setcycle(CY_SetN_Num_Rach_Kor_Order, settings.order_rach_kor);
        setnumstring(ST_SetN_Num_Rach_Kor, docs.numer_rach_kor);
        setcycle(CY_SetN_Num_Rach_Kor_Type, settings.type_rach_kor);
        setcycle(CY_SetN_Num_Rach_Par_Order, settings.order_rach_par);
        setnumstring(ST_SetN_Num_Rach_Par, docs.numer_rach_par);
        setcycle(CY_SetN_Num_Rach_Par_Type, settings.type_rach_par);
*/

        // Numeracja 3

        setcycle(CY_SetN_Num_Order_Order, settings.order_order);
        setnumstring(ST_SetN_Num_Order, docs.numer_order);
        setcycle(CY_SetN_Num_Order_Type, settings.type_order);
        setnumstring(ST_SetN_Num_Paragon, docs.numer_paragon);

}
//|
/// EdycjaNumeracjiFinish
void EdycjaNumeracjiFinish(void)
{


        // Numeracja 1

        settings.order_fvat = getcycle(CY_SetN_Num_FVat_Order);
        docs.numer_fvat = getnumstr(ST_SetN_Num_FVat);
        settings.type_fvat = getcycle(CY_SetN_Num_FVat_Type);
//    settings.fvat_min = String2Price((char *)xget(ST_SetN_Num_FVat_Min, MUIA_String_Contents));
//    settings.sug_rach = getcheckmark(CH_SetN_Num_FVat_Rach);

        settings.order_fvat_kor = getcycle(CY_SetN_Num_FVat_Kor_Order);
        docs.numer_fvat_kor = getnumstr(ST_SetN_Num_FVat_Kor);
        settings.type_fvat_kor = getcycle(CY_SetN_Num_FVat_Kor_Type);
        settings.order_fvat_par = getcycle(CY_SetN_Num_FVat_Par_Order);
        docs.numer_fvat_par = getnumstr(ST_SetN_Num_FVat_Par);
        settings.type_fvat_par = getcycle(CY_SetN_Num_FVat_Par_Type);

/*
        // Numeracja 2

        settings.order_rach = getcycle(CY_SetN_Num_Rach_Order);
        docs.numer_rach = getnumstr(ST_SetN_Num_Rach);
        settings.type_rach = getcycle(CY_SetN_Num_Rach_Type);

        settings.order_rach_kor = getcycle(CY_SetN_Num_Rach_Kor_Order);
        docs.numer_rach_kor = getnumstr(ST_SetN_Num_Rach_Kor);
        settings.type_rach_kor = getcycle(CY_SetN_Num_Rach_Kor_Type);
        settings.order_rach_par = getcycle(CY_SetN_Num_Rach_Par_Order);
        docs.numer_rach_par = getnumstr(ST_SetN_Num_Rach_Par);
        settings.type_rach_par = getcycle(CY_SetN_Num_Rach_Par_Type);

*/

        // Numeracja 3

        settings.order_order = getcycle(CY_SetN_Num_Order_Order);
        docs.numer_order = getnumstr(ST_SetN_Num_Order);
        settings.type_order = getcycle(CY_SetN_Num_Order_Type);

        docs.numer_paragon = getnumstr(ST_SetN_Num_Paragon);

}
//|
/// EdycjaNumeracji
char EdycjaNumeracji(void)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

        set(app, MUIA_Application_Sleep, TRUE);


        if(!CreateNumeracjaWindow())
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           set(app, MUIA_Application_Sleep, FALSE);
           return(result);
           }


        set(NumeracjaWindow, MUIA_Window_ActiveObject , LV_SetN_Strony);
        set(NumeracjaWindow, MUIA_Window_DefaultObject, LV_SetN_Strony);

        EdycjaNumeracjiSetup();

        if(WinOpen(NumeracjaWindow))
                {
                while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);
                  switch(ID)
                        {

                        case ID_OK:
                           {
                           set(app, MUIA_Application_Sleep, TRUE);
                           EdycjaNumeracjiFinish();


                           if(ID == ID_OK)
                                 {
                                 ZapiszNumeracj�Dokument�w();
                                 ZapiszUstawienia();
                                 }

                           posnet_DisplayIdle();

                           set(app, MUIA_Application_Sleep, FALSE);
                           result  = TRUE;
                           running = FALSE;
                           }
                           break;

                        case ID_CANCEL:
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

                }
         else
                {
                DisplayBeep(NULL);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


        _detachwin(NumeracjaWindow);
        set(app, MUIA_Application_Sleep, FALSE);

        return(result);

}
//|

/// USTAWIENIA - STRINGS

#define MSG_SET_ERR_CODE "Nieprawid�owy kod kontrolny: %s"
#define MSG_SET_ERR_OK   "*_Ok"


#define MSG_SET_WIN_TITLE       "Ustawienia"

#define MSG_SET_OWN_PAGE        "Sprzedawca"
#define MSG_SET_OWN_TITLE       "Dane sprzedawcy"

#define  SH_SET_OWN        "Dane dotycz�ce sprzedawcy"
#define MSG_SET_OWN_NAME   "N_azwa"
#define MSG_SET_OWN_SC     "S_p��ka"
#define  SH_SET_OWN_SC     "Wskazuje czy sprzedawca jest sp��k� cywiln� b�d� z o.o."
#define MSG_SET_OWN_ULICA  "A_dres"
#define MSG_SET_OWN_KOD    "_Kod"
#define MSG_SET_OWN_MIASTO "_Miasto"
#define MSG_SET_OWN_TEL    "_Telefon"
#define MSG_SET_OWN_FAX    "_Fax"
#define MSG_SET_OWN_EMAIL  "_E-mail"

#define MSG_SET_OWN_NIP    "N_IP"
#define MSG_SET_OWN_REGON  "_Regon"
#define MSG_SET_OWN_BANK   "_Bank"
#define MSG_SET_OWN_KONTO  "_Nr. konta"

#define MSG_SET_SPR_PAGE            "Sprzeda�"
#define MSG_SET_SPR_TITLE           "Ustawienia dot. sprzeda�y"
#define MSG_SET_SPR_RESZTA          "Okno _wp�aty/reszty"
#define  SH_SET_SPR_RESZTA          "Je�li ta opcja jest aktywna, przy\nsprzeda�y z typem p�atno�ci \"Got�wka\"\npojawia� si� b�dzie okno do wprowadzenia\nwp�aconej kwoty oraz obliczania reszty."
#define MSG_SET_SPR_PARAGON_TITLE   "Domy�lny stan ga�etu \"Paragon\""
#define  SH_SET_SPR_PARAGON         "Domy�lny stan gad�etu \"Paragon\" dla okna\nwydruku dokumentu, oddzielnie dla ka�dego\nz typ�w dokument�w sprzeda�y.\n\nOpcja ta pozwala na uproszczenie pracy,\nprzez ustawienie domy�lnego stanu gad�etu\ndrukowania paragonu fiskalnego."
#define MSG_SET_SPR_PARAGON_FVAT    "Dla faktur _VAT"
#define MSG_SET_SPR_PARAGON_PAR     "Dla _rachunk�w uproszczonych"
#define MSG_SET_SPR_PARAGON_RACH    "Dla _paragon�w"
#define MSG_SET_SPR_PARAGON_INNE    "Dla pozosta�ych _dokument�w"

#define MSG_SET_CUST_PAGE         "Klient"
#define MSG_SET_CUST_TITLE        "Domy�lne dane kontrahenta"
#define MSG_SET_CUST_TOWN         "_Miasto"
#define MSG_SET_CUST_UPVAT_EXPIRE "_Wa�ne przez (dni)"
#define MSG_SET_CUST_TITLE_2      "Domy�lny typ kontrahenta"
#define MSG_SET_CUST_UPVAT        "_Upowa�nienie VAT"
#define MSG_SET_CUST_ADDENABLED   "Do_pisywanie"
#define MSG_SET_CUST_FIFO         "_Kolejka FIFO"
#define MSG_SET_CUST_ODB_1        "Odbiorca _1"
#define MSG_SET_CUST_ODB_2        "Odbiorca _2"
#define MSG_SET_CUST_ODB_3        "Odbiorca _3"
#define MSG_SET_CUST_ODB_4        "Odbiorca _4"
#define MSG_SET_CUST_GR_TITLE     "Domy�lny typ kontrahenta"
#define MSG_SET_CUST_GR_1         "_Sprzedawca"
#define MSG_SET_CUST_GR_2         "O_dbiorca"
#define MSG_SET_CUST_GR_3         "_Kosztowiec"
#define MSG_SET_CUST_GR_4         "_A"
#define MSG_SET_CUST_GR_5         "_B"
#define MSG_SET_CUST_GR_6         "_C"

#define MSG_SET_CUST_TITLE_3      "Przy dopisywaniu klienta w selectorze"
#define MSG_SET_CUST_CLONE        "Typ klienta zgodny z ustawieniami _filtra"

#define MSG_SET_PROD_PAGE        "Magazyn"
#define MSG_SET_MAG_TITLE        "Ustawienia magazynowe"
#define MSG_SET_PROD_TITLE       "Ust. domy�lne grupy magazynowej"
#define MSG_SET_PROD_JEDN        "_Jedn. miary"
#define MSG_SET_PROD_VAT         "Stawka _VAT"
#define MSG_SET_MAG_MARZA_TITLE  "Edycja produktu"
#define MSG_SET_MAG_MARZA        "Sta�a _mar�a procentowa"


#define MSG_SET_MISC_PAGE        "R��ne"
#define MSG_SET_MISC_TITLE       "Ustawienia r��ne"
#define MSG_SET_MISC_WARN        "\033c\0338Zmiany wprowadzone na tej stronie\nb�d� aktywne dopiero przy nast�pnym\nuruchomieniu systemu Golem"
#define MSG_SET_MISC_DOT         "_Kropka dziesi�tna"

#define MSG_SET_POS_PAGE_1       "Posnet"
#define MSG_SET_POS_ACTIVE       "_Drukarka fiskalna aktywna"
#define  SH_SET_POS_ACTIVE       "Je�li posiadasz drukark� fiskaln� zgodn� ze standardem\nPOSNET (w zasadzie ka�da obecnie dost�pna drukarka fiskalna\njest zgodna z POSNETem), i chcesz aby Golem z niech korzysta�\ndo wystawiania paragon�w fiskalnych, uaktywnij t� opcj�.\n\nUwaga: Modu� POSNETowy jest w pe�ni funkcjonalny tak�e w\nwersji demonstracyjnej programu, zatem je�li Twoja drukarka\njest ju� zafiskalizowana, radzimy nie uaktywnia� tej opcji,\nze wzgl�du na nieodwracalno�� transakcji fiskalnych!"
#define MSG_SET_POS_TITLE_1  "Drukarka fiskalna"
#define MSG_SET_POS_TITLE_1A "Ustawienia drukarki fiskalnej"
#define MSG_SET_POS_DEVICE   "S_terownik"
#define  SH_SET_POS_DEVICE   "Naswa sterownika portu do kt�rego przy��czona jest Twoja drukarka\nfiskalna. Drukarki z regu�y posiadaj� interfejs szeregowy, wi�c do\ntakiego typu portu powinny by� przy��czone. Standardowy port\nszeregowy obs�ugiwany jest przez \"serial.device\""
#define MSG_SET_POS_UNIT     "_Jednostka"
#define  SH_SET_POS_UNIT     "Numer logicznej jednostki portu obs�uguj�cego\ndrukark� (0-255). Z regu�y jest to pierwsza jednostka (numer \"0\")"
#define MSG_SET_POS_TITLE_1B "Oznaczenia podatku VAT"
#define  SH_SET_POS_TITLE_1B "Przyporz�dkowanie oznacze� literowych\nposzczeg�lnym stawkom podatku VAT.\n\nW zasadzie nie ma �adnej potrzeby by\nustawienia fabryczne modyfikowa�, gdy�\nodzwierciedlaj� one standardowe mapowanie\nstawek, stosowane we wszystkich drukarkach"
#define MSG_SET_POS_A    "PTU _22%"
#define MSG_SET_POS_B    "PTU _17%"
#define MSG_SET_POS_C    "PTU _7%"
#define MSG_SET_POS_E    "PTU _3%"
#define MSG_SET_POS_D    "PTU _0"
#define MSG_SET_POS_Z    "PTU _Zw."

#define MSG_SET_POS_PAGE_2       "Posnet 2"
#define MSG_SET_POS_TITLE_2      "Wystawianie paragonu"
#define MSG_SET_POS_DRAWER       "Otwieraj szuflad� _kasow�"
#define  SH_SET_POS_DRAWER       "Je�li opcja ta jest aktywna i je�li do drukarki\npod��czony jest modu� szuflady kasowej, zostanienona otwarta na zako�czenie transakcji, by umo�liwi�\noperacje kasowe."
#define MSG_SET_POS_TITLE_2A     "Wykorzystanie wy�wietlacza"
#define  SH_SET_POS_TITLE_2A     "Definiuje spos�b wykorzystania wy�wietlacza\ndrukarki fiskalnej (o ile dany model go posiada)\nw stanie bezczynno�ci drukarki (idle mode)."
#define MSG_SET_POS_IDLE         "Stan _nieaktywny"

#define MSG_SET_POS_IDLE_TXT1    "_Linia 1"
#define MSG_SET_POS_IDLE_TXT2    "L_inia 2"
#define MSG_SET_POS_TITLE_2B     "Nadruki dodatkowe"
#define  SH_SET_POS_TITLE_2B     "Wiekszo�� modeli drukarek umo�liwia dodanie\nw�asnego tekstu na ko�cu drukowanego paragonu.\nJe�li ta opcja jest w��czona, poni�ej zdefiniowany\ntekst zostanie wydrukowany w stopce paragonu."
#define MSG_SET_POS_NADRUKI      "Nadruki _w��czone"
#define MSG_SET_POS_TXT1         "Linia _1"
#define MSG_SET_POS_TXT2         "_2"
#define MSG_SET_POS_TXT3         "_3"


#define MSG_SET_DOC_PAGE_1       "Wydruk"
#define MSG_SET_DOC_TITLE_1      "Teksty drukowane na dokumentach"
#define MSG_SET_DOC_UPVAT        "Upowa�nienie _VAT"

#define MSG_SET_MAP_TITLE        "Przypisanie drukarek"
#define MSG_SET_MAP_FVAT         "Faktura _VAT"
#define MSG_SET_MAP_FVAT_PRO     "Faktura VAT P_RO FORMA"
#define MSG_SET_MAP_PARAGON      "_Paragon"
#define MSG_SET_MAP_ZAMOWIENIE   "_Zam�wienie"
#define MSG_SET_MAP_CENNIK_DETAL "Cennik _detaliczny"
#define MSG_SET_MAP_CENNIK_HURT  "Cennik _hurtowy"
#define MSG_SET_MAP_SPIS         "_Spis z natury"
#define MSG_SET_MAP_PRZELEW      "Przelew _bankowy"

#define MSG_SET_DOC2_PAGE        "Nadruki"
#define MSG_SET_DOC2_TITLE       "Nadruki na dokumentach"
#define MSG_SET_DOC2_TXT1         "_1"
#define MSG_SET_DOC2_TXT2         "_2"
#define MSG_SET_DOC2_TXT3         "_3"
#define MSG_SET_DOC2_TXT4         "_4"
#define MSG_SET_DOC2_TXT5         "_5"
#define MSG_SET_DOC2_TXT6         "_6"
#define MSG_SET_DOC2_TXT7         "_7"
#define MSG_SET_DOC2_TXT8         "_8"
#define MSG_SET_DOC2_TXT9         "_9"
#define MSG_SET_DOC2_TXT0         "1_0"

#define MSG_SET_COM_PAGE         "Zdarzenia"
#define MSG_SET_COM_TITLE        "Zdarzenia"
#define MSG_SET_COM_CALL         "_Aktywne"
#define MSG_SET_COM_SALE         "S_przeda�"

#define MSG_SET_OK               "F10 - Ok"
#define MSG_SET_CANCEL           "ESC - Poniechaj"

//|
/// CreatePrefsWindow


char *PosnetIdleTable[] = {"Nie r�b nic",
                                                   "Prze��cz na zegarek",
                                                   "Prze��cz na tryb kasowy",
                                                   "Tekst u�ytkownika",
                                                   NULL};

/// static Object *...
static Object
           *UstawieniaWindow,             /* USTAWIENIA WINDOW */
           *LV_Set_Strony,
           *GR_Set_Strony,


           *ST_Set_Own_Nazwa_1,
           *ST_Set_Own_Nazwa_2,
           *CH_Set_Own_SC,
           *ST_Set_Own_Ulica,
           *ST_Set_Own_Kod,
           *ST_Set_Own_Miasto,
           *ST_Set_Own_NIP,
           *ST_Set_Own_Regon,
           *ST_Set_Own_Konto,
           *BS_Set_Own_Bank,
           *ST_Set_Own_Tel,
           *ST_Set_Own_Fax,
           *ST_Set_Own_Email,

           *CH_Set_Spr_Reszta,
           *CH_Set_Spr_Paragon_FVat,
           *CH_Set_Spr_Paragon_Rach,
           *CH_Set_Spr_Paragon_Par,
           *CH_Set_Spr_Paragon_Inne,

           *CH_Set_Pos_Active,
           *ST_Set_Pos_Device,
           *ST_Set_Pos_Unit,
           *CY_Set_Pos_A,
           *CY_Set_Pos_B,
           *CY_Set_Pos_C,
           *CY_Set_Pos_D,
           *CY_Set_Pos_E,
           *CY_Set_Pos_Z,

           *CH_Set_Pos_Drawer,
           *CY_Set_Pos_Idle,
           *ST_Set_Pos_Idle_Txt1,
           *ST_Set_Pos_Idle_Txt2,
           *CH_Set_Pos_Nadruki,
           *ST_Set_Pos_Txt1,
           *ST_Set_Pos_Txt2,
           *ST_Set_Pos_Txt3,

           *ST_Set_Cust_Town,
           *CH_Set_Cust_UpVat,
           *ST_Set_Cust_UpVat_Expire,
           *CH_Set_Cust_UpVat_Unlimited,
           *CH_Set_Cust_AddEnabled,
           *CH_Set_Cust_FIFO,

           *CH_Set_Cust_Gr_1,
           *CH_Set_Cust_Gr_2,
           *CH_Set_Cust_Gr_3,
           *CH_Set_Cust_Gr_4,
           *CH_Set_Cust_Gr_5,
           *CH_Set_Cust_Gr_6,

           *CH_Set_Cust_Clone,

           *ST_Set_Prod_Jedn,
           *CY_Set_Prod_Vat,
           *CH_Set_Mag_Mar�a,


           *TX_Set_Map_FVat,
           *BT_Set_Map_FVat,
           *TX_Set_Map_FVatPro,
           *BT_Set_Map_FVatPro,
           *TX_Set_Map_Paragon,
           *BT_Set_Map_Paragon,
           *TX_Set_Map_Zamowienie,
           *BT_Set_Map_Zamowienie,
           *TX_Set_Map_CennikDetal,
           *BT_Set_Map_CennikDetal,
           *TX_Set_Map_CennikHurt,
           *BT_Set_Map_CennikHurt,
           *TX_Set_Map_SpisZNatury,
           *BT_Set_Map_SpisZNatury,
           *TX_Set_Map_StanyMagazynowe,
           *BT_Set_Map_StanyMagazynowe,
           *TX_Set_Map_Przelew,
           *BT_Set_Map_Przelew,

           *ST_Set_Doc_UpVat,

           *ST_Set_Misc_Dot,

           *ST_Set_Doc2_TXT1,
           *ST_Set_Doc2_TXT2,
           *ST_Set_Doc2_TXT3,
           *ST_Set_Doc2_TXT4,
           *ST_Set_Doc2_TXT5,
           *ST_Set_Doc2_TXT6,
           *ST_Set_Doc2_TXT7,
           *ST_Set_Doc2_TXT8,
           *ST_Set_Doc2_TXT9,
           *ST_Set_Doc2_TXT0,

           *CH_Set_Com_Call,
           *ST_Set_Com_Sale,


           *BT_Set_Ok,
           *BT_Set_Cancel;


static char *strony_nazwy[] =
                 {
                 MSG_SET_OWN_PAGE,
                 MSG_SET_SPR_PAGE,

                 MSG_SET_POS_PAGE_1,
                 MSG_SET_POS_PAGE_2,
                 MSG_SET_CUST_PAGE,
                 MSG_SET_PROD_PAGE,
                 MSG_SET_MISC_PAGE,

                 MSG_SET_DOC_PAGE_1,

                 MSG_SET_DOC2_PAGE,
                 MSG_SET_COM_PAGE,
                 NULL
                 };
//|


char CreatePrefsWindow(void)
{
char result = FALSE;

#define PPH(a) PopphObject, MUIA_Popph_Array, PrinterCodesArray, MUIA_Popph_SingleColumn, TRUE, End


///   Create UstawieniaWindow

//                 MUIA_Application_Window,
                                         UstawieniaWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_SET_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_USTAWIENIA,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,
                                                         Child, HGroup,
                                                                        Child, LV_Set_Strony = _ListviewObject,
                                                                                   MUIA_Weight, 25,
                                                                                         MUIA_CycleChain, TRUE,
                                                                                         _MUIA_Listview_List, _ListObject,
                                                                                                   InputListFrame,
                                                                                                   _MUIA_List_AutoVisible, TRUE,
                                                                                                   _MUIA_List_AdjustWidth, TRUE,
                                                                                                   _MUIA_List_Format, "MIW=1 MAW=-1",
                                                                                                   End,
                                                                                         End,

                                                                        Child, BalanceObject, End,

                                                                        Child, GR_Set_Strony = GroupObject,
                                                                                   MUIA_Weight, 75,
                                                                                   MUIA_Group_PageMode, TRUE,

///                                          Sprzedawca

                                                                                   Child, VGroup,

                                                                                           Child, HGroup,
                                                                                                          GroupFrameT(MSG_SET_OWN_TITLE),
                                                                                                          MUIA_Group_Columns, 2,
                                                                                                          _SH( SH_SET_OWN ),

                                                                                                          Child, MakeLabel2(MSG_SET_OWN_NAME),
                                                                                                          Child, ST_Set_Own_Nazwa_1 = MakeString(CUST_NAME_LEN, MSG_SET_OWN_NAME),
//    Child, ST_Set_Own_Nazwa_1 = NewObject(CL_BString->mcc_Class, NULL, TAG_DONE),

                                                                                                          Child, EmptyLabel(),
                                                                                                          Child, ST_Set_Own_Nazwa_2 = MakeString(CUST_NAME_LEN, NULL),

                                                                                                          Child, MakeLabel2(MSG_SET_OWN_SC),
                                                                                                                         Child, HGroup,
                                                                                                                                         Child, CH_Set_Own_SC = _MakeCheck( SET_OWN_SC ),
                                                                                                                                         Child, HVSpace,
                                                                                                                                         End,

                                                                                                          Child, MakeLabel2(MSG_SET_OWN_ULICA),
                                                                                                          Child, ST_Set_Own_Ulica = MakeString(CUST_ADRES_LEN, MSG_SET_OWN_ULICA),

                                                                                                          Child, MakeLabel2(MSG_SET_OWN_KOD),
                                                                                                          Child, HGroup,
                                                                                                                         Child, ST_Set_Own_Kod = MakeStringAccept(CUST_KOD_LEN, MSG_SET_OWN_KOD, "0123456789-"),

                                                                                                                         Child, MakeLabel2(MSG_SET_OWN_MIASTO),
                                                                                                                         Child, ST_Set_Own_Miasto = MakeString(CUST_MIASTO_LEN, MSG_SET_OWN_MIASTO),
                                                                                                                         End,

                                                                                                          Child, MakeLabel2(MSG_SET_OWN_TEL),
                                                                                                          Child, HGroup,
                                                                                                                         Child, ST_Set_Own_Tel = MakeString(CUST_TEL_LEN, MSG_SET_OWN_TEL),

                                                                                                                         Child, MakeLabel2(MSG_SET_OWN_FAX),
                                                                                                                         Child, ST_Set_Own_Fax = MakeString(CUST_FAX_LEN, MSG_SET_OWN_FAX),
                                                                                                                         End,
                                                                                                          Child, MakeLabel2(MSG_SET_OWN_EMAIL),
                                                                                                          Child, ST_Set_Own_Email = MakeString(CUST_EMAIL_LEN, MSG_SET_OWN_EMAIL),

                                                                                                          Child, MakeLabel2(MSG_SET_OWN_NIP),
                                                                                                          Child, HGroup,
                                                                                                                         Child, ST_Set_Own_NIP = MakeStringAccept(CUST_NIP_LEN + 3, MSG_SET_OWN_NIP, "0123456789-"),

                                                                                                                         Child, MakeLabel2(MSG_SET_OWN_REGON),
                                                                                                                         Child, ST_Set_Own_Regon = MakeNumericString(CUST_REGON_LEN, MSG_SET_OWN_REGON),
                                                                                                                         End,


                                                                                                          Child, MakeLabel2(MSG_SET_OWN_KONTO),
                                                                                                          Child, ST_Set_Own_Konto = MakeStringAccept(CUST_KONTO_LEN, MSG_SET_OWN_KONTO, "0123456789-"),
                                                                                                          Child, MakeLabel2(MSG_SET_OWN_BANK),
                                                                                                          Child, BS_Set_Own_Bank = Golem_BankStringObject, End,
                                                                                                          End,
#ifdef HVSPACE
                                                                                           Child, HVSpace,
#endif
                                                                                                  End,
//|
///                                          Sprzeda�

                                                                                   Child, VGroup,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_SPR_TITLE),

                                                                                                                 Child, HGroup,
                                                                                                                                Child, HVSpace,
                                                                                                                                Child, MakeLabel2(MSG_SET_SPR_RESZTA),
                                                                                                                                End,
                                                                                                                 Child, CH_Set_Spr_Reszta = _MakeCheck(SET_SPR_RESZTA),
                                                                                                                 End,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_SPR_PARAGON_TITLE),
                                                                                                                 _SH( SH_SET_SPR_PARAGON ),

                                                                                                                 Child, HGroup,
                                                                                                                                Child, HVSpace,
                                                                                                                                Child, MakeLabel2(MSG_SET_SPR_PARAGON_FVAT),
                                                                                                                                End,
                                                                                                                 Child, CH_Set_Spr_Paragon_FVat = MakeCheck(MSG_SET_SPR_PARAGON_FVAT),

                                                                                                                 Child, HGroup,
                                                                                                                                Child, HVSpace,
                                                                                                                                Child, MakeLabel2(MSG_SET_SPR_PARAGON_PAR),
                                                                                                                                End,
                                                                                                                 Child, CH_Set_Spr_Paragon_Rach = MakeCheck(MSG_SET_SPR_PARAGON_RACH),

                                                                                                                 Child, HGroup,
                                                                                                                                Child, HVSpace,
                                                                                                                                Child, MakeLabel2(MSG_SET_SPR_PARAGON_RACH),
                                                                                                                                End,
                                                                                                                 Child, CH_Set_Spr_Paragon_Par = MakeCheck(MSG_SET_SPR_PARAGON_PAR),

                                                                                                                 Child, HGroup,
                                                                                                                                Child, HVSpace,
                                                                                                                                Child, MakeLabel2(MSG_SET_SPR_PARAGON_INNE),
                                                                                                                                End,
                                                                                                                 Child, CH_Set_Spr_Paragon_Inne = MakeCheck(MSG_SET_SPR_PARAGON_INNE),

                                                                                                                 End,

#ifdef HVSPACE
                                                                                                  Child, HVSpace,
#endif
                                                                                                  End,
//|

///                                          Posnet

                                                                                   Child, VGroup,

                                                                                                  Child, HGroup,
                                                                                                                 GroupFrameT(MSG_SET_POS_TITLE_1),
                                                                                                                 MUIA_Group_Columns, 2,

                                                                                                                 Child, HGroup,
                                                                                                                                Child, HVSpace,
                                                                                                                                Child, MakeLabel2(MSG_SET_POS_ACTIVE),
                                                                                                                                End,
                                                                                                                 Child, CH_Set_Pos_Active = _MakeCheck(SET_POS_ACTIVE),

                                                                                                                 End,

                                                                                                  Child, HGroup,
                                                                                                                 GroupFrameT(MSG_SET_POS_TITLE_1A),
                                                                                                                 MUIA_Group_Columns, 2,

                                                                                                                 Child, MakeLabel2(MSG_SET_POS_DEVICE),
                                                                                                                 Child, ST_Set_Pos_Device = _MakeString(35, SET_POS_DEVICE),
                                                                                                                 Child, MakeLabel2(MSG_SET_POS_UNIT),
                                                                                                                 Child, ST_Set_Pos_Unit   = _MakeNumericString(4, SET_POS_UNIT),
                                                                                                                 End,

                                                                                                  Child, HGroup,
                                                                                                                 GroupFrameT(MSG_SET_POS_TITLE_1B),
                                                                                                                 _SH( SH_SET_POS_TITLE_1B ),
                                                                                                                 MUIA_Group_Columns, 2,

                                                                                                                 Child, MakeLabel2(MSG_SET_POS_A),
                                                                                                                 Child, CY_Set_Pos_A = MakeCycle(VatTablePosnet, MSG_SET_POS_A),
                                                                                                                 Child, MakeLabel2(MSG_SET_POS_B),
                                                                                                                 Child, CY_Set_Pos_B = MakeCycle(VatTablePosnet, MSG_SET_POS_B),
                                                                                                                 Child, MakeLabel2(MSG_SET_POS_C),
                                                                                                                 Child, CY_Set_Pos_C = MakeCycle(VatTablePosnet, MSG_SET_POS_C),
                                                                                                                 Child, MakeLabel2(MSG_SET_POS_D),
                                                                                                                 Child, CY_Set_Pos_D = MakeCycle(VatTablePosnet, MSG_SET_POS_D),
                                                                                                                 Child, MakeLabel2(MSG_SET_POS_E),
                                                                                                                 Child, CY_Set_Pos_E = MakeCycle(VatTablePosnet, MSG_SET_POS_E),
                                                                                                                 Child, MakeLabel2(MSG_SET_POS_Z),
                                                                                                                 Child, CY_Set_Pos_Z = MakeCycle(VatTablePosnet, MSG_SET_POS_Z),
                                                                                                                 End,

#ifdef HVSPACE
                                                                                                  Child, HVSpace,
#endif
                                                                                                  End,
//|
///                                          Posnet 2

                                                                                   Child, VGroup,

                                                                                                  Child, HGroup,
                                                                                                                 GroupFrameT(MSG_SET_POS_TITLE_2),
                                                                                                                 MUIA_Group_Columns, 2,

                                                                                                                 Child, HGroup,
                                                                                                                                Child, HVSpace,
                                                                                                                                Child, MakeLabel2(MSG_SET_POS_DRAWER),
                                                                                                                                End,
                                                                                                                 Child, CH_Set_Pos_Drawer = _MakeCheck( SET_POS_DRAWER ),
                                                                                                                 End,

                                                                                                  Child, HGroup,
                                                                                                                 GroupFrameT(MSG_SET_POS_TITLE_2A),
                                                                                                                 _SH( SH_SET_POS_TITLE_2A ),
                                                                                                                 MUIA_Group_Columns, 2,

                                                                                                                 Child, MakeLabel2(MSG_SET_POS_IDLE),
                                                                                                                 Child, CY_Set_Pos_Idle = MakeCycle( PosnetIdleTable, MSG_SET_POS_IDLE ),

                                                                                                                 Child, MakeLabel2(MSG_SET_POS_IDLE_TXT1),
                                                                                                                 Child, ST_Set_Pos_Idle_Txt1 = MakeString(POSNET_LEN, MSG_SET_POS_IDLE_TXT1),
                                                                                                                 Child, MakeLabel2(MSG_SET_POS_IDLE_TXT2),
                                                                                                                 Child, ST_Set_Pos_Idle_Txt2 = MakeString(POSNET_LEN, MSG_SET_POS_IDLE_TXT2),
                                                                                                                 End,

                                                                                                  Child, HGroup,
                                                                                                                 GroupFrameT(MSG_SET_POS_TITLE_2B),
                                                                                                                 _SH( SH_SET_POS_TITLE_2B ),
                                                                                                                 MUIA_Group_Columns, 2,

                                                                                                                 Child, MakeLabel2(MSG_SET_POS_NADRUKI),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, CH_Set_Pos_Nadruki = MakeCheck(MSG_SET_POS_NADRUKI),
                                                                                                                                Child, HVSpace,
                                                                                                                                End,

                                                                                                                 Child, MakeLabel2(MSG_SET_POS_TXT1),
                                                                                                                 Child, ST_Set_Pos_Txt1 = MakeString(POSNET_PRINT_LEN, MSG_SET_POS_TXT1),
                                                                                                                 Child, MakeLabel2(MSG_SET_POS_TXT2),
                                                                                                                 Child, ST_Set_Pos_Txt2 = MakeString(POSNET_PRINT_LEN, MSG_SET_POS_TXT2),
                                                                                                                 Child, MakeLabel2(MSG_SET_POS_TXT3),
                                                                                                                 Child, ST_Set_Pos_Txt3 = MakeString(POSNET_PRINT_LEN, MSG_SET_POS_TXT3),
                                                                                                                 End,

#ifdef HVSPACE
                                                                                                  Child, HVSpace,
#endif
                                                                                                  End,
//|
///                                          Dane klienta

                                                                                   Child, VGroup,

                                                                                                  Child, HGroup,
                                                                                                                 GroupFrameT(MSG_SET_CUST_TITLE),
                                                                                                                 MUIA_Group_Columns, 2,

                                                                                                                 Child, MakeLabel2(MSG_SET_CUST_TOWN),
                                                                                                                 Child, ST_Set_Cust_Town = MakeString(35, MSG_SET_CUST_TOWN),

                                                                                                                 Child, MakeLabel2(MSG_SET_CUST_UPVAT),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, CH_Set_Cust_UpVat = MakeCheck(MSG_SET_CUST_UPVAT),
                                                                                                                                Child, MakeLabel2(MSG_SET_CUST_UPVAT_EXPIRE),
                                                                                                                                Child, HGroup,
                                                                                                                                           Child, ST_Set_Cust_UpVat_Expire    = MakeNumericString(4, MSG_SET_CUST_UPVAT_EXPIRE),
                                                                                                                                           Child, CH_Set_Cust_UpVat_Unlimited = MakeCheck(NULL),
                                                                                                                                           End,
                                                                                                                                End,

                                                                                                                 Child, MakeLabel2(MSG_SET_CUST_ADDENABLED),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, CH_Set_Cust_AddEnabled = MakeCheck(MSG_SET_CUST_ADDENABLED),

                                                                                                                                Child, HVSpace,

                                                                                                                                Child, MakeLabel2(MSG_SET_CUST_FIFO),
                                                                                                                                Child, CH_Set_Cust_FIFO = MakeCheck(MSG_SET_CUST_FIFO),
                                                                                                                                End,
                                                                                                                 End,




                                                                                                  Child, HGroup,
                                                                                                                 GroupFrameT(MSG_SET_CUST_TITLE_2),
                                                                                                                 MUIA_Group_Columns, 2,

                                                                                                                 Child, MakeLabel2(MSG_SET_CUST_GR_1),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, CH_Set_Cust_Gr_1 = MakeCheck(MSG_SET_CUST_GR_1),
                                                                                                                                Child, HVSpace,

                                                                                                                                Child, MakeLabel2(MSG_SET_CUST_GR_4),
                                                                                                                                Child, CH_Set_Cust_Gr_4 = MakeCheck(MSG_SET_CUST_GR_4),
                                                                                                                                End,

                                                                                                                 Child, MakeLabel2(MSG_SET_CUST_GR_2),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, CH_Set_Cust_Gr_2 = MakeCheck(MSG_SET_CUST_GR_2),
                                                                                                                                Child, HVSpace,

                                                                                                                                Child, MakeLabel2(MSG_SET_CUST_GR_5),
                                                                                                                                Child, CH_Set_Cust_Gr_5 = MakeCheck(MSG_SET_CUST_GR_5),
                                                                                                                                End,

                                                                                                                 Child, MakeLabel2(MSG_SET_CUST_GR_3),
                                                                                                                 Child, HGroup,
                                                                                                                                Child, CH_Set_Cust_Gr_3 = MakeCheck(MSG_SET_CUST_GR_3),
                                                                                                                                Child, HVSpace,

                                                                                                                                Child, MakeLabel2(MSG_SET_CUST_GR_6),
                                                                                                                                Child, CH_Set_Cust_Gr_6 = MakeCheck(MSG_SET_CUST_GR_6),
                                                                                                                                End,
                                                                                                                 End,

                                                                                                  Child, HGroup,
                                                                                                                 GroupFrameT(MSG_SET_CUST_TITLE_3),
                                                                                                                 Child, HVSpace,
                                                                                                                 Child, MakeLabel2(MSG_SET_CUST_CLONE),
                                                                                                                 Child, CH_Set_Cust_Clone = MakeCheck(MSG_SET_CUST_CLONE),
                                                                                                                 End,


#ifdef HVSPACE
                                                                                                  Child, HVSpace,
                                                                                                  Child, HVSpace,
#endif
                                                                                                  End,
//|
///                                          Magazyn

                                                                                   Child, VGroup,
                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_PROD_TITLE),

                                                                                                                 Child, MakeLabel2(MSG_SET_PROD_JEDN),
                                                                                                                 Child, ST_Set_Prod_Jedn = MakeString(PROD_JEDN_LEN, MSG_SET_PROD_JEDN),

                                                                                                                 Child, MakeLabel2(MSG_SET_PROD_VAT),
                                                                                                                 Child, CY_Set_Prod_Vat = MakeCycle(VatTable, MSG_SET_PROD_VAT),
                                                                                                                 End,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_MAG_MARZA_TITLE),

                                                                                                                 Child, HGroup,
                                                                                                                                Child, HVSpace,
                                                                                                                                Child, MakeLabel2(MSG_SET_MAG_MARZA),
                                                                                                                                End,
                                                                                                                 Child, CH_Set_Mag_Mar�a = MakeCheck(MSG_SET_MAG_MARZA),
                                                                                                                 End,

#ifdef HVSPACE
                                                                                                  Child, HVSpace,
#endif
                                                                                                  End,
//|
///                                          R��ne

                                                                                   Child, VGroup,
                                                                                                  Child, TextObject,
                                                                                                                 TextFrame, TextBack,
                                                                                                                 MUIA_Text_Contents, MSG_SET_MISC_WARN,
                                                                                                                 MUIA_Font, MUIV_Font_Tiny,
                                                                                                                 End,

                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_MISC_TITLE),

                                                                                                                 Child, MakeLabel2(MSG_SET_MISC_DOT),
                                                                                                                 Child, ST_Set_Misc_Dot = MakeStringAccept(2, MSG_SET_MISC_DOT, ".,"),
                                                                                                                 End,

#ifdef HVSPACE
                                                                                                  Child, HVSpace,
#endif

                                                                                                  End,
//|

///                                          Wydruki

                                                                                   Child, VGroup,

                                                                                           Child, ColGroup(2),
                                                                                                          GroupFrameT(MSG_SET_MAP_TITLE),

                                                                                                          Child, MakeLabel2( MSG_SET_MAP_FVAT ),
                                                                                                          Child, HGroup,
                                                                                                                         MUIA_Group_Spacing, 1,
                                                                                                                         Child, TX_Set_Map_FVat = TextObject2, End,
                                                                                                                         Child, BT_Set_Map_FVat = PopButton2( MUII_PopUp, MSG_SET_MAP_FVAT ),
                                                                                                                         End,

                                                                                                          Child, MakeLabel2( MSG_SET_MAP_FVAT_PRO ),
                                                                                                          Child, HGroup,
                                                                                                                         MUIA_Group_Spacing, 1,
                                                                                                                         Child, TX_Set_Map_FVatPro = TextObject2, End,
                                                                                                                         Child, BT_Set_Map_FVatPro = PopButton2( MUII_PopUp, MSG_SET_MAP_FVAT_PRO ),
                                                                                                                         End,
                                                                                                          Child, MakeLabel2( MSG_SET_MAP_PARAGON ),
                                                                                                          Child, HGroup,
                                                                                                                         MUIA_Group_Spacing, 1,
                                                                                                                         Child, TX_Set_Map_Paragon = TextObject2, End,
                                                                                                                         Child, BT_Set_Map_Paragon = PopButton2( MUII_PopUp, MSG_SET_MAP_PARAGON ),
                                                                                                                         End,
                                                                                                          Child, MakeLabel2( MSG_SET_MAP_ZAMOWIENIE ),
                                                                                                          Child, HGroup,
                                                                                                                         MUIA_Group_Spacing, 1,
                                                                                                                         Child, TX_Set_Map_Zamowienie = TextObject2, End,
                                                                                                                         Child, BT_Set_Map_Zamowienie = PopButton2( MUII_PopUp, MSG_SET_MAP_ZAMOWIENIE ),
                                                                                                                         End,

                                                                                                          Child, MakeLabel2( MSG_SET_MAP_CENNIK_DETAL ),
                                                                                                          Child, HGroup,
                                                                                                                         MUIA_Group_Spacing, 1,
                                                                                                                         Child, TX_Set_Map_CennikDetal = TextObject2, End,
                                                                                                                         Child, BT_Set_Map_CennikDetal = PopButton2( MUII_PopUp, MSG_SET_MAP_CENNIK_DETAL ),
                                                                                                                         End,
                                                                                                          Child, MakeLabel2( MSG_SET_MAP_CENNIK_HURT ),
                                                                                                          Child, HGroup,
                                                                                                                         MUIA_Group_Spacing, 1,
                                                                                                                         Child, TX_Set_Map_CennikHurt = TextObject2, End,
                                                                                                                         Child, BT_Set_Map_CennikHurt = PopButton2( MUII_PopUp, MSG_SET_MAP_CENNIK_HURT ),
                                                                                                                         End,
                                                                                                          Child, MakeLabel2( MSG_SET_MAP_SPIS ),
                                                                                                          Child, HGroup,
                                                                                                                         MUIA_Group_Spacing, 1,
                                                                                                                         Child, TX_Set_Map_SpisZNatury = TextObject2, End,
                                                                                                                         Child, BT_Set_Map_SpisZNatury = PopButton2( MUII_PopUp, MSG_SET_MAP_SPIS ),
                                                                                                                         End,
                                                                                                          Child, MakeLabel2( MSG_SET_MAP_PRZELEW ),
                                                                                                          Child, HGroup,
                                                                                                                         MUIA_Group_Spacing, 1,
                                                                                                                         Child, TX_Set_Map_Przelew = TextObject2, End,
                                                                                                                         Child, BT_Set_Map_Przelew = PopButton2( MUII_PopUp, MSG_SET_MAP_PRZELEW ),
                                                                                                                         End,
                                                                                                          End,

                                                                                           Child, HGroup,
                                                                                                          GroupFrameT(MSG_SET_DOC_TITLE_1),
                                                                                                          MUIA_Group_Columns, 2,

                                                                                                          Child, MakeLabel2(MSG_SET_DOC_UPVAT),
                                                                                                          Child, ST_Set_Doc_UpVat = MakeString(NORECE_LEN, MSG_SET_DOC_UPVAT),

                                                                                                          End,

#ifdef HVSPACE
                                                                                                  Child, HVSpace,
#endif
                                                                                                  End,
//|

///                                          Nadruki

                                                                                   Child, VGroup,
                                                                                                  Child, ColGroup(2),
                                                                                                                 GroupFrameT(MSG_SET_DOC2_TITLE),
                                                                                                                 Child, MakeLabel2(MSG_SET_DOC2_TXT1),
                                                                                                                 Child, ST_Set_Doc2_TXT1 = MakeString(DOC_TEXT_LEN, MSG_SET_DOC2_TXT1),
                                                                                                                 Child, MakeLabel2(MSG_SET_DOC2_TXT2),
                                                                                                                 Child, ST_Set_Doc2_TXT2 = MakeString(DOC_TEXT_LEN, MSG_SET_DOC2_TXT2),
                                                                                                                 Child, MakeLabel2(MSG_SET_DOC2_TXT3),
                                                                                                                 Child, ST_Set_Doc2_TXT3 = MakeString(DOC_TEXT_LEN, MSG_SET_DOC2_TXT3),
                                                                                                                 Child, MakeLabel2(MSG_SET_DOC2_TXT4),
                                                                                                                 Child, ST_Set_Doc2_TXT4 = MakeString(DOC_TEXT_LEN, MSG_SET_DOC2_TXT4),
                                                                                                                 Child, MakeLabel2(MSG_SET_DOC2_TXT5),
                                                                                                                 Child, ST_Set_Doc2_TXT5 = MakeString(DOC_TEXT_LEN, MSG_SET_DOC2_TXT5),
                                                                                                                 Child, MakeLabel2(MSG_SET_DOC2_TXT6),
                                                                                                                 Child, ST_Set_Doc2_TXT6 = MakeString(DOC_TEXT_LEN, MSG_SET_DOC2_TXT6),
                                                                                                                 Child, MakeLabel2(MSG_SET_DOC2_TXT7),
                                                                                                                 Child, ST_Set_Doc2_TXT7 = MakeString(DOC_TEXT_LEN, MSG_SET_DOC2_TXT7),
                                                                                                                 Child, MakeLabel2(MSG_SET_DOC2_TXT8),
                                                                                                                 Child, ST_Set_Doc2_TXT8 = MakeString(DOC_TEXT_LEN, MSG_SET_DOC2_TXT8),
                                                                                                                 Child, MakeLabel2(MSG_SET_DOC2_TXT9),
                                                                                                                 Child, ST_Set_Doc2_TXT9 = MakeString(DOC_TEXT_LEN, MSG_SET_DOC2_TXT9),
                                                                                                                 Child, MakeLabel2(MSG_SET_DOC2_TXT0),
                                                                                                                 Child, ST_Set_Doc2_TXT0 = MakeString(DOC_TEXT_LEN, MSG_SET_DOC2_TXT0),
                                                                                                  End,

#ifdef HVSPACE
                                                                                                  Child, HVSpace,
#endif
                                                                                                  End,
//|
///                                          Programy zewn�trzne

                                                                                   Child, VGroup,
                                                                                           Child, ColGroup(2),
                                                                                                         GroupFrameT(MSG_SET_COM_TITLE),

                                                                                                          Child, MakeLabel2(MSG_SET_COM_CALL),
                                                                                                          Child, HGroup,
                                                                                                                         Child, CH_Set_Com_Call = MakeCheck(MSG_SET_COM_CALL),
                                                                                                                         Child, HVSpace,
                                                                                                                         End,

                                                                                                          Child, MakeLabel2(MSG_SET_COM_SALE),
                                                                                                          Child, ST_Set_Com_Sale = MakeString(COMMAND_STRING_LEN, MSG_SET_COM_SALE),
                                                                                                          End,

#ifdef HVSPACE
                                                                                                  Child, HVSpace,
#endif
                                                                                                  End,
//|

                                                                                   End,

                                                                        End,

                                                         Child, HGroup,
                                                                        Child, BT_Set_Ok     = TextButton( MSG_SET_OK ),
                                                                        Child, BT_Set_Cancel = TextButton( MSG_SET_CANCEL ),
                                                                        End,

                                                         End,
                                                End;
//|


        if(UstawieniaWindow)
           {
           /* USTAWIENIA WINDOW */

           DoMethod(UstawieniaWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
           DoMethod(LV_Set_Strony, MUIM_List_Insert, strony_nazwy, -1, MUIV_List_Insert_Bottom);

           DoMethod(LV_Set_Strony, MUIM_Notify, _MUIA_List_Active, MUIV_EveryTime, GR_Set_Strony, 3, MUIM_Set, MUIA_Group_ActivePage, MUIV_TriggerValue);
           set(LV_Set_Strony, _MUIA_List_Active, 0);

           DoMethod(CH_Set_Own_SC  , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_SC);

           DoMethod(ST_Set_Own_Konto, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, BS_Set_Own_Bank, 3, MUIM_Set, MUIA_BankString_Account, MUIV_TriggerValue);

           DoMethod(CH_Set_Cust_UpVat          , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_UPVAT);
           DoMethod(CH_Set_Cust_UpVat_Unlimited, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_Set_Cust_UpVat_Expire, 3, MUIM_Set, MUIA_Disabled, MUIV_TriggerValue);
           DoMethod(CH_Set_Cust_AddEnabled     , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, CH_Set_Cust_FIFO, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue);

           DoMethod(CH_Set_Com_Call,  MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_Set_Com_Sale, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue);

//       DoMethod(CH_Set_Pos_Active,MUIM_Notify, MUIA_Selected, MUIV_EveryTime,


           DoMethod(BT_Set_Map_FVat       , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT_0);
           DoMethod(BT_Set_Map_FVatPro    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT_1);
           DoMethod(BT_Set_Map_Paragon    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT_2);
           DoMethod(BT_Set_Map_Zamowienie , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT_3);
           DoMethod(BT_Set_Map_CennikDetal, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT_4);
           DoMethod(BT_Set_Map_CennikHurt , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT_5);
           DoMethod(BT_Set_Map_SpisZNatury, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT_6);
           DoMethod(BT_Set_Map_Przelew    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT_7);



           DoMethod(BT_Set_Ok     ,   MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
           DoMethod(BT_Set_Cancel ,   MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


           DoMethod(UstawieniaWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

           // goustowanie posnetowych gadzetow jesli nie ma wlaczonej opcji POSNET

#define _POSNET(a)               DoMethod( CH_Set_Pos_Active, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, (a), 3,  MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue )
           _POSNET( ST_Set_Pos_Device );
           _POSNET( ST_Set_Pos_Unit );
           _POSNET( CY_Set_Pos_A );
           _POSNET( CY_Set_Pos_B );
           _POSNET( CY_Set_Pos_C );
           _POSNET( CY_Set_Pos_D );
           _POSNET( CY_Set_Pos_E );
           _POSNET( CY_Set_Pos_Z );

           _POSNET( CH_Set_Pos_Drawer );
           _POSNET( CY_Set_Pos_Idle );
           _POSNET( ST_Set_Pos_Idle_Txt1 );
           _POSNET( ST_Set_Pos_Idle_Txt2 );
           _POSNET( CH_Set_Pos_Nadruki );
           _POSNET( ST_Set_Pos_Txt1 );
           _POSNET( ST_Set_Pos_Txt2 );
           _POSNET( ST_Set_Pos_Txt3 );



           _attachwin(UstawieniaWindow);

           result = TRUE;
           }

        return(result);

}

//|

/// GetPrefsPage

// zwraca numer odpowiedniej strony preferencji
// ktorej pointer na nazwe z listy podany
// jest jako argument
//
// !! zwraca 0 jesli nie znalezione
// !! (to jest 1 strona prefsow!)
//

ULONG GetPrefsPage( char *pagenameptr )
{
ULONG page = 0;

        while( strony_nazwy[ page ] != NULL )
           {
           if( strony_nazwy[ page ] == pagenameptr )
                   return( page );

           page++;
           }

        return( 0 );
}

//|

/// EdycjaUstawie�Setup
void EdycjaUstawie�Setup(void)
{
char Tmp[10];

        // Wystawca

        setstring(ST_Set_Own_Nazwa_1, settings.wystawca.Nazwa1);
        setstring(ST_Set_Own_Nazwa_2, settings.wystawca.Nazwa2);
        setcheckmark(CH_Set_Own_SC  , settings.wystawca.Sp��kaCywilna);
        setstring(ST_Set_Own_Ulica  , settings.wystawca.Ulica);
        setstring(ST_Set_Own_Kod    , settings.wystawca.Kod);
        setstring(ST_Set_Own_Miasto , settings.wystawca.Miasto);
        setstring(ST_Set_Own_Tel    , settings.wystawca.Telefon);
        setstring(ST_Set_Own_Fax    , settings.wystawca.Fax);
        setstring(ST_Set_Own_Email  , settings.wystawca.Email);
        setstring(ST_Set_Own_NIP    , NIP2Str(settings.wystawca.NIP, settings.wystawca.Sp��kaCywilna));
        setstring(ST_Set_Own_Regon  , settings.wystawca.Regon);
        setstring(BS_Set_Own_Bank   , settings.wystawca.Bank);
        setstring(ST_Set_Own_Konto  , settings.wystawca.Konto);


        // Sprzeda�

        setcheckmark(CH_Set_Spr_Reszta      , settings.reszta_aktywna);
        setcheckmark(CH_Set_Spr_Paragon_FVat, settings.paragon_fvat);
        setcheckmark(CH_Set_Spr_Paragon_Rach, settings.paragon_rach);
        setcheckmark(CH_Set_Spr_Paragon_Par , settings.paragon_par);
        setcheckmark(CH_Set_Spr_Paragon_Inne, settings.paragon_inne);


        // Posnet

        setcheckmark(CH_Set_Pos_Active, !settings.posnet_active);  // trick to make sure, the checkmark assigned notification will always be triggered
        setcheckmark(CH_Set_Pos_Active, settings.posnet_active);

        setstring(ST_Set_Pos_Device , settings.serial_device);
        setnumstring(ST_Set_Pos_Unit, settings.serial_unit);

        setcycle(CY_Set_Pos_A, settings.ptu_22);
        setcycle(CY_Set_Pos_B, settings.ptu_17);
        setcycle(CY_Set_Pos_C, settings.ptu_7);
        setcycle(CY_Set_Pos_D, settings.ptu_0);
        setcycle(CY_Set_Pos_E, settings.ptu_3);
        setcycle(CY_Set_Pos_Z, settings.ptu_zw);


        // Posnet 2

        setcheckmark(CH_Set_Pos_Drawer, settings.posnet_open_drawer);

        setcycle(CY_Set_Pos_Idle, settings.posnet_idle);
        setstring(ST_Set_Pos_Idle_Txt1, settings.posnet_idle_text1);
        setstring(ST_Set_Pos_Idle_Txt2, settings.posnet_idle_text2);

        setcheckmark(CH_Set_Pos_Nadruki, !settings.posnet_nadruki);  // same trick as with 1st Posnet checkmark above - to fool BOOBSI notifications
        setcheckmark(CH_Set_Pos_Nadruki, settings.posnet_nadruki);
        setstring(ST_Set_Pos_Txt1, settings.posnet_nadruk_1);
        setstring(ST_Set_Pos_Txt2, settings.posnet_nadruk_2);
        setstring(ST_Set_Pos_Txt3, settings.posnet_nadruk_3);



        // Klient

        setstring(ST_Set_Cust_Town  , settings.def_miasto);

        setcheckmark(CH_Set_Cust_UpVat          , settings.def_upvat);
        setnumstring(ST_Set_Cust_UpVat_Expire   , settings.def_expire_offset);
        setcheckmark(CH_Set_Cust_UpVat_Unlimited, settings.def_unlimited);
        setcheckmark(CH_Set_Cust_AddEnabled     , settings.def_addenabled);
        setcheckmark(CH_Set_Cust_FIFO           , settings.def_fifo);

        setcheckmark(CH_Set_Cust_Gr_1, settings.def_sprzedawca);
        setcheckmark(CH_Set_Cust_Gr_2, settings.def_nabywca);
        setcheckmark(CH_Set_Cust_Gr_3, settings.def_kosztowiec);
        setcheckmark(CH_Set_Cust_Gr_4, settings.def_a);
        setcheckmark(CH_Set_Cust_Gr_5, settings.def_b);
        setcheckmark(CH_Set_Cust_Gr_6, settings.def_c);

        setcheckmark(CH_Set_Cust_Clone, settings.clone_filter);


        // Magazyn

        setstring(ST_Set_Prod_Jedn  , settings.def_miara);
        setcycle(CY_Set_Prod_Vat    , settings.def_vat);

        setcheckmark( CH_Set_Mag_Mar�a, settings.sta�a_mar�a_procentowa);


        // R��ne

        sprintf(Tmp, "%c", settings.def_dot);
        setstring(ST_Set_Misc_Dot  , Tmp);



        // Wydruki

        setstring(ST_Set_Doc_UpVat        , settings.upvat_text);

        settext( TX_Set_Map_FVat, GetPrinterName(settings.map_fvat) );
        settext( TX_Set_Map_FVatPro, GetPrinterName(settings.map_fvat_proforma) );
        settext( TX_Set_Map_Paragon, GetPrinterName(settings.map_paragon) );
        settext( TX_Set_Map_Zamowienie, GetPrinterName(settings.map_zamowienie) );
        settext( TX_Set_Map_CennikDetal, GetPrinterName(settings.map_cennik_detal) );
        settext( TX_Set_Map_CennikHurt, GetPrinterName(settings.map_cennik_hurt) );
        settext( TX_Set_Map_SpisZNatury, GetPrinterName(settings.map_spis) );
        settext( TX_Set_Map_StanyMagazynowe, GetPrinterName(settings.map_stany) );
        settext( TX_Set_Map_Przelew, GetPrinterName(settings.map_przelew) );


        // Nadruki

        setstring(ST_Set_Doc2_TXT1, settings.text_1);
        setstring(ST_Set_Doc2_TXT2, settings.text_2);
        setstring(ST_Set_Doc2_TXT3, settings.text_3);
        setstring(ST_Set_Doc2_TXT4, settings.text_4);
        setstring(ST_Set_Doc2_TXT5, settings.text_5);
        setstring(ST_Set_Doc2_TXT6, settings.text_6);
        setstring(ST_Set_Doc2_TXT7, settings.text_7);
        setstring(ST_Set_Doc2_TXT8, settings.text_8);
        setstring(ST_Set_Doc2_TXT9, settings.text_9);
        setstring(ST_Set_Doc2_TXT0, settings.text_0);


        // Programy zewn�trzne

        setcheckmark(CH_Set_Com_Call, settings.com_call);
        setstring(ST_Set_Com_Sale   , settings.com_sale);


}
//|
/// EdycjaUstawie�Finish

ULONG EdycjaUstawie�Finish( Object *window )
{

//    memset(&settings, 0, sizeof(struct Settings));
/* Na kiego to ^ wa�a? Kasuje tak�e typy numeracji, kt�re s� w tej samej strukturze!!!
** jak to ma mie� co� wsp�lnego z wersj� DEMO to chyba jednak nie tu... */

#define _set_error(a)  {char *code = a; pos_counter++; if( code != NULL ) { MUI_Request( app, window, 0, TITLE, MSG_OK, MSG_SET_ERR_CODE, code ); return( pos_counter ); } }


ULONG result = 0;       // 0 - OK, inaczej kod bledy (nie uzywane poki co)
ULONG pos_counter = 1;

#ifndef DEMO
        copystr(settings.wystawca.Nazwa1,  ST_Set_Own_Nazwa_1);
        copystr(settings.wystawca.Nazwa2,  ST_Set_Own_Nazwa_2);
        settings.wystawca.Sp��kaCywilna = getcheckmark(CH_Set_Own_SC);
        copystr(settings.wystawca.Ulica,   ST_Set_Own_Ulica);
        copystr(settings.wystawca.Kod,     ST_Set_Own_Kod);
        copystr(settings.wystawca.Miasto,  ST_Set_Own_Miasto);
        copystr(settings.wystawca.Telefon, ST_Set_Own_Tel);
        copystr(settings.wystawca.Fax,     ST_Set_Own_Fax);
        copystr(settings.wystawca.Email,   ST_Set_Own_Email);
        strcpy(settings.wystawca.NIP,     Str2NIP((char *)xget(ST_Set_Own_NIP, MUIA_String_Contents)));
        copystr(settings.wystawca.Regon,   ST_Set_Own_Regon);
        copystr(settings.wystawca.Bank,    BS_Set_Own_Bank);
        copystr(settings.wystawca.Konto,   ST_Set_Own_Konto);
#endif



        // Sprzeda�

        settings.reszta_aktywna = getcheckmark(CH_Set_Spr_Reszta);
        settings.paragon_fvat   = getcheckmark(CH_Set_Spr_Paragon_FVat);
        settings.paragon_rach   = getcheckmark(CH_Set_Spr_Paragon_Rach);
        settings.paragon_par    = getcheckmark(CH_Set_Spr_Paragon_Par);
        settings.paragon_inne   = getcheckmark(CH_Set_Spr_Paragon_Inne);


        // Posnet

        settings.posnet_active = getcheckmark(CH_Set_Pos_Active);

        copystr(settings.serial_device, ST_Set_Pos_Device);
        settings.serial_unit = getnumstr(ST_Set_Pos_Unit);

        settings.ptu_22 = getcycle(CY_Set_Pos_A);
        settings.ptu_17 = getcycle(CY_Set_Pos_B);
        settings.ptu_7  = getcycle(CY_Set_Pos_C);
        settings.ptu_3  = getcycle(CY_Set_Pos_E);
        settings.ptu_0  = getcycle(CY_Set_Pos_D);
        settings.ptu_zw = getcycle(CY_Set_Pos_Z);


        // Posnet 2

        settings.posnet_open_drawer = getcheckmark(CH_Set_Pos_Drawer);

        settings.posnet_idle = getcycle(CY_Set_Pos_Idle);
        copystr(settings.posnet_idle_text1, ST_Set_Pos_Idle_Txt1);
        copystr(settings.posnet_idle_text2, ST_Set_Pos_Idle_Txt2);

        settings.posnet_nadruki = getcheckmark(CH_Set_Pos_Nadruki);
        copystr(settings.posnet_nadruk_1, ST_Set_Pos_Txt1);
        copystr(settings.posnet_nadruk_2, ST_Set_Pos_Txt2);
        copystr(settings.posnet_nadruk_3, ST_Set_Pos_Txt3);



        // Klient

        copystr(settings.def_miasto, ST_Set_Cust_Town);

        settings.def_upvat         = getcheckmark(CH_Set_Cust_UpVat);
        settings.def_expire_offset = getnumstr(ST_Set_Cust_UpVat_Expire);
        settings.def_unlimited     = getcheckmark(CH_Set_Cust_UpVat_Unlimited);
        settings.def_addenabled    = getcheckmark(CH_Set_Cust_AddEnabled);
        settings.def_fifo          = getcheckmark(CH_Set_Cust_FIFO);

        settings.def_sprzedawca = getcheckmark(CH_Set_Cust_Gr_1);
        settings.def_nabywca    = getcheckmark(CH_Set_Cust_Gr_2);
        settings.def_kosztowiec = getcheckmark(CH_Set_Cust_Gr_3);
        settings.def_a          = getcheckmark(CH_Set_Cust_Gr_4);
        settings.def_b          = getcheckmark(CH_Set_Cust_Gr_5);
        settings.def_c          = getcheckmark(CH_Set_Cust_Gr_6);

        settings.clone_filter   = getcheckmark(CH_Set_Cust_Clone);


        // Magazyn

        copystr(settings.def_miara, ST_Set_Prod_Jedn);
        settings.def_vat = getcycle(CY_Set_Prod_Vat);

        settings.sta�a_mar�a_procentowa = getcheckmark( CH_Set_Mag_Mar�a );


        // R��ne

        settings.def_dot = *(char *)(xget(ST_Set_Misc_Dot, MUIA_String_Contents));


        // Wydruki

        copystr(settings.upvat_text, ST_Set_Doc_UpVat);

        settings.map_fvat = GetPrinterID( gettext( TX_Set_Map_FVat ) );
        settings.map_fvat_proforma = GetPrinterID( gettext( TX_Set_Map_FVatPro ) );
        settings.map_paragon = GetPrinterID( gettext( TX_Set_Map_Paragon ) );
        settings.map_zamowienie = GetPrinterID( gettext( TX_Set_Map_Zamowienie ) );

        settings.map_cennik_detal = GetPrinterID( gettext( TX_Set_Map_CennikDetal ) );
        settings.map_cennik_hurt = GetPrinterID( gettext( TX_Set_Map_CennikHurt ) );
        settings.map_spis = GetPrinterID( gettext( TX_Set_Map_SpisZNatury ) );
        settings.map_stany = GetPrinterID( gettext( TX_Set_Map_StanyMagazynowe ) );
        settings.map_przelew = GetPrinterID( gettext( TX_Set_Map_Przelew ) );


        // Nadruki

        copystr(settings.text_1, ST_Set_Doc2_TXT1);
        copystr(settings.text_2, ST_Set_Doc2_TXT2);
        copystr(settings.text_3, ST_Set_Doc2_TXT3);
        copystr(settings.text_4, ST_Set_Doc2_TXT4);
        copystr(settings.text_5, ST_Set_Doc2_TXT5);
        copystr(settings.text_6, ST_Set_Doc2_TXT6);
        copystr(settings.text_7, ST_Set_Doc2_TXT7);
        copystr(settings.text_8, ST_Set_Doc2_TXT8);
        copystr(settings.text_9, ST_Set_Doc2_TXT9);
        copystr(settings.text_0, ST_Set_Doc2_TXT0);


        // Programy zewn�trzne

        settings.com_call = getcheckmark(CH_Set_Com_Call);
        copystr(settings.com_sale, ST_Set_Com_Sale);


        return( result );
}
//|
/// EdycjaUstawie�
char EdycjaUstawie�(void)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

        set(app, MUIA_Application_Sleep, TRUE);


        if(!CreatePrefsWindow())
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           set(app, MUIA_Application_Sleep, FALSE);
           return(result);
           }


        set(UstawieniaWindow, MUIA_Window_ActiveObject , LV_Set_Strony);
        set(UstawieniaWindow, MUIA_Window_DefaultObject, LV_Set_Strony);

        EdycjaUstawie�Setup();

        if(WinOpen(UstawieniaWindow))
                {
                while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);
                  switch(ID)
                        {

                        case ID_CUST_SC:
                           setstring(ST_Set_Own_NIP, NIP2Str(Str2NIP(getstr(ST_Set_Own_NIP)), getcheckmark(CH_Set_Own_SC)));
                           break;

                        case ID_CUST_UPVAT:
                           set(CH_Set_Cust_UpVat_Unlimited, MUIA_Disabled, !(getcheckmark(CH_Set_Cust_UpVat)));
                           if(!getcheckmark(CH_Set_Cust_UpVat_Unlimited))
                                   set(ST_Set_Cust_UpVat_Expire, MUIA_Disabled, !(getcheckmark(CH_Set_Cust_UpVat)));
                           break;


                        case ID_SELECT_0:
                        case ID_SELECT_1:
                        case ID_SELECT_2:
                        case ID_SELECT_3:
                        case ID_SELECT_4:
                        case ID_SELECT_5:
                        case ID_SELECT_6:
                        case ID_SELECT_7:
                                 {
                                 struct PrinterList *prt = DrukarkaSelector( NULL );
                                 Object *obj;

                                 switch( ID )
                                   {
                                   case ID_SELECT_0: obj = TX_Set_Map_FVat; break;
                                   case ID_SELECT_1: obj = TX_Set_Map_FVatPro; break;
                                   case ID_SELECT_2: obj = TX_Set_Map_Paragon; break;
                                   case ID_SELECT_3: obj = TX_Set_Map_Zamowienie; break;
                                   case ID_SELECT_4: obj = TX_Set_Map_CennikDetal; break;
                                   case ID_SELECT_5: obj = TX_Set_Map_CennikHurt; break;
                                   case ID_SELECT_6: obj = TX_Set_Map_SpisZNatury; break;
                                   case ID_SELECT_7: obj = TX_Set_Map_Przelew; break;
                                   }

                                 if( prt && obj )
                                   settext( obj, prt->pl_printer.Name );
                                 }
                                 break;


                        case ID_OK:
                        case ID_USE:
                           {
                           set(app, MUIA_Application_Sleep, TRUE);
                           if( EdycjaUstawie�Finish( UstawieniaWindow ) > 0 )
                                   {
                                   // przelaczamy na stosowna strone prefsow
//                   set( LV_Set_Strony, _MUIA_List_Active, GetPrefsPage( MSG_SET_PRINT3_PAGE ) );

                                   DisplayBeep(0);
                                   _sleep( FALSE );
                                   break;
                                   }


                           if(ID == ID_OK)
                                 {
                                 ZapiszUstawienia();
                                 }

                           posnet_DisplayIdle();

                           set(app, MUIA_Application_Sleep, FALSE);
                           result  = TRUE;
                           running = FALSE;
                           }
                           break;

                        case ID_CANCEL:
//               set(app, MUIA_Application_Sleep, TRUE);

//               set(app, MUIA_Application_Sleep, FALSE);
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

                }
         else
                {
                DisplayBeep(NULL);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


        _detachwin(UstawieniaWindow);
        set(app, MUIA_Application_Sleep, FALSE);

        return(result);

}
//|

/// EDYCJA RODZAJU P�ATNO�CI STRINGS

#define MSG_ED_PAY_WIN_TITLE "Rodzaje p�atno�ci"

#define MSG_SET_PAY_TITLE    "Rodzaje p�atno�ci"
#define MSG_SET_PAY_ADD      "F1 - Dod_aj"
#define MSG_SET_PAY_EDIT     "F2 - _Edytuj"
#define MSG_SET_PAY_DEL      "DEL - S_kasuj"
#define MSG_SET_PAY_MAX      "\033cMo�esz zadeklarowa� maksymalnie\n%ld rodzaj�w p�atno�ci."

#define MSG_SET_PAY_OK       "F10 - _Ok"
#define MSG_SET_PAY_CANCEL   "ESC - Ponie_chaj"

#define MSG_PAY_DELETED_GAD  "*_Tak|_Nie"
#define MSG_PAY_DELETED      "Rodzaj \"%s\"\njest aktualnie zaznaczony jako \033bSKASOWANY\033n,\ni nie mo�e by� modyfikowany. Czy chcesz go teraz\nodkasowa� i przyst�pi� do edycji?"

//|
/// UstawieniaP�atno�ciSetup

static Object
           *UstawieniaP�atno�ciWindow,
           *LV_Set_Pay,
           *BT_Set_Pay_Add,
           *BT_Set_Pay_Edit,
           *BT_Set_Pay_Del,
           *BT_Set_Pay_Ok,
           *BT_Set_Pay_Cancel;

char UstawieniaP�atno�ciSetup(void)
{

///   Create Window

          UstawieniaP�atno�ciWindow = WindowObject,
                                                MUIA_Window_ID         , ID_WIN_PAYSETTINGS,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                MUIA_Window_Title, MSG_ED_PAY_WIN_TITLE,
                                                WindowContents,
                                                   VGroup,

                                                   Child, VGroup,
                                                                  GroupFrameT(MSG_SET_PAY_TITLE),

                                                                  Child, LV_Set_Pay = _ListviewObject,
                                                                                 MUIA_CycleChain, TRUE,
                                                                                 _MUIA_List_AutoVisible, TRUE,
                                                                                 _MUIA_Listview_List, Golem_PayListObject, End,
                                                                                 End,

                                                                  Child, HGroup,
                                                                                 Child, BT_Set_Pay_Add  = TextButton(MSG_SET_PAY_ADD),
                                                                                 Child, BT_Set_Pay_Edit = TextButton(MSG_SET_PAY_EDIT),
                                                                                 Child, BT_Set_Pay_Del  = TextButton(MSG_SET_PAY_DEL),
                                                                                 End,
                                                                  End,

                                                   Child, HGroup,
                                                                  MUIA_Group_SameSize, TRUE,
                                                                  Child, BT_Set_Pay_Ok     = TextButton(MSG_SET_PAY_OK),
                                                                  Child, BT_Set_Pay_Cancel = TextButton(MSG_SET_PAY_CANCEL),
                                                                  End,


                                                   End,
                                          End;
//|

        if( !UstawieniaP�atno�ciWindow )
           return(FALSE);


        // Lista p�atno�ci

        {
        struct P�atno��List *p�at;

        if( !IsListEmpty(&p�atno�ci) )
           {
           for( p�at = (struct P�atno��List *)p�atno�ci.lh_Head; p�at->pl_node.mln_Succ; p�at = (struct P�atno��List *)p�at->pl_node.mln_Succ )
                 {
                 DoMethod(LV_Set_Pay, _MUIM_List_InsertSingle, p�at, _MUIV_List_Insert_Sorted);
                 }
           }
        }


        // notyfikacje

        DoMethod(UstawieniaP�atno�ciWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(LV_Set_Pay        , MUIM_Notify, _MUIA_Listview_DoubleClick, TRUE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SET_PAY_EDIT);
        DoMethod(BT_Set_Pay_Add    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SET_PAY_ADD);
        DoMethod(BT_Set_Pay_Edit   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SET_PAY_EDIT);
        DoMethod(BT_Set_Pay_Del    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SET_PAY_DEL);

        DoMethod(BT_Set_Pay_Ok     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Set_Pay_Cancel , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        
        _KeyNotify( UstawieniaP�atno�ciWindow, "f10", ID_OK );
        _KeyNotify( UstawieniaP�atno�ciWindow, "f1" , ID_SET_PAY_ADD );
        _KeyNotify( UstawieniaP�atno�ciWindow, "f2" , ID_SET_PAY_EDIT );
        _KeyNotify( UstawieniaP�atno�ciWindow, "del", ID_SET_PAY_DEL );

        _attachwin( UstawieniaP�atno�ciWindow );


        set(UstawieniaP�atno�ciWindow, MUIA_Window_ActiveObject, LV_Set_Pay);


        return( TRUE );

}
//|
/// UstawieniaP�atno�ci
char UstawieniaP�atno�ci(void)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

        _sleep(TRUE);


        if(!UstawieniaP�atno�ciSetup())
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }


        if(WinOpen(UstawieniaP�atno�ciWindow))
                {
                while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);
                  switch(ID)
                        {

///           RODZAJE P�ATNO�CI

                        case ID_SET_PAY_ADD:
                           {
                           struct P�atno��List *p�atno��;

//               D(bug("%ld\n", xget(LV_Set_Pay, _MUIA_List_Entries)));

                           if(xget(LV_Set_Pay, _MUIA_List_Entries) >= MAX_PAYS)
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, UstawieniaP�atno�ciWindow, 0, TITLE, MSG_OK, MSG_SET_PAY_MAX, MAX_PAYS);
                                  break;
                                  }

                           if((p�atno�� = calloc(1, sizeof(struct P�atno��List))) != NULL)
                                  {
                                  if
                                  (EdycjaP�atno�ci(p�atno��, FALSE))
                                        {
                                        long pos = DodajP�atno��(p�atno��);

                                        DoMethod(LV_Set_Pay, _MUIM_List_InsertSingle, p�atno��, pos);
                                        set(LV_Set_Pay, _MUIA_List_Active, pos);
                                        }
                                  }
                           else
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, UstawieniaP�atno�ciWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                                  }
                           }
                           break;


                        case ID_SET_PAY_EDIT:
                           {
                           if(xget(LV_Set_Pay, _MUIA_List_Active) != _MUIV_List_Active_Off)
                                 {
                                 struct P�atno��List *p�atno��;

                                 DoMethod(LV_Set_Pay, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &p�atno��);

                                 if(p�atno��->Deleted)
                                   {
                                   if(MUI_Request(app, UstawieniaP�atno�ciWindow, 0, TITLE, MSG_PAY_DELETED_GAD, MSG_PAY_DELETED, p�atno��->pl_p�atno��.Nazwa))
                                         {
                                         p�atno��->Deleted = FALSE;
                                         DoMethod(LV_Set_Pay, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                                         }
                                   else
                                         break;
                                   }


                                 switch(EdycjaP�atno�ci(p�atno��, TRUE))
                                           {
                                           case TRUE:
                                                        DoMethod(LV_Set_Pay, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                                                        break;

                                           case REFRESH:
                                                        {
                                                        long pos;
                                                        set(LV_Set_Pay, _MUIA_List_Quiet, TRUE);

                                                        DoMethod(LV_Set_Pay, _MUIM_List_Remove, _MUIV_List_Remove_Active);

                                                        Remove((struct Node*)p�atno��);
                                                        pos = DodajP�atno��(p�atno��);

                                                        DoMethod(LV_Set_Pay, _MUIM_List_InsertSingle, p�atno��, pos);
                                                        set(LV_Set_Pay, _MUIA_List_Active, pos);

                                                        set(LV_Set_Pay, _MUIA_List_Quiet, FALSE);
                                                        }
                                                        break;
                                           }
                                 }
                           }
                           break;



                        case ID_SET_PAY_DEL:
                           {
                           if(xget(LV_Set_Pay, _MUIA_List_Active) != _MUIV_List_Active_Off)
                                   {
                                   struct P�atno��List *p�atno��;

                                   DoMethod(LV_Set_Pay, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &p�atno��);

                                   p�atno��->Deleted = ((p�atno��->Deleted + 1) & 0x1);

                                   DoMethod(LV_Set_Pay, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                                   }
                           }
                           break;

//|


                        case ID_OK:
                           {
                           _sleep(TRUE);

                           Usu�P�atno�ci(FALSE);
                           ZapiszP�atno�ci();

                           _sleep(FALSE);
                           result  = TRUE;
                           running = FALSE;
                           }
                           break;

                        case ID_CANCEL:
                           {
                           _sleep(TRUE);

                           Usu�P�atno�ci(TRUE);
                           WczytajP�atno�ci();

                           _sleep(FALSE);
                           running = FALSE;
                           }
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }
                }
         else
                {
                DisplayBeep(NULL);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


        _detachwin(UstawieniaP�atno�ciWindow);
        _sleep(FALSE);

        return(result);

}
//|

