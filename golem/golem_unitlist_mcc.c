
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 


/*** includes ***/

#include <clib/alib_protos.h>
#include <clib/graphics_protos.h>
#include <clib/utility_protos.h>
#include <clib/muimaster_protos.h>
#include <pragmas/muimaster_pragmas.h>
#include <libraries/mui.h>

#if MUIMASTER_VLATEST <= 14
#include <mui/mui33_mcc.h>
#endif

#include <mui/muiundoc.h>

#include <mui/interlist.h>
#include "golem_structs.h"
#include "golem_unitlist_mcc.h"


#include <stdio.h>
#define __NAME      "Golem_UnitList"
#define CLASS       MUIC_Golem_UnitList
#define SUPERCLASS  _MUIC_List

void _XCEXIT(int code) {}


struct Data
{
	LONG  Dummy;
};


#include "golem_unitlist_revision.h"

#define UserLibID UNITLIST_VERSTAG " � 1998-2002 Marcin Orlowski <carlos@wfmh.org.pl>"
#define MASTERVERSION 14

#define MYDEBUG 0

#define VERSION  UNITLIST_VERSION
#define REVISION UNITLIST_REVISION

#define MCC_USES_LOCALE
#include "golem_mccheader_nlist.c"


/// LISTA JEDNOSTEK MIARY - STRINGS

#define MSG_UNIT_NAME "\0338\033bNazwa"
#define MSG_UNIT_CALK "\0338\033bCa�k."

//|


/// Unit list hooks
/// HOOK: UnitList_Display
void * __saveds __asm UnitList_Display(register __a2 char **array, register __a1 struct JednostkaList *node)
{
static char Del[20],
			Nazwa[PROD_JEDN_LEN + 10],
			Ca�k[20];

   if(node)
	 {
	 struct Jednostka *unit = &node->ul_jedn;

	 sprintf(Del   , "%s" , ((node->Deleted==FALSE) ? " " : "*"));

	 *array++ = Del;
	 if( node->Imported == TRUE )
	   {
	   sprintf(Nazwa, "\033b%s", unit->Nazwa);
	   *array++ = Nazwa;
	   sprintf(Ca�k , "\033b%s", ( (unit->Ca�kowita==TRUE) ? "T" : " " ));
	   *array++ = Ca�k;
	   }
	 else
	   {
	   *array++ = unit->Nazwa;
	   sprintf(Ca�k , "%s", ( (unit->Ca�kowita==TRUE) ? "T" : " " ));
	   *array++ = Ca�k;
	   }

	 }
   else
	 {
	 *array++ = "";
	 *array++ = MSG_UNIT_NAME;
	 *array++ = MSG_UNIT_CALK;
	 }


	 return(0);
}
//|
/// HOOK: UnitList_CompareStr
LONG __saveds __asm UnitList_CompareStr(register __a1 struct JednostkaList *Node1, register __a2 struct JednostkaList *Node2)
{
	return((LONG)StrnCmp(Locale, Node1->ul_jedn.Nazwa, Node2->ul_jedn.Nazwa, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG UnitList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook UnitList_DisplayHook     = { {NULL, NULL}, (VOID *)UnitList_Display   ,NULL,NULL };
static const struct Hook UnitList_CompareHookStr  = { {NULL, NULL}, (VOID *)UnitList_CompareStr,NULL,NULL };

	obj = (Object *)DoSuperNew(cl,obj,
					MUIA_CycleChain, TRUE,
					InputListFrame,
					_MUIA_List_AutoVisible  , TRUE,
					_MUIA_List_Title        , TRUE,
					_MUIA_List_DisplayHook  , &UnitList_DisplayHook,
					_MUIA_List_CompareHook  , &UnitList_CompareHookStr,
					_MUIA_List_Format       , "MIW=1 MAW=-1 COL=0 BAR,"
											  "MIW=1 MAW=98 COL=1 BAR,"
											  "MIW=1 MAW=-1 COL=2 P=\033c",


#ifdef USE_NLIST
					 MUIA_NList_TitleSeparator, TRUE,
					 MUIA_NList_AutoCopyToClip, TRUE,
#endif

				TAG_MORE, msg->ops_AttrList);


	  if(obj)
		  {
//          struct Data *data = INST_DATA(cl,obj);


		  msg->MethodID = OM_SET;
		  DoMethodA(obj, (Msg)msg);
		  msg->MethodID = OM_NEW;
		  }


	  return((ULONG)obj);
}

//|
/// OM_GET
static ULONG ASM UnitList_Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ProductList_Data *data = INST_DATA(cl,obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

	switch(((struct opGet *)msg)->opg_AttrID)
	   {
	   case MUIA_Version:
			*store = UNITLIST_VERSION;
			return(TRUE);
			break;

	   case MUIA_Revision:
			*store = UNITLIST_REVISION;
			return(TRUE);
			break;

	   }

	return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher

ULONG ASM SAVEDS _Dispatcher(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{

	switch (msg->MethodID)
	   {
	   case OM_NEW:   return( UnitList_New (cl, obj, (APTR)msg));
	   case OM_GET:   return( UnitList_Get (cl, obj, (APTR)msg));
	   }

	return(DoSuperMethodA(cl,obj,msg));
}

//|

