
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/* $Id: golem_requesters.c,v 1.1 2003/01/01 20:40:52 carl-os Exp $ */

#include "golem.h"

/*
/// NumericSetup
void NumericSetup(void)
{
	set(ST_Numer_Amount, MUIA_String_Integer, 2);

	set(NumericWindow, MUIA_Window_ActiveObject, ST_Numer_Amount);

}
//|
/// NumericFinish
char NumericFinish(void)
{
/*
** sprawdza poprawno�� wprowadzonych danych
** dot. drukowania. Zwraca FALSE, je�li co�
** jest nie tak, TRUE, je�li wszystkie
** elementy s� poprawne.
*/


	if(!(strlen((char *)xget(ST_Numer_Amount, MUIA_String_Contents))) )
	   {
	   set(NumericWindow, MUIA_Window_ActiveObject, ST_Numer_Amount);
	   return(FALSE);
	   }

	return(TRUE);
}
//|
/// Numeric

char RequestNumeric(struct NumericRequest)
{
/*
** Requester dot. warto�ci numerycznych
** Zwraca BOOL w zale�no�ci czy u�ytkownik
** wybra� OK czy nie. Wynik umieszczany
** jest w odpowiednim polu struktury NumericRequest
** w zale�no�ci od typu reqestera
*/

char  running = TRUE;
ULONG signal  = 0;
long  result  = -1;

	set(app, MUIA_Application_Sleep, TRUE);

	NumericSetup();

	set(NumericWindow, MUIA_Window_Open, TRUE);

		while(running)
		  {
		  switch (DoMethod(app, MUIM_Application_Input, &signal))
			{
			case ID_OK:
			   if(NumericFinish() == FALSE)
				  {
				  DisplayBeep(0);
				  }
			   else
				  {
				  result = xget(ST_Numer_Amount, MUIA_String_Integer);
				  running = FALSE;
				  }
			   break;


			case ID_CANCEL:
			   running = FALSE;
			   break;

			case ID_BACK:
			   result = -2;
			   running = FALSE;
			   break;

			}
		  if(running && signal) Wait(signal);
		  }

	set(NumericWindow, MUIA_Window_Open, FALSE);
	set(app, MUIA_Application_Sleep, FALSE);

	return(result);
}

//|
*/

//#ifdef BETA
//char beta_watermark_3[] = BETA_WATERMARK;
//#endif


/// StringSetup

static Object *StringRequesterWindow,     /* STRING REQUESTER */
			  *ST_Str_String,
			  *BT_Str_Ok,
			  *BT_Str_Cancel;


char StringSetup(struct StringRequest *req)
{

///   Create StringRequestWindow

//                 MUIA_Application_Window,
					 StringRequesterWindow = WindowObject,
						MUIA_Window_ID         , ID_WIN_STRINGREQUEST,
						MUIA_Window_ScreenTitle, ScreenTitle,
						WindowContents,
						   VGroup,

						   Child, HGroup,
								  GroupFrameT(MSG_STR_TITLE),
								  Child, ST_Str_String = MakeString(STR_REQ_MAXLEN, MSG_STR_STRING),
								  End,

						   Child, HGroup,
								  MUIA_Group_SameSize, TRUE,
								  Child, BT_Str_Ok     = TextButton(MSG_STR_OK),
								  Child, BT_Str_Cancel = TextButton(MSG_STR_CANCEL),
								  End,

						   End,
					  End;
//|


	if(!StringRequesterWindow)
	   return(FALSE);


	DoMethod(StringRequesterWindow   ,MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

	DoMethod(BT_Str_Ok       ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
	DoMethod(BT_Str_Cancel   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);


	if(req->WindowTitle)
	   set(StringRequesterWindow, MUIA_Window_Title, req->WindowTitle);
	else
	   set(StringRequesterWindow, MUIA_Window_Title, MSG_STR_TITLE);

	setstring(ST_Str_String, req->String);

	set(StringRequesterWindow, MUIA_Window_ActiveObject, ST_Str_String);


	DoMethod(app, OM_ADDMEMBER, StringRequesterWindow);

	return(TRUE);
}
//|
/// StringFinish
char StringFinish(struct StringRequest *req)
{
/*
** sprawdza poprawno�� wprowadzonych danych
** dot. drukowania. Zwraca FALSE, je�li co�
** jest nie tak, TRUE, je�li wszystkie
** elementy s� poprawne.
*/

	strncpy(req->String, getstr(ST_Str_String), req->MaxLen);

	return(TRUE);
}
//|
/// String

char StringRequest(struct StringRequest *req)
{
/*
** Requester dot. warto�ci numerycznych
** Zwraca BOOL w zale�no�ci czy u�ytkownik
** wybra� OK czy nie. Wynik umieszczany
** jest w odpowiednim polu struktury StringRequest
** w zale�no�ci od typu reqestera
*/

char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

	set(app, MUIA_Application_Sleep, TRUE);

	if(!StringSetup(req))
	   {
	   DisplayBeep(0);
	   MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
	   _sleep(FALSE);
	   return(FALSE);
	   }


	if(WinOpen(StringRequesterWindow))
	   {
	   while(running)
		  {
		  switch (DoMethod(app, MUIM_Application_Input, &signal))
			{
			case ID_OK:
			   result = StringFinish(req);
			   if(req->Flags == STR_REQ_NOZERO)
				   {
				   if(strlen(req->String) == 0)
					   result = FALSE;
				   }
			   running = FALSE;
			   break;

			case ID_CANCEL:
			   running = FALSE;
			   break;

			}
		  if(running && signal) Wait(signal);
		  }

	   set(StringRequesterWindow, MUIA_Window_Open, FALSE);
	   }
	else
	   {
	   DisplayBeep(0);
	   MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
	   }


	DoMethod(app, OM_REMMEMBER, StringRequesterWindow);
	MUI_DisposeObject(StringRequesterWindow);

	set(app, MUIA_Application_Sleep, FALSE);

	return(result);
}

//|

/// SORT

#define MSG_SORT_TITLE      "Sortowanie"
#define MSG_SORT_GTITLE     "Sortuj wed�ug"

#define MSG_SORT_RA_LP      "LP"
#define MSG_SORT_RA_NUMER   "Numer"
#define MSG_SORT_RA_DATA    "Data"

#define MSG_SORT_RA_DS_NAZWA  "Nazwa odbiorcy"
#define MSG_SORT_RA_DS_NIP    "NIP"
#define MSG_SORT_RA_DS_REGON  "Regon"
#define MSG_SORT_RA_DS_NUMER  "Numer dokumentu"
#define MSG_SORT_RA_DS_DATA   "Data wystawienia"


#define MSG_SORT_GTITLE2    "Porz�dek sortowania"
#define MSG_SORT_ZA         "O_dwrotny (Z->A)"

#define MSG_SORT_OK         "_Ok"
#define MSG_SORT_CANCEL     "Ponie_chaj"

//|
/// SortRequesterSetup

static Object *SortRequesterWindow,        /* SORT REQUESTER       */
			  *RA_Sort_Order,
			  *CH_Sort_Reverse,
			  *BT_Sort_Ok,
			  *BT_Sort_Cancel;


char *RA_Sort_Zakupy[] =  {MSG_SORT_RA_LP,
						   MSG_SORT_RA_NUMER,
						   MSG_SORT_RA_DATA,
						   NULL};

char *RA_Sort_DocScan[] = {MSG_SORT_RA_DS_NAZWA,
						   MSG_SORT_RA_DS_NIP,
						   MSG_SORT_RA_DS_REGON,
						   MSG_SORT_RA_DS_DATA,
						   MSG_SORT_RA_DS_NUMER,
						   NULL};

char *RA_Sort_DocScan_Od�o�one[] =
						  {MSG_SORT_RA_DS_NAZWA,
						   MSG_SORT_RA_DS_NIP,
						   MSG_SORT_RA_DS_REGON,
						   MSG_SORT_RA_DS_DATA,
						   NULL};



char SortRequesterSetup(char Typ, ULONG def)
{
APTR tablica[] = {RA_Sort_Zakupy, RA_Sort_DocScan, RA_Sort_DocScan_Od�o�one};



///      Create SortRequesterWindow

//                 MUIA_Application_Window,
					 SortRequesterWindow = WindowObject,
						MUIA_Window_ID         , ID_WIN_SORTREQUEST,
						MUIA_Window_ScreenTitle, ScreenTitle,
						MUIA_Window_Title, MSG_SORT_TITLE,
						WindowContents,
						   VGroup,

						   Child, HGroup,
								  GroupFrameT(MSG_SORT_GTITLE),
								  Child, HVSpace,
								  Child, RA_Sort_Order = RadioObject,
										 MUIA_Radio_Entries, tablica[Typ],
										 MUIA_CycleChain, TRUE,
										 MUIA_Radio_Active, def & 0x7fffffff,
										 End,
								  Child, HVSpace,
								  End,

						   Child, HGroup,
								  GroupFrameT(MSG_SORT_GTITLE2),
								  Child, HVSpace,
								  Child, MakeLabel2(MSG_SORT_ZA),
								  Child, CH_Sort_Reverse = MakeCheck(MSG_SORT_ZA),
								  Child, HVSpace,
								  End,

						   Child, HGroup,
								  MUIA_Group_SameSize, TRUE,
								  Child, BT_Sort_Ok     = TextButton(MSG_IDX_OK),
								  Child, BT_Sort_Cancel = TextButton(MSG_IDX_CANCEL),
								  End,

						   End,
					  End;
//|


	if(!SortRequesterWindow)
	   return(FALSE);

	DoMethod(SortRequesterWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
	DoMethod(BT_Sort_Ok         , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
	DoMethod(BT_Sort_Cancel     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

	set(CH_Sort_Reverse, MUIA_Selected, def & 0x80000000);

	set(SortRequesterWindow, MUIA_Window_ActiveObject , MUIV_Window_ActiveObject_None);
	set(SortRequesterWindow, MUIA_Window_DefaultObject, RA_Sort_Order);

	_attachwin(SortRequesterWindow);

	return(TRUE);
}
//|
/// SortRequesterFinish
int SortRequesterFinish(void)
{
/*
** zwraca ustawienia sortowania
*/

int   result = TRUE;


	get(RA_Sort_Order, MUIA_Radio_Active, &result);

	if(getcheckmark(CH_Sort_Reverse))
	   result |= 0x80000000;


	return(result);
}
//|
/// SortRequester


int SortRequester(char TypRequestera, ULONG def)
{
/*
** zwraca ustawienia sortowania, albo -1 jesli CANCEL
*/

char  running = TRUE;
ULONG signal  = 0;
int   result  = FALSE;


	_sleep(TRUE);

	if(!SortRequesterSetup(TypRequestera, def))
	   {
	   DisplayBeep(0);
	   MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
	   _sleep(FALSE);
	   return(-1);      // cancel
	   }

	if(WinOpen(SortRequesterWindow))
		{
		while(running)
		  {
		  long ID = DoMethod(app, MUIM_Application_Input, &signal);

		  switch(ID)
			{
			case ID_OK:
			   result = SortRequesterFinish();
			   running = FALSE;
			   break;

			case ID_CANCEL:
			   SortRequesterFinish();
			   result = -1L;
			   running = FALSE;
			   break;
			}
		  if(running && signal) Wait(signal);
		  }

		set(SortRequesterWindow, MUIA_Window_Open, FALSE);
		}
	 else
		{
		DisplayBeep(0);
		MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
		}


	_detachwin(SortRequesterWindow);

	_sleep(FALSE);

	return(result);
}

//|

/// DOC REQUESTER

#define MSG_DOC_TITLE           "Rodzaj dokumentu"
#define MSG_DOC_GTITLE          "Dost�pne"

#define MSG_DOC_RA_FVAT_PAR     "Faktura VAT"
#define MSG_DOC_RA_RACH_PAR     "Rachunek uproszczony"

#define MSG_DOC_RA_FVAT         "Faktura VAT"
#define MSG_DOC_RA_RACH         "Rachunek uproszczony"
#define MSG_DOC_RA_PARAGON      "Paragon"

#define MSG_DOC_OK              "_Ok"
#define MSG_DOC_CANCEL          "Ponie_chaj"

//|
/// DocRequesterSetup

static Object *DocRequesterWindow,        /* DOC TYPE REQUESTER    */
			  *RA_Doc_Order,
			  *CH_Doc_Reverse,
			  *BT_Doc_Ok,
			  *BT_Doc_Cancel;

char *RA_Doc_Paragon[] = {MSG_DOC_RA_FVAT_PAR,
//                          MSG_DOC_RA_RACH_PAR,
						  NULL};

char *RA_Doc_Zam�wienie[]={MSG_DOC_RA_FVAT,
//                           MSG_DOC_RA_RACH,
						   MSG_DOC_RA_PARAGON,
						   NULL};



char DocRequesterSetup(char Typ, ULONG def)
{
APTR tablica[] = {RA_Doc_Paragon, RA_Doc_Zam�wienie};


///   Create DocRequesterWindow

//                 MUIA_Application_Window,
					 DocRequesterWindow = WindowObject,
						MUIA_Window_ID         , ID_WIN_DOCREQUEST,
						MUIA_Window_ScreenTitle, ScreenTitle,
						MUIA_Window_Title, MSG_DOC_TITLE,
						WindowContents,
						   VGroup,

						   Child, HGroup,
								  GroupFrameT(MSG_DOC_GTITLE),
								  Child, HVSpace,
								  Child, RA_Doc_Order = RadioObject,
										 MUIA_Radio_Entries, tablica[Typ],
										 MUIA_CycleChain, TRUE,
										 MUIA_Radio_Active, def,
										 End,
								  Child, HVSpace,
								  End,

						   Child, HGroup,
								  MUIA_Group_SameSize, TRUE,
								  Child, BT_Doc_Ok     = TextButton(MSG_DOC_OK),
								  Child, BT_Doc_Cancel = TextButton(MSG_DOC_CANCEL),
								  End,

						   End,
					  End;
//|

	if(!DocRequesterWindow)
	   return(FALSE);

	DoMethod(DocRequesterWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
	DoMethod(BT_Doc_Ok         , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
	DoMethod(BT_Doc_Cancel     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

	set(DocRequesterWindow, MUIA_Window_ActiveObject , MUIV_Window_ActiveObject_None);
	set(DocRequesterWindow, MUIA_Window_DefaultObject, RA_Doc_Order);

	_attachwin(DocRequesterWindow);

	return(TRUE);
}
//|
/// DocRequesterFinish
int DocRequesterFinish(void)
{
/*
** zwraca numer dokumentu
*/

	return(xget(RA_Doc_Order, MUIA_Radio_Active));
}
//|
/// DocRequester

int DocRequester(char TypRequestera, ULONG def)
{
/*
** zwraca numer dokumentu wybranego, albo -1 jesli CANCEL
**
** zwraca rodzaj dokumentu, albo -1 jesli CANCEL
** TypRequestera:
**        0 - do PARAGONU
**        1 - do ZAMOWIENIA
*/

char  running = TRUE;
ULONG signal  = 0;
int   result  = FALSE;


	_sleep(TRUE);

	if(!DocRequesterSetup(TypRequestera, def))
	   {
	   DisplayBeep(0);
	   MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
	   _sleep(FALSE);
	   return(-1);      // cancel
	   }

	if(WinOpen(DocRequesterWindow))
		{
		while(running)
		  {
		  long ID = DoMethod(app, MUIM_Application_Input, &signal);

		  switch(ID)
			{
			case ID_OK:
			   result = DocRequesterFinish();
			   running = FALSE;
			   break;

			case ID_CANCEL:
			   result = -1L;
			   running = FALSE;
			   break;
			}
		  if(running && signal) Wait(signal);
		  }

		set(DocRequesterWindow, MUIA_Window_Open, FALSE);
		}
	 else
		{
		DisplayBeep(0);
		MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
		}


	_detachwin(DocRequesterWindow);

	_sleep(FALSE);

	return(result);
}

//|

/// MAGPRT REQUESTER

#define MSG_MAGPRT_WIN_TITLE   "Wydruki z magazynu"
#define MSG_MAGPRT_MODE        "_Rodzaj wydruku"

#define MSG_MAGPRT_TITLE_1  "Cennik detaliczny"
#define MSG_MAGPRT_TITLE_2  "Cennik hurtowy"
#define MSG_MAGPRT_TITLE_3  "Stan magazynowy"
#define MSG_MAGPRT_TITLE_4  "Spis z natury"
#define MSG_MAGPRT_TITLE_5  "Limity magazynowe"

#define MSG_MAGPRT_DRUKARKA "Dru_karka"
#define MSG_MAGPRT_CSV      "Plik C_SV"
#define MSG_MAGPRT_LIMIT    "Drukuj je�li stan _wi�kszy ni�"
#define MSG_MAGPRT_RABAT    "Drukuj z ra_batem hurtowym"
#define MSG_MAGPRT_RABAT2   "%"

#define MSG_MAGPRT_OK       "F10 - _Drukuj"
#define MSG_MAGPRT_CANCEL   "ESC - Ponie_chaj"

#define MSG_MAGPRT_OUT_1    "Wydrukuj na drukarce"
#define MSG_MAGPRT_OUT_2    "Zapisz do pliku CSV"

#define MSG_MAGPRT_RA_1     "Drukuj ca�y magazyn (grupy A->Z)"
#define MSG_MAGPRT_RA_2     "Drukuj ca�y magazyn (produkty A->Z)"
#define MSG_MAGPRT_RA_3     "Drukuj aktualnie wy�wietlon� list�"

#define MSG_MAGPRT_CO       "Uwzgl�dniaj _na wydruku"
#define MSG_MAGPRT_CY_1     "Towary i us�ugi"
#define MSG_MAGPRT_CY_2     "Tylko towary"
#define MSG_MAGPRT_CY_3     "Tylko us�ugi"

#define MSG_MAGPRT_NOOPT    "Brak opcji"
											
#define MSG_MAGPRT_MINMAX   "Sprawdz przekroczone _limity"
#define MSG_MAGPRT_MINMAX_1 "stan�w minimalnych"
#define MSG_MAGPRT_MINMAX_2 "stan�w maksymalnych"


 
//|
/// MagPrtRequesterSetup

char *CY_MagPrt_Opcje[]={
						  MSG_MAGPRT_TITLE_1,
						  MSG_MAGPRT_TITLE_2,
						  MSG_MAGPRT_TITLE_3,
						  MSG_MAGPRT_TITLE_4,
						  MSG_MAGPRT_TITLE_5,
						  NULL};

char *CY_MagPrt_CoDrukowac[]={
						  MSG_MAGPRT_CY_1,
						  MSG_MAGPRT_CY_2,
						  MSG_MAGPRT_CY_3,
						  NULL};

char *CY_MagPrt_OutputTable[] = {
						   MSG_MAGPRT_OUT_1,
//                           MSG_MAGPRT_OUT_2,
						   NULL
						   };

char *RA_MagPrt_PrintMode[] = {
						   MSG_MAGPRT_RA_1,
						   MSG_MAGPRT_RA_2,
						   MSG_MAGPRT_RA_3,
						   NULL
						   };

char *CY_MagPrt_MinMaxMode[] = {
							MSG_MAGPRT_MINMAX_1,
							MSG_MAGPRT_MINMAX_2,
							NULL
							};

static Object *MagPrtRequesterWindow,        /* MAGPRT TYPE REQUESTER    */
			  *CY_MagPrt_Mode,

			  *GR_MagPrt_Pages,

			  *GR_MagPrt_Output,
			  *TX_MagPrt_Drukarka,
			  *BT_MagPrt_Drukarka,
			  *AR_MagPrt_AslReq,

			  *RA_MagPrt_Detal_Mode,
			  *CY_MagPrt_Detal_Co,
			  *CH_MagPrt_Detal_LimitActive,
			  *ST_MagPrt_Detal_Limit,

			  *RA_MagPrt_Hurt_Mode,
			  *CY_MagPrt_Hurt_Co,
			  *CH_MagPrt_Hurt_LimitActive,
			  *ST_MagPrt_Hurt_Limit,
			  *ST_MagPrt_Hurt_Rabat,

			  *RA_MagPrt_Stan_Mode,
			  *CY_MagPrt_Stan_Co,
			  *CH_MagPrt_Stan_LimitActive,
			  *ST_MagPrt_Stan_Limit,

			  *RA_MagPrt_Spis_Mode,
			  *CY_MagPrt_Spis_Co,
			  *CH_MagPrt_Spis_LimitActive,
			  *ST_MagPrt_Spis_Limit,
			  
			  *RA_MagPrt_MinMax_Mode,
			  *CY_MagPrt_MinMax_Co,
			  *CY_MagPrt_MinMax_TypLimitu,

			  *BT_MagPrt_Ok,
			  *BT_MagPrt_Cancel;


char MagPrtRequesterSetup( struct MagPrintRequester *req )
{

///   Create MagPrtRequesterWindow

//                 MUIA_Application_Window,
					 MagPrtRequesterWindow = WindowObject,
						MUIA_Window_ID         , ID_WIN_MAGPRTREQUEST,
						MUIA_Window_ScreenTitle, ScreenTitle,
						MUIA_Window_Title, MSG_MAGPRT_WIN_TITLE,
						WindowContents,
						   VGroup,

						   Child, VGroup,

								  Child, HGroup,
//                                  GroupFrameT(MSG_MAGPRT_GTITLE),
									  Child, MakeLabel2( MSG_MAGPRT_MODE ),
									  Child, CY_MagPrt_Mode = MakeCycle( CY_MagPrt_Opcje, MSG_MAGPRT_MODE ),
								  End,

								  Child, GR_MagPrt_Pages = HGroup,
									  MUIA_Group_PageMode, TRUE,

///                                     Cennik detaliczny
								  Child, VGroup,
										 GroupFrameT( MSG_MAGPRT_TITLE_1 ),


										 Child, HGroup,
												Child, HVSpace,
												Child, RA_MagPrt_Detal_Mode = RadioObject,
														MUIA_Radio_Entries, RA_MagPrt_PrintMode,
														MUIA_CycleChain, TRUE,
														MUIA_Radio_Active, 0,
													   End,
												Child, HVSpace,
												End,


										 Child, HGroup,
												Child, MakeLabel2( MSG_MAGPRT_CO ),
												Child, CY_MagPrt_Detal_Co = MakeCycle( CY_MagPrt_CoDrukowac, MSG_MAGPRT_CO ),
												End,

										 Child, HGroup,
												Child, MakeLabel2( MSG_MAGPRT_LIMIT),
												Child, ST_MagPrt_Detal_Limit = MakeCashString( 10, MSG_MAGPRT_LIMIT ),
												Child, CH_MagPrt_Detal_LimitActive = MakeCheck( NULL ),
												End,


										 Child, HVSpace,
										 End,
//|
///                                     Cennik hurtowy
								  Child, VGroup,
										 GroupFrameT( MSG_MAGPRT_TITLE_2 ),

										 Child, HGroup,
												Child, HVSpace,
												Child, RA_MagPrt_Hurt_Mode = RadioObject,
														MUIA_Radio_Entries, RA_MagPrt_PrintMode,
														MUIA_CycleChain, TRUE,
														MUIA_Radio_Active, 0,
													   End,
												Child, HVSpace,
												End,

										 Child, HGroup,
												Child, MakeLabel2( MSG_MAGPRT_CO ),
												Child, CY_MagPrt_Hurt_Co = MakeCycle( CY_MagPrt_CoDrukowac, MSG_MAGPRT_CO ),
												End,

										 Child, ColGroup(2),
												Child, MakeLabel2( MSG_MAGPRT_LIMIT),
												Child, HGroup,
													   Child, ST_MagPrt_Hurt_Limit = MakeCashString( 10, MSG_MAGPRT_LIMIT ),
													   Child, CH_MagPrt_Hurt_LimitActive = MakeCheck( NULL ),
													   End,

												Child, MakeLabel2( MSG_MAGPRT_RABAT),
												Child, HGroup,
													   Child, ST_MagPrt_Hurt_Rabat = MakeCashString( 10, MSG_MAGPRT_RABAT ),
													   Child, MakeLabel2( MSG_MAGPRT_RABAT2),
													   End,
												End,


										 Child, HVSpace,
										 End,
//|
///                                     Stan magazynowy
								  Child, VGroup,
										 GroupFrameT( MSG_MAGPRT_TITLE_3 ),


										 Child, HGroup,
												Child, HVSpace,
												Child, RA_MagPrt_Stan_Mode = RadioObject,
														MUIA_Radio_Entries, RA_MagPrt_PrintMode,
														MUIA_CycleChain, TRUE,
														MUIA_Radio_Active, 0,
													   End,
												Child, HVSpace,
												End,

										 Child, HGroup,
												Child, MakeLabel2( MSG_MAGPRT_CO ),
												Child, CY_MagPrt_Stan_Co = MakeCycle( CY_MagPrt_CoDrukowac, MSG_MAGPRT_CO ),
												End,

										 Child, HGroup,
												Child, MakeLabel2( MSG_MAGPRT_LIMIT),
												Child, ST_MagPrt_Stan_Limit = MakeCashString( 10, MSG_MAGPRT_LIMIT ),
												Child, CH_MagPrt_Stan_LimitActive = MakeCheck( NULL ),
												End,


										 Child, HVSpace,
										 End,
//|
///                                     Spis z natury
								  Child, VGroup,
										 GroupFrameT( MSG_MAGPRT_TITLE_4 ),


										 Child, HGroup,
												Child, HVSpace,
												Child, RA_MagPrt_Spis_Mode = RadioObject,
														MUIA_Radio_Entries, RA_MagPrt_PrintMode,
														MUIA_CycleChain, TRUE,
														MUIA_Radio_Active, 0,
													   End,
												Child, HVSpace,
												End,

										 Child, HGroup,
												Child, MakeLabel2( MSG_MAGPRT_CO ),
												Child, CY_MagPrt_Spis_Co = MakeCycle( CY_MagPrt_CoDrukowac, MSG_MAGPRT_CO ),
												End,

										 Child, HGroup,
												Child, MakeLabel2( MSG_MAGPRT_LIMIT),
												Child, ST_MagPrt_Spis_Limit = MakeCashString( 10, MSG_MAGPRT_LIMIT ),
												Child, CH_MagPrt_Spis_LimitActive = MakeCheck( NULL ),
												End,

										 Child, HVSpace,
										 End,
//|
///                                     Lista MinMax (towary z przekroczonymi limitami)
								  Child, VGroup,
										 GroupFrameT( MSG_MAGPRT_TITLE_5 ),


										 Child, HGroup,
												Child, HVSpace,
												Child, RA_MagPrt_MinMax_Mode = RadioObject,
														MUIA_Radio_Entries, RA_MagPrt_PrintMode,
														MUIA_CycleChain, TRUE,
														MUIA_Radio_Active, 0,
													   End,
												Child, HVSpace,
												End,

										 Child, HGroup,
												Child, MakeLabel2( MSG_MAGPRT_CO ),
												Child, CY_MagPrt_MinMax_Co = MakeCycle( CY_MagPrt_CoDrukowac, MSG_MAGPRT_CO ),
												End,

										 Child, HGroup,
												Child, MakeLabel2( MSG_MAGPRT_MINMAX ),
												Child, CY_MagPrt_MinMax_TypLimitu = MakeCycle( CY_MagPrt_MinMaxMode, MSG_MAGPRT_MINMAX ),
												End,

										 Child, HVSpace,
										 End,
//|

									  End,
								  End,

///                                 Output

								  Child, GR_MagPrt_Output = HGroup,
															MUIA_Group_PageMode, TRUE,

															// drukarka
															Child, HGroup,
																   Child, MakeLabel2( MSG_MAGPRT_DRUKARKA ),
																   Child, HGroup,
																		  MUIA_Group_Spacing, 1,
																		  Child, TX_MagPrt_Drukarka = TextObject2, End,
																		  Child, BT_MagPrt_Drukarka = PopButton2( MUII_PopUp, MSG_MAGPRT_DRUKARKA ),
																		  End,
																   End,

															// CSV
															Child, HGroup,
																   Child, MakeLabel2( MSG_MAGPRT_CSV ),
																   Child, AR_MagPrt_AslReq = PopaslObject,
																			  MUIA_Group_Spacing, 1,
																			  MUIA_Popstring_String, MakeString( 256, MSG_MAGPRT_CSV ),
																			  MUIA_Popasl_Type, ASL_FileRequest,
																			  MUIA_Popstring_Button, PopButton(MUII_PopFile),
//                                                                              ASLFR_TitleText, "Please select a file...",

																			  End,
																   End,

															End,
//|

						   Child, HGroup,
								  MUIA_Group_SameSize, TRUE,
								  Child, BT_MagPrt_Ok     = TextButton(MSG_MAGPRT_OK),
								  Child, BT_MagPrt_Cancel = TextButton(MSG_MAGPRT_CANCEL),
								  End,

						   End,
					  End;
//|

	if( !MagPrtRequesterWindow )
	   return(FALSE);



	DoMethod(MagPrtRequesterWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

	DoMethod(BT_MagPrt_Drukarka   , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT);

	DoMethod(BT_MagPrt_Ok         , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
	DoMethod(BT_MagPrt_Cancel     , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

	DoMethod(CY_MagPrt_Mode, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime, GR_MagPrt_Pages, 3, MUIM_Set, MUIA_Group_ActivePage, MUIV_TriggerValue);

	DoMethod( CH_MagPrt_Detal_LimitActive, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_MagPrt_Detal_Limit, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue );
	DoMethod( CH_MagPrt_Hurt_LimitActive , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_MagPrt_Hurt_Limit, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue );
	DoMethod( CH_MagPrt_Stan_LimitActive , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_MagPrt_Stan_Limit, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue );
	DoMethod( CH_MagPrt_Spis_LimitActive , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_MagPrt_Spis_Limit, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue );

	set(MagPrtRequesterWindow, MUIA_Window_ActiveObject , CY_MagPrt_Mode);
	set(MagPrtRequesterWindow, MUIA_Window_DefaultObject, CY_MagPrt_Mode);

	DoMethod(MagPrtRequesterWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);



	// setting up

	setcycle( CY_MagPrt_Mode, req->Mode );
	setstring( AR_MagPrt_AslReq, req->CSVFile );

	{
	struct PrinterList *prt = Znajd�Drukark�ID( req->PrtID );

	if( !prt )
	   prt = Znajd�Drukark�ID( DEFAULT_PRINTER );

	settext( TX_MagPrt_Drukarka, prt->pl_printer.Name );
	}



	// w spisie z natury nie mozna zmieniac co drukujemy -> zawsze produkty
	set( ST_MagPrt_Detal_Limit, MUIA_Disabled, !req->detal_LimitAktywny );
	set( ST_MagPrt_Hurt_Limit, MUIA_Disabled, !req->hurt_LimitAktywny );
	set( ST_MagPrt_Stan_Limit, MUIA_Disabled, !req->stan_LimitAktywny );
	set( ST_MagPrt_Spis_Limit, MUIA_Disabled, !req->spis_LimitAktywny );


	// cennik detaliczny

	setradio( RA_MagPrt_Detal_Mode, req->detal_CoDrukowa� );
	setcheckmark( CH_MagPrt_Detal_LimitActive, req->detal_LimitAktywny );
	setcycle( CY_MagPrt_Detal_Co, req->detal_ProdCzyUslugi );
	setstring( ST_MagPrt_Detal_Limit, Price2String( req->detal_Limit ) );


	// cennik hurtowy

	setradio( RA_MagPrt_Hurt_Mode, req->hurt_CoDrukowa� );
	setcheckmark( CH_MagPrt_Hurt_LimitActive, req->hurt_LimitAktywny );
	setcycle( CY_MagPrt_Hurt_Co, req->hurt_ProdCzyUslugi );
	setstring( ST_MagPrt_Hurt_Limit, Price2String( req->hurt_Limit ) );
	setstring( ST_MagPrt_Hurt_Rabat, Price2String( req->hurt_Rabat ) );


	// stany magazynowe

	setradio( RA_MagPrt_Stan_Mode, req->stan_CoDrukowa� );
	setcheckmark( CH_MagPrt_Stan_LimitActive, req->stan_LimitAktywny );
	setcycle( CY_MagPrt_Stan_Co, req->stan_ProdCzyUslugi );
	setstring( ST_MagPrt_Stan_Limit, Price2String( req->stan_Limit ) );


	// spis z natury
	_disable( CY_MagPrt_Spis_Co );

	setradio( RA_MagPrt_Spis_Mode, req->spis_CoDrukowa� );
//    setcycle( CY_MagPrt_Spis_Co, req->stan_ProdCzyUslugi );
	setcycle( CY_MagPrt_Spis_Co, 1 ); // zawsze produkty
	setstring( ST_MagPrt_Spis_Limit, Price2String( req->spis_Limit ) );
	setcheckmark( CH_MagPrt_Spis_LimitActive, req->spis_LimitAktywny );

 
	// w limitach nie mozna zmieniac co drukujemy -> zawsze produkty
	_disable( CY_MagPrt_MinMax_Co );
	setcycle( CY_MagPrt_MinMax_Co, 1 );

 
	_attachwin(MagPrtRequesterWindow);

	return(TRUE);
}
//|
/// MagPrtRequesterFinish
char MagPrtRequesterFinish( struct MagPrintRequester *req )
{
/*
** zwraca co to luzer wybral
*/

	if( xget(AR_MagPrt_AslReq, MUIA_Popasl_Active) )
	   {
	   // ASLka otwarta jeszcze
	   DisplayBeep(0);
	   return(FALSE);
	   }


	switch( getcycle( CY_MagPrt_Mode ) )
	   {
	   case 0: // detal
		   {
		   double ilo�� = String2Price( getstr(ST_MagPrt_Detal_Limit ) );

		   if( ilo�� < 0 )
			   {
			   DisplayBeep(0);
			   set( MagPrtRequesterWindow, MUIA_Window_ActiveObject, ST_MagPrt_Detal_Limit );
			   return(FALSE);
			   }
		   }
		   break;

	   case 1: // hurt
		   {
		   double ilo�� = String2Price( getstr(ST_MagPrt_Hurt_Limit ) );
		   double rabat = String2Price( getstr(ST_MagPrt_Hurt_Rabat ) );


		   if( ilo�� < 0 )
			   {
			   set( MagPrtRequesterWindow, MUIA_Window_ActiveObject, ST_MagPrt_Hurt_Limit );
			   return(FALSE);
			   }

		   if( (rabat < 0) || (rabat > 100) )
			   {
			   set( MagPrtRequesterWindow, MUIA_Window_ActiveObject, ST_MagPrt_Hurt_Rabat );
			   return(FALSE);
			   }
		   }
		   break;

	   case 2: // stany
		   {
		   double ilo�� = String2Price( getstr(ST_MagPrt_Stan_Limit ) );

		   if( ilo�� < 0 )
			   {
			   DisplayBeep(0);
			   set( MagPrtRequesterWindow, MUIA_Window_ActiveObject, ST_MagPrt_Stan_Limit );
			   return(FALSE);
			   }
		   }
		   break;


	   case 3: // spis z natury
		   {
		   double ilo�� = String2Price( getstr(ST_MagPrt_Spis_Limit ) );

		   if( ilo�� < 0 )
			   {
			   DisplayBeep(0);
			   set( MagPrtRequesterWindow, MUIA_Window_ActiveObject, ST_MagPrt_Spis_Limit );
			   return(FALSE);
			   }
		   }
		   break;
	   }


	// ok, copy back

	req->Mode   = getcycle( CY_MagPrt_Mode );
	copystr( req->CSVFile, AR_MagPrt_AslReq );

	{
	struct PrinterList *prt = Znajd�Drukark�Nazwa( gettext( TX_MagPrt_Drukarka ) );
	if(prt)
		req->PrtID = prt->pl_printer.ID;
	else
	   req->PrtID = DEFAULT_PRINTER;
	}

	// detal

	req->detal_CoDrukowa� = getradio( RA_MagPrt_Detal_Mode );
	req->detal_LimitAktywny = getcheckmark( CH_MagPrt_Detal_LimitActive );
	req->detal_Limit = String2Price( getstr( ST_MagPrt_Detal_Limit ) );
	req->detal_ProdCzyUslugi = getcycle( CY_MagPrt_Detal_Co );


	// cennik hurtowy

	req->hurt_CoDrukowa� = getradio( RA_MagPrt_Hurt_Mode );
	req->hurt_LimitAktywny = getcheckmark( CH_MagPrt_Hurt_LimitActive );
	req->hurt_Limit = String2Price( getstr( ST_MagPrt_Hurt_Limit ) );
	req->hurt_Rabat = String2Price( getstr( ST_MagPrt_Hurt_Rabat ) );
	req->hurt_ProdCzyUslugi = getcycle( CY_MagPrt_Hurt_Co );


	// stany magazynowe

	req->stan_CoDrukowa� = getradio( RA_MagPrt_Stan_Mode );
	req->stan_LimitAktywny = getcheckmark( CH_MagPrt_Stan_LimitActive );
	req->stan_Limit = String2Price( getstr( ST_MagPrt_Stan_Limit ) );
	req->stan_ProdCzyUslugi = getcycle( CY_MagPrt_Stan_Co );


	// spis z natury

	req->spis_CoDrukowa� = getradio( RA_MagPrt_Spis_Mode );
	req->spis_LimitAktywny = getcheckmark( CH_MagPrt_Spis_LimitActive );
	req->spis_Limit = String2Price( getstr( ST_MagPrt_Spis_Limit ) );

//    req->spis_ProdCzyUslugi = getcycle( CY_MagPrt_Spis_Co );
	req->spis_ProdCzyUslugi = 1; // zawsze produkty (na spisie uslug nie ma co)

	// limity
	req->minmax_CoDrukowa� = getradio( RA_MagPrt_MinMax_Mode );
	req->minmax_ProdCzyUslugi = 1; // zawsze produkty (uslugi nie maja limitow)
	req->minmax_TypLimitu = getcycle( CY_MagPrt_MinMax_TypLimitu );

	return( TRUE );
}
//|
/// MagPrtRequester

int MagPrtRequester( struct MagPrintRequester *requester )
{
/*
** zwraca numer dokumentu wybranego, albo -1 jesli CANCEL
*/

char  running = TRUE;
ULONG signal  = 0;
int   result  = FALSE;


	_sleep(TRUE);

	if( !MagPrtRequesterSetup( requester ) )
	   {
	   DisplayBeep(0);
	   MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
	   _sleep(FALSE);
	   return(-1);      // cancel
	   }

	if(WinOpen(MagPrtRequesterWindow))
		{
		while(running)
		  {
		  long ID = DoMethod(app, MUIM_Application_Input, &signal);

		  switch(ID)
			{
			case ID_SELECT:
				 {
				 struct PrinterList *prt = DrukarkaSelector( NULL );

				 if( prt )
				   {
				   settext( TX_MagPrt_Drukarka, prt->pl_printer.Name );
				   }
				 }
			   break;

			case ID_OK:
			   if( MagPrtRequesterFinish( requester ) == FALSE )
				   {
				   DisplayBeep(0);
				   break;
				   }

			   result = TRUE;
			   running = FALSE;
			   break;

			case ID_CANCEL:
			   if( xget(AR_MagPrt_AslReq, MUIA_Popasl_Active) )
				  {
				  // ASLka otwarta jeszcze
				  DisplayBeep(0);
				  break;
				  }

			   result = FALSE;
			   running = FALSE;
			   break;
			}
		  if(running && signal) Wait(signal);
		  }

		set(MagPrtRequesterWindow, MUIA_Window_Open, FALSE);
		}
	 else
		{
		DisplayBeep(0);
		MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
		}


	_detachwin( MagPrtRequesterWindow );

	_sleep(FALSE);

	return(result);
}

//|

