
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 


/*** includes ***/

#include <clib/alib_protos.h>
#include <clib/graphics_protos.h>
#include <clib/utility_protos.h>
#include <clib/muimaster_protos.h>
#include <pragmas/muimaster_pragmas.h>
#include <libraries/mui.h>

#if MUIMASTER_VLATEST <= 14
#include <mui/mui33_mcc.h>
#endif

#include <mui/muiundoc.h>
#include <mui/interlist.h>
#include <mui/nlist_mcc.h>
#include "golem_structs.h"
#include "golem_paylist_mcc.h"

#include <stdio.h>
#include <string.h>

#define __NAME      "Golem_PayList"
#define CLASS       MUIC_Golem_PayList
#define SUPERCLASS  _MUIC_List

void _XCEXIT(int code) {}


struct Data
{
	LONG  Dummy;
};


#include "golem_paylist_revision.h"

#define UserLibID PAYLIST_VERSTAG " � 1998-2002 Marcin Orlowski <carlos@wfmh.org.pl>"
#define MASTERVERSION 14

#define MYDEBUG 0

#define VERSION  PAYLIST_VERSION
#define REVISION PAYLIST_REVISION

#define MCC_USES_LOCALE
#include "golem_mccheader_nlist.c"


/// LISTA RODZAJ�W P�ATNO��

#define MSG_PAY_NAME  "\0338\033bNazwa"
#define MSG_PAY_DELAY "\0338\033bZw�oka"

//|


/// Pay list hooks
/// HOOK: PayList_Display

void * __saveds __asm PayList_Display(register __a2 char **array, register __a1 struct P�atno��List *node)
{
static char Del[6],
			Delay[15];

   if(node)
	 {
	 struct P�atno�� *p�at = &node->pl_p�atno��;

	 sprintf(Del   , "%s" , ((node->Deleted==FALSE) ? " " : "*"));

	 *array++ = Del;
	 *array++ = p�at->Nazwa;

	 if(p�at->Got�wka)
	   strcpy(Delay, "---");
	 else
	   sprintf(Delay, "%ld", p�at->Zw�oka);
	 *array++ = Delay;
	 }
   else
	 {
	 *array++ = "";
	 *array++ = MSG_PAY_NAME;
	 *array++ = MSG_PAY_DELAY;
	 }


	 return(0);
}

//|
/// HOOK: PayList_CompareStr
LONG __saveds __asm PayList_CompareStr(register __a1 struct P�atno��List *Node1, register __a2 struct P�atno��List *Node2)
{
	return((LONG)StrnCmp(Locale, Node1->pl_p�atno��.Nazwa, Node2->pl_p�atno��.Nazwa, -1, SC_COLLATE2));
}
//|
//|

/// OM_NEW

ULONG PayList_New(struct IClass *cl, Object *obj, struct opSet *msg)
{
static const struct Hook PayList_DisplayHook     = { {NULL, NULL}, (VOID *)PayList_Display   ,NULL,NULL };
static const struct Hook PayList_CompareHookStr  = { {NULL, NULL}, (VOID *)PayList_CompareStr,NULL,NULL };

	obj = (Object *)DoSuperNew(cl,obj,
					InputListFrame,
					_MUIA_List_AutoVisible  , TRUE,
					_MUIA_List_DisplayHook  , &PayList_DisplayHook,
					_MUIA_List_CompareHook  , &PayList_CompareHookStr,
					_MUIA_List_Format       , "MIW=1 MAW=-1 BAR,"
											  "MIW=1 MAW=80 BAR,"
											  "MIW=1 MAW=-1 P=\033r ",

					_MUIA_List_Title        , TRUE,

					 MUIA_NList_TitleSeparator, TRUE,
					 MUIA_NList_AutoCopyToClip, TRUE,

				TAG_MORE, msg->ops_AttrList);


	  if(obj)
		  {
//          struct PayList_Data *data = INST_DATA(cl,obj);

		  msg->MethodID = OM_SET;
		  DoMethodA(obj, (Msg)msg);
		  msg->MethodID = OM_NEW;
		  }


	  return((ULONG)obj);
}

//|
/// OM_GET
static ULONG ASM PayList_Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ProductList_Data *data = INST_DATA(cl,obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

	switch(((struct opGet *)msg)->opg_AttrID)
	   {
	   case MUIA_Version:
			*store = PAYLIST_VERSION;
			return(TRUE);
			break;

	   case MUIA_Revision:
			*store = PAYLIST_REVISION;
			return(TRUE);
			break;

	   }

	return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher

ULONG ASM SAVEDS _Dispatcher(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{

	switch (msg->MethodID)
	   {
	   case OM_NEW:   return( PayList_New  (cl, obj, (APTR)msg));
	   case OM_GET:   return( PayList_Get  (cl, obj, (APTR)msg));
	   }

	return( DoSuperMethodA( cl, obj, msg) );
}

//|

