
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
$Id: golem_klienci.c,v 1.1 2003/01/01 20:40:53 carl-os Exp $.
*/

#include "golem.h"

struct Filtr cust_filter =
{
        FALSE,
        "#?",                             // Nazwa

        1,
        1,
        1,
        1,
        1,
        1
};


/// CUST STOP CHUNKS
#define CUST_NUM_STOPS (sizeof(Cust_Stops) / (2 * sizeof(ULONG)))

STATIC LONG Cust_Stops[] =
{
                ID_CUST, ID_CAT,
                ID_CUST, ID_VERS,
                ID_CUST, ID_COUN,
                ID_CUST, ID_CUST,  // struct Klient

                ID_CUST, ID_PAY,   // def_Payment
                ID_CUST, ID_NOTE,  // Uwagi
                ID_CUST, ID_MEMO,  // komentarz
                NULL, NULL,

};
//|

/// IsNIPValid

/*
** sprawdza poprawno�� wprowadzonego NIPu
*/

char IsNIPValid( char *NIP )
{
char result = FALSE;

        if( NIP[0] != 0)
           {
           char len = strlen(NIP);

           if( (len >= 10) || (len <= 13) )
                   {
                   char digits = 0;
                   char dash   = 0;
                   int  i;

                   for( i=0 ;i<len; i++)
                           {
                           if( isdigit( NIP[i] ) )
                                   digits++;

                           if( NIP[i] == '-' )
                                   dash++;
                           }

                   if( ((digits==10) && (dash==3)) || ((digits==10) && (dash==0)) )
                           result = TRUE;

                   }
           }

        return( result );
}
//|

/// DodajKlienta

int DodajKlienta(struct KlientList *klient)
{
/*
** Dodaje klienta to listy klient�w -> sortuje alfabetycznie
*/

struct KlientList *node;
int i = 0;

//    NewList(&klient->kl_odbiorcy);


        if(!IsListEmpty(&kontrahenci))
           {
           for(node = (struct KlientList *)kontrahenci.lh_Head; node->kl_node.mln_Succ; node = (struct KlientList *)node->kl_node.mln_Succ, i++)
                   {
                   if( klient->kl_klient.Separator == FALSE )
                           {
                           if(StrnCmp(MyLocale, klient->kl_klient.Nazwa1, node->kl_klient.Nazwa1, -1, SC_COLLATE2) < 0)
                                   {
                                   if((struct Node *)node->kl_node.mln_Pred)
                                           Insert(&kontrahenci, (struct Node *)klient, (struct Node *)node->kl_node.mln_Pred);
                                   else
                                           AddHead(&kontrahenci, (struct Node *)klient);

                                   return(i);
                                   }
                           }
                   else
                           {
                           i++;
                           }   
                   }
           }

        AddTail(&kontrahenci, (struct Node *)klient);
        return(MUIV_List_Insert_Bottom);
}
//|
/// Znajd�KlientaNazwa
struct KlientList *Znajd�KlientaNazwa(char *Nazwa)
{
/*
** Przeszukuj� list� klient�w w poszukiwaniu klienta
** o podanej nazwie
*/

struct KlientList *node;

        if(!IsListEmpty(&kontrahenci))
           {
           for(node = (struct KlientList *)kontrahenci.lh_Head; node->kl_node.mln_Succ; node = (struct KlientList *)node->kl_node.mln_Succ)
//       if(!StrnCmp(MyLocale, Nazwa, node->kl_klient.Nazwa1, -1, SC_COLLATE2))
           if(!stricmp(Nazwa, node->kl_klient.Nazwa1))
                   return(node);
           }

        return(NULL);
}
//|
/// Usu�Klient�w
int Usu�Klient�w(char Flags)
{
/*
** Usuwa skasowanych klient�w (je�li DeleteAll=FALSE)
** albo ca�� list� klient�w (je�li DeleteAll=TRUE)
**
** zwraca liczbe usuni�tych rekord�w
*/

struct KlientList   *klient, *kl_next;
char   DeleteAll = ( Flags == UK_REMOVE_ALL );
char   KeepTemp  = ( Flags == UK_KEEP_TEMP );
int    removed = 0;

        if(!IsListEmpty(&kontrahenci))
           {
           char Delete = FALSE;

           for(klient = (struct KlientList *)kontrahenci.lh_Head; klient->kl_node.mln_Succ; )
                   {
                   Delete = DeleteAll;

                   if(DeleteAll == FALSE)
                           Delete = klient->Deleted;

                   if( Delete && KeepTemp )
                           if( klient->Tymczasowy )
                                   Delete = FALSE;

                   kl_next = (struct KlientList *)klient->kl_node.mln_Succ;


                   if(Delete)
                           {
                           Remove( (struct Node*)klient );

                           if( klient->kl_klientExt.Komentarz )
                                   free( klient->kl_klientExt.Komentarz );

                           if( klient->kl_klientExt.Uwagi )
                                   free( klient->kl_klientExt.Uwagi );

                           free(klient);
                           removed++;
                           }

                   klient = kl_next;
                   }
           }

        return( removed );
}
//|

/// WczytajKlient�w

char WczytajKlient�w(void)
{

struct IFFHandle *iff;
struct ContextNode *cn;
long   _RC = 0;
char   ValidFile = FALSE;
int    Current = 0;
int    CurrentGauge = 1;

int    Errors = 0;
int    Error_Products = 0;
int    Error_Duplicated_Groups = 0;

#define MODULO 5

         set(app, MUIA_Application_Sleep, TRUE);

         set(GA_Startup_Info, MUIA_Gauge_InfoText, MSG_STARTUP_CUSTOMERS);
         set(GA_Startup_Info, MUIA_Gauge_Max, WczytajCount(KlienciCountFileName)/MODULO);
         set(GA_Startup_Info, MUIA_Gauge_Current, 0);

         Usu�Klient�w( UK_REMOVE_ALL );

         if(iff = AllocIFF())
           {
           if(iff->iff_Stream = Open(KlienciFileName, MODE_OLDFILE))
                   {
                   InitIFFasDOS(iff);

                   StopChunks(iff, Cust_Stops, CUST_NUM_STOPS);
                   StopOnExit(iff, ID_CUST, ID_FORM);

                   if(!OpenIFF(iff, IFFF_READ))
                           {
                           struct KlientList *klient = NULL;

                           while(TRUE)
                                  {
                                  _RC = ParseIFF(iff, IFFPARSE_SCAN);

                                  if(!((_RC >= 0) || (_RC == IFFERR_EOC)))
                                         break;

                                  if(cn = CurrentChunk(iff))
                                         {


                                         LONG ID = cn->cn_ID;

                                         if(!ValidFile)
                                                {
                                                if((ID == ID_CAT) && (cn->cn_Type == ID_CUST))
                                                   {
                                                   ValidFile = TRUE;
                                                   continue;
                                                   }

                                                break;
                                                }

///                       ID_VERS
                                         if(ID == ID_VERS)
                                                {
                                                struct BaseVersion version;

                                                if(ReadChunkBytes(iff, &version, cn->cn_Size) == cn->cn_Size)
                                                   {

                                                   }
                                                else
                                                   {
                                                   _RC = IoErr();
                                                   Errors++;
                                                   break;
                                                   }

                                                continue;
                                                }
//|
///                       ID_CUST

                                         if(ID == ID_CUST)
                                                {
                                                char   Failed = FALSE;
                                                klient = calloc(1, sizeof(struct KlientList));

//                        D(bug(" CUST: calloc() = 0x%lx\n", klient));

                                                if(klient)
                                                   {
                                                   if(ReadChunkBytes(iff, &klient->kl_klient, sizeof(struct Klient)) != cn->cn_Size)
                                                         {
                                                         _RC = IoErr();
                                                         Failed = TRUE;
                                                         Errors++;
                                                         D(bug("Nie moge wczytac klienta! Zla dlugosc chunka!\n"));
                                                         break;
                                                         }

                                                   DodajKlienta(klient);
/*
                                                   if(!(Znajd�Grup�ID(grupa->gm_grupa.ID)))
                                                          {
                                                          DodajGrup�(grupa);
                                                          grupa_aktywna = grupa;
                                                          }
                                                   else
                                                          {
                                                          Error_Duplicated_Groups++;
                                                          }
*/

                                                   Current++;
                                                   if((Current%MODULO) == 0)
                                                         {
                                                         set(GA_Startup_Info, MUIA_Gauge_Current, CurrentGauge++);
//                             D(bug("%ld\n", CurrentGauge));
                                                         }

                                                   }
                                                else
                                                   {
                                                   D(bug("** can't malloc() mem for new Klient\n"));
                                                   }
                                                continue;
                                                }
//|
///                       ID_PAY

                                         if(ID == ID_PAY)
                                                {
                                                D(bug("ID_PAY found (klient: %08lx\n", klient));
                                                if( klient )
                                                   {
                                                   if( ReadChunkBytes(iff, &klient->kl_klientExt.def_Payment, PAY_NAME_LEN) != cn->cn_Size )
                                                         {
                                                         _RC = IoErr();
                                                         Errors++;
                                                         D(bug("Nie moge wczytac def_Payment! Zla dlugosc chunka!\n"));
                                                         break;
                                                         }
                                                   }
                                                continue;
                                                }
//|
///                       ID_NOTE

                                         if(ID == ID_NOTE)
                                                {
                                                D(bug("ID_NOTE found (klient: %08lx)\n", klient));

                                                if( klient )
                                                   {
                                                   char *uwagi = calloc(1, CUST_NOTE_LEN);

                                                   if( uwagi )
                                                           {
                                                           if( ReadChunkBytes(iff, uwagi, min(cn->cn_Size, CUST_NOTE_LEN) ) != cn->cn_Size )
                                                                 {
                                                                 free( uwagi );

                                                                 _RC = IoErr();
                                                                 Errors++;
                                                                 D(bug("Nie moge wczytac ID_NOTE! Zla dlugosc chunka!\n"));
                                                                 break;
                                                                 }
                                                           else
                                                                 {
                                                                 // ok
                                                                 klient->kl_klientExt.Uwagi = uwagi;
                                                                 }
                                                           }
                                                   else
                                                           {
                                                           Errors++;
                                                           D(bug("Nie moge zaalokowac pamieci na ID_NOTE!\n"));
                                                           }
                                                   }
                                                continue;
                                                }
//|

                                         }
                                  }

                           CloseIFF(iff);
                           }

                        Close(iff->iff_Stream);
                        }
                 else
                   {
                   D(bug("Nie moge otworzyc pliku '%s' do odczytu\n", KlienciFileName));
                   }

//       if(_RC == IFFERR_EOF) Error = 0;

//       if(((Errors!=0 || ValidFile!=TRUE) && (msg->Quiet==FALSE)))
//             MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_EDIT_AUTHOR_READ_ERROR);

           FreeIFF(iff);
           }

           UpdateSeparators( NULL );

           set(GA_Startup_Info, MUIA_Gauge_Current, 0);
           set(app, MUIA_Application_Sleep, FALSE);

           return(0);
}

//|
/// ZapiszKlient�w


char ZapiszKlient�w(void)
{
struct IFFHandle *MyIFFHandle;
int    Count = 0;


        set(app, MUIA_Application_Sleep, TRUE);

        if(MyIFFHandle = AllocIFF())
                {
                BPTR  FileHandle;

                if(FileHandle = Open(KlienciFileName, MODE_NEWFILE))
                   {
                   MyIFFHandle->iff_Stream = FileHandle;
                   InitIFFasDOS(MyIFFHandle);

                   if(OpenIFF(MyIFFHandle, IFFF_WRITE) == 0)
                           {
                           struct BaseVersion version;
                           struct KlientList *klient;


                           PushChunk(MyIFFHandle, ID_CUST, ID_CAT, IFFSIZE_UNKNOWN);

                           PushChunk(MyIFFHandle, ID_CUST, ID_FORM, IFFSIZE_UNKNOWN);
                                   PushChunk(MyIFFHandle, ID_CUST, ID_VERS, IFFSIZE_UNKNOWN);
                                   version.Version = VERSION;
                                   version.Revision = REVISION;
                                   WriteChunkBytes(MyIFFHandle, &version, sizeof(version));
                                   PopChunk(MyIFFHandle);

/*
                                   PushChunk(MyIFFHandle, ID_CUST, ID_ANNO, IFFSIZE_UNKNOWN);
                                   WriteChunkBytes(MyIFFHandle, ScreenTitle, ScreenTitleLen-1);
                                   WriteChunkBytes(MyIFFHandle, " <", 2);
                                   WriteChunkBytes(MyIFFHandle, EMAIL, strlen(EMAIL));
                                   WriteChunkBytes(MyIFFHandle, ">, ", 3);
                                   WriteChunkBytes(MyIFFHandle, WWW, strlen(WWW));
                                   PopChunk(MyIFFHandle);
*/
                           PopChunk(MyIFFHandle);


                           for(klient = (struct KlientList *)kontrahenci.lh_Head; klient->kl_node.mln_Succ; klient = (struct KlientList *)klient->kl_node.mln_Succ)
                                   {
                                   struct OdbiorcaList *odbiorca;

                                   if( (klient->kl_klient.Separator == FALSE) &&
                                           (klient->Tymczasowy == FALSE)
                                          )
                                           {
                                           PushChunk(MyIFFHandle, ID_CUST, ID_FORM, IFFSIZE_UNKNOWN);

                                           // main hunk
                                           PushChunk(MyIFFHandle, ID_CUST, ID_CUST, IFFSIZE_UNKNOWN);
                                           WriteChunkBytes(MyIFFHandle, &klient->kl_klient, sizeof(struct Klient));
                                           PopChunk(MyIFFHandle);


                                           // new hunks (since v0.90)

                                           if( klient->kl_klientExt.def_Payment[0] != 0 )
                                                   {
                                                   PushChunk(MyIFFHandle, ID_CUST, ID_PAY, IFFSIZE_UNKNOWN);
                                                   WriteChunkBytes( MyIFFHandle, klient->kl_klientExt.def_Payment, PAY_NAME_LEN);
                                                   PopChunk(MyIFFHandle);
                                                   }

                                           if( klient->kl_klientExt.Uwagi )
                                                   {
                                                   PushChunk(MyIFFHandle, ID_CUST, ID_NOTE, IFFSIZE_UNKNOWN);
                                                   WriteChunkBytes( MyIFFHandle, klient->kl_klientExt.Uwagi, strlen( klient->kl_klientExt.Uwagi ) );
                                                   PopChunk(MyIFFHandle);
                                                   }

                                           if( klient->kl_klientExt.Komentarz )
                                                   {
                                                   PushChunk(MyIFFHandle, ID_CUST, ID_MEMO, IFFSIZE_UNKNOWN);
                                                   WriteChunkBytes( MyIFFHandle, klient->kl_klientExt.Komentarz, strlen( klient->kl_klientExt.Komentarz ) );
                                                   PopChunk(MyIFFHandle);
                                                   }

/*
                                   if(!(IsListEmpty(&klient->kl_odbiorcy)))
                                           {
                                           for(odbiorca = (struct OdbiorcaList *)klient->kl_odbiorcy.lh_Head; odbiorca->ol_node.mln_Succ; odbiorca = (struct OdbiorcaList *)odbiorca->ol_node.mln_Succ)
                                                  {
                                                  PushChunk(MyIFFHandle, ID_CUST, ID_RECE, IFFSIZE_UNKNOWN);
                                                  WriteChunkBytes(MyIFFHandle, &odbiorca->ol_odbiorca, sizeof(struct Odbiorca));
                                                  PopChunk(MyIFFHandle);
                                                  }
                                           }
*/

                                           PopChunk(MyIFFHandle);

                                           Count++;
                                           }
                                   }

                           PopChunk(MyIFFHandle);
                           CloseIFF(MyIFFHandle);

                           ZapiszCount(KlienciCountFileName, Count);
                           }
                   else
                           {
                           DisplayBeep(0);
                           D(bug("*** OpenIFF() nie powiod�o si�\n"));
                           }

                   Close(FileHandle);
                   }
                else
                   {
//           MUI_Request((Object *)xget(obj, MUIA_ApplicationObject), (Object *)xget(obj, MUIA_WindowObject), 0, TITLE, MSG_OK, MSG_WRITE_ERROR);
                   D(bug("*** Nie mog� otworzy� pliku do zapisu \"%s\"\n", KlienciFileName));
                   }

                FreeIFF(MyIFFHandle);
                }
         else
                {
                DisplayBeep(0);
                D(bug("*** Nie mog� zaalokowa� struktury IFFHandle\n"));
                }

        set(app, MUIA_Application_Sleep, FALSE);

        return(0);
}



//|

/// Usu�Separatory
//void Usu�Separatory(char DeleteAll)
void Usu�Separatory( void )
{
/*
** Usuwa separatory (SEP_SEP) z listy klient�w
*/

struct KlientList   *klient, *kl_next;

        if(!IsListEmpty(&kontrahenci))
           {
           char Delete = FALSE;

           for(klient = (struct KlientList *)kontrahenci.lh_Head; klient->kl_node.mln_Succ; )
                   {
//           Delete = DeleteAll;
                   Delete = FALSE;

//           if(DeleteAll == FALSE)
//               Delete = klient->Deleted;

                   Delete = klient->kl_klient.Separator;

                   kl_next = (struct KlientList *)klient->kl_node.mln_Succ;

                   if(Delete)
                           {
                           Remove((struct Node*)klient);
                           free(klient);
                           }

                   klient = kl_next;
                   }
           }
}
//|
/// UpdateSeparators
char UpdateSeparators( struct Filtr *filtr )
{
/*
** ustawia separatory (do pozniejszego wyswietlnia (Nlist)
** Lista klientow MUSI byc uprzednio posortowana alfabetycznie!
**
** Filtr wy�wietlania klient�w. jako filtr mozna podac NULL,
** albo poprawna strukture Filtr
*/

static char pattern[(FILTR_NAME_LEN*2)+2];

struct KlientList *node;
char   last_letter = 0;
char   was_digit = FALSE;
char   current;
char   separator;


           if(IsListEmpty(&kontrahenci))
                   return( FALSE );


           // wywalamy stare separatory
           Usu�Separatory();


           if(wfmh_isenv("DontPlaceSeparators"))
                   return( FALSE );


           // i do dziela...

           for( node = (struct KlientList *)kontrahenci.lh_Head; node->kl_node.mln_Succ; node = (struct KlientList *)node->kl_node.mln_Succ )
                   {
                   current   = toupper((int)node->kl_klient.Nazwa1[0]);
                   separator = FALSE;

                   if( node->kl_klient.Separator == FALSE )
                                   {
                                   if( current != last_letter )
                                           {
                                           separator = TRUE;

                                           // grupujemy wszystkie cyfry
                                           if( isdigit((int)current) )
                                                   {
                                                   if( isdigit((int)last_letter) )
                                                           separator = FALSE;
                                                   }

                                           }

                                          last_letter = toupper((int)current);


                                          // dodajemy oddzielny rekord separujacy

                                          if( separator )
                                                  {
                                                  struct KlientList *sep = calloc( 1, sizeof(struct KlientList) );

                                                  if( sep )
                                                         {
                                                         sep->kl_klient.Separator = TRUE;

                                                         sep->kl_klient.Sprzedawca = TRUE;
                                                         sep->kl_klient.Nabywca    = TRUE;
                                                         sep->kl_klient.Kosztowiec = TRUE;
                                                         sep->kl_klient.A = TRUE;
                                                         sep->kl_klient.B = TRUE;
                                                         sep->kl_klient.C = TRUE;

                                                         if( isdigit((int)current) )
                                                           {
                                                           strcpy(sep->kl_klient.Nazwa1, "0-9");
                                                           }
                                                         else
                                                           {
                                                           sep->kl_klient.Nazwa1[0] = toupper((int)current);
                                                           }

                                                         Insert(&kontrahenci, (struct Node *)sep, (struct Node *)node->kl_node.mln_Pred);
                                                         }
                                                  }
                                   }
                   }


           return( TRUE );

}
//|

/// CUST FILTR STRINGS

#define MSG_CUST_FILTR_WIN_TITLE "Filtr wy�wietlania"
#define MSG_CUST_FILTR_NAME "_Filtr nazwy"

#define MSG_CUST_FILTR_S  "_S"
#define  SH_CUST_FILTR_S   "Zaznacz t� opcj�, je�li chcesz\nwy�wietli� kontrahent�w przypisanych\ndo typu Sprzedawca"
#define MSG_CUST_FILTR_N  "_N"
#define  SH_CUST_FILTR_N   "Zaznacz t� opcj�, je�li chcesz\nwy�wietli� kontrahent�w przypisanych\ndo typu Nabywca"
#define MSG_CUST_FILTR_K  "_K"
#define  SH_CUST_FILTR_K   "Zaznacz t� opcj�, je�li chcesz\nwy�wietli� kontrahent�w przypisanych\ndo typu Kosztowiec"
#define MSG_CUST_FILTR_A  "_A"
#define  SH_CUST_FILTR_A   "Zaznacz t� opcj�, je�li chcesz\nwy�wietli� kontrahent�w przypisanych\ndo typu A"
#define MSG_CUST_FILTR_B  "_B"
#define  SH_CUST_FILTR_B   "Zaznacz t� opcj�, je�li chcesz\nwy�wietli� kontrahent�w przypisanych\ndo typu B"
#define MSG_CUST_FILTR_C  "_C"
#define  SH_CUST_FILTR_C   "Zaznacz t� opcj�, je�li chcesz\nwy�wietli� kontrahent�w przypisanych\ndo typu C"

#define MSG_CUST_FILTR_OK "F10 - _Zastosuj"
#define MSG_CUST_FILTR_CANCEL "ESC - Ponie_chaj"

//|
/// ApplyCustFilter
int ApplyCustFilter(Object *lista, struct Filtr *filtr, struct Klient *def_klient)
{
/*
** Filtr wy�wietlania klient�w.
** jesli def_klient != NULL, i ten klient 'lapie' sie na liste,
** zwracany jest jego numer na liscie (do MUIA_List_Active)
**
** 20010118 - omija separatory (zawsze zwraca numer klienta na liscie
**            jesli jest choc jeden wpis)
*/

struct KlientList *klient;
static char pattern[(FILTR_NAME_LEN*2)+2];

char   S, N, K, A, B, C;           // sprzedawca, nabywca...

char   LastWasSeparator = FALSE;

int    hilite_pos = 0;
int    pos = 0;
long   lp = 1;      // Liczba porzadkowa klientow


           set(lista, _MUIA_List_Quiet, TRUE);
           DoMethod(lista, MUIM_List_Clear, NULL);

           S = filtr->Sprzedawca;
           N = filtr->Nabywca;
           K = filtr->Kosztowiec;
           A = filtr->A;
           B = filtr->B;
           C = filtr->C;


           if(!IsListEmpty(&kontrahenci))
                 {
                 if(filtr->Aktywny)
                   {
                   if(ParsePatternNoCase(filtr->Nazwa, pattern, sizeof(pattern)) <1)
                           filtr->Aktywny = FALSE;
                   }


                 if(filtr->Aktywny)
                   {
                   for(klient = (struct KlientList *)kontrahenci.lh_Head; klient->kl_node.mln_Succ; klient = (struct KlientList *)klient->kl_node.mln_Succ)
                         if(MatchPatternNoCase(pattern, klient->kl_klient.Nazwa1) &&
                                (
                                  (klient->kl_klient.Sprzedawca & S) ||
                                  (klient->kl_klient.Nabywca & N) ||
                                  (klient->kl_klient.Kosztowiec & K) ||
                                  (klient->kl_klient.A & A) ||
                                  (klient->kl_klient.B & B) ||
                                  (klient->kl_klient.C & C)
                                )
                           )
                                 {


                                 // coby uniknac pustych separatorow...
                                 if( klient->kl_klient.Separator )
                                   {
                                   if( LastWasSeparator )
                                           {
                                           // wywalamy poprzedni precz
                                           DoMethod(lista, _MUIM_List_Remove, MUIV_List_Remove_Last);
                                           pos--;
                                           }
                                   else
                                           {
                                           LastWasSeparator = TRUE;
                                           }
                                   }
                                 else
                                   {
                                   LastWasSeparator = FALSE;
                                   }


                                 // szukmy default_klient, coby go podswietlic
                                 if( def_klient )
                                   {
                                   if( &klient->kl_klient == def_klient )
                                           hilite_pos = pos;
                                   }

//                 D(bug("%s\n", klient->kl_klient.Nazwa1));

                                 if( !klient->kl_klient.Separator )
                                   {
                                   klient->numer = lp++;
                                   hilite_pos++;
                                   }

                                 DoMethod(lista, _MUIM_List_InsertSingle, klient, _MUIV_List_Insert_Bottom);
                                 pos++;
                                 }
/*
                           else
                                 {
                                 if(klient->kl_klient.Separator )
                                         DoMethod(lista, _MUIM_List_InsertSingle, klient, _MUIV_List_Insert_Bottom);
                                 }
*/


                   }
                 else
                   {
                   // bez filtra
                   for(klient = (struct KlientList *)kontrahenci.lh_Head; klient->kl_node.mln_Succ; klient = (struct KlientList *)klient->kl_node.mln_Succ)
                         {
                         if( def_klient )
                           {
                           if( &klient->kl_klient == def_klient )
                                   hilite_pos = pos;
                           }

                         if( !klient->kl_klient.Separator )
                           {
                           klient->numer = lp++;
                           hilite_pos++;
                           }

                         DoMethod(lista, _MUIM_List_InsertSingle, klient, _MUIV_List_Insert_Bottom);
                         pos++;
                         }
                   }


                 }


           set(lista, _MUIA_List_Quiet, FALSE);

           return( hilite_pos );
}
//|
/// FilterCustSetup

static Object
           *CustFiltrWindow,              /* FILTR KLIENT�W          */
           *ST_Cust_Filtr_Nazwa,
           *BT_Cust_Filtr_S,
           *BT_Cust_Filtr_N,
           *BT_Cust_Filtr_K,
           *BT_Cust_Filtr_A,
           *BT_Cust_Filtr_B,
           *BT_Cust_Filtr_C,

           *BT_Cust_Filtr_Ok,
           *BT_Cust_Filtr_Cancel;


char FilterCustSetup(struct Filtr *filtr)
{

///   CustFiltrWindow

          CustFiltrWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_CUST_FILTR_WIN_TITLE,
                                                MUIA_Window_ID         , ID_CUST_WIN_FILTR,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
//                        MUIA_Window_CloseGadget, FALSE,
                                                WindowContents,
                                                   VGroup,

                                                   Child, HGroup,
                                                                  GroupFrame,
                                                                  MUIA_Group_Columns, 2,

                                                                  Child, MakeLabel2(MSG_CUST_FILTR_NAME),
                                                                  Child, ST_Cust_Filtr_Nazwa = MakeString(FILTR_CUST_NAME_LEN, MSG_CUST_FILTR_NAME),


                                                                  Child, MakeLabel2(NULL),
                                                                  Child, HGroup,
                                                                                 Child, BT_Cust_Filtr_S = ToggleButton(MSG_CUST_FILTR_S),
                                                                                 Child, BT_Cust_Filtr_N = ToggleButton(MSG_CUST_FILTR_N),
                                                                                 Child, BT_Cust_Filtr_K = ToggleButton(MSG_CUST_FILTR_K),
                                                                                 Child, BT_Cust_Filtr_A = ToggleButton(MSG_CUST_FILTR_A),
                                                                                 Child, BT_Cust_Filtr_B = ToggleButton(MSG_CUST_FILTR_B),
                                                                                 Child, BT_Cust_Filtr_C = ToggleButton(MSG_CUST_FILTR_C),
                                                                                 End,
                                                                  End,

                                                   Child, HGroup,
                                                                  Child, BT_Cust_Filtr_Ok     = TextButton(MSG_CUST_FILTR_OK),
                                                                  Child, BT_Cust_Filtr_Cancel = TextButton(MSG_CUST_FILTR_CANCEL),
                                                                  End,

                                                   End,
                                           End;
//|

        if( ! CustFiltrWindow )
           return( FALSE );

		_SetHelp( BT_Cust_Filtr_S, SH_CUST_FILTR_S );
		_SetHelp( BT_Cust_Filtr_N, SH_CUST_FILTR_N );
		_SetHelp( BT_Cust_Filtr_K, SH_CUST_FILTR_K );
		_SetHelp( BT_Cust_Filtr_A, SH_CUST_FILTR_A );
		_SetHelp( BT_Cust_Filtr_B, SH_CUST_FILTR_B );
		_SetHelp( BT_Cust_Filtr_C, SH_CUST_FILTR_C );


        set(ST_Cust_Filtr_Nazwa, MUIA_String_Contents, filtr->Nazwa);

        setcheckmark(BT_Cust_Filtr_S, filtr->Sprzedawca);
        setcheckmark(BT_Cust_Filtr_N, filtr->Nabywca);
        setcheckmark(BT_Cust_Filtr_K, filtr->Kosztowiec);
        setcheckmark(BT_Cust_Filtr_A, filtr->A);
        setcheckmark(BT_Cust_Filtr_B, filtr->B);
        setcheckmark(BT_Cust_Filtr_C, filtr->C);

        set(CustFiltrWindow, MUIA_Window_ActiveObject, ST_Cust_Filtr_Nazwa);


        /* FILTR KLIENT�W WINDOW */

        DoMethod(CustFiltrWindow     , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(CustFiltrWindow     , MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

        DoMethod(BT_Cust_Filtr_Ok    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Cust_Filtr_Cancel, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);



        _attachwin( CustFiltrWindow );

        return( TRUE );

}
//|
/// FilterCustFinish
char FilterCustFinish(struct Filtr *filtr)
{

        if(!strlen((char *)xget(ST_Cust_Filtr_Nazwa, MUIA_String_Contents)) )
           {
           set(CustFiltrWindow, MUIA_Window_ActiveObject, ST_Cust_Filtr_Nazwa);
           return(FALSE);
           }

        strcpy(filtr->Nazwa, (char *)xget(ST_Cust_Filtr_Nazwa, MUIA_String_Contents));

        filtr->Sprzedawca = getcheckmark(BT_Cust_Filtr_S);
        filtr->Nabywca    = getcheckmark(BT_Cust_Filtr_N);
        filtr->Kosztowiec = getcheckmark(BT_Cust_Filtr_K);
        filtr->A          = getcheckmark(BT_Cust_Filtr_A);
        filtr->B          = getcheckmark(BT_Cust_Filtr_B);
        filtr->C          = getcheckmark(BT_Cust_Filtr_C);

        return(TRUE);
}
//|
/// EdycjaCustFiltra

char CustFiltr(Object *lista, struct Filtr *filtr)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

        set(app, MUIA_Application_Sleep, TRUE);

        if( ! FilterCustSetup(filtr) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }


        if( WinOpen(CustFiltrWindow) )
           {
           while(running)
                  {
                  switch(DoMethod(app, MUIM_Application_Input, &signal))
                        {
                        case ID_OK:
                           if( FilterCustFinish( filtr ) )
                                 {
                                 filtr->Aktywny = TRUE;
                                 ApplyCustFilter(lista, filtr, NULL);
                                 running = FALSE;
                                 }
                           else
                                 {
                                 DisplayBeep(0);
                                 }
                           break;


                        case ID_CANCEL:
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }
           }
        else
           {
           DisplayBeep(NULL);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           }

        _detachwin(CustFiltrWindow);
        set(app, MUIA_Application_Sleep, FALSE);

        return(result);
}

//|

/// Str2NIP

char *Str2NIP(char *src)
{
static char dest[10 + 1];
char   buf[14 + 1];

int    i = 0;
char *str = buf;

        memset(dest, 0, sizeof(dest));
        strncpy(buf, src, sizeof(buf));
        str[ sizeof(buf) -1 ] = 0;

        for(;;)
           {
           if(*str != '-')
                   dest[i++] = *str;

           if(*str == '\0')
                   break;

           str++;
           }

        dest[10] = '\0';

        return(dest);
}
//|
/// NIP2Str

char *NIP2Str(char *str, char Sp��kaCywilna)
{

/*
** Sp��ka cywilna: 0 - 3-3-2-2
**                 1 - 3-2-2-3 (sp��ka cywilna)
*/
static char dest[16];
int i=0;


        if(*str == '\0')
           {
           dest[0] = '\0';
           return(dest);
           }


        if(Sp��kaCywilna)
          {
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = '-';
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = '-';
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = '-';
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = '\0';
          }
        else
          {
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = '-';
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = '-';
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = '-';
          dest[i++] = *str++;
          dest[i++] = *str++;
          dest[i++] = '\0';
          }

        return(dest);
}
//|

/// CopyDefCustomerSettings

void CopyDefCustomerSettings(struct Klient *klient)
{
        klient->Upowa�nienieVAT       = settings.def_upvat;
        klient->Upowa�nienieBezLimitu = settings.def_unlimited;
        klient->DopisywanieDozwolone  = settings.def_addenabled;
        klient->FIFO                  = settings.def_fifo;
        klient->UpVat_ExpireDate      = wfmh_GetCurrentTime()->ds_Days + settings.def_expire_offset;

        klient->Sprzedawca = settings.def_sprzedawca;
        klient->Nabywca    = settings.def_nabywca;
        klient->Kosztowiec = settings.def_kosztowiec;
        klient->A          = settings.def_a;
        klient->B          = settings.def_b;
        klient->C          = settings.def_c;

        strcpy(klient->Miasto, settings.def_miasto);
}
//|

/// EDYCJA KONTRAHENTA WINDOW

#define MSG_CUST_EDIT_WIN_TITLE "Edycja danych kontrahenta"

#define MSG_CUST_ED_TEMP    "Wpis _jednorazowy"
#define  SH_CUST_ED_TEMP	"Zaznacz t� opcj�, je�li dane\ntego kontrahenta nie maj� by�\nprzechowywane w bazie kontrahent�w\n(np. sprzeda� jednorazowa). Dane\nkontrahenta b�d� dost�pne do momentu\nzako�czenia pracy z programem"

#define MSG_CUST_ED_TITLE_1 "Dane kontrahenta"
#define MSG_CUST_ED_TITLE_2 "Upowa�nienie VAT"
#define MSG_CUST_ED_TITLE_3 "Odbiorcy dokument�w"

#define MSG_CUST_ED_NAME   "N_azwa"
#define MSG_CUST_ED_SC     "S_p��ka"
#define MSG_CUST_ED_ULICA  "A_dres"
#define MSG_CUST_ED_KOD    "_Kod"
#define MSG_CUST_ED_MIASTO "_Miasto"
#define MSG_CUST_ED_TEL    "_Telefon"
#define MSG_CUST_ED_FAX    "_Fax"
#define MSG_CUST_ED_EMAIL  "_E-mail"
#define MSG_CUST_ED_UWAGI  "_Uwagi"

#define MSG_CUST_ED_NIP    "N_IP"
#define MSG_CUST_ED_REGON  "_Regon"
#define MSG_CUST_ED_BANK   "_Bank"
#define MSG_CUST_ED_KONTO  "_Nr. konta"

#define MSG_CUST_ED_PAGE1  "F1 - Dane"
#define MSG_CUST_ED_PAGE2  "F2 - Odbiorcy"
#define MSG_CUST_ED_PAGE3  "F3 - R��ne"

#define MSG_CUST_ED_UPVAT        "_Upowa�nienie VAT"
#define  SH_CUST_ED_UPVAT		 "Je�li posiadasz aktualne upowa�nienie\ndo wystawiania faktur VAT dla danego\nkontrahenta bez jego podpisu, zaznacz\nt� opcj� aby stosowna pozycja pojawi�a\nsi� w oknie wyboru odbiorcy dokumentu.\nJe�li upowa�nienie jest okresowe, podaj\njego dat� wa�no�ci. Je�li jest bezterminowe\nzaznacz to u�ywaj�c checkboxa obok pola daty." 
#define MSG_CUST_ED_UPVAT_EXPIRE "_Wa�ne do"
#define MSG_CUST_ED_ADDENABLED   "Do_pisywanie"
#define  SH_CUST_ED_ADDENABLED   "Zaznacz t� opcj�, je�li chcesz, mie�\nmo�liwo�� p��niejszego dopisania nazwisk\nodbiorc�w dokumentu, podczas jego wystawiania.\nJe�li opcja ta jest wy��czona, dost�pne\nb�d� wy��cznie dane odbiorc�w podanych\nponi�ej."
#define MSG_CUST_ED_FIFO         "_Kolejka FIFO"
#define  SH_CUST_ED_FIFO		 "Opcja ta reguluje spos�b obs�ugi\nprzepe�nienia listy odbiorc�w.\nJe�li opcja jest aktywna, dodanie\nnowego odbiorcy gdy wszystkie 4 dost�pne\npola s� ju� wykorzystane spowoduje\nusuni�cie najstarszego wpisu, aby zrobi�miejsce na nowego odbiorc�.\n(FIFO =  First In, First Out)"

#define MSG_CUST_ED_ODB_1        "Odbiorca _1"
#define MSG_CUST_ED_ODB_2        "Odbiorca _2"
#define MSG_CUST_ED_ODB_3        "Odbiorca _3"
#define MSG_CUST_ED_ODB_4        "Odbiorca _4"

#define MSG_CUST_ED_GR_TITLE "Domy�lny typ kontrahenta"
#define MSG_CUST_ED_GR_1     "_Sprzedawca"
#define MSG_CUST_ED_GR_2     "O_dbiorca"
#define MSG_CUST_ED_GR_3     "_Kosztowiec"
#define MSG_CUST_ED_GR_4     "_A"
#define MSG_CUST_ED_GR_5     "_B"
#define MSG_CUST_ED_GR_6     "_C"

#define MSG_CUST_ED_DEF_TITLE  "Ustawienia domy�lne"
#define MSG_CUST_ED_DEF_RABAT  "_Rabat"
#define MSG_CUST_ED_DEF_RABATP "%"

#define MSG_CUST_ED_DEF_PAY    "Spos�b _p�atno�ci"

#define MSG_CUST_ED_OK     "F10 - _Ok"
#define MSG_CUST_ED_CANCEL "ESC - Ponie_chaj"

//|
/// EdycjaKlientaSetup

static Object
           *EdycjaKontrahentaWindow,      /* EDYCJA KONTRAHENTA        */

           *CH_Cust_Temporary,

           *GR_Cust_Pages,

           *ST_Cust_Nazwa_1,
           *ST_Cust_Nazwa_2,
           *CH_Cust_SC,
           *ST_Cust_Ulica,
           *ST_Cust_Kod,
           *ST_Cust_Miasto,
           *ST_Cust_NIP,
           *ST_Cust_Regon,
           *BS_Cust_Bank,
           *ST_Cust_Uwagi,

           *ST_Cust_Konto,
           *ST_Cust_Tel,
           *ST_Cust_Fax,
           *ST_Cust_Email,

           *CH_Cust_UpVat,
           *ST_Cust_UpVat_Expire,
           *CH_Cust_UpVat_Unlimited,
           *CH_Cust_AddEnabled,
           *CH_Cust_FIFO,
           *ST_Cust_Odb_1,
           *ST_Cust_Odb_2,
           *ST_Cust_Odb_3,
           *ST_Cust_Odb_4,

           *CH_Cust_Gr_1,
           *CH_Cust_Gr_2,
           *CH_Cust_Gr_3,
           *CH_Cust_Gr_4,
           *CH_Cust_Gr_5,
           *CH_Cust_Gr_6,

           *ST_Cust_Def_Rabat,
           *TX_Cust_Def_Payment,
           *BT_Cust_Def_Payment,
           *ST_Cust_Comment,

           *BT_Cust_Ed_Ok,
           *BT_Cust_Ed_Cancel;


static char *GR_Cust_Ed_Titles[] = {MSG_CUST_ED_PAGE1,
                                                         MSG_CUST_ED_PAGE2,
                                                         MSG_CUST_ED_PAGE3,
                                                         NULL};

char EdycjaKlientaSetup(struct KlientList *KlientList, char Mode)
{
struct Klient    *klient    = &KlientList->kl_klient;
struct KlientExt *klientExt = &KlientList->kl_klientExt;
struct DateStamp ds = {0};
ULONG  NoInput = FALSE;


///   EdycjaKontrahentaWindow

          if( Mode == 2 )
                NoInput = TRUE;

          EdycjaKontrahentaWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_CUST_EDIT_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_KONTRAHENCI_EDIT,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
                                                WindowContents,
                                                   VGroup,

                                                   Child, HGroup,
                                                                  Child, HGroup,
                                                                                 Child, HVSpace,
                                                                                 Child, MakeLabel2( MSG_CUST_ED_TEMP ),
                                                                                 End,
                                                                  Child, CH_Cust_Temporary = _MakeCheck(CUST_ED_TEMP),
                                                                  End,


                                                   Child, GR_Cust_Pages = RegisterGroup(GR_Cust_Ed_Titles),
///                                 Dane personalne
                                                                  Child, HGroup,
                                                                                 GroupFrameT(MSG_CUST_ED_TITLE_1),
                                                                                 MUIA_Group_Columns, 2,


                                                                                 Child, MakeLabel2(MSG_CUST_ED_NAME),
                                                                                 Child, ST_Cust_Nazwa_1 = DoString(CUST_NAME_LEN, MSG_CUST_ED_NAME, NoInput),
                                                                                 Child, EmptyLabel(),
                                                                                 Child, ST_Cust_Nazwa_2 = DoString(CUST_NAME_LEN, NULL, NoInput ),

                                                                                 Child, MakeLabel2(MSG_CUST_ED_ULICA),
                                                                                 Child, ST_Cust_Ulica = DoString(CUST_ADRES_LEN, MSG_CUST_ED_ULICA, NoInput),

                                                                                 Child, MakeLabel2(MSG_CUST_ED_KOD),
                                                                                 Child, HGroup,
                                                                                                Child, ST_Cust_Kod = DoStringAccept(CUST_KOD_LEN, MSG_CUST_ED_KOD, "0123456789-", NoInput ),

                                                                                                Child, MakeLabel2(MSG_CUST_ED_MIASTO),
                                                                                                Child, ST_Cust_Miasto = DoString(CUST_MIASTO_LEN, MSG_CUST_ED_MIASTO, NoInput),
                                                                                                End,

                                                                                 Child, MakeLabel2(MSG_CUST_ED_TEL),
                                                                                 Child, HGroup,
                                                                                                Child, ST_Cust_Tel = DoString(CUST_TEL_LEN, MSG_CUST_ED_TEL, NoInput),

                                                                                                Child, MakeLabel2(MSG_CUST_ED_FAX),
                                                                                                Child, ST_Cust_Fax = DoString(CUST_FAX_LEN, MSG_CUST_ED_FAX, NoInput),
                                                                                                End,
                                                                                 Child, MakeLabel2(MSG_CUST_ED_EMAIL),
                                                                                 Child, ST_Cust_Email = DoString(CUST_EMAIL_LEN, MSG_CUST_ED_EMAIL, NoInput),


                                                                                 Child, MakeLabel2(MSG_CUST_ED_SC),
                                                                                 Child, HGroup,
                                                                                                Child, CH_Cust_SC = MakeCheck(MSG_CUST_ED_SC),

                                                                                                Child, MakeLabel2(MSG_CUST_ED_NIP),
                                                                                                Child, HGroup,
                                                                                                           Child, ST_Cust_NIP = DoStringAccept(CUST_NIP_LEN + 3, MSG_CUST_ED_NIP, "0123456789-", NoInput),

                                                                                                           Child, MakeLabel2(MSG_CUST_ED_REGON),
                                                                                                           Child, ST_Cust_Regon = DoNumericString(CUST_REGON_LEN, MSG_CUST_ED_REGON, NoInput),
                                                                                                           End,
                                                                                                End,


                                                                                 Child, MakeLabel2(MSG_CUST_ED_KONTO),
                                                                                 Child, ST_Cust_Konto = DoStringReject(CUST_KONTO_LEN, MSG_CUST_ED_KONTO, "!@#$%^&*()_+|\=[]{}'\";:,<>?", NoInput),
                                                                                 Child, MakeLabel2(MSG_CUST_ED_BANK),
                                                                                 Child, BS_Cust_Bank = Golem_BankStringObject, MUIA_BankString_NoInput, NoInput, End,

                                                                                 Child, MakeLabel2(MSG_CUST_ED_UWAGI),
                                                                                 Child, ST_Cust_Uwagi = DoString(CUST_NOTE_LEN, MSG_CUST_ED_UWAGI, NoInput),


                                                                                 End,
//|
///                                 Odbiorcy dokumentu

                                                                  Child, VGroup,

                                                                                 Child, HGroup,
                                                                                                GroupFrameT(MSG_CUST_ED_TITLE_2),
                                                                                                MUIA_Group_Columns, 2,

                                                                                                Child, MakeLabel2(MSG_CUST_ED_UPVAT),
                                                                                                Child, HGroup,
                                                                                                           Child, CH_Cust_UpVat = _MakeCheck(CUST_ED_UPVAT),
                                                                                                           Child, MakeLabel2(MSG_CUST_ED_UPVAT_EXPIRE),
                                                                                                           Child, HGroup,
                                                                                                                          Child, ST_Cust_UpVat_Expire    = DoString(CUST_NAME_LEN, MSG_CUST_ED_UPVAT_EXPIRE, NoInput),
                                                                                                                          Child, CH_Cust_UpVat_Unlimited = MakeCheck(NULL),
                                                                                                                          End,
                                                                                                           End,
                                                                                                End,

                                                                                 Child, HGroup,
                                                                                                GroupFrameT(MSG_CUST_ED_TITLE_3),
                                                                                                MUIA_Group_Columns, 2,
                                                                                                Child, MakeLabel2(MSG_CUST_ED_ADDENABLED),
                                                                                                Child, HGroup,
                                                                                                           Child, CH_Cust_AddEnabled = _MakeCheck(CUST_ED_ADDENABLED),

                                                                                                           Child, HVSpace,

                                                                                                           Child, MakeLabel2(MSG_CUST_ED_FIFO),
                                                                                                           Child, CH_Cust_FIFO = _MakeCheck(CUST_ED_FIFO),
                                                                                                           End,

                                                                                                Child, MakeLabel2(MSG_CUST_ED_ODB_1),
                                                                                                Child, ST_Cust_Odb_1 = DoString(CUST_NAME_LEN, MSG_CUST_ED_ODB_1, NoInput),
                                                                                                Child, MakeLabel2(MSG_CUST_ED_ODB_2),
                                                                                                Child, ST_Cust_Odb_2 = DoString(CUST_NAME_LEN, MSG_CUST_ED_ODB_2, NoInput),
                                                                                                Child, MakeLabel2(MSG_CUST_ED_ODB_3),
                                                                                                Child, ST_Cust_Odb_3 = DoString(CUST_NAME_LEN, MSG_CUST_ED_ODB_3, NoInput),
                                                                                                Child, MakeLabel2(MSG_CUST_ED_ODB_4),
                                                                                                Child, ST_Cust_Odb_4 = DoString(CUST_NAME_LEN, MSG_CUST_ED_ODB_4, NoInput),
                                                                                                End,

                                                                                 Child, HVSpace,
                                                                                 End,
//|
///                                 Ustawienia r��ne

                                                                  Child, VGroup,

                                                                                 Child, HGroup,
                                                                                                GroupFrameT(MSG_CUST_ED_GR_TITLE),
                                                                                                MUIA_Group_Columns, 2,

                                                                                                Child, MakeLabel2(MSG_CUST_ED_GR_1),
                                                                                                Child, HGroup,
                                                                                                           Child, CH_Cust_Gr_1 = MakeCheck(MSG_CUST_ED_GR_1),
                                                                                                           Child, HVSpace,

                                                                                                           Child, MakeLabel2(MSG_CUST_ED_GR_4),
                                                                                                           Child, CH_Cust_Gr_4 = MakeCheck(MSG_CUST_ED_GR_4),
                                                                                                           End,

                                                                                                Child, MakeLabel2(MSG_CUST_ED_GR_2),
                                                                                                Child, HGroup,
                                                                                                           Child, CH_Cust_Gr_2 = MakeCheck(MSG_CUST_ED_GR_2),
                                                                                                           Child, HVSpace,

                                                                                                           Child, MakeLabel2(MSG_CUST_ED_GR_5),
                                                                                                           Child, CH_Cust_Gr_5 = MakeCheck(MSG_CUST_ED_GR_5),
                                                                                                           End,

                                                                                                Child, MakeLabel2(MSG_CUST_ED_GR_3),
                                                                                                Child, HGroup,
                                                                                                           Child, CH_Cust_Gr_3 = MakeCheck(MSG_CUST_ED_GR_3),
                                                                                                           Child, HVSpace,

                                                                                                           Child, MakeLabel2(MSG_CUST_ED_GR_6),
                                                                                                           Child, CH_Cust_Gr_6 = MakeCheck(MSG_CUST_ED_GR_6),
                                                                                                           End,
                                                                                                End,


                                                                                 // ustawienia domy�lne
                                                                                 Child, HGroup,
                                                                                                MUIA_Group_Columns, 2,
                                                                                                GroupFrameT(MSG_CUST_ED_DEF_TITLE),

                                                                                                Child, MakeLabel2(MSG_CUST_ED_DEF_RABAT),
                                                                                                Child, HGroup,
                                                                                                           Child, ST_Cust_Def_Rabat = DoCashString( 6, MSG_CUST_ED_DEF_RABAT, NoInput ),
                                                                                                           Child, MakeLabel2(MSG_CUST_ED_DEF_RABATP),
                                                                                                           End,


                                                                                                Child, MakeLabel2( MSG_CUST_ED_DEF_PAY  ),
                                                                                                Child, HGroup,
                                                                                                           MUIA_Group_Spacing, 1,

                                                                                                           Child, TX_Cust_Def_Payment = TextObject2, End,
                                                                                                           Child, BT_Cust_Def_Payment = PopButton2( MUII_PopUp, MSG_CUST_ED_DEF_PAY ),
                                                                                                           End,
                                                                                                End,


                                                                                 Child, HVSpace,
                                                                                 End,
//|
                                                                  End,

                                                   Child, HGroup,
                                                                  Child, BT_Cust_Ed_Ok     = TextButton(MSG_CUST_ED_OK),
                                                                  Child, BT_Cust_Ed_Cancel = TextButton(MSG_CUST_ED_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|


        if( ! EdycjaKontrahentaWindow )
           return( FALSE );




        /* EDYCJA KONTRAHENT�W */

        DoMethod(EdycjaKontrahentaWindow, MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(EdycjaKontrahentaWindow, MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_0);
        DoMethod(EdycjaKontrahentaWindow, MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_1);
        DoMethod(EdycjaKontrahentaWindow, MUIM_Notify, MUIA_Window_InputEvent, "f3", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_PAGE_2);
        DoMethod(EdycjaKontrahentaWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);

        DoMethod(CH_Cust_SC             , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_SC);
        set(CH_Cust_SC, MUIA_CycleChain, FALSE);

        DoMethod(ST_Cust_Konto, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, BS_Cust_Bank, 3, MUIM_Set, MUIA_BankString_Account, MUIV_TriggerValue);

        DoMethod(CH_Cust_UpVat          , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_UPVAT);
        DoMethod(CH_Cust_UpVat_Unlimited, MUIM_Notify, MUIA_Selected, MUIV_EveryTime, ST_Cust_UpVat_Expire, 3, MUIM_Set, MUIA_Disabled, MUIV_TriggerValue);

        DoMethod(CH_Cust_AddEnabled     , MUIM_Notify, MUIA_Selected, MUIV_EveryTime, CH_Cust_FIFO, 3, MUIM_Set, MUIA_Disabled, MUIV_NotTriggerValue);

        DoMethod(BT_Cust_Def_Payment, MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_SELECT);


        DoMethod(BT_Cust_Ed_Ok          , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Cust_Ed_Cancel      , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);




        // view mode?
        if( Mode == 2 )
           {
           _disable( CH_Cust_Temporary );
           _disable( CH_Cust_SC );
           _disable( CH_Cust_UpVat );
           _disable( CH_Cust_UpVat_Unlimited );
           _disable( CH_Cust_AddEnabled );
           _disable( CH_Cust_FIFO );
           _disable( CH_Cust_Gr_1 );
           _disable( CH_Cust_Gr_2 );
           _disable( CH_Cust_Gr_3 );
           _disable( CH_Cust_Gr_4 );
           _disable( CH_Cust_Gr_5 );
           _disable( CH_Cust_Gr_6 );

           _disable( BT_Cust_Def_Payment );

           _disable( BT_Cust_Ed_Ok );
           }


        // values

        setstring(ST_Cust_Nazwa_1 , klient->Nazwa1);
        setstring(ST_Cust_Nazwa_2 , klient->Nazwa2);
        setstring(ST_Cust_Ulica   , klient->Ulica);
        setstring(ST_Cust_Kod     , klient->Kod);
        setstring(ST_Cust_Miasto  , klient->Miasto);
        setstring(ST_Cust_Tel     , klient->Telefon);
        setstring(ST_Cust_Fax     , klient->Fax);
        setstring(ST_Cust_Email   , klient->Email);
        setstring(ST_Cust_NIP     , NIP2Str(klient->NIP, klient->Sp��kaCywilna));
        setstring(ST_Cust_Regon   , klient->Regon);
        setstring(ST_Cust_Konto   , klient->Konto);
        setstring(BS_Cust_Bank    , klient->Bank);
        setcheckmark(CH_Cust_SC   , klient->Sp��kaCywilna);

        if(klientExt->Uwagi)
                setstring(ST_Cust_Uwagi, klientExt->Uwagi);

        setcheckmark(CH_Cust_UpVat, klient->Upowa�nienieVAT);
        setcheckmark(CH_Cust_UpVat_Unlimited, klient->Upowa�nienieBezLimitu);
        setcheckmark(CH_Cust_AddEnabled, klient->DopisywanieDozwolone);
        setcheckmark(CH_Cust_FIFO, klient->FIFO);

        setstring(ST_Cust_Odb_1 , klient->Odb1);
        setstring(ST_Cust_Odb_2 , klient->Odb2);
        setstring(ST_Cust_Odb_3 , klient->Odb3);
        setstring(ST_Cust_Odb_4 , klient->Odb4);

        ds.ds_Days = klient->UpVat_ExpireDate;
        setstring(ST_Cust_UpVat_Expire, wfmh_Date2Str(&ds));

        setcheckmark(CH_Cust_Gr_1, klient->Sprzedawca);
        setcheckmark(CH_Cust_Gr_2, klient->Nabywca);
        setcheckmark(CH_Cust_Gr_3, klient->Kosztowiec);
        setcheckmark(CH_Cust_Gr_4, klient->A);
        setcheckmark(CH_Cust_Gr_5, klient->B);
        setcheckmark(CH_Cust_Gr_6, klient->C);

        setcheckmark(CH_Cust_Temporary, KlientList->Tymczasowy);

        setstring( ST_Cust_Def_Rabat, Price2String(klient->def_Rabat) );
        settext( TX_Cust_Def_Payment, klientExt->def_Payment );
        setstring( ST_Cust_Uwagi, klientExt->Uwagi );


        set(GR_Cust_Pages          , MUIA_Group_ActivePage, 0);

        if( Mode != 2)
                set(EdycjaKontrahentaWindow, MUIA_Window_ActiveObject, ST_Cust_Nazwa_1);



        _attachwin( EdycjaKontrahentaWindow );

        return( TRUE );

}
//|
/// EdycjaKlientaFinish
char EdycjaKlientaFinish(struct KlientList *KlientList, char Mode, char Flags)
{
/*
** sprawdza poprawno�� wprowadzonych danych
** dot. produktu. Zwraca FALSE, je�li co�
** jest nie tak, TRUE, je�li wszystkie
** elementy s� poprawne, oraz REFRESH,
** je�li nazwa produktu zosta�a zmieniona
** (istotne tylko podczas edycji produktu)
*/

struct Klient    *klient    = &KlientList->kl_klient;
struct KlientExt *klientExt = &KlientList->kl_klientExt;
struct DateStamp *ds;
char   result = TRUE;

        if(strlen((char *)xget(ST_Cust_Nazwa_1, MUIA_String_Contents)) == 0)
           {
           set(GR_Cust_Pages, MUIA_Group_ActivePage, 0);
           set(EdycjaKontrahentaWindow, MUIA_Window_ActiveObject, ST_Cust_Nazwa_1);
           return(FALSE);
           }
        if(strlen((char *)xget(ST_Cust_Ulica, MUIA_String_Contents)) == 0)
           {
           set(GR_Cust_Pages, MUIA_Group_ActivePage, 0);
           set(EdycjaKontrahentaWindow, MUIA_Window_ActiveObject, ST_Cust_Ulica);
           return(FALSE);
           }
        if(strlen(getstr(ST_Cust_Miasto)) == 0)
           {
           set(GR_Cust_Pages, MUIA_Group_ActivePage, 0);
           set(EdycjaKontrahentaWindow, MUIA_Window_ActiveObject, ST_Cust_Miasto);
           return(FALSE);
           }


        // czy def_Rabat ok?
        if(strlen( getstr( ST_Cust_Def_Rabat )) == 0)
           {
           set(GR_Cust_Pages, MUIA_Group_ActivePage, 2);
           set(EdycjaKontrahentaWindow, MUIA_Window_ActiveObject, ST_Cust_Def_Rabat);
           return(FALSE);
           }
        else
           {
           double rabat = String2Price(getstr(ST_Cust_Def_Rabat));
           if((rabat > 100) || (rabat <0))
                   {
                   set(GR_Cust_Pages, MUIA_Group_ActivePage, 2);
                   set(EdycjaKontrahentaWindow, MUIA_Window_ActiveObject, ST_Cust_Def_Rabat);
                   return(FALSE);
                   }
           }


        // czy nip poprawny (jesli wymagany badz podany z wlasniej nieprzymuszonej...)?
        if( (strlen(getstr(ST_Cust_NIP)) != 0) || ( Flags & F_CUSTED_NIP_REQUIRED   ) )
           {
           if( IsNIPValid( getstr(ST_Cust_NIP) ) == FALSE )
                   {
                   set(GR_Cust_Pages, MUIA_Group_ActivePage, 0);
                   set(EdycjaKontrahentaWindow, MUIA_Window_ActiveObject, ST_Cust_NIP);
                   return(FALSE);
                   }
           }

        if(strlen(getstr(ST_Cust_Regon)) != 0)
           {
           if(strlen(getstr(ST_Cust_Regon)) != 9)
                   {
                   set(GR_Cust_Pages, MUIA_Group_ActivePage, 0);
                   set(EdycjaKontrahentaWindow, MUIA_Window_ActiveObject, ST_Cust_Regon);
                   return(FALSE);
                   }
           }



        // czy jest up. vat
        ds = wfmh_Str2Date((char *)getstr(ST_Cust_UpVat_Expire));
        if(getcheckmark(CH_Cust_UpVat))
           {
           // czy czasem nie jest bez ogranicze�?
           if(!getcheckmark(CH_Cust_UpVat_Unlimited))
                 {
                 if(ds == NULL)
                   {
                   set(GR_Cust_Pages, MUIA_Group_ActivePage, 1);
                   set(EdycjaKontrahentaWindow, MUIA_Window_ActiveObject, ST_Cust_UpVat_Expire);
                   return(FALSE);
                   }
                 }
           }


        // czy klient o takiej nazwie juz istnieje?
        {
        struct KlientList *tmp;

        if(Mode)
          {
          Remove((struct Node *)KlientList);
          tmp = Znajd�KlientaNazwa((char *)xget(ST_Cust_Nazwa_1, MUIA_String_Contents));
          DodajKlienta(KlientList);
          }
        else
          {
          tmp = Znajd�KlientaNazwa((char *)xget(ST_Cust_Nazwa_1, MUIA_String_Contents));
          }

        if(tmp)
           {
           set(GR_Cust_Pages, MUIA_Group_ActivePage, 0);
           set(EdycjaKontrahentaWindow, MUIA_Window_ActiveObject, ST_Cust_Nazwa_1);
           return(FALSE);
           }
        }

        if(StrnCmp(MyLocale, klient->Nazwa1, (char *)xget(ST_Cust_Nazwa_1, MUIA_String_Contents), -1, SC_COLLATE2) != 0)
           result = REFRESH;

        strcpy(klient->Nazwa1    , (char *)xget(ST_Cust_Nazwa_1, MUIA_String_Contents));
        strcpy(klient->Nazwa2    , (char *)xget(ST_Cust_Nazwa_2, MUIA_String_Contents));
        strcpy(klient->Ulica     , (char *)xget(ST_Cust_Ulica  , MUIA_String_Contents));
        strcpy(klient->Kod       , (char *)xget(ST_Cust_Kod    , MUIA_String_Contents));
        strcpy(klient->Miasto    , (char *)xget(ST_Cust_Miasto , MUIA_String_Contents));
        strcpy(klient->Telefon   , (char *)xget(ST_Cust_Tel  , MUIA_String_Contents));
        strcpy(klient->Fax       , (char *)xget(ST_Cust_Fax  , MUIA_String_Contents));
        strcpy(klient->Email     , (char *)xget(ST_Cust_Email, MUIA_String_Contents));
        strcpy(klient->NIP       , Str2NIP((char *)xget(ST_Cust_NIP, MUIA_String_Contents)));
        strcpy(klient->Regon     , (char *)xget(ST_Cust_Regon   , MUIA_String_Contents));
        strcpy(klient->Konto     , (char *)xget(ST_Cust_Konto   , MUIA_String_Contents));
        strcpy(klient->Bank      , (char *)xget(BS_Cust_Bank    , MUIA_String_Contents));
        klient->Sp��kaCywilna = getcheckmark(CH_Cust_SC);

        klient->Upowa�nienieVAT = getcheckmark(CH_Cust_UpVat);
        klient->UpVat_ExpireDate = ds->ds_Days;
        klient->Upowa�nienieBezLimitu = getcheckmark(CH_Cust_UpVat_Unlimited);
        klient->DopisywanieDozwolone = getcheckmark(CH_Cust_AddEnabled);
        klient->FIFO = getcheckmark(CH_Cust_FIFO);

        copystr(klient->Odb1, ST_Cust_Odb_1);
        copystr(klient->Odb2, ST_Cust_Odb_2);
        copystr(klient->Odb3, ST_Cust_Odb_3);
        copystr(klient->Odb4, ST_Cust_Odb_4);

        klient->Sprzedawca = getcheckmark(CH_Cust_Gr_1);
        klient->Nabywca    = getcheckmark(CH_Cust_Gr_2);
        klient->Kosztowiec = getcheckmark(CH_Cust_Gr_3);
        klient->A          = getcheckmark(CH_Cust_Gr_4);
        klient->B          = getcheckmark(CH_Cust_Gr_5);
        klient->C          = getcheckmark(CH_Cust_Gr_6);

        KlientList->Tymczasowy = getcheckmark(CH_Cust_Temporary);

        klient->def_Rabat  = String2Price( getstr( ST_Cust_Def_Rabat) ) ;
        strncpy( klientExt->def_Payment, (char *)xget( TX_Cust_Def_Payment, MUIA_Text_Contents ), PAY_NAME_LEN );


        if(strlen(getstr(ST_Cust_Uwagi)) != 0)
           {
           if( klientExt->Uwagi == NULL )
                   klientExt->Uwagi = calloc( 1, CUST_NOTE_LEN );

           if( klientExt->Uwagi )
                   copystr(klientExt->Uwagi, ST_Cust_Uwagi);
           }


        return(result);         
}
//|
/// EdycjaKlienta
char EdycjaKlienta(struct KlientList *klient, char Mode, char Flags)
{
/*
** Mode:
**    0 - create - tworzenie nowego klienta
**    1 - edit   - edycja istniejacych danych
**    2 - view   - przegladanie (bez edycji) danych
** Flags:
**    F_CUSTED_NIP_REQUIRED - aby dane klienta zosta�y zatwierdzone
**                            musi by� podany NIP
*/

char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;

//char OpisBankuBuffer[CUST_KONTO_LEN] = "";


        set(app, MUIA_Application_Sleep, TRUE);


//    // poczatkowy opis banku
//    copystr(OpisBankuBuffer, ST_Cust_Bank);


        if( ! EdycjaKlientaSetup(klient, Mode) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }

/*
        set(MagazynWindow  , MUIA_Window_ActiveObject, LV_Mag_Produkty);
        set(MagazynWindow  , MUIA_Window_DefaultObject, LV_Mag_Produkty);
*/

        if(WinOpen(EdycjaKontrahentaWindow))
                {
                while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);
                  switch(ID)
                        {
                        case ID_PAGE_0:
                           set(GR_Cust_Pages, MUIA_Group_ActivePage, 0);
                           break;
                        case ID_PAGE_1:
                           set(GR_Cust_Pages, MUIA_Group_ActivePage, 1);
                           break;
                        case ID_PAGE_2:
                           set(GR_Cust_Pages, MUIA_Group_ActivePage, 2);
                           break;

                        case ID_CUST_UPVAT:
                           set(CH_Cust_UpVat_Unlimited, MUIA_Disabled, !(getcheckmark(CH_Cust_UpVat)));
                           if(!getcheckmark(CH_Cust_UpVat_Unlimited))
                                   set(ST_Cust_UpVat_Expire, MUIA_Disabled, !(getcheckmark(CH_Cust_UpVat)));
                           break;


                        case ID_SELECT:
                           {
                           struct P�atno��List *p�atno�� = P�atno��Selector( NULL );

                           if( p�atno�� )
                                   {
                                   settext( TX_Cust_Def_Payment, p�atno��->pl_p�atno��.Nazwa );
                                   }
                                 else
                                   {
                                   DisplayBeep(0);
                                   }
                           }
                           break;


                        case ID_CUST_SC:
                           setstring(ST_Cust_NIP, NIP2Str(Str2NIP(getstr(ST_Cust_NIP)), getcheckmark(CH_Cust_SC)));
                           break;

                        case ID_OK:
                           result = EdycjaKlientaFinish(klient, Mode, Flags);

                           if(result == FALSE)
                                 {
                                 DisplayBeep(0);
                                 }
                           else
                                 {
                                 running = FALSE;
                                 }
                           break;


                        case ID_CANCEL:
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

                }
         else
                {
                DisplayBeep(NULL);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }


        _detachwin( EdycjaKontrahentaWindow );
        set(app, MUIA_Application_Sleep, FALSE);

        return(result);

}
//|


/// KONTRAHENCI WINDOW STRINGS

#define MSG_CUST_WIN_TITLE "Kontrahenci"

#define MSG_CUST_DODAJ   "F1 - Dod_aj"
#define MSG_CUST_EDYTUJ  "F2 - _Edytuj"
#define MSG_CUST_SKASUJ  "DEL - _Usu�"
#define MSG_CUST_CLEANUP "U_porz�dkuj"

#define MSG_CUST_OK     "F10 - Zapisz list� kontrahent�w"
#define MSG_CUST_CANCEL "ESC - Anuluj wszystkie zmiany"

#define MSG_CUST_SAVE     "\033cNa pewno chcesz zapisa� aktualny\nstan bazy kontrahent�w?"
#define MSG_CUST_SAVE_GAD "*_Tak, zapisz|_Nie"

#define MSG_CUST_CANCEL_REQ     "\033cNa pewno chcesz zako�czy� edycj�\nbazy kontrahent�w i uniewa�ni�\nwszystkie wprowadzone do� zmiany?"
#define MSG_CUST_CANCEL_REQ_GAD "_Tak, zako�cz|*_Nie"

#define MSG_CUST_DELETED "Dane klienta \"%s\"\ns� aktualnie zaznaczone jako \033bSKASOWANE\033n,\ni nie mog� by� modyfikowane! Je�li chcesz dokona�\nw nich jakichkolwiek zmian, musisz je najpierw odkasowa�.\nCzy chcesz to zrobi� teraz?"
#define MSG_CUST_DELETED_GAD "*_Tak, odkasuj|_Nie"

#define MSG_CUST_CLEANUP_REQ     "\033cNa pewno chcesz uporz�dkowa�\nbaz� kontrahent�w?\nRekordy zaznaczone jako \033bskasowane\033n\nzostan� usuni�te!"
#define MSG_CUST_CLEANUP_REQ_GAD "_Tak, uporz�dkuj|*_Nie"

//|
/// EdycjaKontrahent�wSetup

static Object
           *KontrahenciWindow,            /* KONTRAHENCI                */
           *LV_Cust_Klienci,
           *BT_Cust_Dodaj,
           *BT_Cust_Usu�,
           *BT_Cust_Edytuj,

           *BT_Cust_Cleanup,

           *BT_Cust_Filter,
           *BT_Cust_FilterAll,

           *BT_Cust_Ok,
           *BT_Cust_Cancel;


char EdycjaKontrahent�wSetup( struct Filtr *cust_filter)
{

///   KontrahenciWindow

          KontrahenciWindow = WindowObject,
                                                MUIA_Window_Title      , MSG_CUST_WIN_TITLE,
                                                MUIA_Window_ID         , ID_WIN_KONTRAHENCI,
                                                MUIA_Window_ScreenTitle, ScreenTitle,
//                        MUIA_Window_CloseGadget, FALSE,
                                                WindowContents,
                                                   VGroup,

///                          Lista kontrahent�w
                                                                  Child, VGroup,
                                                                                 GroupFrame,
                                                                                 MUIA_Weight, 60,
                                                                                 Child, LV_Cust_Klienci = ListviewObject,
                                                                                                MUIA_CycleChain, TRUE,
                                                                                                _MUIA_Listview_List, Golem_CustListObject, End,
                                                                                                End,

                                                                                 Child, HGroup,
                                                                                                Child, BT_Cust_Dodaj  = TextButton(MSG_CUST_DODAJ),
                                                                                                Child, BT_Cust_Edytuj = TextButton(MSG_CUST_EDYTUJ),
                                                                                                Child, BT_Cust_Usu�   = TextButton(MSG_CUST_SKASUJ),

                                                                                                Child, MUI_MakeObject(MUIO_VBar,1),

                                                                                                Child, HGroup,
//                                                       GroupFrame,
                                                                                                           MUIA_Weight, 20,
                                                                                                           Child, BT_Cust_Cleanup = TextButton(MSG_CUST_CLEANUP),
                                                                                                           End,

                                                                                                Child, MUI_MakeObject(MUIO_VBar,1),


                                                                                                Child, HGroup,
//                                                       GroupFrame,
                                                                                                           MUIA_Weight, 20,
                                                                                                           Child, BT_Cust_Filter    = _TextButton(MAG_FILTER),
                                                                                                           Child, BT_Cust_FilterAll = _TextButton(MAG_FILTER_ALL),
                                                                                                           End,
                                                                                                End,

                                                                                 End,
//|

                                                   Child, HGroup,
                                                                  Child, BT_Cust_Ok     = TextButton(MSG_CUST_OK),
                                                                  Child, BT_Cust_Cancel = TextButton(MSG_CUST_CANCEL),
                                                                  End,

                                                   End,
                                          End;
//|

        if( ! KontrahenciWindow )
           return( FALSE );


        // values

        cust_filter->Aktywny = FALSE;
        ApplyCustFilter(LV_Cust_Klienci, cust_filter, NULL);

        _disable( BT_Cust_Ok );
        _disable( BT_Cust_Cleanup );

        set( KontrahenciWindow, MUIA_Window_ActiveObject , LV_Cust_Klienci );
        set( KontrahenciWindow, MUIA_Window_DefaultObject, LV_Cust_Klienci );



        // notifications

        DoMethod(KontrahenciWindow , MUIM_Notify, MUIA_Window_CloseRequest, TRUE ,MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);
        DoMethod(BT_Cust_Ok        , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_OK);
        DoMethod(BT_Cust_Cancel    , MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(BT_Cust_Dodaj     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_DODAJ);
        DoMethod(BT_Cust_Edytuj    ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_EDYTUJ);
        DoMethod(BT_Cust_Usu�      ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_USU�);
        DoMethod(BT_Cust_Cleanup   ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CLEANUP);
        DoMethod(KontrahenciWindow ,MUIM_Notify, MUIA_Window_InputEvent, "f1", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_DODAJ);
        DoMethod(KontrahenciWindow ,MUIM_Notify, MUIA_Window_InputEvent, "f2", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_EDYTUJ);
        DoMethod(LV_Cust_Klienci   ,MUIM_Notify, _MUIA_Listview_DoubleClick, MUIV_EveryTime, MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_EDYTUJ);
        DoMethod(KontrahenciWindow ,MUIM_Notify, MUIA_Window_InputEvent, "del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_USU�);
        DoMethod(KontrahenciWindow ,MUIM_Notify, MUIA_Window_InputEvent, "control del", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CUST_USU�_AND_SKIP);

        DoMethod(KontrahenciWindow, MUIM_Notify, MUIA_Window_InputEvent, "f10", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_OK);
//    DoMethod(KontrahenciWindow, MUIM_Notify, MUIA_Window_InputEvent, "esc", MUIV_Notify_Application, 2, MUIM_Application_ReturnID, ID_CANCEL);

        DoMethod(BT_Cust_Filter     ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_FILTER);
        DoMethod(BT_Cust_FilterAll  ,MUIM_Notify, MUIA_Pressed, FALSE, MUIV_Notify_Application, 2 ,MUIM_Application_ReturnID, ID_CUST_FILTER_ALL);


        _attachwin( KontrahenciWindow );
        return( TRUE );

}
//|
/// EdycjaKontrahent�w

#define CUST_ZMIANY(x)  {ZmianyUKlient�w += x; set(BT_Cust_Ok, MUIA_Disabled, (ZmianyUKlient�w == 0)); set(BT_Cust_Cleanup, MUIA_Disabled, (ZmianyUKlient�w == 0));}

char EdycjaKontrahent�w(void)
{
char  running = TRUE;
ULONG signal  = 0;
char  result  = FALSE;
long  ZmianyUKlient�w = 0;

        set(app, MUIA_Application_Sleep, TRUE);

        if( ! EdycjaKontrahent�wSetup( &cust_filter ) )
           {
           DisplayBeep(0);
           MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
           _sleep(FALSE);
           return(NULL);
           }


        if(WinOpen(KontrahenciWindow))
                {
                while(running)
                  {
                  long ID = DoMethod(app, MUIM_Application_Input, &signal);
                  switch(ID)
                        {
                        case ID_CUST_DODAJ:
                           {
                           struct KlientList *klient;

                           klient = calloc(1, sizeof(struct KlientList));

                           if(klient)
                                  {
                                  CopyDefCustomerSettings(&klient->kl_klient);
                                  klient->Deleted = FALSE;

                                  if(EdycjaKlienta(klient, FALSE, 0) != FALSE)
                                         {
                                         long pos = DodajKlienta(klient);

                                         DoMethod(LV_Cust_Klienci, _MUIM_List_InsertSingle, klient, pos);

                                         UpdateSeparators( NULL );

                                         CUST_ZMIANY(1)
                                         }
                                  else
                                         {
                                         free(klient);
                                         }
                                  }
                           else
                                  {
                                  DisplayBeep(0);
                                  MUI_Request(app, KontrahenciWindow, 0, TITLE, MSG_OK, MSG_ERR_MEMORY);
                                  }
                           }
                           break;


                        case ID_CUST_EDYTUJ:
                           {
                           if(xget(LV_Cust_Klienci, _MUIA_List_Active) != _MUIV_List_Active_Off)
                                   {
                                   struct KlientList *klient;

                                   DoMethod(LV_Cust_Klienci, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &klient);


                                   if( klient->kl_klient.Separator )
                                           {
                                           DisplayBeep(0);
                                           break;
                                           }



                                   if(klient->Deleted)
                                         {
                                         if(MUI_Request(app, KontrahenciWindow, 0, TITLE, MSG_CUST_DELETED_GAD, MSG_CUST_DELETED, klient->kl_klient.Nazwa1))
                                           {
                                           klient->Deleted = FALSE;
                                           DoMethod(LV_Cust_Klienci, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                                           CUST_ZMIANY(-1)
                                           }
                                         else
                                           break;
                                         }

                                   switch(EdycjaKlienta(klient, TRUE, 0))
                                           {
                                           case TRUE:
                                                        DoMethod(LV_Cust_Klienci, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);
                                                        CUST_ZMIANY(1)
                                                        break;

                                           case REFRESH:
                                                        {
                                                        long pos;
                                                        set(LV_Cust_Klienci, _MUIA_List_Quiet, TRUE);

                                                        DoMethod(LV_Cust_Klienci, _MUIM_List_Remove, _MUIV_List_Remove_Active);

                                                        Remove((struct Node*)klient);
                                                        pos = DodajKlienta(klient);

                                                        DoMethod(LV_Cust_Klienci, _MUIM_List_InsertSingle, klient, pos);
                                                        set(LV_Cust_Klienci, _MUIA_List_Active, pos);

                                                        UpdateSeparators( NULL );

                                                        set(LV_Cust_Klienci, _MUIA_List_Quiet, FALSE);
                                                        CUST_ZMIANY(1)
                                                        }
                                                        break;
                                           }
                                         }
                           }
                           break;

                        case ID_CUST_USU�:
                        case ID_CUST_USU�_AND_SKIP:
                           {
                           if(xget(LV_Cust_Klienci, _MUIA_List_Active) != _MUIV_List_Active_Off)
                                   {
                                   struct KlientList *klient;

                                   DoMethod(LV_Cust_Klienci, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &klient);

                                   if( klient->kl_klient.Separator )
                                           {
                                           DisplayBeep(0);
                                           break;
                                           }


                                   klient->Deleted = ((klient->Deleted + 1) & 0x1);

                                   if(klient->Deleted)
                                         CUST_ZMIANY(1)
                                   else
                                         CUST_ZMIANY(-1)

                                   DoMethod(LV_Cust_Klienci, _MUIM_List_Redraw, _MUIV_List_Redraw_Active);

                                   if(ID == ID_CUST_USU�_AND_SKIP)
                                           set(LV_Cust_Klienci, _MUIA_List_Active, _MUIV_List_Active_Down);
                                   }
                           }
                           break;



                   case ID_CUST_KOPERTA:
                           {
                           if(xget(LV_Cust_Klienci, _MUIA_List_Active) != _MUIV_List_Active_Off)
                                   {
                                   struct KlientList *klient;

                                   DoMethod(LV_Cust_Klienci, _MUIM_List_GetEntry, _MUIV_List_GetEntry_Active, &klient);

                                   }
                           }

                           break;





                        case ID_CUST_FILTER:
                           CustFiltr(LV_Cust_Klienci, &cust_filter);
                           UpdateSeparators( &cust_filter );
                           break;

                        case ID_CUST_FILTER_ALL:
                           cust_filter.Aktywny = FALSE;
                           ApplyCustFilter(LV_Cust_Klienci, &cust_filter, NULL);
                           UpdateSeparators( &cust_filter );
                           break;





                   case ID_CLEANUP:
                                {
                                if(MUI_Request(app, KontrahenciWindow, 0, TITLE, MSG_CUST_CLEANUP_REQ_GAD, MSG_CUST_CLEANUP_REQ ))
                                   {
                                   int pos = xget( LV_Cust_Klienci, MUIA_List_Active );
                                   int count = (Usu�Klient�w( UK_REMOVE_DELETED ) * (-1) );

                                   CUST_ZMIANY( count )

                                   ApplyCustFilter(LV_Cust_Klienci, &cust_filter, NULL);
                                   set( LV_Cust_Klienci, MUIA_List_Active, pos );
                                   }
                                }
                                break;





                        case ID_OK:
                           if(ZmianyUKlient�w)
                                 {
                                 if(MUI_Request(app, KontrahenciWindow, 0, TITLE, MSG_CUST_SAVE_GAD, MSG_CUST_SAVE))
                                   {
                                   Usu�Klient�w( UK_REMOVE_DELETED );
                                   UpdateSeparators( NULL );
                                   ZapiszKlient�w();

                                   result  = TRUE;
                                   running = FALSE;
                                   }
                                 }
                           break;


                        case ID_CANCEL:
                           if(ZmianyUKlient�w)
                                   {
                                   if(MUI_Request(app, KontrahenciWindow, 0, TITLE, MSG_CUST_CANCEL_REQ_GAD, MSG_CUST_CANCEL_REQ))
                                         WczytajKlient�w();
                                   else
                                         break;
                                   }
                           running = FALSE;
                           break;

                        }
                  if(running && signal) Wait(signal);
                  }

                }
         else
                {
                DisplayBeep(NULL);
                MUI_Request(app, MainWindow, 0, TITLE, MSG_OK, MSG_ERR_WINDOW);
                }

        _detachwin( KontrahenciWindow );
        set(app, MUIA_Application_Sleep, FALSE);

        return(result);

}
//|

