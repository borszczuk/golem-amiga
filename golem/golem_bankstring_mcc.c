
/*
 * Golem - system finansowo ksiegowy
 *
 * Copyright (c) 1997, 2002 Marcin Orlowski <carlos@wfmh.org.pl>
 * 
 * http://sf.net/projects/golem-amiga/
 *
 * Wszelkie prawa zastrzezone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 */ 

/*
$Id: golem_bankstring_mcc.c,v 1.1 2003/01/01 20:40:53 carl-os Exp $.
*/


/*** includes ***/

#include <clib/alib_protos.h>
#include <clib/graphics_protos.h>
#include <clib/utility_protos.h>
#include <clib/muimaster_protos.h>
#include <pragmas/muimaster_pragmas.h>
#include <libraries/mui.h>
#if MUIMASTER_VLATEST <= 14
#include <mui/mui33_mcc.h>
#endif

#define MYDEBUG 0

#include <mui/betterstring_mcc.h>
#include <mui/muiundoc.h>

#include "string.h"
#include "stdio.h"
#include "golem_bankstring_mcc.h"
#include "golem_bstring_mcc.h"
#include "golem_structs.h"
//#include "//vapor/textinput/textinput_mcc.h"

#define NO_MT_MACROS
#include "golem_macros.h"
#include "golem_protos.h"

#define __NAME      "Golem_BankString"
#define CLASS       MUIC_Golem_BankString
#define SUPERCLASS  MUIC_Group

void _XCEXIT(int code) {}

struct Data
{
	Object *ST_Opis;
	Object *BT_Fill;
	Object *BT_Info;
	char   Buffer[CUST_BANK_LEN];
	char   Numer [CUST_KONTO_LEN];
	struct BankInfo info;
	struct Bank     bank;

	long   BankInfoRead;
};

#include "golem_bankstring_revision.h"

#define UserLibID BANKSTRING_VERSTAG " � 1998-2002 Marcin Orlowski <carlos@wfmh.org.pl>"
#define MASTERVERSION 14

#define VERSION  BANKSTRING_VERSION
#define REVISION BANKSTRING_REVISION

#define MCC_USES_IFFPARSE
#include "mccheader.c"

#define TITLE "WFMH Golem"

/// BANKI - STRINGS

#define MSG_OK             "_Ok"
#define MSG_BANK_UNKNOWN   "Przykro mi, moja baza danych nie zawiera\nopisu tego banku.\n\nJe�li masz pewno��, �e podany numer okre�la\nistniejacy bank b�d� jego oddzia�, prze�lij\nte informacje do W.F.M.H., aby�my mogli\nrozszerzy� baz� danych."
#define MSG_BANK_UNKNOWN_2 "%s\n\nPrzykro mi, moja baza danych nie zawiera\nopisu tego oddzia�u banku %s.\n\nJe�li masz pewno��, �e podany numer okre�la\nistniejacy oddzia� banku, prze�lij\njego dane do W.F.M.H., aby�my mogli\nrozszerzy� baz� danych."

//|

/// xget
LONG xget(Object *obj, ULONG attribute)
{
LONG x;

	get(obj, attribute, &x);
	return(x);
}
//|

/// DisplayInfo
ULONG ASM _DisplayInfo(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) struct opSet *msg)
{
struct Data *data = INST_DATA(cl,obj);


Object *app = (Object *)xget(obj, MUIA_ApplicationObject);
Object *window = (Object *)xget(obj, MUIA_WindowObject);

int  len;

	len = strlen(data->Numer);

	if( len >= 3 )
	   {
	   // sprawdzamy czy znamy taki bank

	   if( WczytajBankInfo(data->Numer, &data->info) )
		   {
		   if( len < 8 )
			   {
			   if( data->info.Info[0] == '\0' )
				   {
				   MUI_Request(app, window, 0, TITLE, MSG_OK, "%s (%s)", data->info.Nazwa, data->info.Skr�cona);
				   }
			   else
				   {
				   MUI_Request(app, window, 0, TITLE, MSG_OK, "%s (%s)\n\n%s", data->info.Nazwa, data->info.Skr�cona, data->info.Info);
				   }
			   }
		   }
	   else
		   {
		   DisplayBeep(0);
		   MUI_Request(app, window,  0, TITLE, MSG_OK, MSG_BANK_UNKNOWN);
		   return(0);
		   }
	   }


	if( len >= 8 )
	   {
	   // sprawdzamy czy oddzial znamy moze?

	   if( WczytajBank(data->Numer, &data->bank) )
		   {
		   if( data->info.Info[0] == '\0' )
			   {
			   MUI_Request(app, window, 0, TITLE, MSG_OK, "%s (%s)\n%s\n\n%s\n%s %s", data->info.Nazwa, data->info.Skr�cona, data->bank.Opis, data->bank.Ulica, data->bank.Kod, data->bank.Miasto);
			   }
		   else
			   {
			   MUI_Request(app, window, 0, TITLE, MSG_OK, "%s (%s)\n%s\n\n%s\n%s %s\n\n%s", data->info.Nazwa, data->info.Skr�cona, data->bank.Opis, data->bank.Ulica, data->bank.Kod, data->bank.Miasto, data->info.Info);
			   }
		   }
	   else
		   {
		   DisplayBeep(0);
		   MUI_Request(app, window, 0, TITLE, MSG_OK, MSG_BANK_UNKNOWN_2, data->info.Nazwa, data->info.Skr�cona);
		   return(0);
		   }
	   }


	return(0);
}
//|

/// ReadBank
ULONG ASM _ReadBank(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) struct opSet *msg)
{
struct Data *data = INST_DATA(cl,obj);

int  len = strlen(data->Numer);
char result;


		   if( len >= 8 )
			 {
			 // sprawdzamy czy oddzial znamy moze?

			 result = WczytajBank(data->Numer, &data->bank);

			 DoMethod( data->BT_Info, MUIM_MultiSet,
							   MUIA_Disabled, !result,
							   data->BT_Info, data->BT_Fill,
							   NULL);
			 }

	return(result);
}
//|
/// ReadBankInfo
ULONG ASM _ReadBankInfo(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) struct opSet *msg)
{
struct Data *data = INST_DATA(cl,obj);

int  len = strlen(data->Numer);
char result;

	   if( len < 3 )
		   {
		   data->BankInfoRead = FALSE;

		   return(0);
		   }

	   result = WczytajBankInfo(data->Numer, &data->info);

	   DoMethod( data->BT_Info, MUIM_MultiSet,
					   MUIA_Disabled, !result,
					   data->BT_Info, data->BT_Fill,
					   NULL);

	   if( result )
		  if( data->info.Info[0] != '\0' )
			 DoMethod(obj, MUIM_BankString_DisplayInfo);

	return(result);

}
//|
/// Fill
ULONG ASM _Fill(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) struct MUIP_BankString_Fill *msg)
{
struct Data *data = INST_DATA(cl,obj);

char *numer = data->Numer;
int  len = strlen(numer);

	   if( len < 3 )
		   {
		   if( msg->Fill )
			   {
			   setstring(data->ST_Opis, "");
			   strcpy( data->Buffer, "" );
			   }

		   return(0);
		   }


	   if( len == 3 || len == 4 )
		   {
		   // sprawdzamy czy bank taki znamy moze?

		   if( DoMethod(obj, MUIM_BankString_ReadBankInfo ))
			   {
			   if( msg->Fill )
				   {
				   setstring(data->ST_Opis, data->info.Nazwa);
				   strcpy( data->Buffer, data->info.Nazwa );
				   }
			   }

		   }
	   else
		   {
		   if( len >= 8 )
			 {
			 // sprawdzamy czy oddzial znamy moze?

			 if( DoMethod(obj, MUIM_BankString_ReadBank ))
			   {
			   if(msg->Fill)
				   {
				   // dane centrali wczytywalismy?
				   if( !data->BankInfoRead )
					   DoMethod(obj, MUIM_BankString_ReadBankInfo );

				   sprintf( data->Buffer, "%s %s", data->info.Skr�cona, data->bank.Opis );
				   setstring( data->ST_Opis, data->Buffer );
				   }
			   }

			 }
		   }

	return(0);
}
//|
/// Validate
ULONG ASM _Validate(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) struct opSet *msg)
{
struct Data *data = INST_DATA(cl,obj);
char Zmieniamy = FALSE;        // czy mozemy zmieniac opis banku


	if( strcmp( data->Buffer, getstr(data->ST_Opis)) != 0 )
	   {
	   // (l)uzer cos mieszal, nie ruszamy...

	   if( (*((char *)getstr(data->ST_Opis))) == '\0' )
		   {
		   // ... chyba ze gadzet pusty jest

		   Zmieniamy = TRUE;
		   }
	   }
	else
	   {
	   Zmieniamy = TRUE;
	   }

	DoMethod(obj, MUIM_BankString_Fill, Zmieniamy);

	return(0);
}
//|


/// OM_NEW

ULONG ASM _New( REG(a0) struct IClass *cl,
				REG(a2) Object *obj,
				REG(a1) struct opSet *msg)
{
struct Data *data;

Object *string;
Object *fill;
Object *button;

ULONG NoInputVal;
ULONG NoInputTag = TAG_IGNORE;
ULONG Frame = MUIV_Frame_String;

	NoInputVal = (Object *)GetTagData(MUIA_BankString_NoInput, FALSE, ((struct opSet *)msg)->ops_AttrList );

	if( NoInputVal == TRUE )
	   {
//       NoInputTag = MUIA_Textinput_NoInput;
	   NoInputTag = MUIA_BetterString_NoInput;
	   Frame      = MUIV_Frame_Text;
	   }


	obj = (Object *)DoSuperNew(cl,obj,
					MUIA_Group_Horiz, TRUE,
					MUIA_Group_Spacing, 1,

/*
					Child, string = StringObject, StringFrame,
//                                    MUIA_ControlChar, ParseHotKey(num),
									  MUIA_String_MaxLen, CUST_BANK_LEN,
									  MUIA_CycleChain, TRUE,
									  MUIA_String_AdvanceOnCR, TRUE,
									End,
*/

					Child, string = Golem_BStringObject,
									   MUIA_Frame, Frame,
									   NoInputTag, TRUE,
									   End,

// \033I[6:31]\033I[6:31]\033b Pobierz

					Child, fill   = MUI_MakeObject(MUIO_PopButton, MUII_ArrowLeft),
					Child, button = MUI_MakeObject(MUIO_PopButton, MUII_PopUp),

					TAG_DONE);

		if( obj )
		   {
		   /*** init data ***/
		   data = INST_DATA(cl, obj);

		   data->ST_Opis = string;
		   data->BT_Fill = fill;
		   data->BT_Info = button;

		   data->Numer[0]  = 0;
		   data->Buffer[0] = 0;

		   data->BankInfoRead = FALSE;


		   set(button, MUIA_CycleChain, TRUE);
		   _disable(fill);
		   _disable(button);

		   DoMethod(fill  , MUIM_Notify, MUIA_Pressed, FALSE, obj, 2, MUIM_BankString_Fill, TRUE);
		   DoMethod(button, MUIM_Notify, MUIA_Pressed, FALSE, obj, 1, MUIM_BankString_DisplayInfo);



		   /*** trick to set arguments ***/
		   msg->MethodID = OM_SET;
		   DoMethodA(obj, (Msg)msg);
		   msg->MethodID = OM_NEW;
		   }

//MUIA_BankString_Account

		return((ULONG)obj);
}
//|
/// OM_SET

ULONG ASM _Set(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct Data *data = INST_DATA(cl,obj);
struct TagItem *tags,*tag;


	for(tags=((struct opSet *)msg)->ops_AttrList; tag=NextTagItem(&tags); )
	   {
	   switch(tag->ti_Tag)
		  {

//        DoMethod(data->ST_Numer, MUIM_Notify, MUIA_String_Contents, MUIV_EveryTime, obj, 3, MUIM_Set, MUIA_BankString_Account, MUIV_TriggerValue);

		  case MUIA_BankString_Account:
			   {
			   strncpy( data->Numer, (char *)tag->ti_Data, CUST_BANK_LEN );

			   DoMethod(obj, MUIM_BankString_Validate);

			   return(TRUE);
			   }
			   break;

		   }
		}

	return(DoSuperMethodA(cl, obj, msg));
}

//|
/// OM_GET
static ULONG ASM _Get(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{
struct ProductList_Data *data = INST_DATA(cl,obj);
ULONG  *store = ((struct opGet *)msg)->opg_Storage;

	switch(((struct opGet *)msg)->opg_AttrID)
	   {
	   case MUIA_Version:
			*store = BANKSTRING_VERSION;
			return(TRUE);
			break;

	   case MUIA_Revision:
			*store = BANKSTRING_REVISION;
			return(TRUE);
			break;

	   }

	return(DoSuperMethodA(cl, obj, msg));
}
//|

/// Dispatcher
ULONG ASM SAVEDS _Dispatcher(REG(a0) struct IClass *cl, REG(a2) Object *obj, REG(a1) Msg msg)
{

	switch (msg->MethodID)
	   {
	   case OM_NEW: return(_New (cl, obj, (APTR)msg));
	   case OM_SET: return(_Set (cl, obj, (APTR)msg));
	   case OM_GET: return(_Get (cl, obj, (APTR)msg));

	   case MUIM_BankString_ReadBank     : return(_ReadBank (cl, obj, (APTR)msg));
	   case MUIM_BankString_ReadBankInfo : return(_ReadBankInfo (cl, obj, (APTR)msg));
	   case MUIM_BankString_Fill         : return(_Fill (cl, obj, (APTR)msg));
	   case MUIM_BankString_Validate     : return(_Validate (cl, obj, (APTR)msg));
	   case MUIM_BankString_DisplayInfo  : return(_DisplayInfo (cl, obj, (APTR)msg));

	   }

	return((ULONG)DoSuperMethodA(cl,obj,msg));

}
//|

